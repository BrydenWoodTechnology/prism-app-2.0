# PRISM App 2.0



The source code for PRISM 2.0 application by MAYOR OF LONDON, Bryden Wood and CAST Consultancy.
https://www.prism-app.io/

The application was made using Unity 2018.3.6f1. 
Although most of the elements were developed in-house at Bryden Wood, a couple of external assets were used.
* Graph and Chart: https://assetstore.unity.com/packages/tools/gui/graph-and-chart-78488
* JsonDotNet: https://assetstore.unity.com/packages/tools/input-management/json-net-for-unity-11347
* MK Toon Free: https://assetstore.unity.com/packages/vfx/shaders/mk-toon-free-68972
* MapBox Unity SDK: https://docs.mapbox.com/unity/maps/overview/


This is not present in the source code due to copyright and distribution restriction. 


## License
MIT License



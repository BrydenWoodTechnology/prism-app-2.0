{
  "$id": "1",
  "$type": "BrydenWood.DesignData.Building, BrydenWood.DesignData",
  "id": 0,
  "floors": {
    "$type": "BrydenWood.DesignData.Floor[], BrydenWood.DesignData",
    "$values": [
      {
        "$id": "2",
        "$type": "BrydenWood.DesignData.Floor, BrydenWood.DesignData",
        "vertices": {
          "$type": "System.Collections.Generic.List`1[[System.Double, mscorlib]], mscorlib",
          "$values": [
            0.0,
            1.0,
            2.0,
            3.0,
            4.0,
            5.0
          ]
        },
        "id": 0,
        "apartments": {
          "$type": "BrydenWood.DesignData.Apartment[], BrydenWood.DesignData",
          "$values": [
            {
              "$id": "3",
              "$type": "BrydenWood.DesignData.Apartment, BrydenWood.DesignData",
              "vertices": {
                "$type": "System.Collections.Generic.List`1[[System.Double, mscorlib]], mscorlib",
                "$values": [
                  6.0,
                  7.0,
                  2.0,
                  8.0
                ]
              },
              "id": 0,
              "apartmentType": "1b2p",
              "area": 0.0,
              "numberOfPeople": 0,
              "level": 0
            },
            {
              "$id": "4",
              "$type": "BrydenWood.DesignData.Apartment, BrydenWood.DesignData",
              "vertices": {
                "$type": "System.Collections.Generic.List`1[[System.Double, mscorlib]], mscorlib",
                "$values": [
                  9.0,
                  10.0,
                  11.0,
                  12.0
                ]
              },
              "id": 1,
              "apartmentType": "2b3p",
              "area": 0.0,
              "numberOfPeople": 0,
              "level": 0
            },
            {
              "$id": "5",
              "$type": "BrydenWood.DesignData.Apartment, BrydenWood.DesignData",
              "vertices": {
                "$type": "System.Collections.Generic.List`1[[System.Double, mscorlib]], mscorlib",
                "$values": [
                  13.0,
                  14.0,
                  15.0,
                  16.0
                ]
              },
              "id": 2,
              "apartmentType": "3b4p",
              "area": 0.0,
              "numberOfPeople": 0,
              "level": 0
            },
            {
              "$id": "6",
              "$type": "BrydenWood.DesignData.Apartment, BrydenWood.DesignData",
              "vertices": {
                "$type": "System.Collections.Generic.List`1[[System.Double, mscorlib]], mscorlib",
                "$values": [
                  10.0,
                  17.0,
                  18.0,
                  1.0
                ]
              },
              "id": 3,
              "apartmentType": "1b2p",
              "area": 0.0,
              "numberOfPeople": 0,
              "level": 0
            },
            {
              "$id": "7",
              "$type": "BrydenWood.DesignData.Apartment, BrydenWood.DesignData",
              "vertices": {
                "$type": "System.Collections.Generic.List`1[[System.Double, mscorlib]], mscorlib",
                "$values": [
                  17.0,
                  19.0,
                  20.0,
                  18.0
                ]
              },
              "id": 4,
              "apartmentType": "1b2p",
              "area": 0.0,
              "numberOfPeople": 0,
              "level": 0
            },
            {
              "$id": "8",
              "$type": "BrydenWood.DesignData.Apartment, BrydenWood.DesignData",
              "vertices": {
                "$type": "System.Collections.Generic.List`1[[System.Double, mscorlib]], mscorlib",
                "$values": [
                  19.0,
                  21.0,
                  22.0,
                  20.0
                ]
              },
              "id": 5,
              "apartmentType": "1b1p",
              "area": 0.0,
              "numberOfPeople": 0,
              "level": 0
            },
            {
              "$id": "9",
              "$type": "BrydenWood.DesignData.Apartment, BrydenWood.DesignData",
              "vertices": {
                "$type": "System.Collections.Generic.List`1[[System.Double, mscorlib]], mscorlib",
                "$values": [
                  4.0,
                  23.0,
                  24.0,
                  13.0
                ]
              },
              "id": 6,
              "apartmentType": "3b5p",
              "area": 0.0,
              "numberOfPeople": 0,
              "level": 0
            },
            {
              "$id": "10",
              "$type": "BrydenWood.DesignData.Apartment, BrydenWood.DesignData",
              "vertices": {
                "$type": "System.Collections.Generic.List`1[[System.Double, mscorlib]], mscorlib",
                "$values": [
                  23.0,
                  25.0,
                  26.0,
                  24.0
                ]
              },
              "id": 7,
              "apartmentType": "2b4p",
              "area": 0.0,
              "numberOfPeople": 0,
              "level": 0
            },
            {
              "$id": "11",
              "$type": "BrydenWood.DesignData.Apartment, BrydenWood.DesignData",
              "vertices": {
                "$type": "System.Collections.Generic.List`1[[System.Double, mscorlib]], mscorlib",
                "$values": [
                  25.0,
                  27.0,
                  28.0,
                  26.0
                ]
              },
              "id": 8,
              "apartmentType": "2b4p",
              "area": 0.0,
              "numberOfPeople": 0,
              "level": 0
            },
            {
              "$id": "12",
              "$type": "BrydenWood.DesignData.Apartment, BrydenWood.DesignData",
              "vertices": {
                "$type": "System.Collections.Generic.List`1[[System.Double, mscorlib]], mscorlib",
                "$values": [
                  27.0,
                  3.0,
                  7.0,
                  28.0
                ]
              },
              "id": 9,
              "apartmentType": "2b3p",
              "area": 0.0,
              "numberOfPeople": 0,
              "level": 0
            },
            {
              "$id": "13",
              "$type": "BrydenWood.DesignData.Apartment, BrydenWood.DesignData",
              "vertices": {
                "$type": "System.Collections.Generic.List`1[[System.Double, mscorlib]], mscorlib",
                "$values": [
                  29.0,
                  30.0,
                  31.0,
                  5.0
                ]
              },
              "id": 10,
              "apartmentType": "3b5p",
              "area": 0.0,
              "numberOfPeople": 0,
              "level": 0
            },
            {
              "$id": "14",
              "$type": "BrydenWood.DesignData.Apartment, BrydenWood.DesignData",
              "vertices": {
                "$type": "System.Collections.Generic.List`1[[System.Double, mscorlib]], mscorlib",
                "$values": [
                  32.0,
                  33.0,
                  0.0,
                  34.0
                ]
              },
              "id": 11,
              "apartmentType": "3b5p",
              "area": 0.0,
              "numberOfPeople": 0,
              "level": 0
            },
            {
              "$id": "15",
              "$type": "BrydenWood.DesignData.Apartment, BrydenWood.DesignData",
              "vertices": {
                "$type": "System.Collections.Generic.List`1[[System.Double, mscorlib]], mscorlib",
                "$values": [
                  16.0,
                  15.0,
                  30.0,
                  29.0
                ]
              },
              "id": 12,
              "apartmentType": "3b5p",
              "area": 0.0,
              "numberOfPeople": 0,
              "level": 0
            },
            {
              "$id": "16",
              "$type": "BrydenWood.DesignData.Apartment, BrydenWood.DesignData",
              "vertices": {
                "$type": "System.Collections.Generic.List`1[[System.Double, mscorlib]], mscorlib",
                "$values": [
                  12.0,
                  11.0,
                  33.0,
                  32.0
                ]
              },
              "id": 13,
              "apartmentType": "3b5p",
              "area": 0.0,
              "numberOfPeople": 0,
              "level": 0
            }
          ]
        },
        "levels": {
          "$type": "System.Int32[], mscorlib",
          "$values": [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9
          ]
        },
        "area": 0.0,
        "apartmentsArea": 0.0
      },
      {
        "$id": "17",
        "$type": "BrydenWood.DesignData.Floor, BrydenWood.DesignData",
        "vertices": {
          "$type": "System.Collections.Generic.List`1[[System.Double, mscorlib]], mscorlib",
          "$values": [
            35.0,
            36.0,
            37.0,
            38.0
          ]
        },
        "id": 1,
        "apartments": {
          "$type": "BrydenWood.DesignData.Apartment[], BrydenWood.DesignData",
          "$values": [
            {
              "$id": "18",
              "$type": "BrydenWood.DesignData.Apartment, BrydenWood.DesignData",
              "vertices": {
                "$type": "System.Collections.Generic.List`1[[System.Double, mscorlib]], mscorlib",
                "$values": [
                  39.0,
                  40.0,
                  41.0,
                  38.0
                ]
              },
              "id": 0,
              "apartmentType": "3b5p",
              "area": 0.0,
              "numberOfPeople": 0,
              "level": 10
            },
            {
              "$id": "19",
              "$type": "BrydenWood.DesignData.Apartment, BrydenWood.DesignData",
              "vertices": {
                "$type": "System.Collections.Generic.List`1[[System.Double, mscorlib]], mscorlib",
                "$values": [
                  42.0,
                  43.0,
                  35.0,
                  44.0
                ]
              },
              "id": 1,
              "apartmentType": "3b5p",
              "area": 0.0,
              "numberOfPeople": 0,
              "level": 10
            },
            {
              "$id": "20",
              "$type": "BrydenWood.DesignData.Apartment, BrydenWood.DesignData",
              "vertices": {
                "$type": "System.Collections.Generic.List`1[[System.Double, mscorlib]], mscorlib",
                "$values": [
                  45.0,
                  46.0,
                  40.0,
                  39.0
                ]
              },
              "id": 2,
              "apartmentType": "3b5p",
              "area": 0.0,
              "numberOfPeople": 0,
              "level": 10
            },
            {
              "$id": "21",
              "$type": "BrydenWood.DesignData.Apartment, BrydenWood.DesignData",
              "vertices": {
                "$type": "System.Collections.Generic.List`1[[System.Double, mscorlib]], mscorlib",
                "$values": [
                  47.0,
                  48.0,
                  43.0,
                  42.0
                ]
              },
              "id": 3,
              "apartmentType": "3b5p",
              "area": 0.0,
              "numberOfPeople": 0,
              "level": 10
            },
            {
              "$id": "22",
              "$type": "BrydenWood.DesignData.Apartment, BrydenWood.DesignData",
              "vertices": {
                "$type": "System.Collections.Generic.List`1[[System.Double, mscorlib]], mscorlib",
                "$values": [
                  49.0,
                  50.0,
                  48.0,
                  47.0
                ]
              },
              "id": 4,
              "apartmentType": "2b3p",
              "area": 0.0,
              "numberOfPeople": 0,
              "level": 10
            },
            {
              "$id": "23",
              "$type": "BrydenWood.DesignData.Apartment, BrydenWood.DesignData",
              "vertices": {
                "$type": "System.Collections.Generic.List`1[[System.Double, mscorlib]], mscorlib",
                "$values": [
                  51.0,
                  52.0,
                  46.0,
                  45.0
                ]
              },
              "id": 5,
              "apartmentType": "3b4p",
              "area": 0.0,
              "numberOfPeople": 0,
              "level": 10
            },
            {
              "$id": "24",
              "$type": "BrydenWood.DesignData.Apartment, BrydenWood.DesignData",
              "vertices": {
                "$type": "System.Collections.Generic.List`1[[System.Double, mscorlib]], mscorlib",
                "$values": [
                  37.0,
                  36.0,
                  53.0,
                  51.0
                ]
              },
              "id": 6,
              "apartmentType": "3b5p",
              "area": 0.0,
              "numberOfPeople": 0,
              "level": 10
            }
          ]
        },
        "levels": {
          "$type": "System.Int32[], mscorlib",
          "$values": [
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            19
          ]
        },
        "area": 0.0,
        "apartmentsArea": 0.0
      }
    ]
  },
  "grossFloorArea": 0.0,
  "controlPoints": {
    "$type": "System.Collections.Generic.List`1[[System.Double, mscorlib]], mscorlib",
    "$values": [
      15.0,
      50.0,
      0.0,
      15.0,
      15.0,
      0.0,
      50.0,
      15.0,
      0.0,
      50.0,
      0.0,
      0.0,
      0.0,
      0.0,
      0.0,
      0.0,
      50.0,
      0.0,
      44.0,
      6.0,
      0.0,
      50.0,
      6.0,
      0.0,
      44.0,
      15.0,
      0.0,
      9.0,
      9.0,
      0.0,
      15.0,
      9.0,
      0.0,
      15.0,
      20.0,
      0.0,
      9.0,
      20.0,
      0.0,
      0.0,
      6.0,
      0.0,
      6.0,
      6.0,
      0.0,
      6.0,
      20.0,
      0.0,
      0.0,
      20.0,
      0.0,
      25.0,
      9.0,
      0.0,
      25.0,
      15.0,
      0.0,
      35.0,
      9.0,
      0.0,
      35.0,
      15.0,
      0.0,
      41.0,
      9.0,
      0.0,
      41.0,
      15.0,
      0.0,
      15.0,
      0.0,
      0.0,
      15.0,
      6.0,
      0.0,
      27.0,
      0.0,
      0.0,
      27.0,
      6.0,
      0.0,
      39.0,
      0.0,
      0.0,
      39.0,
      6.0,
      0.0,
      0.0,
      35.0,
      0.0,
      6.0,
      35.0,
      0.0,
      6.0,
      50.0,
      0.0,
      9.0,
      35.0,
      0.0,
      15.0,
      35.0,
      0.0,
      9.0,
      50.0,
      0.0,
      15.0,
      50.0,
      3.0,
      15.0,
      0.0,
      3.0,
      0.0,
      0.0,
      3.0,
      0.0,
      50.0,
      3.0,
      0.0,
      35.0,
      3.0,
      6.0,
      35.0,
      3.0,
      6.0,
      50.0,
      3.0,
      9.0,
      35.0,
      3.0,
      15.0,
      35.0,
      3.0,
      9.0,
      50.0,
      3.0,
      0.0,
      20.0,
      3.0,
      6.0,
      20.0,
      3.0,
      9.0,
      20.0,
      3.0,
      15.0,
      20.0,
      3.0,
      9.0,
      9.0,
      3.0,
      15.0,
      9.0,
      3.0,
      0.0,
      6.0,
      3.0,
      6.0,
      6.0,
      3.0,
      15.0,
      6.0,
      3.0
    ]
  },
  "name": "FirstBuilding"
}
﻿Shader "Custom/OutlineLit"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_Color("Main Color", Color) = (1,1,1,1)
			_OutlineColor("Outline Color", Color) = (1,1,1,1)
			_EdgeWidth("Edge Width", Float) = 0.1
		_Sharpness("Sharpness", Float) = 100
			_ToneColor("Tone Color", Color) = (1,1,1,1)
			_ToneMap("Tone Map", Float) = 1.0
			_Opacity("Opacity", Float) = 1.0
			_Modulo("Modulo", Float) = 3.0
			_UseTexture("Use Texture", Float) = 1.0
	}
		SubShader
		{
			Tags { "RenderType" = "Opaque" }//"Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "TransparentCutout"}//
			LOD 200
			
			CGPROGRAM

			#pragma surface surf Standard fullforwardshadows //alpha//surface surf Lambert alpha //
			#pragma target 3.0

			sampler2D _MainTex;
				float4 _Color;
				float4 _OutlineColor;
				float _EdgeWidth;
				float _Sharpness;
				float _ToneMap;
				float4 _ToneColor;
				float _Opacity;
				float _Modulo;
				float _UseTexture;

			struct Input
			{
				float2 uv_MainTex;
			};

//#if _RENDERING_CUT_OUT
			void surf(Input IN, inout SurfaceOutputStandard o)
			{
				float x = IN.uv_MainTex.x;
				x = abs((x - 0.5) *2.0);
				float y = IN.uv_MainTex.y % (1 / _Modulo);
				y = (y - (1 / _Modulo)*0.5) * (2*_Modulo);
				y = abs(y);

				float2 uv = float2(x, y);
				float edge = _EdgeWidth / _Modulo;
				//float leftRight = clamp((uv.x - (1.0 - _EdgeWidth)) * _Sharpness, 0.0, 1.0);
				float upDown = clamp((uv.y - (1.0 - edge * _Modulo)) * _Sharpness, 0.0, 1.0);
				float alpha = clamp(/*leftRight + */upDown, 0.0, 1.0);
				fixed4 col;
				if (alpha == 0)
				{
					col = _Color;
					alpha = _Opacity == 1 ? 1:0;
					
				}
				else
				{
					col = _OutlineColor;
					alpha = 1;
				}

				float modX = IN.uv_MainTex.x % 0.1;
				float modY = IN.uv_MainTex.y % 0.1;
				float abx = abs(0.1 - modX);
				float aby = abs(0.1 - modY);

				if (abx<0.02 && IN.uv_MainTex.x == IN.uv_MainTex.y)
				{
					col = _OutlineColor;
					alpha = 1;
				}

				fixed4 c = col;
				if (_UseTexture > 0.5)
				{
					o.Albedo = c.rgb * tex2D(_MainTex, IN.uv_MainTex);//lerp(c.rgb,_ToneColor,_ToneMap);
				}
				else 
				{
					o.Albedo = c.rgb;
				}
				o.Alpha = alpha;
			}
//#else
//			void surf(Input IN, inout SurfaceOutputStandard o)
//			{
//				float x = IN.uv_MainTex.x;
//				x = abs((x - 0.5) *2.0);
//				float y = IN.uv_MainTex.y % (1 / _Modulo);
//				y = (y - (1 / _Modulo)*0.5) * (2 * _Modulo);
//				y = abs(y);
//
//				float2 uv = float2(x, y);
//				float leftRight = clamp((uv.x - (1.0 - _EdgeWidth)) * _Sharpness, 0.0, 1.0);
//				float upDown = clamp((uv.y - (1.0 - _EdgeWidth * _Modulo)) * _Sharpness, 0.0, 1.0);
//				float alpha = clamp(leftRight + upDown, 0.0, 1.0);
//				fixed4 col;
//				if (alpha == 0)
//				{
//					col = _Color;
//					alpha = _Opacity == 1 ? 1 : 0;
//
//				}
//				else
//				{
//					col = _OutlineColor;
//					alpha = 1;
//				}
//				fixed4 c = col;
//				o.Albedo = c.rgb;//lerp(c.rgb,_ToneColor,_ToneMap);
//				o.Alpha = alpha;
//			}
//#endif
			ENDCG
		}
			CustomEditor "OutlineLitGUI"
			FallBack "Diffuse"
				
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    public class ForceChidrenSize : MonoBehaviour
    {

        public float originalHeight;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void UpdateChildrenSize()
        {
            var height = GetComponent<RectTransform>().sizeDelta.y;
            var overallHeight = (transform.childCount - 1) * originalHeight;
            float newheight = originalHeight;
            if (overallHeight >= height)
            {
                newheight = height / (transform.childCount - 1);
            }
            for (int i = 0; i < transform.childCount - 1; i++)
            {
                if (transform.GetChild(i).GetComponent<Image>().color != transform.GetChild(i).GetComponent<Toggle>().colors.normalColor)
                {
                    transform.GetChild(i).GetComponent<RectTransform>().sizeDelta = new Vector2(transform.GetChild(i).GetComponent<RectTransform>().sizeDelta.x, newheight);
                    if (newheight < 60)
                    {
                        transform.GetChild(i).GetChild(0).gameObject.SetActive(false);
                    }
                    else
                    {
                        transform.GetChild(i).GetChild(0).gameObject.SetActive(true);
                    }
                }
                transform.GetChild(i).GetComponent<BuildingElement>().currentHeight = newheight;
            }
        }

        public void BuildingChildDeleted()
        {
            UpdateChildrenSize();
        }
    }
}

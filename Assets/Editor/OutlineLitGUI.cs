﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;

namespace BrydenWoodUnity
{
    public class OutlineLitGUI : ShaderGUI
    {

        Material target;
        MaterialEditor editor;
        MaterialProperty[] properties;

        public override void OnGUI(MaterialEditor materialEditor, MaterialProperty[] properties)
        {
            //base.OnGUI(materialEditor, properties);
            DoRenderingMode();
        }

        void SetKeyword(string keyword, bool state)
        {
            if (state)
            {
                target.EnableKeyword(keyword);
            }
            else
            {
                target.DisableKeyword(keyword);
            }
        }

        //As seen in https://catlikecoding.com/unity/tutorials/rendering
        void DoRenderingMode()
        {
            RenderingMode mode = RenderingMode.Opaque;
            //shouldShowAlphaCutoff = false;
            if (IsKeywordEnabled("_RENDERING_CUTOUT"))
            {
                mode = RenderingMode.Cutout;
                //shouldShowAlphaCutoff = true;
            }

            EditorGUI.BeginChangeCheck();
            mode = (RenderingMode)EditorGUILayout.EnumPopup(MakeLabel("Rendering Mode"), mode);
            if (EditorGUI.EndChangeCheck())
            {
                RecordAction("Rendering Mode");
                SetKeyword("_RENDERING_CUTOUT", mode == RenderingMode.Cutout);
                RenderingSettings settings = RenderingSettings.modes[(int)mode];
                foreach (Material m in editor.targets)
                {
                    m.renderQueue = (int)settings.queue;
                    m.SetOverrideTag("RenderType", settings.renderType);
                    m.SetInt("_SrcBlend", (int)settings.srcBlend);
                    m.SetInt("_DstBlend", (int)settings.dstBlend);
                    m.SetInt("ZWrite", settings.zWrite ? 1 : 0);
                }
            }
        }

        bool IsKeywordEnabled(string keyword)
        {
            return target.IsKeywordEnabled(keyword);
        }

        void RecordAction(string label)
        {
            editor.RegisterPropertyChangeUndo(label);
        }
        static GUIContent staticLabel = new GUIContent();
        static GUIContent MakeLabel(string text, string tooltip = null)
        {
            staticLabel.text = text;
            staticLabel.tooltip = tooltip;
            return staticLabel;
        }
    }

    enum RenderingMode
    {
        Opaque, Cutout,
    }

    struct RenderingSettings
    {
        public RenderQueue queue;
        public string renderType;
        public BlendMode srcBlend, dstBlend;
        public bool zWrite;


        public static RenderingSettings[] modes =
        {
            new RenderingSettings()
            {
                queue = RenderQueue.Geometry,
                renderType = "",
                srcBlend = BlendMode.One,
                dstBlend = BlendMode.Zero,
                zWrite = true
            },
            new RenderingSettings()
            {
                queue = RenderQueue.AlphaTest,
                renderType = "TransparentCutout",
                srcBlend = BlendMode.One,
                dstBlend = BlendMode.Zero,
                zWrite = true
            },
            new RenderingSettings()
            {
                queue = RenderQueue.Transparent,
                renderType = "Transparent",
                srcBlend = BlendMode.SrcAlpha,
                dstBlend = BlendMode.OneMinusSrcAlpha,
                zWrite = false
            },
            new RenderingSettings()
            {
                queue = RenderQueue.Transparent,
                renderType = "Transparent",
                srcBlend = BlendMode.One,
                dstBlend = BlendMode.OneMinusSrcAlpha,
                zWrite = false
            }
        };
    }
}


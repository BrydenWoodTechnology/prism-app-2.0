﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

[CustomEditor(typeof(ProceduralAptLayoutGenerator))]
public class ProceduralAptLayoutGeneratorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        ProceduralAptLayoutGenerator aptGen = (ProceduralAptLayoutGenerator)target;
        if (GUILayout.Button("Generate"))
        {
            aptGen.GenerateLayouts();
        }
    }
}

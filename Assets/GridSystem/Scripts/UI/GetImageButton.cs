﻿
using BrydenWoodUnity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
//#if UNITY_EDITOR
//using System.Windows.Forms;
//#endif
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    //Based on: https://stackoverflow.com/questions/35183253/unity3d-upload-a-image-from-pc-memory-to-webgl-app
    public class GetImageButton : MonoBehaviour
    {
        public GameObject imagePlane;
        public InputField width;
        public InputField height;
        public UnityEngine.UI.Button importImageButton;
        public MeshFilter contextMesh;
        public ResourcesLoader resourcesLoader;
        private string contextData = "";
//#if UNITY_EDITOR
//        private OpenFileDialog ofd;
//#endif
        private float widthValue;
        private float heightValue;

        private void Start()
        {
            ResourcesLoader.contextDataLoaded += OnContextDataLoaded;
            if (!float.TryParse(width.text, out widthValue) || !float.TryParse(height.text, out heightValue))
            {
                importImageButton.interactable = false;
            }
            else
            {
                importImageButton.interactable = true;
            }
        }

        private void Update()
        {

        }

        public void OnHeightValueChanged(string value)
        {
            if (!float.TryParse(width.text, out widthValue) || !float.TryParse(height.text, out heightValue))
            {
                importImageButton.interactable = false;
            }
            else
            {
                importImageButton.interactable = true;
            }
        }

        public void OnWidthValueChanged(string value)
        {
            if (!float.TryParse(width.text, out widthValue) || !float.TryParse(height.text, out heightValue))
            {
                importImageButton.interactable = false;
            }
            else
            {
                importImageButton.interactable = true;
            }
        }

        public void OnLoadContext()
        {
            resourcesLoader.SetContextFile();
        }

        public void OnContextDataLoaded()
        {
            contextData = resourcesLoader.contextData;
            contextMesh.mesh = BrydenWoodUtils.MeshFromObj(contextData)[0];//FastObjImporter.Instance.ImportFromString(contextData);
        }

        static string s_dataUrlPrefix = "data:image/png;base64,";
        public void ReceiveImage(string dataUrl)
        {
            Debug.Log("Receiving Image");
            if (dataUrl.StartsWith(s_dataUrlPrefix))
            {
                byte[] pngData = System.Convert.FromBase64String(dataUrl.Substring(s_dataUrlPrefix.Length));

                // Create a new Texture (or use some old one?)
                Texture2D tex = new Texture2D(1, 1); // does the size matter?
                if (tex.LoadImage(pngData))
                {
                    Renderer renderer = imagePlane.GetComponent<Renderer>();

                    renderer.material.mainTexture = tex;
                }
                else
                {
                    Debug.LogError("could not decode image");
                }
            }
            else
            {
                Debug.LogError("Error getting image:" + dataUrl);
            }
        }

        public void OnClick()
        {
            if (float.TryParse(width.text, out widthValue) && float.TryParse(height.text, out heightValue))
            {
                imagePlane.SetActive(true);
                imagePlane.transform.localScale = new Vector3(widthValue / 10.0f, 1, heightValue / 10.0f);
#if UNITY_EDITOR
                GetImageDektop();
#elif UNITY_WEBGL
        GetImage.GetImageFromUserAsync(gameObject.name, "ReceiveImage");
#endif
            }
        }

        public void GetImageDektop()
        {
#if UNITY_EDITOR
            //OpenFileDialog ofd = new OpenFileDialog();
            string path = UnityEditor.EditorUtility.OpenFilePanel("Set context image:", "", "png");
            if (path.Length!=0)
            {
                byte[] pngData = File.ReadAllBytes(path);
                Texture2D tex = new Texture2D(1, 1); // does the size matter?
                if (tex.LoadImage(pngData))
                {
                    Renderer renderer = imagePlane.GetComponent<Renderer>();

                    renderer.material.mainTexture = tex;
                }
                else
                {
                    Debug.LogError("could not decode image");
                }
            }
#endif
        }
    }
}

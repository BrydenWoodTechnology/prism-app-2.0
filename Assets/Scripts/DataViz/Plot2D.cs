﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.DataViz
{
    /// <summary>
    /// A MonoBehaviour component for drawing 2D plots
    /// </summary>
    public class Plot2D : MonoBehaviour
    {
        #region Public Variables
        public GameObject ptPrefab;
        public Transform ptParent;
        public Text minX;
        public Text maxX;
        public Text minY;
        public Text maxY;
        public PlotType plotType = PlotType.Scaleable;
        public int ptSize = 3;
        public Color ptColour = Color.black;
        public float xMin { get; private set; }
        public float yMin { get; private set; }
        public float xMax { get; private set; }
        public float yMax { get; private set; }
        #endregion

        #region Private Variables
        private List<Vector2> values;
        private RectTransform rectTrans;
        private float width;
        private float height;
        private GameObject currentObj;
        #endregion

        #region Monobehaviour Methods
        // Use this for initialization
        void Awake()
        {
            rectTrans = GetComponent<RectTransform>();
            width = rectTrans.rect.width;
            height = rectTrans.rect.height;
        }

        // Update is called once per frame
        void Update()
        {
            //if (Input.GetKeyDown("m"))
            //{
            //    AddValue(new Vector2(1, 1));
            //    AddValue(new Vector2(2, 2));
            //}
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Adds a value to the plot
        /// </summary>
        /// <param name="value">The value to be added (Vector2)</param>
        public void AddValue(Vector2 value)
        {
            if (values == null)
            {
                values = new List<Vector2>();
                xMin = yMin = float.MaxValue;
                xMax = yMax = float.MinValue;
            }

            if (value.x < xMin)
            {
                xMin = value.x;
                minX.text = xMin.ToString();
            }
            if (value.y < yMin)
            {
                yMin = value.y;
                minY.text = yMin.ToString();
            }
            if (value.x > xMax)
            {
                xMax = value.x;
                maxX.text = xMax.ToString();
            }
            if (value.y > yMax)
            {
                yMax = value.y;
                maxY.text = yMax.ToString();
            }

            values.Add(value);
            currentObj = Instantiate(ptPrefab, ptParent);
            currentObj.transform.localPosition = new Vector3(width / 2, height / 2, 0);

            DrawGraph();
        }

        /// <summary>
        /// Draws the graph
        /// </summary>
        public void DrawGraph()
        {
            switch (plotType)
            {
                case PlotType.Scaleable:
                    DrawScaleableGraph();
                    break;
            }
        }
        #endregion

        #region Private Methods
        private void DrawScaleableGraph()
        {
            if (ptParent.childCount > 1)
            {
                for (int i = 0; i < ptParent.childCount; i++)
                {
                    ptParent.GetChild(i).localPosition = new Vector3(BrydenWoodUtils.Remap(values[i].x, xMin, xMax, 0, width), BrydenWoodUtils.Remap(values[i].y, yMin, yMax, 0, height), 0);
                }
            }
        }
        #endregion
    }

    #region PLOT TYPE
    /// <summary>
    /// The type of the plot
    /// </summary>
    public enum PlotType
    {
        /// <summary>
        /// Rolling with a fixed number of values
        /// </summary>
        Rolling,
        /// <summary>
        /// Standards with a fixed scale
        /// </summary>
        Standard,
        /// <summary>
        /// Scalable to fit within a fixed size
        /// </summary>
        Scaleable
    }
    #endregion
}

﻿using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace BrydenWoodUnity.UIElements
{
    [CustomEditor(typeof(RibbonSetup))]
    public class RibbonSetupEditor : Editor
    {
        int tab_num = 1;
        Color32 bookmark_color = Color.yellow;
        string title = "Tab Title";
        bool expand = true;
        List<bool> tab_drop_values = new List<bool>();
        List<Color32> tab_Colors = new List<Color32>();
        List<string> tab_titles = new List<string>();

        private void OnEnable()
        {

        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            RibbonSetup ribbon = (RibbonSetup)target;
            tab_num = EditorGUILayout.IntField("Number of tabs", tab_num);
            //bookmark_color = ribbon.tab_prefab.transform.GetChild(0).GetComponent<Image>().color;

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Populate Ribbon Menu", EditorStyles.boldLabel);
            EditorGUILayout.Space();

            GUILayout.BeginHorizontal();

            if (GUILayout.Button("Populate Ribbon"))
            {
                if (ribbon.transform.childCount > 0)
                {
                    ribbon.current_tabs.Clear();
                    ribbon.tab_colors.Clear();
                    for (int i = ribbon.transform.childCount; i > 0; i--)
                    {
                        DestroyImmediate(ribbon.transform.GetChild(0).gameObject);
                    }
                }

                if (ribbon.transform.childCount == 0)
                {
                    ribbon.PopulateRibbon();
                }
            }
            if (GUILayout.Button("Add Tab"))
            {
                ribbon.tabs.Add(TabTypes.PopUp);
                ribbon.AddOneRibbonItem(ribbon.tabs.Last());
                //ribbon.PopulateRibbon(1);
                //ribbon.AddOneRibbonItem()
                //tab_num++;
            }

            GUILayout.EndHorizontal();

            if (GUILayout.Button("Clear Ribbon"))
            {
                ribbon.ReadyToPlay(true);
                ribbon.current_tabs.Clear();
                ribbon.tab_colors.Clear();
                for (int i = ribbon.transform.childCount; i > 0; i--)
                {
                    DestroyImmediate(ribbon.transform.GetChild(0).gameObject);
                }
                tab_num = 0;
                tab_drop_values.Clear();
                tab_Colors.Clear();
                tab_titles.Clear();
            }
            EditorGUILayout.Space();
            if (GUILayout.Button("Colorize with Image"))
            {
                ribbon.DominantColors(tab_num);
                for (int t = 0; t < tab_num; t++)
                {
                    tab_Colors[t] = ribbon.temp_centroids[t];
                }
            }
            EditorGUILayout.Space();
            if (GUILayout.Button("Ready!"))
            {
                ribbon.ReadyToPlay(false);
            }

            GUILayout.Space(10);
            EditorGUILayout.LabelField("Tabs Menu", EditorStyles.boldLabel);
            EditorGUILayout.Space();
            PopulateTabsMenu(ribbon);
            ApplyTabAttributes(ribbon);

        }

        public void PopulateTabsMenu(RibbonSetup myribbon)
        {
            int tab_length = myribbon.current_tabs.Count;
            if (tab_length != 0)
            {
                expand = EditorGUILayout.Foldout(expand, "Current Tabs");

                for (int j = 0; j < tab_length; j++)
                {
                    bool tab_drop = false;
                    tab_drop_values.Add(tab_drop);
                    tab_Colors.Add(bookmark_color);
                    tab_titles.Add(title);
                }
                if (expand)
                {
                    EditorGUI.indentLevel++;
                    EditorGUILayout.Space();

                    for (int i = 0; i < tab_length; i++)
                    {
                        tab_drop_values[i] = EditorGUILayout.Foldout(tab_drop_values[i], myribbon.current_tabs[i].name);


                        if (tab_drop_values[i])
                        {
                            tab_Colors[i] = EditorGUILayout.ColorField("bookmark color", tab_Colors[i]);
                            tab_titles[i] = EditorGUILayout.TextField("Bookmark Title", tab_titles[i]);
                        }
                        EditorGUILayout.Space();

                    }
                }
            }
        }

        public void ApplyTabAttributes(RibbonSetup myribbon)
        {
            for (int i = 0; i < myribbon.current_tabs.Count; i++)
            {
                myribbon.transform.GetChild(i).GetChild(0).GetComponent<Image>().color = tab_Colors[i];
                myribbon.transform.GetChild(i).GetChild(1).GetComponent<Text>().text = tab_titles[i];
            }
        }

        public void DefineTabTitle(RibbonSetup myribbon)
        {
            for (int i = 0; i < myribbon.current_tabs.Count; i++)
            {

            }
        }
    }
}

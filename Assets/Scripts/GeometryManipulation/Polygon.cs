﻿using BrydenWoodUnity.UIElements;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using BT = BrydenWoodUnity.GeometryManipulation.ThreeJs;

namespace BrydenWoodUnity.GeometryManipulation
{

    /// <summary>
    /// A Unity Event class for when the polygon is being edited
    /// </summary>
    [System.Serializable]
    public class PolygonEvent : UnityEvent<Polygon>
    {

    }

    /// <summary>
    /// A MonoBehaviour component for a Polygon
    /// </summary>
    [RequireComponent(typeof(MeshFilter))]
    public class Polygon : MonoBehaviour, ISelectableGeometry
    {
        #region Public Fields
        /// <summary>
        /// The mesh filter which will be used for the mesh
        /// </summary>
        [Header("Unity specific fields:")]
        public MeshFilter meshFilter;

        public GameObject moveUiElement;
        public GameObject edgeUiElement;
        public GameObject controlPoint;
        public GameObject controlPointUi;
        public Transform testPointForProjection;

        public Material errorMaterial { get; private set; }
        public Material selectMaterial { get; private set; }
        public bool isSelected { get; private set; }

        /// <summary>
        /// The area of the polygon
        /// </summary>
        [Space(10)]
        [Header("Geometry specific fields:")]
        [Tooltip("The area of the polygon (read-only)")]
        [SerializeField]
        private float area;
        public float Area
        {
            get
            {
                return CalculateArea();
            }
        }

        /// <summary>
        /// The perimeter of the polygon 
        /// </summary>
        [Tooltip("The perimeter of the polygon (read-only)")]
        public float perimeter;

        [Tooltip("The offset from base to avoid Z-fighting")]
        public float offsetFromBase = 0.1f;
        /// <summary>
        /// Whether the resulting mesh should be flipped
        /// </summary>
        public bool flip = false;
        public bool capped = false;
        public bool moveable = false;
        public bool editEdges = true;
        public bool isClockWise = true;
        public bool isBuildingVertex = true;
        public bool moveColinear = false;
        public bool closed = false;
        public bool isCopy = false;
        private bool fill = false;

        [HideInInspector]
        public Color typeColor;

        /// <summary>
        /// The mesh of the polygon
        /// </summary>
        [HideInInspector]
        public Mesh mesh;
        /// <summary>
        /// The vertices of the polygon as a list of PolygonVertex
        /// </summary>
        [HideInInspector]
        public List<PolygonVertex> vertices;
       
        [HideInInspector]
        public Extrusion extrusion;

        [HideInInspector]
        public ControlPointUI movePoint;

        [Header("Distances Settings")]
        public bool showDistances = false;
        public float distanceOffset = 1.0f;
        [HideInInspector]
        public List<GameObject> distances;
        public bool useCanvas = false;
        [HideInInspector]
        public Material distancesMaterial;
        [HideInInspector]
        public Transform distancesParent;
        [HideInInspector]
        public Transform distanceUIParent;
        [HideInInspector]
        public List<Text> distancesUI;
        [HideInInspector]
        public float lineWidth = 0.2f;

        public bool hasListeners { get; private set; }
        public ControlPointUI selectedCpui { get; private set; }
        public PolygonVertex selectedVertex { get; private set; }
        public Vector3 Centre
        {
            get
            {
                return GetCentroid();
            }
        }

        public PolygonVertex this[int i]
        {
            get { return vertices[i]; }
        }

        public int Count { get { return vertices.Count; } }
        #endregion

        #region Private Fields
        protected int[] triangles { get; set; }
        protected Material originalMaterial;
        protected Material secondOriginalMaterial;
        protected Material typeMaterial;
        private List<GameObject> edgeControlPoints;
        private Dictionary<ControlPointUI, EdgeRelation> edges;
        private ControlPointUI edgeControlPointSelected;
        private GameObject currentDistance { get; set; }
        #endregion

        #region Events
        /// <summary>
        /// Used when the polygon has been updated
        /// </summary>
        /// <param name="sender"></param>
        public delegate void PolygonUpdated(Polygon sender);
        /// <summary>
        /// Triggered when the polygon has been updated
        /// </summary>
        //public event PolygonUpdated updated;
        /// <summary>
        /// Triggered when the polygon has been selected
        /// </summary>
        public event OnSelectGeometry selected;
        /// <summary>
        /// Triggered when the geometry is deselected
        /// </summary>
        public event OnSelectGeometry deselected;
        /// <summary>
        /// Triggered when the polygon has stopped moving
        /// </summary>
        //public event PolygonUpdated stoppedMoving;


        public PolygonEvent updated;
        //public PolygonEvent selected;
        public PolygonEvent stoppedMoving;

        protected virtual void OnUpdated()
        {
            if (updated != null)
            {
                //                updated(this);
                updated.Invoke(this);
            }
        }
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            //vertices = new List<PolygonVertex>();
            if (meshFilter == null)
            {
                meshFilter = GetComponent<MeshFilter>();
            }
            if (showDistances)
            {
                if (distancesMaterial == null)
                {
                    distancesMaterial = new Material(Shader.Find("Standard"));
                    distancesMaterial.color = new Color(0.93f, 0.15f, 0.43f);
                    distancesMaterial.SetColor("_EmissionColor", new Color(0.93f, 0.15f, 0.43f,1f));
                    distancesMaterial.EnableKeyword("_EMISSION");
                    //distancesMaterial.SetColor("_EmissionColor", Color.red);
                }
                if (distances == null)
                {
                    distances = new List<GameObject>();
                    distancesParent = new GameObject("DistancesParent").transform;
                    distancesParent.SetParent(transform);
                    distancesUI = new List<Text>();
                    distanceUIParent = new GameObject("DistancesUIParent").transform;
                    distanceUIParent.SetParent(GameObject.Find("Canvas").transform);
                    distanceUIParent.SetAsFirstSibling();
                    distanceUIParent.localScale = Vector3.one;
                }
            }
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnDestroy()
        {
            if (this.vertices != null && this.vertices.Count != 0)
            {
                for (int i = 0; i < this.vertices.Count; i++)
                {
                    this.vertices[i].vertexMoved -= UpdateGeometry;
                    if (vertices[i] != null)
                    {
                        Destroy(vertices[i].gameObject);
                    }
                }
            }
            if (distances != null && distances.Count > 0)
            {
                for (int i = 0; i < distances.Count; i++)
                {
                    Destroy(distances[i].gameObject);
                    if (distancesUI[i].gameObject != null)
                        Destroy(distancesUI[i].gameObject);
                }
                distances = null;
            }
        }

        void OnEnable()
        {
            for (int i = 0; i < vertices.Count; i++)
            {
                vertices[i].gameObject.SetActive(true);
            }

            if (distances != null && distances.Count > 0)
            {
                for (int i = 0; i < distances.Count; i++)
                {
                    if (distances[i] != null)
                        distances[i].gameObject.SetActive(true);
                    if (distancesUI[i] != null)
                        distancesUI[i].gameObject.SetActive(true);
                }
            }
        }

        void OnDisable()
        {
            for (int i = 0; i < vertices.Count; i++)
            {
                if (vertices[i] != null)
                {
                    vertices[i].gameObject.SetActive(false);
                }
            }
            if (movePoint != null)
            {
                HideMoveUI();
            }
            if (edges != null)
            {
                HideEdgesUI();
            }

            if (distances != null && distances.Count > 0)
            {
                for (int i = 0; i < distances.Count; i++)
                {
                    if (distances[i] != null)
                        distances[i].gameObject.SetActive(false);
                    if (distancesUI[i] != null)
                        distancesUI[i].gameObject.SetActive(false);
                }
            }
        }

        private void OnDrawGizmosSelected()
        {
            if (vertices != null)
            {
                if (closed)
                {
                    for (int i = 0; i < vertices.Count; i++)
                    {
                        Gizmos.color = Color.yellow;
                        Gizmos.DrawCube(vertices[i].currentPosition, Vector3.one * 0.5f);
                        Gizmos.color = Color.green;
                        Gizmos.DrawLine(vertices[i].currentPosition, vertices[(i + 1) % vertices.Count].currentPosition);
                    }
                }
                else
                {
                    for (int i = 0; i < vertices.Count; i++)
                    {
                        Gizmos.color = Color.yellow;
                        Gizmos.DrawCube(vertices[i].currentPosition, Vector3.one * 0.5f);
                        if (i < vertices.Count - 1)
                        {
                            Gizmos.color = Color.green;
                            Gizmos.DrawLine(vertices[i].currentPosition, vertices[i + 1].currentPosition);
                        }
                    }
                }
            }
        }

        private void OnDrawGizmos()
        {
            if (vertices != null && testPointForProjection != null)
            {
                var pt = PointOnCurve(testPointForProjection.position);
                Gizmos.DrawLine(pt, testPointForProjection.position);
            }
        }
        #endregion


        #region Public Methods
        /// <summary>
        /// Initiates the Instance
        /// </summary>
        /// <param name="vertices">The list of PolygonVertex to be used as vertices by the Polygon</param>
        /// <param name="closed">Whether the polygon is closed or not</param>
        /// <param name="fill">Whether the polygon is filled or not</param>
        /// <param name="isBuildingVertex">Whether is part of the building vertices</param>
        public virtual void Initialize(List<PolygonVertex> vertices, bool closed = true, bool fill = false, bool isBuildingVertex = true)
        {
            Vector3 centre = new Vector3();
            if (vertices.Count != 0)
            {
                for (int i = 0; i < vertices.Count; i++)
                {
                    centre += vertices[i].currentPosition;
                }
                centre /= vertices.Count;
            }
            else
            {
                Debug.Log("Zero list vertices!");
            }
            this.isBuildingVertex = isBuildingVertex;
            transform.position = new Vector3(transform.position.x, vertices[0].currentPosition.y, transform.position.z);
            SetPolyline(vertices);
            this.closed = closed;
            this.fill = fill;
            if (closed && fill)
            {
                GenerateMeshes();
            }
            originalMaterial = GetComponent<MeshRenderer>().material;
            
            typeMaterial = new Material(Resources.Load("Materials/TypePolygonMaterial") as Material);
            if (errorMaterial == null)
            {
                errorMaterial = Resources.Load("Materials/ErrorMaterial") as Material;
            }
            if (selectMaterial == null)
            {
                selectMaterial = Resources.Load("Materials/SelectMaterial") as Material;
            }
        }

        /// <summary>
        /// Sets the color of the type material
        /// </summary>
        /// <param name="color">Color</param>
        public void SetTypeColor(Color color)
        {
            typeMaterial.color = color;
            typeColor = color;
        }

        /// <summary>
        /// Sets the vertex colours of the polygon
        /// </summary>
        /// <param name="color">The vertex colour</param>
        public void SetVertexColors(Color color)
        {
            Color[] colours = new Color[GetComponent<MeshFilter>().sharedMesh.vertexCount];
            for (int i = 0; i < colours.Length; i++)
            {
                colours[i] = color;
            }
            GetComponent<MeshFilter>().sharedMesh.colors = colours;
            extrusion.SetVertexColors(color);
        }

        /// <summary>
        /// Sets the polyline of the vertices of the polygon
        /// </summary>
        /// <param name="vertices">The list of PolygonVertex to be used as vertices by the Polygon</param>
        public void SetPolyline(List<PolygonVertex> vertices)
        {
            if (this.vertices != null && this.vertices.Count != 0)
            {
                for (int i = 0; i < this.vertices.Count; i++)
                {
                    this.vertices[i].vertexMoved -= UpdateGeometry;
                }
            }
            this.vertices = new List<PolygonVertex>();
            this.vertices = vertices;
            List<Vector3> verts = new List<Vector3>();
            for (int i = 0; i < vertices.Count; i++)
            {
                vertices[i].SetPolygon(this);
                verts.Add(vertices[i].currentPosition);
                vertices[i].vertexMoved += UpdateGeometry;
            }
            isClockWise = IsClockWise(verts);
            hasListeners = true;
        }

        /// <summary>
        /// Highlights the polygon if there is an error
        /// </summary>
        /// <param name="error">Toggles the highlight</param>
        public void ErrorHighlight(bool error)
        {
            if (error)
            {
                GetComponent<MeshRenderer>().material = errorMaterial;
                extrusion.GetComponent<MeshRenderer>().material = errorMaterial;
            }
            else
            {
                GetComponent<MeshRenderer>().material = originalMaterial;
                extrusion.GetComponent<MeshRenderer>().material = originalMaterial;
            }
        }

        /// <summary>
        /// Highlights the polygon if it is selected
        /// </summary>
        /// <param name="select">Toggles the highlight</param>
        public void SelectHighlight(bool select)
        {
            isSelected = select;
            if (select)
            {
                GetComponent<MeshRenderer>().material = selectMaterial;
                extrusion.GetComponent<MeshRenderer>().material = selectMaterial;
                if (moveable)
                {
                    DisplayMoveUI();
                }
                if (editEdges)
                {
                    DisplayEdgesUI();
                }
            }
            else
            {
                GetComponent<MeshRenderer>().material = originalMaterial;
                extrusion.GetComponent<MeshRenderer>().material = originalMaterial;
                if (moveable)
                {
                    HideMoveUI();
                }
                if (editEdges)
                {
                    HideEdgesUI();
                }
            }
        }

        /// <summary>
        /// Called when the polygon has been moved
        /// </summary>
        /// <param name="sender">The object that moved it</param>
        public void OnMoved(object sender)
        {
            HideEdgesUI();
            if (vertices != null)
            {
                for (int i = 0; i < vertices.Count; i++)
                {
                    vertices[i].cpui.gameObject.SetActive(false);
                }
            }
            if (updated != null)
            {
                //updated(this);
                updated.Invoke(this);
            }
        }

        /// <summary>
        /// Called when the polygon stops moving
        /// </summary>
        /// <param name="sender">The object that moved it</param>
        public void OnStoppedMoving(object sender)
        {
            if (stoppedMoving != null)
            {
                //stoppedMoving(this);
                stoppedMoving.Invoke(this);
            }

            for (int i = 0; i < vertices.Count; i++)
            {
                vertices[i].cpui.gameObject.SetActive(true);
                var transVec = transform.localPosition + mesh.vertices[i] - new Vector3(0, offsetFromBase, 0);
                vertices[i].UpdatePosition(transVec);
                vertices[i].cpui.UpdateUIPosition();
            }
            HideMoveUI();
            DisplayMoveUI();
            DisplayEdgesUI();
        }

        /// <summary>
        /// Hightlights the polygon with the type colour
        /// </summary>
        /// <param name="show">Toggles the highlight</param>
        public void TypeHighlight(bool show)
        {
            if (!isSelected)
            {
                if (show)
                {
                    GetComponent<MeshRenderer>().material = typeMaterial;
                    if (extrusion.GetComponent<MeshRenderer>().materials.Length > 1)
                    {
                        extrusion.GetComponent<MeshRenderer>().materials = new Material[] { typeMaterial, typeMaterial };
                    }
                    else
                    {
                        extrusion.GetComponent<MeshRenderer>().material = typeMaterial;
                    }
                }
                else
                {
                    GetComponent<MeshRenderer>().material = originalMaterial;
                    if (extrusion.GetComponent<MeshRenderer>().materials.Length > 1)
                    {
                        if (secondOriginalMaterial != null)
                            extrusion.GetComponent<MeshRenderer>().materials = new Material[] { originalMaterial, secondOriginalMaterial };
                        else
                            extrusion.GetComponent<MeshRenderer>().materials = new Material[] { originalMaterial, originalMaterial };
                    }
                    else
                    {
                        extrusion.GetComponent<MeshRenderer>().material = originalMaterial;
                    }
                }
            }
        }

        /// <summary>
        /// Sets the original material of the polygon
        /// </summary>
        /// <param name="material">New material</param>
        public void SetOriginalMaterial(Material material, Material secondMaterial = null)
        {
            originalMaterial = material;
            secondOriginalMaterial = secondMaterial;
            TypeHighlight(false);
        }

        /// <summary>
        /// Sets the type material of the polygon as the original material
        /// </summary>
        public void SetTypeMaterialAsOriginal()
        {
            originalMaterial = typeMaterial;
            TypeHighlight(false);
        }

        /// <summary>
        /// Called when the polygon is selected
        /// </summary>
        public void Select()
        {
            if (selected != null)
            {
                selected();
            }
        }

        /// <summary>
        /// Called to deselect the geometry
        /// </summary>
        public void DeSelect()
        {
            if (deselected != null)
            {
                deselected();
            }
        }

        /// <summary>
        /// Sets the display  of the polygon in Standard Mode
        /// </summary>
        public void DisplayStandardMode()
        {
            GetComponent<MeshRenderer>().material = originalMaterial;
            extrusion.GetComponent<MeshRenderer>().material = originalMaterial;
            extrusion.DisplayStandardMode();
        }

        /// <summary>
        /// Shows and hides the control points
        /// </summary>
        /// <param name="show">Whether to show or hide the control points</param>
        public void ShowControlPoints(bool show)
        {
            for (int i = 0; i < vertices.Count; i++)
            {
                vertices[i].cpui.gameObject.SetActive(show);
                vertices[i].gameObject.SetActive(show);
            }
        }

        /// <summary>
        /// Returns the bounding box of the polygon for a given transformation matrix
        /// </summary>
        /// <param name="transform">The transformation matrix in which the bounding box is calcuated</param>
        /// <returns>The corners of the bounding box</returns>
        public Vector3[] GetBoundingBox(Transform transform)
        {
            Vector3[] corners = new Vector3[4];

            List<Vector3> relativePoints = new List<Vector3>();
            for (int i = 0; i < vertices.Count; i++)
            {
                relativePoints.Add(transform.InverseTransformPoint(vertices[i].currentPosition));
            }

            var xValues = relativePoints.Select(x => x.x).ToList();
            //xValues.Sort();
            var zValues = relativePoints.Select(x => x.z).ToList();
            //zValues.Sort();

            var xMax = xValues.Max();
            var xMin = xValues.Min();
            var zMax = zValues.Max();
            var zMin = zValues.Min();
            var y = transform.InverseTransformPoint(vertices[0].currentPosition).y;
            corners[0] = transform.TransformPoint(new Vector3(xMin, y, zMin));
            corners[1] = transform.TransformPoint(new Vector3(xMin, y, zMax));
            corners[2] = transform.TransformPoint(new Vector3(xMax, y, zMax));
            corners[3] = transform.TransformPoint(new Vector3(xMax, y, zMin));

            return corners;
        }

        /// <summary>
        /// Returns the Vectors of the vertices
        /// </summary>
        /// <returns>A List of Vectors</returns>
        public List<Vector3> GetVectors()
        {
            List<Vector3> positions = new List<Vector3>();
            for (int i = 0; i < vertices.Count; i++)
            {
                positions.Add(vertices[i].transform.position);
            }
            return positions;
        }

        /// <summary>
        /// Offsets a polygon as separate line segments on the XZ plane
        /// </summary>
        /// <param name="distance">The distance for the offset</param>
        /// <returns>List of point (two for each line segment)</returns>
        public Vector3[] OffsetAsLineSegments(float distance)
        {
            return OffsetAsLineSegments(distance, Vector3.up);
        }


        /// <summary>
        /// Offsets a polygon as separate line segments on a given plane
        /// </summary>
        /// <param name="distance">The distance for the offset</param>
        /// <param name="normal">The normal of the plane on which the offset will take place</param>
        /// <returns>List of point (two for each line segment)</returns>
        public Vector3[] OffsetAsLineSegments(float distance, Vector3 normal)
        {
            List<Vector3> linePoints = new List<Vector3>();
            if (closed)
            {
                for (int i = 0; i < vertices.Count; i++)
                {
                    int next = (i + 1) % vertices.Count;
                    Vector3 segment = vertices[next].currentPosition - vertices[i].currentPosition;
                    Vector3 offsetDirection = Vector3.Cross(segment, normal);
                    offsetDirection.Normalize();
                    offsetDirection *= distance;
                    linePoints.Add(vertices[i].currentPosition + offsetDirection);
                    linePoints.Add(vertices[next].currentPosition + offsetDirection);
                }
            }
            else
            {
                for (int i = 0; i < vertices.Count - 1; i++)
                {
                    int next = i + 1;
                    Vector3 segment = vertices[next].currentPosition - vertices[i].currentPosition;
                    Vector3 offsetDirection = Vector3.Cross(segment, normal);
                    offsetDirection.Normalize();
                    offsetDirection *= distance;
                    linePoints.Add(vertices[i].currentPosition + offsetDirection);
                    linePoints.Add(vertices[next].currentPosition + offsetDirection);
                }
            }

            return linePoints.ToArray();
        }

        /// <summary>
        /// Offsets the polygon
        /// </summary>
        /// <param name="distance">Offset distance</param>
        /// <returns>Vector Array (the vertices of the offset)</returns>
        public Vector3[] Offset(float distance)
        {
            List<Vector3> linePoints = new List<Vector3>();
            if (closed)
            {
                for (int i = 0; i < vertices.Count; i++)
                {
                    int next = (i + 1) % vertices.Count;
                    int prev = i - 1;
                    if (i - 1 == -1)
                    {
                        prev = vertices.Count - 1;
                    }
                    Vector3 v1 = vertices[prev].currentPosition - vertices[i].currentPosition;
                    Vector3 v2 = vertices[next].currentPosition - vertices[i].currentPosition;
                    v1.Normalize();
                    v2.Normalize();
                    var cross = Vector3.Cross(v1, v2);
                    float angle = Vector3.Angle(v1, v2);
                    float vectorLength;
                    if (Mathf.Sin(Mathf.Deg2Rad * angle * 0.5f) != 0)
                    {
                        vectorLength = (distance) / Mathf.Sin(Mathf.Deg2Rad * angle * 0.5f);
                    }
                    else
                    {
                        vectorLength = (distance);
                    }
                    var addition = (v1 + v2);
                    addition.Normalize();
                    addition *= vectorLength;
                    if (Vector3.Cross(v1, v2).y < 0)
                    {
                        addition *= -1;
                    }
                    if (isClockWise)
                    {
                        addition *= -1;
                    }

                    if (addition.magnitude == 0)
                    {
                        addition = Vector3.Cross(v1, Vector3.up);
                        addition.Normalize();
                        addition *= -vectorLength;
                    }

                    linePoints.Add(vertices[i].currentPosition + addition);
                }
            }
            else
            {
                for (int i = 0; i < vertices.Count; i++)
                {
                    if (i != 0 && i != vertices.Count - 1)
                    {
                        int next = (i + 1) % vertices.Count;
                        int prev = i - 1;
                        if (i - 1 == -1)
                        {
                            prev = vertices.Count - 1;
                        }
                        Vector3 v1 = vertices[prev].currentPosition - vertices[i].currentPosition;
                        Vector3 v2 = vertices[next].currentPosition - vertices[i].currentPosition;
                        v1.Normalize();
                        v2.Normalize();
                        var cross = Vector3.Cross(v1, v2);
                        float angle = Vector3.Angle(v1, v2);
                        float vectorLength;
                        if (Mathf.Sin(Mathf.Deg2Rad * angle * 0.5f) != 0)
                        {
                            vectorLength = (distance) / Mathf.Sin(Mathf.Deg2Rad * angle * 0.5f);
                        }
                        else
                        {
                            vectorLength = (distance);
                        }
                        var addition = (v1 + v2);
                        addition.Normalize();
                        addition *= vectorLength;
                        if (Vector3.Cross(v1, v2).y < 0)
                        {
                            addition *= -1;
                        }

                        if (addition.magnitude == 0)
                        {
                            addition = Vector3.Cross(v1, Vector3.up);
                            addition.Normalize();
                            addition *= -vectorLength;
                        }

                        linePoints.Add(vertices[i].currentPosition + addition);
                    }
                    else if (i == 0)
                    {
                        linePoints.Add(vertices[i].currentPosition + Vector3.Cross(vertices[i + 1].currentPosition - vertices[i].currentPosition, Vector3.up).normalized * distance);
                    }
                    else if (i == vertices.Count - 1)
                    {
                        linePoints.Add(vertices[i].currentPosition + Vector3.Cross(vertices[i].currentPosition - vertices[i - 1].currentPosition, Vector3.up).normalized * distance);
                    }
                }
            }

            return linePoints.ToArray();
        }



        /// <summary>
        /// Offsets a list of points as if they constituted a Polygon
        /// </summary>
        /// <param name="vertices">The list of points</param>
        /// <param name="closed">Whether they constitue a closed polygon</param>
        /// <param name="distance">The offset distance</param>
        /// <returns>Vector3 Array</returns>
        public static Vector3[] OffsetPoints(Vector3[] vertices, bool closed, float distance)
        {
            List<Vector3> linePoints = new List<Vector3>();
            if (closed)
            {
                for (int i = 0; i < vertices.Length; i++)
                {
                    int next = (i + 1) % vertices.Length;
                    int prev = i - 1;
                    if (i - 1 == -1)
                    {
                        prev = vertices.Length - 1;
                    }

                    if (Vector3.Distance(vertices[prev], vertices[i]) < 0.05f)
                    {
                        prev = prev - 1;
                        if (prev - 1 == -1)
                        {
                            prev = vertices.Length - 1;
                        }
                    }

                    if (Vector3.Distance(vertices[next], vertices[i]) < 0.05f)
                    {
                        next = (next + 1) % vertices.Length;
                    }

                    Vector3 v1 = vertices[prev] - vertices[i];
                    Vector3 v2 = vertices[next] - vertices[i];
                    v1.Normalize();
                    v2.Normalize();
                    var cross = Vector3.Cross(v1, v2);
                    float angle = Vector3.Angle(v1, v2);
                    float vectorLength;
                    if (Mathf.Sin(Mathf.Deg2Rad * angle * 0.5f) != 0)
                    {
                        vectorLength = (distance) / Mathf.Sin(Mathf.Deg2Rad * angle * 0.5f);
                    }
                    else
                    {
                        vectorLength = (distance);
                    }
                    var addition = (v1 + v2);
                    addition.Normalize();
                    addition *= vectorLength;
                    if (Vector3.Cross(v1, v2).y < 0)
                    {
                        addition *= -1;
                    }
                    if (IsClockWise(vertices.ToList()))
                    {
                        addition *= -1;
                    }

                    if (addition.magnitude == 0)
                    {
                        addition = Vector3.Cross(v1, IsClockWise(vertices.ToList()) ? Vector3.down : Vector3.up);
                        addition.Normalize();
                        addition *= -vectorLength;
                    }

                    linePoints.Add(vertices[i] + addition);
                }
            }
            else
            {
                for (int i = 0; i < vertices.Length; i++)
                {
                    if (i != 0 && i != vertices.Length - 1)
                    {
                        int next = (i + 1) % vertices.Length;
                        int prev = i - 1;
                        if (i - 1 == -1)
                        {
                            prev = vertices.Length - 1;
                        }
                        Vector3 v1 = vertices[prev] - vertices[i];
                        Vector3 v2 = vertices[next] - vertices[i];
                        v1.Normalize();
                        v2.Normalize();
                        var cross = Vector3.Cross(v1, v2);
                        float angle = Vector3.Angle(v1, v2);
                        float vectorLength;
                        if (Mathf.Sin(Mathf.Deg2Rad * angle * 0.5f) != 0)
                        {
                            vectorLength = (distance) / Mathf.Sin(Mathf.Deg2Rad * angle * 0.5f);
                        }
                        else
                        {
                            vectorLength = (distance);
                        }
                        var addition = (v1 + v2);
                        addition.Normalize();
                        addition *= vectorLength;
                        if (Vector3.Cross(v1, v2).y < 0)
                        {
                            addition *= -1;
                        }

                        if (addition.magnitude == 0)
                        {
                            addition = Vector3.Cross(v1, Vector3.up);
                            addition.Normalize();
                            addition *= -vectorLength;
                        }

                        linePoints.Add(vertices[i] + addition);
                    }
                    else if (i == 0)
                    {
                        linePoints.Add(vertices[i] + Vector3.Cross(vertices[i + 1] - vertices[i], Vector3.up).normalized * distance);
                    }
                    else if (i == vertices.Length - 1)
                    {
                        linePoints.Add(vertices[i] + Vector3.Cross(vertices[i] - vertices[i - 1], Vector3.up).normalized * distance);
                    }
                }
            }

            return linePoints.ToArray();
        }

        /// <summary>
        /// Returns a point on the curve at a specific length
        /// </summary>
        /// <param name="parameter">The length parameter (0-1)</param>
        /// <returns>The point on the curve</returns>
        public Vector3 PointOnCurve(float parameter)
        {
            Vector3 point = vertices[0].currentPosition;

            float m_par;
            int startPar;
            int endPar;
            if (closed)
            {
                m_par = parameter * (vertices.Count);
                startPar = Mathf.FloorToInt(m_par);
                endPar = Mathf.CeilToInt(m_par) % vertices.Count;
            }
            else
            {
                m_par = parameter * (vertices.Count - 1);
                startPar = Mathf.FloorToInt(m_par);
                endPar = Mathf.CeilToInt(m_par);
            }
            Vector3 start = vertices[startPar].currentPosition;
            Vector3 end = vertices[endPar].currentPosition;

            point = start + (end - start) * (m_par - startPar);

            return point;
        }

        /// <summary>
        /// Returns a point on the curve at a specific length
        /// </summary>
        /// <param name="parameter">The length parameter (0-1)</param>
        /// <param name="tangent">The tangent at that point</param>
        /// <returns>The point on the curve</returns>
        public Vector3 PointOnCurve(float parameter, out Vector3 tangent)
        {
            Vector3 point = vertices[0].currentPosition;

            float m_par;
            int startPar;
            int endPar;
            if (closed)
            {
                m_par = parameter * (vertices.Count);
                startPar = Mathf.FloorToInt(m_par);
                endPar = Mathf.CeilToInt(m_par) % vertices.Count;
            }
            else
            {
                m_par = parameter * (vertices.Count - 1);
                startPar = Mathf.FloorToInt(m_par);
                endPar = Mathf.CeilToInt(m_par);
            }
            Vector3 start = vertices[startPar].currentPosition;
            Vector3 end = vertices[endPar].currentPosition;

            tangent = (end - start).normalized;
            point = start + (end - start) * (m_par - startPar);

            return point;
        }

        public Vector3[] PointsOnCurve(float[] parameters)
        {
            List<Vector3> points = new List<Vector3>();
            if (Count>2)
            {
                Array.Sort(parameters);
                List<float> verticesParams = new List<float>();
                for (int i=1; i<Count-1; i++)
                {
                    float param = 0;
                    PointOnCurve(vertices[i].currentPosition, out param);
                    if (param > parameters[0] && param < parameters.Last())
                    {
                        verticesParams.Add(param);
                    }
                }
                verticesParams.AddRange(parameters);
                verticesParams.Sort();
                for (int i=0; i<verticesParams.Count;i++)
                {
                    points.Add(PointOnCurve(verticesParams[i]));
                }
            }
            else
            {
                for (int i=0; i<parameters.Length; i++)
                {
                    points.Add(PointOnCurve(parameters[i]));
                }
            }

            return points.ToArray();
        }

        /// <summary>
        /// Returns a point on the curve at a specific length
        /// </summary>
        /// <param name="length">The length of the curve (not normalized)</param>
        /// <returns>The point on the curve</returns>
        public Vector3 PointOnCurveLength(float length)
        {
            Vector3 point = vertices[0].currentPosition;

            float m_length = 0.0f;
            int indexLast = 0;
            if (closed)
            {

            }
            else
            {
                for (int i = 0; i < vertices.Count - 1; i++)
                {
                    var l = Vector3.Distance(vertices[i].currentPosition, vertices[i + 1].currentPosition);
                    if (m_length + l < length)
                    {
                        m_length += l;
                        indexLast = i;
                    }
                    else
                    {
                        break;
                    }
                }

                if (indexLast != 0)
                {
                    point = vertices[indexLast].currentPosition + (vertices[indexLast + 1].currentPosition - vertices[indexLast].currentPosition).normalized * (length - m_length);
                }
            }

            return point;
        }

        /// <summary>
        /// Splits the polyline and returns the points of the new polylines
        /// </summary>
        /// <param name="length">The length of the polyline to split at</param>
        /// <param name="normalized">Whether the length is from 0-1 or actual length</param>
        /// <returns>Vector3 Double Array</returns>
        public Vector3[][] Split(float length, bool normalized = false)
        {
            Vector3[][] verts = new Vector3[2][];

            if (normalized)
            {

            }
            else
            {
                if (length < Length())
                {
                    float parameter = 0.0f;
                    float currentLength = 0;
                    float remainder = 0;
                    int prevIndex = -1;

                    float paramSteps = 1.0f / (vertices.Count - 1);

                    for (int i = 0; i < vertices.Count - 1; i++)
                    {
                        float tempLength = Vector3.Distance(vertices[i].currentPosition, vertices[i + 1].currentPosition);
                        if (currentLength + tempLength < length)
                        {
                            currentLength += tempLength;
                        }
                        else
                        {
                            prevIndex = i;
                            remainder = length - currentLength;
                            break;
                        }
                    }

                    parameter = (prevIndex * paramSteps) + (remainder / Vector3.Distance(vertices[prevIndex].currentPosition, vertices[prevIndex + 1].currentPosition)) * paramSteps;

                    Vector3 splitPoint = PointOnCurve(parameter);

                    verts[0] = new Vector3[prevIndex + 2];
                    for (int i = 0; i < verts[0].Length; i++)
                    {
                        verts[0][i] = vertices[i].currentPosition;
                    }
                    verts[0][verts[0].Length - 1] = splitPoint;

                    verts[1] = new Vector3[vertices.Count - prevIndex];
                    verts[1][0] = splitPoint;
                    for (int i = prevIndex; i < vertices.Count - 1; i++)
                    {
                        var index = i - prevIndex + 1;
                        verts[1][index] = vertices[i + 1].currentPosition;
                    }

                }
            }

            return verts;
        }

        /// <summary>
        /// Converts from curve length to curve parameter (0-1)
        /// </summary>
        /// <param name="length">The length of the curve</param>
        /// <returns>Float</returns>
        public float LengthToParameter(float length)
        {
            if (length < Length())
            {
                float parameter = 0.0f;
                float currentLength = 0;
                float remainder = 0;
                int prevIndex = -1;

                float paramSteps = 1.0f / (vertices.Count - 1);

                for (int i = 0; i < vertices.Count - 1; i++)
                {
                    float tempLength = Vector3.Distance(vertices[i].currentPosition, vertices[i + 1].currentPosition);
                    if (currentLength + tempLength < length)
                    {
                        currentLength += tempLength;
                    }
                    else
                    {
                        prevIndex = i;
                        remainder = length - currentLength;
                        break;
                    }
                }

                parameter = (prevIndex * paramSteps) + (remainder / Vector3.Distance(vertices[prevIndex].currentPosition, vertices[prevIndex + 1].currentPosition)) * paramSteps;

                return parameter;
            }
            else return 1.0f;
        }

        /// <summary>
        /// Converts from curve parameter (0-1) to curve length
        /// </summary>
        /// <param name="parameter">The parameter of the curve</param>
        /// <returns>Float</returns>
        public float ParameterToLength(float parameter)
        {
            float length = 0;
            float remainder = 0;

            float paramSteps = 1.0f / (vertices.Count - 1);

            int prevIndex = Mathf.FloorToInt(parameter / paramSteps);
            remainder = parameter % paramSteps;

            for (int i = 0; i < prevIndex; i++)
            {
                length += Vector3.Distance(vertices[i].currentPosition, vertices[i + 1].currentPosition);
            }

            length += Vector3.Distance(vertices[prevIndex].currentPosition, vertices[prevIndex + 1].currentPosition) * (remainder / paramSteps);

            return length;
        }

        //Based on LineLIneIntersection from http://wiki.unity3d.com/index.php/3d_Math_functions
        /// <summary>
        /// Return the point of intersection between two lines (only for polygons with 2 vertices)
        /// </summary>
        /// <param name="other">The polygon to check against</param>
        /// <param name="intersectionPoint">the point of intersection</param>
        /// <param name="extend">Whether the lines should be extended to their intersection points</param>
        /// <returns>Whether the lines intersect or not</returns>
        public bool Intersects(Polygon other, out Vector3 intersectionPoint, bool extend = false)
        {
            // check in first direction
            Vector3 start1 = vertices[0].currentPosition;
            Vector3 dir1 = vertices[1].currentPosition - vertices[0].currentPosition;
            Vector3 start2 = other.vertices[0].currentPosition;
            Vector3 dir2 = other.vertices[1].currentPosition - other.vertices[0].currentPosition;

            if (BrydenWoodUtils.RayRayIntersection(start1, dir1, start2, dir2, out intersectionPoint))
            {
                if (extend)
                {
                    ExtendPolygons(other, intersectionPoint);
                }
                return true;
            }
            else
            {
                return false;
            }

        }

        /// <summary>
        /// Returns the total length of the curve
        /// </summary>
        /// <returns>Float</returns>
        public float Length()
        {
            float length = 0;
            if (closed)
            {
                for (int i = 0; i < vertices.Count; i++)
                {
                    length += (Vector3.Distance(vertices[i].currentPosition, vertices[(i + 1) % vertices.Count].currentPosition));
                }
            }
            else
            {
                for (int i = 0; i < vertices.Count - 1; i++)
                {
                    length += (Vector3.Distance(vertices[i].currentPosition, vertices[(i + 1)].currentPosition));
                }
            }
            return (float)(Math.Round(length, 2));
        }

        /// <summary>
        /// Displays the distance between the last point of the curve and a given point
        /// </summary>
        /// <param name="end">The given point</param>
        public void DisplayDistance(Vector3 end)
        {
            if (currentDistance != null)
            {
                Vector3 start = currentDistance.transform.GetChild(1).GetComponent<LineRenderer>().GetPosition(0);
                float dist = Vector3.Distance(start, end);

                TextMesh mText = currentDistance.transform.GetChild(0).GetComponent<TextMesh>();
                mText.text = dist.ToString("0.00");
                currentDistance.transform.GetChild(0).position = start + (end - start) / 2 + Vector3.Cross((end - start), Vector3.up).normalized * distanceOffset;

                if (useCanvas)
                {
                    distancesUI.Last().GetComponent<RectTransform>().position = Camera.main.WorldToScreenPoint(currentDistance.transform.GetChild(0).position);
                    distancesUI.Last().text = dist.ToString("0.00");
                }

                float angle = Vector3.Angle(Vector3.right, end - start);
                if (Vector3.Cross((end - start), Vector3.right).y < 0)
                {
                    angle = -angle;
                }
                currentDistance.transform.GetChild(0).eulerAngles = new Vector3(90, -angle, 0);

                LineRenderer l1 = currentDistance.transform.GetChild(1).GetComponent<LineRenderer>();                
                //l1.sortingOrder = -1;
                l1.SetPosition(1, end);
            }
        }

        /// <summary>
        /// Updates the distance between two given points
        /// </summary>
        /// <param name="start">The first point</param>
        /// <param name="end">The second point</param>
        /// <param name="distance">The gameObject of the distance</param>
        /// <param name="index">The index of the distance</param>
        public void UpdateDistance(Vector3 start, Vector3 end, GameObject distance, int index)
        {
            float dist = Vector3.Distance(start, end);

            TextMesh mText = distance.transform.GetChild(0).GetComponent<TextMesh>();
            mText.text = dist.ToString("0.00");
            distance.transform.GetChild(0).position = start + (end - start) / 2 + Vector3.Cross((end - start), Vector3.up).normalized * distanceOffset;

            if (useCanvas)
            {
                distancesUI[index].GetComponent<RectTransform>().position = Camera.main.WorldToScreenPoint(distance.transform.GetChild(0).position);
                distancesUI[index].text = dist.ToString("0.00");
            }

            float angle = Vector3.Angle(Vector3.right, end - start);
            if (Vector3.Cross((end - start), Vector3.right).y < 0)
            {
                angle = -angle;
            }
            distance.transform.GetChild(0).eulerAngles = new Vector3(90, -angle, 0);

            LineRenderer l1 = distance.transform.GetChild(1).GetComponent<LineRenderer>();           
            l1.SetPosition(0, start);
            l1.SetPosition(1, end);
        }

        /// <summary>
        /// Sets the height of the extrusion
        /// </summary>
        /// <param name="height">New height</param>
        public void SetExtrusionHeight(float height)
        {
            extrusion.totalHeight = height;
            extrusion.GenerateMeshes();
        }

        /// <summary>
        /// Sets the start of the displayed distances
        /// </summary>
        /// <param name="start">The first point</param>
        public void StartDistance(Vector3 start)
        {
            if (distancesMaterial == null)
            {
                distancesMaterial = new Material(Shader.Find("Standard"));
                distancesMaterial.color = new Color(0.93f, 0.15f, 0.43f);
                distancesMaterial.SetColor("_EmissionColor", new Color(0.93f, 0.15f, 0.43f));
                distancesMaterial.EnableKeyword("_EMISSION");
            }
            if (distances == null)
            {
                distances = new List<GameObject>();
            }
            if (distancesParent == null)
            {
                distancesParent = new GameObject("DistancesParent").transform;
                distancesParent.SetParent(transform);
            }
            if (distancesUI == null)
            {
                distancesUI = new List<Text>();
            }

            distanceUIParent = GameObject.Find("DistancesUIParent").transform;
            if (distanceUIParent == null)
            {
                distanceUIParent = new GameObject("DistancesUIParent").transform;
                distanceUIParent.SetParent(GameObject.Find("Canvas").transform);
                distanceUIParent.SetAsFirstSibling();
                distanceUIParent.localScale = Vector3.one;
            }

            currentDistance = new GameObject("Distance_" + distances.Count);
            distances.Add(currentDistance);
            currentDistance.transform.SetParent(distancesParent);

            GameObject text = Instantiate(Resources.Load("Geometry/TextMesh") as GameObject, currentDistance.transform);
            text.transform.Rotate(text.transform.right, 90);
            TextMesh mText = text.GetComponent<TextMesh>();
            mText.characterSize = 1;

            mText.color = distancesMaterial.color;
            mText.anchor = TextAnchor.LowerCenter;
            mText.alignment = TextAlignment.Center;
            if (useCanvas)
            {
                mText.GetComponent<MeshRenderer>().enabled = false;
                GameObject distUI = Instantiate(Resources.Load("GUI/DistanceText") as GameObject, distanceUIParent);
                distancesUI.Add(distUI.GetComponent<Text>());
                distUI.GetComponent<Text>().color = distancesMaterial.color;
                distUI.GetComponent<UiRepresentation>().Initialize(text.transform);
            }

            GameObject line1 = new GameObject("Line1");
            line1.transform.SetParent(currentDistance.transform);
            LineRenderer l1 = line1.AddComponent<LineRenderer>();
            l1.material = distancesMaterial;
            l1.useWorldSpace = true;
            l1.positionCount = 2;
            l1.startWidth = lineWidth;
            l1.endWidth = lineWidth;
            l1.SetPosition(0, start);
            l1.SetPosition(1, start);
        }

        /// <summary>
        /// Finished the set up of the displayed distances
        /// </summary>
        public void CloseDistance()
        {
            currentDistance = null;
        }

        /// <summary>
        /// Toggles the display of the distances
        /// </summary>
        /// <param name="show">Toggle</param>
        public void ToggleDistances(bool show)
        {
            if (distances != null && distances.Count > 0)
            {
                for (int i = 0; i < distances.Count; i++)
                {
                    distances[i].SetActive(show);
                }
            }
            else
            {
                if (show)
                {
                    showDistances = show;
                    distances = new List<GameObject>();
                    for (int i = 0; i < vertices.Count - 1; i++)
                    {
                        StartDistance(vertices[i].currentPosition);
                        DisplayDistance(vertices[i + 1].currentPosition);
                        CloseDistance();
                    }
                }
            }
        }

        /// <summary>
        /// Recalculate the displayed distances
        /// </summary>
        public void RecalculateDistances()
        {
            DiscardAllDistances();
            distances = new List<GameObject>();
            for (int i = 0; i < vertices.Count - 1; i++)
            {
                StartDistance(vertices[i].currentPosition);
                DisplayDistance(vertices[i + 1].currentPosition);
                CloseDistance();
            }
        }

        /// <summary>
        /// Discards the last added distance
        /// </summary>
        public void DiscardLastDistance()
        {
            distances.Remove(currentDistance);
            Destroy(currentDistance.gameObject);
            Destroy(distancesUI.Last().gameObject);
            distancesUI.RemoveAt(distancesUI.Count - 1);
            currentDistance = null;
        }

        /// <summary>
        /// Undoes the last added distance
        /// </summary>
        public void UndoDistance()
        {
            DiscardLastDistance();
            currentDistance = distances.Last();
            //StartDistance(vertices.Last().currentPosition);
        }

        /// <summary>
        /// Discards all distances
        /// </summary>
        public void DiscardAllDistances()
        {
            for (int i = 0; i < distances.Count; i++)
            {
                Destroy(distances[i]);
                Destroy(distancesUI[i].gameObject);
            }
            currentDistance = null;
            distances.Clear();
            distancesUI.Clear();
        }

        /// <summary>
        /// Returns a list with the angles of all corners of the polygon (degrees)
        /// </summary>
        /// <returns>Float Array</returns>
        public float[] GetCornersAngles()
        {
            List<float> corners = new List<float>();

            if (closed)
            {
                for (int i = 1; i <= vertices.Count; i++)
                {
                    int cur = i % vertices.Count;
                    int next = (cur + 1) % vertices.Count;
                    int prev = cur - 1;
                    if (prev < 0)
                    {
                        prev = vertices.Count - 1;
                    }
                    var side1 = vertices[cur].currentPosition - vertices[prev].currentPosition;
                    var side2 = vertices[cur].currentPosition - vertices[next].currentPosition;
                    float angle = Vector3.Angle(side1, side2);
                    corners.Add(angle);
                }
            }
            else
            {
                for (int i = 1; i < vertices.Count - 1; i++)
                {
                    int next = i + 1;
                    var side1 = vertices[i].currentPosition - vertices[i - 1].currentPosition;
                    var side2 = vertices[i].currentPosition - vertices[next].currentPosition;
                    float angle = Vector3.Angle(side1, side2);
                    corners.Add(angle);
                }
            }

            return corners.ToArray();
        }

        /// <summary>
        /// Checks inclusion of a point
        /// </summary>
        /// <param name="point">The point to check</param>
        /// <returns>Boolean</returns>
        public bool IsIncluded(Vector3 point)
        {
            Vector3 infinity = point + new Ray(point, Vector3.right).GetPoint(100000);
            int counter = 0;
            for (int i = 0; i < vertices.Count; i++)
            {
                int next = (i + 1) % vertices.Count;
                Vector3 intersection;
                if (BrydenWoodUtils.LineLineIntersection(point, infinity, vertices[i].currentPosition, vertices[next].currentPosition, out intersection))
                {
                    counter++;
                }
            }

            if (counter % 2 == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Check whether a point is included in a polygon constituted from a list of points
        /// </summary>
        /// <param name="points">The list of points</param>
        /// <param name="point">The point</param>
        /// <returns>Boolean</returns>
        public static bool IsIncluded(Vector3[] points, Vector3 point)
        {
            Vector3 infinity = point + new Ray(point, Vector3.right).GetPoint(100000);
            int counter = 0;
            for (int i = 0; i < points.Length; i++)
            {
                int next = (i + 1) % points.Length;
                Vector3 intersection;
                if (BrydenWoodUtils.LineLineIntersection(point, infinity, points[i], points[next], out intersection))
                {
                    counter++;
                }
            }

            if (counter % 2 == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        

        /// <summary>
        /// Projects a point onto the curve
        /// </summary>
        /// <param name="point">The point to project</param>
        /// <param name="curveParameter">Key: the index of the segment, Value: the parameter of that segment</param>
        /// <param name="snapDist">The distance within which the projected point will snap to its closest vertex</param>
        /// <returns>The point on the curve</returns>
        public Vector3 PointOnCurve(Vector3 point, out KeyValuePair<int, float> curveParameter, float snapDist = 0.0f)
        {
            Vector3 projection = vertices[0].currentPosition;
            curveParameter = new KeyValuePair<int, float>();
            List<float> distances = new List<float>();
            List<float> parameters = new List<float>();
            List<int> indices = new List<int>();
            List<Vector3> projections = new List<Vector3>();
            float minDist = float.MaxValue;

            for (int i = 0; i < vertices.Count - 1; i++)
            {
                int indexNext = (i + 1);
                float param;
                var proj = BrydenWoodUtils.ProjectOnCurve(vertices[i].currentPosition, vertices[indexNext].currentPosition, point, out param, snapDist, true);

                var dist = Vector3.Distance(point, proj);

                if (dist < minDist)
                {
                    minDist = dist;
                    projections.Insert(0, proj);
                    distances.Insert(0, Vector3.Distance(vertices[i].currentPosition, proj));
                    parameters.Insert(0, param);
                    indices.Insert(0, i);
                }
                else
                {
                    projections.Add(proj);
                    distances.Add(Vector3.Distance(vertices[i].currentPosition, proj));
                    parameters.Add(param);
                    indices.Add(i);
                }
            }

            float length = 0.0f;
            for (int i = 0; i < indices[0]; i++)
            {
                length += Vector3.Distance(vertices[i].currentPosition, vertices[i + 1].currentPosition);
            }
            length += distances[0];
            length = (float)Math.Round(length, 2);

            float tempLength = 0;
            int indexLast = 0;
            float parStep = 1.0f / (vertices.Count - 1);

            for (int i = 0; i < vertices.Count - 1; i++)
            {
                var l = Vector3.Distance(vertices[i].currentPosition, vertices[i + 1].currentPosition);
                l = (float)Math.Round(l, 2);
                indexLast = i;
                if (Mathf.Abs((tempLength + l) - length) > 0.1f)
                {
                    tempLength += l;
                }
                else
                {

                    break;
                }
            }

            tempLength = (float)Math.Round(tempLength, 2);

            float remainder = length - tempLength;
            float lastLength = Vector3.Distance(vertices[indexLast].currentPosition, vertices[indexLast + 1].currentPosition);
            float remainderPar = (remainder / lastLength);
            curveParameter = new KeyValuePair<int, float>(indices[0], parameters[0]);

            projection = projections[0];

            return projection;
        }

        /// <summary>
        /// Projects a point onto the curve
        /// </summary>
        /// <param name="point">The point to project</param>
        /// <param name="curveParameter">The parameter (0-1) of the whole curve of the projected point</param>
        /// <param name="snapDist">The distance within which the projected point will snap to its closest vertex</param>
        /// <returns>The point on the curve</returns>
        public Vector3 PointOnCurve(Vector3 point, out float curveParameter, float snapDist = 0.0f)
        {
            Vector3 projection = vertices[0].currentPosition;
            curveParameter = 0.0f;
            List<float> distances = new List<float>();
            List<int> indices = new List<int>();
            List<Vector3> projections = new List<Vector3>();
            float minDist = float.MaxValue;

            if (closed)
            {
                for (int i = 0; i < vertices.Count; i++)
                {
                    int indexNext = (i + 1) % vertices.Count;
                    var proj = BrydenWoodUtils.ProjectOnCurve(vertices[i].currentPosition, vertices[indexNext].currentPosition, point, snapDist, true);
                    var dist = Vector3.Distance(point, proj);

                    if (dist < minDist)
                    {
                        minDist = dist;
                        projections.Insert(0, proj);
                        distances.Insert(0, Vector3.Distance(vertices[i].currentPosition, proj));
                        indices.Insert(0, i);
                    }
                    else
                    {
                        projections.Add(proj);
                        distances.Add(Vector3.Distance(vertices[i].currentPosition, proj));
                        indices.Add(i);
                    }
                }
            }
            else
            {
                for (int i = 0; i < vertices.Count - 1; i++)
                {
                    int indexNext = (i + 1);
                    var proj = BrydenWoodUtils.ProjectOnCurve(vertices[i].currentPosition, vertices[indexNext].currentPosition, point, snapDist, true);
                    var dist = Vector3.Distance(point, proj);

                    if (dist < minDist)
                    {
                        minDist = dist;
                        projections.Insert(0, proj);
                        distances.Insert(0, Vector3.Distance(vertices[i].currentPosition, proj));
                        indices.Insert(0, i);
                    }
                    else
                    {
                        projections.Add(proj);
                        distances.Add(Vector3.Distance(vertices[i].currentPosition, proj));
                        indices.Add(i);
                    }
                }
            }

            float length = 0.0f;
            for (int i = 0; i < indices[0]; i++)
            {
                length += Vector3.Distance(vertices[i].currentPosition, vertices[i + 1].currentPosition);
            }
            length += distances[0];
            length = (float)Math.Round(length, 2);

            float tempLength = 0;
            int indexLast = 0;
            float parStep = 1.0f / (vertices.Count - 1);

            if (closed)
            {
                for (int i = 0; i < vertices.Count; i++)
                {
                    int indexNext = (i + 1) % vertices.Count;
                    var l = Vector3.Distance(vertices[i].currentPosition, vertices[indexNext].currentPosition);
                    l = (float)Math.Round(l, 2);
                    indexLast = i;
                    if (tempLength + l < length)
                    {
                        tempLength += l;
                    }
                    else
                    {

                        break;
                    }
                }

                tempLength = (float)Math.Round(tempLength, 2);

                float remainder = length - tempLength;
                float lastLength = Vector3.Distance(vertices[indexLast].currentPosition, vertices[(indexLast + 1) % vertices.Count].currentPosition);
                float remainderPar = (remainder / lastLength);
                curveParameter = indexLast * parStep + remainderPar;
            }
            else
            {
                curveParameter = LengthToParameter(length);
            }
            projection = projections[0];

            return projection;
        }

        /// <summary>
        /// Projects a point onto the curve
        /// </summary>
        /// <param name="point">The point to project</param>
        /// <param name="curveParameter">The parameter (0-1) of the whole curve of the projected point</param>
        /// <param name="distance">The distance between the projected point and the initial one</param>
        /// <param name="snapDist">The distance within which the projected point will snap to its closest vertex</param>
        /// <returns>The point on the curve</returns>
        public Vector3 PointOnCurve(Vector3 point, out float curveParameter, out float distance, float snapDist = 0.0f)
        {
            Vector3 projection = vertices[0].currentPosition;
            curveParameter = 0.0f;
            List<float> distances = new List<float>();
            List<int> indices = new List<int>();
            List<Vector3> projections = new List<Vector3>();
            float minDist = float.MaxValue;

            if (closed)
            {
                for (int i = 0; i < vertices.Count; i++)
                {
                    int indexNext = (i + 1) % vertices.Count;
                    var proj = BrydenWoodUtils.ProjectOnCurve(vertices[i].currentPosition, vertices[indexNext].currentPosition, point, snapDist, true);
                    var dist = Vector3.Distance(point, proj);

                    if (dist < minDist)
                    {
                        minDist = dist;
                        projections.Insert(0, proj);
                        distances.Insert(0, Vector3.Distance(vertices[i].currentPosition, proj));
                        indices.Insert(0, i);
                    }
                    else
                    {
                        projections.Add(proj);
                        distances.Add(Vector3.Distance(vertices[i].currentPosition, proj));
                        indices.Add(i);
                    }
                }
            }
            else
            {
                for (int i = 0; i < vertices.Count - 1; i++)
                {
                    int indexNext = (i + 1);
                    var proj = BrydenWoodUtils.ProjectOnCurve(vertices[i].currentPosition, vertices[indexNext].currentPosition, point, snapDist, true);
                    var dist = Vector3.Distance(point, proj);

                    if (dist < minDist)
                    {
                        minDist = dist;
                        projections.Insert(0, proj);
                        distances.Insert(0, Vector3.Distance(vertices[i].currentPosition, proj));
                        indices.Insert(0, i);
                    }
                    else
                    {
                        projections.Add(proj);
                        distances.Add(Vector3.Distance(vertices[i].currentPosition, proj));
                        indices.Add(i);
                    }
                }
            }

            float length = 0.0f;
            for (int i = 0; i < indices[0]; i++)
            {
                length += Vector3.Distance(vertices[i].currentPosition, vertices[i + 1].currentPosition);
            }
            length += distances[0];
            length = (float)Math.Round(length, 2);

            float tempLength = 0;
            int indexLast = 0;
            float parStep = 1.0f / (vertices.Count - 1);

            if (closed)
            {
                for (int i = 0; i < vertices.Count; i++)
                {
                    int indexNext = (i + 1) % vertices.Count;
                    var l = Vector3.Distance(vertices[i].currentPosition, vertices[indexNext].currentPosition);
                    l = (float)Math.Round(l, 2);
                    indexLast = i;
                    if (tempLength + l < length)
                    {
                        tempLength += l;
                    }
                    else
                    {

                        break;
                    }
                }

                tempLength = (float)Math.Round(tempLength, 2);

                float remainder = length - tempLength;
                float lastLength = Vector3.Distance(vertices[indexLast].currentPosition, vertices[(indexLast + 1) % vertices.Count].currentPosition);
                float remainderPar = (remainder / lastLength);
                curveParameter = indexLast * parStep + remainderPar;
            }
            else
            {
                curveParameter = LengthToParameter(length);
            }
            projection = projections[0];
            distance = Vector3.Distance(projection, point);
            return projection;
        }

        public Vector3 PointOnCurve(Vector3 pt, out Vector3 tangent)
        {
            tangent = Vector3.zero;
            Vector3 point = new Vector3();
            float minDist = float.MaxValue;

            if (closed)
            {
                for (int i = 0; i < vertices.Count; i++)
                {
                    Vector3 delta = vertices[(i + 1) % vertices.Count].currentPosition - vertices[i].currentPosition;
                    Vector3 connA = pt - vertices[i].currentPosition;
                    Vector3 projA = Vector3.Project(connA, delta);
                    Vector3 connB = pt - vertices[(i + 1) % vertices.Count].currentPosition;
                    Vector3 projB = Vector3.Project(connB, delta);
                    Vector3 projection = Vector3.zero;
                    if (projA.magnitude <= delta.magnitude && projB.magnitude <= delta.magnitude) //The projected point is between the two points of the line
                    {
                        projection = vertices[i].currentPosition + projA;
                    }
                    else if (projA.magnitude > delta.magnitude) //The projected point is on B side
                    {
                        projection = vertices[(i + 1) % vertices.Count].currentPosition;
                    }
                    else if (projB.magnitude > delta.magnitude) //The projected point is on A side
                    {
                        projection = vertices[i].currentPosition;
                    }

                    if (Vector3.Distance(projection, pt) < minDist)
                    {
                        point = projection;
                        minDist = Vector3.Distance(projection, pt);
                        tangent = delta.normalized;
                    }
                }
            }
            else
            {
                for (int i = 0; i < vertices.Count - 1; i++)
                {
                    Vector3 delta = vertices[i + 1].currentPosition - vertices[i].currentPosition;
                    Vector3 connA = pt - vertices[i].currentPosition;
                    Vector3 projA = Vector3.Project(connA, delta);
                    Vector3 connB = pt - vertices[i + 1].currentPosition;
                    Vector3 projB = Vector3.Project(connB, delta);
                    Vector3 projection = Vector3.zero;
                    if (projA.magnitude <= delta.magnitude && projB.magnitude <= delta.magnitude) //The projected point is between the two points of the line
                    {
                        projection = vertices[i].currentPosition + projA;
                    }
                    else if (projA.magnitude > delta.magnitude) //The projected point is on B side
                    {
                        projection = vertices[i + 1].currentPosition;
                    }
                    else if (projB.magnitude > delta.magnitude) //The projected point is on A side
                    {
                        projection = vertices[i].currentPosition;
                    }

                    if (Vector3.Distance(projection, pt) < minDist)
                    {
                        point = projection;
                        minDist = Vector3.Distance(projection, pt);
                        tangent = delta.normalized;
                    }
                }
            }


            return point;
        }

        public Vector3 PointOnCurve(Vector3 pt)
        {
            Vector3 point = new Vector3();
            float minDist = float.MaxValue;

            if (closed)
            {
                for (int i = 0; i < vertices.Count; i++)
                {
                    Vector3 delta = vertices[(i + 1) % vertices.Count].currentPosition - vertices[i].currentPosition;
                    Vector3 connA = pt - vertices[i].currentPosition;
                    Vector3 projA = Vector3.Project(connA, delta);
                    Vector3 connB = pt - vertices[(i + 1) % vertices.Count].currentPosition;
                    Vector3 projB = Vector3.Project(connB, delta);
                    Vector3 projection = Vector3.zero;
                    if (projA.magnitude <= delta.magnitude && projB.magnitude <= delta.magnitude) //The projected point is between the two points of the line
                    {
                        projection = vertices[i].currentPosition + projA;
                    }
                    else if (projA.magnitude > delta.magnitude) //The projected point is on B side
                    {
                        projection = vertices[(i + 1) % vertices.Count].currentPosition;
                    }
                    else if (projB.magnitude > delta.magnitude) //The projected point is on A side
                    {
                        projection = vertices[i].currentPosition;
                    }

                    if (Vector3.Distance(projection, pt) < minDist)
                    {
                        point = projection;
                        minDist = Vector3.Distance(projection, pt);
                    }
                }
            }
            else
            {
                for (int i = 0; i < vertices.Count - 1; i++)
                {
                    Vector3 delta = vertices[i + 1].currentPosition - vertices[i].currentPosition;
                    Vector3 connA = pt - vertices[i].currentPosition;
                    Vector3 projA = Vector3.Project(connA, delta);
                    Vector3 connB = pt - vertices[i + 1].currentPosition;
                    Vector3 projB = Vector3.Project(connB, delta);
                    Vector3 projection = Vector3.zero;
                    if (projA.magnitude <= delta.magnitude && projB.magnitude <= delta.magnitude) //The projected point is between the two points of the line
                    {
                        projection = vertices[i].currentPosition + projA;
                    }
                    else if (projA.magnitude > delta.magnitude) //The projected point is on B side
                    {
                        projection = vertices[i + 1].currentPosition;
                    }
                    else if (projB.magnitude > delta.magnitude) //The projected point is on A side
                    {
                        projection = vertices[i].currentPosition;
                    }

                    if (Vector3.Distance(projection, pt) < minDist)
                    {
                        point = projection;
                        minDist = Vector3.Distance(projection, pt);
                    }
                }
            }


            return point;
        }

        /// <summary>
        /// Updates the positions of the vertices
        /// </summary>
        public void UpdateVertexPositions()
        {
            for (int i = 0; i < distances.Count; i++)
            {
                int next = (i + 1) % vertices.Count;
                UpdateDistance(vertices[i].currentPosition, vertices[next].currentPosition, distances[i], i);
            }
            for (int i = 0; i < vertices.Count; i++)
            {
                vertices[i].UpdatePosition();
            }
        }
        /// <summary>
        /// Creates a copy of this polygon GameObject and its control points
        /// </summary>
        /// <param name="interdependent">Whether the control points of the copy should be controlled by the control points of the original (default = false)</param>
        /// <param name="parent">The parent for the copy polygon (default = null)</param>
        /// <returns>Polygon</returns>
        public Polygon Copy(bool interdependent = false, Transform parent = null)
        {
            return Copy(Vector3.zero, interdependent, 0, parent);
        }
        /// <summary>
        /// Creates a copy of this polygon GameObject and its control points
        /// </summary>
        /// <param name="move">The vector along which the copy will be moved</param>
        /// <param name="interdependent">Whether the control points of the copy should be controlled by the control points of the original (default = false)</param>
        /// <param name="parent">The parent for the copy polygon (default = null)</param>
        /// <returns>Polygon</returns>
        public Polygon Copy(Vector3 move, bool interdependent = false, float offset = 0, Transform parent = null)
        {
            Polygon copy = Instantiate(gameObject, parent).GetComponent<Polygon>();
            for (int i = 0; i < copy.distancesParent.childCount; i++)
            {
                Destroy(copy.distancesParent.GetChild(i).gameObject);
            }
            copy.distances = null;
            copy.distancesUI = null;
            copy.showDistances = false;
            copy.transform.position = transform.position + move;
            List<PolygonVertex> vertices = new List<PolygonVertex>();
            var pts = Offset(offset);
            for (int i = 0; i < Count; i++)
            {
                this[i].gameObject.name = "Original_" + i;
                var pt = Instantiate(this[i].gameObject, copy.transform).GetComponent<PolygonVertex>();
                pt.transform.position = pts[i] + move;
                pt.gameObject.name = "Copy_" + i;
                pt.Initialize(i);
                vertices.Add(pt);
                if (interdependent)
                {
                    pt.Follow(this[i]);
                }
            }
            copy.Initialize(vertices, true, true);
            return copy;
        }

        public BT.Line ToThreeJs()
        {
            BT.Line obj = new BT.Line();
            obj.userData.Add("type", "FloorOutline");
            BT.Geometry geom = new BT.Geometry();
            geom.data.vertices = new double[(vertices.Count + 1) * 3];
            for (int i = 0; i < vertices.Count; i++)
            {
                geom.data.vertices[i * 3] = vertices[i].currentPosition.x;
                geom.data.vertices[i * 3 + 1] = vertices[i].currentPosition.y;
                geom.data.vertices[i * 3 + 2] = vertices[i].currentPosition.z;
            }
            geom.data.vertices[vertices.Count * 3] = vertices[0].currentPosition.x;
            geom.data.vertices[vertices.Count * 3 + 1] = vertices[0].currentPosition.y;
            geom.data.vertices[vertices.Count * 3 + 2] = vertices[0].currentPosition.z;
            obj._geometry = geom;
            obj.geometry = geom.uuid;
            return obj;
        }


       


        #endregion

        #region Private Methods
        private void ExtendPolygons(Polygon other, Vector3 intersectionPoint)
        {
            float minDist = float.MaxValue;
            int index = -1;
            for (int i = 0; i < vertices.Count; i++)
            {
                if (Vector3.Distance(vertices[i].currentPosition, intersectionPoint) < minDist)
                {
                    minDist = Vector3.Distance(vertices[i].currentPosition, intersectionPoint);
                    index = i;
                }
            }
            vertices[index].UpdatePosition(intersectionPoint);

            minDist = float.MaxValue;
            for (int i = 0; i < other.vertices.Count; i++)
            {
                if (Vector3.Distance(other.vertices[i].currentPosition, intersectionPoint) < minDist)
                {
                    minDist = Vector3.Distance(other.vertices[i].currentPosition, intersectionPoint);
                    index = i;
                }
            }
            other.vertices[index].UpdatePosition(intersectionPoint);
        }
        private void DisplayMoveUI()
        {
            GameObject m_parent = GameObject.Find("Canvas");
            movePoint = Instantiate(moveUiElement, m_parent.transform).GetComponent<ControlPointUI>();
            movePoint.Initialize(transform);
            movePoint.moved += OnMoved;
            movePoint.stoppedMoving += OnStoppedMoving;
        }

        private void HideMoveUI()
        {
            if (movePoint != null)
            {
                movePoint.moved -= OnMoved;
                movePoint.stoppedMoving -= OnStoppedMoving;
                Destroy(movePoint.gameObject);
                movePoint = null;
            }
        }

        private void DisplayEdgesUI()
        {
            if (edges == null)
            {
                edges = new Dictionary<ControlPointUI, EdgeRelation>();
                edgeControlPoints = new List<GameObject>();
                for (int i = 0; i < vertices.Count; i++)
                {
                    int next = (i + 1) % vertices.Count;
                    GameObject m_parent = GameObject.Find("EdgeControlPoints");
                    var m_edgePoint = Instantiate(edgeUiElement, m_parent.transform).GetComponent<ControlPointUI>();
                    edgeControlPoints.Add(Instantiate(controlPoint));
                    edgeControlPoints[i].GetComponent<PolygonVertex>().SetGUIElement(m_edgePoint);
                    edgeControlPoints[i].transform.position = vertices[i].currentPosition + (vertices[next].currentPosition - vertices[i].currentPosition) / 2.0f;
                    m_edgePoint.Initialize(edgeControlPoints[i].transform);
                    m_edgePoint.axisConstrain = true;
                    m_edgePoint.axis = Vector3.Cross(Vector3.up, vertices[next].currentPosition - vertices[i].currentPosition).normalized;
                    m_edgePoint.moved += OnEdgeMoved;
                    m_edgePoint.onSelected += OnEdgeCPUISelected;
                    if (!edges.ContainsKey(m_edgePoint))
                    {
                        EdgeRelation m_relation = new EdgeRelation();
                        m_relation.vertices = new List<PolygonVertex>() { vertices[i], vertices[next] };
                        m_relation.originalDifs = new List<Vector3>();
                        m_relation.originalDifs.Add(vertices[i].currentPosition - edgeControlPoints[i].transform.position);
                        m_relation.originalDifs.Add(vertices[next].currentPosition - edgeControlPoints[i].transform.position);

                        if (moveColinear)
                        {
                            List<PolygonVertex> verts = null;
                            if (isBuildingVertex)
                            {
                                //verts = geometryHandler.buildingPolygonVertices;
                            }
                            else
                            {
                                //verts = geometryHandler.apartmentLayoutPolygonVertices;
                            }

                            if (verts != null)
                            {
                                for (int j = 0; j < verts.Count; j++)
                                {
                                    if (verts[j].gameObject.activeSelf && verts[j] != vertices[i] && verts[j] != vertices[next])
                                    {
                                        PolygonVertex vertex = verts[j];
                                        var projection = BrydenWoodUtils.ProjectOnCurve(vertices[i].currentPosition, vertices[next].currentPosition, vertex.currentPosition);
                                        if (Vector3.Distance(projection, vertex.currentPosition) < 0.3)
                                        {
                                            m_relation.vertices.Add(vertex);
                                            m_relation.originalDifs.Add(vertex.currentPosition - edgeControlPoints[i].transform.position);
                                        }
                                    }
                                }
                            }
                        }

                        edges.Add(m_edgePoint, m_relation);
                    }
                }
            }
        }

        private void OnEdgeCPUISelected(ControlPointUI cpui)
        {
            edgeControlPointSelected = cpui;
            EdgeRelation m_relation;
            if (edges.TryGetValue(cpui, out m_relation))
            {
                cpui.axis = Vector3.Cross(Vector3.up, m_relation.vertices[1].currentPosition - m_relation.vertices[0].currentPosition).normalized;
                m_relation.originalDifs = new List<Vector3>();
                if (moveColinear)
                {
                    for (int i = 0; i < m_relation.vertices.Count; i++)
                    {
                        m_relation.originalDifs.Add(m_relation.vertices[i].transform.position - cpui.sceneObjects[0].transform.position);
                    }
                }
                else
                {
                    for (int i = 0; i < 2; i++)
                    {
                        m_relation.originalDifs.Add(m_relation.vertices[i].transform.position - cpui.sceneObjects[0].transform.position);
                    }
                }
                edges[cpui] = m_relation;
            }
        }

        private void HideEdgesUI()
        {
            if (edges != null)
            {
                foreach (var item in edges)
                {
                    item.Key.moved -= OnEdgeMoved;
                    item.Key.onSelected -= OnEdgeCPUISelected;
                    if (item.Key.gameObject != null)
                    {
                        Destroy(item.Key.gameObject);
                    }
                }
            }
            if (edgeControlPoints != null)
            {
                foreach (var item in edgeControlPoints)
                {
                    if (item != null)
                    {
                        Destroy(item);
                    }
                }
            }
            edges = null;
            edgeControlPoints = null;
        }

        private void OnEdgeMoved(object sender)
        {
            ControlPointUI cpui = (ControlPointUI)sender;
            if (edgeControlPointSelected == cpui)
            {
                EdgeRelation relation;
                if (edges.TryGetValue(cpui, out relation))
                {
                    for (int i = 0; i < relation.originalDifs.Count; i++)
                    {
                        relation.vertices[i].UpdatePosition(cpui.sceneObjects[0].transform.position + relation.originalDifs[i]);
                        relation.vertices[i].cpui.UpdateUIPosition();
                    }
                }
            }
        }

        public virtual void UpdateGeometry()
        {
            UpdateGeometry(null);
        }

        protected virtual void UpdateGeometry(GeometryVertex vertex)
        {
            var m = this as PolygonSegment;

            if (showDistances)
            {
                for (int i = 0; i < distances.Count; i++)
                {
                    int next;
                    if (closed)
                    {
                        next = (i + 1) % (vertices.Count);
                    }
                    else
                    {
                        next = (i + 1);
                    }
                    UpdateDistance(vertices[i].currentPosition, vertices[next].currentPosition, distances[i], i);
                }
            }
            if (closed && fill)
            {
                GenerateMeshes();
            }
            else
            {
                UpdateGeometryData();
                OnUpdated();
            }
        }

        protected void UpdateGeometryData()
        {
            area = CalculateArea();
            perimeter = CalculatePerimeter();
        }

        protected virtual void GenerateMeshes()
        {
            Vector2[] verts = new Vector2[vertices.Count];
            Vector3[] vert3D = new Vector3[vertices.Count];
            for (int i = 0; i < verts.Length; i++)
            {
                vert3D[i] = vertices[i].currentPosition + new Vector3(0, offsetFromBase, 0);
                vert3D[i] = transform.InverseTransformPoint(vert3D[i]);
                verts[i] = new Vector2(vert3D[i].x, vert3D[i].z);
            }

            if (!Polygon.IsClockWise(vert3D.ToList()))
            {
                vert3D = vert3D.Reverse().ToArray();
                verts = verts.Reverse().ToArray();
            }

            var triangulator = new TriangulatorSimple(verts);
            var tris = triangulator.Triangulate();

            mesh = new Mesh();
            mesh.vertices = vert3D;

            if (!capped)
            {
                mesh.triangles = tris;
                triangles = tris;
            }
            else
            {
                triangles = new int[tris.Length];
                for (int i = 0; i < tris.Length; i += 3)
                {
                    triangles[i] = tris[i + 2];
                    triangles[i + 1] = tris[i + 1];
                    triangles[i + 2] = tris[i];
                }
                mesh.triangles = triangles;
            }


            Vector2[] m_uvs = new Vector2[mesh.vertexCount];
            for (int i = 0; i < m_uvs.Length; i++)
            {
                m_uvs[i] = new Vector2(mesh.vertices[i].x, mesh.vertices[i].y);
            }
            mesh.uv = m_uvs;
            mesh.uv2 = m_uvs;
            mesh.uv3 = m_uvs;
            mesh.uv4 = m_uvs;

            UpdateMeshComponents();
            UpdateGeometryData();
            OnUpdated();
        }

        protected void UpdateMeshComponents()
        {
            try
            {
                mesh.RecalculateNormals();
                mesh.RecalculateBounds();
                meshFilter.mesh = mesh;
                GetComponent<MeshCollider>().sharedMesh = mesh;
            }
            catch (Exception e)
            {
                Debug.Log(gameObject.name + ": " + e);
            }
        }


        private float CalculateArea()
        {
            area = 0;
            if (closed && fill)
            {
                for (int i = 0; i < triangles.Length / 3; i++)
                {
                    Vector3 a = vertices[triangles[i * 3]].transform.position;
                    Vector3 b = vertices[triangles[i * 3 + 1]].transform.position;
                    Vector3 c = vertices[triangles[i * 3 + 2]].transform.position;
                    float partA = a.x * (b.z - c.z);
                    float partB = b.x * (c.z - a.z);
                    float partC = c.x * (a.z - b.z);
                    area += Mathf.Abs((partA + partB + partC)) / 2.0f;
                }
            }
            return area;
        }

        private float CalculatePerimeter()
        {
            float m_perimeter = 0;
            for (int i = 1; i < vertices.Count; i++)
            {
                m_perimeter += Vector3.Distance(vertices[i].currentPosition, vertices[i - 1].currentPosition);
            }
            return m_perimeter;
        }



        private void OnColinearChanged(bool colinear)
        {
            moveColinear = colinear;
        }



        private void OnControlPointSelected(ControlPointUI cpui)
        {
            for (int i = 0; i < vertices.Count; i++)
            {
                if (vertices[i].cpui == cpui)
                {
                    selectedCpui = cpui;
                    selectedVertex = vertices[i];
                }
            }
        }

        private void DeleteVertex()
        {
            if (selectedVertex != null)
            {
                vertices.Remove(selectedVertex);
                selectedVertex.DestroyVertex();
                if (showDistances)
                {
                    RecalculateDistances();
                }
            }
        }

        private Vector3 GetCentroid()
        {
            float A = 0;
            float cX = 0;
            float cZ = 0;

            for (int i = 0; i < vertices.Count - 1; i++)
            {
                A += ((vertices[i].currentPosition.x * vertices[i + 1].currentPosition.z) - (vertices[i + 1].currentPosition.x * vertices[i].currentPosition.z));
                cX += ((vertices[i].currentPosition.x + vertices[i + 1].currentPosition.x) * ((vertices[i].currentPosition.x * vertices[i + 1].currentPosition.z) - (vertices[i + 1].currentPosition.x * vertices[i].currentPosition.z)));
                cZ += ((vertices[i].currentPosition.z + vertices[i + 1].currentPosition.z) * ((vertices[i].currentPosition.x * vertices[i + 1].currentPosition.z) - (vertices[i + 1].currentPosition.x * vertices[i].currentPosition.z)));
            }

            A *= 0.5f;
            cX /= (6 * A);
            cZ /= (6 * A);

            return new Vector3(cX, 0, cZ);
        }

        List<List<Vector3>> GetColinearPoints(List<float> distinctValues, List<Vector3> points, List<List<Vector3>> groups, string _case)
        {
            for (int i = 0; i < distinctValues.Count; i++)
            {
                List<Vector3> group = new List<Vector3>();
                for (int j = 0; j < points.Count; j++)
                {
                    if (_case == "x")
                    {
                        if (points[j].y == distinctValues[i])
                        {
                            group.Add(points[j]);
                        }
                    }
                    else if (_case == "y")
                    {
                        if (points[j].x == distinctValues[i])
                        {
                            group.Add(points[j]);
                        }
                    }

                }

                //get the first and last point allong axis
                if (_case == "x")
                {
                    group.OrderBy(c => c.x);
                }
                if (_case == "x")
                {
                    group.OrderBy(c => c.y);
                }
                List<Vector3> coords = new List<Vector3>();
                coords.Add(group[0]);
                coords.Add(group[group.Count - 1]);
                group = new List<Vector3>();
                groups.Add(coords);
            }

            return groups;
        }

        #endregion

        #region Static Methods
        /// <summary>
        /// Checks if a list of points is clock-wise
        /// </summary>
        /// <param name="points">The list of points</param>
        /// <returns>Boolean</returns>
        public static bool IsClockWise(List<Vector3> points)
        {
            float sum = 0;
            for (int i = 0; i < points.Count; i++)
            {
                int next = (i + 1) % points.Count;
                sum += ((points[next].x - points[i].x) * (points[next].z + points[i].z));
            }
            return sum >= 0;
        }

        /// <summary>
        /// Checks if a list of points is clock-wise
        /// </summary>
        /// <param name="points">The list of points</param>
        /// <returns>Boolean</returns>
        public static bool IsClockWise(Vector3[] points)
        {
            float sum = 0;
            for (int i = 0; i < points.Length; i++)
            {
                int next = (i + 1) % points.Length;
                sum += ((points[next].x - points[i].x) * (points[next].z + points[i].z));
            }
            return sum >= 0;
        }
        #endregion
    }
}

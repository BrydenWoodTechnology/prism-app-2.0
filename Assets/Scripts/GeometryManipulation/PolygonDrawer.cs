﻿using BrydenWoodUnity.DesignData;
using BrydenWoodUnity.UIElements;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BrydenWoodUnity.GeometryManipulation
{
    /// <summary>
    /// The plane on which the polygons will be drawn
    /// </summary>
    public enum DrawingPlane
    {
        XY,
        XZ,
        ZY
    }

    /// <summary>
    /// A MonoBehaviour component to draw polygons
    /// </summary>
    public class PolygonDrawer : MonoBehaviour
    {

        #region Public Fields and Properties
        [Header("Main References")]
        public GameObject polygonPrefab;
        public GameObject polygonSegmentPrefab;
        public GameObject polygonVertexPrefab;
        public GameObject controlPointUIPrefab;
        public DrawingPlane drawingPlane = DrawingPlane.XZ;
        public Button addPolygon;
        public Button stopPolygon;
        public Button addSegment;
        public Button stopSegment;
        public DesignInputTabs designInputTabs;

        [Header("Optional References:")]
        public Camera raycastingCamera;
        public Transform polygonParent;
        public Transform polygonVertexParent;
        public Transform controlPointUiParent;

        [Header("Behaviour Settings:")]
        public float lastVertexSnap = 0.1f;
        public bool showDistances = false;
        public bool ortho = false;
        public float orthoDelta = 5.0f;
        public bool isSite;
        public LayerMask raycastingMask;
        public bool usePlane = true;
        private bool isBasement;
        private bool isPodium;
        public bool IsBasement
        {
            get { return isBasement; }
            set
            {
                isBasement = value;
                isPodium = false;
            }
        }
        public bool IsPodium
        {
            get { return isPodium; }
            set
            {
                isPodium = value;
                isBasement = false;
            }
        }

        public Polygon sitePolygon { get; set; }
        public Polygon basePolygon { get; set; }
        public bool isPolygonSegment { get; set; }
        public float temp_Y { get; set; }
        #endregion

        #region Private Field and Properties
        protected List<PolygonVertex> temp_Vertices { get; set; }
        protected bool creatingPolygon { get; set; }
        protected Plane plane { get; set; }
        protected Polygon temp_polygon { get; set; }
        protected PolygonSegment temp_segment { get; set; }
        protected PolygonVertex tempVertex { get; set; }
        protected ControlPointUI tempCpui { get; set; }
        protected LineRenderer temp_lineRenderer { get; set; }
        protected List<Vector3> temp_points { get; set; }
        protected List<KeyValuePair<int, float>> tempLengths { get; set; }
        private List<float> coresParams { get; set; }
        private float halfCore { get; set; }
        private float corridorWidth { get; set; }
        private float aptWidth { get; set; }
        #endregion

        #region Events
        /// <summary>
        /// Used when the drawing of a polygon has ended
        /// </summary>
        /// <param name="drawer">The drawer of the polygon</param>
        /// <param name="polygon">The resulting polygon</param>
        public delegate void OnPolygonEnded(PolygonDrawer drawer, Polygon polygon);
        /// <summary>
        /// Triggered when the drawing of a polygon has ended
        /// </summary>
        public static event OnPolygonEnded polygonEnded;
        /// <summary>
        /// Triggered when the loading of a polygon has ended
        /// </summary>
        public static event OnPolygonEnded polygonLoaded;
        /// <summary>
        /// Triggered when the drawing of a polygon has started
        /// </summary>
        public static event OnPolygonEnded polygonStarted;
        /// <summary>
        /// Used when the drawing of a Polygon Segment 
        /// </summary>
        /// <param name="drawer">The polygon drawer</param>
        /// <param name="polygonSegment">The resulting polygon segment</param>
        public delegate void OnPolygonSegmentEnded(PolygonDrawer drawer, PolygonSegment polygonSegment);
        /// <summary>
        /// Triggered when the drawing of a Polygon Segment has ended
        /// </summary>
        public static event OnPolygonSegmentEnded polygonSegmentEnded;
        #endregion

        #region MonoBehaviour Methods
        private void OnDestroy()
        {
            if (polygonEnded != null)
            {
                foreach (Delegate eh in polygonEnded.GetInvocationList())
                {
                    polygonEnded -= (OnPolygonEnded)eh;
                }
            }
            if (polygonLoaded != null)
            {
                foreach (Delegate eh in polygonLoaded.GetInvocationList())
                {
                    polygonLoaded -= (OnPolygonEnded)eh;
                }
            }
            if (polygonStarted != null)
            {
                foreach (Delegate eh in polygonStarted.GetInvocationList())
                {
                    polygonStarted -= (OnPolygonEnded)eh;
                }
            }
        }

        // Use this for initialization
        void Start()
        {
            if (raycastingCamera == null)
            {
                raycastingCamera = Camera.main;
            }
            if (polygonParent == null)
            {
                polygonParent = transform;
            }
            if (controlPointUiParent == null)
            {
                controlPointUiParent = new GameObject("ControlPointUIParent").transform;
                controlPointUiParent.SetParent(GameObject.Find("Canvas").transform);
            }
            SetPlane(Vector3.zero, drawingPlane);
            PolygonDrawer.polygonEnded += OnSiteEnded;

            addPolygon.gameObject.SetActive(true);
            stopPolygon.gameObject.SetActive(false);

        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                ortho = true;
            }
            else
            {
                ortho = false;
            }

            if (creatingPolygon)
            {
                if (Selector.TaggedObject.enabled)
                {
                    Selector.TaggedObject.enabled = false;
                }

                if (isPolygonSegment)
                {
                    Vector3 pt = new Vector3();
                    KeyValuePair<int, float> curveParameter = new KeyValuePair<int, float>();
                    Ray r = raycastingCamera.ScreenPointToRay(Input.mousePosition);
                    float dist = 0;
                    plane = new Plane(Vector3.up, new Vector3(0, basePolygon[0].currentPosition.y, 0));
                    if (plane.Raycast(r, out dist))
                    {
                        pt = basePolygon.PointOnCurve(r.GetPoint(dist), out curveParameter, aptWidth);

                        if (coresParams != null && coresParams.Count > 0)
                        {
                            for (int i = 0; i < coresParams.Count; i++)
                            {
                                var masterCorePosition = basePolygon.PointOnCurve(coresParams[i]);
                                float coreDistance = Vector3.Distance(masterCorePosition, pt);
                                if (coreDistance < (halfCore + corridorWidth))
                                {
                                    Vector3 baseVector = masterCorePosition - basePolygon[curveParameter.Key].currentPosition;
                                    Vector3 deltaVector = pt - masterCorePosition;
                                    pt = basePolygon[curveParameter.Key].currentPosition + baseVector + deltaVector.normalized * (halfCore + corridorWidth);
                                    float len1 = (basePolygon[curveParameter.Key + 1].currentPosition - basePolygon[curveParameter.Key].currentPosition).magnitude;
                                    float len2 = (pt - basePolygon[curveParameter.Key].currentPosition).magnitude;
                                    curveParameter = new KeyValuePair<int, float>(curveParameter.Key, len2 / len1);
                                }
                            }
                        }

                        tempVertex.transform.position = new Vector3(pt.x, temp_Y, pt.z);
                        tempCpui.GetComponent<RectTransform>().position = Camera.main.WorldToScreenPoint(tempVertex.transform.position);

                        if (Input.GetMouseButtonDown(0))
                        {
                            AddPolygonSegmentPoint(curveParameter);
                            tempCpui.GetComponent<Image>().color = Color.yellow;
                        }

                        if (temp_segment.showDistances && temp_Vertices.Count > 0)
                        {
                            temp_segment.DisplayDistance(tempVertex.transform.position);
                        }
                    }

                    if (Input.GetKeyDown(KeyCode.Escape))
                    {
                        StartCoroutine(AbortPolygon());
                        if (!Selector.TaggedObject.enabled)
                        {
                            Selector.TaggedObject.enabled = true;
                        }
                    }

                    if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Space))
                    {
                        OnStopSegment();
                    }
                }
                else
                {
                    Vector3 pt = new Vector3();

                    if (temp_polygon.showDistances && temp_Vertices.Count > 0)
                    {
                        Ray r = raycastingCamera.ScreenPointToRay(Input.mousePosition);
                        float dist = 0;
                        if (plane.Raycast(r, out dist))
                        {
                            if (temp_Vertices.Count >= 2)
                            {
                                bool close = false;
                                float angle = Vector3.Angle(temp_Vertices[temp_Vertices.Count - 2].currentPosition - temp_Vertices[temp_Vertices.Count - 1].currentPosition, temp_Vertices[temp_Vertices.Count - 1].currentPosition - r.GetPoint(dist));


                                if (Mathf.Abs(angle - 0) <= orthoDelta)
                                {
                                    angle = 0;
                                    close = true;
                                }
                                else if (Mathf.Abs(angle - 30) <= orthoDelta)
                                {
                                    angle = 30;
                                    close = true;
                                }
                                else if (Mathf.Abs(angle - 60) <= orthoDelta)
                                {
                                    angle = 60;
                                    close = true;
                                }
                                else if (Mathf.Abs(angle - 90) <= orthoDelta)
                                {
                                    angle = 90;
                                    close = true;
                                }
                                else if (Mathf.Abs(angle - 180) <= orthoDelta)
                                {
                                    angle = 180;
                                    close = true;
                                }

                                if (close)
                                {
                                    var cross = Vector3.Cross(temp_Vertices[temp_Vertices.Count - 2].currentPosition - temp_Vertices[temp_Vertices.Count - 1].currentPosition, temp_Vertices[temp_Vertices.Count - 1].currentPosition - r.GetPoint(dist));
                                    if (cross.y < 0)
                                    {
                                        angle = -angle;
                                    }
                                    var vec = temp_Vertices[temp_Vertices.Count - 1].currentPosition - temp_Vertices[temp_Vertices.Count - 2].currentPosition;
                                    Vector3 axis = (Quaternion.AngleAxis(angle, Vector3.up) * vec).normalized;//new Vector3(x,0,y).normalized;

                                    pt = temp_Vertices.Last().currentPosition + Vector3.Project(r.GetPoint(dist) - temp_Vertices.Last().currentPosition, axis);
                                }
                                else
                                {
                                    pt = r.GetPoint(dist);
                                }
                            }
                            else
                            {
                                pt = r.GetPoint(dist);
                            }

                            if (!ortho)
                            {
                                temp_polygon.DisplayDistance(r.GetPoint(dist));
                            }
                            else
                            {
                                temp_polygon.DisplayDistance(pt);
                            }
                        }
                    }

                    if (Input.GetMouseButtonDown(0))
                    {
                        if (!ortho || temp_Vertices.Count < 2)
                        {
                            AddPoint();
                        }
                        else
                        {
                            AddPoint(pt);
                        }
                    }

                    if (Input.GetKeyDown(KeyCode.Escape))
                    {
                        StartCoroutine(AbortPolygon());
                        if (!Selector.TaggedObject.enabled)
                        {
                            Selector.TaggedObject.enabled = true;
                        }
                    }

                    if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Space))
                    {
                        OnStopPolygon();
                    }
                }

            }

            if (Input.GetKeyDown(KeyCode.Delete))
            {
                if (creatingPolygon)
                {
                    UndoPointAddition();
                }
            }

            if (designInputTabs != null)
            {
                if (designInputTabs.hasSelected && !creatingPolygon)
                {
                    //addPolygon.interactable = true;
                }
                else
                {
                    //addPolygon.interactable = false;
                }
            }
            else
            {
                if (!creatingPolygon)
                {
                    addPolygon.interactable = true;
                }
                else
                {
                    addPolygon.interactable = false;
                }
            }
        }
        #endregion


        #region Public Methods
        /// <summary>
        /// Called to stop the creation of a polygon segment
        /// </summary>
        public void OnStopSegment()
        {
            if (temp_Vertices.Count > 1)
            {
                if (!isSite)
                {
                    EndOpenPolygonSegment();
                }
                if (!Selector.TaggedObject.enabled)
                {
                    Selector.TaggedObject.enabled = true;
                }
                addSegment.gameObject.SetActive(true);
                stopSegment.gameObject.SetActive(false);
            }
        }

        /// <summary>
        /// Called to stop the creation of a polygon
        /// </summary>
        public void OnStopPolygon()
        {
            if (!isSite)
            {
                if (isBasement || isPodium)
                {
                    if (temp_Vertices.Count > 2)
                    {
                        EndClosedPolygon();
                        if (!Selector.TaggedObject.enabled)
                        {
                            Selector.TaggedObject.enabled = true;
                        }
                    }
                }
                else
                {
                    if (temp_Vertices.Count > 1 && !isSite)
                    {
                        EndOpenPolygon();

                        if (!Selector.TaggedObject.enabled)
                        {
                            Selector.TaggedObject.enabled = true;
                        }
                    }
                }
            }
            else
            {
                if (temp_Vertices.Count > 2)
                {
                    EndClosedPolygon();
                    if (!Selector.TaggedObject.enabled)
                    {
                        Selector.TaggedObject.enabled = true;
                    }
                }
            }
            temp_polygon.UpdateVertexPositions();
        }

        /// <summary>
        /// Loads a Polygon
        /// </summary>
        /// <param name="verts">The coordinates of the vertices as a continuous array</param>
        /// <param name="closed">Whether the polygon should be closed or not</param>
        public void LoadPolygon(float[] verts, bool closed, bool isPodium = false, bool isBasement = false)
        {
            showDistances = false;
            List<Vector3> vertices = new List<Vector3>();
            for (int i = 0; i < verts.Length; i += 3)
            {
                vertices.Add(new Vector3(verts[i], verts[i + 1], verts[i + 2]));
            }
            StartPolygon(isBasement, isPodium);
            for (int i = 0; i < vertices.Count; i++)
            {
                AddPoint(vertices[i]);
            }
            if (closed)
            {
                EndClosedPolygon();
            }
            else
            {
                EndOpenPolygon();
            }
            showDistances = true;
            temp_polygon.DiscardAllDistances();
            temp_polygon.ToggleDistances(showDistances);
        }

        public void LoadPodiumPolygon(float[] verts, bool closed)
        {
            IsPodium = true;
            LoadPolygon(verts, closed,true,false);
        }

        public void LoadBasementPolygon(float[] verts, bool closed)
        {
            IsBasement = true;
            LoadPolygon(verts, closed, false, true);
        }

        /// <summary>
        /// Loads a Polygon segment
        /// </summary>
        /// <param name="curveParams">The of base polygon segment indices and parameters</param>
        /// <param name="verts">The coordinates of the vertices as a continuous array</param>
        /// <param name="basePolygon">The base polygon</param>
        /// <param name="coreParams">The curve parameters (0-1) of the cores</param>
        /// <param name="halfCore">Half the length of a core</param>
        /// <param name="corridorWidth">The width of the corridor</param>
        /// <param name="aptWidth">The width of the apartment</param>
        public void LoadPolygonSegment(List<KeyValuePair<int, float>> curveParams, float[] verts, Polygon basePolygon, List<float> coreParams, float halfCore, float corridorWidth, float aptWidth = 6.8f)
        {
            showDistances = false;
            List<Vector3> vertices = new List<Vector3>();
            for (int i = 0; i < verts.Length; i += 3)
            {
                vertices.Add(new Vector3(verts[i], verts[i + 1], verts[i + 2]));
            }
            StartPolygonSegment(basePolygon, coreParams, halfCore, corridorWidth, aptWidth);
            for (int i = 0; i < curveParams.Count; i++)
            {
                AddPolygonSegmentPoint(curveParams[i], vertices[i]);
            }
            EndOpenPolygonSegment();
        }

        /// <summary>
        /// Called when the creation of a site has ended
        /// </summary>
        /// <param name="sender">The polygon drawer</param>
        /// <param name="polygon">The resulting site</param>
        public void OnSiteEnded(PolygonDrawer sender, Polygon polygon)
        {
            if (sender.isSite)
            {
                sitePolygon = polygon;
            }
        }

        /// <summary>
        /// Called when the creation of a polygon starts
        /// </summary>
        public virtual void StartPolygon()
        {
            StartPolygon(false);
        }

        /// <summary>
        /// Called when the creation of a polygon starts
        /// </summary>
        public virtual void StartPolygon(bool isBasement = false, bool isPodium = false)
        {
            if (!isSite)
            {
                addPolygon.gameObject.SetActive(false);
                stopPolygon.gameObject.SetActive(true);
            }
            this.isBasement = isBasement;
            this.isPodium = isPodium;
            temp_Vertices = new List<PolygonVertex>();
            temp_points = new List<Vector3>();
            creatingPolygon = true;
            temp_polygon = Instantiate(polygonPrefab, polygonParent).GetComponent<Polygon>();

            if (isSite)
            {
                temp_polygon.gameObject.layer = 31;
                temp_polygon.lineWidth = 0.8f;
            }

            //addPolygon.interactable = false;
            temp_polygon.showDistances = showDistances;
            if (polygonStarted != null)
            {
                polygonStarted(this, temp_polygon);
            }
        }

        public virtual void StartBasementPolygon()
        {
            StartPolygon(true, false);
        }

        public virtual void StartPodiumPolygon()
        {
            StartPolygon(false, true);
        }

        /// <summary>
        /// Called when the creation of a polygon segment starts
        /// </summary>
        /// <param name="basePolygon">The base polygon</param>
        /// <param name="coresParams">The curve parameters (0-1) of the cores</param>
        /// <param name="halfCore">Half the length of a core</param>
        /// <param name="corridorWidth">The width of the corridor</param>
        /// <param name="aptWidth">The width of the apartment</param>
        public void StartPolygonSegment(Polygon basePolygon, List<float> coresParams = null, float halfCore = 0, float corridorWidth = 0, float aptWidth = 6.8f)
        {
            if (!isSite)
            {
                addSegment.gameObject.SetActive(false);
                stopSegment.gameObject.SetActive(true);
            }

            isPolygonSegment = true;
            this.basePolygon = basePolygon;

            if (coresParams != null)
            {
                this.coresParams = coresParams;
                this.halfCore = halfCore;
                this.corridorWidth = corridorWidth;
            }
            this.aptWidth = aptWidth;

            tempLengths = new List<KeyValuePair<int, float>>();
            temp_Vertices = new List<PolygonVertex>();
            temp_points = new List<Vector3>();
            creatingPolygon = true;
            temp_segment = Instantiate(polygonSegmentPrefab, polygonParent).GetComponent<PolygonSegment>();
            //addSegment.interactable = false;
            temp_segment.showDistances = showDistances;

            if (polygonVertexParent == null)
            {
                polygonVertexParent = temp_segment.transform;
            }

            tempVertex = Instantiate(polygonVertexPrefab).GetComponent<PolygonVertex>();
            tempCpui = Instantiate(controlPointUIPrefab, controlPointUiParent).GetComponent<ControlPointUI>();
            if (isSite) tempCpui.belongsToSite = true; //differentiate site control points
            //tempCpui.transform.SetAsFirstSibling();
            tempCpui.GetComponent<Image>().enabled = true;
            tempCpui.GetComponent<Image>().color = Color.yellow;
        }

        /// <summary>
        /// Adds a point to the polygon currently being drawn
        /// </summary>
        public virtual void AddPoint()
        {
            bool eventCheck = Input.touchCount > 0 ? !EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId) : !EventSystem.current.IsPointerOverGameObject(-1);
            if (eventCheck)
            {
                Ray r = raycastingCamera.ScreenPointToRay(Input.mousePosition);
                float dist = 0;
                if (usePlane)
                {
                    if (plane.Raycast(r, out dist))
                    {
                        bool cast = false;
                        if (sitePolygon != null && !isSite)
                        {
                            if (sitePolygon.IsIncluded(r.GetPoint(dist)))
                            {
                                cast = true;
                            }
                            else
                            {
                                cast = false;
                            }
                        }
                        else
                        {
                            cast = true;
                        }

                        if (cast)
                        {
                            if (temp_polygon.showDistances && temp_polygon.Count > 0)
                            {
                                temp_polygon.CloseDistance();
                            }

                            Vector3 point = r.GetPoint(dist);

                            if (polygonVertexParent == null)
                            {
                                polygonVertexParent = temp_polygon.transform;
                            }

                            PolygonVertex pVertex = Instantiate(polygonVertexPrefab).GetComponent<PolygonVertex>();
                            pVertex.transform.position = point;
                            pVertex.Initialize(temp_Vertices.Count);
                            ControlPointUI cpui = Instantiate(controlPointUIPrefab, controlPointUiParent).GetComponent<ControlPointUI>();
                            if (isSite) cpui.belongsToSite = true;
                            cpui.Initialize(pVertex.transform);
                            pVertex.SetGUIElement(cpui);
                            pVertex.UpdatePosition(point);
                            if (temp_Vertices.Count == 0)
                            {
                                cpui.onSelected += OnFisrtCPClicked;
                            }
                            temp_Vertices.Add(pVertex);
                            temp_points.Add(pVertex.currentPosition);

                            if (temp_polygon.showDistances)
                            {
                                temp_polygon.StartDistance(r.GetPoint(dist));
                            }
                        }
                    }
                }
                else
                {
                    RaycastHit hit;
                    if (Physics.Raycast(r, out hit, 1000, raycastingMask))
                    {
                        bool cast = false;
                        if (sitePolygon != null && !isSite)
                        {
                            if (sitePolygon.IsIncluded(hit.point))
                            {
                                cast = true;
                            }
                            else
                            {
                                cast = false;
                            }
                        }
                        else
                        {
                            cast = true;
                        }

                        if (cast)
                        {
                            if (temp_polygon.showDistances && temp_polygon.Count > 0)
                            {
                                temp_polygon.CloseDistance();
                            }

                            Vector3 point = hit.point;

                            if (polygonVertexParent == null)
                            {
                                polygonVertexParent = temp_polygon.transform;
                            }

                            PolygonVertex pVertex = Instantiate(polygonVertexPrefab).GetComponent<PolygonVertex>();
                            pVertex.transform.position = point;
                            pVertex.Initialize(temp_Vertices.Count);
                            ControlPointUI cpui = Instantiate(controlPointUIPrefab, controlPointUiParent).GetComponent<ControlPointUI>();
                            if (isSite) cpui.belongsToSite = true;
                            cpui.Initialize(pVertex.transform);
                            pVertex.SetGUIElement(cpui);
                            pVertex.UpdatePosition(point);
                            if (temp_Vertices.Count == 0)
                            {
                                cpui.onSelected += OnFisrtCPClicked;
                            }
                            temp_Vertices.Add(pVertex);
                            temp_points.Add(pVertex.currentPosition);

                            if (temp_polygon.showDistances)
                            {
                                temp_polygon.StartDistance(hit.point);
                            }


                            plane = new Plane(Vector3.up, hit.point);
                            usePlane = true;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Adds a polygon vertex at the position of the mouse
        /// </summary>
        /// <param name="mousePosition">The position of the mouse</param>
        public virtual void AddPointAtMousePosition(Vector2 mousePosition)
        {
            Ray r = raycastingCamera.ScreenPointToRay(mousePosition);
            float dist = 0;
            if (plane.Raycast(r, out dist))
            {
                bool cast = false;
                if (sitePolygon != null)
                {
                    if (sitePolygon.IsIncluded(r.GetPoint(dist)))
                    {
                        cast = true;
                    }
                    else
                    {
                        cast = false;
                    }
                }
                else
                {
                    cast = true;
                }

                if (cast)
                {
                    if (temp_polygon.showDistances && temp_polygon.Count > 0)
                    {
                        temp_polygon.CloseDistance();
                    }

                    Vector3 point = r.GetPoint(dist);

                    if (polygonVertexParent == null)
                    {
                        polygonVertexParent = temp_polygon.transform;
                    }

                    PolygonVertex pVertex = Instantiate(polygonVertexPrefab).GetComponent<PolygonVertex>();
                    pVertex.transform.position = point;
                    pVertex.Initialize(temp_Vertices.Count);
                    ControlPointUI cpui = Instantiate(controlPointUIPrefab, controlPointUiParent).GetComponent<ControlPointUI>();
                    if (isSite) cpui.belongsToSite = true;
                    cpui.Initialize(pVertex.transform);
                    pVertex.SetGUIElement(cpui);
                    pVertex.UpdatePosition(point);
                    if (temp_Vertices.Count == 0)
                    {
                        cpui.onSelected += OnFisrtCPClicked;
                    }
                    temp_Vertices.Add(pVertex);
                    temp_points.Add(pVertex.currentPosition);

                    if (temp_polygon.showDistances)
                    {
                        temp_polygon.StartDistance(r.GetPoint(dist));
                    }
                }
            }
        }

        /// <summary>
        /// Adds a point to the polygon currently being drawn
        /// </summary>
        /// <param name="point">The point to be added</param>
        public virtual void AddPoint(Vector3 point)
        {
            bool cast = false;
            if (sitePolygon != null)
            {
                if (sitePolygon.IsIncluded(point))
                {
                    cast = true;
                }
                else
                {
                    cast = false;
                }
            }
            else
            {
                cast = true;
            }

            if (cast)
            {
                if (temp_polygon.showDistances && temp_polygon.Count > 0)
                {
                    temp_polygon.CloseDistance();
                }

                if (polygonVertexParent == null)
                {
                    polygonVertexParent = temp_polygon.transform;
                }

                PolygonVertex pVertex = Instantiate(polygonVertexPrefab).GetComponent<PolygonVertex>();
                pVertex.transform.position = point;
                pVertex.Initialize(temp_Vertices.Count);
                ControlPointUI cpui = Instantiate(controlPointUIPrefab, controlPointUiParent).GetComponent<ControlPointUI>();
                if (isSite) cpui.belongsToSite = true;
                cpui.Initialize(pVertex.transform);
                pVertex.SetGUIElement(cpui);
                pVertex.UpdatePosition(point);
                if (temp_Vertices.Count == 0)
                {
                    cpui.onSelected += OnFisrtCPClicked;
                }
                temp_Vertices.Add(pVertex);
                temp_points.Add(pVertex.currentPosition);

                if (temp_polygon.showDistances)
                {
                    temp_polygon.StartDistance(point);
                }
            }
        }

        /// <summary>
        /// Adds a polygon vertex at a given position
        /// </summary>
        /// <param name="value">The world-space position as string</param>
        public virtual void AddPointAtWorldSpace(string value)
        {
            var coords = value.Split(',').Select(x => float.Parse(x)).ToArray();
            Vector3 point = new Vector3(coords[0], coords[1], coords[2]);

            bool cast = false;
            if (sitePolygon != null)
            {
                if (sitePolygon.IsIncluded(point))
                {
                    cast = true;
                }
                else
                {
                    cast = false;
                }
            }
            else
            {
                cast = true;
            }

            if (cast)
            {
                if (temp_polygon.showDistances && temp_polygon.Count > 0)
                {
                    temp_polygon.CloseDistance();
                }

                if (polygonVertexParent == null)
                {
                    polygonVertexParent = temp_polygon.transform;
                }

                PolygonVertex pVertex = Instantiate(polygonVertexPrefab).GetComponent<PolygonVertex>();
                pVertex.transform.position = point;
                pVertex.Initialize(temp_Vertices.Count);
                ControlPointUI cpui = Instantiate(controlPointUIPrefab, controlPointUiParent).GetComponent<ControlPointUI>();
                if (isSite) cpui.belongsToSite = true;
                cpui.Initialize(pVertex.transform);
                pVertex.SetGUIElement(cpui);
                pVertex.UpdatePosition(point);
                if (temp_Vertices.Count == 0)
                {
                    cpui.onSelected += OnFisrtCPClicked;
                }
                temp_Vertices.Add(pVertex);
                temp_points.Add(pVertex.currentPosition);

                if (temp_polygon.showDistances)
                {
                    temp_polygon.StartDistance(point);
                }
            }
        }

        /// <summary>
        /// Adds a point to the polygon currently being drawn
        /// </summary>
        /// <param name="point">The point to be added</param>
        /// <param name="cpui">The GUI element of that point</param>
        public virtual void AddPoint(Vector3 point, ControlPointUI cpui)
        {
            if (temp_polygon.showDistances && temp_polygon.Count > 0)
            {
                temp_polygon.CloseDistance();
            }

            if (polygonVertexParent == null)
            {
                polygonVertexParent = temp_polygon.transform;
            }

            PolygonVertex pVertex = Instantiate(polygonVertexPrefab).GetComponent<PolygonVertex>();
            pVertex.transform.position = point;
            pVertex.Initialize(temp_Vertices.Count);
            cpui.AddSceneObject(pVertex.transform);
            pVertex.SetGUIElement(cpui);
            pVertex.UpdatePosition(point);
            if (temp_Vertices.Count == 0)
            {
                cpui.onSelected += OnFisrtCPClicked;
            }
            temp_Vertices.Add(pVertex);
            temp_points.Add(pVertex.currentPosition);

            if (temp_polygon.showDistances)
            {
                temp_polygon.StartDistance(point);
            }
        }

        /// <summary>
        /// Returns a copy of a polygon
        /// </summary>
        /// <param name="polygon">The base polygon to be copied</param>
        /// <param name="yValue">Offset in the Y-axis</param>
        /// <returns>Polygon</returns>
        public Polygon CopyPolygon(Polygon polygon, float yValue)
        {
            StartPolygon();
            temp_polygon.showDistances = false;
            temp_polygon.isCopy = true;
            //yield return new WaitForEndOfFrame();

            for (int i = 0; i < polygon.Count; i++)
            {
                Vector3 newPos = new Vector3(polygon[i].currentPosition.x, yValue, polygon[i].currentPosition.z);
                AddPoint(newPos, polygon[i].cpui);
            }

            if (polygon.closed)
            {
                EndClosedPolygon(false, false);
            }
            else
            {
                EndOpenPolygon(false, false);
            }

            return temp_polygon;
        }

        /// <summary>
        /// Stops the creation of a closed polygon
        /// </summary>
        /// <param name="loaded">Whether the polygon was loaded or created</param>
        /// <param name="triggerEvent">Whether an event should be triggered</param>
        public virtual void EndClosedPolygon(bool loaded = false, bool triggerEvent = true)
        {
            if (stopPolygon != null)
            {
                stopPolygon.gameObject.SetActive(false);
            }
            if (addPolygon != null)
            {
                addPolygon.gameObject.SetActive(true);
            }

            List<PolygonVertex> m_vertices = new List<PolygonVertex>();
            for (int i = 0; i < temp_Vertices.Count; i++)
            {
                m_vertices.Add(temp_Vertices[i]);
            }

            if (temp_lineRenderer != null && temp_lineRenderer.gameObject != null)
            {
                Destroy(temp_lineRenderer.gameObject);
            }

            if (isSite)
            {
                temp_polygon.GetComponent<MeshRenderer>().material = Resources.Load("Materials/SiteMaterial") as Material;
                temp_polygon.Initialize(m_vertices, true, true);
            }
            else
            {
                temp_polygon.transform.position = m_vertices[0].currentPosition;
                temp_polygon.Initialize(m_vertices, true, isBasement || isPodium);
            }

            for (int i = 0; i < m_vertices.Count; i++)
            {
                if (isSite)
                {
                    m_vertices[i].transform.SetParent(polygonVertexParent);
                }
                else
                {
                    m_vertices[i].transform.SetParent(temp_polygon.transform);
                }
            }
            temp_points.Add(temp_points[0]);
            if (temp_polygon.showDistances)
            {
                temp_polygon.DisplayDistance(temp_points.Last());
                temp_polygon.CloseDistance();
            }
            temp_Vertices.Clear();
            creatingPolygon = false;

            if (triggerEvent)
            {
                if (!loaded)
                {
                    if (polygonEnded != null)
                    {
                        polygonEnded(this, temp_polygon);
                    }
                }
                else
                {
                    if (polygonLoaded != null)
                    {
                        polygonLoaded(this, temp_polygon);
                    }
                }
            }
        }

        /// <summary>
        /// Stops the creation of an open Polygon
        /// </summary>
        /// <param name="loaded">Whether the polygon was loaded</param>
        /// <param name="triggerEvent">Whether to trigger an event</param>
        public virtual void EndOpenPolygon(bool loaded = false, bool triggerEvent = true)
        {
            if (!isSite)
            {
                List<PolygonVertex> m_vertices = new List<PolygonVertex>();
                for (int i = 0; i < temp_Vertices.Count; i++)
                {
                    m_vertices.Add(temp_Vertices[i]);
                }
                if (temp_lineRenderer != null && temp_lineRenderer.gameObject != null)
                {
                    Destroy(temp_lineRenderer.gameObject);
                }
                temp_polygon.transform.position = temp_Vertices[0].currentPosition;
                temp_polygon.Initialize(m_vertices, false);
                if (temp_polygon.showDistances)
                {
                    temp_polygon.DiscardLastDistance();
                }
                for (int i = 0; i < m_vertices.Count; i++)
                {
                    m_vertices[i].transform.SetParent(temp_polygon.transform);
                }



                var cpui = temp_polygon[0].cpui;
                cpui.allignWhenShift = true;
                cpui.axisVertices = new List<GeometryVertex>() { temp_polygon[0], temp_polygon[1] };
                var cpui2 = temp_polygon.vertices.Last().cpui;
                cpui2.allignWhenShift = true;
                cpui2.axisVertices = new List<GeometryVertex>() { temp_polygon.vertices.Last(), temp_polygon[temp_polygon.Count - 2] };

                temp_Vertices.Clear();
                creatingPolygon = false;

                temp_polygon.UpdateVertexPositions();

                if (triggerEvent)
                {
                    if (!loaded)
                    {
                        if (polygonEnded != null)
                        {
                            polygonEnded(this, temp_polygon);
                        }
                    }
                    else
                    {
                        if (polygonLoaded != null)
                        {
                            polygonLoaded(this, temp_polygon);
                        }
                    }
                }
            }
            addPolygon.gameObject.SetActive(true);
            stopPolygon.gameObject.SetActive(false);
        }

        /// <summary>
        /// Stops the creation of an open polygon segment
        /// </summary>
        /// <param name="loaded">Whether the polygon segment was loaded</param>
        /// <param name="triggerEvent">Whether to trigger an event</param>
        public void EndOpenPolygonSegment(bool loaded = false, bool triggerEvent = true)
        {
            if (!isSite)
            {
                isPolygonSegment = false;
                List<PolygonVertex> m_vertices = new List<PolygonVertex>();
                for (int i = 0; i < temp_Vertices.Count; i++)
                {
                    m_vertices.Add(temp_Vertices[i]);
                }
                List<KeyValuePair<int, float>> m_param = new List<KeyValuePair<int, float>>();
                for (int i = 0; i < tempLengths.Count; i++)
                {
                    m_param.Add(tempLengths[i]);
                }
                if (temp_lineRenderer != null && temp_lineRenderer.gameObject != null)
                {
                    Destroy(temp_lineRenderer.gameObject);
                }
                if (temp_segment.showDistances)
                {
                    temp_segment.DiscardLastDistance();
                }
                temp_segment.transform.position = m_vertices[0].currentPosition;
                temp_segment.Initialize(m_vertices, basePolygon, m_param, false, false);
                for (int i = 0; i < m_vertices.Count; i++)
                {
                    m_vertices[i].transform.SetParent(temp_segment.transform);
                }
                temp_Vertices.Clear();
                tempLengths.Clear();
                creatingPolygon = false;

                tempVertex.DestroyVertex();
                tempCpui.DestroyUIRep(tempVertex.transform);
                Destroy(tempCpui.gameObject);
                addSegment.gameObject.SetActive(true);
                stopSegment.gameObject.SetActive(false);
                if (triggerEvent)
                {
                    if (!loaded)
                    {
                        if (polygonSegmentEnded != null)
                        {
                            polygonSegmentEnded(this, temp_segment);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Aborts the creation of a polygon
        /// </summary>
        /// <returns>IEnumerator</returns>
        public virtual IEnumerator AbortPolygon()
        {
            for (int i = 0; i < temp_Vertices.Count; i++)
            {
                Destroy(temp_Vertices[i].cpui.gameObject);
                Destroy(temp_Vertices[i].gameObject);
            }
            yield return new WaitForEndOfFrame();
            if (isPolygonSegment)
            {
                temp_segment.DiscardAllDistances();
                temp_segment = null;
            }
            else
            {
                temp_polygon.DiscardAllDistances();
                temp_polygon = null;
            }
            if (tempCpui != null)
            {
                Destroy(tempCpui.gameObject);
            }
            temp_points = null;
            temp_Vertices.Clear();
            creatingPolygon = false;
            addPolygon.gameObject.SetActive(true);
            stopPolygon.gameObject.SetActive(false);
            if (!isSite)
            {
                addSegment.gameObject.SetActive(true);
                stopSegment.gameObject.SetActive(false);
            }
        }

        /// <summary>
        /// Called when a polygon is deleted
        /// </summary>
        /// <param name="sender">The deleted polygon</param>
        public void OnPolygonDeleted(Polygon sender)
        {
            //sender.updated -= OnPolygonDeleted;
            sender.updated.RemoveListener(OnPolygonDeleted);
        }

        /// <summary>
        /// Undo of the last added point
        /// </summary>
        public void UndoPointAddition()
        {
            Destroy(temp_Vertices[temp_Vertices.Count - 1].gameObject);
            Destroy(temp_Vertices[temp_Vertices.Count - 1].cpui.gameObject);
            temp_Vertices.RemoveAt(temp_Vertices.Count - 1);
            temp_points.RemoveAt(temp_points.Count - 1);
            temp_polygon.UndoDistance();
        }

        /// <summary>
        /// Called when the first Control Point of the polygon is clicked again
        /// </summary>
        /// <param name="sender">The control point clicked</param>
        public void OnFisrtCPClicked(ControlPointUI sender)
        {
            if (isSite)
            {
                if (temp_Vertices != null && temp_Vertices.Count > 2)
                {
                    if (sender == temp_Vertices[0].cpui)
                    {
                        if (creatingPolygon)
                        {
                            EndClosedPolygon();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Sets the plane of the polygon
        /// </summary>
        /// <param name="origin">The origin of the plane</param>
        /// <param name="orientation">The orientation of the plane</param>
        public void SetPlane(Vector3 origin, DrawingPlane orientation)
        {
            switch (orientation)
            {
                case DrawingPlane.XY:
                    plane = new Plane(Vector3.forward, origin);
                    break;
                case DrawingPlane.XZ:
                    plane = new Plane(Vector3.up, origin);
                    break;
                case DrawingPlane.ZY:
                    plane = new Plane(Vector3.right, origin);
                    break;
                default:
                    plane = new Plane();
                    break;
            }
        }


        public void HidePolygon(bool show)
        {           
            for(int i=0; i<transform.childCount; i++)
            {
                transform.GetChild(i).gameObject.SetActive(!show);
                for(int j=0; j< transform.GetChild(i).GetComponent<Polygon>().vertices.Count; j++)
                {
                    transform.GetChild(i).GetComponent<Polygon>().vertices[j].gameObject.SetActive(!show);
                    transform.GetChild(i).GetComponent<Polygon>().vertices[j].cpui.gameObject.SetActive(!show);                    
                }
                
            }


        }

        public void ShowHideControlPoints(bool isactive)
        {
            for(int i=0; i<controlPointUiParent.childCount; i++)
            {
                if (!controlPointUiParent.GetChild(i).GetComponent<ControlPointUI>().belongsToSite)
                {
                    controlPointUiParent.GetChild(i).gameObject.SetActive(isactive);
                }
            }
        }

        #endregion

        #region Private Methods
        private void AddPolygonSegmentPoint(KeyValuePair<int, float> curveParameter)
        {
            if (temp_segment.showDistances && temp_segment.Count > 0)
            {
                temp_segment.CloseDistance();
            }
            tempVertex.Initialize(temp_Vertices.Count);
            tempVertex.UpdatePosition(tempVertex.transform.position);
            temp_Vertices.Add(tempVertex);
            temp_points.Add(tempVertex.currentPosition);
            tempLengths.Add(curveParameter);

            if (temp_segment.showDistances)
            {
                temp_segment.StartDistance(tempVertex.transform.position);
            }

            tempVertex = Instantiate(polygonVertexPrefab).GetComponent<PolygonVertex>();
        }

        private void AddPolygonSegmentPoint(KeyValuePair<int, float> curveParameter, Vector3 position)
        {
            if (temp_segment.showDistances && temp_segment.Count > 0)
            {
                temp_segment.CloseDistance();
            }
            tempVertex.Initialize(temp_Vertices.Count);
            tempVertex.transform.position = position;
            tempVertex.UpdatePosition(tempVertex.transform.position);
            temp_Vertices.Add(tempVertex);
            temp_points.Add(tempVertex.currentPosition);
            tempLengths.Add(curveParameter);

            if (temp_segment.showDistances)
            {
                temp_segment.StartDistance(tempVertex.transform.position);
            }

            tempVertex = Instantiate(polygonVertexPrefab).GetComponent<PolygonVertex>();
        }

        private void OnControlPointClicked(ControlPointUI cpui)
        {
            if (isPolygonSegment)
            {
                for (int i = 0; i < basePolygon.Count; i++)
                {
                    if (cpui == basePolygon[i].cpui)
                    {
                        PolygonVertex pVertex = Instantiate(polygonVertexPrefab).GetComponent<PolygonVertex>();
                        pVertex.transform.position = new Vector3(basePolygon[i].currentPosition.x, temp_Y, basePolygon[i].currentPosition.z);
                        pVertex.Initialize(temp_Vertices.Count);
                        ControlPointUI newCpui = Instantiate(controlPointUIPrefab, controlPointUiParent).GetComponent<ControlPointUI>();
                        cpui.Initialize(pVertex.transform);
                        pVertex.SetGUIElement(cpui);
                        pVertex.UpdatePosition(new Vector3(basePolygon[i].currentPosition.x, temp_Y, basePolygon[i].currentPosition.z));
                        temp_Vertices.Add(pVertex);
                        temp_points.Add(pVertex.currentPosition);
                    }
                }
            }
        }
        #endregion







    }
}

﻿using BrydenWoodUnity.DesignData;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BrydenWoodUnity.GeometryManipulation
{
    public class MoveObjects : Tagged<MoveObjects>
    {
        public LayerMask movables;
        public Camera cam;
        public Polygon constraint { get; set; }
        public Transform objectToMove { get; set; }
        public bool deleteObject { get; set; }

        private float dist;
        private Vector3 originalMousePos;
        private Vector3 mouseDelta;
        private Vector3 originalObjectPos;
        private Plane movementPlane;
        private float param = 0;


        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (!deleteObject)
                MoveObject();
        }

        public void SetObjectToMove(Transform objectToMove, Ray selectionRay)
        {
            this.objectToMove = objectToMove;
            this.objectToMove.GetComponent<Core>().Select();
            originalObjectPos = objectToMove.position;
            movementPlane = new Plane(Vector3.up, originalObjectPos);
            movementPlane.Raycast(selectionRay, out dist);
            originalMousePos = selectionRay.GetPoint(dist);
        }

        public void DeleteObject()
        {
            objectToMove.GetComponent<Core>().Deselect();
            objectToMove.GetComponent<Core>().OnDelete();
            param = 0;
            objectToMove = null;
        }

        private void MoveObject()
        {
            if (objectToMove != null)
            {
                if (Input.GetMouseButton(0))
                {
                    Ray r = cam.ScreenPointToRay(Input.mousePosition);
                    movementPlane.Raycast(r, out dist);
                    mouseDelta = r.GetPoint(dist) - originalMousePos;
                    objectToMove.position = originalObjectPos + mouseDelta;
                    if (constraint != null)
                    {
                        Vector3 pos = constraint.PointOnCurve(objectToMove.position, out param);
                        pos = new Vector3(pos.x, objectToMove.position.y, pos.z);
                        objectToMove.position = pos;//constraint.PointOnCurve(objectToMove.position, out param) + Vector3.up * objectToMove.localScale.y * 0.5f;
                    }
                }

                if (Input.GetMouseButtonUp(0))
                {
                    if (!deleteObject)
                    {
                        objectToMove.GetComponent<Core>().Deselect();
                        objectToMove.GetComponent<Core>().OnMoved(param);
                        param = 0;
                        objectToMove = null;
                    }
                }
            }
        }
    }
}

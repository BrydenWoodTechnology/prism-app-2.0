﻿using BrydenWoodUnity.UIElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BrydenWoodUnity.GeometryManipulation
{
    /// <summary>
    /// Used when a vertex has been moved
    /// </summary>
    /// <param name="sender">The moved vertex</param>
    public delegate void GeometryVertexMoved(GeometryVertex sender);

    /// <summary>
    /// Struct used to describe the relation between polygon vertices which belong to the same edge
    /// </summary>
    public struct EdgeRelation
    {
        public List<PolygonVertex> vertices;
        public List<Vector3> originalDifs;
    }

    /// <summary>
    /// Struct used to describe the relation between mesh vertices which belong to the same edge
    /// </summary>
    public struct MeshEdgeRelation
    {
        public List<MeshVertex> vertices;
        public List<Vector3> originalDifs;
    }

    /// <summary>
    /// A base class for geometry vertices that are editable
    /// </summary>
    public class GeometryVertex : MonoBehaviour
    {
        #region Public Propeties
        /// <summary>
        /// The distance threshold for snapping to other vertices
        /// </summary>
        public float snapThreshold = 1.0f;
        /// <summary>
        /// The initial world position of the vertex component
        /// </summary>
        public Vector3 initialPosition { get; set; }
        /// <summary>
        /// The current world position of the Vertex
        /// </summary>
        public Vector3 currentPosition
        {
            get
            {
                return transform.position;
            }
            set
            {
                transform.position = value;
            }
        }
        /// <summary>
        /// The current local position of the vertex
        /// </summary>
        public Vector3 currentLocalPosition { get; set; }
        /// <summary>
        /// The index of this vertex
        /// </summary>
        public int index { get; set; }
        /// <summary>
        /// The UI element which controls this vertex
        /// </summary>
        public ControlPointUI cpui { get; set; }
        /// <summary>
        /// The group of vertices to be controlled together with this
        /// </summary>
        public List<GeometryVertex> group { get; set; }
        /// <summary>
        /// The initial deltas between this vertex and its group
        /// </summary>
        public List<Vector3> groupDiffs { get; set; }
        #endregion

        private bool moving = false;
        private Vector3 prevPos;
        private float delay = 500;
        private DateTime stopMoving;
        private bool triggerStopMoving;

        #region Events
        /// <summary>
        /// Triggered when the vertex has been moved
        /// </summary>
        public event GeometryVertexMoved vertexMoved;
        /// <summary>
        /// Triggered when the vertex has stopped moving
        /// </summary>
        public event GeometryVertexMoved stoppedMoving;
        /// <summary>
        /// Called when the vertex is being moved in order to trigger the corresponding event
        /// </summary>
        /// <param name="sender">The sender of the event</param>
        public virtual void OnMoved(object sender)
        {
            currentPosition = transform.position;
            if (vertexMoved != null)
            {
                vertexMoved(this);
            }
            if (moved != null)
            {
                moved(this);
            }
            if (!moving) moving = true;
        }
        /// <summary>
        /// Triggered when the vertex has been destroyed
        /// </summary>
        public static event GeometryVertexMoved destroyed;
        /// <summary>
        /// Called when the vertex is being moved in order to trigger the corresponding event
        /// </summary>
        public void OnMoved()
        {
            OnMoved(this);
        }
        /// <summary>
        /// Triggered when the vertex has been moved
        /// </summary>
        public static event GeometryVertexMoved moved;
        #endregion

        #region MonoBehaviour Methods
        private void Start()
        {
            currentPosition = transform.position;
        }

        private void Update()
        {

        }

        private void LateUpdate()
        {
            if (moving)
            {
                if (currentPosition == prevPos)
                {
                    moving = false;
                    triggerStopMoving = true;
                    stopMoving = DateTime.Now;
                }
                prevPos = currentPosition;
            }

            if (triggerStopMoving && ((DateTime.Now-stopMoving).TotalMilliseconds>delay))
            {
                triggerStopMoving = false;
                if (stoppedMoving!=null)
                {
                    stoppedMoving(this);
                }
            }
        }

        private void OnEnable()
        {
            if (cpui != null)
            {
                cpui.gameObject.SetActive(true);
            }
        }

        private void OnDisable()
        {
            if (currentPosition != transform.position)
            {
                currentPosition = transform.position;
            }

            if (cpui != null && (cpui.sceneObjects.Count == 1 && cpui.sceneObjects[0] == this))
            {
                cpui.gameObject.SetActive(false);
            }
        }

        protected virtual void OnDestroy()
        {
            if (cpui != null)
            {
                cpui.DestroyUIRep(transform);
                cpui.moved -= OnMoved;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Initializes the Instance
        /// </summary>
        /// <param name="index">The index of the vertex</param>
        public virtual void Initialize(int index)
        {
            this.index = index;
            initialPosition = transform.position;
            currentPosition = transform.position;
            currentLocalPosition = transform.localPosition;
        }

        /// <summary>
        /// Checks if the Vertex Moved event has a given delegate registered
        /// As taken from https://stackoverflow.com/questions/136975/has-an-event-handler-already-been-added
        /// </summary>
        /// <param name="prospectiveHandler">The delegate to check against</param>
        /// <returns>Boolean</returns>

        public bool IsEventHandlerRegisteredMoved(Delegate prospectiveHandler)
        {
            if (vertexMoved != null)
            {
                foreach (Delegate existingHandler in vertexMoved.GetInvocationList())
                {
                    if (existingHandler == prospectiveHandler)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Checks whether aan event handler is registered to the StoppedMoving Event
        /// As taken from https://stackoverflow.com/questions/136975/has-an-event-handler-already-been-added
        /// </summary>
        /// <param name="prospectiveHandler">The event handler</param>
        /// <returns></returns>
        public bool IsEventHandlerRegisteredStopped(Delegate prospectiveHandler)
        {
            if (stoppedMoving != null)
            {
                foreach (Delegate existingHandler in stoppedMoving.GetInvocationList())
                {
                    if (existingHandler == prospectiveHandler)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Sets the corresponding GUI element
        /// </summary>
        /// <param name="cpui">The new element</param>
        public virtual void SetGUIElement(ControlPointUI cpui)
        {
            this.cpui = cpui;
            this.cpui.moved += OnMoved;
        }

        /// <summary>
        /// Removes the current GUI element
        /// </summary>
        public virtual void RemoveGUIElement()
        {
            cpui = null;
            cpui.moved -= OnMoved;
        }

        /// <summary>
        /// Called to destroy the vertex
        /// </summary>
        public virtual void DestroyVertex()
        {
            if (cpui != null)
            {
                cpui.DestroyUIRep(transform);
            }
            if (destroyed != null)
            {
                destroyed(this);
            }
            Destroy(gameObject);
        }

        /// <summary>
        /// Sets the global position of the vertex
        /// </summary>
        /// <param name="pos">The new position</param>
        /// <param name="moveGroup">Whether the vertices in each group should update as well</param>
        public virtual void SetPosition(Vector3 pos, bool moveGroup = false)
        {
            try
            {
                transform.position = pos;
                currentPosition = pos;
                currentLocalPosition = transform.localPosition;
                if (cpui != null)
                {
                    cpui.UpdatePositions(transform);
                }
                if (moveGroup)
                {
                    if (group != null)
                    {
                        for (int i = 0; i < group.Count; i++)
                        {
                            if (group[i] != null)
                            {
                                group[i].UpdatePosition(currentPosition + groupDiffs[i]);
                                group[i].OnMoved(group[i]);
                            }
                        }
                    }
                }
                OnMoved(this);
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }
        }

        /// <summary>
        /// Sets the local position of the vertex
        /// </summary>
        /// <param name="pos">The new local position</param>
        /// <param name="moveGroup">Whether the vertices in each group should update as well</param>
        public virtual void SetLocalPosition(Vector3 pos, bool moveGroup = false)
        {
            transform.localPosition = pos;
            currentPosition = transform.position;
            currentLocalPosition = transform.localPosition;
            if (cpui != null)
            {
                cpui.UpdatePositions(transform);
            }
            if (moveGroup)
            {
                for (int i = 0; i < group.Count; i++)
                {
                    group[i].UpdatePosition(currentPosition + groupDiffs[i]);
                    group[i].OnMoved(group[i]);
                }
            }
            OnMoved(this);
        }

        /// <summary>
        /// Updates the global position of the vertex (without triggering an event)
        /// </summary>
        /// <param name="pos">The new position</param>
        public virtual void UpdatePosition(Vector3 pos)
        {
            transform.position = pos;
            currentPosition = pos;
            currentLocalPosition = transform.localPosition;
        }

        public virtual void UpdateLocalPosition(Vector3 pos)
        {
            transform.localPosition = pos;
            currentLocalPosition = pos;
            currentPosition = transform.position;
        }

        /// <summary>
        /// Updates the position of the vertex
        /// </summary>
        public virtual void UpdatePosition()
        {
            currentPosition = transform.position;
            currentLocalPosition = transform.localPosition;
        }

        /// <summary>
        /// Sets the group of the vertex
        /// </summary>
        /// <param name="vertices">The list of vertices from which the closest ones will create its group</param>
        public virtual void SetGroup(List<GeometryVertex> vertices)
        {
            group = new List<GeometryVertex>();
            groupDiffs = new List<Vector3>();
            vertices.RemoveAll(item => item == null);
            for (int i = 0; i < vertices.Count; i++)
            {
                if (vertices[i] != this && vertices[i] != null)
                {
                    if (Vector3.Distance(currentPosition, vertices[i].currentPosition) < 0.1f)
                    {
                        group.Add(vertices[i]);
                        groupDiffs.Add(vertices[i].currentPosition - currentPosition);
                    }
                }
            }
        }

        /// <summary>
        /// Adds a geometry vertex to the group that this belongs
        /// </summary>
        /// <param name="vertex">The geometry vertex to be added</param>
        public void AddToGroup(GeometryVertex vertex)
        {
            if (group != null && groupDiffs != null)
            {
                group.Add(vertex);
                groupDiffs.Add(vertex.currentPosition - currentPosition);
            }
        }
        #endregion

        
    }
}

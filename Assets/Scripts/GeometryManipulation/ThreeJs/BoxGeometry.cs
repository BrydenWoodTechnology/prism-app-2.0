﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrydenWoodUnity.GeometryManipulation.ThreeJs
{
    public class BoxGeometry : Geometry
    {
        public float[] parameters;

        public BoxGeometry(float width, float height, float depth)
        {
            parameters = new float[6];
            parameters[0] = width;
            parameters[1] = height;
            parameters[2] = depth;
            parameters[3] = 1;
            parameters[4] = 1;
            parameters[5] = 1;
            uuid = Guid.NewGuid().ToString();
            type = "BoxGeometry";
            data = new Data();
        }
    }
}

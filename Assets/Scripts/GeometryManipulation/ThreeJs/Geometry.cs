﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrydenWoodUnity.GeometryManipulation.ThreeJs
{
    [Serializable]
    public class Geometry
    {
        public string uuid;
        public string type;
        public Data data;

        public Geometry()
        {
            type = "Geometry";
            data = new Data();
            uuid = Guid.NewGuid().ToString();
        }
    }

    [Serializable]
    public struct Vector3
    {
        public float x, y, z;
        public Vector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }

    [Serializable]
    public struct Euler
    {
        public float x, y, z;
        public Euler(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrydenWoodUnity.GeometryManipulation.ThreeJs
{
    [Serializable]
    public class Data
    {
        public double[] vertices;
        public double[] normals;
        public double[] uvs;
        public int[] faces;

        public Data()
        {
            vertices = new double[0];
            normals = new double[0];
            uvs = new double[0];
            faces = new int[0];
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrydenWoodUnity.GeometryManipulation.ThreeJs
{
    public class Mesh : Object3d
    {
        public Mesh() : base()
        {
            type = "Mesh";
        }
    }
}

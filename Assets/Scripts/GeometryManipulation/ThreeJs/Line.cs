﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrydenWoodUnity.GeometryManipulation.ThreeJs
{
    [Serializable]
    public class Line : Object3d
    {
        public Line() : base()
        {
            type = "Line";
        }
    }
}

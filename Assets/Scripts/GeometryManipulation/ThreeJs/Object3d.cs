﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace BrydenWoodUnity.GeometryManipulation.ThreeJs
{
    [Serializable]
    public class Object3d
    {
        public Vector3 position;
        public Euler rotation;
        public Vector3 scale;
        public string geometry;
        public string material;
        public string type;
        public List<Object3d> children;
        public Dictionary<string, string> userData;
        [JsonIgnore]
        public Geometry _geometry;

        public Object3d()
        {
            position = new Vector3(0, 0, 0);
            rotation = new Euler(0, 0, 0);
            scale = new Vector3(1, 1, 1);
            type = "object3d";
            children = new List<Object3d>();
            userData = new Dictionary<string, string>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BrydenWoodUnity.GeometryManipulation
{
    /// <summary>
    /// A MonoBehaviour component for an Extrusion
    /// </summary>
    [RequireComponent(typeof(MeshFilter))]
    public class Extrusion : MonoBehaviour, ISelectableGeometry
    {
        #region Public Fields
        /// <summary>
        /// The mesh filter which will be used for the mesh
        /// </summary>
        [Header("Unity specific fields:")]
        public MeshFilter meshFilter;
        /// <summary>
        /// Whether the resulting mesh should be flipped
        /// </summary>
        public bool flip = false;

        /// <summary>
        /// The total height of the extrusion
        /// </summary>
        [Space(10)]
        [Header("Geometry specific fields:")]
        [Tooltip("The total height of the polygon (read-only)")]
        public float totalHeight;
        /// <summary>
        /// The total facade of the extrusion
        /// </summary>
        [Tooltip("The facade area of the polygon (read-only)")]
        public float totalFacade;

        [Tooltip("Whether the extrusion should be capped or not")]
        public bool capped = true;

        [Tooltip("An offset for the reversed vertical faces in order to avoid Z-fighting")]
        public float reversedOffset = 0.02f;

        [Space(10)]
        [Header("Materials and Prefabs")]
        public Material vertexColor;

        /// <summary>
        /// The polygon which is being extruded
        /// </summary>
        [HideInInspector]
        public Polygon polygon;
        /// <summary>
        /// The editable mesh that is being extruded
        /// </summary>
        [HideInInspector]
        public EditableMesh editableMesh;
        /// <summary>
        /// The overall mesh of the polygon
        /// </summary>
        [HideInInspector]
        public Mesh overallMesh;
        /// <summary>
        /// The top face of the extrusion
        /// </summary>
        [HideInInspector]
        public Mesh topFace;
        /// <summary>
        /// A List including the vertical faces of the extrusion
        /// </summary>
        [HideInInspector]
        public List<Mesh> verticalFaces;
        [HideInInspector]
        public int[] sidesToNotExtrude;

        [HideInInspector]
        public List<Mesh> reverseVerticalFaces;

        public bool convex = true;
        public bool splitMesh = false;
        #endregion

        #region Private Fields
        private GameObject m_gameObject;
        #endregion

        #region Events
        /// <summary>
        /// Triggered when the extrusion is selected
        /// </summary>
        public event OnSelectGeometry selected;
        /// <summary>
        /// Triggered when the extrusion is deselected
        /// </summary>
        public event OnSelectGeometry deselected;
        #endregion

        #region MonoBehaviour Methods
        private void Start()
        {

        }

        private void Update()
        {

        }

        /// <summary>
        /// Called when the gameObject is destroyed
        /// </summary>
        public void OnDestroy()
        {
            if (editableMesh != null)
            {
                editableMesh.updated -= OnEditableMeshUpdated;
            }
            if (polygon != null)
            {
                //polygon.updated -= OnPolygonUpdated;
                polygon.updated.RemoveListener(OnPolygonUpdated);
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Initializes the Instance
        /// </summary>
        /// <param name="polygon">The polygon to be extruded</param>
        public void Initialize(Polygon polygon, bool splitMesh = false)
        {
            this.splitMesh = splitMesh;
            this.polygon = polygon;
            transform.localPosition = Vector3.zero;
            //this.polygon.updated += OnPolygonUpdated;
            this.polygon.updated.AddListener(OnPolygonUpdated);
            this.polygon.extrusion = this;
            GetComponent<MeshRenderer>().material = polygon.gameObject.GetComponent<MeshRenderer>().material;

            GenerateMeshes();
        }

        /// <summary>
        /// Initializes the Instance
        /// </summary>
        /// <param name="editableMesh">The mesh to be extruded</param>
        public void Initialize(EditableMesh editableMesh)
        {
            this.editableMesh = editableMesh;
            transform.localPosition = Vector3.zero;
            this.editableMesh.updated += OnEditableMeshUpdated;
            GetComponent<MeshRenderer>().material = editableMesh.gameObject.GetComponent<MeshRenderer>().material;
            GenerateExtrusionMeshes();
        }

        /// <summary>
        /// Called when the extrusion is selected
        /// </summary>
        public void Select()
        {
            if (selected != null)
            {
                selected();
            }
        }

        /// <summary>
        /// Called in order to deselect the extrusion
        /// </summary>
        public void DeSelect()
        {
            if (deselected != null)
            {
                deselected();
            }
        }

        /// <summary>
        /// Sets the vertex colors of the extrusion
        /// </summary>
        /// <param name="color">The vertex color</param>
        public void SetVertexColors(Color color)
        {
            Color[] colours = new Color[GetComponent<MeshFilter>().sharedMesh.vertexCount];
            for (int i = 0; i < colours.Length; i++)
            {
                colours[i] = color;
            }
            GetComponent<MeshFilter>().sharedMesh.colors = colours;
        }

        /// <summary>
        /// Sets the display of the extrusion in Standard Mode
        /// </summary>
        public void DisplayStandardMode()
        {
            GenerateMeshes();
        }

        /// <summary>
        /// Generates the Mesh Extrusions
        /// </summary>
        public void GenerateExtrusionMeshes()
        {
            Mesh mesh = editableMesh.mesh;
            List<CombineInstance> m_instances = new List<CombineInstance>();

            List<Vector3> bottomVerts = new List<Vector3>();
            for (int i = 0; i < editableMesh.outterEdges.Count; i++)
            {
                var edge = editableMesh.outterEdges[i];
                bottomVerts.Add(mesh.vertices[edge.v1]);
            }

            bool clockWise = Polygon.IsClockWise(bottomVerts);

            if (!clockWise && capped)
            {
                flip = true;
            }

            if (capped)
            {
                Vector3[] topVerts = new Vector3[mesh.vertexCount];
                for (int i = 0; i < topVerts.Length; i++)
                {
                    topVerts[i] = mesh.vertices[i] + new Vector3(0, totalHeight, 0);
                }
                Mesh topMesh = new Mesh();
                topMesh.vertices = topVerts;
                int[] topTriangles = new int[mesh.triangles.Length];
                if (clockWise)
                {
                    for (int i = 0; i < mesh.triangles.Length; i += 3)
                    {
                        topTriangles[i] = mesh.triangles[i];
                        topTriangles[i + 1] = mesh.triangles[i + 1];
                        topTriangles[i + 2] = mesh.triangles[i + 2];
                    }
                }
                else
                {
                    for (int i = 0; i < mesh.triangles.Length; i += 3)
                    {
                        topTriangles[i] = mesh.triangles[i + 2];
                        topTriangles[i + 1] = mesh.triangles[i + 1];
                        topTriangles[i + 2] = mesh.triangles[i];
                    }
                }
                topMesh.triangles = topTriangles;
                topMesh.colors = mesh.colors;
                m_instances.Add(new CombineInstance() { mesh = topMesh, transform = Matrix4x4.identity });
            }

            verticalFaces = new List<Mesh>();
            for (int i = 0; i < editableMesh.outterEdges.Count; i++)
            {
                var edge = editableMesh.outterEdges[i];
                Mesh verticalFace = new Mesh();
                Vector3[] verts = new Vector3[]
                {
                    mesh.vertices[edge.v1],
                    mesh.vertices[edge.v2],
                    mesh.vertices[edge.v2] + new Vector3(0,totalHeight,0),
                    mesh.vertices[edge.v1] + new Vector3(0,totalHeight,0),
                };
                verticalFace = GeometryHelper.GetMeshFrom4Points(verts, flip, Color.white);
                verticalFaces.Add(verticalFace);
                m_instances.Add(new CombineInstance() { mesh = verticalFace, transform = Matrix4x4.identity });
                if (verts[0].x != verts[3].x || verts[0].z != verts[3].z)
                {
                    Debug.Log("Verts issue!");
                }
                if (verts[1].x != verts[2].x || verts[1].z != verts[2].z)
                {
                    Debug.Log("Verts issue!");
                }
            }

            if (!capped)
            {
                verticalFaces = new List<Mesh>();

                for (int i = 0; i < editableMesh.outterEdges.Count; i++)
                {
                    var edge = editableMesh.outterEdges[i];
                    Vector3 offsetVector = Vector3.Cross((mesh.vertices[edge.v2] - mesh.vertices[edge.v1]), Vector3.up);
                    if (clockWise)
                    {
                        offsetVector *= -1;
                    }
                    Mesh verticalFace = new Mesh();
                    Vector3[] verts = new Vector3[]
                    {
                    mesh.vertices[edge.v1] + offsetVector.normalized*reversedOffset,
                    mesh.vertices[edge.v2] + offsetVector.normalized*reversedOffset,
                    mesh.vertices[edge.v2] + new Vector3(0,totalHeight,0)+ offsetVector.normalized*reversedOffset,
                    mesh.vertices[edge.v1] + new Vector3(0,totalHeight,0)+ offsetVector.normalized*reversedOffset,
                    };
                    verticalFace = GeometryHelper.GetMeshFrom4Points(verts, true, Color.white);
                    verticalFaces.Add(verticalFace);
                    m_instances.Add(new CombineInstance() { mesh = verticalFace, transform = Matrix4x4.identity });
                    if (verts[0].x != verts[3].x || verts[0].z != verts[3].z)
                    {
                        Debug.Log("Verts issue!");
                    }
                    if (verts[1].x != verts[2].x || verts[1].z != verts[2].z)
                    {
                        Debug.Log("Verts issue!");
                    }
                }
            }

            overallMesh = new Mesh();
            overallMesh.Clear();
            overallMesh.CombineMeshes(m_instances.ToArray(), true, true);
            Vector2[] m_uvs = new Vector2[overallMesh.vertexCount];
            for (int i = 0; i < m_uvs.Length; i++)
            {
                m_uvs[i] = new Vector2(overallMesh.vertices[i].x, overallMesh.vertices[i].y);
            }
            overallMesh.uv = m_uvs;
            overallMesh.uv2 = m_uvs;
            overallMesh.uv3 = m_uvs;
            overallMesh.uv4 = m_uvs;
            UpdateMeshComponents();
        }

        /// <summary>
        /// Generates the polygon extrusions
        /// </summary>
        public void GenerateMeshes()
        {
            List<CombineInstance> m_instances = new List<CombineInstance>();

            //------Creating the lists of bottom and top vertices----//
            List<Vector3> topVertices = new List<Vector3>();
            List<Vector3> bottomVertices = new List<Vector3>();
            if (polygon != null && polygon.mesh != null)
            {
                for (int i = 0; i < polygon.mesh.vertices.Length; i++)
                {
                    bottomVertices.Add(polygon.mesh.vertices[i] - new Vector3(0, 0.01f, 0));
                    topVertices.Add(bottomVertices[i] + new Vector3(0, totalHeight, 0));
                }
            }

            bool clockWise = Polygon.IsClockWise(bottomVertices);

            if (!clockWise && capped)
            {
                flip = true;
            }

            verticalFaces = new List<Mesh>();
            for (int i = 0; i < bottomVertices.Count; i++)
            {
                if (sidesToNotExtrude!=null && sidesToNotExtrude.Length>0)
                {
                    if (sidesToNotExtrude.Contains(i))
                    {
                        continue;
                    }
                }
                Mesh verticalFace = new Mesh();
                int nextIndex = (i + 1) % bottomVertices.Count;
                Vector3[] verts = new Vector3[]
                {
                    bottomVertices[i],
                    bottomVertices[nextIndex],
                    topVertices[nextIndex],
                    topVertices[i]
                };
                verticalFace = GeometryHelper.GetMeshFrom4Points(verts, flip, Color.white);
                verticalFaces.Add(verticalFace);
                m_instances.Add(new CombineInstance() { mesh = verticalFace, transform = Matrix4x4.identity });
            }

            if (!capped)
            {
                verticalFaces = new List<Mesh>();
                var offsetBottom = OffsetPolyline(bottomVertices.ToArray(), reversedOffset);
                var offsetTop = OffsetPolyline(topVertices.ToArray(), reversedOffset);

                for (int i = 0; i < bottomVertices.Count; i++)
                {
                    if (sidesToNotExtrude != null && sidesToNotExtrude.Length > 0)
                    {
                        if (sidesToNotExtrude.Contains(i))
                        {
                            continue;
                        }
                    }
                    Mesh verticalFace = new Mesh();
                    int nextIndex = (i + 1) % bottomVertices.Count;
                    Vector3[] verts = new Vector3[]
                    {
                        offsetBottom[i],
                        offsetBottom[nextIndex],
                        offsetTop[nextIndex],
                        offsetTop[i]
                    };
                    verticalFace = GeometryHelper.GetMeshFrom4Points(verts, true, Color.white);
                    verticalFaces.Add(verticalFace);
                    m_instances.Add(new CombineInstance() { mesh = verticalFace, transform = Matrix4x4.identity });
                }
            }

            if (capped)
            {
                //-----------Generating the mesh of the top face of the extrusion----//
                topFace = new Mesh();
                topFace.vertices = topVertices.ToArray();
                int[] tris = new int[polygon.mesh.triangles.Length];
                for (int i = 0; i < tris.Length; i += 3)
                {
                    tris[i] = polygon.mesh.triangles[i];
                    tris[i + 1] = polygon.mesh.triangles[i + 1];
                    tris[i + 2] = polygon.mesh.triangles[i + 2];
                }
                topFace.triangles = tris;
                topFace.colors = polygon.mesh.colors;
                m_instances.Add(new CombineInstance() { mesh = topFace, transform = Matrix4x4.identity });
            }

            overallMesh = new Mesh();
            overallMesh.Clear();
            overallMesh.CombineMeshes(m_instances.ToArray(), true, true);

            if (splitMesh)
            {
                int[] firstMeshTris;
                int[] secondMeshTris;
                BrydenWoodUtils.Split(overallMesh.triangles, overallMesh.triangles.Length - polygon.mesh.triangles.Length, out firstMeshTris, out secondMeshTris);
                overallMesh.subMeshCount = 2;
                overallMesh.SetTriangles(firstMeshTris, 0);
                overallMesh.SetTriangles(secondMeshTris, 1);
                if (GetComponent<MeshRenderer>().materials.Length != 2)
                    GetComponent<MeshRenderer>().materials = new Material[] { GetComponent<MeshRenderer>().material, GetComponent<MeshRenderer>().material };
            }
            //Vector2[] m_uvs = new Vector2[overallMesh.vertexCount];
            //for (int i = 0; i < m_uvs.Length; i++)
            //{
            //    m_uvs[i] = new Vector2(overallMesh.vertices[i].x, overallMesh.vertices[i].y);
            //}
            //overallMesh.uv = m_uvs;
            //overallMesh.uv2 = m_uvs;
            //overallMesh.uv3 = m_uvs;
            //overallMesh.uv4 = m_uvs;
            UpdateMeshComponents();

        }
        #endregion

        #region Private Methods
        private Vector3[] OffsetPolyline(Vector3[] vertices, float distance)
        {
            Vector3[] offsetVerts = new Vector3[vertices.Length];

            for (int i = 0; i < vertices.Length; i++)
            {
                int prevIndex = i - 1;
                if (prevIndex < 0)
                {
                    prevIndex = vertices.Length - 1;
                }
                int nextIndex = (i + 1) % vertices.Length;

                Vector3 a = vertices[nextIndex] - vertices[i];
                Vector3 b = vertices[prevIndex] - vertices[i];
                Vector3 c = a.normalized + b.normalized;
                c.Normalize();

                offsetVerts[i] = vertices[i] + c * distance;
            }

            return offsetVerts;
        }

        private void OnPolygonUpdated(Polygon sender)
        {
            GenerateMeshes();
        }

        private void OnEditableMeshUpdated(EditableMesh sender)
        {
            GenerateExtrusionMeshes();
        }

        private void UpdateMeshComponents()
        {
            overallMesh.RecalculateNormals();
            overallMesh.RecalculateBounds();
            meshFilter.sharedMesh = overallMesh;
            try
            {
                Destroy(GetComponent<MeshCollider>());
                if(gameObject.activeInHierarchy)
                    StartCoroutine(DelayUpdate());
                //GetComponent<MeshCollider>().sharedMesh = meshFilter.sharedMesh;
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }
            //if (convex)
            //{
            //    GetComponent<MeshCollider>().inflateMesh = true;
            //    GetComponent<MeshCollider>().convex = convex;
            //}
        }
        private IEnumerator DelayUpdate()
        {
            yield return new WaitForEndOfFrame();
            if(gameObject.GetComponent<MeshCollider>()==null)gameObject.AddComponent<MeshCollider>();
        }

        #endregion
    }

  
}

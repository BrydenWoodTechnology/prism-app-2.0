﻿using BrydenWoodUnity.UIElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.GeometryManipulation
{
    /// <summary>
    /// A MonoBehaviour component for a Polygon Segment (inherits from Polygon)
    /// </summary>
    public class PolygonSegment : Polygon
    {
        #region Public Fields and Properties
        public Polygon basePolygon { get; set; }
        [HideInInspector]
        public List<KeyValuePair<int, float>> curveLengths;
        #endregion

        #region Private Fields and Properties
        private bool isInitialized { get; set; }
        #endregion

        #region MonoBehaviour Methods
        void Start()
        {
            if (meshFilter == null)
            {
                meshFilter = GetComponent<MeshFilter>();
            }
            if (showDistances)
            {
                if (distancesMaterial == null)
                {
                    distancesMaterial = new Material(Shader.Find("Standard"));
                    //distancesMaterial.color = Color.red;
                    distancesMaterial.color = new Color(0.93f, 0.15f, 0.4f, 1f);
                }
                if (distances == null)
                {
                    distances = new List<GameObject>();
                    distancesParent = new GameObject("DistancesParent").transform;
                    distancesParent.SetParent(transform);
                    distancesUI = new List<Text>();
                    distanceUIParent = new GameObject("DistancesUIParent").transform;
                    distanceUIParent.SetParent(GameObject.Find("Canvas").transform);
                    distanceUIParent.SetAsFirstSibling();
                }
            }
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnDestroy()
        {
            if (this.vertices != null && this.vertices.Count != 0)
            {
                for (int i = 0; i < this.vertices.Count; i++)
                {
                    this.vertices[i].vertexMoved -= UpdateGeometry;
                    if (i == 0 || i == vertices.Count - 1)
                    {
                        vertices[i].vertexMoved += UpdateCurveLengths;
                    }
                    if (vertices[i] != null)
                    {
                        Destroy(vertices[i].gameObject);
                    }
                }
            }
            if (distances != null && distances.Count > 0)
            {
                for (int i = 0; i < distances.Count; i++)
                {
                    Destroy(distances[i].gameObject);
                    Destroy(distancesUI[i].gameObject);
                }
            }
            if (basePolygon != null)
            {
                //basePolygon.updated -= OnBasePolygonUpdated;
                basePolygon.updated.AddListener(OnBasePolygonUpdated);
            }
        }

        void OnEnable()
        {
            for (int i = 0; i < vertices.Count; i++)
            {
                vertices[i].gameObject.SetActive(true);
            }

            if (distances != null && distances.Count > 0)
            {
                for (int i = 0; i < distances.Count; i++)
                {
                    distances[i].gameObject.SetActive(true);
                    distancesUI[i].gameObject.SetActive(true);
                }
            }
        }

        void OnDisable()
        {
            for (int i = 0; i < vertices.Count; i++)
            {
                vertices[i].gameObject.SetActive(false);
            }

            if (distances != null && distances.Count > 0)
            {
                for (int i = 0; i < distances.Count; i++)
                {
                    distances[i].gameObject.SetActive(false);
                    distancesUI[i].gameObject.SetActive(false);
                }
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Initializes the Instance
        /// </summary>
        /// <param name="vertices">The list of Polygon Vertices</param>
        /// <param name="basePolygon">The base polygon</param>
        /// <param name="parameters">The list of base polygon segment indices and their parameters (0-1)</param>
        /// <param name="closed">Whether the polygon segment is closed</param>
        /// <param name="fill">Whether the polygon segment has a fill</param>
        /// <param name="isBuildingVertex">Whether the vertices of the polygon belong to a building</param>
        public void Initialize(List<PolygonVertex> vertices, Polygon basePolygon, List<KeyValuePair<int, float>> parameters, bool closed = true, bool fill = false, bool isBuildingVertex = true)
        {
            isInitialized = false;
            this.basePolygon = basePolygon;
            //basePolygon.updated += OnBasePolygonUpdated;
            basePolygon.updated.AddListener(OnBasePolygonUpdated);
            curveLengths = parameters;
            this.vertices = vertices;
            OrganizeVertices(ref curveLengths);
            base.Initialize(vertices, closed, fill, isBuildingVertex);
            lineWidth = lineWidth * 2;
            DiscardAllDistances();
            ToggleDistances(true);
            isInitialized = true;
        }
        #endregion

        #region Private Properties
        private void UpdateCurveLengths(object sender)
        {
            var keys = curveLengths.Select(x => x.Key).ToList();

            for (int i = 0; i < curveLengths.Count; i++)
            {
                if (i == 0 || i == curveLengths.Count - 1)
                {
                    Vector3 baseVector = basePolygon.vertices[(curveLengths[i].Key + 1)].currentPosition - basePolygon.vertices[curveLengths[i].Key].currentPosition;
                    Vector3 difVector = vertices[i].currentPosition - basePolygon.vertices[curveLengths[i].Key].currentPosition;
                    Vector3 proj = Vector3.Project(difVector, baseVector);
                    float temp_ratio = proj.magnitude / baseVector.magnitude;
                    var dot = Vector3.Dot(baseVector, proj);
                    curveLengths[i] = new KeyValuePair<int, float>(curveLengths[i].Key, temp_ratio);
                }
            }
        }

        /// <summary>
        /// Called when a geometry vertex has moved
        /// </summary>
        /// <param name="vertex">The geometry vertex which moved</param>
        protected override void UpdateGeometry(GeometryVertex vertex)
        {
            base.UpdateGeometry(vertex);
        }

        private void UpdateVertices()
        {
            List<KeyValuePair<int, float>> curveParameters = new List<KeyValuePair<int, float>>();

            List<int> keys = new List<int>();
            List<float> values = new List<float>();

            List<float> nums = new List<float>();

            for (int i = 0; i < curveLengths.Count; i++)
            {
                nums.Add(curveLengths[i].Key + (float)Math.Round(curveLengths[i].Value, 3));
            }

            nums.Sort();

            List<double> resultNums = new List<double>();
            List<double> copyNums = new List<double>();

            for (int i = 0; i < nums.Count - 1; i++)
            {
                if (nums[i] % 1 != 0)
                {
                    copyNums.Add(nums[i]);
                    while (Math.Floor(copyNums.Last() + 1) <= nums[i + 1])
                    {
                        copyNums.Add(Math.Floor(copyNums.Last() + 1));
                    }
                }
                else
                {
                    copyNums.Add(nums[i]);
                }
            }
            copyNums.Add(Math.Floor(nums.Last()));
            copyNums.Add(nums.Last());
            copyNums = copyNums.Distinct().ToList();

            resultNums.Add(copyNums[0]);
            resultNums.AddRange(copyNums.Where(i => i % 1 == 0 && !resultNums.Contains(i)).ToList());
            resultNums.Add(copyNums.Last());
            resultNums = resultNums.Distinct().ToList();

            for (int i = 0; i < resultNums.Count; i++)
            {
                if (resultNums[i] < basePolygon.vertices.Count - 1)
                {
                    curveParameters.Add(new KeyValuePair<int, float>(Mathf.FloorToInt((float)resultNums[i]), (float)resultNums[i] % 1));
                }
                else
                {
                    curveParameters.Add(new KeyValuePair<int, float>(Mathf.FloorToInt((float)resultNums[i] - 1), 1.0f));
                }
            }

            List<int> indexToPlace = new List<int>();

            for (int i = 0; i < curveParameters.Count; i++)
            {
                if (!curveLengths.Contains(curveParameters[i]))
                {
                    indexToPlace.Add(i);
                }
            }

            curveLengths = curveParameters;

            for (int i = 0; i < indexToPlace.Count; i++)
            {
                var tempVertex = Instantiate(Resources.Load("Geometry/PolygonVertex") as GameObject).GetComponent<PolygonVertex>();
                tempVertex.Initialize(vertices.Count);
                vertices.Insert(i, tempVertex);
                Vector3 projection = Vector3.zero;
                Vector3 start = basePolygon.vertices[curveLengths[i].Key].currentPosition;
                Vector3 addition = (basePolygon.vertices[curveLengths[i].Key + 1].currentPosition - basePolygon.vertices[curveLengths[i].Key].currentPosition);
                Vector3 additionMagnitude = addition * curveLengths[i].Value;
                projection = start + additionMagnitude;
                tempVertex.UpdatePosition(new Vector3(projection.x, vertices[0].currentPosition.y, projection.z));
            }
        }

        private void OrganizeVertices(ref List<KeyValuePair<int, float>> curveLengths)
        {
            List<KeyValuePair<int, float>> curveParameters = new List<KeyValuePair<int, float>>();

            List<int> keys = new List<int>();
            List<float> values = new List<float>();

            List<float> nums = new List<float>();

            for (int i = 0; i < curveLengths.Count; i++)
            {
                nums.Add(curveLengths[i].Key + (float)Math.Round(curveLengths[i].Value, 3));
            }

            nums.Sort();

            List<double> resultNums = new List<double>();
            List<double> copyNums = new List<double>();

            for (int i = 0; i < nums.Count - 1; i++)
            {
                if (nums[i] % 1 != 0)
                {
                    copyNums.Add(nums[i]);
                    while (Math.Floor(copyNums.Last() + 1) <= nums[i + 1])
                    {
                        copyNums.Add(Math.Floor(copyNums.Last() + 1));
                    }
                }
                else
                {
                    copyNums.Add(nums[i]);
                }
            }
            for (int i = Mathf.CeilToInt(nums[0]); i < Mathf.CeilToInt(nums.Last()); i++)
            {
                copyNums.Add(i);
            }
            copyNums.Add(Math.Floor(nums.Last()));
            copyNums.Add(nums.Last());
            copyNums.Sort();
            copyNums = copyNums.Distinct().ToList();

            resultNums.Add(copyNums[0]);
            resultNums.AddRange(copyNums.Where(i => i % 1 == 0 && !resultNums.Contains(i)).ToList());
            resultNums.Add(copyNums.Last());
            resultNums = resultNums.Distinct().ToList();

            for (int i = 0; i < resultNums.Count; i++)
            {
                if (resultNums[i] < basePolygon.vertices.Count - 1)
                {
                    if ((float)Math.Round(resultNums[i], 3) % 1 == 0 && (float)Math.Round(resultNums[i], 3) != 0)
                    {
                        curveParameters.Add(new KeyValuePair<int, float>((int)resultNums[i] - 1, 1.0f));
                    }
                    else
                    {
                        curveParameters.Add(new KeyValuePair<int, float>(Mathf.FloorToInt((float)resultNums[i]), (float)resultNums[i] % 1));
                    }
                }
                else
                {
                    curveParameters.Add(new KeyValuePair<int, float>((int)resultNums[i] - 1, 1.0f));
                }
            }

            if (curveParameters.Count != vertices.Count)
            {
                Debug.Log("Error");
            }

            curveLengths = curveParameters;

            for (int i = 0; i < curveLengths.Count; i++)
            {
                if (i < vertices.Count)
                {
                    if (basePolygon.closed)
                    {
                        Vector3 projection = Vector3.zero;
                        Vector3 start = basePolygon.vertices[curveLengths[i].Key].currentPosition;
                        Vector3 addition = (basePolygon.vertices[(curveLengths[i].Key + 1) % basePolygon.vertices.Count].currentPosition - basePolygon.vertices[curveLengths[i].Key].currentPosition);
                        Vector3 additionMagnitude = addition * curveLengths[i].Value;
                        projection = start + additionMagnitude;
                        vertices[i].UpdatePosition(new Vector3(projection.x, vertices[i].currentPosition.y, projection.z));
                    }
                    else
                    {
                        Vector3 projection = Vector3.zero;
                        Vector3 start = basePolygon.vertices[curveLengths[i].Key].currentPosition;
                        Vector3 addition = (basePolygon.vertices[curveLengths[i].Key + 1].currentPosition - basePolygon.vertices[curveLengths[i].Key].currentPosition);
                        Vector3 additionMagnitude = addition * curveLengths[i].Value;
                        projection = start + additionMagnitude;
                        vertices[i].UpdatePosition(new Vector3(projection.x, vertices[i].currentPosition.y, projection.z));
                    }
                }
                else
                {
                    var tempVertex = Instantiate(Resources.Load("Geometry/PolygonVertex") as GameObject).GetComponent<PolygonVertex>();
                    tempVertex.Initialize(vertices.Count);
                    vertices.Add(tempVertex);

                    if (basePolygon.closed)
                    {
                        Vector3 projection = Vector3.zero;
                        Vector3 start = basePolygon.vertices[curveLengths[i].Key].currentPosition;
                        Vector3 addition = (basePolygon.vertices[(curveLengths[i].Key + 1) % basePolygon.vertices.Count].currentPosition - basePolygon.vertices[curveLengths[i].Key].currentPosition);
                        Vector3 additionMagnitude = addition * curveLengths[i].Value;
                        projection = start + additionMagnitude;
                        vertices[i].UpdatePosition(new Vector3(projection.x, vertices[0].currentPosition.y, projection.z));
                    }
                    else
                    {
                        Vector3 projection = Vector3.zero;
                        Vector3 start = basePolygon.vertices[curveLengths[i].Key].currentPosition;
                        Vector3 addition = (basePolygon.vertices[curveLengths[i].Key + 1].currentPosition - basePolygon.vertices[curveLengths[i].Key].currentPosition);
                        Vector3 additionMagnitude = addition * curveLengths[i].Value;
                        projection = start + additionMagnitude;
                        vertices[i].UpdatePosition(new Vector3(projection.x, vertices[0].currentPosition.y, projection.z));
                    }
                }

                if (i == 0 || i == curveLengths.Count - 1)
                {
                    if (basePolygon.closed)
                    {
                        ControlPointUI cpui = Instantiate(controlPointUi, GameObject.Find("ControlPoints").transform).GetComponent<ControlPointUI>();
                        cpui.axisConstrain = true;
                        cpui.axis = basePolygon.vertices[(curveLengths[i].Key + 1) % basePolygon.vertices.Count].currentPosition - basePolygon.vertices[curveLengths[i].Key].currentPosition;
                        cpui.Initialize(vertices[i].transform);
                        vertices[i].SetGUIElement(cpui);
                        cpui.moved += UpdateCurveLengths;
                    }
                    else
                    {
                        ControlPointUI cpui = Instantiate(controlPointUi, GameObject.Find("ControlPoints").transform).GetComponent<ControlPointUI>();
                        cpui.axisConstrain = true;
                        cpui.axis = basePolygon.vertices[curveLengths[i].Key + 1].currentPosition - basePolygon.vertices[curveLengths[i].Key].currentPosition;
                        cpui.Initialize(vertices[i].transform);
                        vertices[i].SetGUIElement(cpui);
                        cpui.moved += UpdateCurveLengths;
                    }
                }
            }
        }

        private void CalculateVertices()
        {

        }

        private void OnBasePolygonUpdated(Polygon sender)
        {
            for (int i = 0; i < vertices.Count; i++)
            {
                if (basePolygon.closed)
                {
                    Vector3 projection = Vector3.zero;
                    Vector3 start = basePolygon.vertices[curveLengths[i].Key].currentPosition;
                    Vector3 addition = (basePolygon.vertices[(curveLengths[i].Key + 1) % basePolygon.vertices.Count].currentPosition - basePolygon.vertices[curveLengths[i].Key].currentPosition);
                    Vector3 additionMagnitude = addition * curveLengths[i].Value;
                    projection = start + additionMagnitude;
                    vertices[i].SetPosition(new Vector3(projection.x, vertices[i].currentPosition.y, projection.z));
                    if (vertices[i].cpui != null)
                    {
                        vertices[i].cpui.axis = basePolygon.vertices[(curveLengths[i].Key + 1) % basePolygon.vertices.Count].currentPosition - basePolygon.vertices[curveLengths[i].Key].currentPosition;
                        vertices[i].cpui.UpdateUIPosition();
                    }
                }
                else
                {
                    Vector3 projection = Vector3.zero;
                    Vector3 start = basePolygon.vertices[curveLengths[i].Key].currentPosition;
                    Vector3 addition = (basePolygon.vertices[curveLengths[i].Key + 1].currentPosition - basePolygon.vertices[curveLengths[i].Key].currentPosition);
                    Vector3 additionMagnitude = addition * curveLengths[i].Value;
                    projection = start + additionMagnitude;
                    vertices[i].SetPosition(new Vector3(projection.x, vertices[i].currentPosition.y, projection.z));
                    if (vertices[i].cpui != null)
                    {
                        vertices[i].cpui.axis = basePolygon.vertices[(curveLengths[i].Key + 1)].currentPosition - basePolygon.vertices[curveLengths[i].Key].currentPosition;
                        vertices[i].cpui.UpdateUIPosition();
                    }
                }
            }
        }

        #endregion
    }
}

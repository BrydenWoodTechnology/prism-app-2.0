﻿using BrydenWoodUnity.DesignData;
using BrydenWoodUnity.UIElements;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BrydenWoodUnity.GeometryManipulation
{
    /// <summary>
    /// A MonoBehaviour component for an Editable Mesh
    /// </summary>
    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshRenderer))]
    public class EditableMesh : MonoBehaviour, ISelectableGeometry
    {
        #region Public Fields and Properties
        [Space(10)]
        [Header("Geometry specific fields:")]
        public bool initializeOnStart = false;
        public float area { get { return CalculateArea(); } }
        [Tooltip("The perimeter of the polygon (read-only)")]
        public float perimeter;
        [Tooltip("The offset from base to avoid Z-fighting")]
        public float offsetFromBase = 0.1f;

        public bool flip = false;
        public bool capped = false;
        public bool moveable = false;
        public bool editEdges = true;
        public bool isClockWise = true;
        public bool isBuildingVertex = true;
        public bool moveColinear = false;
        public bool alteredAxis = false;
        public bool constrainedSide = false;
        public bool flippedInterior = false;
        public bool snappingEdges = false;
        public float snappingDistance = 0.5f;
        public bool constrainUnresolvedL = false;
        public bool constrainUnresolvedR = false;
        public bool towerApt = false;
        public bool mansionApt = false;

        [HideInInspector]
        public Color typeColor;
        [HideInInspector]
        public Extrusion meshExtrusion;
        [HideInInspector]
        public ControlPointUI movePoint;
        public Material originalMaterial { get; set; }
        public Material typeMaterial { get; set; }
        public Material errorMaterial { get; set; }
        public Material selectMaterial { get; private set; }
        public Transform verticesParent { get; set; }
        public Transform cpuiParent { get; set; }
        public GameObject vertexPrefab { get; set; }
        public GameObject cpuiPrefab { get; set; }
        public GameObject edgeUiElement { get; set; }
        public GameObject moveUiElement { get; set; }
        public bool hasListeners { get; private set; }
        public List<MeshVertex> vertices { get; set; }
        public Mesh mesh { get; set; }
        public List<EdgeHelpers.Edge> outterEdges { get; set; }
        public bool isSelected { get; private set; }
        public bool isError { get; private set; }
        public Vector3 Centre
        {
            get
            {
                Vector3 cent = new Vector3();
                for (int i = 0; i < mesh.vertices.Length; i++)
                {
                    cent += mesh.vertices[i];
                }
                cent /= mesh.vertices.Length;
                return transform.TransformPoint(cent);
            }
        }
        public MeshVertex this[int i]
        {
            get { return vertices[i]; }
        }

        public int Count { get { return vertices.Count; } }
        #endregion

        #region Private Fields and Properties
        private MeshFilter meshFilter { get; set; }
        private MeshRenderer meshRenderer { get; set; }

        private List<GameObject> edgeControlPoints { get; set; }
        private Dictionary<ControlPointUI, MeshEdgeRelation> edges { get; set; }
        private ControlPointUI edgeControlPointSelected { get; set; }
        #endregion

        #region Events
        /// <summary>
        /// Used when the mesh has been updated
        /// </summary>
        /// <param name="sender">The mesh that has been updated</param>
        public delegate void MeshUpdated(EditableMesh sender);
        /// <summary>
        /// Triggered when a mesh has been updated
        /// </summary>
        public event MeshUpdated updated;
        /// <summary>
        /// Triggered when the mesh has been selected
        /// </summary>
        public event OnSelectGeometry selected;
        /// <summary>
        /// Triggered when the mesh is deselected
        /// </summary>
        public event OnSelectGeometry deselected;
        /// <summary>
        /// Triggered when the mesh has stopped moving
        /// </summary>
        public event MeshUpdated stoppedMoving;
        /// <summary>
        /// Triggered when the mesh has stopped being edited
        /// </summary>
        public event MeshUpdated stoppedEditing;
        /// <summary>
        /// Used to trigger the update event
        /// </summary>
        public virtual void OnUpdated()
        {
            CalculateArea();
            if (updated != null)
            {
                updated(this);
            }
        }
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Awake()
        {
            if (initializeOnStart)
            {
                Initialize();
            }
        }

        // Update is called once per frame
        void Update()
        {

            if (outterEdges != null)
            {
                for (int i = 0; i < outterEdges.Count; i++)
                {
                    Debug.DrawLine(mesh.vertices[outterEdges[i].v1], mesh.vertices[outterEdges[i].v2], Color.red);
                }
            }
        }

        private void OnDestroy()
        {
            if (this.vertices != null && this.vertices.Count != 0)
            {
                for (int i = 0; i < this.vertices.Count; i++)
                {
                    this.vertices[i].vertexMoved -= OnVertexMoved;
                }
            }
        }

        void OnEnable()
        {
            if (vertices != null)
            {
                for (int i = 0; i < vertices.Count; i++)
                {
                    vertices[i].gameObject.SetActive(true);
                }
            }
        }

        void OnDisable()
        {
            if (vertices != null)
            {
                for (int i = 0; i < vertices.Count; i++)
                {
                    if (vertices[i] != null && vertices[i].gameObject != null)
                    {
                        vertices[i].gameObject.SetActive(false);
                    }
                }
            }
            if (movePoint != null)
            {
                HideMoveUI();
            }
            if (edges != null)
            {
                HideEdgesUI();
            }
        }

        private void OnDrawGizmosSelected()
        {
            if (vertices != null)
            {
                for (int i = 0; i < vertices.Count; i++)
                {
                    Gizmos.color = Color.yellow;
                    Gizmos.DrawCube(vertices[i].currentPosition, Vector3.one * 0.5f);
                    Gizmos.color = Color.green;
                    Gizmos.DrawLine(vertices[i].currentPosition, vertices[(i + 1) % vertices.Count].currentPosition);
                }
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Initiates the polygon component
        /// </summary>
        /// <param name="mesh">The Unity Mesh to serve as a basis</param>
        /// <param name="verticesParent">The transform to which the vertices will be children</param>
        /// <param name="cpuiParent">The transform to which the UI elements of the vertices will be children</param>
        public void Initialize(Mesh mesh = null, Transform verticesParent = null, Transform cpuiParent = null)
        {
            meshFilter = GetComponent<MeshFilter>();
            meshRenderer = GetComponent<MeshRenderer>();
            if (mesh != null)
            {
                this.mesh = mesh;
                meshFilter.sharedMesh = this.mesh;

            }
            else
            {
                if (meshFilter.sharedMesh == null)
                {
                    this.mesh = new Mesh();
                }
                else
                {
                    this.mesh = meshFilter.sharedMesh;
                }
            }

            if (verticesParent != null)
            {
                this.verticesParent = verticesParent;
            }
            else
            {
                this.verticesParent = transform;
            }

            if (cpuiParent != null)
            {
                this.cpuiParent = cpuiParent;
            }
            if (vertexPrefab == null)
            {
                vertexPrefab = Resources.Load("Geometry/MeshVertex") as GameObject;
            }
            if (cpuiPrefab == null)
            {
                cpuiPrefab = Resources.Load("GUI/ControlPointUI") as GameObject;
            }
            if (edgeUiElement == null)
            {
                edgeUiElement = Resources.Load("GUI/MovePointUI") as GameObject;
            }
            if (moveUiElement == null)
            {
                moveUiElement = Resources.Load("GUI/MovePointUI") as GameObject;
            }
            if (typeMaterial == null)
            {
                typeMaterial = new Material(Resources.Load("Materials/TypeMaterial") as Material);
            }
            if (originalMaterial == null)
            {
                originalMaterial = GetComponent<MeshRenderer>().material;
            }
            if (errorMaterial == null)
            {
                errorMaterial = Resources.Load("Materials/ErrorMaterial") as Material;
            }
            if (selectMaterial == null)
            {
                selectMaterial = Resources.Load("Materials/SelectMaterial") as Material;
            }

            vertices = new List<MeshVertex>();
            outterEdges = EdgeHelpers.GetEdges(this.mesh.triangles).FindBoundary().SortEdges();
            PopulateVertexList();
        }


        /// <summary>
        /// Removes the event listeners for the mesh
        /// </summary>
        public void RemoveListeners()
        {
            if (hasListeners)
            {
                if (this.vertices != null && this.vertices.Count != 0)
                {
                    for (int i = 0; i < this.vertices.Count; i++)
                    {
                        this.vertices[i].vertexMoved -= OnVertexMoved;
                    }
                }
                DisplayControls.colinearChanged -= OnColinearChanged;
            }
            hasListeners = false;
        }

        /// <summary>
        /// Updates the positions of the vertices
        /// </summary>
        public void UpdateVerticesPositions()
        {
            for (int i = 0; i < vertices.Count; i++)
            {
                vertices[i].UpdatePosition();
            }
        }

        /// <summary>
        /// Sets new vertices to the mesh
        /// </summary>
        /// <param name="vertices">The new vertices</param>
        public void SetVertices(List<MeshVertex> vertices)
        {
            if (hasListeners)
            {
                RemoveListeners();
            }
            for (int i = 0; i < vertices.Count; i++)
            {
                vertices[i].editableMesh = this;
            }
            this.vertices = vertices;
            AddListeners();
        }

        /// <summary>
        /// Updates the Geometry of the mesh
        /// </summary>
        public void UpdateGeometry()
        {
            OnVertexMoved(vertices[0]);
            //OnUpdated();
        }

        /// <summary>
        /// Returns the extrusion of the mesh
        /// </summary>
        /// <returns>Mesh Extrusion</returns>
        public Extrusion GetExtrusion()
        {
            meshExtrusion = Instantiate(Resources.Load("Geometry/Extrusion") as GameObject, transform).GetComponent<Extrusion>();
            meshExtrusion.totalHeight = 3.0f;
            meshExtrusion.Initialize(this);
            return meshExtrusion;
        }

        /// <summary>
        /// Returns the extrusion of the mesh
        /// </summary>
        /// <param name="totalHeight">The total height of the extrusion</param>
        /// <param name="capped">Whether the extrusion should be capped or not</param>
        /// <returns>Mesh extrusion</returns>
        public Extrusion GetExtrusion(float totalHeight, bool capped = false)
        {
            meshExtrusion = Instantiate(Resources.Load("Geometry/Extrusion") as GameObject, transform).GetComponent<Extrusion>();
            meshExtrusion.capped = capped;
            this.capped = capped;
            if (capped)
            {
                flip = true;
                FlipMesh();
            }
            meshExtrusion.totalHeight = totalHeight;
            meshExtrusion.Initialize(this);
            return meshExtrusion;
        }

        /// <summary>
        /// Called when the mesh is being moved
        /// </summary>
        /// <param name="sender">The object that moves it</param>
        public void OnMoved(object sender)
        {
            HideEdgesUI();
            if (vertices != null)
            {
                for (int i = 0; i < vertices.Count; i++)
                {
                    //vertices[i].cpui.gameObject.SetActive(false);
                }
            }
            if (updated != null)
            {
                updated(this);
            }
        }

        /// <summary>
        /// Called when the mesh stops being moved
        /// </summary>
        /// <param name="sender">The object that moved it</param>
        public void OnStoppedMoving(object sender)
        {
            if (stoppedMoving != null)
            {
                stoppedMoving(this);
            }

            for (int i = 0; i < vertices.Count; i++)
            {
                //vertices[i].cpui.gameObject.SetActive(true);
                var transVec = transform.localPosition + mesh.vertices[i] - new Vector3(0, offsetFromBase, 0);
                vertices[i].UpdatePosition(transVec);
                //vertices[i].cpui.UpdateUIPosition();
            }
            HideMoveUI();
            DisplayMoveUI();
            DisplayNakedEdgesUI();
        }

        /// <summary>
        /// Called when the mesh is selected
        /// </summary>
        public void Select()
        {
            if (selected != null)
            {
                selected();
            }
        }

        /// <summary>
        /// Called in order to deselect the mesh
        /// </summary>
        public void DeSelect()
        {
            if (deselected != null)
            {
                deselected();
            }
        }

        /// <summary>
        /// Sets the mesh display into standard mode
        /// </summary>
        public void DisplayStandardMode()
        {
            GetComponent<MeshRenderer>().material = originalMaterial;
            meshExtrusion.GetComponent<MeshRenderer>().material = originalMaterial;
            meshExtrusion.DisplayStandardMode();
        }

        /// <summary>
        /// Sets the color of the type material
        /// </summary>
        /// <param name="color">Color</param>
        public void SetTypeColor(Color color)
        {
            typeMaterial.color = color;
            typeColor = color;
        }

        /// <summary>
        /// Highlights the mesh if there is an error
        /// </summary>
        /// <param name="error">Toggles the highlight</param>
        public void ErrorHighlight(bool error)
        {
            isError = error;
            if (error)
            {
                GetComponent<MeshRenderer>().material = errorMaterial;
                meshExtrusion.GetComponent<MeshRenderer>().material = errorMaterial;
            }
            else
            {
                GetComponent<MeshRenderer>().material = originalMaterial;
                meshExtrusion.GetComponent<MeshRenderer>().material = originalMaterial;
            }
        }

        /// <summary>
        /// Highlights the mesh if it has been selected
        /// </summary>
        /// <param name="select">Toggles the highlight</param>
        public void SelectHighlight(bool select)
        {
            isSelected = select;
            if (select)
            {
                GetComponent<MeshRenderer>().material = selectMaterial;
                meshExtrusion.GetComponent<MeshRenderer>().material = selectMaterial;
                if (moveable)
                {
                    DisplayMoveUI();
                }
                if (editEdges)
                {
                    DisplayNakedEdgesUI();
                }
            }
            else
            {
                //if (!isError)
                //{
                GetComponent<MeshRenderer>().material = originalMaterial;
                meshExtrusion.GetComponent<MeshRenderer>().material = originalMaterial;
                //}
                //else
                //{
                //    ErrorHighlight(isError);
                //}
                if (moveable)
                {
                    HideMoveUI();
                }
                if (editEdges)
                {
                    HideEdgesUI();
                }
            }
        }

        /// <summary>
        /// Highlights the mesh with the type colour
        /// </summary>
        /// <param name="show">Toggles the highlight</param>
        public void TypeHighlight(bool show)
        {
            if (!isSelected)
            {
                if (show)
                {
                    typeMaterial.SetFloat("_OutlineSize", 0.1f);
                    typeMaterial.SetColor("_EmissionColor", typeMaterial.color*0.2f);
                    GetComponent<MeshRenderer>().material = typeMaterial;
                    meshExtrusion.GetComponent<MeshRenderer>().material = typeMaterial;
                }
                else
                {
                    typeMaterial.SetFloat("_OutlineSize", 0.03f);
                    typeMaterial.SetColor("_EmissionColor", typeMaterial.color * 0.0f);
                    GetComponent<MeshRenderer>().material = originalMaterial;
                    meshExtrusion.GetComponent<MeshRenderer>().material = originalMaterial;
                }
            }
        }

        /// <summary>
        /// Flips the mesh
        /// </summary>
        public void FlipMesh()
        {
            int[] flippedTris = new int[mesh.triangles.Length];
            for (int i = 0; i < mesh.triangles.Length; i += 3)
            {
                flippedTris[i] = mesh.triangles[i + 2];
                flippedTris[i + 1] = mesh.triangles[i + 1];
                flippedTris[i + 2] = mesh.triangles[i];
            }
            mesh.triangles = flippedTris;
            UpdateMeshGeometry();
        }

        /// <summary>
        /// Moves the mesh to a given position
        /// </summary>
        /// <param name="to">The new position</param>
        /// <param name="reverse">The direction of the movement (positive or negative)</param>
        public void Move(Vector3 to, bool reverse = false)
        {
            List<Vector3> diffs = new List<Vector3>();
            if (reverse)
            {
                for (int i = 0; i < vertices.Count; i++)
                {
                    diffs.Add(vertices[i].currentPosition - vertices[3].currentPosition);
                }
                for (int i = 0; i < vertices.Count; i++)
                {
                    vertices[i].SetPosition(to + diffs[i], false);
                }
            }
            else
            {
                for (int i = 0; i < vertices.Count; i++)
                {
                    diffs.Add(vertices[i].currentPosition - vertices[0].currentPosition);
                }
                for (int i = 0; i < vertices.Count; i++)
                {
                    vertices[i].SetPosition(to + diffs[i], false);
                }
            }
        }

        /// <summary>
        /// Returns the corners of the mesh
        /// </summary>
        /// <returns>Vector3 Array</returns>
        public Vector3[] GetCorners()
        {
            List<Vector3> relevantPositions = new List<Vector3>();
            for (int i = 0; i < vertices.Count; i++)
            {
                relevantPositions.Add(vertices[i].currentLocalPosition);
            }

            List<float> xValues = relevantPositions.Select(x => (float)Math.Round(x.x, 2)).ToList();
            xValues.Sort();
            List<float> yValues = relevantPositions.Select(x => (float)Math.Round(x.z, 2)).ToList();
            yValues.Sort();

            Vector3[] corners = new Vector3[4];
            for (int i = 0; i < vertices.Count; i++)
            {
                float x = (float)Math.Round(vertices[i].currentLocalPosition.x, 2);
                float y = (float)Math.Round(vertices[i].currentLocalPosition.z, 2);

                if (x == xValues[0] && y == yValues[0])
                {
                    corners[3] = vertices[i].currentPosition;
                }
                else if (x == xValues[0] && y == yValues.Last())
                {
                    corners[2] = vertices[i].currentPosition;
                }
                else if (x == xValues.Last() && y == yValues.Last())
                {
                    corners[1] = vertices[i].currentPosition;
                }
                else if (x == xValues.Last() && y == yValues[0])
                {
                    corners[0] = vertices[i].currentPosition;
                }
            }
            return corners.ToArray();
        }

        /// <summary>
        /// Sets the type material as the main material of the mesh
        /// </summary>
        public void SetTypeMaterialAsMain()
        {
            typeMaterial.SetFloat("_OutlineSize", 0.03f);
            originalMaterial = typeMaterial;
            if (!isSelected && !isError)
            {
                GetComponent<MeshRenderer>().material = originalMaterial;
                if (meshExtrusion != null)
                {
                    meshExtrusion.GetComponent<MeshRenderer>().material = originalMaterial;
                }
            }
        }

        /// <summary>
        /// Sets a new height for the extrusion
        /// </summary>
        /// <param name="height">New extrusion height</param>
        public void SetExtrusionHeight(float height)
        {
            meshExtrusion.totalHeight = height;
            meshExtrusion.capped = capped;
            meshExtrusion.GenerateExtrusionMeshes();
        }

        /// <summary>
        /// Updates the extrusion
        /// </summary>
        public void UpdateExtrusion()
        {
            meshExtrusion.capped = capped;
            meshExtrusion.GenerateExtrusionMeshes();
        }

        /// <summary>
        /// Calculates the edges of the mesh
        /// </summary>
        public void CalculateEdges()
        {
            outterEdges = EdgeHelpers.GetEdges(this.mesh.triangles).FindBoundary().SortEdges();
        }

        /// <summary>
        /// Destroys the mesh
        /// </summary>
        /// <param name="destroyVertices">Whether the vertices should be destroyed</param>
        public void DestroyMesh(bool destroyVertices = false)
        {
            if (this.vertices != null && this.vertices.Count != 0)
            {
                for (int i = 0; i < this.vertices.Count; i++)
                {
                    if (vertices[i] != null)
                    {
                        this.vertices[i].vertexMoved -= OnVertexMoved;
                        this.vertices[i].stoppedMoving -= OnVertexStoppedMoving;
                        if (destroyVertices)
                        {
                            vertices[i].DestroyVertex();
                        }
                    }
                }
            }
            Destroy(meshExtrusion.gameObject);
            Destroy(gameObject);
        }

        /// <summary>
        /// Updates the vertices of the mesh
        /// </summary>
        public void UpdateVertices()
        {
            for (int i = 0; i < vertices.Count; i++)
            {
                if (vertices[i] != null)
                {
                    GeometryVertexMoved d1 = OnVertexMoved;
                    GeometryVertexMoved d2 = OnVertexStoppedMoving;
                    if (!vertices[i].IsEventHandlerRegisteredMoved(d1))
                    {
                        vertices[i].vertexMoved += d1;
                    }
                    if (!vertices[i].IsEventHandlerRegisteredStopped(d2))
                    {
                        vertices[i].stoppedMoving += d2;
                    }
                }
            }
        }

        /// <summary>
        /// Updates the geometry of the mesh
        /// </summary>
        public void UpdateMeshGeometry()
        {
            //area = CalculateArea();
            Vector2[] uvs = new Vector2[mesh.vertices.Length];
            for (int i = 0; i < uvs.Length; i++)
            {
                uvs[i] = new Vector2(mesh.vertices[i].x, mesh.vertices[i].z);
            }
            mesh.uv = uvs;
            mesh.uv2 = uvs;
            mesh.RecalculateBounds();
            mesh.RecalculateNormals();
            meshFilter.sharedMesh = mesh;
            GetComponent<MeshCollider>().sharedMesh = mesh;
        }

        /// <summary>
        /// Updates the origin point of the mesh
        /// </summary>
        /// <param name="position">The new position</param>
        public void UpdateMeshPosition(Vector3 position)
        {
            BrydenWoodUtils.MoveParentIndependently(transform, position);
            meshExtrusion.transform.position = transform.position;
            UpdateVerticesPositions();
            var verts = mesh.vertices;
            for (int i = 0; i < verts.Length; i++)
            {
                verts[i] = vertices[i].currentLocalPosition;
            }
            mesh.vertices = verts;
            UpdateMeshGeometry();
            UpdateExtrusion();
        }
        #endregion

        #region Private Methods
        private void AddListeners()
        {
            if (!hasListeners)
            {
                if (this.vertices != null && this.vertices.Count != 0)
                {
                    for (int i = 0; i < this.vertices.Count; i++)
                    {
                        this.vertices[i].vertexMoved += OnVertexMoved;
                        this.vertices[i].stoppedMoving += OnVertexStoppedMoving;
                    }
                }
                DisplayControls.colinearChanged += OnColinearChanged;
            }
            hasListeners = true;
        }
        private void OnColinearChanged(bool colinear)
        {
            moveColinear = colinear;
        }
        private void PopulateVertexList()
        {
            if (mesh.vertices != null && mesh.vertices.Length > 0)
            {
                for (int i = 0; i < mesh.vertices.Length; i++)
                {
                    MeshVertex meshVertex = Instantiate(vertexPrefab, verticesParent).GetComponent<MeshVertex>();
                    meshVertex.gameObject.name = name + "_vertex" + i;
                    meshVertex.transform.localPosition = mesh.vertices[i];
                    meshVertex.Initialize(i);
                    meshVertex.editableMesh = this;
                    meshVertex.vertexMoved += OnVertexMoved;
                    meshVertex.stoppedMoving += OnVertexStoppedMoving;
                    vertices.Add(meshVertex);
                }
            }
        }

        private float CalculateArea()
        {
            float m_area = 0;
            for (int i = 0; i < mesh.triangles.Length / 3; i++)
            {
                Vector3 a = vertices[mesh.triangles[i * 3]].currentLocalPosition;
                Vector3 b = vertices[mesh.triangles[i * 3 + 1]].currentLocalPosition;
                Vector3 c = vertices[mesh.triangles[i * 3 + 2]].currentLocalPosition;
                float partA = a.x * (b.z - c.z);
                float partB = b.x * (c.z - a.z);
                float partC = c.x * (a.z - b.z);
                m_area += Mathf.Abs((partA + partB + partC)) / 2.0f;
            }
            return m_area;
        }

        private void OnVertexMoved(GeometryVertex sender)
        {
            var m_sender = (MeshVertex)sender;
            if (m_sender != null)
            {
                var verts = mesh.vertices;
                verts[sender.index] = transform.InverseTransformPoint(sender.currentPosition);
                mesh.vertices = verts;
                UpdateMeshGeometry();                
                OnUpdated();
            }
        }

        private void HideMoveUI()
        {
            if (movePoint != null)
            {
                movePoint.moved -= OnMoved;
                movePoint.stoppedMoving -= OnStoppedMoving;
                Destroy(movePoint.gameObject);
                movePoint = null;
            }
        }

        private void DisplayMoveUI()
        {
            GameObject m_parent = GameObject.Find("Canvas");
            movePoint = Instantiate(moveUiElement, m_parent.transform).GetComponent<ControlPointUI>();
            movePoint.Initialize(transform);
            movePoint.moved += OnMoved;
            movePoint.stoppedMoving += OnStoppedMoving;
        }

        private void DisplayNakedEdgesUI()
        {
            if (mansionApt) return;
            if (edges == null && vertices.Count == 4)
            {
                edges = new Dictionary<ControlPointUI, MeshEdgeRelation>();
                edgeControlPoints = new List<GameObject>();
                for (int i = 0; i < outterEdges.Count; i++)
                {
                    GameObject m_parent = GameObject.Find("EdgeControlPoints");
                    var m_edgePoint = Instantiate(edgeUiElement, m_parent.transform).GetComponent<ControlPointUI>();
                    edgeControlPoints.Add(Instantiate(vertexPrefab));
                    edgeControlPoints[i].GetComponent<MeshVertex>().SetGUIElement(m_edgePoint);
                    edgeControlPoints[i].transform.position = vertices[outterEdges[i].v1].currentPosition + (vertices[outterEdges[i].v2].currentPosition - vertices[outterEdges[i].v1].currentPosition) / 2.0f;
                    m_edgePoint.Initialize(edgeControlPoints[i].transform);
                    m_edgePoint.axisConstrain = true;
                    m_edgePoint.axis = Vector3.Cross(Vector3.up, vertices[outterEdges[i].v2].currentPosition - vertices[outterEdges[i].v1].currentPosition).normalized;
                    m_edgePoint.moved += OnEdgeMoved;
                    m_edgePoint.onSelected += OnEdgeCPUISelected;
                    if (!edges.ContainsKey(m_edgePoint))
                    {
                        MeshEdgeRelation m_relation = new MeshEdgeRelation();
                        m_relation.vertices = new List<MeshVertex>() { vertices[outterEdges[i].v1], vertices[outterEdges[i].v2] };
                        m_relation.originalDifs = new List<Vector3>();
                        m_relation.originalDifs.Add(vertices[outterEdges[i].v1].currentPosition - edgeControlPoints[i].transform.position);
                        m_relation.originalDifs.Add(vertices[outterEdges[i].v2].currentPosition - edgeControlPoints[i].transform.position);

                        if (moveColinear)
                        {
                            List<MeshVertex> verts = null;
                            if (isBuildingVertex)
                            {
                                //verts = geometryHandler.buildingPolygonVertices;
                            }
                            else
                            {
                                //verts = geometryHandler.apartmentLayoutMeshVertices;
                            }

                            if (verts != null)
                            {
                                for (int j = 0; j < verts.Count; j++)
                                {
                                    if (verts[j].gameObject.activeSelf && verts[j] != vertices[outterEdges[i].v1] && verts[j] != vertices[outterEdges[i].v2])
                                    {
                                        MeshVertex vertex = verts[j];
                                        var projection = BrydenWoodUtils.ProjectOnCurve(vertices[outterEdges[i].v1].currentPosition, vertices[outterEdges[i].v2].currentPosition, vertex.currentPosition);
                                        if (Vector3.Distance(projection, vertex.currentPosition) < 0.3)
                                        {
                                            m_relation.vertices.Add(vertex);
                                            m_relation.originalDifs.Add(vertex.currentPosition - edgeControlPoints[i].transform.position);
                                        }
                                    }
                                }
                            }
                        }
                        edges.Add(m_edgePoint, m_relation);
                    }


                    if (towerApt)
                    {
                        if (!alteredAxis)
                        {
                            if (constrainedSide)
                            {
                                if (flippedInterior && i == 1)
                                    m_edgePoint.gameObject.SetActive(false);
                                else if (!flippedInterior && i == 3)
                                    m_edgePoint.gameObject.SetActive(false);
                            }
                            if (i % 2 == 0)
                                m_edgePoint.gameObject.SetActive(false);
                        }
                        else if (alteredAxis)
                        {
                            if (constrainedSide)
                            {
                                if (flippedInterior && i == 2)
                                    m_edgePoint.gameObject.SetActive(false);
                                else if (!flippedInterior && i == 2)
                                    m_edgePoint.gameObject.SetActive(false);
                            }
                            if (i % 2 == 1)
                                m_edgePoint.gameObject.SetActive(false);
                        }
                    }
                    else
                    {
                        if (!constrainUnresolvedL && !constrainUnresolvedR)
                        {
                            if (!alteredAxis)
                            {
                                if (constrainedSide)
                                {
                                    if (flippedInterior && i == 1)
                                        m_edgePoint.gameObject.SetActive(false);
                                    else if (!flippedInterior && i == 3)
                                        m_edgePoint.gameObject.SetActive(false);
                                }
                                if (i % 2 == 0)
                                    m_edgePoint.gameObject.SetActive(false);
                            }
                            else if (alteredAxis)
                            {
                                if (constrainedSide)
                                {
                                    if (flippedInterior && i == 2)
                                        m_edgePoint.gameObject.SetActive(false);
                                    else if (!flippedInterior && i == 2)
                                        m_edgePoint.gameObject.SetActive(false);
                                }
                                if (i % 2 == 1)
                                    m_edgePoint.gameObject.SetActive(false);
                            }
                        }
                        else
                        {
                            if (constrainUnresolvedL && i == 3)
                            {
                               // m_edgePoint.gameObject.SetActive(false);
                            }
                            if (constrainUnresolvedR && i == 1)
                            {
                                // m_edgePoint.gameObject.SetActive(false);
                            }
                            if (i % 2 == 0)
                                m_edgePoint.gameObject.SetActive(false);
                        }
                    }
                }
            }
        }

        private void HideEdgesUI()
        {
            //Debug.Log("HideEdges");
            if (edges != null)
            {
                foreach (var item in edges)
                {
                    item.Key.moved -= OnEdgeMoved;
                    item.Key.onSelected -= OnEdgeCPUISelected;
                    if (item.Key.gameObject != null)
                    {
                        Destroy(item.Key.gameObject);
                    }
                }
            }
            if (edgeControlPoints != null)
            {
                foreach (var item in edgeControlPoints)
                {
                    if (item != null)
                    {
                        Destroy(item);
                    }
                }
            }
            edges = null;
            edgeControlPoints = null;
        }

        private void OnEdgeMoved(object sender)
        {
            ControlPointUI cpui = (ControlPointUI)sender;
            if (edgeControlPointSelected == cpui)
            {
                MeshEdgeRelation relation;
                if (snappingEdges)
                {
                    var apt = transform.parent.GetComponent<ApartmentUnity>();
                    var currentPosition = cpui.sceneObjects[0].transform.position;
                    var direction = apt.SnappingDirection;
                    var projectionPoint = currentPosition + direction * 100;
                    var collider = apt.GetCloseOnesCollider();
                    var closestPoint = collider.ClosestPoint(projectionPoint);
                    if (Vector3.Distance(closestPoint, currentPosition) < snappingDistance)
                    {
                        cpui.sceneObjects[0].transform.position = closestPoint;
                    }
                    Debug.DrawLine(closestPoint, currentPosition);
                    collider.enabled = false;
                }
                if (edges.TryGetValue(cpui, out relation))
                {
                    for (int i = 0; i < relation.originalDifs.Count; i++)
                    {
                        relation.vertices[i].SetPosition(cpui.sceneObjects[0].transform.position + relation.originalDifs[i], true);
                        //relation.vertices[i].cpui.UpdateUIPosition();
                    }
                }
            }
        }

        private void OnVertexStoppedMoving(GeometryVertex vertex)
        {
            if (stoppedEditing != null)
            {
                stoppedEditing(this);
            }
        }

        private void OnEdgeCPUISelected(ControlPointUI cpui)
        {
            edgeControlPointSelected = cpui;
            MeshEdgeRelation m_relation;
            if (edges.TryGetValue(cpui, out m_relation))
            {
                cpui.axis = Vector3.Cross(Vector3.up, m_relation.vertices[1].currentPosition - m_relation.vertices[0].currentPosition).normalized;
                m_relation.originalDifs = new List<Vector3>();
                if (moveColinear)
                {
                    for (int i = 0; i < m_relation.vertices.Count; i++)
                    {
                        m_relation.originalDifs.Add(m_relation.vertices[i].currentPosition - cpui.sceneObjects[0].transform.position);
                    }
                }
                else
                {
                    for (int i = 0; i < 2; i++)
                    {
                        m_relation.originalDifs.Add(m_relation.vertices[i].currentPosition - cpui.sceneObjects[0].transform.position);
                    }
                }
                edges[cpui] = m_relation;
            }
        }
        #endregion
    }
}

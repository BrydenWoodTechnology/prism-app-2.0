﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BrydenWoodUnity.GeometryManipulation
{
    public class RecalculateUVs : MonoBehaviour
    {
        private MeshFilter meshFilter;
        // Use this for initialization
        void Start()
        {
            RecalcUVs();
        }

        private void OnEnable()
        {
            RecalcUVs();
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void RecalcUVs()
        {
            meshFilter = GetComponent<MeshFilter>();
            if (meshFilter == null) return;

            Mesh m = meshFilter.sharedMesh;

            Vector2[] uvs = new Vector2[m.vertices.Length];
            for (int i=0; i<m.vertices.Length; i++)
            {
                uvs[i] = new Vector2(m.vertices[i].x, m.vertices[i].y);
            }
            m.uv = uvs;
            m.uv2 = uvs;
            m.uv3 = uvs;
        }
    }
}

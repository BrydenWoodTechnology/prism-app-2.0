﻿using BrydenWoodUnity.UIElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BrydenWoodUnity.GeometryManipulation
{

    /// <summary>
    /// A MonoBehaviour component for a Polygon Vertex (inherits from Geometry Vertex)
    /// </summary>
    public class PolygonVertex : GeometryVertex
    {
        #region Public Fields and Properties
        public List<Polygon> polygons { get; set; }
        public Polygon polygon { get; set; }
        private Vector3 diff;
        private PolygonVertex toFollow;
        #endregion

        #region Private Fields and Properties
        private List<int> polygonVerts;
        #endregion

        #region MonoBehaviour Methods
        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (toFollow!=null)
            {
                toFollow.vertexMoved -= UpdatePosition;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Initializes the Instance
        /// </summary>
        /// <param name="index">The index of the polygon vertex</param>
        public override void Initialize(int index)
        {
            base.Initialize(index);
            polygons = new List<Polygon>();
            polygonVerts = new List<int>();
        }

        /// <summary>
        /// Sets the polygon to which the vertex belongs
        /// </summary>
        /// <param name="polygon">The polygon to which it belongs</param>
        public void SetPolygon(Polygon polygon)
        {
            this.polygon = polygon;
            for (int i = 0; i < polygon.Count; i++)
            {
                polygonVerts.Add(polygon[i].index);
            }
        }

        /// <summary>
        /// Sets the local position of the polygon vertex
        /// </summary>
        /// <param name="localPos">The new local position</param>
        public void SetLocalPosition(Vector3 localPos)
        {
            transform.localPosition = localPos;
            currentLocalPosition = localPos;
            currentPosition = transform.position;
            if (cpui != null)
            {
                cpui.UpdatePositions(transform);
            }
            OnMoved(this);

        }

        /// <summary>
        /// Adds a polygon to the polygons the vertex belongs
        /// </summary>
        /// <param name="polygon">The polygon to be added</param>
        public void AddPolygon(Polygon polygon)
        {
            polygons.Add(polygon);
            for (int i = 0; i < polygon.Count; i++)
            {
                polygonVerts.Add(polygon[i].index);
            }
        }

        public void Follow(PolygonVertex vertex)
        {
            toFollow = vertex;
            diff = currentPosition - vertex.currentPosition;
            vertex.vertexMoved += UpdatePosition;
        }

        public void UpdateFollowDist(PolygonVertex vertex)
        {
            toFollow = vertex;
            diff = currentPosition - vertex.currentPosition;
        }
        #endregion

        #region Private Methods
        private void UpdatePosition(GeometryVertex vertex)
        {
            SetPosition(vertex.currentPosition + diff);
        }
        #endregion

        public static Vector3[] ToVectorArray(List<PolygonVertex> array)
        {
            Vector3[] _array = new Vector3[array.Count];
            for (int i = 0; i < array.Count; i++)
            {
                _array[i] = array[i].currentPosition;
            }
            return _array;
        }

    }
}

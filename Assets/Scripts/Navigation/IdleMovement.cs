﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

namespace BrydenWoodUnity.Navigation
{
    /// <summary>
    /// A MonoBehaviour component for moving then camera when application is idle
    /// </summary>
    public class IdleMovement : MonoBehaviour
    {
        #region Public Fields and Properties
        [Header("Scene Reference:")]
        public OrbitCamera maxCamera;
        public PostProcessVolume volume;
        [Header("User Input:")]
        public float secondsIdle;
        #endregion

        #region Private Fields and Properties
        private Vector3 lastMousePosition = Vector3.zero;
        private DateTime start;
        private Vignette vignette;
        private ChromaticAberration chromaticAberration;
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            volume.profile.TryGetSettings(out vignette);
            volume.profile.TryGetSettings(out chromaticAberration);
        }

        // Update is called once per frame
        void Update()
        {
            if (lastMousePosition!=Input.mousePosition)
            {
                start = DateTime.Now;
                vignette.intensity.value = 0.2f;
                chromaticAberration.intensity.value = 0;
            }
            else
            {
                if ((DateTime.Now-start).TotalSeconds>secondsIdle)
                {
                 //   maxCamera.RotateCamera();
                    vignette.intensity.value = 0.45f;
                    chromaticAberration.intensity.value = 0.25f;
                }
            }
            lastMousePosition = Input.mousePosition;
        }
        #endregion
    }
}

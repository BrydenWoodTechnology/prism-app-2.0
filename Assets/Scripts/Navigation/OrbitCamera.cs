﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using BrydenWoodUnity.DesignData;
using UnityEngine.EventSystems;

namespace BrydenWoodUnity.Navigation
{
    /// <summary>
    /// Navigation modes for mouse interaction
    /// </summary>
    public enum NavigationMode
    {
        Select = 0,
        Pan = 1,
        Zoom = 2,
        Rotate = 3,
        Full = 4,
    }

    /// <summary>
    /// The camera modes
    /// </summary>
    public enum CameraMode
    {
        Perspective = 0,
        Plan = 2,
        Ortho = 1
    }

    /// <summary>
    /// A MonoBehaviour component for an orbiting camera
    /// </summary>
    [RequireComponent(typeof(Camera))]
    public class OrbitCamera : MonoBehaviour
    {
        public Transform target;
        public float orbitScale = 2;
        public float panScale = 0.5f;
        public float zoomScale = 3;
        public float orthographicZoomScale = 1;
        public float minDistance = 1;
        public float maxYAngle = 80;
        public float minOrthographicSize = 1;

        public NavigationMode navigationMode = NavigationMode.Select;
        public CameraMode cameraMode = CameraMode.Perspective;

        [Header("Scene References:")]
        public Selector selector;
        public CanvasGroup cpCanvasGroup;
        public Button[] moveButton;
        public Button rotateButton;
        public ProceduralBuildingManager buildingManager;

        private Plane targetPlane;
        public float distance;
        private Vector3 projectedForward;
        private Vector3 prev_euler;

        private Vector3 lastPos;
        private Vector3 lastRotation;
        private float lastDist;
        private float prevZoomRate = 40;
        private bool move = false;
        // Use this for initialization
        void Start()
        {
            distance = Vector3.Distance(transform.position, target.position);
            target.LookAt(transform);
        }

        bool start = false;
        // Update is called once per frame
        void LateUpdate()
        {
            //if (Input.GetKeyDown(KeyCode.RightArrow))
            //{
            //    start = !start;
            //}
            //if (start)
            //{
            //    RotateAroundTarget(0.1f);
            //}
            if (!EventSystem.current.IsPointerOverGameObject(-1))
            {
                switch (navigationMode)
                {
                    case NavigationMode.Select:
                        SelectNavigationMode();
                        break;
                    case NavigationMode.Pan:
                        PanNavigationMode();
                        break;
                    case NavigationMode.Rotate:
                        OrbitNavigationMode();
                        break;
                    case NavigationMode.Zoom:
                        ZoomNavigationMode();
                        break;
                    case NavigationMode.Full:
                        FullNavigationMode();
                        break;
                }

                transform.position = target.position + target.forward * distance;
                transform.LookAt(target);
            }
        }

        /// <summary>
        /// Sets the navigation mode
        /// </summary>
        /// <param name="index">The target navigation mode</param>
        public void SetNavigationMode(int index)
        {
            navigationMode = (NavigationMode)index;
            selector.enabled = navigationMode == NavigationMode.Select;
            cpCanvasGroup.blocksRaycasts = navigationMode == NavigationMode.Select;
            for (int i = 0; i < moveButton.Length; i++)
            {
                moveButton[i].interactable = navigationMode == NavigationMode.Select;
            }
            rotateButton.interactable = navigationMode == NavigationMode.Select;
        }

        /// <summary>
        /// Toggles between perspective, plan and ortho
        /// </summary>
        /// <param name="mode">The target camera mode</param>
        public void TogglePlanView(int mode)
        {
            switch (mode)
            {
                case 0:
                    if (cameraMode == CameraMode.Plan)
                    {
                        target.position = lastPos;
                        target.eulerAngles = lastRotation;
                    }
                    GetComponent<Camera>().orthographic = false;
                    distance = lastDist;
                    break;
                case 1:
                    if (cameraMode == CameraMode.Plan)
                    {
                        target.position = lastPos;
                        target.eulerAngles = lastRotation;
                    }
                    distance = 800f;
                    GetComponent<Camera>().orthographic = true;
                    
                    break;
                case 2:
                    lastPos = target.position;
                    lastRotation = target.eulerAngles;
                    lastDist = distance;
                    GetComponent<Camera>().orthographic = true;
                    transform.position = target.position + new Vector3(0, 100, 0);
                    transform.eulerAngles = new Vector3(90, 0, 0);
                    target.eulerAngles = new Vector3(-90, -180 - 0);
                    break;
            }

            if (buildingManager.sitePolygon != null)
            {
                target.transform.position = buildingManager.sitePolygon.Centre;
            }
            else
            {
                if (buildingManager.proceduralBuildings != null && buildingManager.proceduralBuildings.Count > 0)
                {
                    if (buildingManager.proceduralBuildings[0].hasGeometry)
                    {
                        target.transform.position = buildingManager.proceduralBuildings[0].transform.position;
                    }
                }
            }

            cameraMode = (CameraMode)mode;
        }

        private void SelectNavigationMode()
        {
            Orbit(1);
            Pan(2);
            Zoom(-1);
        }

        private void PanNavigationMode()
        {
            Pan(0);
            Zoom(-1);
        }

        private void ZoomNavigationMode()
        {
            Zoom(0);
        }

        private void OrbitNavigationMode()
        {
            Orbit(0);
            Zoom(-1);
        }

        private void FullNavigationMode()
        {
            Orbit(0);
            Pan(1);
            Zoom(-1);
        }

        private void Pan(int mouseButtonIndex)
        {
            if (Input.GetMouseButton(mouseButtonIndex))
            {
                var deltaX = Input.GetAxis("Mouse X");
                target.position += target.right * deltaX * panScale;
                var deltaY = -Input.GetAxis("Mouse Y");
                target.position += target.up * deltaY * panScale;
            }
        }

        private void Orbit(int mouseButtonIndex)
        {
            if (Input.GetMouseButton(mouseButtonIndex) && cameraMode != CameraMode.Plan)
            {
                projectedForward = new Vector3(target.transform.forward.x, 0, target.transform.forward.z);
                float prevAngle = Vector3.Angle(target.transform.forward, projectedForward);
                prev_euler = target.eulerAngles;
                var deltaX = Input.GetAxis("Mouse X");
                target.RotateAround(target.position, Vector3.up, deltaX * orbitScale);
                var deltaY = Input.GetAxis("Mouse Y");
                target.RotateAround(target.position, target.right, deltaY * orbitScale);
                projectedForward = new Vector3(target.transform.forward.x, 0, target.transform.forward.z);
                if (Vector3.Angle(target.transform.forward, projectedForward) > maxYAngle && prevAngle < maxYAngle)
                {
                    target.eulerAngles = prev_euler;
                }
            }
        }

        private void RotateAroundTarget(float increment)
        {
            target.RotateAround(target.position, Vector3.up, increment);
        }

        private void Zoom(int mouseButtonIndex)
        {
            if (cameraMode == CameraMode.Perspective)
            {
                if (mouseButtonIndex == -1)
                {
                    var delta = -Input.GetAxis("Mouse ScrollWheel");
                    var newDist = distance + delta * zoomScale;
                    if (newDist < minDistance)
                    {
                        distance = minDistance;
                    }
                    else
                    {
                        distance += delta * zoomScale;
                    }
                }
                else
                {
                    if (Input.GetMouseButton(mouseButtonIndex))
                    {
                        var delta = -Input.GetAxis("Mouse Y");
                        var newDist = distance + delta * zoomScale;
                        if (newDist < minDistance)
                        {
                            distance = minDistance;
                        }
                        else
                        {
                            distance += delta * zoomScale;
                        }
                    }
                }
            }
            else
            {
                if (mouseButtonIndex == -1)
                {
                    var delta = -Input.GetAxis("Mouse ScrollWheel");
                    var newOrthoSize = GetComponent<Camera>().orthographicSize + delta * zoomScale;
                    if (newOrthoSize < minOrthographicSize)
                    {
                        GetComponent<Camera>().orthographicSize = minOrthographicSize;
                    }
                    else
                    {
                        GetComponent<Camera>().orthographicSize += delta * zoomScale;
                    }
                }
                else
                {
                    if (Input.GetMouseButton(mouseButtonIndex))
                    {
                        var delta = -Input.GetAxis("Mouse Y");
                        var newOrthoSize = GetComponent<Camera>().orthographicSize + delta * zoomScale;
                        if (newOrthoSize < minOrthographicSize)
                        {
                            GetComponent<Camera>().orthographicSize = minOrthographicSize;
                        }
                        else
                        {
                            GetComponent<Camera>().orthographicSize += delta * zoomScale;
                        }
                    }
                }
            }
        }
    }
}

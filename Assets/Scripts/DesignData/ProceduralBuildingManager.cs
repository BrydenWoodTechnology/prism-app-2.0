﻿using BrydenWoodUnity.GeometryManipulation;
using BrydenWoodUnity.Lighting;
using BrydenWoodUnity.UIElements;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Events;
using BT = BrydenWoodUnity.GeometryManipulation.ThreeJs;

namespace BrydenWoodUnity.DesignData
{
    /// <summary>
    /// A MonoBehaviour component which manages the procedural generation of buildings based on the input brief
    /// </summary>
    public class ProceduralBuildingManager : MonoBehaviour
    {
        #region Public Properties
        [Header("Prefabs:")]
        public GameObject proceduralFloorPrefab;
        public GameObject proceduralBuildingPrefab;
        public GameObject extrusionPrefab;
        public GameObject fullMap;

        [Header("UI References:")]
        public InputField offsetInputField;
        public InputField corridorInputField;
        public Dropdown apartmentDropdown;
        public Button swapApartments;
        public Button combineApartments;
        // public Button showInterior;
        public Button customPlButton;
        public Toggle briefToggle;
        public Toggle sunControlToggle;
        public GameObject roomNames;
        public Slider netToGrossSlider;
        public GameObject difWidthPopUp;
        public Text aptPreviewTitle;
        public CanvasGroup canvasGroupCP;
        [Tooltip("The chart with the types of the apartments")]
        public ApartmentTypesChart apartmentTypesChart;
        //public Button populateInteriorToggle;
        public Dropdown manufacturingDropDown;
        public Transform distanceUIParent;
        public GameObject amenitiesButtons;
        public Button rotateButton;
        public Button moveButton;
        public Button placeButton;
        public GameObject confirmRedraw;
        public GameObject confirmCustomRedraw;
        public SelectedInfoDisplay selectedInfoDisplay;
        public WallWidth constructionFeaturesElement;
        public SiteWidePanel sitePodiumPanel;
        public SiteWidePanel siteBasementPanel;
        public Button frontViewButton;
        public Button backViewButton;
        public Button topViewButton;
        public GameObject dimensionsParent;
        //public GameObject facadeToggle;
        public Dropdown aptLayoutDropDown;
        public Toggle m3LayoutToggle;
        public BlocksPanel blocksPanel;

        [Header("Scene References:")]
        public PolygonDrawer polygonDrawer;
        public PolygonDrawer siteDrawer;
        public DesignInputTabs designInputTabs;
        public ProceduralApartmentLayout proceduralApartment;
        public Transform repRootParent;
        public MapHandler mapHandler;
        public UndoActionLog undoActionLog;
        public GameObject cityObject;
        public GameObject ptalObject;
        public GameObject gridObject;
        public SunPosition sunPosition;
        public ConstructionTrafficLight volumetricTrafficLight;
        public ConstructionTrafficLight panelisedTrafficLight;
        public ConstructionTrafficLight platformsTrafficLight;
        public DrawBuildingLines drawBuildingLines;
        public ResourcesLoader resourcesLoader;
        public Transform apartmentTopCamera;
        public Transform apartmentFrontCamera;
        public Transform apartmentBackCamera;
        public Transform basementParent;
        public Transform podiumParent;
        public ApartmentTypeStats apartmentTypeStats;
        public Toggle moveCoresToggle;
        public Toggle deleteCoresToggle;

        [Header("Settings:")]
        public PreviewMode previewMode = PreviewMode.Buildings;
        public CoreAllignment coreAllignment = CoreAllignment.Centre;
        public LinearTypology typology = LinearTypology.Single;
        public float extraFloorDepth;
        public bool useCanvas = true;
        public float distanceOffset = 1.0f;
        public float lineWidth = 0.5f;
        public float offsetDistanceLine = 4;
        public LayerMask ptalLayerMask;
        public string numbersFormat = "{0:0,0}";
        public ExtraMassingType siteBasementType = ExtraMassingType.Footprint;
        public ExtraMassingType buildingBasementType = ExtraMassingType.Footprint;
        public ExtraMassingType sitePodiumType = ExtraMassingType.Footprint;
        public ExtraMassingType buildingPodiumType = ExtraMassingType.Footprint;
        public int siteBasementFloors;
        public float siteBasementFloor2Floor;
        public float siteBasementOffset;
        public int sitePodiumFloors = 3;
        public float sitePodiumFloor2Floor = 4.5f;
        public float sitePodiumOffset;
        public bool customFloorPodium;
        public bool customFloorBasement;
        public BuildingType aptPreviewBuildingType = BuildingType.Linear;

        public const float SqMeterToSqFoot = 10.7639f;
        public const float SqMeterToHectares = 0.0001f;

        public ProceduralBuilding CurrentBuilding
        {
            get
            {
                if (proceduralBuildings != null && proceduralBuildings.Count > 0)
                    return proceduralBuildings[currentBuildingIndex];
                else
                    return null;
            }
        }
        //[HideInInspector]

        public float offsetDistance
        {
            get
            {
                if (proceduralBuildings != null && proceduralBuildings.Count > 0)
                {
                    return proceduralBuildings[currentBuildingIndex].GetOffsetDistance();
                }
                else
                {
                    return m_aptDepth;
                }
            }
            set
            {
                if (proceduralBuildings != null && proceduralBuildings.Count > 0)
                {
                    proceduralBuildings[currentBuildingIndex].SetOffsetDistance(value);
                }
                else
                {
                    m_aptDepth = value;
                }
            }
        }
        private float m_aptDepth = 7.2f;
        private float mansion_aptDepth = 11.5f;
        //[HideInInspector]
        public float corridorWidth
        {
            get
            {
                if (proceduralBuildings != null && proceduralBuildings.Count > 0)
                {
                    return proceduralBuildings[currentBuildingIndex].corridorWidth;
                }
                else
                {
                    return m_crdrWidth;
                }
            }
            set
            {
                if (proceduralBuildings != null && proceduralBuildings.Count > 0)
                {
                    proceduralBuildings[currentBuildingIndex].corridorWidth = value;
                }
                else
                {
                    m_crdrWidth = value;
                }
            }
        }
        private float m_crdrWidth = 1.2f;
        public float CoreWidth
        {
            get
            {
                if (CurrentBuilding != null)
                {
                    switch (CurrentBuilding.buildingType)
                    {
                        case BuildingType.Linear:
                            return Standards.TaggedObject.CoreSizesPerBuildingType[BuildingType.Linear][0];
                        case BuildingType.Mansion:
                            return Standards.TaggedObject.CoreSizesPerBuildingType[BuildingType.Mansion][0];
                        case BuildingType.Tower:
                            return Standards.TaggedObject.CoreSizesPerBuildingType[BuildingType.Tower][0];
                        default:
                            return Standards.TaggedObject.CoreSizesPerBuildingType[BuildingType.Linear][0];
                            break;
                    }
                }
                else
                {
                    return Standards.TaggedObject.CoreSizesPerBuildingType[BuildingType.Linear][0];
                }
            }
            set
            {
                if (CurrentBuilding != null)
                {
                    switch (CurrentBuilding.buildingType)
                    {
                        case BuildingType.Linear:
                            Standards.TaggedObject.CoreSizesPerBuildingType[BuildingType.Linear][0] = value;
                            break;
                        case BuildingType.Mansion:
                            Standards.TaggedObject.CoreSizesPerBuildingType[BuildingType.Mansion][0] = value;
                            break;
                        case BuildingType.Tower:
                            Standards.TaggedObject.CoreSizesPerBuildingType[BuildingType.Tower][0] = value;
                            break;
                        default:
                            Standards.TaggedObject.CoreSizesPerBuildingType[BuildingType.Linear][0] = value;
                            break;
                    }
                }
                else
                {
                    Standards.TaggedObject.CoreSizesPerBuildingType[BuildingType.Linear][0] = value;
                }
            }
        }
        private float m_coreWidth = 4.8f;
        public float CoreLength
        {
            get
            {
                if (CurrentBuilding != null)
                {
                    switch (CurrentBuilding.buildingType)
                    {
                        case BuildingType.Linear:
                            return Standards.TaggedObject.CoreSizesPerBuildingType[BuildingType.Linear][1];
                        case BuildingType.Mansion:
                            return Standards.TaggedObject.CoreSizesPerBuildingType[BuildingType.Mansion][1];
                        case BuildingType.Tower:
                            return Standards.TaggedObject.CoreSizesPerBuildingType[BuildingType.Tower][1];
                        default:
                            return Standards.TaggedObject.CoreSizesPerBuildingType[BuildingType.Linear][1];
                    }
                }
                else
                {
                    return Standards.TaggedObject.CoreSizesPerBuildingType[BuildingType.Linear][1];
                }
            }
            set
            {
                if (CurrentBuilding != null)
                {
                    switch (CurrentBuilding.buildingType)
                    {
                        case BuildingType.Linear:
                            Standards.TaggedObject.CoreSizesPerBuildingType[BuildingType.Linear][1] = value;
                            break;
                        case BuildingType.Mansion:
                            Standards.TaggedObject.CoreSizesPerBuildingType[BuildingType.Mansion][1] = value;
                            break;
                        case BuildingType.Tower:
                            Standards.TaggedObject.CoreSizesPerBuildingType[BuildingType.Tower][1] = value;
                            break;
                        default:
                            Standards.TaggedObject.CoreSizesPerBuildingType[BuildingType.Linear][1] = value;
                            break;
                    }
                }
                else
                {
                    Standards.TaggedObject.CoreSizesPerBuildingType[BuildingType.Linear][1] = value;
                }
            }
        }
        private float m_coreLength = 7f;

        public Balconies exteriorBalconies
        {
            get
            {
                if (proceduralBuildings != null && proceduralBuildings.Count > currentBuildingIndex && proceduralBuildings[currentBuildingIndex] != null)
                {
                    return proceduralBuildings[currentBuildingIndex].balconies;
                }
                else
                {
                    return Balconies.External;
                }
            }
        }
        public List<Transform> buildings { get; set; }
        public ProceduralFloor currentFloor
        {
            get
            {
                if (currentBuildingIndex >= proceduralBuildings.Count)
                {
                    return null;
                }
                else
                {
                    return proceduralBuildings[currentBuildingIndex].currentFloor;
                }
            }
        }
        public string currentFloorKey { get; set; }
        public string stateData { get; set; }
        public Dictionary<string, ProceduralFloor> floors { get; set; }
        public Dictionary<string, FloorLayoutState> floorLayoutStates { get; set; }
        public List<Polygon> masterPolylines { get; set; }
        public List<ProceduralBuilding> proceduralBuildings { get; set; }
        public int currentBuildingIndex { get; set; }
        /// <summary>
        /// The maximum apartment width based on the brief of the current building
        /// </summary>
        public float currentMaximumWidth
        {
            get
            {
                return proceduralBuildings[currentBuildingIndex].currentMaximumWidth;
            }
        }

        public float prevMinimumWidth { get; private set; }

        /// <summary>
        /// The polyline parameters of the cores for the current building
        /// </summary>
        public List<float> currentCoresParameters
        {
            get
            {
                return proceduralBuildings[currentBuildingIndex].floors[0].coresParams;
            }
        }

        /// <summary>
        /// The length between the corridors for the current floor
        /// </summary>
        public float currentCoreLength
        {
            get
            {
                return proceduralBuildings[currentBuildingIndex].floors[0].coreLength;
            }
        }

        /// <summary>
        /// The corridor width of the current floor
        /// </summary>
        public float currentCorridorWidth
        {
            get
            {
                return proceduralBuildings[currentBuildingIndex].floors[0].corridorWidth;
            }
        }

        /// <summary>
        /// The master-floor (ground floor) for the current building
        /// </summary>
        public ProceduralFloor currentMasterFloor
        {
            get
            {
                return proceduralBuildings[currentBuildingIndex].floors[0];
            }
        }
        public bool populateApartments { get; set; }
        public List<Text> distancesUI { get; private set; }
        public Material distancesMaterial { get; private set; }
        public Polygon sitePolygon { get; set; }
        public SiteBasement siteBasement { get; set; }
        public SitePodium sitePodium { get; set; }
        public float SiteBasementArea
        {
            get
            {
                if (siteBasement != null)
                {
                    float area = siteBasement.FloorArea;
                    if (siteBasement.buildings.Count > 0)
                    {
                        for (int i = 0; i < siteBasement.buildings.Count; i++)
                        {
                            if (siteBasement.buildings[i].basement != null)
                                area -= siteBasement.buildings[i].basement.GEA;
                        }
                    }
                    area *= siteBasement.floors;
                    return area;
                }
                else return 0;
            }
        }
        public float SitePodiumArea
        {
            get
            {
                if (sitePodium != null)
                {
                    return sitePodium.FloorArea * sitePodium.floors;
                }
                else return 0;
            }
        }
        public ApartmentViewMode apartmentViewMode;
        public string PodiumUses
        {
            get
            {
                Dictionary<string, float> uses = new Dictionary<string, float>();
                float area = SitePodiumArea;
                if (sitePodium != null)
                {
                    foreach (var item in sitePodium.use)
                    {
                        if (uses.ContainsKey(item.Key))
                        {
                            uses[item.Key] += item.Value * 0.01f * area;
                        }
                        else
                        {
                            uses.Add(item.Key, item.Value * 0.01f * area);
                        }
                    }
                }
                for (int i = 0; i < proceduralBuildings.Count; i++)
                {
                    if (proceduralBuildings[i].podium != null && proceduralBuildings[i].podium.percentages != null && proceduralBuildings[i].podium.hasGeometry)
                    {
                        float m_area = proceduralBuildings[i].podium.GEA;
                        foreach (var item in proceduralBuildings[i].podium.percentages)
                        {
                            if (uses.ContainsKey(item.Key))
                            {
                                uses[item.Key] += item.Value * 0.01f * m_area;
                            }
                            else
                            {
                                uses.Add(item.Key, item.Value * 0.01f * m_area);
                            }
                        }
                    }
                }
                string data = "Podium Uses:\r\n";
                foreach (var item in uses)
                {
                    data += string.Format("{0} = {1}m\xB2 / {2}ft\xB2\n",
                        item.Key,
                        String.Format(numbersFormat, Math.Round(item.Value)),
                        String.Format(numbersFormat, Math.Round(item.Value * SqMeterToSqFoot)));
                }
                return data;
            }
        }
        public string BasementUses
        {
            get
            {
                Dictionary<string, float> uses = new Dictionary<string, float>();
                float area = SiteBasementArea;
                if (siteBasement != null)
                {
                    foreach (var item in siteBasement.use)
                    {
                        if (uses.ContainsKey(item.Key))
                        {
                            uses[item.Key] += item.Value * 0.01f * area;
                        }
                        else
                        {
                            uses.Add(item.Key, item.Value * 0.01f * area);
                        }
                    }
                }
                for (int i = 0; i < proceduralBuildings.Count; i++)
                {
                    if (proceduralBuildings[i].basement != null && proceduralBuildings[i].basement.percentages != null && proceduralBuildings[i].basement.hasGeometry)
                    {
                        float m_area = proceduralBuildings[i].basement.GEA;
                        foreach (var item in proceduralBuildings[i].basement.percentages)
                        {
                            if (uses.ContainsKey(item.Key))
                            {
                                uses[item.Key] += item.Value * 0.01f * m_area;
                            }
                            else
                            {
                                uses.Add(item.Key, item.Value * 0.01f * m_area);
                            }
                        }
                    }
                }
                string data = "Basement Uses:\r\n";
                foreach (var item in uses)
                {
                    data += string.Format("{0} = {1}m\xB2 / {2}ft\xB2\n",
                        item.Key,
                        String.Format(numbersFormat, Math.Round(item.Value)),
                        String.Format(numbersFormat, Math.Round(item.Value * SqMeterToSqFoot)));
                }
                return data;
            }
        }

        public string SiteUsage
        {
            get
            {
                if (sitePolygon != null)
                {
                    float groundFloorArea = 0;
                    if (sitePodium != null && sitePodium.polygon != null)
                    {
                        groundFloorArea += sitePodium.polygon.Area;
                    }
                    for (int i = 0; i < proceduralBuildings.Count; i++)
                    {
                        if (sitePodium != null && sitePodium.buildings != null && sitePodium.buildings.Count > 0)
                        {
                            if (!sitePodium.buildings.Contains(proceduralBuildings[i]))
                            {
                                if (proceduralBuildings[i].podium != null && proceduralBuildings[i].podium.hasGeometry)
                                {
                                    groundFloorArea += proceduralBuildings[i].podium.GEA;
                                }
                                else
                                {
                                    if (proceduralBuildings[i].floors[0].hasGeometry)
                                    {
                                        groundFloorArea += proceduralBuildings[i].floors[i].GEA;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (proceduralBuildings[i].podium != null && proceduralBuildings[i].podium.hasGeometry)
                            {
                                groundFloorArea += proceduralBuildings[i].podium.GEA;
                            }
                            else
                            {
                                if (proceduralBuildings[i].floors.Count > 0)
                                {
                                    if (proceduralBuildings[i].floors[0].hasGeometry)
                                    {
                                        groundFloorArea += proceduralBuildings[i].floors[0].GEA;
                                    }
                                }
                            }
                        }
                    }
                    return Math.Round((groundFloorArea / sitePolygon.Area) * 100, 2).ToString();
                }
                else return "N/A";
            }
        }
        #endregion

        #region Private Properties
        private List<GameObject> apartmentsInteriors { get; set; }
        private ManufacturingSystem manufacturingSystem { get; set; }
        private List<ApartmentUnity> aptToSwap { get; set; }
        private Save load { get; set; }
        private string loadData { get; set; }
        private bool checkToLoad = false;
        private ProceduralFloor lastAddedFloor { get; set; }
        private List<float> perBuildingPlantArea { get; set; }

        private WaitForEndOfFrame waitFrame { get; set; }
        private List<GameObject> heightLines { get; set; }
        private GameObject currentDistance { get; set; }
        private Transform distancesParent { get; set; }
        private int buildingIDCounter { get; set; }
        private bool placingTower { get; set; }
        private bool placingMansionBlock { get; set; }
        private bool movingBuilding { get; set; }
        private bool rotatingBuilding { get; set; }
        private bool movingBasement { get; set; }
        private bool rotatingBasement { get; set; }
        private bool movingPodium { get; set; }
        private bool rotatingPodium { get; set; }
        private Plane towerPlane = new Plane(Vector3.up, Vector3.zero);

        private Vector3 originalBuildingEuler { get; set; }
        private Vector3 originalPos = Vector3.up;
        private Vector3 updatedPos { get; set; }
        private GameObject anchorObject { get; set; }
        private int prevBuildingSiblingIndex { get; set; }
        private bool moveCores { get; set; }
        private bool deleteCores { get; set; }
        #endregion

        #region Events
        /// <summary>
        /// An empty delegate to used be various events
        /// </summary>
        public delegate void EmptyDelegate();
        /// <summary>
        /// Triggered when the overall information about the building have been updated
        /// </summary>
        public event EmptyDelegate overallUpdated;
        /// <summary>
        /// Triggered when one of the floor layouts has changed
        /// </summary>
        public event EmptyDelegate apartmentLayoutChanged;
        /// <summary>
        /// Triggered when the selection has been cleared
        /// </summary>
        public event EmptyDelegate clearSelect;
        /// <summary>
        /// Triggered when at least on of the apartments of any of the floor layouts has changed
        /// </summary>
        public static event EmptyDelegate apartmentsChanged;
        /// <summary>
        /// Triggered when the area range for any of the apartment types has changed 
        /// </summary>
        public static event EmptyDelegate updateAreaRange;

        public delegate void OnPreviewChanged(PreviewMode previewMode);
        public static event OnPreviewChanged previewChanged;


        /// <summary>
        /// Triggered when custom basement polygon has been drawn
        /// </summary>
        //public event EmptyDelegate addBasementCustomPolygonInUI;
        //public delegate void OnUIcustomPolygonRemoval(int index);
        //public event OnUIcustomPolygonRemoval removeUIbasementCustomPolygon;
        public UnityEvent addUIBasementCustomPolygon;
        public UnityEvent addUIPodiumCustomPolygon;
        public OnCustomPolygonChanged removeUIBasementCustomPolygon;
        public OnCustomPolygonChanged removeUIPodiumCustomPolygon;

        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Awake()
        {
            AddEvents();
            buildings = new List<Transform>();
            floors = new Dictionary<string, ProceduralFloor>();
            floorLayoutStates = new Dictionary<string, FloorLayoutState>();
            proceduralBuildings = new List<ProceduralBuilding>();
            OnPreviewModeChanged(previewMode);
            masterPolylines = new List<Polygon>();
            //offsetDistance = 1.2f;
            prevMinimumWidth = (float)Standards.TaggedObject.ApartmentTypesMinimumSizes["1b2p"] / offsetDistance;
            waitFrame = new WaitForEndOfFrame();
            heightLines = new List<GameObject>();
            distancesParent = new GameObject("HeightLinesParent").transform;
            distancesUI = new List<Text>();
            anchorObject = new GameObject("Anchor");
        }

        private void OnDestroy()
        {
            //RemoveEvents();
            if (overallUpdated != null)
            {
                foreach (Delegate eh in overallUpdated.GetInvocationList())
                {
                    overallUpdated -= (EmptyDelegate)eh;
                }
            }
            if (apartmentLayoutChanged != null)
            {
                foreach (Delegate eh in apartmentLayoutChanged.GetInvocationList())
                {
                    apartmentLayoutChanged -= (EmptyDelegate)eh;
                }
            }
            if (clearSelect != null)
            {
                foreach (Delegate eh in clearSelect.GetInvocationList())
                {
                    clearSelect -= (EmptyDelegate)eh;
                }
            }
            if (apartmentsChanged != null)
            {
                foreach (Delegate eh in apartmentsChanged.GetInvocationList())
                {
                    apartmentsChanged -= (EmptyDelegate)eh;
                }
            }
            if (updateAreaRange != null)
            {
                foreach (Delegate eh in updateAreaRange.GetInvocationList())
                {
                    updateAreaRange -= (EmptyDelegate)eh;
                }
            }
            if (previewChanged != null)
            {
                foreach (Delegate eh in previewChanged.GetInvocationList())
                {
                    previewChanged -= (OnPreviewChanged)eh;
                }
            }
        }


        // Update is called once per frame
        void Update()
        {
            if (!EventSystem.current.IsPointerOverGameObject(-1))
            {
                if (placingTower)
                {
                    var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    float dist = 0;
                    if (sitePolygon != null || (sitePodium != null && sitePodium.polygon != null))
                    {
                        RaycastHit hit;
                        if (Physics.Raycast(ray, out hit, 1000, polygonDrawer.raycastingMask))
                        {
                            proceduralBuildings[currentBuildingIndex].transform.position = hit.point;

                            if (Input.GetMouseButtonDown(0))
                            {
                                PlaceTowerAt(hit.point);
                                canvasGroupCP.blocksRaycasts = true;

                            }
                        }
                    }
                    else
                    {
                        if (towerPlane.Raycast(ray, out dist))
                        {
                            proceduralBuildings[currentBuildingIndex].transform.position = ray.GetPoint(dist);

                            if (Input.GetMouseButtonDown(0))
                            {
                                PlaceTowerAt(ray.GetPoint(dist));
                                canvasGroupCP.blocksRaycasts = true;

                            }
                        }
                    }
                }

                if (placingMansionBlock)
                {
                    var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    float dist = 0;
                    if (sitePolygon != null || (sitePodium != null && sitePodium.polygon != null))
                    {
                        RaycastHit hit;
                        if (Physics.Raycast(ray, out hit, 1000, polygonDrawer.raycastingMask))
                        {
                            proceduralBuildings[currentBuildingIndex].transform.position = hit.point;

                            if (Input.GetMouseButtonDown(0))
                            {
                                PlaceMansionBlockAt(hit.point);
                                canvasGroupCP.blocksRaycasts = true;

                            }
                        }
                    }
                    else
                    {
                        if (towerPlane.Raycast(ray, out dist))
                        {
                            proceduralBuildings[currentBuildingIndex].transform.position = ray.GetPoint(dist);

                            if (Input.GetMouseButtonDown(0))
                            {
                                PlaceMansionBlockAt(ray.GetPoint(dist));
                                canvasGroupCP.blocksRaycasts = true;

                            }
                        }
                    }
                }

                if (movingBasement)
                {
                    var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    float dist = 0;
                    if (towerPlane.Raycast(ray, out dist))
                    {

                        if (Input.GetMouseButtonDown(0))
                        {
                            originalPos = ray.GetPoint(dist);
                            originalBuildingEuler = siteBasement.polygon.transform.position;
                        }

                        if (Input.GetMouseButton(0))
                        {
                            updatedPos = ray.GetPoint(dist);
                            Vector3 delta = updatedPos - originalPos;
                            siteBasement.polygon.transform.position = originalBuildingEuler + delta;
                        }

                        if (Input.GetMouseButtonUp(0) && originalPos != Vector3.up)
                        {
                            movingBasement = false;
                            originalPos = Vector3.up;
                            canvasGroupCP.blocksRaycasts = true;
                            siteBasement.polygon.UpdateVertexPositions();
                        }
                    }
                }

                if (movingPodium)
                {
                    var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    float dist = 0;
                    if (towerPlane.Raycast(ray, out dist))
                    {

                        if (Input.GetMouseButtonDown(0))
                        {
                            originalPos = ray.GetPoint(dist);
                            originalBuildingEuler = sitePodium.polygon.transform.position;
                        }

                        if (Input.GetMouseButton(0))
                        {
                            updatedPos = ray.GetPoint(dist);
                            Vector3 delta = updatedPos - originalPos;
                            sitePodium.polygon.transform.position = originalBuildingEuler + delta;
                        }

                        if (Input.GetMouseButtonUp(0) && originalPos != Vector3.up)
                        {
                            movingPodium = false;
                            originalPos = Vector3.up;
                            canvasGroupCP.blocksRaycasts = true;
                            sitePodium.polygon.UpdateVertexPositions();
                        }
                    }
                }

                if (movingBuilding)
                {
                    var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    float dist = 0;
                    if (towerPlane.Raycast(ray, out dist))
                    {

                        if (Input.GetMouseButtonDown(0))
                        {
                            originalPos = ray.GetPoint(dist);
                            originalBuildingEuler = CurrentBuilding.transform.position;
                        }

                        if (Input.GetMouseButton(0))
                        {
                            updatedPos = ray.GetPoint(dist);
                            Vector3 delta = updatedPos - originalPos;
                            CurrentBuilding.transform.position = originalBuildingEuler + delta;
                        }

                        if (Input.GetMouseButtonUp(0) && originalPos != Vector3.up)
                        {
                            if (CurrentBuilding.buildingType == BuildingType.Tower)
                            {
                                movingBuilding = false;
                                originalPos = Vector3.up;

                                for (int i = 0; i < CurrentBuilding.floors.Count; i++)
                                {
                                    CurrentBuilding.floors[i].UpdateFloorOutline(previewMode);
                                }
                                if (CurrentBuilding.podium != null)
                                {
                                    if (CurrentBuilding.podium.hasCustomExterior)
                                    {
                                        CurrentBuilding.podium.exteriorPolygon.UpdateVertexPositions();
                                    }
                                    CurrentBuilding.podium.UpdateFloorOutline(previewMode);
                                }
                                if (CurrentBuilding.basement != null)
                                {
                                    if (CurrentBuilding.basement.hasCustomExterior)
                                    {
                                        CurrentBuilding.basement.exteriorPolygon.UpdateVertexPositions();
                                    }
                                    CurrentBuilding.basement.UpdateFloorOutline(previewMode);
                                }
                                CurrentBuilding.UpdateHeightLine(null);
                            }
                            else if (CurrentBuilding.buildingType == BuildingType.Mansion)
                            {
                                movingBuilding = false;
                                originalPos = Vector3.up;

                                for (int i = 0; i < CurrentBuilding.floors.Count; i++)
                                {
                                    CurrentBuilding.floors[i].UpdateFloorOutline(previewMode);
                                }
                                if (CurrentBuilding.podium != null)
                                {
                                    if (CurrentBuilding.podium.hasCustomExterior)
                                    {
                                        CurrentBuilding.podium.exteriorPolygon.UpdateVertexPositions();
                                    }
                                    CurrentBuilding.podium.UpdateFloorOutline(previewMode);
                                }
                                if (CurrentBuilding.basement != null)
                                {
                                    if (CurrentBuilding.basement.hasCustomExterior)
                                    {
                                        CurrentBuilding.basement.exteriorPolygon.UpdateVertexPositions();
                                    }
                                    CurrentBuilding.basement.UpdateFloorOutline(previewMode);
                                }
                                CurrentBuilding.UpdateHeightLine(null);
                            }
                            else
                            {

                                movingBuilding = false;
                                originalPos = Vector3.up;
                                for (int i = 0; i < CurrentBuilding.floors.Count; i++)
                                {
                                    CurrentBuilding.floors[i].centreLine.UpdateVertexPositions();
                                    CurrentBuilding.floors[i].UpdateFloorOutline(previewMode);
                                    CurrentBuilding.GetCoreDistances();
                                }
                                if (CurrentBuilding.podium != null)
                                {
                                    if (CurrentBuilding.podium.hasCustomExterior)
                                    {
                                        CurrentBuilding.podium.exteriorPolygon.UpdateVertexPositions();
                                    }
                                    CurrentBuilding.podium.UpdateFloorOutline(previewMode);
                                    CurrentBuilding.podium.centreLine.UpdateVertexPositions();
                                }
                                if (CurrentBuilding.basement != null)
                                {
                                    if (CurrentBuilding.basement.hasCustomExterior)
                                    {
                                        CurrentBuilding.basement.exteriorPolygon.UpdateVertexPositions();
                                    }
                                    CurrentBuilding.basement.UpdateFloorOutline(previewMode);
                                    CurrentBuilding.basement.centreLine.UpdateVertexPositions();
                                }
                                //currentBuilding.floors[0].UpdatePlantRoom();
                                CurrentBuilding.UpdateHeightLine(CurrentBuilding.masterPolyline);
                            }
                            canvasGroupCP.blocksRaycasts = true;
                        }
                    }
                }

                if (rotatingBasement)
                {
                    var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    float dist = 0;
                    if (towerPlane.Raycast(ray, out dist))
                    {
                        if (Input.GetMouseButtonDown(0))
                        {
                            originalBuildingEuler = siteBasement.polygon.transform.eulerAngles;
                            originalPos = ray.GetPoint(dist) - siteBasement.polygon.transform.position;
                        }

                        if (Input.GetMouseButton(0))
                        {
                            if (originalPos == Vector3.zero)
                            {
                                originalBuildingEuler = siteBasement.polygon.transform.eulerAngles;
                                originalPos = ray.GetPoint(dist) - siteBasement.polygon.transform.position;
                            }
                            updatedPos = ray.GetPoint(dist) - siteBasement.polygon.transform.position;
                            var angle = Vector3.Angle(originalPos, updatedPos);
                            if (Vector3.Cross(originalPos, updatedPos).y < 0)
                            {
                                angle *= -1;
                            }
                            siteBasement.polygon.transform.eulerAngles = originalBuildingEuler + new Vector3(0, angle/* * Mathf.Rad2Deg*/, 0);
                        }
                        if (Input.GetMouseButtonUp(0) && originalPos != Vector3.up)
                        {
                            rotatingPodium = false;
                            originalPos = Vector3.up;
                            canvasGroupCP.blocksRaycasts = true;
                            siteBasement.polygon.UpdateVertexPositions();
                        }
                    }
                }

                if (rotatingPodium)
                {
                    var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    float dist = 0;
                    if (towerPlane.Raycast(ray, out dist))
                    {
                        if (Input.GetMouseButtonDown(0))
                        {
                            originalBuildingEuler = sitePodium.polygon.transform.eulerAngles;
                            originalPos = ray.GetPoint(dist) - sitePodium.polygon.transform.position;
                        }

                        if (Input.GetMouseButton(0))
                        {
                            if (originalPos == Vector3.zero)
                            {
                                originalBuildingEuler = sitePodium.polygon.transform.eulerAngles;
                                originalPos = ray.GetPoint(dist) - sitePodium.polygon.transform.position;
                            }
                            updatedPos = ray.GetPoint(dist) - sitePodium.polygon.transform.position;
                            var angle = Vector3.Angle(originalPos, updatedPos);
                            if (Vector3.Cross(originalPos, updatedPos).y < 0)
                            {
                                angle *= -1;
                            }
                            sitePodium.polygon.transform.eulerAngles = originalBuildingEuler + new Vector3(0, angle/* * Mathf.Rad2Deg*/, 0);
                        }
                        if (Input.GetMouseButtonUp(0) && originalPos != Vector3.up)
                        {
                            rotatingPodium = false;
                            originalPos = Vector3.up;
                            canvasGroupCP.blocksRaycasts = true;
                            sitePodium.polygon.UpdateVertexPositions();
                        }
                    }
                }

                if (rotatingBuilding)
                {
                    var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    float dist = 0;
                    if (towerPlane.Raycast(ray, out dist))
                    {
                        if (CurrentBuilding.buildingType == BuildingType.Tower)
                        {
                            if (Input.GetMouseButtonDown(0))
                            {
                                originalBuildingEuler = CurrentBuilding.transform.eulerAngles;
                                originalPos = ray.GetPoint(dist) - CurrentBuilding.transform.position;
                            }

                            if (Input.GetMouseButton(0))
                            {
                                if (originalPos == Vector3.zero)
                                {
                                    originalBuildingEuler = CurrentBuilding.transform.eulerAngles;
                                    originalPos = ray.GetPoint(dist) - CurrentBuilding.transform.position;
                                }
                                updatedPos = ray.GetPoint(dist) - CurrentBuilding.transform.position;
                                var angle = Vector3.Angle(originalPos, updatedPos);
                                if (Vector3.Cross(originalPos, updatedPos).y < 0)
                                {
                                    angle *= -1;
                                }
                                CurrentBuilding.transform.eulerAngles = originalBuildingEuler + new Vector3(0, angle/* * Mathf.Rad2Deg*/, 0);
                            }
                            if (Input.GetMouseButtonUp(0) && originalPos != Vector3.up)
                            {
                                rotatingBuilding = false;
                                originalPos = Vector3.up;
                                for (int i = 0; i < CurrentBuilding.floors.Count; i++)
                                {
                                    CurrentBuilding.floors[i].UpdateFloorOutline(previewMode);
                                }
                                if (CurrentBuilding.podium != null)
                                {
                                    if (CurrentBuilding.podium.hasCustomExterior)
                                    {
                                        CurrentBuilding.podium.exteriorPolygon.UpdateVertexPositions();
                                    }
                                    CurrentBuilding.podium.UpdateFloorOutline(previewMode);
                                }
                                if (CurrentBuilding.basement != null)
                                {
                                    if (CurrentBuilding.basement.hasCustomExterior)
                                    {
                                        CurrentBuilding.basement.exteriorPolygon.UpdateVertexPositions();
                                    }
                                    CurrentBuilding.basement.UpdateFloorOutline(previewMode);
                                }
                                CurrentBuilding.UpdateHeightLine(null);
                                canvasGroupCP.blocksRaycasts = true;
                            }
                        }
                        else if (CurrentBuilding.buildingType == BuildingType.Mansion)
                        {
                            if (Input.GetMouseButtonDown(0))
                            {
                                originalBuildingEuler = CurrentBuilding.transform.eulerAngles;
                                originalPos = ray.GetPoint(dist) - CurrentBuilding.transform.position;
                            }

                            if (Input.GetMouseButton(0))
                            {
                                if (originalPos == Vector3.zero)
                                {
                                    originalBuildingEuler = CurrentBuilding.transform.eulerAngles;
                                    originalPos = ray.GetPoint(dist) - CurrentBuilding.transform.position;
                                }
                                updatedPos = ray.GetPoint(dist) - CurrentBuilding.transform.position;
                                var angle = Vector3.Angle(originalPos, updatedPos);
                                if (Vector3.Cross(originalPos, updatedPos).y < 0)
                                {
                                    angle *= -1;
                                }
                                CurrentBuilding.transform.eulerAngles = originalBuildingEuler + new Vector3(0, angle/* * Mathf.Rad2Deg*/, 0);
                            }
                            if (Input.GetMouseButtonUp(0) && originalPos != Vector3.up)
                            {
                                rotatingBuilding = false;
                                originalPos = Vector3.up;
                                for (int i = 0; i < CurrentBuilding.floors.Count; i++)
                                {
                                    CurrentBuilding.floors[i].UpdateFloorOutline(previewMode);
                                }
                                if (CurrentBuilding.podium != null)
                                {
                                    if (CurrentBuilding.podium.hasCustomExterior)
                                    {
                                        CurrentBuilding.podium.exteriorPolygon.UpdateVertexPositions();
                                    }
                                    CurrentBuilding.podium.UpdateFloorOutline(previewMode);
                                }
                                if (CurrentBuilding.basement != null)
                                {
                                    if (CurrentBuilding.basement.hasCustomExterior)
                                    {
                                        CurrentBuilding.basement.exteriorPolygon.UpdateVertexPositions();
                                    }
                                    CurrentBuilding.basement.UpdateFloorOutline(previewMode);
                                }
                                CurrentBuilding.UpdateHeightLine(null);
                                canvasGroupCP.blocksRaycasts = true;
                            }
                        }
                        else
                        {
                            if (Input.GetMouseButtonDown(0))
                            {
                                originalBuildingEuler = CurrentBuilding.transform.eulerAngles;
                                originalPos = ray.GetPoint(dist) - CurrentBuilding.transform.position;
                            }

                            if (Input.GetMouseButton(0))
                            {
                                if (originalPos == Vector3.zero)
                                {
                                    originalBuildingEuler = CurrentBuilding.transform.eulerAngles;
                                    originalPos = ray.GetPoint(dist) - CurrentBuilding.transform.position;
                                }
                                updatedPos = ray.GetPoint(dist) - CurrentBuilding.transform.position;
                                var angle = Vector3.Angle(originalPos, updatedPos);
                                if (Vector3.Cross(originalPos, updatedPos).y < 0)
                                {
                                    angle *= -1;
                                }
                                CurrentBuilding.transform.eulerAngles = originalBuildingEuler + new Vector3(0, angle, 0);
                            }
                            if (Input.GetMouseButtonUp(0) && originalPos != Vector3.up)
                            {
                                rotatingBuilding = false;
                                originalPos = Vector3.up;
                                for (int i = 0; i < CurrentBuilding.floors.Count; i++)
                                {
                                    CurrentBuilding.floors[i].centreLine.UpdateVertexPositions();
                                    CurrentBuilding.floors[i].UpdateFloorOutline(previewMode);
                                }
                                if (CurrentBuilding.podium != null)
                                {
                                    CurrentBuilding.podium.centreLine.UpdateVertexPositions();
                                    if (CurrentBuilding.podium.hasCustomExterior)
                                    {
                                        CurrentBuilding.podium.exteriorPolygon.UpdateVertexPositions();
                                    }
                                    CurrentBuilding.podium.UpdateFloorOutline(previewMode);
                                }
                                if (CurrentBuilding.basement != null)
                                {

                                    CurrentBuilding.basement.centreLine.UpdateVertexPositions();
                                    if (CurrentBuilding.basement.hasCustomExterior)
                                    {
                                        CurrentBuilding.basement.exteriorPolygon.UpdateVertexPositions();
                                    }
                                    CurrentBuilding.basement.UpdateFloorOutline(previewMode);
                                }
                                //currentBuilding.floors[0].UpdatePlantRoom();
                                CurrentBuilding.UpdateHeightLine(CurrentBuilding.masterPolyline);
                                canvasGroupCP.blocksRaycasts = true;
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region Methods called by Unity UI

        /// <summary>
        /// Deletes the podium of the current building
        /// </summary>
        public void DeleteBuildingPodium()
        {
            if (CurrentBuilding != null)
                CurrentBuilding.DeletePodium();
        }
        /// <summary>
        /// Deletes the basement of the current building
        /// </summary>
        public void DeleteBuildingBasement()
        {
            if (CurrentBuilding != null)
                CurrentBuilding.DeleteBasement();
        }
        /// <summary>
        /// Deletes the site-wide podium
        /// </summary>
        public void DeleteSitePodium()
        {
            if (sitePodium != null)
            {
                if (sitePodium.polygon != null)
                    Destroy(sitePodium.polygon.gameObject);
                for (int i = 0; i < proceduralBuildings.Count; i++)
                {
                    if (proceduralBuildings[i].sitePodium == sitePodium)
                    {
                        proceduralBuildings[i].RemoveSitePodium();
                    }
                }
                sitePodium = null;
                sitePodiumPanel.DeletePodium();
            }
        }
        /// <summary>
        /// Deletes the site wide basement
        /// </summary>
        public void DeleteSiteBasement()
        {
            if (siteBasement != null)
            {
                if (siteBasement.polygon != null)
                    Destroy(siteBasement.polygon.gameObject);
                for (int i = 0; i < proceduralBuildings.Count; i++)
                {
                    if (proceduralBuildings[i].siteBasement == siteBasement)
                    {
                        proceduralBuildings[i].RemoveSiteBasement();
                    }
                }
                siteBasement = null;
                siteBasementPanel.DeleteBasement();
            }
        }
        /// <summary>
        /// Adds a core to the current building
        /// </summary>
        public void AddCore()
        {
            CurrentBuilding.currentFloor.OnCoreAdded(new CoreAddedEventArgs { coreParam = 0.05f });
        }
        /// <summary>
        /// Moves the cores of the current building
        /// </summary>
        /// <param name="value">Whether the cores should be allowed to move</param>
        public void MoveCores(bool value)
        {
            if (CurrentBuilding.buildingType == BuildingType.Linear)
            {
                moveCores = value;
                MoveObjects.TaggedObject.constraint = moveCores ? CurrentBuilding.masterPolyline : null;
                Selector.TaggedObject.enabled = moveCores;
                Selector.TaggedObject.ToggleCoreDelete(false);
                Selector.TaggedObject.ToggleCoreSelection(moveCores);
            }
            else moveCoresToggle.SetValue(false);
        }
        /// <summary>
        /// Toggles the distances between the cores ofthe current building
        /// </summary>
        /// <param name="show">Whether the distances should be shown</param>
        public void ShowCoreDistances(bool show)
        {
            CurrentBuilding.ShowCoreDistances(show);
        }
        /// <summary>
        /// Delete the cores of the current building
        /// </summary>
        /// <param name="value">Whether the cores should be allowed to be deleted</param>
        public void DeleteCores(bool value)
        {
            if (CurrentBuilding.buildingType == BuildingType.Linear)
            {
                deleteCores = value;
                MoveObjects.TaggedObject.constraint = deleteCores ? CurrentBuilding.masterPolyline : null;
                Selector.TaggedObject.enabled = deleteCores;
                Selector.TaggedObject.ToggleCoreSelection(false);
                Selector.TaggedObject.ToggleCoreDelete(deleteCores);

            }
            else deleteCoresToggle.SetValue(false);
        }

        /// <summary>
        /// Places a tower at a given point
        /// </summary>
        /// <param name="point">The centre of the tower</param>
        public void PlaceTowerAt(Vector3 point)
        {
            CurrentBuilding.transform.position = point;
            //currentBuilding.baseHeight = point.y;
            placingTower = false;
            CurrentBuilding.PlaceTower();
            rotateButton.interactable = true;
            placeButton.gameObject.SetActive(false);
            moveButton.gameObject.SetActive(true);
            CheckIfBuildingOverBasement(CurrentBuilding);
            CheckIfBuildingOverPodium(CurrentBuilding);
            StartCoroutine(OverallUpdate());
        }
        /// <summary>
        /// Places a mansion block building at a specified position
        /// </summary>
        /// <param name="point">The position to place the mansion block building</param>
        public void PlaceMansionBlockAt(Vector3 point)
        {
            CurrentBuilding.transform.position = point;
            //currentBuilding.baseHeight = point.y;
            placingMansionBlock = false;
            CurrentBuilding.PlaceMansionBlock();
            rotateButton.interactable = true;
            placeButton.gameObject.SetActive(false);
            moveButton.gameObject.SetActive(true);
            CheckIfBuildingOverBasement(CurrentBuilding);
            CheckIfBuildingOverPodium(CurrentBuilding);
            StartCoroutine(OverallUpdate());
        }

        /// <summary>
        /// Called to reset the scene
        /// </summary>
        public void OnReset()
        {
            GameObject gObj = new GameObject("NotFirstStart");
            DontDestroyOnLoad(gObj);
            resourcesLoader.OnReset();
        }

        private void OnResetForLoad()
        {
            resourcesLoader.OnReset();
        }

        /// <summary>
        /// Called when the Load Project button is pressed
        /// </summary>
        public void OnLoadProject()
        {
            if (designInputTabs.buildings.Count != 0)
            {
                GameObject gObj = new GameObject("ShouldLoadFiles");
                DontDestroyOnLoad(gObj);
                OnResetForLoad();
            }
            else
            {
                resourcesLoader.SetSaveFiles();
            }
        }

        /// <summary>
        /// Called when a building has been selected
        /// </summary>
        /// <param name="building">The selected building</param>
        /// <returns>IEnumerator (Coroutine)</returns>
        public IEnumerator OnBuildingSelected(ProceduralBuilding building)
        {
            yield return StartCoroutine(proceduralBuildings[currentBuildingIndex].Deselect());
            currentBuildingIndex = proceduralBuildings.IndexOf(building);
            if (!building.gameObject.activeSelf)
            {
                building.gameObject.SetActive(true);
            }
            customPlButton.interactable = building.hasGeometry && building.currentFloor.Index != 0;
        }

        /// <summary>
        /// Called to initiate the saving process
        /// </summary>
        public void OnSave()
        {
            Save newSave = new Save();
            newSave.siteState = SerializeSite();
            newSave.geometryState = SerializeStates();
            newSave.briefState = designInputTabs.SerializeBuildings();
            newSave.standardsState = SerializeStandards();
            string data = JsonConvert.SerializeObject(newSave, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            resourcesLoader.OnSaveData(newSave, "PRISM_" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss"));
        }

        /// <summary>
        /// Called to resolve all residual spaces
        /// </summary>
        public void ResolveResiduals()
        {
            StartCoroutine(ResolveResidualsIteratively());
        }

        /// <summary>
        /// Called when the Resolve and Retain button is pressed
        /// </summary>
        public void ResolveAndRetain()
        {
            StartCoroutine(ResolveAndRetainIteratively());
        }

        /// <summary>
        /// Called to swap apartments
        /// </summary>
        public void SwapApartments()
        {
            Vector3 start;
            Vector3 axis;
            var corners0 = aptToSwap[0].GetCorners();
            var corners1 = aptToSwap[1].GetCorners();
            SwapAction m_action = new SwapAction();
            m_action.floor = aptToSwap[0].ProceduralFloor;
            m_action.prevPos = new Vector3[] { aptToSwap[0].transform.position, aptToSwap[1].transform.position };
            m_action.aptNames = new string[] { aptToSwap[0].name, aptToSwap[1].name };
            undoActionLog.AddAction(m_action);
            bool dir = ((corners1[3] - corners0[0]).magnitude) > ((corners0[3] - corners1[0]).magnitude);
            if (dir)//corners0[3] == corners1[0])
            {
                start = aptToSwap[1].transform.position;
                axis = (aptToSwap[0].transform.position - aptToSwap[1].transform.position).normalized;

                aptToSwap[0].Move(start);
                aptToSwap[1].Move(start + axis * aptToSwap[0].Width);
            }
            else
            {
                start = aptToSwap[0].transform.position;
                axis = (aptToSwap[1].transform.position - aptToSwap[0].transform.position).normalized;

                aptToSwap[1].Move(start);
                aptToSwap[0].Move(start + axis * aptToSwap[1].Width);
            }
            if (clearSelect != null)
            {
                clearSelect();
            }

            StartCoroutine(OnApartmentsChanged());
        }

        /// <summary>
        /// Called to initiate the export process
        /// </summary>
        public void OnExportObj()
        {
            StartCoroutine(ExportModel());
        }

        /// <summary>
        /// Called when the Export Raw Data button is pressed
        /// </summary>
        public void OnExportRawData()
        {
            StartCoroutine(ExportRawData());
        }

        /// <summary>
        /// Called when the Export PDF button is pressed
        /// </summary>
        public void OnExportPDF()
        {
            ExportPdfReport();
        }

        /// <summary>
        /// Called to start the process of defining a master-polyline
        /// </summary>
        public void StartMasterPolyline()
        {
            if (proceduralBuildings[currentBuildingIndex].hasGeometry)
            {
                confirmRedraw.SetActive(true);
            }
            else
            {
                string key = designInputTabs.buildingIndex + "," + designInputTabs.floorIndex;

                polygonDrawer.showDistances = true;

                if (masterPolylines == null)
                {
                    masterPolylines = new List<Polygon>();
                }

                //if (float.TryParse(offsetInputField.text, out m_aptDepth))
                //{
                //    offsetDistance = m_aptDepth;
                //}
                //else
                //{
                //    offsetDistance = 7.2f;
                //}
                if (sitePolygon != null || (sitePodium != null && sitePodium.polygon != null))
                {
                    polygonDrawer.usePlane = false;
                }
                else
                {
                    polygonDrawer.SetPlane(Vector3.zero, DrawingPlane.XZ);
                    polygonDrawer.usePlane = true;
                }
                polygonDrawer.StartPolygon();
            }
        }

        /// <summary>
        /// Called from the UI to confirm the redrawing of the master polyline of the building
        /// </summary>
        public void ConfirmRedrawMasterPolyline()
        {
            confirmRedraw.SetActive(false);
            string key = designInputTabs.buildingIndex + "," + designInputTabs.floorIndex;

            polygonDrawer.showDistances = true;

            if (masterPolylines == null)
            {
                masterPolylines = new List<Polygon>();
            }

            //if (float.TryParse(offsetInputField.text, out m_aptDepth))
            //{
            //    leftOffsetDistance = m_aptDepth;
            //}
            //else
            //{
            //    leftOffsetDistance = 7.2f;
            //}

            polygonDrawer.StartPolygon();
        }

        /// <summary>
        /// Called from the UI to confirm the re-drawing of the custom offset polyline
        /// </summary>
        public void ConfirmRedrawCustomPolyline()
        {
            confirmCustomRedraw.SetActive(false);
            if (proceduralBuildings[currentBuildingIndex].hasGeometry)
            {
                polygonDrawer.temp_Y = proceduralBuildings[currentBuildingIndex].buildingElement.GetYForLevel(proceduralBuildings[currentBuildingIndex].currentFloor.Index);
                float maxWidth = currentMaximumWidth;
                if (maxWidth > 10)
                {
                    maxWidth = 10;
                }
                polygonDrawer.StartPolygonSegment(proceduralBuildings[currentBuildingIndex].masterPolyline, currentCoresParameters, currentCoreLength / 2.0f, currentCorridorWidth, maxWidth);
            }
        }

        /// <summary>
        /// Called when the Create Custom C-Line button is pressed
        /// </summary>
        public void StartCustomPolyline()
        {
            if (proceduralBuildings[currentBuildingIndex].hasGeometry)
            {
                if (proceduralBuildings[currentBuildingIndex].currentFloor.hasGeometry)
                {
                    confirmCustomRedraw.SetActive(true);
                }
                else
                {
                    polygonDrawer.temp_Y = CurrentBuilding.buildingElement.GetYForLevel(CurrentBuilding.currentFloor.Index);
                    float maxWidth = currentMaximumWidth;
                    if (maxWidth > 10)
                    {
                        maxWidth = 10;
                    }
                    polygonDrawer.StartPolygonSegment(proceduralBuildings[currentBuildingIndex].masterPolyline, currentCoresParameters, currentCoreLength / 2.0f, currentCorridorWidth, maxWidth);
                }
            }
        }

        /// <summary>
        /// Called to refresh the apartment distribution along the floors
        /// </summary>
        public void Refresh()
        {
            StartCoroutine(Refresh(currentMaximumWidth));
        }

        /// <summary>
        /// Recalculates the geometry of all floors in the project
        /// </summary>
        /// <param name="maxWidth">The maximum apartment width</param>
        /// <returns>IEnumerator</returns>
        public IEnumerator Refresh(float maxWidth)
        {
            float.TryParse(offsetInputField.text, out m_aptDepth);
            float.TryParse(corridorInputField.text, out m_crdrWidth);
            //SetBuildingAspects();
            corridorWidth = m_crdrWidth;
            offsetDistance = m_aptDepth;
            proceduralBuildings[currentBuildingIndex].corridorWidth = m_crdrWidth;
            yield return StartCoroutine(proceduralBuildings[currentBuildingIndex].Refresh(m_aptDepth));
            OnPreviewModeChanged(previewMode);
            StartCoroutine(OverallUpdate());
        }

        //-------- BASEMENT - PODIUM ---------//
        /// <summary>
        /// Invoke function to generate a basement at the size of the site
        /// </summary>
        public void GenerateSiteFootprintBasement()
        {
            SetSiteBasementType(ExtraMassingType.Footprint);
            GetSiteFootprintBasement(siteBasementFloors, siteBasementFloor2Floor, siteBasementOffset);
        }
        /// <summary>
        /// Update the value of site basement floors
        /// </summary>
        /// <param name="inputField"></param>
        public void SiteBasementFloors(InputField inputField)
        {
            siteBasementFloors = int.Parse(inputField.text);
            if (siteBasement != null)
            {
                siteBasement.floors = siteBasementFloors;
                if (siteBasement.polygon != null)
                {
                    siteBasement.polygon.extrusion.totalHeight = siteBasement.floors * siteBasement.floor2Floor * -1;
                    siteBasement.polygon.extrusion.GenerateMeshes();
                    siteBasement.polygon.extrusion.GetComponent<MeshRenderer>().materials[0].SetFloat("_Modulo", siteBasement.floors);
                    if (siteBasement.buildings != null)
                    {
                        for (int i = 0; i < siteBasement.buildings.Count; i++)
                        {
                            siteBasement.buildings[i].SetBasement(siteBasement);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Update the value of site basement floor height
        /// </summary>
        /// <param name="inputField"></param>
        public void SiteBasementHeight(InputField inputField)
        {
            siteBasementFloor2Floor = float.Parse(inputField.text);
            if (siteBasement != null)
            {
                siteBasement.floor2Floor = siteBasementFloor2Floor;
                if (siteBasement.polygon != null)
                {
                    siteBasement.polygon.extrusion.totalHeight = siteBasement.floors * siteBasement.floor2Floor * -1;
                    siteBasement.polygon.extrusion.GenerateMeshes();
                    if (siteBasement.buildings != null)
                    {
                        for (int i = 0; i < siteBasement.buildings.Count; i++)
                        {
                            siteBasement.buildings[i].SetBasement(siteBasement);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Update the value of site basement offset
        /// </summary>
        /// <param name="inputField"></param>
        public void SiteBasementOffset(InputField inputField)
        {
            float delta = float.Parse(inputField.text) - siteBasementOffset;
            siteBasementOffset = float.Parse(inputField.text);
            if (siteBasement != null)
            {
                siteBasement.offset = siteBasementOffset;
                if (siteBasement.polygon != null && siteBasementType == ExtraMassingType.Footprint)
                {
                    var verts = PolygonVertex.ToVectorArray(siteBasement.polygon.vertices);
                    verts = Polygon.OffsetPoints(verts, true, delta);
                    for (int i = 0; i < verts.Length; i++)
                    {
                        siteBasement.polygon[i].SetPosition(verts[i]);
                        siteBasement.polygon[i].UpdateFollowDist(sitePolygon[i]);
                    }
                }
            }
        }
        /// <summary>
        /// Update the value of site podium floors
        /// </summary>
        /// <param name="inputField"></param>
        public void SitePodiumFloors(InputField inputField)
        {
            sitePodiumFloors = int.Parse(inputField.text);
            if (sitePodium != null)
            {
                sitePodium.floors = sitePodiumFloors;
                if (sitePodium.polygon != null)
                {
                    sitePodium.polygon.extrusion.totalHeight = sitePodium.floors * sitePodium.floor2Floor;
                    sitePodium.polygon.extrusion.GenerateMeshes();
                    sitePodium.polygon.extrusion.GetComponent<MeshRenderer>().materials[0].SetFloat("_Modulo", sitePodium.floors);
                    if (sitePodium.buildings != null)
                    {
                        for (int i = 0; i < sitePodium.buildings.Count; i++)
                        {
                            sitePodium.buildings[i].SetPodium(sitePodium);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Update the value of site basement floor height
        /// </summary>
        /// <param name="inputField"></param>
        public void SitePodiumHeight(InputField inputField)
        {
            sitePodiumFloor2Floor = float.Parse(inputField.text);
            if (sitePodium != null)
            {
                sitePodium.floor2Floor = sitePodiumFloor2Floor;
                if (sitePodium.polygon != null)
                {
                    sitePodium.polygon.extrusion.totalHeight = sitePodium.floors * sitePodium.floor2Floor;
                    sitePodium.polygon.extrusion.GenerateMeshes();
                    if (sitePodium.buildings != null)
                    {
                        for (int i = 0; i < sitePodium.buildings.Count; i++)
                        {
                            sitePodium.buildings[i].SetPodium(sitePodium);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Update the value of site basement offset
        /// </summary>
        /// <param name="inputField"></param>
        public void SitePodiumOffset(InputField inputField)
        {
            float delta = float.Parse(inputField.text) - sitePodiumOffset;
            sitePodiumOffset = float.Parse(inputField.text);
            if (sitePodium != null)
            {
                sitePodium.offset = sitePodiumOffset;
                if (sitePodium.polygon != null && sitePodiumType == ExtraMassingType.Footprint)
                {
                    var verts = PolygonVertex.ToVectorArray(sitePodium.polygon.vertices);
                    verts = Polygon.OffsetPoints(verts, true, delta);
                    for (int i = 0; i < verts.Length; i++)
                    {
                        sitePodium.polygon[i].SetPosition(verts[i]);
                        sitePodium.polygon[i].UpdateFollowDist(sitePolygon[i]);
                    }
                }
            }
        }
        /// <summary>
        /// Invoke function to generate a podium at the size of the site
        /// </summary>
        public void GenerateSiteFootprintPodium()
        {
            SetSitePodiumType(ExtraMassingType.Footprint);
            GetSiteFootprintPodium(sitePodiumFloors, sitePodiumFloor2Floor, sitePodiumOffset);
        }

        /// <summary>
        /// Called when the mix of the site-wide basement has changed
        /// </summary>
        /// <param name="args">The arguments for the new mix</param>
        public void OnSiteBasementPercentagesChanged(BriefChangedEventArgs args)
        {
            if (args.index == -1)
            {
                if (siteBasement == null) siteBasement = new SiteBasement() { use = args.use };
                else siteBasement.use = args.use;
            }
        }
        /// <summary>
        /// Called when the mix of the site-wide podium has changed
        /// </summary>
        /// <param name="args">The arguments for the new mix</param>
        public void OnSitePodiumPercentagesChanged(BriefChangedEventArgs args)
        {
            if (args.index == -1)
            {
                if (sitePodium == null) sitePodium = new SitePodium() { use = args.use };
                else sitePodium.use = args.use;
            }
        }
        /// <summary>
        /// Called when the site-wide podium polygon has changed
        /// </summary>
        /// <param name="args">The arguments for the polygon changes</param>
        public void OnCustomPodiumChanged(CustomPolygonEventArgs args)
        {
            sitePodium.floors = args.floors;
            sitePodium.floor2Floor = args.floor2Floor;
            sitePodium.use = args.use;
            sitePodium.polygon.extrusion.totalHeight = sitePodium.floors * sitePodium.floor2Floor;
            sitePodium.polygon.extrusion.GetComponent<MeshRenderer>().sharedMaterial.SetFloat("_Modulo", sitePodium.floors);
            sitePodium.polygon.extrusion.GenerateMeshes();
        }
        /// <summary>
        /// Called when the mix of the site-wide podium has changed (custom polygon only)
        /// </summary>
        /// <param name="args">The arguments for the new mix</param>
        public void OnCustomPodiumBriefChanged(BriefChangedEventArgs args)
        {
            sitePodium.use = args.use;
        }

        /// <summary>
        /// Called when the site-wide basement polygon has changed
        /// </summary>
        /// <param name="args">The arguments for the polygon changes</param>
        public void OnCustomBasementChanged(CustomPolygonEventArgs args)
        {
            siteBasement.floors = args.floors;
            siteBasement.floor2Floor = args.floor2Floor;
            siteBasement.use = args.use;
            siteBasement.polygon.extrusion.totalHeight = siteBasement.floors * siteBasement.floor2Floor * -1;
            siteBasement.polygon.extrusion.GetComponent<MeshRenderer>().sharedMaterial.SetFloat("_Modulo", siteBasement.floors);
            siteBasement.polygon.extrusion.GenerateMeshes();
        }

        /// <summary>
        /// Called when then mix of the site-wide basement has changed (custom polygon only)
        /// </summary>
        /// <param name="args">The arguments for the new mix</param>
        public void OnCustomBasementBriefChanged(BriefChangedEventArgs args)
        {
            siteBasement.use = args.use;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Deletes the site polygon
        /// </summary>
        public void OnDeleteSitePolygon()
        {
            if (sitePolygon != null)
            {
                Destroy(sitePolygon.gameObject);
                if (sitePodium != null)
                {
                    DeleteSitePodium();
                }
                if (siteBasement != null)
                {
                    DeleteSiteBasement();
                }
                sitePolygon = null;
            }
        }

        /// <summary>
        /// Sets the type of site basement.
        ///  - Footprint
        ///  - Custom
        /// </summary>
        /// <param name="siteBasementType">The new type of site basement</param>
        /// <param name="forceClear">Force to clear all previous site-wide basements</param>
        public void SetSiteBasementType(ExtraMassingType siteBasementType, bool forceClear = false)
        {
            if (this.siteBasementType != siteBasementType)
            {
                ClearSiteBasements();
                this.siteBasementType = siteBasementType;
            }
            else
            {
                if (forceClear) ClearSiteBasements();
            }
        }

        /// <summary>
        /// Sets the type of site podium.
        ///  - Footprint
        ///  - Custom
        /// </summary>
        /// <param name="sitePodiumType">The new type of site podium</param>
        /// <param name="forceClear">Force to clear all previous site-wide podiums</param>
        public void SetSitePodiumType(ExtraMassingType sitePodiumType, bool forceClear = false)
        {
            if (this.sitePodiumType != sitePodiumType)
            {
                ClearSitePodiums();
                this.sitePodiumType = sitePodiumType;
            }
            else
            {
                if (forceClear) ClearSitePodiums();
            }
        }

        /// <summary>
        /// Creates a site basement which is an offset of the site boundary
        /// </summary>
        /// <param name="numberOfFloors">The number of floors for the basement</param>
        /// <param name="floor2Floor">The floor to floor height</param>
        /// <param name="offset">The offset from the site boundary</param>
        /// <returns>Polygon</returns>
        public Polygon GetSiteFootprintBasement(int numberOfFloors, float floor2Floor, float offset)
        {
            if (siteBasement != null && siteBasement.polygon != null)
            {
                Destroy(siteBasement.polygon.gameObject);
            }
            Polygon basement = sitePolygon.Copy(new Vector3(0, 0, 0), true, offset);
            basement.transform.SetParent(basementParent);
            Extrusion basementWalls = Instantiate(extrusionPrefab, basement.transform).GetComponent<Extrusion>();
            basementWalls.totalHeight = numberOfFloors * floor2Floor * -1;
            basementWalls.Initialize(basement);
            basement.extrusion = basementWalls;
            Material mat = new Material(Resources.Load("Materials/OutlineCutoff") as Material);
            mat.SetFloat("_Modulo", numberOfFloors);
            mat.SetFloat("_UseTexture", 1);
            //mat.color = Color.gray;
            basement.SetOriginalMaterial(mat);
            basement.gameObject.name = "SiteBasement";
            if (siteBasement != null)
            {
                siteBasement.polygon = basement;
                siteBasement.floor2Floor = siteBasementFloor2Floor;
                siteBasement.floors = siteBasementFloors;
            }
            else
            {
                siteBasement = new SiteBasement { polygon = basement, floors = numberOfFloors, floor2Floor = floor2Floor };
            }
            CheckIfBuildingOverBasement(siteBasement);
            return basement;
        }


        /// <summary>
        /// Creates a site podium which is an offset of the site boundary
        /// </summary>
        /// <param name="numberOfFloors">The number of floors for the podium</param>
        /// <param name="floor2Floor">The floor to floor height</param>
        /// <param name="offset">The offset from the site boundary</param>
        /// <returns>Polygon</returns>
        public void GetSiteFootprintPodium(int numberOfFloors, float floor2Floor, float offset)
        {
            if (sitePodium != null && sitePodium.polygon != null)
            {
                Destroy(sitePodium.polygon.gameObject);
            }
            Polygon podium = sitePolygon.Copy(new Vector3(0, 0, 0), true, offset);
            podium.transform.SetParent(podiumParent);
            Extrusion basementWalls = Instantiate(extrusionPrefab, podium.transform).GetComponent<Extrusion>();
            basementWalls.totalHeight = numberOfFloors * floor2Floor;
            basementWalls.capped = true;
            basementWalls.Initialize(podium, true);
            podium.extrusion = basementWalls;
            Material mat = new Material(Resources.Load("Materials/OutlineCutoff") as Material);
            mat.SetFloat("_Modulo", numberOfFloors);
            mat.SetFloat("_UseTexture", 0);
            mat.SetColor("_Color", Color.gray);
            Material mat2 = new Material(Shader.Find("Standard"));
            mat2.color = Color.grey;
            podium.SetOriginalMaterial(mat, mat2);
            podium.gameObject.SetLayerToChildren(21);
            podium.gameObject.name = "SitePodium";

            if (sitePodium != null)
            {
                sitePodium.polygon = podium;
                sitePodium.floor2Floor = sitePodiumFloor2Floor;
                sitePodium.floors = sitePodiumFloors;
            }
            else
            {
                sitePodium = new SitePodium { polygon = podium, floors = numberOfFloors, floor2Floor = floor2Floor, offset = offset };
            }
            CheckIfBuildingOverPodium(sitePodium);
            //return podium;
        }

        /// <summary>
        /// Checks if a given building is over any site podium
        /// </summary>
        /// <param name="building">The building to check</param>
        public void CheckIfBuildingOverPodium(ProceduralBuilding building)
        {
            if (sitePodium != null && sitePodium.polygon != null)
            {
                if (sitePodium.buildings == null)
                {
                    sitePodium.buildings = new List<ProceduralBuilding>();
                }
                if (building.buildingType == BuildingType.Linear && building.masterPolyline != null)
                {
                    int counter = 0;
                    for (int j = 0; j < building.masterPolyline.Count; j++)
                    {
                        if (sitePodium.polygon.IsIncluded(new Vector3(building.masterPolyline[j].currentPosition.x, sitePodium.polygon[0].currentPosition.y, building.masterPolyline[j].currentPosition.z)))
                        {
                            counter++;
                        }
                    }

                    if (counter >= building.masterPolyline.Count * 0.5f)
                    {
                        building.SetPodium(sitePodium);
                        sitePodium.buildings.Add(building);
                    }
                }
                else if (building.buildingType == BuildingType.Tower || building.buildingType == BuildingType.Mansion)
                {
                    if (sitePodium.polygon.IsIncluded(new Vector3(building.transform.position.x, sitePodium.polygon[0].currentPosition.y, building.transform.position.z)))
                    {
                        building.SetPodium(sitePodium);
                        sitePodium.buildings.Add(building);
                    }
                }
            }
        }

        /// <summary>
        /// Checks if there are any buildings above a given podium
        /// </summary>
        /// <param name="podium">The podium to check</param>
        public void CheckIfBuildingOverPodium(SitePodium podium)
        {
            if (podium.polygon != null)
            {
                podium.buildings = new List<ProceduralBuilding>();
                for (int i = 0; i < proceduralBuildings.Count; i++)
                {

                    if (!proceduralBuildings[i].hasGeometry) continue;

                    if (proceduralBuildings[i].buildingType == BuildingType.Linear && proceduralBuildings[i].masterPolyline != null)
                    {
                        int counter = 0;
                        for (int j = 0; j < proceduralBuildings[i].masterPolyline.Count; j++)
                        {
                            if (podium.polygon.IsIncluded(new Vector3(proceduralBuildings[i].masterPolyline[j].currentPosition.x, podium.polygon[0].currentPosition.y, proceduralBuildings[i].masterPolyline[j].currentPosition.z)))
                            {
                                counter++;
                            }
                        }

                        if (counter >= proceduralBuildings[i].masterPolyline.Count * 0.5f)
                        {
                            proceduralBuildings[i].SetPodium(podium);
                            podium.buildings.Add(proceduralBuildings[i]);
                        }
                        else
                            proceduralBuildings[i].SetPodium(null);
                    }
                    else if (proceduralBuildings[i].buildingType == BuildingType.Tower || proceduralBuildings[i].buildingType == BuildingType.Mansion)
                    {
                        if (podium.polygon.IsIncluded(new Vector3(proceduralBuildings[i].transform.position.x, podium.polygon[0].currentPosition.y, proceduralBuildings[i].transform.position.z)))
                        {
                            proceduralBuildings[i].SetPodium(podium);
                            podium.buildings.Add(proceduralBuildings[i]);
                        }
                        else
                            proceduralBuildings[i].SetPodium(null);
                    }
                }
            }
        }

        /// <summary>
        /// Checks if a given building is over any site basement
        /// </summary>
        /// <param name="building">The building to check</param>
        public void CheckIfBuildingOverBasement(ProceduralBuilding building)
        {
            if (siteBasement != null && siteBasement.polygon != null)
            {
                if (siteBasement.buildings == null) siteBasement.buildings = new List<ProceduralBuilding>();
                if (building.buildingType == BuildingType.Linear && building.masterPolyline != null)
                {
                    int counter = 0;
                    for (int j = 0; j < building.masterPolyline.Count; j++)
                    {
                        if (siteBasement.polygon.IsIncluded(new Vector3(building.masterPolyline[j].currentPosition.x, siteBasement.polygon[0].currentPosition.y, building.masterPolyline[j].currentPosition.z)))
                        {
                            counter++;
                        }
                    }

                    if (counter >= building.masterPolyline.Count * 0.5f)
                    {
                        building.SetBasement(siteBasement);
                        siteBasement.buildings.Add(building);
                    }
                }
                else if (building.buildingType == BuildingType.Tower)
                {
                    if (siteBasement.polygon.IsIncluded(new Vector3(building.transform.position.x, siteBasement.polygon[0].currentPosition.y, building.transform.position.z)))
                    {
                        building.SetBasement(siteBasement);
                        siteBasement.buildings.Add(building);
                    }
                }
            }
        }

        /// <summary>
        /// Checks if there are any buildings above a given basement
        /// </summary>
        /// <param name="basement">The basement to check</param>
        public void CheckIfBuildingOverBasement(SiteBasement basement)
        {
            if (basement.polygon != null)
            {
                basement.buildings = new List<ProceduralBuilding>();
                for (int i = 0; i < proceduralBuildings.Count; i++)
                {
                    if (proceduralBuildings[i].buildingType == BuildingType.Linear && proceduralBuildings[i].masterPolyline != null)
                    {
                        int counter = 0;
                        for (int j = 0; j < proceduralBuildings[i].masterPolyline.Count; j++)
                        {
                            if (basement.polygon.IsIncluded(new Vector3(proceduralBuildings[i].masterPolyline[j].currentPosition.x, basement.polygon[0].currentPosition.y, proceduralBuildings[i].masterPolyline[j].currentPosition.z)))
                            {
                                counter++;
                            }
                        }

                        if (counter >= proceduralBuildings[i].masterPolyline.Count * 0.5f)
                        {
                            proceduralBuildings[i].SetBasement(basement);
                            basement.buildings.Add(proceduralBuildings[i]);
                        }
                    }
                    else if (proceduralBuildings[i].buildingType == BuildingType.Tower)
                    {
                        if (basement.polygon.IsIncluded(new Vector3(proceduralBuildings[i].transform.position.x, basement.polygon[0].currentPosition.y, proceduralBuildings[i].transform.position.z)))
                        {
                            proceduralBuildings[i].SetBasement(basement);
                            basement.buildings.Add(proceduralBuildings[i]);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Clears the list of site basements
        /// </summary>
        public void ClearSiteBasements()
        {
            if (siteBasement != null)
            {
                if (siteBasement.polygon != null)
                    Destroy(siteBasement.polygon.gameObject);
            }
        }

        /// <summary>
        /// Clears the list of site podiums
        /// </summary>
        public void ClearSitePodiums()
        {
            if (sitePodium != null)
            {
                if (sitePodium.polygon != null)
                    Destroy(sitePodium.polygon.gameObject);
            }
        }

        /// <summary>
        /// Generate the footprint based podium of the current building
        /// </summary>
        public void GenerateFoorprintBuildingPodium()
        {
            if (CurrentBuilding.podium != null)
            {
                CurrentBuilding.podium.GenerateFootprintPodium();
            }
        }

        /// <summary>
        /// Generate the footprint based basement of the current building
        /// </summary>
        public void GenerateFoorprintBuildingBasement()
        {
            if (CurrentBuilding.basement != null)
            {
                CurrentBuilding.basement.GenerateFootprintBasement();
            }
        }

        /// <summary>
        /// Initiates the process of custom site basement creation
        /// </summary>
        /// <param name="site">Whether the basement is site-wide</param>
        public void StartCustomBasement(bool site)
        {
            if (site)
            {
                SetSiteBasementType(ExtraMassingType.Custom, true);
                polygonDrawer.showDistances = true;
                polygonDrawer.usePlane = true;
                polygonDrawer.SetPlane(sitePolygon == null ? Vector3.zero : sitePolygon[0].currentPosition, DrawingPlane.XZ);
                polygonDrawer.StartBasementPolygon();
            }
            else
            {
                polygonDrawer.showDistances = true;
                polygonDrawer.usePlane = true;
                polygonDrawer.SetPlane(Vector3.up * CurrentBuilding.baseHeight, DrawingPlane.XZ);
                polygonDrawer.StartBasementPolygon();
                customFloorBasement = true;
            }
        }

        /// <summary>
        /// Initiates the process of custom site podium creation
        /// </summary>
        /// <param name="site">Whether the podium is site-wide</param>
        public void StartCustomPodium(bool site)
        {
            if (site)
            {
                SetSitePodiumType(ExtraMassingType.Custom, true);
                polygonDrawer.showDistances = true;
                polygonDrawer.usePlane = true;
                polygonDrawer.SetPlane(sitePolygon != null ? sitePolygon[0].currentPosition : Vector3.zero, DrawingPlane.XZ);
                polygonDrawer.StartPodiumPolygon();
            }
            else
            {
                polygonDrawer.showDistances = true;
                polygonDrawer.usePlane = true;
                polygonDrawer.SetPlane(Vector3.up * CurrentBuilding.baseHeight, DrawingPlane.XZ);
                polygonDrawer.StartPodiumPolygon();
                customFloorPodium = true;
            }
        }

        /// <summary>
        /// Load a custom polygon podium for the current building
        /// </summary>
        /// <param name="state">The floor save state to load</param>
        public void LoadCustomPodiumFloor(FloorLayoutState state)
        {
            customFloorPodium = true;
            polygonDrawer.LoadPodiumPolygon(state.customExterior, true);
        }
        /// <summary>
        /// Load a custom polygon basement for the current building
        /// </summary>
        /// <param name="state">The floor save state to load</param>
        public void LoadCustomBasementFloor(FloorLayoutState state)
        {
            customFloorBasement = true;
            polygonDrawer.LoadBasementPolygon(state.customExterior, true);
        }
        /// <summary>
        /// Load a custom polygon site-wide basement
        /// </summary>
        /// <param name="state">The save state for the extra massing</param>
        public void LoadCustomSiteBasement(ExtraMassingState state)
        {
            polygonDrawer.LoadBasementPolygon(state.vertices, true);
        }
        /// <summary>
        /// Load a custom polygon site-wide podium
        /// </summary>
        /// <param name="state">The save state for the extra massing</param>
        public void LoadCustomSitePodium(ExtraMassingState state)
        {
            polygonDrawer.LoadPodiumPolygon(state.vertices, true);
        }

        /// <summary>
        /// Called to populate the apartments with their interior geometry
        /// </summary>
        public void PopulateInterior()
        {
            //StartCoroutine(UpdateInternalLayout());
            if (previewMode == PreviewMode.Buildings)
            {
                populateApartments = !populateApartments;
                for (int i = 0; i < proceduralBuildings.Count; i++)
                {
                    StartCoroutine(proceduralBuildings[i].PopulateInterior());
                }
            }
            else if (previewMode == PreviewMode.Floors)
            {
                populateApartments = !populateApartments;
                StartCoroutine(proceduralBuildings[currentBuildingIndex].PopulateInterior());
            }
        }

        /// <summary>
        /// Toggles the interior geometry of the apartments
        /// </summary>
        /// <param name="show">Whether the interior geometry should be shown or not</param>
        /// <param name="facade">Whether the facade should be visible</param>
        public IEnumerator PopulateInterior(bool show, bool facade = false)
        {
            if (!show)
            {
                manufacturingDropDown.SetValue(0);
                manufacturingDropDown.RefreshShownValue();
            }
            //facadeToggle.SetActive(show);
            //facadeToggle.GetComponent<Toggle>().SetValue(false);
            populateApartments = show;
            //StartCoroutine(UpdateInternalLayout());
            for (int i = 0; i < proceduralBuildings.Count; i++)
            {
                yield return StartCoroutine(proceduralBuildings[i].PopulateInterior(show));
            }
        }

        /// <summary>
        /// Toggle the facade of the buildings
        /// </summary>
        /// <param name="facade">Whether the facade should be visible</param>
        public void ShowFacade(bool facade)
        {
            for (int i = 0; i < proceduralBuildings.Count; i++)
            {
                proceduralBuildings[i].ShowFacade(facade);
            }
        }

        /// <summary>
        /// Toggles the preview of the manufacturing system (None, Volumetric, Panels)
        /// </summary>
        /// <param name="manufacturingSystem">The manufacturing system selected</param>
        public void ToggleManufacturingSystem(ManufacturingSystem manufacturingSystem)
        {
            this.manufacturingSystem = manufacturingSystem;
            if (previewMode == PreviewMode.Apartments)
            {
                Debug.Log(proceduralApartment.roomParent);
                if (proceduralApartment.roomParent != null)
                {
                    proceduralApartment.SetManufacturingSystem(manufacturingSystem);
                }
                else
                {
                    proceduralApartment.roomParent = new GameObject("Rooms").transform;
                    proceduralApartment.roomParent.SetParent(proceduralApartment.transform);
                    proceduralApartment.SetManufacturingSystem(manufacturingSystem);
                }
            }
            else
            {
                for (int i = 0; i < proceduralBuildings.Count; i++)
                {
                    StartCoroutine(proceduralBuildings[i].ToggleManufacturingSystem(manufacturingSystem));
                }
            }
        }

        /// <summary>
        /// Returns the actual apartment distribution based on a target one
        /// </summary>
        /// <param name="percentages">The target distribution</param>
        /// <returns>String Float Dictionary</returns>
        public Dictionary<string, float> CheckCurrentFloor(Dictionary<string, float> percentages)
        {
            if (!proceduralBuildings[currentBuildingIndex].currentFloor.hasGeometry)
            {
                throw new Exception("You neeed to create an instance of the floor!");
            }
            else
            {
                var floor = proceduralBuildings[currentBuildingIndex].currentFloor;
                int totalNum = 0;
                return floor.CheckDistribution(percentages, out totalNum);
            }
        }

        /// <summary>
        /// Returns statistics regarding Volumetric manufacturing
        /// </summary>
        /// <returns>Dictionary with module type and number</returns>
        public Dictionary<string, float> GetVerticalModulesNumber()
        {
            if (populateApartments)
            {
                Dictionary<string, float> modulesSizes = new Dictionary<string, float>();

                for (int i = 0; i < proceduralBuildings.Count; i++)
                {
                    if (proceduralBuildings[i].apartmentsInteriors != null)
                    {
                        var buildingNumbers = proceduralBuildings[i].GetVerticalModulesNumber();
                        foreach (var item in buildingNumbers)
                        {
                            if (modulesSizes.ContainsKey(item.Key))
                            {
                                modulesSizes[item.Key] += item.Value;
                            }
                            else
                            {
                                modulesSizes.Add(item.Key, item.Value);
                            }
                        }
                    }

                    for (int j = 1; j < proceduralBuildings[i].floors.Count; j++)
                    {
                        for (int k = 0; k < proceduralBuildings[i].floors[j].apartments.Count; k++)
                        {
                            if (proceduralBuildings[i].floors[j].apartments[k].ApartmentType == "SpecialApt")
                            {
                                if (modulesSizes.ContainsKey("Not Modular"))
                                {
                                    modulesSizes["Not Modular"] += 1;// (proceduralBuildings[i].floors[j].apartments[k].Area * proceduralBuildings[i].floors[j].levels.Length);
                                }
                                else
                                {
                                    modulesSizes.Add("Not Modular", 1/*proceduralBuildings[i].floors[j].apartments[k].Area * proceduralBuildings[i].floors[j].levels.Length*/);
                                }
                            }
                        }
                    }
                }
                return modulesSizes;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Returns the numbers of the platforms system for the whole project
        /// </summary>
        /// <returns>Dictionary of String and Integer</returns>
        public Dictionary<string, int> GetPlatformsNumbers()
        {
            Dictionary<string, int> platformsNumbers = new Dictionary<string, int>();

            for (int i = 0; i < proceduralBuildings.Count; i++)
            {
                var platNumbers = proceduralBuildings[i].GetPlatformsNumbers();
                foreach (var item in platNumbers)
                {
                    if (platformsNumbers.ContainsKey(item.Key))
                    {
                        platformsNumbers[item.Key] += item.Value;
                    }
                    else
                    {
                        platformsNumbers.Add(item.Key, item.Value);
                    }
                }
            }

            return platformsNumbers;
        }

        /// <summary>
        /// Returns statistics regarding Panel manufacturing
        /// </summary>
        /// <returns>Dictionary with panel type and number</returns>
        public Dictionary<string, int> GetPanelsNumbers()
        {
            if (populateApartments)
            {
                Dictionary<string, int> panelsSizes = new Dictionary<string, int>();
                for (int i = 0; i < proceduralBuildings.Count; i++)
                {
                    if (proceduralBuildings[i].apartmentsInteriors != null)
                    {
                        var buildingNumbers = proceduralBuildings[i].GetPanelsNumbers();
                        foreach (var item in buildingNumbers)
                        {
                            if (panelsSizes.ContainsKey(item.Key))
                            {
                                panelsSizes[item.Key] += item.Value;
                            }
                            else
                            {
                                panelsSizes.Add(item.Key, item.Value);
                            }
                        }
                    }
                }
                return panelsSizes;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Initiates the process of combining apartments
        /// </summary>
        public void CombineApartments()
        {

            bool isCorner = false;
            if (aptToSwap.Count == 2 && (aptToSwap[0].editableMesh.Count == 3 && aptToSwap[1].editableMesh.Count == 3))
            {
                isCorner = true;
            }

            ResolveAction combAction = new ResolveAction();
            combAction.aptNames = new string[aptToSwap.Count];
            combAction.aptVertices = new Vector3[aptToSwap.Count][];
            combAction.aptTypes = new string[aptToSwap.Count];
            combAction.aptIndex = new int[aptToSwap.Count];
            combAction.flipped = new bool[aptToSwap.Count];
            combAction.aptCorridorIndex = new int[aptToSwap.Count];
            combAction.aptHasCorridor = new bool[aptToSwap.Count];
            combAction.isExteriorCorner = new bool[aptToSwap.Count];
            combAction.aptVertexIndex = new List<KeyValuePair<int, int>>[aptToSwap.Count];
            combAction.floor = currentFloor;
            bool isExt = false;

            for (int i = 0; i < aptToSwap.Count; i++)
            {
                combAction.aptNames[i] = aptToSwap[i].gameObject.name;
                combAction.aptIndex[i] = aptToSwap[i].Id;
                combAction.aptTypes[i] = aptToSwap[i].ApartmentType;
                combAction.flipped[i] = aptToSwap[i].flipped;
                combAction.aptCorridorIndex[i] = aptToSwap[i].activeCorridorIndex;
                combAction.aptVertices[i] = new Vector3[aptToSwap[i].editableMesh.vertices.Count];
                combAction.aptHasCorridor[i] = aptToSwap[i].HasCorridor;
                combAction.isExteriorCorner[i] = aptToSwap[i].isExterior;
                for (int j = 0; j < combAction.aptVertices[i].Length; j++)
                {
                    combAction.aptVertices[i][j] = aptToSwap[i].editableMesh.vertices[j].currentPosition;
                }
                if (aptToSwap[i].IsCorner == true && aptToSwap[i].isExterior == true)
                {
                    isExt = true;
                }
            }

            Vector3 startVector1 = aptToSwap[0].editableMesh.vertices[0].currentPosition;
            Vector3 startVector2 = aptToSwap[0].editableMesh.vertices.Last().currentPosition;

            List<int> vertsToDestroy = new List<int>();
            List<MeshVertex> vertices = new List<MeshVertex>();
            List<Vector3> positions = new List<Vector3>();
            List<MeshVertex> uniqueVertices = new List<MeshVertex>();
            Vector3 centre = new Vector3();

            Dictionary<Vector3, MeshVertex> dict = new Dictionary<Vector3, MeshVertex>();

            //Setting up vertices and getting positions for making a distinct list
            for (int i = 0; i < aptToSwap.Count; i++)
            {
                vertices.AddRange(aptToSwap[i].editableMesh.vertices);
            }
            positions = vertices.Select(x => x.currentPosition).Select(v => new Vector3((float)Math.Round(v.x, 2), (float)Math.Round(v.y, 2), (float)Math.Round(v.z, 2))).ToList();

            for (int i = 0; i < positions.Count; i++)
            {
                if (!dict.ContainsKey(positions[i]))
                {
                    dict.Add(positions[i], vertices[i]);
                }
                else
                {
                    vertsToDestroy.Add(i);

                }
            }
            uniqueVertices = dict.Values.ToList();



            //Calculating centre and sorting them clockwise
            for (int i = 0; i < uniqueVertices.Count; i++)
            {
                centre += uniqueVertices[i].currentPosition;
            }
            centre /= uniqueVertices.Count;
            uniqueVertices = uniqueVertices.OrderBy(x => Mathf.Atan2((x.currentPosition - centre).x, (x.currentPosition - centre).z)).ToList();

            List<int> uniqueToDestroy = new List<int>();
            //Removing co-linear ones
            List<MeshVertex> noCollinear = new List<MeshVertex>();
            for (int i = 0; i < uniqueVertices.Count; i++)
            {
                int next = (i + 1) % uniqueVertices.Count;
                int prev = i - 1;
                if (prev < 0) { prev = uniqueVertices.Count - 1; }

                float angle = Vector3.Angle(uniqueVertices[i].currentPosition - uniqueVertices[prev].currentPosition, uniqueVertices[next].currentPosition - uniqueVertices[i].currentPosition);

                if (Mathf.Abs(angle) > 2)
                {
                    try
                    {
                        uniqueVertices[i].transform.SetParent(null);
                        noCollinear.Add(uniqueVertices[i]);
                    }
                    catch (Exception e)
                    {
                        Debug.Log(e);
                    }
                }
                else
                {
                    uniqueToDestroy.Add(i);
                }
            }

            MeshVertex[] sortedVerts = new MeshVertex[noCollinear.Count];
            for (int i = 0; i < noCollinear.Count; i++)
            {
                if (Vector3.Distance(noCollinear[i].currentPosition, startVector1) < 0.05f)
                {
                    for (int j = 0; j < noCollinear.Count; j++)
                    {
                        int toIndex = j - 1;
                        if (toIndex < 0) { toIndex = noCollinear.Count + (j - 1); }
                        sortedVerts[toIndex] = noCollinear[(i + j) % noCollinear.Count];
                    }
                    sortedVerts = sortedVerts.Reverse().ToArray();
                }
                else if (Vector3.Distance(noCollinear[i].currentPosition, startVector2) < 0.05f)
                {
                    for (int j = 0; j < noCollinear.Count; j++)
                    {
                        int index = i - j;
                        if (index < 0) { index = noCollinear.Count + (i - j); }
                        int toIndex = j - 1;
                        if (toIndex < 0) { toIndex = noCollinear.Count - 1; }
                        sortedVerts[toIndex] = noCollinear[index];
                    }
                }
            }

            bool hasNull = false;
            for (int i = 0; i < aptToSwap.Count; i++)
            {
                combAction.aptVertexIndex[i] = new List<KeyValuePair<int, int>>();
                for (int j = 0; j < aptToSwap[i].editableMesh.vertices.Count; j++)
                {
                    for (int k = 0; k < sortedVerts.Length; k++)
                    {
                        if (sortedVerts[k] != null)
                        {
                            var v1 = aptToSwap[i].editableMesh.vertices[j].currentPosition;
                            var v2 = sortedVerts[k].currentPosition;
                            if (Vector3.Distance(v1, v2) < 0.05f)
                            {
                                combAction.aptVertexIndex[i].Add(new KeyValuePair<int, int>(j, k));
                            }
                        }
                        else
                        {
                            hasNull = true;
                        }
                    }
                }
            }

            if (hasNull) return;

            ApartmentUnity m_apt = aptToSwap[0];
            m_apt.SetGeometry(sortedVerts.ToList());
            m_apt.isExterior = isExt;
            m_apt.IsCorner = isCorner;
            combAction.resultingApts = new List<ApartmentUnity>() { m_apt };
            undoActionLog.AddAction(combAction);

            for (int i = 0; i < uniqueToDestroy.Count; i++)
            {
                try
                {
                    uniqueVertices[uniqueToDestroy[i]].DestroyVertex();
                }
                catch (Exception e)
                {
                    Debug.Log(e);
                }
            }

            for (int i = 0; i < vertsToDestroy.Count; i++)
            {
                try
                {
                    vertices[vertsToDestroy[i]].DestroyVertex();
                }
                catch (Exception e)
                {
                    Debug.Log(e);
                }
            }

            for (int i = 1; i < aptToSwap.Count; i++)
            {
                aptToSwap[i].DestroyApartment();
            }

            StartCoroutine(OnApartmentsChanged());
        }

        /// <summary>
        /// Called when the parapet height has changed
        /// </summary>
        /// <param name="val">The new value</param>
        public void OnParapetHeightChanged(float val)
        {
            for (int i = 0; i < proceduralBuildings.Count; i++)
            {
                proceduralBuildings[i].OnRoofSettingsChanged();
            }
        }
        /// <summary>
        /// Called when the roof depth has changed
        /// </summary>
        /// <param name="val">The new value</param>
        public void OnRoofDepthChanged(float val)
        {
            for (int i = 0; i < proceduralBuildings.Count; i++)
            {
                proceduralBuildings[i].OnRoofSettingsChanged();
            }
        }

        /// <summary>
        /// Logs a resolve action for undo
        /// </summary>
        /// <param name="resolveAction">The resolve action to be logged</param>
        public void SetResolveAction(ResolveAction resolveAction)
        {
            undoActionLog.AddAction(resolveAction);
        }

        /// <summary>
        /// Sets the apartments that are to be swapped
        /// </summary>
        /// <param name="designData">The list of apartments</param>
        public void SetSwapApartments(List<BaseDesignData> designData)
        {
            aptToSwap = new List<ApartmentUnity>();

            for (int i = 0; i < designData.Count; i++)
            {
                var m_apt = designData[i] as ApartmentUnity;
                if (m_apt != null)
                {
                    aptToSwap.Add(m_apt);
                }
            }
            aptToSwap = aptToSwap.OrderBy(x => x.IsCorner).ToList();
            if (designData.Count == 2)
            {
                var apt1 = designData[0] as ApartmentUnity;
                var apt2 = designData[1] as ApartmentUnity;
                Vector3 vec1 = Vector3.zero;
                Vector3 vec2 = Vector3.zero;
                if (apt1.editableMesh != null && apt2.editableMesh != null)
                {
                    vec1 = apt1.editableMesh.Centre;
                    vec2 = apt2.editableMesh.Centre;
                }
                var dist = Vector3.Distance(vec1, vec2);
                var combLength = apt1.Width + apt2.Width;
                bool neighbours = Mathf.Abs((combLength / 2.0f) - dist) < 0.1f;

                swapApartments.interactable = true && apt1.activeCorridorIndex == apt2.activeCorridorIndex && neighbours;
            }
            else
            {
                swapApartments.interactable = false;
            }

            bool hasCoreAdjacent = false;
            bool areSameSide = true;
            for (int i = 0; i < aptToSwap.Count; i++)
            {
                if (aptToSwap[i].isCoreResidual)
                {
                    hasCoreAdjacent = true;
                }
                if (i > 0)
                {
                    if (!aptToSwap[i].IsCorner && !aptToSwap[i - 1].IsCorner)
                    {
                        if (Vector3.Angle(aptToSwap[i].transform.forward, aptToSwap[i - 1].transform.forward) != 0)
                        {
                            areSameSide = false;
                        }
                    }
                }
            }

            combineApartments.interactable = designData.Count >= 2 && !hasCoreAdjacent && areSameSide;
        }

        /// <summary>
        /// Returns all the different sizes of the volumetric modules
        /// </summary>
        /// <returns>Dictionary of module type and list of sizes</returns>
        public Dictionary<string, int> GetVerticalModulesSizes()
        {
            if (populateApartments)
            {
                Dictionary<string, int> modulesSizes = new Dictionary<string, int>();
                for (int i = 0; i < proceduralBuildings.Count; i++)
                {
                    if (proceduralBuildings[i].apartmentsInteriors != null)
                    {
                        var buildingSizes = proceduralBuildings[i].GetVerticalModulesSizes();
                        foreach (var item in buildingSizes)
                        {
                            if (modulesSizes.ContainsKey(item.Key))
                            {
                                modulesSizes[item.Key] += item.Value;//.AddRange(item.Value);
                            }
                            else
                            {
                                modulesSizes.Add(item.Key, item.Value);
                            }
                        }
                    }
                }
                return modulesSizes;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// Returns the sizes for the platforms system for the whole project
        /// </summary>
        /// <returns>Dictionary of String and Integer</returns>
        public Dictionary<string, int> GetPlatformsSizes()
        {
            Dictionary<string, int> platformsSizes = new Dictionary<string, int>();

            for (int i = 0; i < proceduralBuildings.Count; i++)
            {
                var pltSizes = proceduralBuildings[i].GetPlatformsSizes();
                foreach (var item in pltSizes)
                {
                    if (platformsSizes.ContainsKey(item.Key))
                    {
                        platformsSizes[item.Key] += item.Value;
                    }
                    else
                    {
                        platformsSizes.Add(item.Key, item.Value);
                    }
                }
            }

            return platformsSizes;
        }

        /// <summary>
        /// Gets the different facade panel sizes for the project
        /// </summary>
        /// <returns>String - Int Dictionary</returns>
        public Dictionary<string, int> GetFacadePanelsSizes()
        {
            if (populateApartments)
            {
                Dictionary<string, int> panelSizes = new Dictionary<string, int>();
                for (int i = 0; i < proceduralBuildings.Count; i++)
                {
                    if (proceduralBuildings[i].apartmentsInteriors != null)
                    {
                        var buildingSizes = proceduralBuildings[i].GetFacadePanelsSizes();
                        foreach (var item in buildingSizes)
                        {
                            if (panelSizes.ContainsKey(item.Key))
                            {
                                panelSizes[item.Key] += item.Value;//.AddRange(item.Value);
                            }
                            else
                            {
                                panelSizes.Add(item.Key, item.Value);
                            }
                        }
                    }
                }
                return panelSizes;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the different Volumetric Module sizes for the project
        /// </summary>
        /// <returns>String - List of Float Dictionary</returns>
        public Dictionary<string, List<float>> GetVolumetricSizes()
        {
            if (populateApartments)
            {
                Dictionary<string, List<float>> volumetricSizes = new Dictionary<string, List<float>>();
                for (int i = 0; i < proceduralBuildings.Count; i++)
                {
                    if (proceduralBuildings[i].apartmentsInteriors != null)
                    {
                        var buildingSizes = proceduralBuildings[i].GetVolumetricSizes();
                        foreach (var item in buildingSizes)
                        {
                            if (volumetricSizes.ContainsKey(item.Key))
                            {
                                volumetricSizes[item.Key].AddRange(item.Value);
                            }
                            else
                            {
                                volumetricSizes.Add(item.Key, item.Value);
                            }
                        }
                    }
                }
                return volumetricSizes;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Returns all the different sizes of the Panels
        /// </summary>
        /// <returns>Dictionary of panel type and list of sizes</returns>
        public Dictionary<string, List<float>> GetPanelsSizes()
        {
            if (populateApartments)
            {
                Dictionary<string, List<float>> panelsSizes = new Dictionary<string, List<float>>();
                for (int i = 0; i < proceduralBuildings.Count; i++)
                {
                    if (proceduralBuildings[i].apartmentsInteriors != null)
                    {
                        var buildingSizes = proceduralBuildings[i].GetPanelsSizes();
                        foreach (var item in buildingSizes)
                        {
                            if (panelsSizes.ContainsKey(item.Key))
                            {
                                panelsSizes[item.Key].AddRange(item.Value);
                            }
                            else
                            {
                                panelsSizes.Add(item.Key, item.Value);
                            }
                        }
                    }
                }
                return panelsSizes;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Returns information about the selected apartment type
        /// </summary>
        /// <returns>String Array</returns>
        public string[] GetApartmentLayoutInfo()
        {
            string[] info = new string[2];

            if (aptToSwap != null && aptToSwap.Count > 0 && aptToSwap[0] != null && aptToSwap[0].ApartmentType != "SpecialApt" && aptToSwap[0].editableMesh.vertices.Count == 4)
            {
                info[0] = string.Format("Selected {0} Information:", Standards.TaggedObject.ApartmentTypesMinimumSizes.Keys.ToList()[apartmentDropdown.value]);
                info[1] = string.Format("Total Area: {0}m\xB2\r\n", Math.Round(aptToSwap[0].Area));
            }
            else
            {
                info[0] = string.Format("Standard {0} Information:", Standards.TaggedObject.ApartmentTypesMinimumSizes.Keys.ToList()[apartmentDropdown.value]);
                info[1] = string.Format("Total Area: {0}m\xB2\r\n", Math.Round(Standards.TaggedObject.ApartmentTypesMinimumSizes.Values.ToList()[apartmentDropdown.value]));
            }

            try
            {
                if (Standards.TaggedObject.ApartmentTypesMinimumSizes.Keys.ToList()[apartmentDropdown.value].Contains('b'))
                    info[1] += string.Format("Number of people: {0}\r\n", Standards.TaggedObject.ApartmentTypesMinimumSizes.Keys.ToList()[apartmentDropdown.value].Split('b')[1].Split('p')[0]);
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }
            return info;
        }

        /// <summary>
        /// Returns the total area per apartment type
        /// </summary>
        /// <returns>Dictionary</returns>
        public Dictionary<string, float> GetApartmentNumbers()
        {
            if (previewMode == PreviewMode.Floors)
            {
                Dictionary<string, float> areas = new Dictionary<string, float>();
                var buildingAreas = proceduralBuildings[currentBuildingIndex].GetApartmentNumbers();
                foreach (var item in buildingAreas)
                {
                    if (areas.ContainsKey(item.Key))
                    {
                        areas[item.Key] += (item.Value);
                    }
                    else
                    {
                        areas.Add(item.Key, item.Value);
                    }
                }
                return areas;
            }
            else
            {
                Dictionary<string, float> areas = new Dictionary<string, float>();
                for (int i = 0; i < proceduralBuildings.Count; i++)
                {
                    var buildingAreas = proceduralBuildings[i].GetApartmentNumbers();
                    foreach (var item in buildingAreas)
                    {
                        if (areas.ContainsKey(item.Key))
                        {
                            areas[item.Key] += (item.Value);
                        }
                        else
                        {
                            areas.Add(item.Key, item.Value);
                        }
                    }
                }
                return areas;
            }
        }

        /// <summary>
        /// Returns the areas for each apartment type in the project
        /// </summary>
        /// <returns>String - Float Dictionary</returns>
        public Dictionary<string, float> GetApartmentAreas()
        {
            if (previewMode == PreviewMode.Floors)
            {
                Dictionary<string, float> areas = new Dictionary<string, float>();
                var buildingAreas = proceduralBuildings[currentBuildingIndex].GetApartmentAreas();
                foreach (var item in buildingAreas)
                {
                    if (areas.ContainsKey(item.Key))
                    {
                        areas[item.Key] += (item.Value);
                    }
                    else
                    {
                        areas.Add(item.Key, item.Value);
                    }
                }
                return areas;
            }
            else
            {
                Dictionary<string, float> areas = new Dictionary<string, float>();
                for (int i = 0; i < proceduralBuildings.Count; i++)
                {
                    var buildingAreas = proceduralBuildings[i].GetApartmentAreas();
                    foreach (var item in buildingAreas)
                    {
                        if (areas.ContainsKey(item.Key))
                        {
                            areas[item.Key] += (item.Value);
                        }
                        else
                        {
                            areas.Add(item.Key, item.Value);
                        }
                    }
                }
                return areas;
            }
        }

        /// <summary>
        /// Updates the overall information about the building
        /// </summary>
        /// <returns>IEnumerator</returns>
        public IEnumerator OverallUpdate()
        {
            yield return waitFrame;

            if (proceduralBuildings.Count > currentBuildingIndex && proceduralBuildings[currentBuildingIndex].floors.Count > 0)
            {
                if (apartmentTypesChart != null)
                {
                    var areas = GetApartmentAreas();

                    string title = (previewMode == PreviewMode.Floors) ? ("Areas by Type (m\xB2) - " + proceduralBuildings[currentBuildingIndex].buildingName) : "Areas by Type (m\xB2) - Project";

                    if (areas.Count != 0)
                    {
                        apartmentTypesChart.SetChartValues(areas, title);

                        if (overallUpdated != null)
                        {
                            overallUpdated();
                        }
                    }
                }
                if (previewChanged != null)
                {
                    previewChanged(previewMode);
                }
            }
        }

        /// <summary>
        /// Returns the maximum bay of the platforms system
        /// </summary>
        /// <param name="index">The index of the building (if -1 then it will check through all buildings)</param>
        /// <returns>Float</returns>
        public float GetMaxPlatformsBay(int index = -1)
        {
            float maxBay = float.MinValue;
            if (proceduralBuildings != null)
            {
                return 0;
            }
            if (index == -1)
            {
                for (int i = 0; i < proceduralBuildings.Count; i++)
                {
                    for (int j = 0; j < proceduralBuildings[i].floors.Count; j++)
                    {
                        var bays = proceduralBuildings[i].floors[j].bays;
                        for (int k = 0; k < bays.Count; k++)
                        {
                            if (maxBay < bays[k])
                            {
                                maxBay = bays[k];
                            }
                        }
                    }
                }
            }
            else
            {
                for (int j = 0; j < proceduralBuildings[index].floors.Count; j++)
                {
                    var bays = proceduralBuildings[index].floors[j].bays;
                    for (int k = 0; k < bays.Count; k++)
                    {
                        if (maxBay < bays[k])
                        {
                            maxBay = bays[k];
                        }
                    }
                }
            }
            return maxBay;
        }

        /// <summary>
        /// Returns the maximum apartment depth in the project
        /// </summary>
        /// <returns></returns>
        public float GetMaxAptDepth(int index = -1)
        {
            float maxDepth = float.MinValue;
            if (index == -1)
            {
                for (int i = 0; i < proceduralBuildings.Count; i++)
                {
                    var depth = proceduralBuildings[i].GetMaxDepth();
                    if (depth > maxDepth)
                    {
                        maxDepth = depth;
                    }
                }
            }
            else
            {
                var depth = proceduralBuildings[index].GetMaxDepth();
                if (depth > maxDepth)
                {
                    maxDepth = depth;
                }
            }
            return maxDepth;
        }

        /// <summary>
        /// Returns the apartments in the project of a given type
        /// </summary>
        /// <param name="currentType">The apartment type</param>
        /// <param name="previousType">The previously selected apartment type</param>
        /// <returns>List of Apartments</returns>
        public List<ApartmentUnity> ShowSelectedApartmentType(string currentType, string previousType)
        {
            List<ApartmentUnity> apts = new List<ApartmentUnity>();
            if (previewMode == PreviewMode.Floors)
            {
                for (int k = 0; k < proceduralBuildings[currentBuildingIndex].currentFloor.levels.Length; k++)
                {
                    proceduralBuildings[currentBuildingIndex].currentFloor.ColorApartmentsOfType(previousType, false);
                    apts.AddRange(proceduralBuildings[currentBuildingIndex].currentFloor.ColorApartmentsOfType(currentType, true));
                }
            }
            else if (previewMode == PreviewMode.Buildings)
            {

                for (int i = 0; i < proceduralBuildings.Count; i++)
                {
                    for (int j = 0; j < proceduralBuildings[i].floors.Count; j++)
                    {
                        for (int k = 0; k < proceduralBuildings[i].floors[j].levels.Length; k++)
                        {
                            proceduralBuildings[i].floors[j].ColorApartmentsOfType(previousType, false);
                            apts.AddRange(proceduralBuildings[i].floors[j].ColorApartmentsOfType(currentType, true));
                        }
                    }
                }
            }
            return apts;
        }

        //public List<ApartmentUnity> ShowSelectedApartmentType(string currentType, string previousType)
        //{
        //    List<ApartmentUnity> apts = new List<ApartmentUnity>();
        //    if (previewMode == PreviewMode.Floors)
        //    {
        //        for (int k = 0; k < proceduralBuildings[currentBuildingIndex].currentFloor.levels.Length; k++)
        //        {
        //            proceduralBuildings[currentBuildingIndex].currentFloor.ColorApartmentsOfType(previousType, false);
        //            apts.AddRange(proceduralBuildings[currentBuildingIndex].currentFloor.ColorApartmentsOfType(currentType, true));
        //        }
        //    }
        //    else if (previewMode == PreviewMode.Buildings)
        //    {
        //        for (int i = 0; i < proceduralBuildings.Count; i++)
        //        {
        //            for (int j = 0; j < proceduralBuildings[i].floors.Count; j++)
        //            {
        //                for (int k = 0; k < proceduralBuildings[i].floors[j].levels.Length; k++)
        //                {
        //                    proceduralBuildings[i].floors[j].ColorApartmentsOfType(previousType, false);
        //                    apts.AddRange(proceduralBuildings[i].floors[j].ColorApartmentsOfType(currentType, true));
        //                }
        //            }
        //        }
        //    }
        //    return apts;
        //}

        /// <summary>
        /// Returns the information about the selected Design entity
        /// </summary>
        /// <returns>String Array</returns>
        public string[] GetSelectedInfo()
        {
            if (previewMode == PreviewMode.Buildings)
            {
                float NIA = 0;
                float GIA = 0;
                var info = proceduralBuildings[currentBuildingIndex].GetCurrentBuildingStats(out NIA, out GIA);
                return info;
            }
            else
            {
                float GEA = 0.0f;
                float GIA = 0.0f;
                float NIA = 0.0f;

                GEA = proceduralBuildings[currentBuildingIndex].currentFloor.GEA;
                GIA = proceduralBuildings[currentBuildingIndex].currentFloor.GIA;
                NIA = proceduralBuildings[currentBuildingIndex].currentFloor.NIA;

                var numbers = proceduralBuildings[currentBuildingIndex].GetCurrentApartmentNumbers();

                var info = new string[2];
                info[0] = "Levels " + proceduralBuildings[currentBuildingIndex].currentFloor.levels[0] + "-" + proceduralBuildings[currentBuildingIndex].currentFloor.levels.Last() + " - " + proceduralBuildings[currentBuildingIndex].buildingName;
                info[1] = string.Format("GEA = {0}m\xB2\r\nGIA = {1}m\xB2\r\nNIA = {2}m\xB2\r\n", Mathf.Round(GEA), Mathf.Round(GIA), Mathf.Round(NIA));
                foreach (var item in numbers)
                {
                    info[1] += string.Format("{0} = {1}\r\n", (item.Key == "SpecialApt") ? "Unresolved" : item.Key, item.Value);
                }
                return info;
            }
        }

        /// <summary>
        /// Returns information about the current building
        /// </summary>
        /// <returns>String Array</returns>
        public string[] GetGeneralInfo()
        {
            if (previewMode == PreviewMode.Floors)
            {
                float NIA = 0;
                float GIA = 0;
                var info = CurrentBuilding.GetCurrentBuildingStats(out NIA, out GIA);
                netToGrossSlider.value = (float)Math.Round((NIA / GIA) * 100, 1);
                netToGrossSlider.transform.GetChild(2).GetChild(0).GetChild(0).GetComponent<Text>().text = (float)Math.Round((NIA / GIA) * 100, 0) + "%";
                return info;
            }
            else
            {
                float GEA = 0.0f;
                float GIA = 0.0f;
                float NIA = 0.0f;
                float maxHeight = 0.0f;
                float unitDensity = -1.0f;
                float habDensity = -1.0f;
                Dictionary<string, float> aptAreas = new Dictionary<string, float>();
                Dictionary<string, float> aptNumbers = new Dictionary<string, float>();
                int totalAptNumber = 0;
                float plantRoomArea = 0;
                string ptalZone = "0";
                float siteArea = 0.0f;
                float groundFloorArea = 0.0f;
                float receptionArea = 0f;
                float commercialArea = 0f;
                float otherArea = 0f;
                float facadeArea = 0f;
                int northApts = 0;
                int eastApts = 0;
                int southApts = 0;
                int westApts = 0;
                int dualApts = 0;
                int m3 = 0;
                List<float> heights = new List<float>();
                List<string> names = new List<string>();

                float podiumArea = SitePodiumArea;
                float basementArea = SiteBasementArea;

                for (int i = 0; i < proceduralBuildings.Count; i++)
                {
                    float m_GEA = 0.0f;
                    float m_GIA = 0.0f;
                    float m_NIA = 0.0f;
                    float m_totalHeight = 0.0f;
                    float m_unitDensity = 0.0f;
                    float m_habDensity = 0.0f;
                    float m_groundFloorArea = 0.0f;
                    Dictionary<string, float> m_aptAreas = new Dictionary<string, float>();
                    Dictionary<string, float> m_aptNumbers = new Dictionary<string, float>();
                    float m_plantRoomArea = 0;
                    float m_receptionArea = 0;
                    float m_commercialArea = 0;
                    float m_otherArea = 0;
                    float m_facadeArea = 0;
                    int m_northApts = 0;
                    int m_eastApts = 0;
                    int m_southApts = 0;
                    int m_westApts = 0;
                    int m_dualASpect = 0;
                    int m_3 = 0;

                    proceduralBuildings[i].GetCurrentBuildingStats(out m_GEA, out m_GIA, out m_NIA, out m_totalHeight, out m_unitDensity, out m_habDensity, out m_plantRoomArea, out m_aptAreas, out m_aptNumbers, out m_groundFloorArea, out m_receptionArea, out m_commercialArea, out m_otherArea, out m_facadeArea);
                    proceduralBuildings[i].GetApartmentAspects(out m_northApts, out m_eastApts, out m_southApts, out m_westApts, out m_dualASpect, out m_3);

                    dualApts += m_dualASpect;
                    northApts += m_northApts;
                    eastApts += m_eastApts;
                    southApts += m_southApts;
                    westApts += m_westApts;
                    m3 += m_3;
                    GEA += m_GEA;
                    GIA += m_GIA;
                    NIA += m_NIA;
                    groundFloorArea += m_groundFloorArea;
                    heights.Add(m_totalHeight);
                    names.Add(proceduralBuildings[i].buildingName);
                    if (m_totalHeight > maxHeight)
                    {
                        maxHeight = m_totalHeight;
                    }
                    unitDensity += m_unitDensity;
                    habDensity += m_habDensity;
                    plantRoomArea += m_plantRoomArea;
                    receptionArea += m_receptionArea;
                    commercialArea += m_commercialArea;
                    otherArea += m_otherArea;
                    facadeArea += m_facadeArea;

                    foreach (var item in m_aptAreas)
                    {
                        if (aptAreas.ContainsKey(item.Key))
                        {
                            aptAreas[item.Key] += item.Value;
                        }
                        else
                        {
                            aptAreas.Add(item.Key, item.Value);
                        }
                    }

                    foreach (var item in m_aptNumbers)
                    {
                        totalAptNumber += (int)item.Value;
                        if (aptNumbers.ContainsKey(item.Key))
                        {
                            aptNumbers[item.Key] += item.Value;
                        }
                        else
                        {
                            aptNumbers.Add(item.Key, item.Value);
                        }
                    }

                    if (proceduralBuildings[i].podium != null)
                    {
                        podiumArea += proceduralBuildings[i].PodiumArea;
                    }
                    if (proceduralBuildings[i].basement != null)
                    {
                        basementArea += proceduralBuildings[i].BasementArea;
                    }
                }



                if (sitePolygon != null)
                {
                    siteArea = (float)Math.Round(sitePolygon.Area);
                    unitDensity /= (sitePolygon.Area / 10000.0f);
                    habDensity /= (sitePolygon.Area / 10000.0f);
                    Vector3 siteCentre = sitePolygon.Centre + Vector3.up * 10;
                    Ray r = new Ray(siteCentre, Vector3.down);
                    RaycastHit hit;
                    if (Physics.Raycast(r, out hit, 5000, ptalLayerMask))
                    {
                        ptalZone = hit.collider.gameObject.tag;
                    }

                    float maxDensity = 500.0f;
                    if (Standards.TaggedObject.ptalZones.TryGetValue(ptalZone, out maxDensity))
                    {
                        if (maxDensity < unitDensity)
                        {
                            if (!Notifications.TaggedObject.activeNotifications.Contains("Density"))
                            {
                                Notifications.TaggedObject.activeNotifications.Add("Density");
                            }
                        }
                        else
                        {
                            if (Notifications.TaggedObject.activeNotifications.Contains("Density"))
                            {
                                Notifications.TaggedObject.activeNotifications.Remove("Density");
                            }
                        }
                        Notifications.TaggedObject.UpdateNotifications();
                    }
                }

                var info = new string[2];
                info[0] = "Summary - Project";
                info[1] = string.Format(
                    "Site area = {7}m\xB2 / {14}hectares\n" +
                    "Density = {5} (units per hectare)\n" +
                    "Density = {6} (habitable rooms per hectare)\n" +
                    "Site Coverage = {11}%\n" +
                    "PTAL Zone = {8}\n" +
                    "GEA = {0}m\xB2 / {15}ft\xB2\n" +
                    "GIA = {1}m\xB2 / {16}ft\xB2\n" +
                    "NIA = {2}m\xB2 / {17}ft\xB2\n" +
                    "Net to gross = {9}%\n" +
                    "GIA to GEA = {10}%\n" +
                    "Podium Area = {25}m\xB2 / {26}ft\xB2\n" +
                    "Basement Area = {27}m\xB2 / {28}ft\xB2\n" +
                    "{29}" +
                    "{30}" +
                    //"Plant-Room Area = {4}m\xB2 / {18}ft\xB2\n" +
                    //"Entrance = {12}m\xB2 / {19}ft\xB2\n" +
                    //"Commercial = {13}m\xB2 / {20}ft\xB2\n" +
                    //"Other = {22}m\xB2 / {23}ft\xB2\n" +
                    "Maximum Height = {3}m\n" +
                    "Building Heights: {33}\n" +
                    "Number of floors = {21}\n" +
                    "Wall to floor = {24}\n" +
                    "Plot ratio = {31}\n" +
                    "Site usage = {32}\n",
                    String.Format(numbersFormat, Mathf.Round(GEA)),
                    String.Format(numbersFormat, Mathf.Round(GIA)),
                    String.Format(numbersFormat, Mathf.Round(NIA)),
                    maxHeight,
                    String.Format(numbersFormat, Math.Round(plantRoomArea)),
                    (unitDensity != -1 && proceduralBuildings.Count > 0) ? Math.Round(unitDensity).ToString() : "N/A",
                    (habDensity != -1 && proceduralBuildings.Count > 0) ? Math.Round(habDensity).ToString() : "N/A",
                    String.Format(numbersFormat, Math.Round(siteArea)),
                    ptalZone,
                    (proceduralBuildings.Count > 0) ? Math.Round((Mathf.Round(NIA) / Mathf.Round(GIA)) * 100, 2).ToString() : "0",
                    (proceduralBuildings.Count > 0) ? Math.Round((Mathf.Round(GIA) / Mathf.Round(GEA)) * 100, 2).ToString() : "0",
                    (sitePolygon != null) ? Math.Round((groundFloorArea / sitePolygon.Area) * 100, 2).ToString() : "N/A",
                    String.Format(numbersFormat, Math.Round(receptionArea)),
                    String.Format(numbersFormat, Math.Round(commercialArea)),
                    Math.Round(siteArea * SqMeterToHectares, 3),
                    String.Format(numbersFormat, Mathf.Round(GEA * SqMeterToSqFoot)),
                    String.Format(numbersFormat, Mathf.Round(GIA * SqMeterToSqFoot)),
                    String.Format(numbersFormat, Mathf.Round(NIA * SqMeterToSqFoot)),
                    String.Format(numbersFormat, Math.Round(plantRoomArea * SqMeterToSqFoot)),
                    String.Format(numbersFormat, Math.Round(receptionArea * SqMeterToSqFoot)),
                    String.Format(numbersFormat, Math.Round(commercialArea * SqMeterToSqFoot)),
                    TotalNumberOfFloors(),
                    String.Format(numbersFormat, Math.Round(otherArea)),
                    String.Format(numbersFormat, Math.Round(otherArea * SqMeterToSqFoot)),
                    (proceduralBuildings.Count > 0) ? Math.Round(facadeArea / GEA, 2).ToString() : "0",
                    String.Format(numbersFormat, Mathf.Round(podiumArea)),
                    String.Format(numbersFormat, Mathf.Round(podiumArea * SqMeterToSqFoot)),
                    String.Format(numbersFormat, Mathf.Round(basementArea)),
                    String.Format(numbersFormat, Mathf.Round(basementArea * SqMeterToSqFoot)),
                    PodiumUses,
                    BasementUses,
                    (sitePolygon != null) ? Math.Round((GEA / sitePolygon.Area) * 100, 2).ToString() : "N/A",
                    SiteUsage,
                    DisplayHeights(names, heights)
                    );

                netToGrossSlider.value = (float)Math.Round((NIA / GIA) * 100, 1);
                netToGrossSlider.transform.GetChild(2).GetChild(0).GetChild(0).GetComponent<Text>().text = (float)Math.Round((NIA / GIA) * 100, 0) + "%";
                info[1] += "\n";
                info[1] += "Total = " + totalAptNumber + "\n";
                info[1] += string.Format("North Facing Apts =  {0} ({5}%)\r\n" +
                    "East Facing Apts =  {1} ({6}%)\r\n" +
                    "South Facing Apts =  {2} ({7}%)\r\n" +
                    "West Facing Apts =  {3} ({8}%)\r\n" +
                    "Dual Aspect Apts =  {4} ({9}%)\r\n" +
                    "M3 Apts = {10} ({11}%)\r\n",
                    northApts, eastApts, southApts, westApts, dualApts,
                    Mathf.Round(((float)northApts / totalAptNumber) * 100),
                    Mathf.Round(((float)eastApts / totalAptNumber) * 100),
                    Mathf.Round(((float)westApts / totalAptNumber) * 100),
                    Mathf.Round(((float)southApts / totalAptNumber) * 100),
                    Mathf.Round(((float)dualApts / totalAptNumber) * 100),
                    m3,
                    Mathf.Round(((float)m3 / totalAptNumber) * 100));
                foreach (var item in aptNumbers)
                {
                    info[1] += string.Format("{0} = {1}, {2}%\n", item.Key == "SpecialApt" ? "Unresolved" : item.Key, item.Value, Math.Round((item.Value / totalAptNumber) * 100, 2));
                }
                info[1] += "\n";
                info[1] += GetTotalBriefDifference();
                return info;
            }
        }

        private string DisplayHeights(List<string> names, List<float> heights)
        {
            string heightString = string.Empty;

            for (int i = 0; i < heights.Count; i++)
            {
                heightString += "\n" + (string.IsNullOrEmpty(names[i]) ? "No Name" : names[i]) + " = " + heights[i].ToString("0.0") + "m";
            }

            return heightString;
        }

        /// <summary>
        /// Returns the total number of floors
        /// </summary>
        /// <returns>Int</returns>
        public int TotalNumberOfFloors()
        {
            int number = 0;
            for (int i = 0; i < proceduralBuildings.Count; i++)
            {
                number += proceduralBuildings[i].numberOfFloors;
            }
            return number;
        }

        /// <summary>
        /// Returns the difference between the original brief set and the actual distribution of apartments
        /// </summary>
        /// <returns>String</returns>
        public string GetTotalBriefDifference()
        {
            string data = "";

            Dictionary<string, float> areas = new Dictionary<string, float>();
            Dictionary<string, float> percentages = new Dictionary<string, float>();
            float totalPercentages = 0;
            float totalArea = 0;
            try
            {
                for (int j = 0; j < proceduralBuildings.Count; j++)
                {
                    for (int i = 1; i < proceduralBuildings[j].floors.Count; i++)
                    {
                        foreach (var item in proceduralBuildings[j].floors[i].percentages)
                        {
                            if (percentages.ContainsKey(item.Key))
                            {
                                percentages[item.Key] += item.Value;
                            }
                            else
                            {
                                percentages.Add(item.Key, item.Value);
                            }
                            totalPercentages += item.Value;
                        }


                        var floor = proceduralBuildings[j].floors[i];
                        if (floor != null && floor.apartments != null)
                        {
                            for (int k = 0; k < floor.apartments.Count; k++)
                            {
                                if (areas.ContainsKey(floor.apartments[k].ApartmentType))
                                {
                                    areas[floor.apartments[k].ApartmentType] += floor.apartments[k].Area * floor.levels.Length;
                                }
                                else
                                {
                                    areas.Add(floor.apartments[k].ApartmentType, floor.apartments[k].Area * floor.levels.Length);
                                }
                                totalArea += floor.apartments[k].Area * floor.levels.Length;
                            }
                        }
                    }
                }

                if (areas.Count > 0)
                {
                    foreach (var item in percentages)
                    {
                        if (item.Key != "Commercial" && item.Key != "Other")
                        {
                            var perc = percentages[item.Key];
                            perc /= totalPercentages;
                            perc *= 100;
                            perc = (float)Math.Round(perc, 1);
                            var area = areas[item.Key];
                            area /= totalArea;
                            area *= 100;
                            area = (float)Math.Round(area, 1);
                            data += string.Format("{0} = Brief {1}% vs Current {2}% \n", item.Key, Math.Round(perc, 1), Math.Round(area, 1));
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }

            return data;
        }

        /// <summary>
        /// Called when an apartment has changed
        /// </summary>
        public void OnApartmentChanged()
        {
            if (populateApartments)
            {
                StartCoroutine(PopulateInterior(false));
            }
        }

        /// <summary>
        /// Updates the line display of the total height of the building
        /// </summary>
        /// <param name="poly">The polygon which triggered this update</param>
        public void UpdateHeightLine(Polygon poly)
        {
            float totalHeight = floors[designInputTabs.buildingIndex + ",0"].floor2floor;
            for (int i = 1; i < designInputTabs.currentBuilding.levels.Count; i++)
            {
                try
                {
                    string key = designInputTabs.buildingIndex + "," + i;
                    var floor = floors[key];
                    totalHeight += designInputTabs.currentBuilding.levels[i].Count * floor.floor2floor;
                }
                catch (Exception e)
                {
                    Debug.Log(e);
                }
            }
            //For the transfer decks
            totalHeight += (designInputTabs.currentBuilding.levels.Count - 1) * 0.5f;
            Vector3 start = (poly[0].currentPosition + (poly[0].currentPosition - poly[1].currentPosition).normalized * offsetDistanceLine);
            Vector3 end = start + Vector3.up * totalHeight;
            UpdateDistance(start, end, heightLines[designInputTabs.buildingIndex], designInputTabs.buildingIndex);
        }

        /// <summary>
        /// Returns the difference between the original brief set and the current state
        /// </summary>
        /// <returns>String</returns>
        public string GetBriefDifference()
        {
            return proceduralBuildings[currentBuildingIndex].GetBriefDifference();
        }

        /// <summary>
        /// Returns a list of apartments that are ofa specific type
        /// </summary>
        /// <param name="type">The apartment type</param>
        /// <returns>List of Apartments</returns>
        public List<ApartmentUnity> GetApartments(string type)
        {
            List<ApartmentUnity> apartments = new List<ApartmentUnity>();

            for (int i = 0; i < proceduralBuildings.Count; i++)
            {
                apartments.AddRange(proceduralBuildings[i].GetApartments(type));
            }

            return apartments;
        }

        /// <summary>
        /// Refreshes all buildings and floors in the project
        /// </summary>
        public void RefreshAll()
        {
            float maxWidth = proceduralBuildings[currentBuildingIndex].currentMaximumWidth;
            if (float.TryParse(offsetInputField.text, out m_aptDepth) && float.TryParse(corridorInputField.text, out m_crdrWidth))
            {
                offsetDistance = m_aptDepth;
                corridorWidth = m_crdrWidth;
            }
            StartCoroutine(RefreshAllIterative(maxWidth));
            prevMinimumWidth = maxWidth;
        }

        /// <summary>
        /// Updates the core positions based on the maximum apartment width
        /// </summary>
        /// <param name="refresh">Whether the core positions should be updated</param>
        public void DiffWidthPopUpAnswer(bool refresh)
        {
            if (refresh)
            {
                float maxWidth = proceduralBuildings[currentBuildingIndex].currentMaximumWidth;
                if (float.TryParse(offsetInputField.text, out m_aptDepth) && float.TryParse(corridorInputField.text, out m_crdrWidth))
                {
                    offsetDistance = m_aptDepth;
                    corridorWidth = m_crdrWidth;
                }
                StartCoroutine(Refresh(maxWidth));
                prevMinimumWidth = maxWidth;
            }
            else
            {
                if (float.TryParse(offsetInputField.text, out m_aptDepth) && float.TryParse(corridorInputField.text, out m_crdrWidth))
                {
                    offsetDistance = m_aptDepth;
                    corridorWidth = m_crdrWidth;
                    StartCoroutine(Offset(m_aptDepth, proceduralBuildings[currentBuildingIndex].prevMinimumWidth));
                }
            }
            difWidthPopUp.SetActive(false);
        }

        /// <summary>
        /// Called by the Unity UI to place a tower building
        /// </summary>
        public void PlaceTower()
        {
            float.TryParse(offsetInputField.text, out m_aptDepth);
            canvasGroupCP.blocksRaycasts = false;
            if (!proceduralBuildings[currentBuildingIndex].Placed)
            {
                //currentBuilding.baseHeight = 0;
                placingTower = true;
                //rotatingBuilding = true;
                for (int i = 1; i < CurrentBuilding.floors.Count; i++)
                {
                    var tower = CurrentBuilding.floors[i] as ProceduralTower;
                    tower.aptDepth = m_aptDepth;
                    tower.GenerateGeometries(PreviewMode.Floors);
                }
                var towerGround = CurrentBuilding.floors[0] as ProceduralTower;
                towerGround.aptDepth = m_aptDepth;
                //towerGround.baseTower = currentBuilding.floors[1] as ProceduralTower;
                towerGround.GenerateGeometries(PreviewMode.Floors);
                //proceduralBuildings[currentBuildingIndex].AddTowerFloor(1, "Tower", "Config01");
            }
            else
            {
                movingBuilding = true;
            }
        }
        /// <summary>
        /// Called by Unity UI to place a mansion block building
        /// </summary>
        public void PlaceMansionBlock()
        {
            if (!proceduralBuildings[currentBuildingIndex].Placed)
            {
                //currentBuilding.baseHeight = 0;
                placingMansionBlock = true;
                //rotatingBuilding = true;
                for (int i = 1; i < CurrentBuilding.floors.Count; i++)
                {
                    var mansion = CurrentBuilding.floors[i] as ProceduralMansion;
                    mansion.leftAptDepth = mansion_aptDepth;
                    mansion.aptDepth = mansion_aptDepth;
                    mansion.GenerateGeometries(PreviewMode.Floors);
                }
                var mansionGround = CurrentBuilding.floors[0] as ProceduralMansion;
                mansionGround.leftAptDepth = mansion_aptDepth;
                mansionGround.aptDepth = mansion_aptDepth;
                //towerGround.baseTower = currentBuilding.floors[1] as ProceduralTower;
                mansionGround.GenerateGeometries(PreviewMode.Floors);
                //proceduralBuildings[currentBuildingIndex].AddTowerFloor(1, "Tower", "Config01");
            }
            else
            {
                movingBuilding = true;
            }
        }

        /// <summary>
        /// Called from the UI to enable the movement of the building
        /// </summary>
        public void MoveBuilding()
        {
            if (CurrentBuilding.masterPolyline != null)
            {
                canvasGroupCP.blocksRaycasts = false;
                movingBuilding = true;
            }
        }

        /// <summary>
        /// Called by the Unity UI to rotate the selected tower
        /// </summary>
        public void RotateBuilding()
        {
            canvasGroupCP.blocksRaycasts = false;
            rotatingBuilding = true;
        }
        /// <summary>
        /// Called by Unity UI to enable the movement of a site-wide basement
        /// </summary>
        public void MoveSiteBasement()
        {
            if (siteBasement != null && siteBasement.isCustomPolygon)
            {
                canvasGroupCP.blocksRaycasts = false;
                movingBasement = true;
            }
        }
        /// <summary>
        /// Called by Unity UI to enable the rotation of a site-wide basement
        /// </summary>
        public void RotateSiteBasement()
        {
            if (siteBasement != null && siteBasement.isCustomPolygon)
            {
                canvasGroupCP.blocksRaycasts = false;
                rotatingBasement = true;
            }
        }
        /// <summary>
        /// Called by Unity UI to enable the movement of a site-wide podium
        /// </summary>
        public void MoveSitePodium()
        {
            if (sitePodium != null && sitePodium.isCustomPolygon)
            {
                canvasGroupCP.blocksRaycasts = false;
                movingPodium = true;
            }
        }
        /// <summary>
        /// Called by Unity UI to enable the rotation of a site-wide podium
        /// </summary>
        public void RotateSitePodium()
        {
            if (sitePodium != null && sitePodium.isCustomPolygon)
            {
                canvasGroupCP.blocksRaycasts = false;
                rotatingPodium = true;
            }
        }

        /// <summary>
        /// Offsets the centreline and generates the floor layout
        /// </summary>
        public void Offset()
        {
            if (CurrentBuilding.buildingType == BuildingType.Linear)
            {
                if (/*float.TryParse(offsetInputField.text, out m_aptDepth) && */float.TryParse(corridorInputField.text, out m_crdrWidth))
                {
                    offsetDistance = m_aptDepth;
                    corridorWidth = m_crdrWidth;
                    CurrentBuilding.SetOffsetDistance(m_aptDepth);
                    float maxWidth = CurrentBuilding.currentMaximumWidth;
                    if (CurrentBuilding.prevMinimumWidth >= CurrentBuilding.currentMaximumWidth || !CurrentBuilding.floors[0].hasGeometry)
                    {
                        StartCoroutine(Offset(offsetDistance, maxWidth));
                    }
                    else
                    {
                        difWidthPopUp.SetActive(true);
                    }
                }
                else
                {
                    designInputTabs.generateButtonTooltip.warningMessage += "The apartment depth must be set (ex. 6.8 m)!\r\n\r\n";
                }
            }
            else
            {
                CurrentBuilding.GenerateFloors();
            }
        }

        /// <summary>
        /// Called by the Unity UI to set the apartment depth
        /// </summary>
        /// <param name="val">The apartment depth as a string</param>
        public void SetOffsetDistance(string val)
        {
            float.TryParse(offsetInputField.text, out m_aptDepth);
            offsetDistance = m_aptDepth;
        }

        /// <summary>
        /// Called by the Unity UI to set the corridor width
        /// </summary>
        /// <param name="val">The value of the input field</param>
        public void SetCorridorWidth(string val)
        {
            float.TryParse(corridorInputField.text, out m_crdrWidth);
            corridorWidth = m_crdrWidth;
        }
        /// <summary>
        /// Sets the width of the core for the current building
        /// </summary>
        /// <param name="val">The new core width</param>
        public void SetCoreWidth(string val)
        {
            if (float.TryParse(val, out m_coreWidth))
                CurrentBuilding.coreWidth = m_coreWidth;
        }
        /// <summary>
        /// Sets the length of the core for the current building
        /// </summary>
        /// <param name="val">The new core length</param>
        public void SetCoreLength(string val)
        {
            if (float.TryParse(val, out m_coreLength))
                CurrentBuilding.coreLength = m_coreLength;
        }

        /// <summary>
        /// Offsets the centreline and generates the floor layout
        /// </summary>
        /// <param name="depth">The apartment depth</param>
        /// <param name="maxWidth">The maximum apartment width (for the calculation of the positions of the cores)</param>
        public IEnumerator Offset(float depth, float maxWidth)
        {
            yield return StartCoroutine(CurrentBuilding.Offset(depth, maxWidth));

            OnPreviewModeChanged(previewMode);
            StartCoroutine(OverallUpdate());
        }

        /// <summary>
        /// Returns the total plant area needed for the building
        /// </summary>
        /// <param name="index">The building index</param>
        /// <returns>Float</returns>
        public float GetBuildingPlantArea(int index)
        {
            return proceduralBuildings[index].GetBuildingPlantArea();
        }

        /// <summary>
        /// Clears all undo actions stored
        /// </summary>
        public void ClearUndoActions()
        {
            GetComponent<UndoActionLog>().Clear();
        }

        /// <summary>
        /// Updates the Internal Layout of the floors
        /// </summary>
        /// <returns>IEnumerator</returns>
        public IEnumerator UpdateInternalLayout()
        {
            yield return UpdateInternalLayout(populateApartments);
        }

        /// <summary>
        /// Creates a new building of a given type
        /// </summary>
        /// <param name="type">The type of the building to be created</param>
        /// <returns>Procedural Building</returns>
        public ProceduralBuilding CreateNewBuilding(BuildingType type)
        {
            if (type == BuildingType.Tower)
            {
                moveButton.gameObject.SetActive(false);
                placeButton.gameObject.SetActive(true);
                rotateButton.interactable = false;
            }

            if (perBuildingPlantArea == null)
            {
                perBuildingPlantArea = new List<float>();
            }
            perBuildingPlantArea.Add(0);
            if (previewMode == PreviewMode.Floors)
            {
                if (currentFloor != null)
                {
                    if (currentFloor.centreLine != null)
                    {
                        currentFloor.centreLine.gameObject.SetActive(false);
                    }
                    currentFloor.gameObject.SetActive(false);
                }
            }

            //currentFloor = null;
            var obj = Instantiate(proceduralBuildingPrefab, transform);
            obj.name = "Building_" + proceduralBuildings.Count;
            //obj.GetComponent<ProceduralBuilding>().buildingName = designInputTabs.currentBuilding.name;
            //obj.name = designInputTabs.currentBuilding.name;
            var rootObj = new GameObject("RepRoot_Building_" + proceduralBuildings.Count);
            rootObj.transform.SetParent(repRootParent);
            rootObj.transform.localPosition = Vector3.zero;
            rootObj.transform.localEulerAngles = Vector3.zero;
            rootObj.transform.localScale = Vector3.one;
            obj.GetComponent<ProceduralBuilding>().Initialize();
            obj.GetComponent<ProceduralBuilding>().repRootParent = rootObj.transform;
            obj.GetComponent<ProceduralBuilding>().distanceUIParent = distanceUIParent;
            proceduralBuildings.Add(obj.GetComponent<ProceduralBuilding>());
            obj.GetComponent<ProceduralBuilding>().buildingManager = this;
            //obj.GetComponent<ProceduralBuilding>().index = buildingIDCounter;
            obj.GetComponent<ProceduralBuilding>().distanceUIParent = distanceUIParent;
            buildings.Add(obj.transform);
            buildingIDCounter++;
            StartCoroutine(OverallUpdate());
            customPlButton.interactable = false;
            return obj.GetComponent<ProceduralBuilding>();
        }

        /// <summary>
        /// Deletes the building ar a given index
        /// </summary>
        /// <param name="buildingIndex">The index of the building to be deleted</param>
        public void DeleteBuilding(int buildingIndex)
        {
            if (proceduralBuildings.Count > buildingIndex)
            {
                Destroy(proceduralBuildings[buildingIndex].gameObject);
                proceduralBuildings.RemoveAt(buildingIndex);
            }
        }

        /// <summary>
        /// Sets the selected floor of a building
        /// </summary>
        /// <param name="building">The building</param>
        /// <param name="floorIndex">The index of the floor to be selected</param>
        public void SetSelectedFloor(ProceduralBuilding building, int floorIndex)
        {
            if (floorIndex >= 0)
            {
                if (populateApartments && previewMode == PreviewMode.Floors)
                {
                    populateApartments = false;
                    manufacturingDropDown.SetValue(0);
                    //populateInteriorToggle.SetValue(false);
                }
                building.currentFloor = building.floors[floorIndex];
                apartmentTypesChart.selected = false;
                apartmentTypesChart.OnNonHovered();
                apartmentTypesChart.designData = currentFloor;
                StartCoroutine(OverallUpdate());

                if (previewMode == PreviewMode.Floors)
                {
                    Camera.main.GetComponent<DrawBuildingLines>().selectedKey = new List<string>() { building.floors[floorIndex].key };
                }
                else
                {
                    Camera.main.GetComponent<DrawBuildingLines>().selectedKey = new List<string>();
                    for (int i = 0; i < building.floors.Count; i++)
                    {
                        Camera.main.GetComponent<DrawBuildingLines>().selectedKey.Add(building.floors[i].key);
                    }
                }
                var draw = Camera.main.GetComponent<DrawBuildingLines>();

                float minF2f = 0;
                float maxF2f = 0;
                float maxModule = 0;
                float maxSpan = 0;
                int storiesNum = 0;

                proceduralBuildings[currentBuildingIndex].GetSystemNumbers(ref minF2f, ref maxF2f, ref maxModule, ref maxSpan, ref storiesNum);

                volumetricTrafficLight.UpdateTrafficSystem(storiesNum, minF2f, maxF2f, maxModule, maxSpan);
                panelisedTrafficLight.UpdateTrafficSystem(storiesNum, minF2f, maxF2f, maxModule, maxSpan);
                if (platformsTrafficLight != null)
                {
                    float bay = GetMaxPlatformsBay(currentBuildingIndex);
                    float span = GetMaxAptDepth(currentBuildingIndex);
                    CheckBuildingConstraintsForPlatforms(storiesNum, span);
                    platformsTrafficLight.UpdateTrafficSystem(storiesNum, minF2f, maxF2f, bay, span);
                }
                ProceduralMansion mansion = building.currentFloor as ProceduralMansion;
                if (mansion != null)
                {
                    blocksPanel.blocksChanged.RemoveAllListeners();
                    blocksPanel.blocksChanged.AddListener(mansion.OnBlocksChanged);
                }
            }
        }

        /// <summary>
        /// Checks a building against the constraints of the different manufacturing systems
        /// </summary>
        /// <param name="building">The building to be checked</param>
        public void CheckSystemConstraints(ProceduralBuilding building)
        {
            float minF2f = 0;
            float maxF2f = 0;
            float maxModule = 0;
            float maxSpan = 0;
            int storiesNum = 0;

            building.GetSystemNumbers(ref minF2f, ref maxF2f, ref maxModule, ref maxSpan, ref storiesNum);

            volumetricTrafficLight.UpdateTrafficSystem(storiesNum, minF2f, maxF2f, maxModule, maxSpan);
            panelisedTrafficLight.UpdateTrafficSystem(storiesNum, minF2f, maxF2f, maxModule, maxSpan);
            if (platformsTrafficLight != null)
            {
                int index = proceduralBuildings.IndexOf(building);
                float bay = GetMaxPlatformsBay(index);
                float span = GetMaxAptDepth(index);
                CheckBuildingConstraintsForPlatforms(storiesNum, span);
                platformsTrafficLight.UpdateTrafficSystem(storiesNum, minF2f, maxF2f, bay, span);
            }
        }

        /// <summary>
        /// Checks the currently selected building against the constraints of the different manufacturing systems
        /// </summary>
        public void CheckSystemConstraints()
        {
            try
            {
                if (proceduralBuildings.Count > 0)
                    CheckSystemConstraints(proceduralBuildings[currentBuildingIndex]);
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }
        }

        /// <summary>
        /// Adds a floor to a specific building
        /// </summary>
        /// <param name="buildingIndex">The index of the building</param>
        /// <param name="floorIndex">The index of the floor</param>
        /// <param name="valuesChanged"></param>
        /// <returns></returns>
        public ProceduralFloor AddFloor(int buildingIndex, int floorIndex, bool valuesChanged = false)
        {
            proceduralBuildings[buildingIndex].typology = typology;
            proceduralBuildings[buildingIndex].coreAllignment = coreAllignment;
            return proceduralBuildings[buildingIndex].AddFloor(floorIndex, designInputTabs.buildings[designInputTabs.buildingIndex].name);
        }

        /// <summary>
        /// Called when the balconies have changed
        /// </summary>
        public void OnBalconiesChanged()
        {
            if (previewMode == PreviewMode.Apartments)
            {
                StartCoroutine(RefreshProceduralApartment(apartmentDropdown.value, proceduralApartment.m3));
            }
        }

        /// <summary>
        /// Called when the apartment area ranges have changed
        /// </summary>
        /// <param name="sender">The Standards class which manages the area ranges</param>
        /// <param name="redraw">Whether the apartment layout should be re-drawn</param>
        public void OnAreaRangesUpdated(Standards sender, bool redraw)
        {
            if (previewMode == PreviewMode.Apartments && redraw)
            {
                StartCoroutine(RefreshProceduralApartment(apartmentDropdown.value, proceduralApartment.m3));
            }
        }
        /// <summary>
        /// Called when the front view has been selected in the apartment layout mode
        /// </summary>
        public void OnFrontViewSelected()
        {
            apartmentTopCamera.gameObject.SetActive(false);
            apartmentTopCamera.tag = "Untagged";
            apartmentFrontCamera.gameObject.SetActive(true);
            apartmentFrontCamera.tag = "MainCamera";
            apartmentBackCamera.gameObject.SetActive(false);
            apartmentBackCamera.tag = "Untagged";
            dimensionsParent.SetActive(false);
            roomNames.SetActive(false);
            amenitiesButtons.SetActive(false);
            frontViewButton.gameObject.SetActive(false);
            backViewButton.gameObject.SetActive(false);
            topViewButton.gameObject.SetActive(true);
            apartmentViewMode = ApartmentViewMode.Front;
            proceduralApartment.ToggleMaxLayout(false);
            proceduralApartment.GetComponent<ApartmentDimensions>().ToggleDimensions(true, apartmentViewMode);
        }
        /// <summary>
        /// Called when the back view has been selected in the apartment layout mode
        /// </summary>
        public void OnBackViewSelected()
        {
            apartmentTopCamera.gameObject.SetActive(false);
            apartmentTopCamera.tag = "Untagged";
            apartmentFrontCamera.gameObject.SetActive(false);
            apartmentFrontCamera.tag = "Untagged";
            apartmentBackCamera.gameObject.SetActive(true);
            apartmentBackCamera.tag = "MainCamera";
            dimensionsParent.SetActive(false);
            roomNames.SetActive(false);
            amenitiesButtons.SetActive(false);
            frontViewButton.gameObject.SetActive(false);
            backViewButton.gameObject.SetActive(false);
            topViewButton.gameObject.SetActive(true);
            apartmentViewMode = ApartmentViewMode.Back;
            proceduralApartment.ToggleMaxLayout(false);
            proceduralApartment.GetComponent<ApartmentDimensions>().ToggleDimensions(true, apartmentViewMode);
        }
        /// <summary>
        /// Called when the top view has been selected in the apartment layout mode
        /// </summary>
        public void OnTopViewSelected()
        {
            apartmentTopCamera.gameObject.SetActive(true);
            apartmentTopCamera.tag = "MainCamera";
            apartmentFrontCamera.gameObject.SetActive(false);
            apartmentFrontCamera.tag = "Untagged";
            apartmentBackCamera.gameObject.SetActive(false);
            apartmentBackCamera.tag = "Untagged";
            dimensionsParent.SetActive(true);
            roomNames.SetActive(true);
            amenitiesButtons.SetActive(true);
            frontViewButton.gameObject.SetActive(true);
            backViewButton.gameObject.SetActive(true);
            topViewButton.gameObject.SetActive(false);
            apartmentViewMode = ApartmentViewMode.Top;
            proceduralApartment.ToggleMaxLayout(true);
            proceduralApartment.GetComponent<ApartmentDimensions>().ToggleDimensions(true, apartmentViewMode);
        }
        /// <summary>
        /// Called when the user requests for a Three.Js representation of the project
        /// </summary>
        /// <returns>IEnumerator</returns>
        public IEnumerator OnExportThreeJs()
        {
            LoadingBar.TaggedObject.Enable(true);
            LoadingBar.TaggedObject.SetValue("Getting Geometry", 0.0f);
            PreviewMode prevMode = previewMode;
            ChangePreviewMode(PreviewMode.Buildings);

            if (!populateApartments)
            {
                yield return StartCoroutine(PopulateForExport());
            }
            else
            {
                yield return waitFrame;
            }

            List<BT.Geometry> geometries = new List<BT.Geometry>();
            BT.Object3d scene = new BT.Object3d();

            for (int i = 0; i < proceduralBuildings.Count; i++)
            {
                scene.children.Add(proceduralBuildings[i].GetThreeJsObject());
            }

            for (int i = 0; i < repRootParent.childCount; i++)
            {
                for (int j = 0; j < repRootParent.GetChild(i).childCount; j++)
                {
                    for (int k = 0; k < repRootParent.GetChild(i).GetChild(j).childCount; k++)
                    {
                        repRootParent.GetChild(i).GetChild(j).GetChild(k).GetComponent<ProceduralApartmentLayout>().UpdateLinesCoords(i, j);
                        scene.children.AddRange(repRootParent.GetChild(i).GetChild(j).GetChild(k).GetComponent<ProceduralApartmentLayout>().lines);
                    }
                }
            }

            RecursivelyGetGeometries(scene, geometries);
            yield return null;

            Dictionary<string, object> config = new Dictionary<string, object>();
            config.Add("geometries", geometries);
            config.Add("Object", scene);
            string data = JsonConvert.SerializeObject(config, Formatting.Indented, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
            File.WriteAllText(Application.streamingAssetsPath + "/three.json", data);

            LoadingBar.TaggedObject.Enable(false);
            OnPreviewModeChanged(prevMode);
        }
        #endregion

        #region Private Methods
        private void RecursivelyGetGeometries(BT.Object3d obj, List<BT.Geometry> geoms)
        {
            if (obj._geometry != null)
            {
                geoms.Add(obj._geometry);
            }
            if (obj.children != null && obj.children.Count > 0)
            {
                for (int i = 0; i < obj.children.Count; i++)
                {
                    RecursivelyGetGeometries(obj.children[i], geoms);
                }
            }
        }

        private void SetSelectedApartment(BaseDesignData sender)
        {
            aptToSwap = new List<ApartmentUnity>();
            aptToSwap.Add(sender as ApartmentUnity);
        }

        private IEnumerator ShowInterior()
        {
            Debug.Log("ShowInterior");
            frontViewButton.gameObject.SetActive(true);
            frontViewButton.gameObject.SetActive(true);
            yield return StartCoroutine(proceduralApartment.ResetLayout());
            proceduralApartment.width = aptToSwap[0].Width - Standards.TaggedObject.ConstructionFeatures["PartyWall"];
            proceduralApartment.readOnly = true;
            proceduralApartment.apartmentType = aptToSwap[0].ApartmentType;
            proceduralApartment.modulesScale = 1;
            //proceduralApartment.SetRooms(Standards.TaggedObject.ApartmentTypeRooms[aptToSwap[0].ApartmentType]);
            proceduralApartment.SetRooms(Standards.TaggedObject.aptLayouts[aptToSwap[0].ProceduralFloor.building.buildingType][aptToSwap[0].ApartmentType + "_M2"]);
            apartmentTopCamera.transform.position = new Vector3(proceduralApartment.width / 2, 20, proceduralApartment.height / 2);
            apartmentFrontCamera.transform.position = new Vector3(proceduralApartment.width / 2, 1.7f, 20);
            apartmentBackCamera.transform.position = new Vector3(proceduralApartment.width / 2, 1.7f, -20);
            proceduralApartment.Initialize();
            ToggleManufacturingSystem(manufacturingSystem);

            for (int i = 0; i < apartmentDropdown.options.Count; i++)
            {
                if (apartmentDropdown.options[i].text == aptToSwap[0].ApartmentType)
                {
                    apartmentDropdown.SetValue(i);
                    apartmentDropdown.RefreshShownValue();
                    apartmentDropdown.interactable = false;
                }
            }
            proceduralApartment.GetComponent<ApartmentDimensions>().ToggleDimensions(true);
            proceduralApartment.ShowOutline(true);
            if (apartmentLayoutChanged != null)
            {
                apartmentLayoutChanged();
            }
            //Selector.TaggedObject.ClearSelect();
        }


        private void DeleteFloor(int buildingIndex, int floorIndex, bool valuesChanged = false)
        {
            proceduralBuildings[buildingIndex].DeleteFloor(floorIndex);
        }

        private void AddCustomFloorPolygon(PolygonDrawer drawer, PolygonSegment polygon)
        {
            if (drawer == polygonDrawer && !drawer.IsBasement && !drawer.IsPodium)
            {
                proceduralBuildings[currentBuildingIndex].AddCustomFloorPolygon(offsetDistance, polygon);
            }
        }


        private void AddHeightLine(Polygon poly)
        {
            if (designInputTabs.buildingIndex < heightLines.Count)
            {
                Destroy(heightLines[designInputTabs.buildingIndex]);
                heightLines.RemoveAt(designInputTabs.buildingIndex);
            }

            currentDistance = new GameObject("Distance_" + heightLines.Count);
            heightLines.Add(currentDistance);
            currentDistance.transform.SetParent(distancesParent);

            GameObject text = Instantiate(Resources.Load("Geometry/TextMesh") as GameObject, currentDistance.transform);
            text.transform.Rotate(text.transform.right, 90);
            TextMesh mText = text.GetComponent<TextMesh>();
            mText.characterSize = 1;

            if (useCanvas)
            {
                mText.GetComponent<MeshRenderer>().enabled = false;
                GameObject distUI = Instantiate(Resources.Load("GUI/DistanceText") as GameObject, distanceUIParent);
                distancesUI.Add(distUI.GetComponent<Text>());
                distUI.GetComponent<Text>().color = distancesMaterial.color;
                distUI.GetComponent<UiRepresentation>().Initialize(text.transform);
            }

            GameObject line1 = new GameObject("Line1");
            line1.transform.SetParent(currentDistance.transform);
            LineRenderer l1 = line1.AddComponent<LineRenderer>();
            l1.material = distancesMaterial;
            l1.useWorldSpace = true;
            l1.positionCount = 2;
            l1.startWidth = lineWidth;
            l1.endWidth = lineWidth;
            l1.SetPosition(0, (poly[0].currentPosition + (poly[0].currentPosition - poly[1].currentPosition).normalized * offsetDistanceLine));
            l1.SetPosition(1, (poly[0].currentPosition + (poly[0].currentPosition - poly[1].currentPosition).normalized * offsetDistanceLine));
        }

        private void UpdateDistance(Vector3 start, Vector3 end, GameObject distance, int index)
        {
            float dist = Vector3.Distance(start, end);

            TextMesh mText = distance.transform.GetChild(0).GetComponent<TextMesh>();
            mText.text = dist.ToString("0.00");
            distance.transform.GetChild(0).position = start + (end - start) / 2 + Vector3.Cross((end - start), Vector3.right).normalized * distanceOffset;

            if (useCanvas)
            {
                distancesUI[index].GetComponent<RectTransform>().position = Camera.main.WorldToScreenPoint(distance.transform.GetChild(0).position);
                distancesUI[index].text = dist.ToString("0.00");
            }

            float angle = Vector3.Angle(Vector3.right, end - start);
            if (Vector3.Cross((end - start), Vector3.right).y < 0)
            {
                angle = -angle;
            }
            distance.transform.GetChild(0).eulerAngles = new Vector3(90, -angle, 0);

            LineRenderer l1 = distance.transform.GetChild(1).GetComponent<LineRenderer>();
            l1.SetPosition(0, start);
            l1.SetPosition(1, end);
        }

        private void OnPolygonLoaded(PolygonDrawer drawer, Polygon polygon)
        {
            if (drawer == polygonDrawer)
            {
                if (!drawer.IsBasement && !drawer.IsPodium)
                {
                    CurrentBuilding.SetMasterPolyline(polygon);
                    if (CurrentBuilding.hasGeometry)
                    {
                        Refresh();
                    }
                }
                else if (drawer.IsBasement)
                {
                    if (customFloorBasement)
                    {
                        CurrentBuilding.basement.SetCustomExterior(polygon);
                        customFloorBasement = false;
                    }
                    else
                    {
                        polygon.gameObject.name = "SiteBasement";
                        siteBasement = new SiteBasement() { polygon = polygon, floor2Floor = siteBasementFloor2Floor, floors = siteBasementFloors };
                        polygon.transform.SetParent(basementParent);
                        Extrusion extrusion = Instantiate(extrusionPrefab, polygon.transform).GetComponent<Extrusion>();
                        extrusion.totalHeight = siteBasementFloors * siteBasementFloor2Floor * -1;
                        extrusion.Initialize(polygon);
                        polygon.extrusion = extrusion;
                        Material mat = new Material(Resources.Load("Materials/OutlineCutoff") as Material);
                        mat.SetFloat("_Modulo", siteBasementFloors);
                        mat.SetFloat("_UseTexture", 1);
                        polygon.SetOriginalMaterial(mat);
                        polygon.GetComponent<MeshRenderer>().enabled = false;
                        //currentBuilding.SetBasementPolygon(polygon);
                        CheckIfBuildingOverBasement(siteBasement);
                        //Trigger UI event
                        addUIBasementCustomPolygon.Invoke();
                    }
                }
                else if (drawer.IsPodium)
                {
                    if (customFloorPodium)
                    {
                        CurrentBuilding.podium.SetCustomExterior(polygon);
                        customFloorPodium = false;
                    }
                    else
                    {
                        polygon.gameObject.name = "SitePodium";
                        sitePodium = new SitePodium() { polygon = polygon, floor2Floor = sitePodiumFloor2Floor, floors = sitePodiumFloors };
                        polygon.transform.SetParent(podiumParent);
                        Extrusion extrusion = Instantiate(extrusionPrefab, polygon.transform).GetComponent<Extrusion>();
                        extrusion.totalHeight = sitePodiumFloors * sitePodiumFloor2Floor;
                        extrusion.capped = true;
                        extrusion.Initialize(polygon, true);
                        polygon.extrusion = extrusion;
                        Material mat = new Material(Resources.Load("Materials/OutlineCutoff") as Material);
                        mat.SetFloat("_Modulo", sitePodiumFloors);
                        mat.SetFloat("_UseTexture", 0);
                        mat.SetColor("_Color", Color.gray);
                        Material mat2 = new Material(Shader.Find("Standard"));
                        mat2.color = Color.gray;
                        polygon.SetOriginalMaterial(mat, mat2);
                        polygon.GetComponent<MeshRenderer>().enabled = false;
                        polygon.gameObject.SetLayerToChildren(21);
                        CheckIfBuildingOverPodium(sitePodium);
                        //Trigger UI event
                        addUIPodiumCustomPolygon.Invoke();
                    }
                }
            }
            else
            {
                if (drawer.isSite)
                {
                    if (sitePolygon != null)
                    {
                        DestroyImmediate(sitePolygon.gameObject);
                    }
                    sitePolygon = polygon;
                    StartCoroutine(OverallUpdate());
                }
            }
        }

        private void OnPolygonFinished(PolygonDrawer drawer, Polygon polygon)
        {
            if (drawer == polygonDrawer)
            {
                if (!drawer.IsBasement && !drawer.IsPodium)
                {
                    CurrentBuilding.SetMasterPolyline(polygon);
                    if (CurrentBuilding.hasGeometry)
                    {
                        Refresh();
                    }
                }
                else if (drawer.IsBasement)
                {
                    if (customFloorBasement)
                    {
                        if (CurrentBuilding.basement != null)
                            CurrentBuilding.basement.SetCustomExterior(polygon);
                        customFloorBasement = false;
                    }
                    else
                    {
                        polygon.gameObject.name = "SiteBasement";
                        siteBasement = new SiteBasement { polygon = polygon, floor2Floor = siteBasementFloor2Floor, floors = siteBasementFloors };
                        siteBasement.use = siteBasementPanel.GetPercentages();
                        siteBasement.isCustomPolygon = true;
                        polygon.transform.SetParent(basementParent);
                        Extrusion extrusion = Instantiate(extrusionPrefab, polygon.transform).GetComponent<Extrusion>();
                        extrusion.totalHeight = siteBasementFloors * siteBasementFloor2Floor * -1;
                        extrusion.Initialize(polygon);
                        polygon.extrusion = extrusion;
                        Material mat = new Material(Resources.Load("Materials/OutlineCutoff") as Material);
                        mat.SetFloat("_Modulo", siteBasementFloors);
                        mat.SetFloat("_UseTexture", 1);
                        //mat.color = Color.gray;
                        polygon.SetOriginalMaterial(mat);
                        polygon.GetComponent<MeshRenderer>().enabled = false;
                        //currentBuilding.SetBasementPolygon(polygon);
                        CheckIfBuildingOverBasement(siteBasement);
                        //Trigger UI event
                        addUIBasementCustomPolygon.Invoke();
                    }
                }
                else if (drawer.IsPodium)
                {
                    if (customFloorPodium)
                    {
                        if (CurrentBuilding.podium != null)
                            CurrentBuilding.podium.SetCustomExterior(polygon);
                        customFloorPodium = false;
                    }
                    else
                    {
                        polygon.gameObject.name = "SitePodium";
                        sitePodium = new SitePodium { polygon = polygon, floor2Floor = sitePodiumFloor2Floor, floors = sitePodiumFloors };
                        sitePodium.use = sitePodiumPanel.GetPercentages();
                        sitePodium.isCustomPolygon = true;
                        polygon.transform.SetParent(podiumParent);
                        Extrusion extrusion = Instantiate(extrusionPrefab, polygon.transform).GetComponent<Extrusion>();
                        extrusion.totalHeight = sitePodiumFloors * sitePodiumFloor2Floor;
                        extrusion.capped = true;
                        extrusion.Initialize(polygon, true);
                        polygon.extrusion = extrusion;
                        Material mat = new Material(Resources.Load("Materials/OutlineCutoff") as Material);
                        mat.SetFloat("_Modulo", sitePodiumFloors);
                        mat.SetFloat("_UseTexture", 0);
                        mat.SetColor("_Color", Color.gray);
                        Material mat2 = new Material(Shader.Find("Standard"));
                        mat2.color = Color.gray;
                        polygon.SetOriginalMaterial(mat, mat2);
                        polygon.GetComponent<MeshRenderer>().enabled = false;
                        polygon.gameObject.SetLayerToChildren(21);
                        CheckIfBuildingOverPodium(sitePodium);
                        //Trigger UI event
                        addUIPodiumCustomPolygon.Invoke();
                    }
                }
            }
            else
            {
                if (drawer.isSite)
                {
                    if (sitePolygon != null)
                    {
                        DestroyImmediate(sitePolygon.gameObject);
                    }
                    sitePolygon = polygon;
                    apartmentTypeStats.UpdateBuildingData();
                }
            }
        }

        private void LoadGeometries(float depth, FloorLayoutState loadState, ProceduralFloor masterFloor)
        {

            var building = proceduralBuildings[loadState.buildingIndex];
            building.LoadGeometries(depth, building.buildingElement.buildingData.floorHeights[loadState.floorIndex], loadState);
            apartmentTypesChart.designData = currentFloor;
            StartCoroutine(OverallUpdate());
        }

        private void LoadGeometries(float leftDepth, float rightDepth, FloorLayoutState loadState, ProceduralFloor masterFloor)
        {
            var building = proceduralBuildings[loadState.buildingIndex];
            building.LoadGeometries(leftDepth, rightDepth, building.buildingElement.buildingData.floorHeights[loadState.floorIndex], loadState);
            apartmentTypesChart.designData = currentFloor;
            StartCoroutine(OverallUpdate());
        }

        private void LoadPodiumGeometries(float leftDepth, float rightDepth, FloorLayoutState loadState, ProceduralFloor masterFloor)
        {
            var building = proceduralBuildings[loadState.buildingIndex];
            building.LoadGeometries(leftDepth, rightDepth, building.buildingElement.buildingData.podiumFloorHeight, loadState);
            StartCoroutine(OverallUpdate());
        }

        private void LoadBasementGeometries(float leftDepth, float rightDepth, FloorLayoutState loadState, ProceduralFloor masterFloor)
        {
            var building = proceduralBuildings[loadState.buildingIndex];
            building.LoadGeometries(leftDepth, rightDepth, building.buildingElement.buildingData.basementFloorHeight, loadState);
            StartCoroutine(OverallUpdate());
        }

        private void OnSaveDataLoaded()
        {
            StartCoroutine(LoadState(resourcesLoader.saveData));
        }

        private IEnumerator LoadState(string data)
        {
            LoadingBar.TaggedObject.Enable(true);
            load = JsonConvert.DeserializeObject<Save>(data);
            if (load.siteState != null)
            {
                mapHandler.SetLatLon(load.siteState.latitude, load.siteState.longitude);
                FeaturesManager.Instance.ActivateMapToggles();
                if (load.siteState.vertices != null)
                {
                    siteDrawer.LoadPolygon(load.siteState.vertices, true);
                }
                if (load.siteState.deletedExisting)
                {
                    mapHandler.DeleteExisting();
                }
                if (load.siteState.basements != null)
                    LoadSiteBasement(load.siteState.basements);
                if (load.siteState.podiums != null)
                    LoadSitePodium(load.siteState.podiums);
                gridObject.SetActive(false);
            }

            Standards.TaggedObject.ApartmentTypeRooms = load.standardsState.ApartmentTypeRooms;
            Standards.TaggedObject.ApartmentTypeRoomsM3 = load.standardsState.ApartmentTypeRoomsM3;
            Standards.TaggedObject.ApartmentTypeRoomsMansion = load.standardsState.ApartmentTypeRoomsMansion;
            Standards.TaggedObject.ApartmentTypeRoomsMansionM3 = load.standardsState.ApartmentTypeRoomsMansionM3;
            Standards.TaggedObject.ApartmentTypesMaximumSizes = load.standardsState.ApartmentTypesMaximumSizes;
            Standards.TaggedObject.apartmentTypesMinimumSizes = load.standardsState.ApartmentTypesMinimumSizes;
            Standards.TaggedObject.DesiredAreas = load.standardsState.DesiredAreas;
            Standards.TaggedObject.DesiredAreasM3 = load.standardsState.DesiredAreasM3;
            Standards.TaggedObject.amenitiesAreas = load.standardsState.AmenitiesAreas;
            Standards.TaggedObject.ConstructionFeatures = load.standardsState.ConstructionFeatures;
            Standards.TaggedObject.ValidLineLoads = load.standardsState.ValidLineLoads;
            Standards.TaggedObject.customVals = load.standardsState.customVals;
            Standards.TaggedObject.customValsM3 = load.standardsState.customValsM3;
            Standards.TaggedObject.TotalAmenitiesArea = load.standardsState.TotalAmenitiesArea;
            Standards.TaggedObject.balconies = (Balconies)load.standardsState.Balconies;
            Standards.TaggedObject.WindowsPositions = load.standardsState.WindowsPositions;
            Standards.TaggedObject.WindowsPositionsM3 = load.standardsState.WindowsPositionsM3;
            Standards.TaggedObject.WindowsPositionsMansion = load.standardsState.WindowsPositionsMansion;
            Standards.TaggedObject.WindowsPositionsMansionM3 = load.standardsState.WindowsPositionsMansionM3;

            Standards.TaggedObject.SetRoofFieldValue(load.standardsState.roofDepth);
            Standards.TaggedObject.SetParapetFieldValue(load.standardsState.parapetHeight);
            Standards.TaggedObject.SetRoomInclusions();
            Standards.TaggedObject.SetConstructionFeaturesToUI(load.standardsState.ConstructionFeaturesTitle);
            Standards.TaggedObject.EvaluateStandardRooms();
            Standards.TaggedObject.EvaluateStructuralWalls();
            Standards.TaggedObject.EvaluateAmenitiesArea();

            if (updateAreaRange != null)
            {
                updateAreaRange();
            }
            LoadingBar.TaggedObject.SetValue("Getting the data", 0.0f);
            yield return waitFrame;
            yield return StartCoroutine(designInputTabs.SetBriefState(load.briefState));
        }

        private void LoadSiteBasement(ExtraMassingState state)
        {
            siteBasementFloor2Floor = state.f2f;
            siteBasementFloors = state.floors;
            siteBasementOffset = state.offset;
            siteBasementPanel.SetValues(state.floors, state.f2f, state.offset);
            siteBasementPanel.SetBrief(state.percentages);
            if (state.isCustom)
            {
                siteBasementType = ExtraMassingType.Custom;
                LoadCustomSiteBasement(state);
            }
            else
            {
                GenerateSiteFootprintBasement();
            }
            siteBasement.use = state.percentages;
            siteBasement.buildingIndices = state.buildingIndices;
        }

        private void LoadSitePodium(ExtraMassingState state)
        {
            sitePodiumFloor2Floor = state.f2f;
            sitePodiumFloors = state.floors;
            sitePodiumOffset = state.offset;
            sitePodiumPanel.SetValues(state.floors, state.f2f, state.offset);
            sitePodiumPanel.SetBrief(state.percentages);
            if (state.isCustom)
            {
                sitePodiumType = ExtraMassingType.Custom;
                LoadCustomSitePodium(state);
            }
            else
            {
                GenerateSiteFootprintPodium();
            }
            sitePodium.use = state.percentages;
            sitePodium.buildingIndices = state.buildingIndices;
        }

        private void OnBriefLoaded()
        {
            StartCoroutine(SetGeometryState(load.geometryState));
        }

        private IEnumerator SetGeometryState(GeometryState geometryState)
        {
            if (siteBasement != null)
            {
                for (int i = 0; i < siteBasement.buildingIndices.Length; i++)
                {
                    proceduralBuildings[siteBasement.buildingIndices[i]].siteBasement = siteBasement;
                }
            }
            if (sitePodium != null)
            {
                for (int i = 0; i < sitePodium.buildingIndices.Length; i++)
                {
                    proceduralBuildings[sitePodium.buildingIndices[i]].sitePodium = sitePodium;
                }
            }

            var builds = geometryState.states.GroupBy(x => x.buildingIndex).ToList();
            LoadingBar.TaggedObject.SetValue("Generating Geometries", 0.45f);
            for (int i = 0; i < builds.Count; i++)
            {
                foreach (var temp_State in builds[i])
                {
                    if (!temp_State.isBasement && !temp_State.isPodium)
                    {
                        currentBuildingIndex = temp_State.buildingIndex;

                        if (temp_State.position != null)
                        {
                            proceduralBuildings[temp_State.buildingIndex].transform.position = new Vector3(temp_State.position[0], 0, temp_State.position[2]);
                            proceduralBuildings[temp_State.buildingIndex].transform.eulerAngles = new Vector3(temp_State.euler[0], temp_State.euler[1], temp_State.euler[2]);
                        }

                        offsetDistance = temp_State.depth;
                        corridorWidth = temp_State.corridorWidth;
                        proceduralBuildings[temp_State.buildingIndex].prevMinimumWidth = temp_State.minApartmentWidth;
                        proceduralBuildings[temp_State.buildingIndex].SetCurrentFloor(temp_State.floorIndex);

                        if (!temp_State.isTower && !temp_State.isMansion)
                        {
                            if (temp_State.isCopy)
                            {
                                LoadGeometries(temp_State.leftAptDepth, temp_State.rightAptDepth, temp_State, currentMasterFloor);
                            }
                            else if (temp_State.isCustom)
                            {
                                polygonDrawer.temp_Y = proceduralBuildings[currentBuildingIndex].buildingElement.GetYForLevel(temp_State.floorIndex);//designInputTabs.GetCurrentYValue();
                                polygonDrawer.LoadPolygonSegment(temp_State.customParams, temp_State.centreLine, proceduralBuildings[temp_State.buildingIndex].masterPolyline, currentCoresParameters, currentFloor.coreLength * 0.5f, currentFloor.corridorWidth);
                                LoadGeometries(temp_State.leftAptDepth, temp_State.rightAptDepth, temp_State, currentMasterFloor);
                            }
                            else
                            {
                                polygonDrawer.LoadPolygon(temp_State.centreLine, temp_State.closed);
                                //proceduralBuildings[temp_State.buildingIndex].baseHeight = temp_State.centreLine[1];
                                LoadGeometries(temp_State.leftAptDepth, temp_State.rightAptDepth, temp_State, null);
                            }
                        }
                        else if (temp_State.isTower)
                        {
                            LoadGeometries(offsetDistance, temp_State, null);
                        }
                        else if (temp_State.isMansion)
                        {
                            LoadGeometries(offsetDistance, temp_State, null);
                        }
                    }
                    else if (temp_State.isPodium)
                    {
                        LoadPodiumGeometries(temp_State.leftAptDepth, temp_State.rightAptDepth, temp_State, currentMasterFloor);
                    }
                    else if (temp_State.isBasement)
                    {
                        LoadBasementGeometries(temp_State.leftAptDepth, temp_State.rightAptDepth, temp_State, currentMasterFloor);
                    }
                    if (i == builds.Count - 1)
                    {
                        LoadingBar.TaggedObject.SetValue("Almost there", 0.8f);
                    }
                    yield return waitFrame;
                }
            }

            //for (int i = 0; i < proceduralBuildings.Count; i++)
            //{
            //    proceduralBuildings[i].floors[0].GeneratePlantRoom();
            //}

            ChangePreviewMode(previewMode);
            RefreshAll();
            CheckSystemConstraints();
            LoadingBar.TaggedObject.Enable(false);
        }

        private GeometryState SerializeStates()
        {
            GeometryState geometryState = new GeometryState();

            for (int i = 0; i < proceduralBuildings.Count; i++)
            {
                for (int j = 0; j < proceduralBuildings[i].floors.Count; j++)
                {
                    geometryState.states.Add(proceduralBuildings[i].floors[j].GetLayoutState());
                }
                if (proceduralBuildings[i].podium != null)
                {
                    geometryState.states.Add(proceduralBuildings[i].podium.GetLayoutState());
                }
                if (proceduralBuildings[i].basement != null)
                {
                    geometryState.states.Add(proceduralBuildings[i].basement.GetLayoutState());
                }
            }

            return geometryState;
        }

        private StandardsState SerializeStandards()
        {
            StandardsState standards = new StandardsState();

            standards.ApartmentTypeRooms = Standards.TaggedObject.ApartmentTypeRooms;
            standards.ApartmentTypeRoomsM3 = Standards.TaggedObject.ApartmentTypeRoomsM3;
            standards.ApartmentTypeRoomsMansion = Standards.TaggedObject.ApartmentTypeRoomsMansion;
            standards.ApartmentTypeRoomsMansionM3 = Standards.TaggedObject.ApartmentTypeRoomsMansionM3;
            standards.ApartmentTypesMaximumSizes = Standards.TaggedObject.ApartmentTypesMaximumSizes;
            standards.ApartmentTypesMinimumSizes = Standards.TaggedObject.apartmentTypesMinimumSizes;
            standards.DesiredAreas = Standards.TaggedObject.DesiredAreas;
            standards.DesiredAreasM3 = Standards.TaggedObject.DesiredAreasM3;
            standards.AmenitiesAreas = Standards.TaggedObject.amenitiesAreas;
            standards.customVals = Standards.TaggedObject.customVals;
            standards.customValsM3 = Standards.TaggedObject.customValsM3;
            standards.ValidLineLoads = Standards.TaggedObject.ValidLineLoads;
            standards.TotalAmenitiesArea = Standards.TaggedObject.TotalAmenitiesArea;
            standards.ConstructionFeatures = Standards.TaggedObject.ConstructionFeatures;
            standards.ConstructionFeaturesTitle = Standards.TaggedObject.ConstructionFeaturesTitle;
            standards.Balconies = (int)Standards.TaggedObject.balconies;
            standards.WindowsPositions = Standards.TaggedObject.WindowsPositions;
            standards.WindowsPositionsM3 = Standards.TaggedObject.WindowsPositionsM3;
            standards.WindowsPositionsMansion = Standards.TaggedObject.WindowsPositionsMansion;
            standards.WindowsPositionsMansionM3 = Standards.TaggedObject.WindowsPositionsMansionM3;
            standards.roofDepth = Standards.TaggedObject.roofDepth;
            standards.parapetHeight = Standards.TaggedObject.parapetHeight;

            return standards;
        }

        private SiteState SerializeSite()
        {
            if (mapHandler.hasMapLoaded)
            {
                SiteState siteState = new SiteState(mapHandler.currentLat, mapHandler.currentLon, mapHandler.hasDeleted);

                if (sitePolygon != null)
                {
                    List<float> verts = new List<float>();
                    for (int i = 0; i < sitePolygon.Count; i++)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            verts.Add(sitePolygon[i].currentPosition[j]);
                        }
                    }
                    siteState.vertices = verts.ToArray();
                }

                if (siteBasement != null)
                {
                    List<int> buildingIndices = new List<int>();
                    for (int i = 0; i < sitePodium.buildings.Count; i++)
                    {
                        buildingIndices.Add(proceduralBuildings.IndexOf(sitePodium.buildings[i]));
                    }
                    siteState.basements = new ExtraMassingState()
                    {
                        isCustom = siteBasementType == ExtraMassingType.Custom,
                        offset = siteBasementOffset,
                        floors = siteBasement.floors,
                        f2f = siteBasement.floor2Floor,
                        percentages = siteBasement.use,
                        buildingIndices = buildingIndices.ToArray()
                    };
                    if (siteState.basements.isCustom)
                    {
                        siteState.basements.vertices = new float[siteBasement.polygon.Count * 3];
                        for (int j = 0; j < siteBasement.polygon.Count; j++)
                        {
                            siteState.basements.vertices[j * 3] = siteBasement.polygon[j].currentPosition.x;
                            siteState.basements.vertices[j * 3 + 1] = siteBasement.polygon[j].currentPosition.y;
                            siteState.basements.vertices[j * 3 + 2] = siteBasement.polygon[j].currentPosition.z;
                        }
                    }
                }

                if (sitePodium != null)
                {
                    List<int> buildingIndices = new List<int>();
                    for (int i = 0; i < sitePodium.buildings.Count; i++)
                    {
                        buildingIndices.Add(proceduralBuildings.IndexOf(sitePodium.buildings[i]));
                    }
                    siteState.podiums = new ExtraMassingState()
                    {
                        isCustom = sitePodiumType == ExtraMassingType.Custom,
                        offset = sitePodiumOffset,
                        floors = sitePodium.floors,
                        f2f = sitePodium.floor2Floor,
                        percentages = sitePodium.use,
                        buildingIndices = buildingIndices.ToArray()
                    };
                    if (siteState.podiums.isCustom)
                    {
                        siteState.podiums.vertices = new float[sitePodium.polygon.Count * 3];
                        for (int j = 0; j < sitePodium.polygon.Count; j++)
                        {
                            siteState.podiums.vertices[j * 3] = sitePodium.polygon[j].currentPosition.x;
                            siteState.podiums.vertices[j * 3 + 1] = sitePodium.polygon[j].currentPosition.y;
                            siteState.podiums.vertices[j * 3 + 2] = sitePodium.polygon[j].currentPosition.z;
                        }
                    }
                }

                return siteState;
            }
            else
            {
                return null;
            }
        }



        private IEnumerator ResolveResidualsIteratively()
        {
            if (previewMode == PreviewMode.Floors)
            {
                for (int i = 0; i < currentFloor.apartments.Count; i++)
                {
                    var apt = currentFloor.apartments[i];
                    if (apt.ApartmentType == "SpecialApt" && apt.IsCorner == false)
                    {
                        yield return StartCoroutine(apt.AutoResolveWithAll());
                    }
                }
                yield return waitFrame;
                currentFloor.OnApartmentsChanged();
                currentFloor.UpdateUnresolvables();
            }
            else if (previewMode == PreviewMode.Buildings)
            {
                for (int i = 1; i < proceduralBuildings[currentBuildingIndex].floors.Count; i++)
                {
                    var floor = proceduralBuildings[currentBuildingIndex].floors[i];
                    for (int j = 0; j < floor.apartments.Count; j++)
                    {
                        var apt = floor.apartments[j];
                        if (apt.ApartmentType == "SpecialApt" && apt.IsCorner == false)
                        {
                            yield return StartCoroutine(apt.AutoResolveWithAll());
                        }
                    }
                    yield return waitFrame;
                    floor.OnApartmentsChanged();
                    floor.UpdateUnresolvables();
                }
            }
            CheckSystemConstraints();
        }

        private IEnumerator ResolveAndRetainIteratively()
        {
            if (previewMode == PreviewMode.Floors)
            {
                for (int i = 0; i < currentFloor.apartments.Count; i++)
                {
                    var apt = currentFloor.apartments[i];
                    if (apt.ApartmentType == "SpecialApt" && apt.IsCorner == false)
                    {
                        yield return StartCoroutine(apt.AutoResolveAndRetain());
                    }
                }
                currentFloor.OnApartmentsChanged();
                currentFloor.UpdateUnresolvables();
            }
            else if (previewMode == PreviewMode.Buildings)
            {
                for (int i = 1; i < proceduralBuildings[currentBuildingIndex].floors.Count; i++)
                {
                    var floor = proceduralBuildings[currentBuildingIndex].floors[i];
                    for (int j = 0; j < floor.apartments.Count; j++)
                    {
                        var apt = floor.apartments[j];
                        if (apt.ApartmentType == "SpecialApt" && apt.IsCorner == false)
                        {
                            yield return StartCoroutine(apt.AutoResolveAndRetain());
                        }
                    }
                    floor.OnApartmentsChanged();
                    floor.UpdateUnresolvables();
                }
            }
            CheckSystemConstraints();
        }

        private void OnUndo(UndoAction lastAction)
        {
            var undoComb = lastAction as ResolveAction;
            if (undoComb != null)
            {
                StartCoroutine(UndoCombination(undoComb));
            }
            else
            {
                var swapAction = lastAction as SwapAction;
                UndoSwap(swapAction);
            }
        }

        private void UndoSwap(SwapAction lastAction)
        {
            ApartmentUnity apt0 = null;
            ApartmentUnity apt1 = null;

            for (int i = 0; i < lastAction.floor.apartments.Count; i++)
            {
                if (lastAction.floor.apartments[i].name == lastAction.aptNames[0])
                {
                    apt0 = lastAction.floor.apartments[i];
                }
                if (lastAction.floor.apartments[i].name == lastAction.aptNames[1])
                {
                    apt1 = lastAction.floor.apartments[i];
                }
            }

            if (apt0 != null)
            {
                apt0.Move(lastAction.prevPos[0]);
            }

            if (apt1 != null)
            {
                apt1.Move(lastAction.prevPos[1]);
            }

            if (clearSelect != null)
            {
                clearSelect();
            }

            StartCoroutine(OnApartmentsChanged());
        }

        private IEnumerator UndoCombination(ResolveAction lastAction)
        {
            for (int i = 0; i < lastAction.resultingApts.Count; i++)
            {
                if (lastAction.resultingApts[i] == null)
                {
                    for (int j = 0; j < lastAction.floor.aptParent.transform.childCount; j++)
                    {
                        if (lastAction.floor.aptParent.transform.GetChild(j).gameObject.name == lastAction.aptNames[0])
                        {
                            lastAction.resultingApts[i] = lastAction.floor.aptParent.transform.GetChild(j).gameObject.GetComponent<ApartmentUnity>();
                        }
                    }
                }
            }

            if (lastAction.aptVertexIndex != null)
            {
                List<KeyValuePair<int, Vector3>>[] newPos = new List<KeyValuePair<int, Vector3>>[lastAction.aptVertexIndex.Length];
                for (int i = 0; i < lastAction.aptVertexIndex.Length; i++)
                {
                    newPos[i] = new List<KeyValuePair<int, Vector3>>();
                    for (int j = 0; j < lastAction.aptVertexIndex[i].Count; j++)
                    {
                        var item = lastAction.aptVertexIndex[i][j];
                        newPos[i].Add(new KeyValuePair<int, Vector3>(item.Key, lastAction.resultingApts[0].editableMesh.vertices[item.Value].currentPosition));
                    }
                }

                for (int i = 0; i < newPos.Length; i++)
                {
                    for (int j = 0; j < newPos[i].Count; j++)
                    {
                        lastAction.aptVertices[i][newPos[i][j].Key] = newPos[i][j].Value;
                    }
                }
            }

            for (int i = 0; i < lastAction.resultingApts.Count; i++)
            {
                lastAction.resultingApts[i].DestroyApartment(true);
            }

            yield return waitFrame;

            for (int i = 0; i < lastAction.aptNames.Length; i++)
            {
                if (lastAction.aptVertices[i].Length > 3)
                {
                    currentFloor.CreateApartment(lastAction.aptTypes[i], lastAction.aptVertices[i], lastAction.aptIndex[i], lastAction.aptCorridorIndex[i], lastAction.aptHasCorridor[i], lastAction.aptNames[i], null, lastAction.flipped[i]);
                }
                else
                {
                    currentFloor.CreateCornerApartment(lastAction.aptTypes[i], lastAction.aptVertices[i], lastAction.aptIndex[i], lastAction.isExteriorCorner[i]);
                }
            }

            yield return waitFrame;

            currentFloor.ReEvaluateLayoutNeighbours();
            StartCoroutine(OnApartmentsChanged());
        }

        private IEnumerator OnApartmentsChanged()
        {
            yield return waitFrame;
            if (apartmentsChanged != null)
            {
                apartmentsChanged();
            }
            Selector.TaggedObject.ClearSelect();
            proceduralBuildings[currentBuildingIndex].currentFloor.UpdateUnresolvables();
            OnPreviewModeChanged(previewMode);
        }

        private void DeselectApartments()
        {
            aptToSwap = new List<ApartmentUnity>();
            swapApartments.interactable = false;
            combineApartments.interactable = false;
        }



        private IEnumerator ExportModel()
        {
            LoadingBar.TaggedObject.Enable(true);
            LoadingBar.TaggedObject.SetValue("Getting Geometry", 0.0f);
            PreviewMode prevMode = previewMode;
            ChangePreviewMode(PreviewMode.Floors);

            string path = Path.Combine(Application.streamingAssetsPath, "model.obj");
            List<GameObject> objectsToExport = new List<GameObject>(100);
            List<GameObject> temp_Instances = new List<GameObject>(100);
            for (int k = 0; k < proceduralBuildings.Count; k++)
            {
                for (int m = 0; m < proceduralBuildings[k].floors.Count; m++)
                {

                    var tower = proceduralBuildings[k].floors[m] as ProceduralTower;
                    if (tower == null)
                    {
                        //Exporting cores
                        for (int i = 0; i < proceduralBuildings[k].floors[m].coresParent.transform.childCount; i++)
                        {
                            objectsToExport.Add(proceduralBuildings[k].floors[m].coresParent.transform.GetChild(i).gameObject);
                            List<GameObject> instances = new List<GameObject>();
                            for (int j = 1; j < proceduralBuildings[k].floors[m].levels.Length; j++)
                            {
                                GameObject inst = Instantiate(objectsToExport.Last());
                                inst.transform.position = objectsToExport.Last().transform.position;
                                inst.transform.position += new Vector3(0, proceduralBuildings[k].floors[m].floor2floor * (proceduralBuildings[k].floors[m].levels[j] - proceduralBuildings[k].floors[m].levels[0]), 0);
                                inst.transform.eulerAngles = objectsToExport.Last().transform.eulerAngles;
                                instances.Add(inst);
                            }
                            objectsToExport.AddRange(instances);
                            temp_Instances.AddRange(instances);
                        }


                        yield return null;
                        //Exporting other geometry
                        if (proceduralBuildings[k].floors[m].commercialPolygons == null)
                        {
                            for (int i = 0; i < proceduralBuildings[k].floors[m].apartments.Count; i++)
                            {
                                objectsToExport.Add(proceduralBuildings[k].floors[m].apartments[i].gameObject);
                                List<GameObject> instances = new List<GameObject>();
                                for (int j = 1; j < proceduralBuildings[k].floors[m].levels.Length; j++)
                                {
                                    GameObject inst = Instantiate(objectsToExport.Last());
                                    inst.transform.position = objectsToExport.Last().transform.position + new Vector3(0, proceduralBuildings[k].floors[m].floor2floor * (proceduralBuildings[k].floors[m].levels[j] - proceduralBuildings[k].floors[m].levels[0]), 0);
                                    inst.transform.eulerAngles = objectsToExport.Last().transform.eulerAngles;
                                    instances.Add(inst);
                                }
                                objectsToExport.AddRange(instances);
                                temp_Instances.AddRange(instances);
                                if (i % 5 == 0)
                                {
                                    yield return null;
                                }
                            }

                            try
                            {
                                for (int i = 0; i < proceduralBuildings[k].floors[m].corridors.transform.childCount; i++)
                                {
                                    objectsToExport.Add(proceduralBuildings[k].floors[m].corridors.transform.GetChild(i).gameObject);
                                    List<GameObject> instances = new List<GameObject>();
                                    for (int j = 1; j < proceduralBuildings[k].floors[m].levels.Length; j++)
                                    {
                                        GameObject inst = Instantiate(objectsToExport.Last());
                                        inst.transform.position = objectsToExport.Last().transform.position + new Vector3(0, proceduralBuildings[k].floors[m].floor2floor * (proceduralBuildings[k].floors[m].levels[j] - proceduralBuildings[k].floors[m].levels[0]), 0);
                                        inst.transform.eulerAngles = objectsToExport.Last().transform.eulerAngles;
                                        instances.Add(inst);
                                    }
                                    objectsToExport.AddRange(instances);
                                    temp_Instances.AddRange(instances);
                                }
                            }
                            catch (Exception e)
                            {
                                Debug.Log(e);
                            }
                        }
                        else
                        {
                            for (int i = 0; i < proceduralBuildings[k].floors[m].commercialPolygons.Count; i++)
                            {
                                objectsToExport.Add(proceduralBuildings[k].floors[m].commercialPolygons[i].gameObject);
                            }
                        }
                    }
                    else
                    {
                        //Exporting cores
                        for (int i = 0; i < proceduralBuildings[k].floors[m].coresParent.transform.childCount; i++)
                        {
                            objectsToExport.Add(proceduralBuildings[k].floors[m].coresParent.transform.GetChild(i).gameObject);
                            List<GameObject> instances = new List<GameObject>();
                            for (int j = 1; j < proceduralBuildings[k].floors[m].levels.Length; j++)
                            {
                                GameObject inst = Instantiate(objectsToExport.Last());
                                inst.transform.position = objectsToExport.Last().transform.position;
                                inst.transform.position += new Vector3(0, proceduralBuildings[k].floors[m].floor2floor * (proceduralBuildings[k].floors[m].levels[j] - proceduralBuildings[k].floors[m].levels[0]), 0);
                                inst.transform.eulerAngles = objectsToExport.Last().transform.eulerAngles;
                                instances.Add(inst);
                            }
                            objectsToExport.AddRange(instances);
                            temp_Instances.AddRange(instances);
                        }

                        for (int i = 0; i < proceduralBuildings[k].floors[m].apartments.Count; i++)
                        {
                            objectsToExport.Add(proceduralBuildings[k].floors[m].apartments[i].gameObject);
                            List<GameObject> instances = new List<GameObject>();
                            for (int j = 1; j < proceduralBuildings[k].floors[m].levels.Length; j++)
                            {
                                GameObject inst = Instantiate(objectsToExport.Last());
                                inst.transform.position = objectsToExport.Last().transform.position + new Vector3(0, proceduralBuildings[k].floors[m].floor2floor * (proceduralBuildings[k].floors[m].levels[j] - proceduralBuildings[k].floors[m].levels[0]), 0);
                                inst.transform.eulerAngles = objectsToExport.Last().transform.eulerAngles;
                                instances.Add(inst);
                            }
                            objectsToExport.AddRange(instances);
                            temp_Instances.AddRange(instances);
                            if (i % 5 == 0)
                            {
                                yield return null;
                            }
                        }
                    }
                }
            }
            LoadingBar.TaggedObject.SetValue("Getting Interiors", 0.3f);
            ChangePreviewMode(PreviewMode.Buildings);

            if (!populateApartments)
            {
                yield return StartCoroutine(PopulateForExport());
            }
            else
            {
                yield return waitFrame;
            }

            for (int i = 0; i < repRootParent.childCount; i++)
            {
                for (int j = 0; j < repRootParent.GetChild(i).childCount; j++)
                {
                    for (int k = 0; k < repRootParent.GetChild(i).GetChild(j).childCount; k++)
                    {
                        objectsToExport.Add(repRootParent.GetChild(i).GetChild(j).GetChild(k).GetChild(repRootParent.GetChild(i).GetChild(j).GetChild(k).childCount - 1).gameObject);
                    }
                }
            }

            ChangePreviewMode(PreviewMode.Floors);
            for (int i = 0; i < proceduralBuildings.Count; i++)
            {
                for (int j = 0; j < proceduralBuildings[i].floors.Count; j++)
                {
                    proceduralBuildings[i].floors[j].exteriorPolygon.SetExtrusionHeight(proceduralBuildings[i].floors[j].floor2floor * proceduralBuildings[i].floors[j].levels.Length);
                    objectsToExport.Add(proceduralBuildings[i].floors[j].exteriorPolygon.gameObject);
                }
            }
            LoadingBar.TaggedObject.SetValue("Writting Everything", 0.6f);

            yield return null;
            string data = BW_ObjExporter.ExportMeshes(objectsToExport);
            resourcesLoader.OnExportModel(data, "ModelExport_" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss"));

            for (int i = 0; i < proceduralBuildings.Count; i++)
            {
                for (int j = 0; j < proceduralBuildings[i].floors.Count; j++)
                {
                    proceduralBuildings[i].floors[j].exteriorPolygon.SetExtrusionHeight(proceduralBuildings[i].floors[j].floor2floor);
                }
            }
            LoadingBar.TaggedObject.Enable(false);
            for (int i = 0; i < temp_Instances.Count; i++)
            {
                Destroy(temp_Instances[i]);
            }
            OnPreviewModeChanged(prevMode);
        }

        private void AddEvents()
        {
            DisplayControls.layoutChanged += OnApartmentDropdownChanged;
            DisplayControls.buildingTypeLayoutChanged += OnBuildingLayoutChanged;
            DisplayControls.m3ToggleChanged += OnM3ToggleChanged;
            Selector.selected2 += SetSwapApartments;
            Selector.selected1 += SetSelectedApartment;
            Selector.deselect += DeselectApartments;
            ResourcesLoader.dataLoaded += OnSaveDataLoaded;
            PolygonDrawer.polygonEnded += OnPolygonFinished;
            PolygonDrawer.polygonLoaded += OnPolygonLoaded;
            PolygonDrawer.polygonSegmentEnded += AddCustomFloorPolygon;
            DisplayControls.previewChanged += OnPreviewModeChanged;
            //designInputTabs.floorSet += SetFloor;
            designInputTabs.briefLoaded += OnBriefLoaded;
            UndoActionLog.undo += OnUndo;
        }

        private IEnumerator ExportRawData()
        {
            var prevMode = previewMode;
            OnPreviewModeChanged(PreviewMode.Buildings);
            yield return waitFrame;
            string[] scheduleLines = new string[2];
            List<string> perFloorLines = new List<string>();
            perFloorLines.Add("BuildingIndex,BuildingName,FloorLayoutIndex," + ProceduralFloor.infoLabels);
            scheduleLines[0] = string.Format("Plot Area,Ground Floor GEA,Plot Coverage,FAR,Density(apts),Density(habitables),Total GEA,Total GIA,Total NIA");
            float[,] buildingValues = new float[designInputTabs.buildings.Count, 4];

            float totalGEA = 0;
            float totalGIA = 0;
            float totalNIA = 0;
            float floorGEA = 0;

            for (int i = 0; i < proceduralBuildings.Count; i++)
            {
                for (int j = 0; j < proceduralBuildings[i].floors.Count; j++)
                {
                    var NIA = proceduralBuildings[i].floors[j].NIA;
                    var GEA = proceduralBuildings[i].floors[j].GEA;
                    var GIA = proceduralBuildings[i].floors[j].GIA;

                    totalGEA += GEA;
                    totalNIA += NIA;
                    totalGIA += GIA;

                    if (j == 0)
                    {
                        floorGEA += GEA;
                    }
                }

                perFloorLines.AddRange(proceduralBuildings[i].GetPerFloorRawData());
            }

            scheduleLines[1] = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8}", (sitePolygon == null) ? "N/A" : sitePolygon.Area.ToString(),
                floorGEA,
                (sitePolygon == null) ? "N/A" : Math.Round(((floorGEA / sitePolygon.Area) * 100), 2).ToString(),
                (sitePolygon == null) ? "N/A" : Math.Round(((totalGEA / sitePolygon.Area) * 100), 2).ToString(),
                (sitePolygon == null) ? "N/A" : "0",
                (sitePolygon == null) ? "N/A" : "0",
                Math.Round(totalGEA),
                Math.Round(totalGIA),
                Math.Round(totalNIA)
                );

            yield return StartCoroutine(PopulateInterior(true));
            yield return waitFrame;
            var lines = GetModuleDetails();
            var aptLines = GetApartmentDetails();
            var coreLines = GetCoreDetails();
            var floorOutline = GetFloorOutlineDetails();
            resourcesLoader.OnExportModulesData(string.Join("\r\n", lines), "ModulesDataExport_" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss"));
            yield return StartCoroutine(PopulateInterior(false));
            yield return waitFrame;
            resourcesLoader.OnExportApartmentsData(string.Join("\r\n", aptLines), "ApartmentsDataExport_" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss"));
            yield return waitFrame;
            resourcesLoader.OnExportCoresData(string.Join("\r\n", coreLines), "CoresDataExport_" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss"));
            yield return waitFrame;
            resourcesLoader.OnExportFloorOutlineData(string.Join("\r\n", floorOutline), "FloorOutlinesDataExport_" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss"));
            yield return waitFrame;
            resourcesLoader.OnExportBuildingData(string.Join("\r\n", scheduleLines), "BuildingDataExport_" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss"));
            yield return waitFrame;
            resourcesLoader.OnExportFloorData(string.Join("\r\n", perFloorLines.ToArray()), "PerFloorDataExport_" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss"));
            OnPreviewModeChanged(prevMode);
        }

        private string[] GetModuleDetails()
        {
            List<string> lines = new List<string>();
            lines.Add("ModuleID,ModuleWidth,ModuleDepth,ModuleHeight,ApartmentID,ApartmentType,posX,posY,posZ,rotX,rotY,rotZ");
            for (int i = 0; i < proceduralBuildings.Count; i++)
            {
                lines.AddRange(proceduralBuildings[i].GetModuleDetails());
            }
            return lines.ToArray();
        }

        private string[] GetApartmentDetails()
        {
            List<string> lines = new List<string>();
            lines.Add("FloorLevel,ApartmentType,OutlineCoordinates");
            for (int i = 0; i < proceduralBuildings.Count; i++)
            {
                lines.AddRange(proceduralBuildings[i].GetApartmentDetails());
            }
            return lines.ToArray();
        }

        private string[] GetCoreDetails()
        {
            List<string> lines = new List<string>();
            lines.Add("FloorLevel,Position,Rotation,Scale");
            for (int i = 0; i < proceduralBuildings.Count; i++)
            {
                lines.AddRange(proceduralBuildings[i].GetCoresDetails());
            }
            return lines.ToArray();
        }

        private string[] GetFloorOutlineDetails()
        {
            List<string> lines = new List<string>();
            lines.Add("FloorLevel,OutlineCoordinates");
            for (int i = 0; i < proceduralBuildings.Count; i++)
            {
                lines.AddRange(proceduralBuildings[i].GetFloorOutlineDetails());
            }
            return lines.ToArray();
        }

        private Dictionary<string, int> GetModuleSchedule()
        {
            Dictionary<string, int> modSched = new Dictionary<string, int>();

            for (int i = 0; i < proceduralBuildings.Count; i++)
            {
                var _modeSched = proceduralBuildings[i].GetModuleSchedule();
                foreach (var item in _modeSched)
                {
                    if (modSched.ContainsKey(item.Key))
                    {
                        modSched[item.Key] += item.Value;
                    }
                    else
                    {
                        modSched.Add(item.Key, item.Value);
                    }
                }
            }

            return modSched;
        }

        private Dictionary<string, int> GetPanelSchedule()
        {
            Dictionary<string, int> panSched = new Dictionary<string, int>();

            for (int i = 0; i < proceduralBuildings.Count; i++)
            {
                var _panSched = proceduralBuildings[i].GetPanelSchedule();
                foreach (var item in _panSched)
                {
                    if (panSched.ContainsKey(item.Key))
                    {
                        panSched[item.Key] += item.Value;
                    }
                    else
                    {
                        panSched.Add(item.Key, item.Value);
                    }
                }
            }

            return panSched;
        }

        private void ExportPdfReport()
        {
            StartCoroutine(ExportPdfCoroutine());
        }

        private void CheckBuildingConstraintsForPlatforms(int storiesNum, float span)
        {
            if (storiesNum >= 10 && storiesNum <= 14)
            {
                platformsTrafficLight.constructionSystems[0].partyWall.greenRange = new string[] { "0.31-100" };
                platformsTrafficLight.constructionSystems[0].partyWall.yellowRange = new string[0];
                platformsTrafficLight.constructionSystems[0].partyWall.RedRange = new string[] { "0-0.309" };

                platformsTrafficLight.constructionSystems[0].exteriorWall.greenRange = new string[] { "0.51-100" };
                platformsTrafficLight.constructionSystems[0].exteriorWall.yellowRange = new string[0];
                platformsTrafficLight.constructionSystems[0].exteriorWall.RedRange = new string[] { "0-0.509" };
            }
            else
            {
                platformsTrafficLight.constructionSystems[0].partyWall.greenRange = new string[] { "0.26-0.31" };
                platformsTrafficLight.constructionSystems[0].partyWall.yellowRange = new string[] { "0.311-100" };
                platformsTrafficLight.constructionSystems[0].partyWall.RedRange = new string[] { "0-0.259" };

                platformsTrafficLight.constructionSystems[0].exteriorWall.greenRange = new string[] { "0.46-100" };
                platformsTrafficLight.constructionSystems[0].exteriorWall.yellowRange = new string[0];
                platformsTrafficLight.constructionSystems[0].exteriorWall.RedRange = new string[] { "0-0.459" };
            }

            if (span > 7.5f && span < 8.5f)
            {
                float bayLimit = BrydenWoodUtils.Remap(span, 7.5f, 8.5f, 4.05f, 3.6f);
                platformsTrafficLight.constructionSystems[0].moduleWidth.greenRange = new string[] { "0-" + bayLimit.ToString() };
                platformsTrafficLight.constructionSystems[0].moduleWidth.RedRange = new string[] { (bayLimit + 0.001f).ToString() + "-100" };
            }
            else
            {
                platformsTrafficLight.constructionSystems[0].moduleWidth.greenRange = new string[] { "0-4.05" };
                platformsTrafficLight.constructionSystems[0].moduleWidth.RedRange = new string[] { "4.051-100" };
            }
        }

        private IEnumerator ExportPdfCoroutine()
        {
            float GEA = 0.0f;
            float GIA = 0.0f;
            float NIA = 0.0f;
            float maxHeight = 0.0f;
            float unitDensity = 0.0f;
            float habDensity = 0.0f;
            Dictionary<string, float> aptAreas = new Dictionary<string, float>();
            Dictionary<string, float> aptNumbers = new Dictionary<string, float>();
            Dictionary<string, float[]> aptStats = new Dictionary<string, float[]>();
            float plantRoomArea = 0;
            string ptalZone = "0";
            float siteArea = 0.0f;
            float groundFloorArea = 0.0f;
            float receptionArea = 0;
            float commercialArea = 0;
            float otherArea = 0;

            ReportData.TaggedObject.buildings = new List<BuildingReportData>();

            for (int i = 0; i < proceduralBuildings.Count; i++)
            {
                float m_GEA = 0.0f;
                float m_GIA = 0.0f;
                float m_NIA = 0.0f;
                float m_totalHeight = 0.0f;
                float m_unitDensity = 0.0f;
                float m_habDensity = 0.0f;
                float m_groundFloorArea = 0.0f;
                Dictionary<string, float> m_aptAreas = new Dictionary<string, float>();
                Dictionary<string, float> m_aptNumbers = new Dictionary<string, float>();
                float m_plantRoomArea = 0;
                float m_receptionArea = 0;
                float m_commercialArea = 0;
                float m_otherArea = 0;
                float m_facadeArea = 0;

                proceduralBuildings[i].GetCurrentBuildingStats(out m_GEA, out m_GIA, out m_NIA, out m_totalHeight, out m_unitDensity, out m_habDensity, out m_plantRoomArea, out m_aptAreas, out m_aptNumbers, out m_groundFloorArea, out m_receptionArea, out m_commercialArea, out m_otherArea, out m_facadeArea);

                GEA += m_GEA;
                GIA += m_GIA;
                NIA += m_NIA;
                groundFloorArea += m_groundFloorArea;
                if (m_totalHeight > maxHeight)
                {
                    maxHeight = m_totalHeight;
                }
                unitDensity += m_unitDensity;
                habDensity += m_habDensity;
                plantRoomArea += m_plantRoomArea;
                receptionArea += m_receptionArea;
                commercialArea += m_commercialArea;
                otherArea += m_otherArea;

                foreach (var item in m_aptAreas)
                {
                    if (aptAreas.ContainsKey(item.Key))
                    {
                        aptAreas[item.Key] += item.Value;
                    }
                    else
                    {
                        aptAreas.Add(item.Key, item.Value);
                    }
                }

                foreach (var item in m_aptNumbers)
                {
                    if (aptNumbers.ContainsKey(item.Key))
                    {
                        aptNumbers[item.Key] += item.Value;
                    }
                    else
                    {
                        aptNumbers.Add(item.Key, item.Value);
                    }
                }

                BuildingReportData brd = new BuildingReportData();
                var m_aptStats = proceduralBuildings[i].GetApartmentStats();
                foreach (var item in m_aptStats)
                {
                    if (aptStats.ContainsKey(item.Key))
                    {
                        aptStats[item.Key][0] += item.Value[0];
                        aptStats[item.Key][1] += item.Value[1];
                    }
                    else
                    {
                        aptStats.Add(item.Key, item.Value);
                    }

                    if (!brd.aptNumbers.ContainsKey(item.Key))
                    {
                        brd.aptNumbers.Add(item.Key, string.Format("{0},{1}", item.Value[0], Math.Round(item.Value[1])));
                    }
                }
                brd.buildingName = proceduralBuildings[i].buildingName;
                brd.gea = (int)Math.Round(m_GEA);
                brd.gia = (int)Math.Round(m_GIA);
                brd.nia = (int)Math.Round(m_NIA);
                brd.netgross = (int)((m_NIA / m_GIA) * 100);

                float minF2f = 0;
                float maxF2f = 0;
                float maxModule = 0;
                float maxSpan = 0;
                int storiesNum = 0;

                proceduralBuildings[i].GetSystemNumbers(ref minF2f, ref maxF2f, ref maxModule, ref maxSpan, ref storiesNum);

                volumetricTrafficLight.GetBuildingEvaluation(storiesNum, minF2f, maxF2f, maxModule, maxSpan, ref brd.constructionSystems);
                panelisedTrafficLight.GetBuildingEvaluation(storiesNum, minF2f, maxF2f, maxModule, maxSpan, ref brd.constructionSystems);
                if (platformsTrafficLight != null)
                {
                    float bay = GetMaxPlatformsBay(i);
                    float span = GetMaxAptDepth(i);
                    CheckBuildingConstraintsForPlatforms(storiesNum, span);
                    platformsTrafficLight.UpdateTrafficSystem(storiesNum, minF2f, maxF2f, bay, span);
                }
                ReportData.TaggedObject.buildings.Add(brd);
                yield return waitFrame;
            }

            if (sitePolygon != null)
            {
                siteArea = (float)Math.Round(sitePolygon.Area);
                ReportData.TaggedObject.siteArea = (int)siteArea;

                unitDensity /= (sitePolygon.Area / 10000.0f);
                habDensity /= (sitePolygon.Area / 10000.0f);
                ReportData.TaggedObject.unitDensity = (float)Math.Round(unitDensity);
                ReportData.TaggedObject.habitableDensity = (float)Math.Round(habDensity);

                Vector3 siteCentre = sitePolygon.Centre + Vector3.up * 10;
                Ray r = new Ray(siteCentre, Vector3.down);
                RaycastHit hit;
                if (Physics.Raycast(r, out hit, 5000, ptalLayerMask))
                {
                    ptalZone = hit.collider.gameObject.tag;
                }

                float maxDensity = 500.0f;
                if (Standards.TaggedObject.ptalZones.TryGetValue(ptalZone, out maxDensity))
                {
                    if (maxDensity < unitDensity)
                    {
                        if (!Notifications.TaggedObject.activeNotifications.Contains("Density"))
                        {
                            Notifications.TaggedObject.activeNotifications.Add("Density");
                        }
                    }
                    else
                    {
                        if (Notifications.TaggedObject.activeNotifications.Contains("Density"))
                        {
                            Notifications.TaggedObject.activeNotifications.Remove("Density");
                        }
                    }
                    Notifications.TaggedObject.UpdateNotifications();
                }
            }
            ReportData.TaggedObject.ConstructionFeaturesName = constructionFeaturesElement.constructionTypesDropdown.options[constructionFeaturesElement.constructionTypesDropdown.value].text;
            ReportData.TaggedObject.ConstructionFeatures = Standards.TaggedObject.ConstructionFeaturesReport;
            ReportData.TaggedObject.ptalRating = ptalZone;
            ReportData.TaggedObject.location = mapHandler.currentLat + "," + mapHandler.currentLon;
            ReportData.TaggedObject.gea = (int)Math.Round(GEA);
            ReportData.TaggedObject.gia = (int)Math.Round(GIA);
            ReportData.TaggedObject.nia = (int)Math.Round(NIA);
            ReportData.TaggedObject.netgross = (int)((NIA / GIA) * 100);
            ReportData.TaggedObject.images = new string[1];
            ReportData.TaggedObject.aptNumbers = new Dictionary<string, string>();
            ReportData.TaggedObject.modulesNumbers = new Dictionary<string, int>();
            ReportData.TaggedObject.panelsNumbers = new Dictionary<string, int>();
            ReportData.TaggedObject.modulesColors = new Dictionary<string, string>();

            yield return StartCoroutine(PopulateInterior(true));
            //For the Volumetric Modules
            foreach (var item in Standards.TaggedObject.modulesMaterials)
            {
                ReportData.TaggedObject.modulesColors.Add(item.Key, item.Value.color.r + "," + item.Value.color.g + "," + item.Value.color.b);
            }
            ReportData.TaggedObject.modulesNumbers = GetModuleSchedule();
            ReportData.TaggedObject.panelsNumbers = GetPanelSchedule();
            //For the Panels

            yield return StartCoroutine(PopulateInterior(false));

            foreach (var item in aptStats)
            {
                if (!ReportData.TaggedObject.aptNumbers.ContainsKey(item.Key))
                {
                    ReportData.TaggedObject.aptNumbers.Add(item.Key, string.Format("{0},{1}", item.Value[0], Math.Round(item.Value[1])));
                }
            }

            var width = Screen.width;
            var height = Screen.height;
            var tex = ScreenCapture.CaptureScreenshotAsTexture();
            var bytes = tex.EncodeToJPG();
            Destroy(tex);
            //ReportData.TaggedObject.images[0] = Convert.ToBase64String(bytes);
#if UNITY_EDITOR
            File.WriteAllBytes(Application.streamingAssetsPath + "/ScreenShot.jpg", bytes);
            File.WriteAllText(Application.streamingAssetsPath + "/testBuildingFlat.txt", ReportData.TaggedObject.ToString());
#elif UNITY_WEBGL
            ResourcesLoader.TaggedObject.ExportReport(ReportData.TaggedObject.ToString(), DateTime.Now.ToString() + "_Report");
#endif
        }

        private string CalculateBuildingStats()
        {
            string data = "";

            return data;
        }


        private IEnumerator UpdateInternalLayout(bool show)
        {
            if (!show)
            {
                manufacturingDropDown.SetValue(0);
            }
            //populateInteriorToggle.SetValue(show);
            for (int i = 0; i < proceduralBuildings.Count; i++)
            {
                proceduralBuildings[i].populateApartments = show;
                yield return StartCoroutine(proceduralBuildings[i].UpdateInternalLayout());
            }
        }

        private IEnumerator PopulateForExport()
        {
            for (int i = 0; i < proceduralBuildings.Count; i++)
            {
                yield return StartCoroutine(proceduralBuildings[i].PopulateForExport());
            }
        }

        private void OnPreviewModeChanged(PreviewMode previewMode)
        {
            this.previewMode = previewMode;
            for (int i = 0; i < proceduralBuildings.Count; i++)
            {
                proceduralBuildings[i].OnPreviewModeChanged(previewMode);
            }
            populateApartments = false;
            StartCoroutine(UpdateInternalLayout(false));
            switch (previewMode)
            {
                case PreviewMode.Buildings:
                    if (aptToSwap == null || aptToSwap.Count == 0)
                    {
                        selectedInfoDisplay.OnDeselect();
                    }
                    if (sitePolygon != null)
                    {
                        sitePolygon.GetComponent<MeshRenderer>().enabled = true;
                        sitePolygon.GetComponent<MeshRenderer>().receiveShadows = true;
                    }
                    apartmentViewMode = ApartmentViewMode.Top;
                    fullMap.SetActive(true);
                    amenitiesButtons.SetActive(false);
                    roomNames.SetActive(false);
                 
                    Selector.TaggedObject.ClearSelect();
                    cityObject.SetActive(true);
                    ptalObject.SetActive(true);
                    aptPreviewTitle.gameObject.SetActive(false);
                    proceduralApartment.gameObject.SetActive(false);
                    //sunPosition.enabled = true;
                    //sunPosition.GetComponent<Light>().shadows = LightShadows.Hard;
                    //sunControlToggle.interactable = true;
                    Selector.TaggedObject.enabled = false;
                    proceduralApartment.GetComponent<ApartmentDimensions>().ToggleDimensions(false);
                    proceduralApartment.transform.SetLayerToChildren(0);
                    frontViewButton.gameObject.SetActive(false);
                    backViewButton.gameObject.SetActive(false);
                    topViewButton.gameObject.SetActive(false);
                    apartmentFrontCamera.gameObject.SetActive(false);
                    apartmentFrontCamera.tag = "Untagged";
                    apartmentBackCamera.gameObject.SetActive(false);
                    apartmentBackCamera.tag = "Untagged";
                 
                    try
                    {
                        for (int i = 0; i < proceduralBuildings.Count; i++)
                        {
                            proceduralBuildings[i].gameObject.SetActive(true);
                            for (int j = 0; j < proceduralBuildings[i].floors.Count; j++)
                            {
                                proceduralBuildings[i].floors[j].SetPreviewMode(previewMode);
                                if (proceduralBuildings[i].floors[j].centreLine != null)
                                {
                                    proceduralBuildings[i].floors[j].centreLine.gameObject.SetActive(true);
                                }
                                proceduralBuildings[i].floors[j].ToggleChildren(true);
                            }
                            if (proceduralBuildings[i].basement != null)
                            {
                                proceduralBuildings[i].basement.SetPreviewMode(previewMode);
                                if (proceduralBuildings[i].basement.centreLine != null)
                                {
                                    proceduralBuildings[i].basement.centreLine.gameObject.SetActive(true);
                                }
                                proceduralBuildings[i].basement.ToggleChildren(true);
                            }
                            if (proceduralBuildings[i].podium != null)
                            {
                                proceduralBuildings[i].podium.SetPreviewMode(previewMode);
                                if (proceduralBuildings[i].podium.centreLine != null)
                                {
                                    proceduralBuildings[i].podium.centreLine.gameObject.SetActive(true);
                                }
                                proceduralBuildings[i].podium.ToggleChildren(true);
                            }
                        }
                        if (proceduralBuildings.Count != 0)
                        {
                            Camera.main.GetComponent<DrawBuildingLines>().selectedKey = new List<string>();
                            for (int i = 0; i < proceduralBuildings[currentBuildingIndex].floors.Count; i++)
                            {
                                Camera.main.GetComponent<DrawBuildingLines>().selectedKey.Add(proceduralBuildings[currentBuildingIndex].floors[i].key);
                            }
                        }
                        if (heightLines != null)
                        {
                            for (int i = 0; i < heightLines.Count; i++)
                            {
                                heightLines[i].SetActive(true);
                            }
                        }
                        StartCoroutine(OverallUpdate());
                    }
                    catch (Exception e)
                    {
                        Debug.Log(e);
                    }
                    break;
                case PreviewMode.Floors:
                    if (sitePolygon != null)
                    {
                        sitePolygon.GetComponent<MeshRenderer>().enabled = false;
                        sitePolygon.GetComponent<MeshRenderer>().receiveShadows = true;
                    }
                    apartmentViewMode = ApartmentViewMode.Top;
                    fullMap.SetActive(false);
                    
                    amenitiesButtons.SetActive(false);
                    roomNames.SetActive(false);
                    dimensionsParent.SetActive(false);
                    if (aptToSwap == null || aptToSwap.Count == 0)
                    {
                        selectedInfoDisplay.OnDeselect();
                    }
                    aptPreviewTitle.gameObject.SetActive(false);
                    cityObject.SetActive(false);
                    ptalObject.SetActive(false);
                    proceduralApartment.gameObject.SetActive(false);
                    //sunPosition.enabled = false;
                    //sunPosition.GetComponent<Light>().shadows = LightShadows.None;
                    //sunPosition.transform.eulerAngles = new Vector3(43.43f, -71.90f, 0);
                    //sunControlToggle.interactable = false;
                    //sunControlToggle.isOn = false;
                    proceduralApartment.GetComponent<ApartmentDimensions>().ToggleDimensions(false);
                    Selector.TaggedObject.enabled = true;
                    proceduralApartment.transform.SetLayerToChildren(0);
                    frontViewButton.gameObject.SetActive(false);
                    backViewButton.gameObject.SetActive(false);
                    topViewButton.gameObject.SetActive(false);
                    apartmentFrontCamera.gameObject.SetActive(false);
                    apartmentFrontCamera.tag = "Untagged";
                    apartmentBackCamera.gameObject.SetActive(false);
                    apartmentBackCamera.tag = "Untagged";
                    try
                    {
                        for (int i = 0; i < proceduralBuildings.Count; i++)
                        {
                            if (i == currentBuildingIndex)
                            {
                                proceduralBuildings[i].gameObject.SetActive(true);
                                if (proceduralBuildings[i].podium != null)
                                {
                                    if (proceduralBuildings[i].buildingType == BuildingType.Tower)
                                    {
                                        proceduralBuildings[i].podium.gameObject.SetActive(true);
                                        proceduralBuildings[i].podium.SetPreviewMode(previewMode);
                                        proceduralBuildings[i].podium.ToggleChildren(true);
                                    }
                                    else
                                    {
                                        proceduralBuildings[i].podium.gameObject.SetActive(true);
                                        proceduralBuildings[i].podium.SetPreviewMode(previewMode);
                                        proceduralBuildings[i].podium.centreLine.gameObject.SetActive(true);
                                        proceduralBuildings[i].podium.ToggleChildren(true);
                                    }
                                }

                                if (proceduralBuildings[i].basement != null)
                                {
                                    if (proceduralBuildings[i].buildingType == BuildingType.Tower)
                                    {
                                        proceduralBuildings[i].basement.gameObject.SetActive(true);
                                        proceduralBuildings[i].basement.SetPreviewMode(previewMode);
                                        proceduralBuildings[i].basement.ToggleChildren(true);
                                    }
                                    else
                                    {
                                        proceduralBuildings[i].basement.gameObject.SetActive(true);
                                        proceduralBuildings[i].basement.SetPreviewMode(previewMode);
                                        proceduralBuildings[i].basement.centreLine.gameObject.SetActive(true);
                                        proceduralBuildings[i].basement.ToggleChildren(true);
                                    }
                                }

                                for (int j = 0; j < proceduralBuildings[i].floors.Count; j++)
                                {
                                    proceduralBuildings[i].floors[j].SetPreviewMode(previewMode);
                                    if (proceduralBuildings[i].floors[j] == proceduralBuildings[i].currentFloor)
                                    {
                                        var tower = proceduralBuildings[i].floors[j] as ProceduralTower;

                                        if (proceduralBuildings[i].floors[j].centreLine != null && tower == null)
                                        {
                                            proceduralBuildings[i].floors[j].gameObject.SetActive(true);
                                            proceduralBuildings[i].floors[j].SetPreviewMode(previewMode);
                                            proceduralBuildings[i].floors[j].centreLine.gameObject.SetActive(true);
                                            proceduralBuildings[i].floors[j].ToggleChildren(true);
                                        }
                                        else if (tower != null)
                                        {
                                            tower.gameObject.SetActive(true);
                                            tower.SetPreviewMode(previewMode);
                                            tower.ToggleChildren(true);
                                        }
                                    }
                                    else
                                    {
                                        proceduralBuildings[i].floors[j].gameObject.SetActive(true);
                                        proceduralBuildings[i].floors[j].SetPreviewMode(previewMode);
                                        if (proceduralBuildings[i].floors[j].centreLine != null)
                                        {
                                            proceduralBuildings[i].floors[j].centreLine.gameObject.SetActive(false);
                                        }
                                        proceduralBuildings[i].floors[j].ToggleChildren(false);
                                    }
                                }
                            }
                            else
                            {
                                if (proceduralBuildings[i].podium != null)
                                {
                                    if (proceduralBuildings[i].buildingType == BuildingType.Tower)
                                    {
                                        proceduralBuildings[i].podium.gameObject.SetActive(true);
                                        proceduralBuildings[i].podium.SetPreviewMode(previewMode);
                                        proceduralBuildings[i].podium.ToggleChildren(false);
                                    }
                                    else
                                    {
                                        proceduralBuildings[i].podium.gameObject.SetActive(true);
                                        proceduralBuildings[i].podium.SetPreviewMode(previewMode);
                                        proceduralBuildings[i].podium.centreLine.gameObject.SetActive(false);
                                        proceduralBuildings[i].podium.ToggleChildren(false);
                                    }
                                }

                                if (proceduralBuildings[i].basement != null)
                                {
                                    if (proceduralBuildings[i].buildingType == BuildingType.Tower)
                                    {
                                        proceduralBuildings[i].basement.gameObject.SetActive(true);
                                        proceduralBuildings[i].basement.SetPreviewMode(previewMode);
                                        proceduralBuildings[i].basement.ToggleChildren(false);
                                    }
                                    else
                                    {
                                        proceduralBuildings[i].basement.gameObject.SetActive(true);
                                        proceduralBuildings[i].basement.SetPreviewMode(previewMode);
                                        proceduralBuildings[i].basement.centreLine.gameObject.SetActive(false);
                                        proceduralBuildings[i].basement.ToggleChildren(false);
                                    }
                                }

                                for (int j = 0; j < proceduralBuildings[i].floors.Count; j++)
                                {
                                    proceduralBuildings[i].floors[j].SetPreviewMode(previewMode);
                                    if (proceduralBuildings[i].floors[j].centreLine != null)
                                    {
                                        proceduralBuildings[i].floors[j].centreLine.gameObject.SetActive(false);
                                    }
                                    proceduralBuildings[i].floors[j].ToggleChildren(false);
                                }
                            }
                        }
                        if (proceduralBuildings.Count != 0)
                        {
                            Camera.main.GetComponent<DrawBuildingLines>().selectedKey = new List<string>() { proceduralBuildings[currentBuildingIndex].currentFloor.key };
                        }
                        if (heightLines != null)
                        {
                            for (int i = 0; i < heightLines.Count; i++)
                            {
                                heightLines[i].SetActive(false);
                            }
                        }
                        StartCoroutine(OverallUpdate());
                    }
                    catch (Exception e)
                    {
                        Debug.Log(e);
                    }
                    break;
                case PreviewMode.Apartments:
                    apartmentViewMode = ApartmentViewMode.Top;
                    fullMap.SetActive(false);
                   
                    amenitiesButtons.SetActive(true);
                    roomNames.SetActive(true);
                    dimensionsParent.SetActive(true);
                    aptPreviewTitle.gameObject.SetActive(true);
                    cityObject.SetActive(false);
                    ptalObject.SetActive(false);
                    //sunPosition.enabled = false;
                    //sunPosition.GetComponent<Light>().shadows = LightShadows.None;
                    //sunPosition.transform.eulerAngles = new Vector3(43.43f, -71.90f, 0);
                    proceduralApartment.gameObject.SetActive(true);
                    //sunControlToggle.interactable = false;
                    Selector.TaggedObject.enabled = false;
                    frontViewButton.gameObject.SetActive(true);
                    backViewButton.gameObject.SetActive(true);
                    topViewButton.gameObject.SetActive(false);
                    apartmentFrontCamera.gameObject.SetActive(false);
                    apartmentFrontCamera.tag = "Untagged";
                    apartmentBackCamera.gameObject.SetActive(false);
                    apartmentBackCamera.tag = "Untagged";
                    try
                    {
                        if (offsetDistance != 0)
                        {
                            proceduralApartment.height = offsetDistance;
                        }
                        else
                        {
                            proceduralApartment.height = 6.8f;
                        }

                        if (aptToSwap != null && aptToSwap.Count > 0 && aptToSwap[0] != null && aptToSwap[0].ApartmentType != "SpecialApt" && aptToSwap[0].editableMesh.vertices.Count == 4)
                        {
                            aptPreviewTitle.text = aptToSwap[0].ApartmentType + " " + (aptToSwap[0].isM3 ? "M3" : "M2") + " - Instance";
                            StartCoroutine(ShowInterior());
                        }
                        else
                        {

                            var aptTypes = Standards.TaggedObject.ApartmentTypesMinimumSizes.Keys.ToList();
                            aptPreviewTitle.text = aptTypes[apartmentDropdown.value] + " " + (proceduralApartment.m3 ? "M3" : "M2") + " - Type";
                            apartmentDropdown.interactable = true;
                            StartCoroutine(RefreshProceduralApartment(apartmentDropdown.value, proceduralApartment.m3));
                        }
                        if (heightLines != null)
                        {
                            for (int i = 0; i < heightLines.Count; i++)
                            {
                                heightLines[i].SetActive(false);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.Log(e);
                    }
                    break;
            }
            if (previewChanged != null)
            {
                previewChanged(previewMode);
            }
        }



        private void ChangePreviewMode(PreviewMode previewMode)
        {
            switch (previewMode)
            {
                case PreviewMode.Buildings:
                    amenitiesButtons.SetActive(false);
                    for (int i = 0; i < proceduralBuildings.Count; i++)
                    {
                        for (int j = 0; j < proceduralBuildings[i].floors.Count; j++)
                        {
                            proceduralBuildings[i].floors[j].SetPreviewMode(previewMode);
                            if (proceduralBuildings[i].floors[j].centreLine != null)
                                proceduralBuildings[i].floors[j].centreLine.gameObject.SetActive(true);
                            proceduralBuildings[i].floors[j].ToggleChildren(true);
                        }
                    }
                    proceduralApartment.gameObject.SetActive(false);
                    proceduralApartment.GetComponent<ApartmentDimensions>().ToggleDimensions(false);
                    for (int i = 0; i < dimensionsParent.transform.childCount; i++)
                    {
                        Destroy(dimensionsParent.transform.GetChild(i));
                    }
                  
                    for (int i = 0; i < heightLines.Count; i++)
                    {
                        heightLines[i].SetActive(true);
                        UpdateHeightLine(masterPolylines[i]);
                    }

                    break;
                case PreviewMode.Floors:
                    amenitiesButtons.SetActive(false);
                    for (int i = 0; i < dimensionsParent.transform.childCount; i++)
                    {
                        Destroy(dimensionsParent.transform.GetChild(i));                        
                    }
                    
                    for (int i = 0; i < proceduralBuildings.Count; i++)
                    {
                        for (int j = 0; j < proceduralBuildings[i].floors.Count; j++)
                        {
                            var item = proceduralBuildings[i].floors[j];
                            item.SetPreviewMode(previewMode);
                            if (i == currentBuildingIndex)
                            {
                                if (item != proceduralBuildings[currentBuildingIndex].currentFloor)
                                {
                                    item.SetPreviewMode(previewMode);
                                    if (item.centreLine != null)
                                        item.centreLine.gameObject.SetActive(false);
                                    item.ToggleChildren(false);
                                }
                                else
                                {
                                    item.SetPreviewMode(previewMode);
                                    if (item.centreLine != null)
                                        item.centreLine.gameObject.SetActive(true);
                                    item.ToggleChildren(true);
                                }
                            }
                            else
                            {
                                item.SetPreviewMode(previewMode);
                                if (item.centreLine != null)
                                    item.centreLine.gameObject.SetActive(false);
                                item.ToggleChildren(false);
                            }
                        }
                    }
                    proceduralApartment.gameObject.SetActive(false);
                    proceduralApartment.GetComponent<ApartmentDimensions>().ToggleDimensions(false);

                    for (int i = 0; i < heightLines.Count; i++)
                    {
                        heightLines[i].SetActive(false);
                    }

                    break;
                case PreviewMode.Apartments:
                    amenitiesButtons.SetActive(true);
                    for (int i = 0; i < proceduralBuildings.Count; i++)
                    {
                        for (int j = 0; j < proceduralBuildings[i].floors.Count; j++)
                        {
                            proceduralBuildings[i].floors[j].SetPreviewMode(previewMode);
                            if (proceduralBuildings[i].floors[j].centreLine != null)
                                proceduralBuildings[i].floors[j].centreLine.gameObject.SetActive(false);
                            proceduralBuildings[i].floors[j].ToggleChildren(false);
                        }
                    }
                    proceduralApartment.gameObject.SetActive(true);
                    proceduralApartment.height = offsetDistance;
                    apartmentDropdown.interactable = true;
                    proceduralApartment.GetComponent<ApartmentDimensions>().ToggleDimensions(true);
                    for (int i = 0; i < heightLines.Count; i++)
                    {
                        heightLines[i].SetActive(false);
                    }
                    break;
            }
        }

        private void OnApartmentDropdownChanged(int value)
        {
            List<string> aptTypes = Standards.TaggedObject.ApartmentTypesMinimumSizes.Keys.ToList();
            if (aptPreviewBuildingType == BuildingType.Mansion)
            {
                aptTypes.Remove("Studio");
            }
            aptPreviewTitle.text = aptTypes[value] + " " + (proceduralApartment.m3 ? "M3" : "M2") + " - Type";
            StartCoroutine(RefreshProceduralApartment(value, proceduralApartment.m3));
        }

        private void OnM3ToggleChanged(bool m3)
        {
            aptPreviewTitle.text = proceduralApartment.apartmentType + " " + (m3 ? "M3" : "M2") + " - Type";
            StartCoroutine(RefreshProceduralApartment(aptLayoutDropDown.value, m3));
        }

        private void OnBuildingLayoutChanged(int value)
        {
            var aptTypes = Standards.TaggedObject.ApartmentTypesMinimumSizes.Keys.ToList();            
            if (value == 0)
            {
                aptPreviewBuildingType = BuildingType.Linear;
                aptLayoutDropDown.ClearOptions();
                aptTypes.Remove("SpecialApt");
            }
            else
            {
                aptPreviewBuildingType = BuildingType.Mansion;
                aptLayoutDropDown.ClearOptions();
                aptTypes.Remove("SpecialApt");
                aptTypes.Remove("Studio");
            }
            for (int i = 0; i < aptTypes.Count; i++)
            {
                aptLayoutDropDown.options.Add(new Dropdown.OptionData(aptTypes[i]));
            }
            aptLayoutDropDown.RefreshShownValue();
            aptLayoutDropDown.SetValue(0);
            m3LayoutToggle.SetValue(false);
            StartCoroutine(RefreshProceduralApartment(0, false));
        }

        private IEnumerator RefreshProceduralApartment(int value, bool m3 = false)
        {
            yield return StartCoroutine(proceduralApartment.ResetLayout());
            proceduralApartment.readOnly = false;
            proceduralApartment.m3 = m3;
            // proceduralApartment.height = (offsetDistance == 0) ? 7.2f : offsetDistance;// + Standards.TaggedObject.wallWidths["CorridorWall"];
            proceduralApartment.height = (offsetDistance == 0) ? 7.2f : ((aptPreviewBuildingType == BuildingType.Mansion) ? 11.77f : 7.2f);
            //{
            //    return (floors[0] as ProceduralTower).aptDepth;
            //}
      
            List<string> aptTypes = Standards.TaggedObject.ApartmentTypesMinimumSizes.Keys.ToList();
            if (aptPreviewBuildingType == BuildingType.Mansion)
            {
                aptTypes.Remove("Studio");
            }
            proceduralApartment.apartmentType = aptTypes[value];
            proceduralApartment.width = (float)((m3 ? Standards.TaggedObject.DesiredAreasM3[aptTypes[value]] : Standards.TaggedObject.DesiredAreas[aptTypes[value]]) / proceduralApartment.height);// +Standards.TaggedObject.wallWidths["PartyWalls"];
            proceduralApartment.buildingType = aptPreviewBuildingType;
            if (aptTypes[value] != "SpecialApt")
            {
                if (proceduralApartment.buildingType == BuildingType.Mansion)
                {
                    if (m3)
                    {
                        proceduralApartment.SetRooms(Standards.TaggedObject.aptLayouts[BuildingType.Mansion][aptTypes[value] + "_M3"]);
                    }
                    else
                    {
                        proceduralApartment.SetRooms(Standards.TaggedObject.aptLayouts[BuildingType.Mansion][aptTypes[value] + "_M2"]);
                    }
                }
                else
                {
                    if (m3)
                    {
                        proceduralApartment.SetRooms(Standards.TaggedObject.aptLayouts[BuildingType.Linear][aptTypes[value] + "_M3"]);
                    }
                    else
                    {
                        proceduralApartment.SetRooms(Standards.TaggedObject.aptLayouts[BuildingType.Linear][aptTypes[value] + "_M2"]);
                    }
                }
            }

            if (aptPreviewBuildingType == BuildingType.Mansion)
            {
                if (proceduralApartment.m3)
                {
                    proceduralApartment.width = Standards.TaggedObject.mansionWidthM3[aptTypes[value]];
                    proceduralApartment.height = 11.62f;
                }
                else
                {
                    proceduralApartment.width = Standards.TaggedObject.mansionWidth[aptTypes[value]];
                    proceduralApartment.height = 11.77f;
                }
            }
            else
            {

            }
            proceduralApartment.modulesScale = 1;
            apartmentTopCamera.transform.position = new Vector3(proceduralApartment.width / 2, 20, proceduralApartment.height / 2);
            apartmentFrontCamera.transform.position = new Vector3(proceduralApartment.width / 2, 1.7f, 20);
            apartmentBackCamera.transform.position = new Vector3(proceduralApartment.width / 2, 1.7f, -20);

            proceduralApartment.Initialize();
            proceduralApartment.GetComponent<ApartmentDimensions>().ToggleDimensions(true, apartmentViewMode);
            if (apartmentLayoutChanged != null)
            {
                apartmentLayoutChanged();
            }
            proceduralApartment.ShowOutline(true);
            proceduralApartment.ToggleMaxLayout(apartmentViewMode == ApartmentViewMode.Top);
            ToggleManufacturingSystem(manufacturingSystem);
            Selector.TaggedObject.ClearSelect();
        }

        private bool CheckStacking(ProceduralFloor first, ProceduralFloor second)
        {
            bool issue = false;
            if (first.percentages.Count != second.percentages.Count)
            {
                issue = true;
            }
            else
            {
                foreach (var item in first.percentages)
                {
                    if (!second.percentages.ContainsKey(item.Key))
                    {
                        issue = true;
                    }
                    else
                    {
                        if (second.percentages[item.Key] != item.Value)
                        {
                            issue = true;
                        }
                    }
                }
            }

            return issue;
        }

        private IEnumerator RefreshAllIterative(float maxWidth)
        {
            float.TryParse(offsetInputField.text, out m_aptDepth);
            float.TryParse(corridorInputField.text, out m_crdrWidth);

            for (int i = 0; i < proceduralBuildings.Count; i++)
            {
                //SetBuildingAspects();
                corridorWidth = m_crdrWidth;
                offsetDistance = m_aptDepth;
                proceduralBuildings[i].corridorWidth = m_crdrWidth;
                yield return StartCoroutine(proceduralBuildings[i].Refresh(m_aptDepth));
            }
            OnPreviewModeChanged(previewMode);
            StartCoroutine(OverallUpdate());
        }
        #endregion
    }

    #region SITE SERIALISATION CLASSES
    /// <summary>
    /// Types of extra massings for the site
    /// </summary>
    public enum ExtraMassingType
    {
        /// <summary>
        /// Based on the footprint of the site
        /// </summary>
        Footprint = 0,
        /// <summary>
        /// Based on a custom polygon
        /// </summary>
        Custom = 1
    }

    /// <summary>
    /// A class for the site wide basement
    /// </summary>
    [Serializable]
    public class SiteBasement : SiteGeometry
    {

    }

    /// <summary>
    /// A class for the site-wide podium
    /// </summary>
    [Serializable]
    public class SitePodium : SiteGeometry
    {

    }

    /// <summary>
    /// A class for the site-wide extra massings on the site
    /// </summary>
    public class SiteGeometry
    {
        /// <summary>
        /// The number of floors for the massing
        /// </summary>
        public int floors;
        /// <summary>
        /// The floor to floor height for the massing
        /// </summary>
        public float floor2Floor;
        /// <summary>
        /// The polygon of the massing
        /// </summary>
        public Polygon polygon;
        /// <summary>
        /// The site offset (if footprint based) for the massing
        /// </summary>
        public float offset;
        /// <summary>
        /// The mix of the massing
        /// </summary>
        public Dictionary<string, float> use;
        /// <summary>
        /// Whether it has a custom polygon for geometry
        /// </summary>
        public bool isCustomPolygon;
        /// <summary>
        /// The indices of the buildings which are above the massing
        /// </summary>
        public int[] buildingIndices;
        /// <summary>
        /// The floor are of the massing
        /// </summary>
        public float FloorArea
        {
            get
            {
                return polygon.Area;
            }
        }
        /// <summary>
        /// The list of buildings which are above the massing
        /// </summary>
        public List<ProceduralBuilding> buildings;
    }
    #endregion
}

﻿using BrydenWoodUnity.GeometryManipulation;
using BrydenWoodUnity.UIElements;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using BWT = BrydenWoodUnity.GeometryManipulation.ThreeJs;

namespace BrydenWoodUnity.DesignData
{
    public enum WindowType
    {
        front,
        back
    }
    /// <summary>
    /// A MonoBehaviour component for Procedural Rooms
    /// </summary>
    public class ProceduralRoom : MonoBehaviour, ICancelable
    {
        #region Public Properties
        public GameObject polygonPrefab;
        public GameObject polygonVertexPrefab;
        public GameObject extrusionPrefab;
        public GameObject lineLoadPrefab;
        public GameObject amenitiesButtonPrefab;
        public Material roomMaterial;
        [HideInInspector]
        public ProceduralApartmentLayout aptLayout;
        public List<float> lengths;
        [HideInInspector]
        public List<Vector3> vertices;
        [HideInInspector]
        public Color color;


        public Polygon polygon { get; set; }
        public bool hasBalcony { get; set; }
        public float totalHeight { get; set; }
        public int roomIndex { get; set; }
        public bool isBalcony { get; set; }
        public int dimensionStartIndex { get; set; }
        public int dimensionEndIndex { get; set; }
        public int dimensionSide { get; set; }
        public float[] originalLengths { get; set; }
        public Vector3 dimensionStart
        {
            get
            {
                try
                {
                    return vertices[dimensionStartIndex];
                }
                catch (Exception e)
                {
                    Debug.Log(gameObject.name);
                    return Vector3.zero;
                }
            }
        }
        public Vector3 dimensionEnd
        {
            get
            {
                try
                {
                    return vertices[dimensionEndIndex];
                }
                catch (Exception e)
                {
                    Debug.Log(gameObject.name);
                    return Vector3.zero;
                }
            }
        }
        public bool showDimension;
        public bool issue { get; private set; }

        public GameObject window { get; private set; }
        public GameObject backWindow { get; private set; }
        public WindowType windowtype { get; private set; } = WindowType.front;
        public List<BWT.Line> lines;
        #endregion

        #region Private Properties
        private Vector3 currentPoint { get; set; }
        private Text roomName { get; set; }
        private List<GameObject> lineLoads { get; set; }
        private List<LineLoad> issues { get; set; }
        private GameObject amenitiesUIObject { get; set; }
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnDestroy()
        {
            CheckMinDm();
            if (amenitiesUIObject != null)
            {
                Destroy(amenitiesUIObject);
            }
            if (roomName != null)
            {
                DestroyImmediate(roomName.gameObject);
            }
            //ShowRoomName(false);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Initializes the Instance
        /// </summary>
        /// <param name="lengths">The lengths of the room's sides as a list</param>
        public void Initialize(List<float> lengths)
        {
            this.lengths = lengths;
            GetVectors();
            GenerateGeometry();
            GenerateLineLoads();
            ToggleLineLoads(false);
            CheckMinDm();
        }

        /// <summary>
        /// Initializes the Instance
        /// </summary>
        /// <param name="lengths">The lengths of the room's sides as a list</param>
        /// <param name="type">The type of the room</param>
        /// <param name="totalHeight">The total height of the room</param>
        /// <param name="index">The index of the room</param>
        /// <param name="showDimension">Whether the room should display its dimensions</param>
        public void Initialize(List<float> lengths, string type, float totalHeight, int index, bool showDimension, bool generateWindow = false)
        {
            this.totalHeight = totalHeight;
            this.showDimension = showDimension;
            gameObject.AddComponent<RoomUnity>().Initialize(index, type);
            this.lengths = lengths;
            roomIndex = index;
            isBalcony = false;
            if (aptLayout.aptLayout.buildingType == BuildingType.Mansion)
            {
                Dictionary<string, Dictionary<int, int>> dict = aptLayout.m3 ? Standards.TaggedObject.amenitiesIndicesMansionM3 : Standards.TaggedObject.amenitiesIndicesMansion;
                foreach (var item in dict)
                {
                    foreach (var balc in item.Value)
                    {
                        if (index == balc.Value)
                        {
                            isBalcony = true;
                        }
                    }
                }
            }
            else
            {
                Dictionary<string, Dictionary<int, int>> dict = aptLayout.m3 ? Standards.TaggedObject.amenitiesIndicesM3 : Standards.TaggedObject.amenitiesIndices;
                foreach (var item in dict)
                {
                    foreach (var balc in item.Value)
                    {
                        if (index == balc.Value)
                        {
                            isBalcony = true;
                        }
                    }
                }
            }

            if (isBalcony)
            {
                this.totalHeight = 1.0f;
            }

            originalLengths = new float[lengths.Count];

            for (int i = 0; i < originalLengths.Length; i++)
            {
                originalLengths[i] = lengths[i];
            }

            GetVectors();
            GenerateGeometry();
            ShowRoomName(true);
            int[] dimIndices = null;
            if (aptLayout.buildingType == BuildingType.Mansion)
            {
                if (aptLayout.m3)
                {
                    dimIndices = Standards.TaggedObject.RoomsDimensionIndicesMansionM3[gameObject.name];
                }
                else
                {
                    dimIndices = Standards.TaggedObject.RoomsDimensionIndicesMansion[gameObject.name];
                }
            }
            else
            {
                if (aptLayout.m3)
                    dimIndices = Standards.TaggedObject.RoomsDimensionIndicesM3[gameObject.name];
                else
                    dimIndices = Standards.TaggedObject.RoomsDimensionIndices[gameObject.name];
            }
            dimensionStartIndex = dimIndices[0];
            dimensionEndIndex = dimIndices[1];
            dimensionSide = dimIndices[2];
            GenerateLineLoads();
            var wind = aptLayout.buildingType == BuildingType.Mansion ? Standards.TaggedObject.roomsWindowsMansion : Standards.TaggedObject.roomsWindows;
            if (wind.ContainsKey(gameObject.name))
            {
                if (wind[gameObject.name] && lengths[0] != 0)
                    GenerateWindow();
            }
            ToggleLineLoads(false);
            CheckMinDm();
            if (showDimension)
            {
                //if (aptLayout.buildingType == BuildingType.Mansion)
                //{

                //}
                //else
                //{
                CreateAmenitiesButton();
            }
        }

        public void GenerateWindow()
        {
            Dictionary<string, Dictionary<string, WindowsPosition>> windPos = null;
            if (aptLayout.buildingType == BuildingType.Mansion)
            {
                if (aptLayout.m3)
                {
                    windPos = Standards.TaggedObject.WindowsPositionsMansionM3;
                }
                else
                {
                    windPos = Standards.TaggedObject.WindowsPositionsMansion;
                }
            }
            else
            {
                if (aptLayout.m3)
                    windPos = Standards.TaggedObject.WindowsPositionsM3;
                else
                    windPos = Standards.TaggedObject.WindowsPositions;
            }
            if (windPos.ContainsKey(aptLayout.apartmentType))
            {
                WindowsPosition _position = windPos[aptLayout.apartmentType][gameObject.name];
                Vector3 origin = vertices[_position.origin];
                window = GameObject.CreatePrimitive(PrimitiveType.Cube);
                window.name = "window_" + gameObject.name + "_" + aptLayout.apartmentType;
                window.GetComponent<MeshRenderer>().sharedMaterial = Resources.Load("Materials/Windows") as Material;
                Destroy(window.GetComponent<Collider>());
                window.transform.SetParent(transform);

                float defaultHeight = 3f;
                float height = _position.height;
                float width = _position.width;
                float heightRemainder = defaultHeight - height - _position.cill;
                float minWidthRemainder = 0.35f;
                if (Vector3.Distance(vertices[_position.origin], vertices[(_position.origin + 1) % vertices.Count]) - width < minWidthRemainder)
                    width = Vector3.Distance(vertices[_position.origin], vertices[_position.origin + 1]) - _position.fromWalls - minWidthRemainder;
                height = aptLayout.totalHeight - _position.cill - heightRemainder;



                Vector3 scale = new Vector3(width, height, 0.05f);
                window.transform.localScale = scale;
                GetMeshFromCube(window);
                //change local position based on building type
                if (aptLayout.buildingType == BuildingType.Mansion)
                {
                    window.transform.localPosition = origin + new Vector3(scale.x * 0.5f, scale.y * 0.5f, -scale.z * 0.5f) + new Vector3(_position.fromWalls, _position.cill, 0);
                    if (aptLayout.height / 2 < window.transform.position.z)
                    {
                        window.transform.localPosition += new Vector3(0, 0, Standards.TaggedObject.ConstructionFeatures["ExteriorWall"]);
                        windowtype = WindowType.front;
                    }
                    else
                    {
                        window.transform.localPosition += new Vector3(0, 0, -(Standards.TaggedObject.ConstructionFeatures["ExteriorWall"] + Standards.TaggedObject.ConstructionFeatures["PartyWall"] / 2)); //extend to blue line
                        //window.transform.localPosition += new Vector3(0, 0, -Standards.TaggedObject.ConstructionFeatures["ExteriorWall"]);
                        windowtype = WindowType.back;
                    }

                }
                else
                {
                    window.transform.localPosition = origin + new Vector3(scale.x * 0.5f, scale.y * 0.5f, -scale.z * 0.5f) + new Vector3(_position.fromWalls, _position.cill, Standards.TaggedObject.ConstructionFeatures["ExteriorWall"]);
                    windowtype = WindowType.front;
                }
            }

            if (aptLayout.SingleLoaded && aptLayout.buildingType != BuildingType.Mansion && gameObject.name.Contains("LR"))
            {
                Vector3 origin = vertices[0];
                backWindow = GameObject.CreatePrimitive(PrimitiveType.Cube);
                backWindow.name = "backWindow_" + gameObject.name + "_" + aptLayout.apartmentType;
                backWindow.GetComponent<MeshRenderer>().sharedMaterial = Resources.Load("Materials/Windows") as Material;
                Destroy(backWindow.GetComponent<Collider>());
                backWindow.transform.SetParent(transform);

                float defaultHeight = 3f;
                float height = 1.65f;
                float width = 1.05f;
                float heightRemainder = defaultHeight - height - 0.8f;
                float minWidthRemainder = 0.35f;
                if (Vector3.Distance(vertices[0], vertices[1]) - width < minWidthRemainder)
                    width = Vector3.Distance(vertices[0], vertices[1]) - 0.5f - minWidthRemainder;
                height = aptLayout.totalHeight - 0.8f - heightRemainder;

                Vector3 scale = new Vector3(width, height, 0.05f);
                backWindow.transform.localScale = scale;
                GetMeshFromCube(backWindow);
                backWindow.transform.localPosition = origin + new Vector3(scale.x * 0.5f, scale.y * 0.5f, -scale.z * 0.5f) + new Vector3(0.5f, 0.8f, 0);
                backWindow.transform.localPosition += new Vector3(0, 0, -(Standards.TaggedObject.ConstructionFeatures["ExteriorWall"] + Standards.TaggedObject.ConstructionFeatures["PartyWall"] / 2));
            }
        }

        private void GetMeshFromCube(GameObject cubeObject)
        {
            Mesh m = new Mesh();
            Mesh cubeMesh = cubeObject.GetComponent<MeshFilter>().sharedMesh;
            Vector3[] verts = new Vector3[cubeMesh.vertexCount];
            Color[] vertexColors = new Color[verts.Length];
            int[] tris = cubeMesh.triangles;

            for (int i = 0; i < verts.Length; i++)
            {
                verts[i] = cubeObject.transform.TransformPoint(cubeMesh.vertices[i]);
                vertexColors[i] = Color.cyan;
            }
            m.vertices = verts;
            m.colors = vertexColors;
            m.triangles = tris;
            m.RecalculateNormals();
            m.RecalculateBounds();
            m.uv = cubeMesh.uv;

            cubeObject.transform.localScale = Vector3.one;
            cubeObject.GetComponent<MeshFilter>().sharedMesh = m;
        }

        public void GetThreeJsObjects(List<Vector3> verts)
        {
            lines = new List<BWT.Line>();

            for (int i = 0; i < verts.Count; i++)
            {
                int next = (i + 1) % verts.Count;
                //Debug.Log(next);

                Debug.DrawLine(verts[i], verts[next], Color.yellow, 5f);

                if (Vector3.Distance(verts[i], verts[next]) < 0.05f)
                {
                    continue;
                }
                BWT.Line line = new BWT.Line();
                BWT.Geometry geom = new BWT.Geometry();
                Vector3 current = transform.TransformPoint(verts[i]);
                Vector3 nextVec = transform.TransformPoint(verts[next]);


                if (Math.Abs(Math.Round(current.x, 2) - Math.Round(nextVec.x, 2)) <= 0.015f)
                {
                    if (current.z < nextVec.z)
                    {
                        geom.data.vertices = new double[6]
                        {
                            Math.Round(current.x, 2),Math.Round(current.y, 2),Math.Round(current.z, 2),
                            Math.Round(current.x, 2),Math.Round(nextVec.y, 2),Math.Round(nextVec.z, 2)
                        };

                    }
                    else
                    {
                        geom.data.vertices = new double[6]
                        {
                            Math.Round(current.x, 2),Math.Round(nextVec.y, 2),Math.Round(nextVec.z, 2),
                            Math.Round(current.x, 2),Math.Round(current.y, 2),Math.Round(current.z, 2)
                        };

                    }
                }
                else if (Math.Abs(Math.Round(current.z, 2) - Math.Round(nextVec.z, 2)) <= 0.015f)
                {

                    if (current.x < nextVec.x)
                    {
                        geom.data.vertices = new double[6]
                        {
                            Math.Round(current.x, 2),Math.Round(current.y, 2),Math.Round(current.z, 2),
                            Math.Round(nextVec.x, 2),Math.Round(nextVec.y, 2),Math.Round(current.z, 2)
                        };

                    }
                    else
                    {
                        geom.data.vertices = new double[6]
                        {
                            Math.Round(nextVec.x, 2),Math.Round(nextVec.y, 2),Math.Round(current.z, 2),
                            Math.Round(current.x, 2),Math.Round(current.y, 2),Math.Round(current.z, 2)
                        };

                    }
                }



                if (geom.data.vertices.Length == 0)
                {
                    Debug.Log("not parallel line!!!  " + i + "_" + verts.Count);
                    Debug.Log(Math.Round(current.x, 2) + " , " + Math.Round(current.y, 2) + " , " + Math.Round(current.z, 2));
                    Debug.Log(Math.Round(nextVec.x, 2) + " , " + Math.Round(nextVec.y, 2) + " , " + Math.Round(nextVec.z, 2));
                }
                else
                {
                    line._geometry = geom;
                    line.geometry = geom.uuid;
                    lines.Add(line);
                }

            }
            aptLayout.lines.AddRange(lines);
        }

        /// <summary>
        /// Updates the room geometry
        /// </summary>
        /// <param name="lengths">The lengths of the room's sides as a list</param>
        public void UpdateRoom(List<float> lengths)
        {
            this.lengths = lengths;
            GetVectors();
            UpdateGeometry();
            GenerateLineLoads();
            ToggleLineLoads(aptLayout.manufacturingSystem == ManufacturingSystem.Panels);
            CheckMinDm();
        }

        /// <summary>
        /// Returns the positions of the room vertex at a given index
        /// </summary>
        /// <param name="index">The index of the vertex</param>
        /// <returns></returns>
        public Vector3 GetVertex(int index)
        {
            return transform.TransformPoint(vertices[index]);
        }

        /// <summary>
        /// Toggle the display of the name of the room
        /// </summary>
        /// <param name="show">Whether the name should be displayed or not</param>
        public void ShowRoomName(bool show)
        {
            var cam = GameObject.Find("TopViewCamera");
            if (cam != null)
            {
                var centre = polygon.Centre;
                if (show && centre != Vector3.zero && lengths[0] != 0)
                {
                    if (roomName == null)
                    {
                        roomName = Instantiate(Resources.Load("GUI/RoomName") as GameObject).GetComponent<Text>();
                        roomName.transform.position = cam.GetComponent<Camera>().WorldToScreenPoint(transform.TransformPoint(polygon.Centre));
                        roomName.transform.SetParent(aptLayout.roomNamesParent);
                        roomName.transform.localScale = Vector3.one;
                    }

                    if (aptLayout.buildingType == BuildingType.Mansion)
                    {
                        if (aptLayout.m3)
                        {
                            roomName.text = Standards.TaggedObject.RoomNamesMansionM3[gameObject.name] + "\r\n" + Math.Round(polygon.Area) + "m\xB2";
                        }
                        else
                        {
                            roomName.text = Standards.TaggedObject.RoomNamesMansion[gameObject.name] + "\r\n" + Math.Round(polygon.Area) + "m\xB2";
                        }
                    }
                    else
                    {
                        Dictionary<string, string> dict = aptLayout.m3 ? Standards.TaggedObject.RoomNamesM3 : Standards.TaggedObject.RoomNames;
                        if (gameObject.name == "BA")
                        {
                            if (aptLayout.apartmentType == "2b4p" || aptLayout.apartmentType == "3b5p")
                            {
                                roomName.text = "Ensuite" + "\r\n" + Math.Round(polygon.Area) + "m\xB2";
                            }
                            else
                            {
                                roomName.text = dict[gameObject.name] + "\r\n" + Math.Round(polygon.Area) + "m\xB2";
                            }
                        }
                        else
                            roomName.text = dict[gameObject.name] + "\r\n" + Math.Round(polygon.Area) + "m\xB2";
                    }
                }
                else
                {
                    if (roomName != null)
                    {
                        DestroyImmediate(roomName.gameObject);
                    }
                }
            }
        }

        /// <summary>
        /// Toggles the line loads On or Off
        /// </summary>
        /// <param name="show">On or Off</param>
        public void ToggleLineLoads(bool show)
        {
            if (aptLayout.buildingType != BuildingType.Mansion)
            {
                if (lineLoads == null && show)
                {
                    GenerateLineLoads();
                }

                if (lineLoads != null)
                {
                    for (int i = 0; i < lineLoads.Count; i++)
                    {
                        if (lineLoads[i] != null)
                        {
                            lineLoads[i].GetComponent<MeshRenderer>().enabled = show;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Returns the game objects of the line loads
        /// </summary>
        /// <returns>List of GameObjects</returns>
        public List<GameObject> GetLineLoads()
        {
            List<GameObject> loads = new List<GameObject>();
            if (lineLoads != null)
            {
                for (int i = 0; i < lineLoads.Count; i++)
                {
                    if (!lineLoads[i].GetComponent<LineLoad>().issue)
                    {
                        loads.Add(lineLoads[i]);
                    }
                }
            }
            return loads;
        }

        /// <summary>
        /// Returns the game objects of the line loads for a given height
        /// </summary>
        /// <param name="height">The height of the gameobjects</param>
        /// <returns>List of GameObjects</returns>
        public List<GameObject> GetLineLoads(float height)
        {
            List<GameObject> loads = new List<GameObject>();
            if (lengths[0] != 0)
            {
                if (Standards.TaggedObject.LineLoadsPerType[aptLayout.apartmentType].Contains(gameObject.name))
                {
                    var points = Standards.TaggedObject.lineLoadStart[gameObject.name];

                    for (int i = 0; i < points.Length; i++)
                    {
                        var index = points[i][0];
                        var next = (index + 1) % polygon.Count;

                        float length = aptLayout.outterHeight;
                        GameObject obj = Instantiate(lineLoadPrefab, transform);
                        if (points[i][1] == 0)
                        {
                            obj.transform.position = new Vector3(transform.TransformPoint(vertices[index]).x, height / 2.0f, length * 0.25f);
                            obj.transform.localScale = new Vector3(0.03f, height * 0.98f, length * 0.5f);
                            obj.layer = 17;
                        }
                        else
                        {
                            obj.layer = 18;
                            obj.transform.position = new Vector3(transform.TransformPoint(vertices[index]).x, height / 2.0f, length * 0.75f);
                            obj.transform.localScale = new Vector3(0.03f, height * 0.98f, length * 0.5f);
                        }
                        loads.Add(obj);
                        aptLayout.AddLineLoad(obj, points[i][1]);
                    }
                }
            }
            return loads;
        }

        /// <summary>
        /// Create the button for adding and removing amenity space
        /// </summary>
        public void CreateAmenitiesButton()
        {
            var cam = GameObject.Find("TopViewCamera");
            if (cam != null && lengths[0] != 0)
            {
                if (Standards.TaggedObject.balconies == Balconies.Inset)
                {
                    if (aptLayout.buildingType != BuildingType.Mansion)
                    {
                        if (name == "LRR" || name == "LR" || name == "LRRRR" || name == "LRRR")
                        {
                            amenitiesUIObject = Instantiate(amenitiesButtonPrefab, GameObject.Find("AmenitiesButtons").transform);
                            Vector3 pos = transform.TransformPoint((dimensionStart + (dimensionEnd - dimensionStart) / 2) + new Vector3(0, 0, -1.0f));
                            amenitiesUIObject.transform.position = cam.GetComponent<Camera>().WorldToScreenPoint(pos);
                            amenitiesUIObject.GetComponent<Button>().onClick.AddListener(CreateBalcony);
                            int indexOfBalcony = -1;
                            Dictionary<string, Dictionary<int, int>> dict = aptLayout.m3 ? Standards.TaggedObject.amenitiesIndicesM3 : Standards.TaggedObject.amenitiesIndices;
                            if (dict["Interior"].TryGetValue(GetComponent<RoomUnity>().id, out indexOfBalcony))
                            {
                                amenitiesUIObject.transform.GetChild(0).GetComponent<Text>().text = (aptLayout.aptLayout.roomInclusion[indexOfBalcony]) ? "-" : "+";
                            }
                        }
                    }
                }
                else
                {
                    if (aptLayout.buildingType == BuildingType.Mansion)
                    {
                        if (name == "LRR" || name == "LR")
                        {
                            amenitiesUIObject = Instantiate(amenitiesButtonPrefab, GameObject.Find("AmenitiesButtons").transform);
                            Vector3 pos = Vector3.zero;
                            if (aptLayout.m3)
                            {
                                if (name == "LR")
                                    pos = transform.TransformPoint((vertices[6] + (vertices[7] - vertices[6]) / 2) + new Vector3(0, 0, -1.0f));
                                else
                                    pos = transform.TransformPoint((vertices[4] + (vertices[5] - vertices[4]) / 2) + new Vector3(0, 0, -1.0f));
                            }
                            else
                            {
                                pos = transform.TransformPoint((vertices[4] + (vertices[5] - vertices[4]) / 2) + new Vector3(0, 0, -1.0f));
                            }
                            amenitiesUIObject.transform.position = cam.GetComponent<Camera>().WorldToScreenPoint(pos);
                            amenitiesUIObject.GetComponent<Button>().onClick.AddListener(CreateBalcony);
                            int indexOfBalcony = -1;
                            Dictionary<string, Dictionary<int, int>> dict = aptLayout.m3 ? Standards.TaggedObject.amenitiesIndicesMansionM3 : Standards.TaggedObject.amenitiesIndicesMansion;
                            if (dict["Exterior"].TryGetValue(GetComponent<RoomUnity>().id, out indexOfBalcony))
                            {
                                amenitiesUIObject.transform.GetChild(0).GetComponent<Text>().text = (aptLayout.aptLayout.roomInclusion[indexOfBalcony]) ? "-" : "+";
                            }
                        }
                        else if (name == "LRRR" || name == "LRRRR")
                        {
                            amenitiesUIObject = Instantiate(amenitiesButtonPrefab, GameObject.Find("AmenitiesButtons").transform);
                            Vector3 pos = Vector3.zero;
                            if (aptLayout.m3)
                            {
                                if (name == "LRRRR")
                                    pos = transform.TransformPoint((vertices[8] + (vertices[9] - vertices[8]) / 2) + new Vector3(0, 0, -1.0f));
                                else
                                    pos = transform.TransformPoint((vertices[6] + (vertices[7] - vertices[6]) / 2) + new Vector3(0, 0, -1.0f));
                            }
                            else
                            {
                                pos = transform.TransformPoint((vertices[6] + (vertices[7] - vertices[6]) / 2) + new Vector3(0, 0, -1.0f));
                            }
                            amenitiesUIObject.transform.position = cam.GetComponent<Camera>().WorldToScreenPoint(pos);
                            amenitiesUIObject.GetComponent<Button>().onClick.AddListener(CreateBalcony);
                            int indexOfBalcony = -1;
                            Dictionary<string, Dictionary<int, int>> dict = aptLayout.m3 ? Standards.TaggedObject.amenitiesIndicesMansionM3 : Standards.TaggedObject.amenitiesIndicesMansion;
                            if (dict["Exterior"].TryGetValue(GetComponent<RoomUnity>().id, out indexOfBalcony))
                            {
                                amenitiesUIObject.transform.GetChild(0).GetComponent<Text>().text = (aptLayout.aptLayout.roomInclusion[indexOfBalcony]) ? "-" : "+";
                            }
                        }
                    }
                    else
                    {
                        if (name == "LRR" || name == "LR" || name == "LRRRR" || name == "LRRR")
                        {
                            //if (name == "DB" ||
                            //    name == "DBB" ||
                            //    name == "PB" ||
                            //    name == "PBB" ||
                            //    name == "LR" ||
                            //    name == "LRR" ||
                            //    name == "PSB")
                            //{


                            amenitiesUIObject = Instantiate(amenitiesButtonPrefab, aptLayout.amenitiesButtonsParent);
                            Vector3 pos = transform.TransformPoint((dimensionStart + (dimensionEnd - dimensionStart) / 2) + new Vector3(0, 0, -1.0f));
                            amenitiesUIObject.transform.position = cam.GetComponent<Camera>().WorldToScreenPoint(pos);
                            amenitiesUIObject.GetComponent<Button>().onClick.AddListener(CreateBalcony);
                            int indexOfBalcony = -1;
                            Dictionary<string, Dictionary<int, int>> dict = aptLayout.m3 ? Standards.TaggedObject.amenitiesIndicesM3 : Standards.TaggedObject.amenitiesIndices;
                            if (dict["Exterior"].TryGetValue(GetComponent<RoomUnity>().id, out indexOfBalcony))
                            {
                                amenitiesUIObject.transform.GetChild(0).GetComponent<Text>().text = (aptLayout.aptLayout.roomInclusion[indexOfBalcony]) ? "-" : "+";
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Toggles the amenity space of thius room
        /// </summary>
        public void CreateBalcony()
        {
            if (Standards.TaggedObject.balconies == Balconies.Inset)
            {
                RefreshPopUp.TaggedObject.AddChangedValue(this, new string[] { String.Empty, String.Empty });
            }
            else
            {
                if (aptLayout.buildingType == BuildingType.Mansion)
                {
                    aptLayout.UpdateMansionAmenities(GetComponent<RoomUnity>().id);
                }
                else
                {
                    float length = 0;
                    if (name == "LR" || name == "LRR" || name == "LRRR" || name == "LRRRR")
                    {
                        length = lengths[2];
                    }
                    aptLayout.UpdateAmenities(GetComponent<RoomUnity>().id, length);
                }
            }
        }

        /// <summary>
        /// Checks if there are any issues with the panels
        /// </summary>
        /// <returns></returns>
        public bool CheckIfPanelIssues()
        {
            int counter = 0;
            for (int i = 0; i < lineLoads.Count; i++)
            {
                if (lineLoads[i].GetComponent<LineLoad>().issue)
                {
                    counter++;
                }
            }
            return counter != 0;
        }

        /// <summary>
        /// Resets the value of the amenity button
        /// </summary>
        /// <param name="prevValue"></param>
        public void ResetValue(string prevValue)
        {

        }

        /// <summary>
        /// Submits the new value of the amenity button
        /// </summary>
        /// <param name="currentValue"></param>
        public void SubmitValue(string currentValue)
        {
            float length = 0;
            if (name == "LR" || name == "LRR")
            {
                length = lengths[2];
            }
            aptLayout.UpdateAmenities(GetComponent<RoomUnity>().id, length);
        }
        #endregion

        #region Private Methods
        private void CheckMinDm()
        {
            //if (lengths[0] != 0)
            //{
            //    bool widthIssue = false;
            //    float minWidth = 0.0f;
            //    if ((gameObject.name == "LRR" || gameObject.name == "LR"))
            //    {
            //        if ((lengths[0] <= aptLayout.wallWidth && lengths[0] != 0))
            //        {
            //            SendMessageUpwards("IssuedFlagged", true);
            //        }
            //        else if (lengths[0] != 0 && lengths[0] > aptLayout.wallWidth)
            //        {
            //            SendMessageUpwards("IssuedFlagged", false);
            //        }
            //        if (Standards.TaggedObject.MinimumRoomWidths.TryGetValue(gameObject.name, out minWidth))
            //        {
            //            widthIssue = minWidth > Mathf.Abs(lengths[2]);
            //        }
            //    }
            //    else if (gameObject.name == "DB")
            //    {
            //        if (Standards.TaggedObject.MinimumRoomWidths.TryGetValue(gameObject.name, out minWidth))
            //        {
            //            widthIssue = minWidth > Mathf.Abs(lengths[4]);
            //        }
            //    }
            //    else
            //    {
            //        if (Standards.TaggedObject.MinimumRoomWidths.TryGetValue(gameObject.name, out minWidth))
            //        {
            //            widthIssue = minWidth > Mathf.Abs(lengths[0]);
            //        }
            //    }

            //    string widthNotification = string.Format("The {0} in the standard {1} is too narrow!", Standards.TaggedObject.RoomNames[gameObject.name], aptLayout.apartmentType);

            //    if (widthIssue)
            //    {
            //        Notifications.TaggedObject.AddRoomNotification(widthNotification);
            //        Notifications.TaggedObject.UpdateNotifications();
            //    }
            //    else
            //    {
            //        Notifications.TaggedObject.RemoveRoomNotification(widthNotification);
            //        Notifications.TaggedObject.UpdateNotifications();
            //    }
            //}
        }

        private void GetVectors()
        {
            vertices = new List<Vector3>();
            currentPoint = Vector3.zero;
            vertices.Add(currentPoint);
            for (int i = 0; i < lengths.Count; i++)
            {
                if (i % 2 == 0)
                {
                    currentPoint += new Vector3(lengths[i], 0, 0);
                }
                else
                {
                    currentPoint += new Vector3(0, 0, lengths[i]);
                }
                vertices.Add(new Vector3(currentPoint.x, currentPoint.y, currentPoint.z));
            }
        }

        private void GenerateGeometry()
        {
            List<PolygonVertex> verts = new List<PolygonVertex>();

            List<Vector3> temp_verts = new List<Vector3>();
            List<Vector3> offsets = new List<Vector3>();
            for (int i = 0; i < vertices.Count; i++)
            {
                bool allowed = true;
                for (int j = 0; j < temp_verts.Count; j++)
                {
                    if (Vector3.Distance(vertices[i], temp_verts[j]) < 0.05f)
                    {
                        allowed = false;
                    }
                }
                if (allowed)
                {
                    var o = new GameObject(name + ":" + temp_verts.Count);
                    temp_verts.Add(vertices[i]);
                    o.transform.SetParent(transform);
                    o.transform.localPosition = vertices[i];
                }
            }

            bool isClockWise = Polygon.IsClockWise(temp_verts);

            for (int i = 0; i < temp_verts.Count; i++)
            {
                int next = (i + 1) % temp_verts.Count;
                int prev = i - 1;
                if (i - 1 == -1)
                {
                    prev = temp_verts.Count - 1;
                }
                Vector3 v1 = temp_verts[prev] - temp_verts[i];
                Vector3 v2 = temp_verts[next] - temp_verts[i];
                v1.Normalize();
                v2.Normalize();
                var cross = Vector3.Cross(v1, v2);
                float angle = Vector3.Angle(v1, v2);
                float vectorLength;
                if (Mathf.Sin(Mathf.Deg2Rad * angle * 0.5f) != 0)
                {
                    vectorLength = (aptLayout.wallWidth / 2.0f) / Mathf.Sin(Mathf.Deg2Rad * angle * 0.5f);
                }
                else
                {
                    vectorLength = (aptLayout.wallWidth / 2.0f);
                }
                var addition = (v1 + v2);
                addition.Normalize();
                addition *= vectorLength;
                if (Vector3.Cross(v1, v2).y < 0)
                {
                    addition *= -1;
                }
                if (isClockWise)
                {
                    addition *= -1;
                }

                if (addition.magnitude == 0)
                {
                    addition = Vector3.Cross(v1, Vector3.up);
                    addition.Normalize();
                    addition *= -vectorLength;
                }

                offsets.Add(temp_verts[i] + addition);
            }

            for (int i = 0; i < offsets.Count; i++)
            {
                var obj = Instantiate(polygonVertexPrefab, transform);
                verts.Add(obj.GetComponent<PolygonVertex>());
                verts[i].Initialize(i);
                verts[i].UpdatePosition(offsets[i]);
            }
            polygon = Instantiate(polygonPrefab).GetComponent<Polygon>();
            polygon.GetComponent<Renderer>().material = roomMaterial;//new Material(polygon.GetComponent<Renderer>().material);
            //polygon.GetComponent<Renderer>().material.color = color;
            polygon.Initialize(verts, true, true, false);
            polygon.transform.SetParent(transform);
            polygon.transform.localPosition = Vector3.zero;

            GameObject extr = Instantiate(extrusionPrefab, polygon.transform);
            extr.GetComponent<Extrusion>().capped = false;
            extr.GetComponent<Extrusion>().convex = false;
            extr.GetComponent<Extrusion>().totalHeight = totalHeight;
            if (name.Contains("ALR"))
            {
                extr.GetComponent<Extrusion>().sidesToNotExtrude = new int[] { 0 };
            }
            extr.GetComponent<Extrusion>().Initialize(polygon);

            polygon.SetVertexColors(color);
            GetComponent<RoomUnity>().SetPolygon(polygon);
            if (Standards.TaggedObject.TotalAmenitiesArea.ContainsKey(aptLayout.apartmentType))
            {
                if (Standards.TaggedObject.TotalAmenitiesArea[aptLayout.apartmentType].ContainsKey(gameObject.name))
                {
                    Standards.TaggedObject.TotalAmenitiesArea[aptLayout.apartmentType][gameObject.name] = (float)Math.Round(polygon.Area);
                }
            }
            if (name.Contains("HW") && lengths[0] != 0)
            {
                if (aptLayout.buildingType == BuildingType.Mansion)
                {
                    aptLayout.hallEntrance = new Line(transform.TransformPoint(vertices[vertices.Count - 2]), transform.TransformPoint(vertices[0]));
                }
                else
                {
                    aptLayout.hallEntrance = new Line(transform.TransformPoint(vertices[0]), transform.TransformPoint(vertices[1]));
                    //Debug.Log(transform.TransformPoint(vertices[0]) + "," + transform.TransformPoint(vertices[1]));
                }
            }
            GetThreeJsObjects(temp_verts);
        }

        private void GenerateLineLoads()
        {
            if (aptLayout.buildingType != BuildingType.Mansion)
            {
                if (lengths[0] != 0)
                {
                    if (lineLoads != null)
                    {
                        foreach (var item in lineLoads)
                        {
                            DestroyImmediate(item);
                        }
                    }

                    lineLoads = new List<GameObject>();
                    issues = new List<LineLoad>();
                    if (Standards.TaggedObject.LineLoadsPerType[aptLayout.apartmentType].Contains(gameObject.name))
                    {
                        var points = Standards.TaggedObject.lineLoadStart[gameObject.name];

                        for (int i = 0; i < points.Length; i++)
                        {
                            var index = points[i][0];
                            var next = (index + 1) % polygon.Count;

                            float length = aptLayout.outterHeight;//Vector3.Distance(vertices[index], vertices[next]) + 0.02f;
                            GameObject obj = Instantiate(lineLoadPrefab, transform);
                            obj.GetComponent<LineLoad>().room = this;
                            if (points[i][1] == 0)
                            {
                                Vector3 position = new Vector3(transform.TransformPoint(vertices[index]).x, -1.0f, length * 0.25f);
                                obj.layer = 17;
                                obj.transform.position = position;
                                obj.transform.localScale = new Vector3(0.03f, 0.03f, length * 0.5f);
                            }
                            else
                            {
                                Vector3 position = new Vector3(transform.TransformPoint(vertices[index]).x, -1.0f, length * 0.75f);
                                obj.layer = 18;
                                obj.transform.position = position;
                                obj.transform.localScale = new Vector3(0.03f, 0.03f, length * 0.5f);
                            }
                            lineLoads.Add(obj);
                            aptLayout.AddLineLoad(obj, points[i][1]);
                        }
                    }

                    if (Standards.TaggedObject.ValidLineLoads.ContainsKey(aptLayout.apartmentType))
                    {
                        if (!Standards.TaggedObject.ValidLineLoads[aptLayout.apartmentType].Contains(name))
                        {
                            Standards.TaggedObject.ValidLineLoads[aptLayout.apartmentType].Add(name);
                        }
                    }
                    else
                    {
                        Standards.TaggedObject.ValidLineLoads.Add(aptLayout.apartmentType, new List<string>() { name });
                    }
                }
            }
        }

        private void UpdateGeometry()
        {
            List<Vector3> temp_verts = new List<Vector3>();
            for (int i = 0; i < vertices.Count; i++)
            {
                bool allowed = true;
                for (int j = 0; j < temp_verts.Count; j++)
                {
                    if (Vector3.Distance(vertices[i], temp_verts[j]) < 0.05f)
                    {
                        allowed = false;
                    }
                }
                if (allowed)
                {
                    temp_verts.Add(vertices[i]);
                }
            }

            bool isClockWise = Polygon.IsClockWise(temp_verts);

            for (int i = 0; i < temp_verts.Count; i++)
            {
                int next = (i + 1) % temp_verts.Count;
                int prev = i - 1;
                if (i - 1 == -1)
                {
                    prev = temp_verts.Count - 1;
                }
                Vector3 v1 = temp_verts[prev] - temp_verts[i];
                Vector3 v2 = temp_verts[next] - temp_verts[i];
                v1.Normalize();
                v2.Normalize();
                v1 *= (aptLayout.wallWidth / 2.0f);
                v2 *= (aptLayout.wallWidth / 2.0f);
                var cross = Vector3.Cross(v1, v2);
                var addition = (v1 + v2);
                if (Vector3.Angle(v1, v2) > 170)
                {
                    var c = Vector3.Cross(v2, Vector3.up);
                    temp_verts[i] = (temp_verts[i] + c.normalized * (aptLayout.wallWidth / 2.0f));
                }
                else
                {
                    if (cross.y < 0 == !isClockWise)
                    {
                        addition *= -1;
                    }
                    temp_verts[i] = (temp_verts[i] + addition);
                }
            }

            for (int i = 0; i < polygon.Count; i++)
            {
                polygon.transform.SetParent(null);
                polygon.transform.position = Vector3.zero;
                polygon[i].UpdatePosition(temp_verts[i]);
                polygon.transform.SetParent(transform);
                polygon.transform.localPosition = Vector3.zero;
            }

            polygon.SetVertexColors(color);
        }
        #endregion
    }
}

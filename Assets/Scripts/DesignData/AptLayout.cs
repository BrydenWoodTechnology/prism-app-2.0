﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace BrydenWoodUnity.DesignData
{
    /// <summary>
    /// An apartment layout Unity Scriptable Object
    /// </summary>
    [CreateAssetMenu(fileName ="AptLayout", menuName = "Design Data/AptLayout", order = 1)]
    public class AptLayout : ScriptableObject
    {
        #region Public Variables
        /// <summary>
        /// The type of the building this apartment layout belongs to
        /// </summary>
        public BuildingType buildingType;
        /// <summary>
        /// Whether the apartment layout is M3 compliant
        /// </summary>
        public bool m3;
        /// <summary>
        /// The type of the apartment layout
        /// </summary>
        public string aptType;
        /// <summary>
        /// The rooms it includes
        /// </summary>
        public bool[] roomInclusion;
        /// <summary>
        /// The names of the rooms it includes
        /// </summary>
        public string[] roomNames;
        /// <summary>
        /// The colours of the rooms it includes
        /// </summary>
        public Color[] roomColors;
        /// <summary>
        /// The types of the rooms it includes
        /// </summary>
        public string[] types;
        /// <summary>
        /// The text asset with the serialization of the rooms
        /// </summary>
        public TextAsset roomData;
        #endregion
    }
}

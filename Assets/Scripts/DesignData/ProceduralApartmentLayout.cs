﻿using BrydenWoodUnity.GeometryManipulation;
using BrydenWoodUnity.UIElements;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using BWT = BrydenWoodUnity.GeometryManipulation.ThreeJs;

namespace BrydenWoodUnity.DesignData
{
    /// <summary>
    /// A Unity Event class for the change of the layout
    /// </summary>
    [System.Serializable]
    public class LayoutChanged : UnityEvent<ProceduralApartmentLayout>
    {

    }

    /// <summary>
    /// A MonoBehaviour component for the Procedural Apartment Layout
    /// </summary>
    public class ProceduralApartmentLayout : MonoBehaviour
    {
        #region Public Properties
        public BuildingType buildingType = BuildingType.Linear;
        public Text roomIssue;
        public GameObject proceduralRoomPrefab;
        public GameObject maxLayoutPrefab;
        public GameObject minLayoutPrefab;
        public GameObject minMaxRenderer;
        public Transform roomNamesParent;
        public Transform amenitiesButtonsParent;
        public Text viewSubTitle;
        public SelectedInfoDisplay selectedInfoDisplay;
        public Vector3 origin;
        public float wallWidth = 0.15f;
        public float width = 8f;
        public float height = 6.8f;

        public float partyWall = 0.3f;
        public float exteriorWall = 0.45f;
        public float corridorWall = 0.2f;
        public string content;
        public float modulesScale = 1f;

        public string apartmentType = "1b1p";

        public bool initializeOnStart = false;
        public Gradient panelsGradient;
        public float totalArea;
        public float NIA;

        public bool hasLeft = true;
        public bool hasRight = true;
        public bool m3;
        private bool singleLoaded;
        public bool SingleLoaded
        {
            get
            {
                return singleLoaded;
            }
            set
            {
                if (buildingType == BuildingType.Mansion) singleLoaded = false;
                else singleLoaded = value;
            }
        }
        public bool isInBuilding { get; private set; }
        public AptLayout aptLayout;
        public Line hallEntrance;
        //[Space(10)]
        //[Header("Rooms included:")]
        //public bool[] roomInclusion;// = new bool[24];
        //public string[] roomNames;

        //public List<Color> roomColors;
        //public List<string> types;
        public float maxPanelWidth
        {
            get
            {
                return Standards.TaggedObject.ConstructionFeatures["PanelLength"];
            }
        }
        public List<ProceduralRoom> rooms { get; set; }
        public List<GameObject> verticalModules { get; set; }
        public List<GameObject> panels { get; set; }
        public Transform verticalModulesParent;
        public Transform panelsParent;
        public Transform roomParent;
        public Transform lineLoadsParent;
        [HideInInspector]
        public float totalHeight = 3;
        public ManufacturingSystem manufacturingSystem { get; set; }
        public bool readOnly { get; set; }
        public List<List<float>> lengths { get; set; }
        public List<List<float>> originalLengths { get; set; }
        public bool hasCorridor { get; set; }
        public float corridorWidth { get; set; }

        public float area { get; set; }
        public bool issue;
        public float outterHeight { get; set; }
        public float outterWidth { get; set; }
        public float maxOutterWidth { get; set; }
        public float minOutterWidth { get; set; }
        public double AmenitySpace
        {
            get
            {
                double area = 0;

                if (rooms != null && rooms.Count > 0)
                {
                    for (int i = 0; i < rooms.Count; i++)
                    {
                        if (rooms[i].name == "AILR" || rooms[i].name == "AILRR" || rooms[i].name == "ALRR" || rooms[i].name == "ALR")
                        {
                            area += rooms[i].polygon.Area;
                        }
                    }
                }

                return area;
            }
        }

        public List<GameObject> corridorLineLoads;
        public List<GameObject> facadeLineLoads;

        public float dbVal { get { return m3 ? Standards.TaggedObject.customValsM3[apartmentType][0] : Standards.TaggedObject.customVals[apartmentType][0]; } }
        public float pbVal { get { return m3 ? Standards.TaggedObject.customValsM3[apartmentType][1] : Standards.TaggedObject.customVals[apartmentType][1]; } }
        public float baVal { get { return m3 ? Standards.TaggedObject.customValsM3[apartmentType][2] : Standards.TaggedObject.customVals[apartmentType][2]; } }
        public float baaVal { get { return m3 ? Standards.TaggedObject.customValsM3[apartmentType][3] : Standards.TaggedObject.customVals[apartmentType][3]; } }
        public float wcVal { get { return m3 ? Standards.TaggedObject.customValsM3[apartmentType][4] : Standards.TaggedObject.customVals[apartmentType][4]; } }
        public float wccVal { get { return m3 ? Standards.TaggedObject.customValsM3[apartmentType][5] : Standards.TaggedObject.customVals[apartmentType][5]; } }
        public float dbbVal { get { return m3 ? Standards.TaggedObject.customValsM3[apartmentType][6] : Standards.TaggedObject.customVals[apartmentType][6]; } }
        public float pbbVal { get { return m3 ? Standards.TaggedObject.customValsM3[apartmentType][7] : Standards.TaggedObject.customVals[apartmentType][7]; } }
        public float psbVal { get { return m3 ? Standards.TaggedObject.customValsM3[apartmentType][8] : Standards.TaggedObject.customVals[apartmentType][8]; } }
        //public float wdbVal1 { get { return m3 ? Standards.TaggedObject.customValsM3[apartmentType][9] : Standards.TaggedObject.customVals[apartmentType][9]; } }
        //public float wdbVal2 { get { return m3 ? Standards.TaggedObject.customValsM3[apartmentType][10] : Standards.TaggedObject.customVals[apartmentType][10]; } }
        public float hwVal { get { return m3 ? Standards.TaggedObject.customValsM3[apartmentType][9] : Standards.TaggedObject.customVals[apartmentType][9]; } }

        public float minAmenity { get { return Standards.TaggedObject.MinimumAmenitiesArea[apartmentType] + 1; } }
        public GameObject frontWindowsCrop { get; private set; }
        public List<BWT.Line> lines;
        public GameObject windowsParent;
        #endregion

        #region Private Properties
        private Dictionary<string, List<string>> edges { get; set; }
        private List<string> origins { get; set; }
        private float m_width { get; set; }
        private float m_height { get; set; }
        private LineRenderer outline { get; set; }
        private GameObject maxLayout { get; set; }
        private GameObject minLayout { get; set; }
        #endregion

        #region Events

        #endregion

        #region MonoBehaviour Methods

        // Use this for initialization
        void Start()
        {
            if (initializeOnStart)
            {
                if (aptLayout.buildingType == BuildingType.Mansion)
                {
                    Initialize(PrepareAptLayoutData(aptLayout.roomData));//roomData.text);
                }
                else
                {
                    Initialize(aptLayout.roomData);
                }
            }
            modulesScale = 1;
        }

        // Update is called once per frame
        void Update()
        {
#if UNITY_EDITOR
            if (Input.GetKeyDown("a"))
            {
                CreateApartmentLayoutObject();
            }
#endif
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Initializes the Instance
        /// </summary>
        /// <param name="content">The txt content to be used for initialization</param>
        /// <param name="isExt">Whether the apartment is on an external corner</param>
        /// <param name="getMax">Whether it should show the maximum and minimum areas of the apartment layout</param>
        public void Initialize(string content, bool isExt = true, bool getMax = false)
        {
            this.content = content;

            partyWall = Standards.TaggedObject.ConstructionFeatures["PartyWall"];
            exteriorWall = Standards.TaggedObject.ConstructionFeatures["ExteriorWall"];
            corridorWall = Standards.TaggedObject.ConstructionFeatures["CorridorWall"];

            isInBuilding = !getMax;
            GetEdgesFromContent();
            GetLengthsFromEdges();


            ResetLineLoads();

            GenerateRooms();
            GenerateVolumetricModules();
            GeneratePanels();

            if (getMax)
            {
                if (viewSubTitle != null) viewSubTitle.text = "Total area = " + Math.Round(NIA) + "m\xB2";
                GenerateMaxLayout();
                GenerateMinimumLayout();
                if (selectedInfoDisplay != null)
                {
                    selectedInfoDisplay.SetSelected(this);
                }
            }
            transform.SetLayerToChildren(16);
            if (selectedInfoDisplay != null)
            {
                selectedInfoDisplay.SetSelected(this);
            }

            EvaluateLineLoads();
            Standards.TaggedObject.EvaluateStandardRooms();
            Standards.TaggedObject.EvaluateAmenitiesArea();
            Standards.TaggedObject.EvaluateStructuralWalls();
        }

        /// <summary>
        /// Returns the information of the apartment layout
        /// </summary>
        /// <returns>Array of Strings</returns>
        public string[] GetInfoText()
        {
            string title = string.Format("{0}\r\n", apartmentType);

            string info = "";
            info += string.Format("Apartment id = {0}\r\n", "N/A");
            info += string.Format("Apartment type = {0}\r\n", apartmentType);
            info += string.Format("Apartment area = {0}m\xB2\r\n", Math.Round(NIA));
            int num = 0;
            if (apartmentType != "Studio")
            {
                num = (int)Char.GetNumericValue(apartmentType[apartmentType.Length - 2]);
            }
            else
            {
                num = 2;
            }
            info += string.Format("Number of people = {0}\r\n", num);

            return new string[2] { title, info };
        }

        /// <summary>
        /// Resets the line-loads of the apartment layout
        /// </summary>
        public void ResetLineLoads()
        {
            if (facadeLineLoads != null)
            {
                for (int i = 0; i < facadeLineLoads.Count; i++)
                {
                    if (facadeLineLoads[i] != null)
                        DestroyImmediate(facadeLineLoads[i]);
                }
                facadeLineLoads = new List<GameObject>();
            }

            if (corridorLineLoads != null)
            {
                for (int i = 0; i < corridorLineLoads.Count; i++)
                {
                    if (corridorLineLoads[i] != null)
                        DestroyImmediate(corridorLineLoads[i]);
                }
                corridorLineLoads = new List<GameObject>();
            }

            GetApartmentLineLoads();
            lineLoadsParent.gameObject.SetActive(manufacturingSystem == ManufacturingSystem.LineLoads);
        }

        /// <summary>
        /// Adds a line-load to the apartment layout
        /// </summary>
        /// <param name="lineLoad">The line-load to be added</param>
        /// <param name="side">Which side the line load is on (corridor or facade)</param>
        public void AddLineLoad(GameObject lineLoad, int side)
        {
            if (facadeLineLoads == null)
            {
                facadeLineLoads = new List<GameObject>();
            }

            if (corridorLineLoads == null)
            {
                corridorLineLoads = new List<GameObject>();
            }

            if (side == 1)
                facadeLineLoads.Add(lineLoad);
            else
                corridorLineLoads.Add(lineLoad);

            facadeLineLoads = facadeLineLoads.OrderBy(x => transform.InverseTransformPoint(x.transform.position).x).ToList();
            corridorLineLoads = corridorLineLoads.OrderBy(x => transform.InverseTransformPoint(x.transform.position).x).ToList();
        }

        /// <summary>
        /// Generates the line-loads for the apartments
        /// </summary>
        public void GetApartmentLineLoads()
        {
            if (lineLoadsParent != null)
            {
                DestroyImmediate(lineLoadsParent.gameObject);
            }
            lineLoadsParent = new GameObject("LineLoadsParent").transform;
            lineLoadsParent.SetParent(transform);
            lineLoadsParent.localPosition = Vector3.zero;
            lineLoadsParent.localEulerAngles = Vector3.zero;
            lineLoadsParent.localScale = Vector3.one;

            if (buildingType != BuildingType.Mansion)
            {
                GameObject obj = Instantiate(proceduralRoomPrefab.GetComponent<ProceduralRoom>().lineLoadPrefab, lineLoadsParent);
                obj.GetComponent<LineLoad>().isFixed = true;
                obj.GetComponent<LineLoad>().allignMaterial = obj.GetComponent<LineLoad>().green;
                obj.GetComponent<MeshRenderer>().material = obj.GetComponent<LineLoad>().green;
                obj.GetComponent<LineLoad>().issue = false;
                obj.transform.localPosition = new Vector3(wallWidth * 0.5f, -1.0f, outterHeight * 0.5f);
                obj.transform.localScale = new Vector3(0.03f, 0.03f, outterHeight);
                GameObject obj2 = Instantiate(proceduralRoomPrefab.GetComponent<ProceduralRoom>().lineLoadPrefab, lineLoadsParent);
                obj2.GetComponent<LineLoad>().isFixed = true;
                obj2.GetComponent<LineLoad>().allignMaterial = obj2.GetComponent<LineLoad>().green;
                obj2.GetComponent<MeshRenderer>().material = obj2.GetComponent<LineLoad>().green;
                obj2.GetComponent<LineLoad>().issue = false;
                obj2.transform.localPosition = new Vector3(outterWidth - wallWidth * 0.5f, -1.0f, outterHeight * 0.5f);
                obj2.transform.localScale = new Vector3(0.03f, 0.03f, outterHeight);
            }
            else
            {
                GetMansionLineLoads();
            }
        }

        /// <summary>
        /// Toggles the minimum and maximum apartment outlines
        /// </summary>
        /// <param name="show">Whether it should show the outlines</param>
        public void ToggleMaxLayout(bool show)
        {
            if (maxLayout != null) maxLayout.gameObject.SetActive(show);
            if (minLayout != null) minLayout.gameObject.SetActive(show);
        }

        /// <summary>
        /// Evaluates the line-loads
        /// </summary>
        public void EvaluateLineLoads()
        {
            for (int i = 0; i < facadeLineLoads.Count; i++)
            {
                var xVal = transform.InverseTransformPoint(facadeLineLoads[i].transform.position).x;
                var maxSpan = Standards.TaggedObject.ConstructionFeatures["LineSpan"];
                if (i == 0)
                {
                    if (xVal > maxSpan) facadeLineLoads[i].GetComponent<LineLoad>().MarkAsRed(true);
                    else facadeLineLoads[i].GetComponent<LineLoad>().MarkAsRed(false);
                }
                else
                {
                    var prevXVal = transform.InverseTransformPoint(facadeLineLoads[i - 1].transform.position).x;
                    if ((xVal - prevXVal) > maxSpan)
                    {
                        facadeLineLoads[i].GetComponent<LineLoad>().MarkAsRed(true);
                    }
                    else
                    {
                        facadeLineLoads[i].GetComponent<LineLoad>().MarkAsRed(false);
                    }
                }

                if (i == facadeLineLoads.Count - 1)
                {
                    if (outterWidth - xVal > maxSpan)
                        facadeLineLoads[i].GetComponent<LineLoad>().MarkAsRed(true);
                    else
                        facadeLineLoads[i].GetComponent<LineLoad>().MarkAsRed(false);
                }
            }

            for (int i = 0; i < corridorLineLoads.Count; i++)
            {
                var xVal = transform.InverseTransformPoint(corridorLineLoads[i].transform.position).x;
                var maxSpan = Standards.TaggedObject.ConstructionFeatures["LineSpan"];
                if (i == 0)
                {
                    if (xVal > maxSpan) corridorLineLoads[i].GetComponent<LineLoad>().MarkAsRed(true);
                    else corridorLineLoads[i].GetComponent<LineLoad>().MarkAsRed(false);
                }
                else
                {
                    var prevXVal = transform.InverseTransformPoint(corridorLineLoads[i - 1].transform.position).x;
                    if ((xVal - prevXVal) > maxSpan)
                    {
                        corridorLineLoads[i].GetComponent<LineLoad>().MarkAsRed(true);
                    }
                    else
                    {
                        corridorLineLoads[i].GetComponent<LineLoad>().MarkAsRed(false);
                    }
                }

                if (i == corridorLineLoads.Count - 1)
                {
                    if (outterWidth - xVal > maxSpan)
                        corridorLineLoads[i].GetComponent<LineLoad>().MarkAsRed(true);
                    else
                        corridorLineLoads[i].GetComponent<LineLoad>().MarkAsRed(false);
                }
            }
        }

        /// <summary>
        /// Initializes the Instance
        /// </summary>
        /// <param name="showMinMax">Whether the maximum and minimum areas should be visualized</param>
        /// <param name="isExt">Whether the apartment is on an external corner</param>
        public void Initialize(bool showMinMax = true, bool isExt = true)
        {
            if (buildingType != BuildingType.Mansion)
                Initialize(aptLayout.roomData.text, isExt, showMinMax);
            else
                Initialize(PrepareAptLayoutData(aptLayout.roomData), isExt, showMinMax);

        }

        /// <summary>
        /// Re-Initializes the Instance
        /// </summary>
        public void ReInitialize()
        {
            GenerateRooms();
            GenerateVolumetricModules();
            GeneratePanels();
            transform.SetLayerToChildren(16);
        }

        /// <summary>
        /// Sets the line loads for the apartment layout
        /// </summary>
        public void SetLineLoads()
        {
            GetApartmentLineLoads();
            for (int i = 0; i < lineLoadsParent.childCount; i++)
            {
                lineLoadsParent.GetChild(i).localScale = new Vector3(lineLoadsParent.GetChild(i).localScale.x, totalHeight, lineLoadsParent.GetChild(i).localScale.z);
                lineLoadsParent.GetChild(i).localPosition = new Vector3(lineLoadsParent.GetChild(i).localPosition.x, totalHeight * 0.5f, lineLoadsParent.GetChild(i).localPosition.z);
            }

            if (buildingType != BuildingType.Mansion)
            {
                List<string> lineLoadRooms;
                if (Standards.TaggedObject.ValidLineLoads.TryGetValue(apartmentType, out lineLoadRooms))
                {
                    for (int i = 0; i < lineLoadRooms.Count; i++)
                    {
                        for (int j = 0; j < rooms.Count; j++)
                        {
                            if (rooms[j].gameObject.name == lineLoadRooms[i])
                            {
                                var loads = rooms[j].GetLineLoads(totalHeight);
                                for (int k = 0; k < loads.Count; k++)
                                {
                                    loads[k].transform.SetParent(lineLoadsParent);
                                }
                            }
                        }
                    }
                    EvaluateLineLoads();
                }
            }
            else
            {

            }
        }

        /// <summary>
        /// Gets the Line Loads for the apartment layout
        /// </summary>
        public void GetLineLoads()
        {
            if (lineLoadsParent == null)
            {
                lineLoadsParent = new GameObject("LineLoadsParent").transform;
                lineLoadsParent.SetParent(transform);
                lineLoadsParent.localPosition = Vector3.zero;
                lineLoadsParent.localEulerAngles = Vector3.zero;
                lineLoadsParent.localScale = Vector3.one;
            }
            for (int i = 0; i < rooms.Count; i++)
            {
                if (buildingType != BuildingType.Mansion)
                {
                    var loads = rooms[i].GetLineLoads();
                    for (int j = 0; j < loads.Count; j++)
                    {
                        loads[j].transform.localScale = new Vector3(loads[j].transform.localScale.x, totalHeight, loads[j].transform.localScale.z);
                        loads[j].transform.localPosition = new Vector3(loads[j].transform.localPosition.x, totalHeight / 2.0f, loads[j].transform.localPosition.z);
                        loads[j].transform.SetParent(lineLoadsParent);
                    }
                }
            }
        }

        /// <summary>
        /// Turns the Line Loads on or off
        /// </summary>
        /// <param name="show">Toggle</param>
        public void ToggleLineLoads(bool show)
        {
            for (int i = 0; i < rooms.Count; i++)
            {
                rooms[i].ToggleLineLoads(show);
            }
        }

        /// <summary>
        /// Sets the manufacturing system
        /// </summary>
        /// <param name="manufacturingSystem">The new manufacturing system</param>
        public void SetManufacturingSystem(ManufacturingSystem manufacturingSystem)
        {
            this.manufacturingSystem = manufacturingSystem;
            switch (manufacturingSystem)
            {
                case ManufacturingSystem.Off:
                    verticalModulesParent.gameObject.SetActive(false);
                    panelsParent.gameObject.SetActive(false);
                    lineLoadsParent.gameObject.SetActive(false);
                    break;
                case ManufacturingSystem.VolumetricModules:
                    verticalModulesParent.gameObject.SetActive(true);
                    panelsParent.gameObject.SetActive(false);
                    lineLoadsParent.gameObject.SetActive(false);
                    break;
                case ManufacturingSystem.Panels:
                    verticalModulesParent.gameObject.SetActive(false);
                    panelsParent.gameObject.SetActive(true);
                    lineLoadsParent.gameObject.SetActive(false);
                    break;
                case ManufacturingSystem.LineLoads:
                    verticalModulesParent.gameObject.SetActive(false);
                    panelsParent.gameObject.SetActive(false);
                    lineLoadsParent.gameObject.SetActive(true);
                    break;
                case ManufacturingSystem.Platforms:
                    verticalModulesParent.gameObject.SetActive(false);
                    panelsParent.gameObject.SetActive(false);
                    lineLoadsParent.gameObject.SetActive(false);                   
                    break;
            }
            ToggleLineLoads(manufacturingSystem == ManufacturingSystem.LineLoads);
        }

        /// <summary>
        /// Toggles through the manufacturing systems
        /// </summary>
        public void ToggleManufacturingSystem()
        {
            switch (manufacturingSystem)
            {
                case ManufacturingSystem.Off:
                    verticalModulesParent.gameObject.SetActive(false);
                    panelsParent.gameObject.SetActive(false);
                    break;
                case ManufacturingSystem.VolumetricModules:
                    verticalModulesParent.gameObject.SetActive(true);
                    panelsParent.gameObject.SetActive(false);
                    break;
                case ManufacturingSystem.Panels:
                    verticalModulesParent.gameObject.SetActive(false);
                    panelsParent.gameObject.SetActive(true);
                    break;
                case ManufacturingSystem.LineLoads:
                    verticalModulesParent.gameObject.SetActive(false);
                    panelsParent.gameObject.SetActive(false);
                    break;
            }
            ToggleLineLoads(manufacturingSystem == ManufacturingSystem.LineLoads);
        }

        /// <summary>
        /// Returns the Three.Js representation of the apartment layout
        /// </summary>
        /// <returns>Three.Js Object 3D</returns>
        public BWT.Object3d GetThreeJsObjects()
        {
            BWT.Object3d object3D = new BWT.Object3d();



            return object3D;
        }

        /// <summary>
        /// Resets the apartment layout
        /// </summary>
        /// <returns>IEnumerator</returns>
        public IEnumerator ResetLayout()
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                Destroy(transform.GetChild(i).gameObject);
            }
            ShowOutline(false);
            ResetLineLoads();
            yield return new WaitForEndOfFrame();
        }

        /// <summary>
        /// Updates the amenities of the apartment layout
        /// </summary>
        /// <param name="roomId">The unique id of the room</param>
        /// <param name="length">The length of the room</param>
        public void UpdateAmenities(int roomId, float length)
        {
            StartCoroutine(UpdateBalconiesCoroutine(roomId, length));
        }

        /// <summary>
        /// Updates the amenities of the Mansion BLock apartment layout
        /// </summary>
        /// <param name="roomId">The unique id of the room</param>
        public void UpdateMansionAmenities(int roomId)
        {
            StartCoroutine(UpdateMansionBalconiesCoroutine(roomId));
        }

        /// <summary>
        /// Sets the total width of the apartment layout
        /// </summary>
        /// <param name="width">The total width</param>
        public void SetWidth(float width)
        {
            this.width = width;
            GetEdgesFromContent();
            GetLengthsFromEdges();
            UpdateRooms();
        }

        /// <summary>
        /// Sets the total depth of the apartment
        /// </summary>
        /// <param name="depth">The total depth</param>
        public void SetDepth(float depth)
        {
            this.height = depth;
            GetEdgesFromContent();
            GetLengthsFromEdges();
            UpdateRooms();
        }

        /// <summary>
        /// Sets the width of the interior walls
        /// </summary>
        /// <param name="wallWidth">The width of the interior walls</param>
        public void SetWall(float wallWidth)
        {
            this.wallWidth = wallWidth;
            GetEdgesFromContent();
            GetLengthsFromEdges();
            UpdateRooms();
        }

        /// <summary>
        /// Sets the rooms of the apartment layout
        /// </summary>
        /// <param name="roomBools">A list of which rooms should be included</param>
        public void SetRooms(bool[] roomBools)
        {
            //roomInclusion = roomBools;
            //for (int i = 0; i < roomInclusion.Length; i++)
            //{
            //    roomInclusion[i] = roomBools[i];
            //}
        }

        /// <summary>
        /// Sets the list of rooms for the apartment layout
        /// </summary>
        /// <param name="aptLayout">The apartment layout asset from which to get the list of rooms</param>
        public void SetRooms(AptLayout aptLayout)
        {
            this.aptLayout = aptLayout;
        }

        /// <summary>
        /// Sets the rooms of the apartment layout
        /// </summary>
        /// <param name="has">Whether the layout has rooms or not</param>
        public void SetRooms(bool has)
        {
            //for (int i = 0; i < roomInclusion.Length; i++)
            //{
            //    roomInclusion[i] = has;
            //}
        }

        /// <summary>
        /// Returns the GameObject with the combined meshes of the rooms
        /// </summary>
        /// <param name="type">The type of the apartment layout</param>
        /// <returns>GameObject</returns>
        public GameObject GetCombinedMesh(string type)
        {
            GameObject gObj = new GameObject(type);
            gObj.AddComponent<MeshFilter>();
            gObj.AddComponent<MeshRenderer>().material = Resources.Load("Materials/VertexLit") as Material;

            Mesh m = new Mesh();
            m.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;

            List<CombineInstance> instances = new List<CombineInstance>();

            for (int i = 0; i < rooms.Count; i++)
            {
                instances.Add(new CombineInstance() { mesh = rooms[i].polygon.GetComponent<MeshFilter>().sharedMesh, transform = rooms[i].transform.localToWorldMatrix });
                instances.Add(new CombineInstance() { mesh = rooms[i].polygon.extrusion.GetComponent<MeshFilter>().sharedMesh, transform = rooms[i].transform.localToWorldMatrix });
            }

            m.CombineMeshes(instances.ToArray());
            m.RecalculateNormals();
            m.RecalculateBounds();

            return gObj;
        }

        /// <summary>
        /// Combines the meshes of the rooms
        /// </summary>
        /// <param name="type">The type of the apartment layout</param>
        public void CombineRoomsMeshes(string type)
        {
            for (int i = 0; i < roomParent.childCount; i++)
            {
                Destroy(roomParent.GetChild(i).gameObject);
            }

            if (windowsParent != null)
            {
                DestroyImmediate(windowsParent);
                windowsParent = null;
            }
            windowsParent = new GameObject("WindowsParent");
            windowsParent.transform.SetParent(transform);
            windowsParent.transform.localPosition = Vector3.zero;
            windowsParent.transform.localEulerAngles = Vector3.zero;

            GameObject gObj = new GameObject(type);
            gObj.transform.SetParent(transform);
            gObj.AddComponent<MeshFilter>();
            gObj.AddComponent<MeshRenderer>().material = Resources.Load("Materials/VertexLit") as Material;

            Mesh m = new Mesh();
            m.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;

            List<CombineInstance> instances = new List<CombineInstance>();

            for (int i = 0; i < rooms.Count; i++)
            {
                instances.Add(new CombineInstance() { mesh = rooms[i].polygon.GetComponent<MeshFilter>().sharedMesh, transform = rooms[i].transform.localToWorldMatrix });
                instances.Add(new CombineInstance() { mesh = rooms[i].polygon.extrusion.GetComponent<MeshFilter>().sharedMesh, transform = rooms[i].transform.localToWorldMatrix });
                if (rooms[i].window != null)
                {
                    //instances.Add(new CombineInstance() { mesh = rooms[i].window.GetComponent<MeshFilter>().sharedMesh, transform = rooms[i].window.transform.localToWorldMatrix });
                    rooms[i].window.transform.SetParent(windowsParent.transform);
                    rooms[i].window.AddComponent<BoxCollider>();
                }
                if (rooms[i].backWindow != null)
                {
                    rooms[i].backWindow.transform.SetParent(windowsParent.transform);
                    rooms[i].backWindow.AddComponent<BoxCollider>();
                }
            }

            m.CombineMeshes(instances.ToArray());
            m.RecalculateNormals();
            m.RecalculateBounds();
            m.UploadMeshData(true);

            gObj.GetComponent<MeshFilter>().sharedMesh = m;

            gObj.transform.localEulerAngles = Vector3.zero;
            gObj.transform.localPosition = Vector3.zero;
        }

        /// <summary>
        /// Checks for duplicate lines for the Three.Js object
        /// </summary>
        public void CheckDuplicateLines()
        {
            try
            {
                List<BWT.Line> tempLines = new List<BWT.Line>();

                List<List<double>> vertices = new List<List<double>>();
                for (int i = 0; i < lines.Count; i++)
                {
                    List<double> verts = new List<double>();
                    verts.Add(lines[i]._geometry.data.vertices[0]);
                    verts.Add(lines[i]._geometry.data.vertices[1]);
                    verts.Add(lines[i]._geometry.data.vertices[2]);
                    verts.Add(lines[i]._geometry.data.vertices[3]);
                    verts.Add(lines[i]._geometry.data.vertices[4]);
                    verts.Add(lines[i]._geometry.data.vertices[5]);

                    if (!vertices.Contains(verts))
                    {
                        tempLines.Add(lines[i]);
                        vertices.Add(verts);
                    }
                }
                lines = GetWallLines(tempLines);
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }
        }

        /// <summary>
        /// Join lines in vertical and horizontal directions
        /// </summary>
        /// <param name="incomingLines"></param>
        /// <returns></returns>
        public List<BWT.Line> GetWallLines(List<BWT.Line> incomingLines)
        {
            //Debug.Log(incomingLines.Count);
            //get start end end point of each line
            List<Vector3> startPts = new List<Vector3>();
            List<Vector3> endPts = new List<Vector3>();
            for (int i = 0; i < incomingLines.Count; i++)
            {
                var verts = lines[i]._geometry.data.vertices;
                startPts.Add(new Vector3((float)verts[0], (float)verts[1], (float)verts[2]));
                endPts.Add(new Vector3((float)verts[3], (float)verts[4], (float)verts[5]));
                //Debug.DrawLine(new Vector3((float)verts[0], (float)verts[1], (float)verts[2]), new Vector3((float)verts[3], (float)verts[4], (float)verts[5]), Color.yellow, 5f);
            }

            //Get vectors parallel to x and y axis
            List<List<Vector3>> pointsX = new List<List<Vector3>>();
            List<List<Vector3>> pointsY = new List<List<Vector3>>();
            for (int i = 0; i < startPts.Count; i++)
            {
                Vector3 v = endPts[i] - startPts[i];
                System.Numerics.Vector3 cross = System.Numerics.Vector3.Cross(System.Numerics.Vector3.UnitX, new System.Numerics.Vector3(v.x, v.y, v.z));
                if (cross.Length() == 0)
                {
                    List<Vector3> line = new List<Vector3>();
                    line.Add(startPts[i]);
                    line.Add(endPts[i]);
                    line.OrderBy(c => c.x);
                    pointsX.Add(line);
                    // Debug.DrawLine(startPts[i], endPts[i], Color.cyan, 5f);
                }
                else
                {
                    cross = System.Numerics.Vector3.Cross(System.Numerics.Vector3.UnitZ, new System.Numerics.Vector3(v.x, v.y, v.z));
                    if (cross.Length() == 0)
                    {
                        List<Vector3> line = new List<Vector3>();
                        line.Add(startPts[i]);
                        line.Add(endPts[i]);
                        line.OrderBy(c => c.z);
                        pointsY.Add(line);
                        // Debug.DrawLine(startPts[i], endPts[i], Color.magenta, 5f);
                    }
                    else
                    {
                        Debug.Log("invalid Line");
                    }
                }
            }

            pointsX.OrderBy(list => list[0].x);
            pointsY.OrderBy(list => list[0].z);

            //continuous lines
            List<BWT.Line> groups = new List<BWT.Line>();

            GroupIntersectingLines(pointsX, groups, 0, 2);
            GroupIntersectingLines(pointsY, groups, 2, 0);

            return groups;
        }

        /// <summary>
        /// Group colinear and continuous points 
        /// </summary>
        /// <param name="points"></param>
        /// <param name="groups"></param>
        /// <param name="alignmentAxis"></param>
        /// <param name="verticalAxis"></param>
        public void GroupIntersectingLines(List<List<Vector3>> points, List<BWT.Line> groups, int alignmentAxis, int verticalAxis)
        {
            //get distinct values on vertical axis
            List<float> distinctvalues = new List<float>();
            for (int i = 0; i < points.Count; i++)
            {
                for (int j = 0; j < points[i].Count; j++)
                {
                    if (!distinctvalues.Contains(points[i][j][verticalAxis]))
                    {
                        distinctvalues.Add(points[i][j][verticalAxis]);
                    }
                }
            }

            //group colinear points
            for (int i = 0; i < distinctvalues.Count; i++)
            {
                List<Vector3> group = new List<Vector3>();
                for (int j = 0; j < points.Count; j++)
                {
                    if (points[j][0][verticalAxis] == distinctvalues[i])
                    {
                        if (group.Count > 0)
                        {
                            //continuous lines
                            if (points[j][0][alignmentAxis] >= group[0][alignmentAxis] && points[j][0][alignmentAxis] <= group[group.Count - 1][alignmentAxis])
                            {
                                group.Add(points[j][0]);
                                group.Add(points[j][1]);
                            }
                            else
                            {
                                group.OrderBy(c => c[alignmentAxis]);
                                BWT.Line lineVt = new BWT.Line();
                                lineVt.type = "Line";
                                lineVt._geometry = new BWT.Geometry();
                                lineVt._geometry.data.vertices = new double[6]
                                {
                                    group[0].x,group[0].y,group[0].z,
                                    group[group.Count - 1].x,group[group.Count - 1].y,group[group.Count - 1].z
                                };
                                if (lineVt._geometry.data.vertices.Length == 0)
                                {
                                    Debug.Log("empty line!!!");
                                }
                                groups.Add(lineVt);

                                group = new List<Vector3>();

                                group.Add(points[j][0]);
                                group.Add(points[j][1]);
                            }
                        }
                        else
                        {
                            group.Add(points[j][0]);
                            group.Add(points[j][1]);
                        }
                        group.OrderBy(c => c[alignmentAxis]);
                    }
                }
                if (group.Count > 0)
                {
                    group.OrderBy(c => c[alignmentAxis]);
                    BWT.Line lineV = new BWT.Line();
                    lineV.type = "Line";
                    lineV._geometry = new BWT.Geometry();
                    lineV._geometry.data.vertices = new double[6]
                    {
                    group[0].x,group[0].y,group[0].z,
                    group[group.Count - 1].x,group[group.Count - 1].y,group[group.Count - 1].z
                    };

                    if (lineV._geometry.data.vertices.Length == 0)
                    {
                        Debug.Log("empty line!!!");
                    }
                    group = new List<Vector3>();
                    groups.Add(lineV);
                }
            }
        }

        /// <summary>
        /// Updates the Three.Js line coordinates when the apartment is placed in the building
        /// </summary>
        /// <param name="buildingIndex">The index of the building</param>
        /// <param name="floorIndex">The index of the floor</param>
        public void UpdateLinesCoords(int buildingIndex, int floorIndex)
        {
            for (int i = 0; i < lines.Count; i++)
            {
                var verts = lines[i]._geometry.data.vertices;
                if (verts.Length < 6)
                {
                    //Debug.Log(verts.Length);
                    return;
                }

                Vector3 localStart = new Vector3((float)verts[0], (float)verts[1], (float)verts[2]);
                Vector3 localEnd = new Vector3((float)verts[3], (float)verts[4], (float)verts[5]);
                localStart = transform.TransformPoint(localStart);
                localEnd = transform.TransformPoint(localEnd);
                lines[i]._geometry.data.vertices = new double[6]
                {
                    localStart.x,localStart.y,localStart.z,
                    localEnd.x,localEnd.y,localEnd.z
                };
                lines[i]._geometry.uuid = Guid.NewGuid().ToString();
                lines[i].geometry = lines[i]._geometry.uuid;
                lines[i].userData.Add("buildingIndex", buildingIndex.ToString());
                lines[i].userData.Add("floorIndex", floorIndex.ToString());
                lines[i].userData.Add("extrusionHeight", totalHeight.ToString());
            }
        }

        /// <summary>
        /// Toggles the outline of the apartment
        /// </summary>
        /// <param name="show">Toggle</param>
        public void ShowOutline(bool show)
        {
            if (show)
            {
                if (outline == null)
                {
                    outline = new GameObject("Outline").AddComponent<LineRenderer>();
                    outline.gameObject.layer = gameObject.layer;
                    outline.transform.SetParent(transform);
                    outline.transform.localPosition = Vector3.zero;
                    outline.transform.localEulerAngles = Vector3.zero;
                    outline.material = new Material(Shader.Find("Standard"));
                    outline.material.color = Color.black;
                    outline.loop = true;
                    outline.endWidth = 0.03f;
                    outline.startWidth = 0.03f;
                }

                var sideOffset = (partyWall - wallWidth);
                var topOffset = (exteriorWall) + (corridorWall) - wallWidth;
                outline.useWorldSpace = false;
                outline.positionCount = 5;
                outline.SetPosition(0, new Vector3(0, 0, 0));
                outline.SetPosition(1, new Vector3(0, 0, outterHeight));
                outline.SetPosition(2, new Vector3(outterWidth, 0, outterHeight));
                outline.SetPosition(3, new Vector3(outterWidth, 0, 0));
                outline.SetPosition(4, new Vector3(0, 0, 0));
            }
            else
            {
                if (outline != null)
                {
                    DestroyImmediate(outline.gameObject);
                    outline = null;
                }
            }
        }

        /// <summary>
        /// Checks if there is an issue with the Line Load panels
        /// </summary>
        /// <returns>Boolean</returns>
        public bool CheckIfPanelIssues()
        {
            int counter = 0;
            if (buildingType != BuildingType.Mansion)
            {
                for (int i = 0; i < rooms.Count; i++)
                {
                    if (rooms[i].CheckIfPanelIssues())
                    {
                        counter++;
                    }
                }
            }
            return counter != 0;
        }

        /// <summary>
        /// Checks if there are collision between the elements of the apartment and the platforms system
        /// </summary>
        /// <param name="framesParent">The parent of the platforms system</param>
        public void CheckIfPlatformsCollision(Transform framesParent)
        {
            if (hallEntrance != null && framesParent != null && framesParent.childCount > 0)
            {
                hallEntrance.start = transform.TransformPoint(hallEntrance.start);
                hallEntrance.end = transform.TransformPoint(hallEntrance.end);

                //GameObject line = new GameObject();
                //line.AddComponent<LineRenderer>();
                //line.GetComponent<LineRenderer>().positionCount = 2;
                //line.GetComponent<LineRenderer>().SetPosition(0, hallEntrance.start);
                //line.GetComponent<LineRenderer>().SetPosition(1, hallEntrance.end);

                Line frameLine;
                for (int j = 0; j < framesParent.childCount; j++)
                {
                    var frame = framesParent.GetChild(j).GetComponent<PlatformsFrame>();
                    frameLine = new Line(frame.start, frame.end);
                    Vector3 point = Vector3.zero;
                    if (BrydenWoodUtils.RayRayIntersection(hallEntrance.start, hallEntrance.Direction, frameLine.start, frameLine.Direction, out point))
                    {
                        Vector3 delta1 = point - hallEntrance.start;
                        Vector3 proj = hallEntrance.start + Vector3.Project(delta1, hallEntrance.end - hallEntrance.start);
                        Vector3 d1 = proj - hallEntrance.start;
                        Vector3 d2 = proj - hallEntrance.end;
                        //GameObject proj1 = new GameObject("proj" + j);
                        //proj1.transform.SetParent(line.transform);
                        //proj1.transform.position = proj;
                        float distance = hallEntrance.Length;
                        if (d1.magnitude < distance && d2.magnitude < distance)
                        {
                            frame.AddHallCollision(gameObject);
                        }
                    }
                }
            }
        }
        #endregion

        #region Private Methods
        private IEnumerator UpdateBalconiesCoroutine(int roomId, float length)
        {
            if (Standards.TaggedObject.balconies == Balconies.External)
            {
                int indexOfBalcony = -1;
                var dict = m3 ? Standards.TaggedObject.amenitiesIndicesM3 : Standards.TaggedObject.amenitiesIndices;
                if (dict["Exterior"].TryGetValue(roomId, out indexOfBalcony))
                {
                    aptLayout.roomInclusion[indexOfBalcony] = !aptLayout.roomInclusion[indexOfBalcony];
                    if (!aptLayout.roomInclusion[indexOfBalcony])
                    {
                        //Standards.TaggedObject.amenitiesAreas[apartmentType] = 0;
                        if (m3)
                            Standards.TaggedObject.TrySetAmenitiesAreaM3(apartmentType, 0);
                        else
                            Standards.TaggedObject.TrySetAmenitiesArea(apartmentType, 0);
                    }
                    else
                    {
                        float lrLength = length;
                        var amenityArea = Standards.TaggedObject.MinimumAmenitiesArea[apartmentType];//lrLength * 1.75;
                        //float depth = amenityArea / lrLength;
                        var keys = m3 ? Standards.TaggedObject.RoomNamesM3.Keys.ToList() : Standards.TaggedObject.RoomNames.Keys.ToList();
                        if (keys[indexOfBalcony] == "AILR" || keys[indexOfBalcony] == "AILRR" || keys[indexOfBalcony] == "ALRR" || keys[indexOfBalcony] == "ALR"|| keys[indexOfBalcony] == "AILRRR" || keys[indexOfBalcony] == "AILRRRR" || keys[indexOfBalcony] == "ALRRRR" || keys[indexOfBalcony] == "ALRRR")
                        {
                            //Standards.TaggedObject.amenitiesAreas[apartmentType] = (float)amenityArea * -1;
                            if (m3)
                                Standards.TaggedObject.TrySetAmenitiesAreaM3(apartmentType, amenityArea * -1);
                            else
                                Standards.TaggedObject.TrySetAmenitiesArea(apartmentType, amenityArea * -1);
                        }
                    }
                    if (m3)
                        Standards.TaggedObject.ApartmentTypeRoomsM3[apartmentType][indexOfBalcony] = aptLayout.roomInclusion[indexOfBalcony];
                    else
                        Standards.TaggedObject.ApartmentTypeRooms[apartmentType][indexOfBalcony] = aptLayout.roomInclusion[indexOfBalcony];
                    yield return StartCoroutine(ResetLayout());
                    Initialize();
                    ShowOutline(true);
                    SetManufacturingSystem(manufacturingSystem);
                }
            }
            else
            {
                //refreshNotification.gameObject.SetActive(true);
                float lrLength = length;
                var amenityArea = Standards.TaggedObject.MinimumAmenitiesArea[apartmentType];//lrLength * 1.75;
                int indexOfBalcony = -1;
                var dict = m3 ? Standards.TaggedObject.amenitiesIndicesM3 : Standards.TaggedObject.amenitiesIndices;
                if (dict["Interior"].TryGetValue(roomId, out indexOfBalcony))
                {
                    aptLayout.roomInclusion[indexOfBalcony] = !aptLayout.roomInclusion[indexOfBalcony];
                    if (m3)
                        Standards.TaggedObject.ApartmentTypeRoomsM3[apartmentType][indexOfBalcony] = aptLayout.roomInclusion[indexOfBalcony];
                    else
                        Standards.TaggedObject.ApartmentTypeRooms[apartmentType][indexOfBalcony] = aptLayout.roomInclusion[indexOfBalcony];
                    if (!aptLayout.roomInclusion[indexOfBalcony])
                    {
                        if (m3)
                            Standards.TaggedObject.TrySetAmenitiesAreaM3(apartmentType, 0);
                        else
                            Standards.TaggedObject.TrySetAmenitiesArea(apartmentType, 0);
                    }
                    else
                    {
                        if (m3)
                            Standards.TaggedObject.TrySetAmenitiesAreaM3(apartmentType, amenityArea);
                        else
                            Standards.TaggedObject.TrySetAmenitiesArea(apartmentType, amenityArea);
                    }
                    if (m3)
                        width = (float)Standards.TaggedObject.DesiredAreasM3[apartmentType] / height;
                    else
                        width = (float)Standards.TaggedObject.DesiredAreas[apartmentType] / height;
                    yield return StartCoroutine(ResetLayout());
                    SetManufacturingSystem(manufacturingSystem);
                }
            }
        }

        private IEnumerator UpdateMansionBalconiesCoroutine(int roomId)
        {
            if (Standards.TaggedObject.balconies == Balconies.External)
            {
                int indexOfBalcony = -1;
                Dictionary<string, Dictionary<int, int>> dict = m3 ? Standards.TaggedObject.amenitiesIndicesMansionM3 : Standards.TaggedObject.amenitiesIndicesMansion;
                if (dict["Exterior"].TryGetValue(roomId, out indexOfBalcony))
                {
                    aptLayout.roomInclusion[indexOfBalcony] = !aptLayout.roomInclusion[indexOfBalcony];
                    if (buildingType == BuildingType.Mansion)
                    {
                        if (m3)
                            Standards.TaggedObject.ApartmentTypeRoomsMansionM3[apartmentType][indexOfBalcony] = aptLayout.roomInclusion[indexOfBalcony];
                        else
                            Standards.TaggedObject.ApartmentTypeRoomsMansion[apartmentType][indexOfBalcony] = aptLayout.roomInclusion[indexOfBalcony];
                    }
                    else
                    {
                        if (m3)
                            Standards.TaggedObject.ApartmentTypeRooms[apartmentType][indexOfBalcony] = aptLayout.roomInclusion[indexOfBalcony];
                        else
                            Standards.TaggedObject.ApartmentTypeRoomsM3[apartmentType][indexOfBalcony] = aptLayout.roomInclusion[indexOfBalcony];
                    }
                    yield return StartCoroutine(ResetLayout());
                    Initialize();
                    ShowOutline(true);
                    SetManufacturingSystem(manufacturingSystem);
                }
            }
        }

        private void GenerateMaxLayout()
        {
            var delta = maxOutterWidth - outterWidth;
            if (maxLayout != null)
            {
                DestroyImmediate(maxLayout);
            }
            maxLayout = Instantiate(minMaxRenderer, transform);
            maxLayout.name = "maxLayout";
            var lr = maxLayout.GetComponent<LineRenderer>();
            lr.positionCount = 4;
            lr.SetPosition(0, new Vector3(outterWidth, 5, outterHeight));
            lr.SetPosition(1, new Vector3(outterWidth + delta, 5, outterHeight));
            lr.SetPosition(2, new Vector3(outterWidth + delta, 5, 0));
            lr.SetPosition(3, new Vector3(outterWidth, 5, 0));
        }

        private void GenerateMinimumLayout()
        {
            var delta = minOutterWidth;
            if (delta > 0)
            {
                if (minLayout != null)
                {
                    DestroyImmediate(minLayout);
                }
                minLayout = Instantiate(minMaxRenderer, transform);
                minLayout.name = "minLayout";
                var lr = minLayout.GetComponent<LineRenderer>();
                lr.positionCount = 2;
                lr.SetPosition(0, new Vector3(minOutterWidth, 5, outterHeight));
                lr.SetPosition(1, new Vector3(minOutterWidth, 5, 0));
                //minLayout.transform.localPosition = new Vector3(delta / 2, 5, outterHeight / 2);
                //minLayout.transform.localScale = new Vector3(delta / 10, 1, outterHeight / 10);
            }
            else
            {
                return;
            }
        }

        private void GeneratePanelsMansion()
        {
            Vector3 localOrigin = Vector3.zero;
            if (panelsParent != null)
            {
                DestroyImmediate(panelsParent.gameObject);
            }
            panelsParent = new GameObject("PanelsParent").transform;
            panelsParent.SetParent(transform);
            Material panelMaterial = null;
            panels = new List<GameObject>();
        }

        private void GeneratePanels()
        {
            Vector3 localOrigin = Vector3.zero;
            if (panelsParent != null)
            {
                DestroyImmediate(panelsParent.gameObject);
            }
            panelsParent = new GameObject("PanelsParent").transform;
            panelsParent.SetParent(transform);
            Material panelMaterial = null;
            panels = new List<GameObject>();
            if (buildingType != BuildingType.Mansion)
            {
                float m_width = outterWidth;

                if (!hasRight) m_width += (Standards.TaggedObject.ConstructionFeatures["ExteriorWall"] - wallWidth);
                if (!hasLeft)
                {
                    m_width += (Standards.TaggedObject.ConstructionFeatures["ExteriorWall"] - wallWidth);
                    localOrigin = new Vector3(-(Standards.TaggedObject.ConstructionFeatures["ExteriorWall"] - wallWidth), 0, 0);
                }

                if (m_width <= maxPanelWidth)
                {
                    //For the facade
                    var temp_scale = new Vector3(m_width, totalHeight, wallWidth);
                    var temp_pos = new Vector3(localOrigin.x + temp_scale.x / 2.0f, totalHeight / 2.0f, outterHeight + temp_scale.z / 2.0f);
                    panels.Add(GetSingleFacadePanel(
                                m_width,
                                temp_pos,
                                temp_scale,
                                new Vector3(0, 0, 0),
                                panelMaterial
                                ));

                    //For the corridor
                    temp_scale = new Vector3(m_width, totalHeight, Standards.TaggedObject.ConstructionFeatures["CorridorWall"]);
                    temp_pos = new Vector3(localOrigin.x + temp_scale.x / 2.0f, totalHeight / 2.0f, temp_scale.z / 2.0f);
                    panels.Add(GetSingleFacadePanel(
                                m_width,
                                temp_pos,
                                temp_scale,
                                new Vector3(0, 0, 0),
                                panelMaterial
                                ));
                    localOrigin += new Vector3(temp_scale.x, 0, 0);
                }
                else
                {
                    panels.AddRange(GetFacadePanelsForApartment(true));
                    panels.AddRange(GetFacadePanelsForApartment(false));
                }
                panelsParent.gameObject.SetActive(false);

                if (!hasLeft)
                {
                    float m_length = outterHeight;
                    if (m_length <= maxPanelWidth)
                    {
                        panels.Add(GetSingleFacadePanel(
                                m_length,
                                new Vector3(-Standards.TaggedObject.ConstructionFeatures["ExteriorWall"] + wallWidth / 2.0f, totalHeight / 2, m_length / 2),
                                new Vector3(m_length, totalHeight, wallWidth),
                                new Vector3(0, 90, 0),
                                panelMaterial
                                ));
                    }
                    else
                    {
                        int num = Mathf.CeilToInt(m_length / maxPanelWidth);
                        float length = outterHeight / num;
                        for (int i = 0; i < num; i++)
                        {
                            panels.Add(GetSingleFacadePanel(
                                length,
                                new Vector3(-Standards.TaggedObject.ConstructionFeatures["ExteriorWall"] + wallWidth / 2.0f, totalHeight / 2, length * i + length / 2),
                                new Vector3(length, totalHeight, wallWidth),
                                new Vector3(0, 90, 0),
                                panelMaterial
                                ));
                        }
                    }
                }
                else
                {
                    float m_length = outterHeight;
                    if (m_length <= maxPanelWidth)
                    {
                        panels.Add(GetSingleFacadePanel(
                                m_length,
                                new Vector3(Standards.TaggedObject.ConstructionFeatures["PartyWall"] / 4, totalHeight / 2, m_length / 2),
                                new Vector3(m_length, totalHeight, Standards.TaggedObject.ConstructionFeatures["PartyWall"] / 2),
                                new Vector3(0, 90, 0),
                                panelMaterial
                                ));
                    }
                    else
                    {
                        int num = Mathf.CeilToInt(m_length / maxPanelWidth);
                        float length = outterHeight / num;
                        for (int i = 0; i < num; i++)
                        {
                            panels.Add(GetSingleFacadePanel(
                                length,
                                new Vector3(Standards.TaggedObject.ConstructionFeatures["PartyWall"] / 4, totalHeight / 2, length * i + length / 2),
                                new Vector3(length, totalHeight, Standards.TaggedObject.ConstructionFeatures["PartyWall"] / 2),
                                new Vector3(0, 90, 0),
                                panelMaterial
                                ));
                        }
                    }

                }

                if (!hasRight)
                {
                    float m_length = outterHeight;// m_height + wallWidth;
                    if (m_length <= maxPanelWidth)
                    {
                        panels.Add(GetSingleFacadePanel(
                                m_length,
                                new Vector3(outterWidth + Standards.TaggedObject.ConstructionFeatures["ExteriorWall"] - wallWidth / 2.0f, totalHeight / 2, m_length / 2),
                                new Vector3(m_length, totalHeight, wallWidth),
                                new Vector3(0, 90, 0),
                                panelMaterial
                                ));
                    }
                    else
                    {
                        int num = Mathf.CeilToInt(m_length / maxPanelWidth);
                        float length = outterHeight / num;
                        for (int i = 0; i < num; i++)
                        {
                            panels.Add(GetSingleFacadePanel(
                                length,
                                new Vector3(outterWidth + Standards.TaggedObject.ConstructionFeatures["ExteriorWall"] - wallWidth / 2.0f, totalHeight / 2, length * i + length / 2),
                                new Vector3(length, totalHeight, wallWidth),
                                new Vector3(0, 90, 0),
                                panelMaterial
                                ));
                        }
                    }
                }
                else
                {
                    float m_length = outterHeight;// m_height + wallWidth;
                    if (m_length <= maxPanelWidth)
                    {
                        panels.Add(GetSingleFacadePanel(
                                m_length,
                                new Vector3(outterWidth - Standards.TaggedObject.ConstructionFeatures["PartyWall"] / 4, totalHeight / 2, m_length / 2),
                                new Vector3(m_length, totalHeight, Standards.TaggedObject.ConstructionFeatures["PartyWall"] / 2),
                                new Vector3(0, 90, 0),
                                panelMaterial
                                ));
                    }
                    else
                    {
                        int num = Mathf.CeilToInt(m_length / maxPanelWidth);
                        float length = outterHeight / num;
                        for (int i = 0; i < num; i++)
                        {
                            panels.Add(GetSingleFacadePanel(
                                length,
                                new Vector3(outterWidth - Standards.TaggedObject.ConstructionFeatures["PartyWall"] / 4, totalHeight / 2, length * i + length / 2),
                                new Vector3(length, totalHeight, Standards.TaggedObject.ConstructionFeatures["PartyWall"] / 2),
                                new Vector3(0, 90, 0),
                                panelMaterial
                                ));
                        }
                    }
                }



                for (int i = 0; i < corridorLineLoads.Count; i++)
                {
                    var obj = Instantiate(corridorLineLoads[i], panelsParent);
                    obj.transform.localScale = new Vector3(corridorLineLoads[i].transform.localScale.z, totalHeight, wallWidth);
                    obj.name = "Panel" + corridorLineLoads[i].transform.localScale.z.ToString("0.00");
                    var temp_Pos = panelsParent.InverseTransformPoint(corridorLineLoads[i].transform.localPosition);
                    obj.transform.position = corridorLineLoads[i].transform.position;
                    obj.transform.localPosition = new Vector3(obj.transform.localPosition.x, totalHeight / 2.0f, obj.transform.localPosition.z);
                    obj.transform.localEulerAngles = new Vector3(0, 90, 0);
                    obj.GetComponent<MeshRenderer>().enabled = true;
                    if (Standards.TaggedObject.panelsMaterials.ContainsKey(obj.name))
                    {
                        panelMaterial = Standards.TaggedObject.panelsMaterials[obj.name];
                    }
                    else
                    {
                        panelMaterial = new Material(Resources.Load("Materials/Modules/Orange") as Material);
                        panelMaterial.color = panelsGradient.Evaluate(corridorLineLoads[i].transform.localScale.z / maxPanelWidth);
                        Standards.TaggedObject.panelsMaterials.Add(obj.name, panelMaterial);
                    }
                    obj.GetComponent<MeshRenderer>().material = panelMaterial;
                    panels.Add(obj);
                }

                for (int i = 0; i < facadeLineLoads.Count; i++)
                {
                    var obj = Instantiate(facadeLineLoads[i], panelsParent);
                    obj.transform.localScale = new Vector3(facadeLineLoads[i].transform.localScale.z, totalHeight, wallWidth);
                    obj.name = "Panel" + facadeLineLoads[i].transform.localScale.z.ToString("0.00");
                    var temp_Pos = panelsParent.InverseTransformPoint(facadeLineLoads[i].transform.localPosition);
                    obj.transform.position = facadeLineLoads[i].transform.position;
                    obj.transform.localPosition = new Vector3(obj.transform.localPosition.x, totalHeight / 2.0f, obj.transform.localPosition.z);
                    obj.transform.localEulerAngles = new Vector3(0, 90, 0);
                    obj.GetComponent<MeshRenderer>().enabled = true;
                    if (Standards.TaggedObject.panelsMaterials.ContainsKey(obj.name))
                    {
                        panelMaterial = Standards.TaggedObject.panelsMaterials[obj.name];
                    }
                    else
                    {
                        panelMaterial = new Material(Resources.Load("Materials/Modules/Orange") as Material);
                        panelMaterial.color = panelsGradient.Evaluate(facadeLineLoads[i].transform.localScale.z / maxPanelWidth);
                        Standards.TaggedObject.panelsMaterials.Add(obj.name, panelMaterial);
                    }
                    obj.GetComponent<MeshRenderer>().material = panelMaterial;
                    panels.Add(obj);
                }
            }
            else
            {
                if (m3)
                {

                }
                else
                {
                    if (apartmentType == "3b5p")
                    {
                        float m_width = outterWidth;
                        //-------for front of apartment--------//
                        {
                            var temp_scale = new Vector3(m_width, totalHeight, wallWidth);
                            var temp_pos = new Vector3(localOrigin.x + temp_scale.x / 2.0f, totalHeight / 2.0f, outterHeight - wallWidth + temp_scale.z / 2.0f);
                            panels.Add(GetSingleFacadePanel(
                                        m_width,
                                        temp_pos,
                                        temp_scale,
                                        new Vector3(0, 0, 0),
                                        panelMaterial
                                        ));
                        }

                        //-------for back of apartment--------//
                        {
                            var temp_scale = new Vector3(m_width, totalHeight, wallWidth);
                            var temp_pos = new Vector3(localOrigin.x + temp_scale.x / 2.0f, totalHeight / 2.0f, localOrigin.z + temp_scale.z / 2.0f);
                            panels.Add(GetSingleFacadePanel(
                                        m_width,
                                        temp_pos,
                                        temp_scale,
                                        new Vector3(0, 0, 0),
                                        panelMaterial
                                        ));
                        }

                        //---------For interior panels --------//
                        float zCoord = 0;
                        for (int i = 0; i < rooms.Count; i++)
                        {
                            if (rooms[i].name == "PSB" || rooms[i].name == "HWWWW")
                            {
                                zCoord += Mathf.Abs(rooms[i].lengths.Last());
                            }
                        }
                        zCoord += 0.15f;
                        {
                            var temp_scale = new Vector3(m_width, totalHeight, wallWidth);
                            var temp_pos = new Vector3(localOrigin.x + m_width / 2.0f, totalHeight / 2.0f, zCoord);
                            panels.Add(GetSingleFacadePanel(
                                        m_width,
                                        temp_pos,
                                        temp_scale,
                                        new Vector3(0, 0, 0),
                                        panelMaterial
                                        ));
                        }

                        m_width = outterHeight;
                        //-------for left of apartment--------//
                        {
                            var temp_scale = new Vector3(m_width, totalHeight, wallWidth);
                            var temp_pos = new Vector3(localOrigin.x + wallWidth / 2.0f, totalHeight / 2.0f, m_width / 2.0f);
                            panels.Add(GetSingleFacadePanel(
                                        m_width,
                                        temp_pos,
                                        temp_scale,
                                        new Vector3(0, 90, 0),
                                        panelMaterial
                                        ));
                        }

                        //-------for right of apartment--------//
                        {
                            var temp_scale = new Vector3(m_width, totalHeight, wallWidth);
                            var temp_pos = new Vector3(outterWidth - wallWidth + wallWidth / 2.0f, totalHeight / 2.0f, m_width / 2.0f);
                            panels.Add(GetSingleFacadePanel(
                                        m_width,
                                        temp_pos,
                                        temp_scale,
                                        new Vector3(0, 90, 0),
                                        panelMaterial
                                        ));
                        }
                    }
                    else
                    {

                        float m_width = outterWidth;
                        //-------for front of apartment--------//
                        {
                            var temp_scale = new Vector3(m_width, totalHeight, wallWidth);
                            var temp_pos = new Vector3(localOrigin.x + temp_scale.x / 2.0f, totalHeight / 2.0f, outterHeight - wallWidth + temp_scale.z / 2.0f);
                            panels.Add(GetSingleFacadePanel(
                                        m_width,
                                        temp_pos,
                                        temp_scale,
                                        new Vector3(0, 0, 0),
                                        panelMaterial
                                        ));
                        }

                        //-------for back of apartment--------//
                        {
                            var temp_scale = new Vector3(m_width, totalHeight, wallWidth);
                            var temp_pos = new Vector3(localOrigin.x + temp_scale.x / 2.0f, totalHeight / 2.0f, localOrigin.z + temp_scale.z / 2.0f);
                            panels.Add(GetSingleFacadePanel(
                                        m_width,
                                        temp_pos,
                                        temp_scale,
                                        new Vector3(0, 0, 0),
                                        panelMaterial
                                        ));
                        }

                        m_width = outterHeight;
                        //-------for left of apartment--------//
                        {
                            var temp_scale = new Vector3(m_width, totalHeight, wallWidth);
                            var temp_pos = new Vector3(localOrigin.x + wallWidth / 2.0f, totalHeight / 2.0f, m_width / 2.0f);
                            panels.Add(GetSingleFacadePanel(
                                        m_width,
                                        temp_pos,
                                        temp_scale,
                                        new Vector3(0, 90, 0),
                                        panelMaterial
                                        ));
                        }

                        //-------for right of apartment--------//
                        {
                            var temp_scale = new Vector3(m_width, totalHeight, wallWidth);
                            var temp_pos = new Vector3(outterWidth - wallWidth + wallWidth / 2.0f, totalHeight / 2.0f, m_width / 2.0f);
                            panels.Add(GetSingleFacadePanel(
                                        m_width,
                                        temp_pos,
                                        temp_scale,
                                        new Vector3(0, 90, 0),
                                        panelMaterial
                                        ));
                        }
                    }
                }
            }
        }

        private void GetMansionLineLoads()
        {
            Vector3 localOrigin = Vector3.zero;
            if (m3)
            {

            }
            else
            {
                if (apartmentType == "3b5p")
                {
                    float m_width = outterWidth;
                    //-------for front of apartment--------//
                    {
                        var temp_scale = new Vector3(m_width, totalHeight, wallWidth);
                        var temp_pos = new Vector3(localOrigin.x + temp_scale.x / 2.0f, totalHeight / 2.0f, outterHeight - wallWidth + temp_scale.z / 2.0f);

                        GameObject lineLoadObject = Instantiate(proceduralRoomPrefab.GetComponent<ProceduralRoom>().lineLoadPrefab, lineLoadsParent);
                        lineLoadObject.GetComponent<LineLoad>().isFixed = true;
                        lineLoadObject.GetComponent<LineLoad>().allignMaterial = lineLoadObject.GetComponent<LineLoad>().green;
                        lineLoadObject.GetComponent<MeshRenderer>().material = lineLoadObject.GetComponent<LineLoad>().green;
                        lineLoadObject.GetComponent<LineLoad>().issue = false;
                        lineLoadObject.transform.localPosition = new Vector3(temp_pos.x, -1, temp_pos.z);
                        lineLoadObject.transform.localScale = new Vector3(outterWidth, 0.03f, 0.03f);
                    }

                    //-------for back of apartment--------//
                    {
                        var temp_scale = new Vector3(m_width, totalHeight, wallWidth);
                        var temp_pos = new Vector3(localOrigin.x + temp_scale.x / 2.0f, totalHeight / 2.0f, localOrigin.z + temp_scale.z / 2.0f);

                        GameObject lineLoadObject = Instantiate(proceduralRoomPrefab.GetComponent<ProceduralRoom>().lineLoadPrefab, lineLoadsParent);
                        lineLoadObject.GetComponent<LineLoad>().isFixed = true;
                        lineLoadObject.GetComponent<LineLoad>().allignMaterial = lineLoadObject.GetComponent<LineLoad>().green;
                        lineLoadObject.GetComponent<MeshRenderer>().material = lineLoadObject.GetComponent<LineLoad>().green;
                        lineLoadObject.GetComponent<LineLoad>().issue = false;
                        lineLoadObject.transform.localPosition = new Vector3(temp_pos.x, -1, temp_pos.z);
                        lineLoadObject.transform.localScale = new Vector3(outterWidth, 0.03f, 0.03f);
                    }

                    //---------For interior panels --------//
                    float zCoord = 0;
                    if (rooms != null)
                    {
                        for (int i = 0; i < rooms.Count; i++)
                        {
                            if (rooms[i] != null)
                            {
                                if (rooms[i].name == "PSB" || rooms[i].name == "HWWWW")
                                {
                                    zCoord += Mathf.Abs(rooms[i].lengths.Last());
                                }
                            }
                        }
                    }
                    else
                    {
                        zCoord = 5.95f;
                    }
                    zCoord += 0.15f;
                    {
                        var temp_scale = new Vector3(m_width, totalHeight, wallWidth);
                        var temp_pos = new Vector3(localOrigin.x + m_width / 2.0f, totalHeight / 2.0f, zCoord);

                        GameObject lineLoadObject = Instantiate(proceduralRoomPrefab.GetComponent<ProceduralRoom>().lineLoadPrefab, lineLoadsParent);
                        lineLoadObject.GetComponent<LineLoad>().isFixed = true;
                        lineLoadObject.GetComponent<LineLoad>().allignMaterial = lineLoadObject.GetComponent<LineLoad>().green;
                        lineLoadObject.GetComponent<MeshRenderer>().material = lineLoadObject.GetComponent<LineLoad>().green;
                        lineLoadObject.GetComponent<LineLoad>().issue = false;
                        lineLoadObject.transform.localPosition = new Vector3(temp_pos.x, -1, temp_pos.z);
                        lineLoadObject.transform.localScale = new Vector3(outterWidth, 0.03f, 0.03f);
                    }
                }
                else
                {

                    m_width = outterHeight;
                    //-------for left of apartment--------//
                    {
                        var temp_scale = new Vector3(m_width, totalHeight, wallWidth);
                        var temp_pos = new Vector3(localOrigin.x + wallWidth / 2.0f, totalHeight / 2.0f, m_width / 2.0f);

                        GameObject lineLoadObject = Instantiate(proceduralRoomPrefab.GetComponent<ProceduralRoom>().lineLoadPrefab, lineLoadsParent);
                        lineLoadObject.GetComponent<LineLoad>().isFixed = true;
                        lineLoadObject.GetComponent<LineLoad>().allignMaterial = lineLoadObject.GetComponent<LineLoad>().green;
                        lineLoadObject.GetComponent<MeshRenderer>().material = lineLoadObject.GetComponent<LineLoad>().green;
                        lineLoadObject.GetComponent<LineLoad>().issue = false;
                        lineLoadObject.transform.localPosition = new Vector3(temp_pos.x, -1, temp_pos.z);
                        lineLoadObject.transform.localScale = new Vector3(0.03f, 0.03f, outterHeight);
                    }

                    //-------for right of apartment--------//
                    {
                        var temp_scale = new Vector3(m_width, totalHeight, wallWidth);
                        var temp_pos = new Vector3(outterWidth - wallWidth + wallWidth / 2.0f, totalHeight / 2.0f, m_width / 2.0f);

                        GameObject lineLoadObject = Instantiate(proceduralRoomPrefab.GetComponent<ProceduralRoom>().lineLoadPrefab, lineLoadsParent);
                        lineLoadObject.GetComponent<LineLoad>().isFixed = true;
                        lineLoadObject.GetComponent<LineLoad>().allignMaterial = lineLoadObject.GetComponent<LineLoad>().green;
                        lineLoadObject.GetComponent<MeshRenderer>().material = lineLoadObject.GetComponent<LineLoad>().green;
                        lineLoadObject.GetComponent<LineLoad>().issue = false;
                        lineLoadObject.transform.localPosition = new Vector3(temp_pos.x, -1, temp_pos.z);
                        lineLoadObject.transform.localScale = new Vector3(0.03f, 0.03f, outterHeight);
                    }
                }
            }
        }

        private GameObject GetSingleFacadePanel(float length, Vector3 position, Vector3 scale, Vector3 euler, Material panelMaterial)
        {
            GameObject singleFacadePanel = GameObject.CreatePrimitive(PrimitiveType.Cube);
            singleFacadePanel.name = "Panel" + length.ToString("0.00");
            singleFacadePanel.transform.localScale = scale;
            singleFacadePanel.transform.SetParent(panelsParent);
            singleFacadePanel.transform.localEulerAngles = euler;
            singleFacadePanel.transform.localPosition = position;
            if (Standards.TaggedObject.panelsMaterials.ContainsKey(singleFacadePanel.name))
            {
                panelMaterial = Standards.TaggedObject.panelsMaterials[singleFacadePanel.name];
            }
            else
            {
                panelMaterial = new Material(Resources.Load("Materials/Modules/Orange") as Material);
                panelMaterial.color = panelsGradient.Evaluate(length / maxPanelWidth);
                Standards.TaggedObject.panelsMaterials.Add(singleFacadePanel.name, panelMaterial);
            }
            singleFacadePanel.GetComponent<MeshRenderer>().material = panelMaterial;
            return singleFacadePanel;
        }

        private void GenerateVolumetricModules()
        {
            if (verticalModulesParent != null)
            {
                DestroyImmediate(verticalModulesParent.gameObject);
            }
            verticalModulesParent = new GameObject("VerticalModulesParent").transform;
            verticalModulesParent.SetParent(transform);
            verticalModules = new List<GameObject>();
            if (buildingType == BuildingType.Mansion)
            {
                verticalModules.AddRange(GetModulesForApartmentsMansion());
            }
            else
            {
                verticalModules.AddRange(GetModulesForApartment());
            }
        }

        private void IssuedFlagged(bool show)
        {
            if (roomIssue != null)
            {
                roomIssue.text = "The room dimensions you have set are not fit for this size of apartment!";
                roomIssue.gameObject.SetActive(show);
            }
        }

        private void GenerateRooms()
        {
            if (roomParent != null)
            {
                DestroyImmediate(roomParent.gameObject);
            }
            roomParent = new GameObject("Rooms").transform;
            roomParent.SetParent(transform);

            rooms = new List<ProceduralRoom>();
            lines = new List<BWT.Line>();
            for (int i = 0; i < lengths.Count; i++)
            {
                if (aptLayout.types[i] != "WDB")
                {
                    GameObject obj = Instantiate(proceduralRoomPrefab, roomParent);
                    obj.name = edges.Keys.ToList()[i];
                    if (origins[i] == "orig")
                    {
                        obj.transform.localPosition = origin;
                    }
                    else
                    {
                        var inds = origins[i].Split(';');
                        try
                        {
                            var vert = rooms[int.Parse(inds[0])].GetVertex(int.Parse(inds[1]));
                            obj.transform.position = vert;
                        }
                        catch (Exception e)
                        {
                            Debug.Log(e);
                        }
                    }
                    var procedural = obj.GetComponent<ProceduralRoom>();
                    procedural.aptLayout = this;
                    if (i < aptLayout.roomColors.Length)
                    {
                        procedural.color = aptLayout.roomColors[i];
                    }
                    else
                    {
                        procedural.color = aptLayout.roomColors.Last();
                    }
                    bool roomBools = false;
                    if (buildingType == BuildingType.Mansion)
                    {
                        if (m3)
                        {
                            roomBools = Standards.TaggedObject.RoomDimensionBoolsMansionM3[obj.name];
                        }
                        else
                        {
                            roomBools = Standards.TaggedObject.RoomDimensionBoolsMansion[obj.name];
                        }
                    }
                    else
                    {
                        if (m3)
                            roomBools = Standards.TaggedObject.RoomDimensionBoolsM3[obj.name];
                        else
                            roomBools = Standards.TaggedObject.RoomDimensionBools[obj.name];
                    }
                    procedural.Initialize(lengths[i], aptLayout.types[i], totalHeight, i, roomBools);
                    rooms.Add(procedural);
                }
                else
                {
                    Debug.Log(lengths[i]);
                    GameObject obj = Instantiate(proceduralRoomPrefab, roomParent);
                    obj.name = edges.Keys.ToList()[i];
                    if (origins[i] == "orig")
                    {
                        obj.transform.localPosition = origin;
                    }
                    else
                    {
                        var inds = origins[i].Split(';');
                        var vert = rooms[int.Parse(inds[0])].GetVertex(int.Parse(inds[1]));
                        obj.transform.position = vert;
                    }
                    var procedural = obj.GetComponent<ProceduralRoom>();
                }
            }
            if (!isInBuilding)
                GenerateWindowsCrops();
            CheckDuplicateLines();
        }

        private void GenerateWindowsCrops()
        {
            if (frontWindowsCrop == null)
                frontWindowsCrop = GameObject.CreatePrimitive(PrimitiveType.Plane);
            frontWindowsCrop.transform.SetParent(transform);
            frontWindowsCrop.transform.localScale = new Vector3(outterWidth * 0.1f, 1, totalHeight * 0.1f);
            frontWindowsCrop.transform.localEulerAngles = new Vector3(90, 0, 0);
            frontWindowsCrop.transform.localPosition = new Vector3(outterWidth * 0.5f, totalHeight * 0.5f, outterHeight - 1);
        }

        private void UpdateRooms()
        {
            for (int i = 0; i < rooms.Count; i++)
            {
                var obj = rooms[i].gameObject;
                if (origins[i] == "orig")
                {
                    obj.transform.localPosition = origin;
                }
                else
                {
                    var inds = origins[i].Split(';');
                    var vert = rooms[int.Parse(inds[0])].GetVertex(int.Parse(inds[1]));
                    obj.transform.position = vert;
                }
                rooms[i].UpdateRoom(lengths[i]);
            }
        }

        private void GetEdgesFromContent()
        {
            var sideOffset = (partyWall);
            var topOffset = (exteriorWall) + (corridorWall);
            outterWidth = width + sideOffset;
            outterHeight = height + topOffset;
            maxOutterWidth = (float)Standards.TaggedObject.ApartmentTypesMaximumSizes[apartmentType] / height + sideOffset;
            minOutterWidth = (float)Standards.TaggedObject.ApartmentTypesMinimumSizes[apartmentType] / height + sideOffset;
            m_width = width + wallWidth;
            m_height = height + wallWidth;


            totalArea = width * height;
            NIA = width * height;

            origin = new Vector3(sideOffset * 0.5f - wallWidth * 0.5f, 0, corridorWall - wallWidth * 0.5f);

            edges = new Dictionary<string, List<string>>();
            origins = new List<string>();
            var lines = content.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < lines.Length; i++)
            {
                var values = lines[i].Split(',').ToList();
                string name = values[0];
                try
                {
                    origins.Add(values[1]);
                }
                catch (Exception e)
                {
                    Debug.Log(e);
                }
                values.RemoveAt(0);
                values.RemoveAt(0);
                for (int j = 0; j < values.Count; j++)
                {
                    float v = 0.0f;
                    if (float.TryParse(values[j], out v))
                    {
                        values[j] = (v).ToString();
                    }
                }

                for (int j = 0; j < aptLayout.roomInclusion.Length; j++)
                {
                    if (name == aptLayout.roomNames[j])
                    {
                        if (!aptLayout.roomInclusion[j])
                        {
                            for (int k = 0; k < values.Count; k++)
                            {
                                values[k] = 0.ToString();
                            }
                        }
                    }
                }
                edges.Add(name, values);
            }
        }

        private void GetLengthsFromEdges()
        {
            lengths = new List<List<float>>();
            foreach (var item in edges)
            {
                List<float> m_lengths = new List<float>();
                for (int i = 0; i < item.Value.Count; i++)
                {
                    var length = ParseEdgeValue(item.Value[i], item.Key, edges, m_height, m_width);
                    m_lengths.Add(length);
                }
                lengths.Add(m_lengths);
            }
            originalLengths = new List<List<float>>();
            for (int i = 0; i < lengths.Count; i++)
            {
                originalLengths.Add(new List<float>());
                for (int j = 0; j < lengths[i].Count; j++)
                {
                    originalLengths[i].Add(lengths[i][j]);
                }
            }
        }

        private float ParseEdgeValue(string val, string name, Dictionary<string, List<string>> edges, float height, float width)
        {
            try
            {
                //Print("in: " + val);
                float m_val = 0;

                if (float.TryParse(val, out m_val))
                {
                    return m_val;
                }
                else
                {
                    if (val.Contains("-"))
                    {
                        var parts = val.Split('-');
                        float v = ParseEdgeValue(parts[0], name, edges, height, width);
                        for (int i = 1; i < parts.Length; i++)
                        {
                            v -= ParseEdgeValue(parts[i], name, edges, height, width);
                        }
                        m_val = v;
                    }
                    else if (val.Contains("+"))
                    {
                        var parts = val.Split('+');
                        float v = 0;
                        for (int i = 0; i < parts.Length; i++)
                        {
                            var m_v = ParseEdgeValue(parts[i], name, edges, height, width);
                            v += m_v;
                        }
                        m_val = v;
                    }
                    else if (val.Contains("\""))
                    {
                        int m_index = -1;
                        val = val.Replace("\"", String.Empty);
                        if (val == "HEIGHT")
                        {
                            m_val = height;
                        }
                        else if (val == "WIDTH")
                        {
                            m_val = width;
                        }
                        else if (val == "dbVal")
                        {
                            m_val = dbVal;
                        }
                        else if (val == "pbVal")
                        {
                            m_val = pbVal;
                        }
                        else if (val == "baVal")
                        {
                            m_val = baVal;
                        }
                        else if (val == "baaVal")
                        {
                            m_val = baaVal;
                        }
                        else if (val == "wcVal")
                        {
                            m_val = wcVal;
                        }
                        else if (val == "wccVal")
                        {
                            m_val = wccVal;
                        }
                        else if (val == "dbbVal")
                        {
                            m_val = dbbVal;
                        }
                        else if (val == "pbbVal")
                        {
                            m_val = pbbVal;
                        }
                        else if (val == "psbVal")
                        {
                            m_val = psbVal;
                        }
                        else if (val == "hwVal")
                        {
                            m_val = hwVal;
                        }
                        else if (val == "balcVal")
                        {
                            if (name == "AILRR" || name == "LRR")
                            {
                                var depth = ParseEdgeValue(edges["LRR"][2], name, edges, height, width);
                                m_val = Math.Abs(minAmenity / depth);
                            }
                            else if (name == "AILR" || name == "LR")
                            {
                                var depth = ParseEdgeValue(edges["LR"][2], name, edges, height, width);
                                m_val = Math.Abs(minAmenity / depth);
                            }

                            if (name == "AILRRRR" || name == "LRRRR")
                            {
                                var depth = ParseEdgeValue(edges["LRRRR"][2], name, edges, height, width);
                                m_val = Math.Abs(minAmenity / depth);
                            }
                            else if (name == "AILRRR" || name == "LRRR")
                            {
                                var depth = ParseEdgeValue(edges["LRRR"][2], name, edges, height, width);
                                m_val = Math.Abs(minAmenity / depth);
                            }
                        }
                        else if (val == "balcVal2")
                        {
                            if (name == "ALRR")
                            {
                                m_val = Math.Abs(minAmenity / 1.5f);
                            }
                            else if (name == "ALR")
                            {
                                m_val = Math.Abs(minAmenity / 1.5f);
                            }
                            else if (name == "ALRRRR")
                            {
                                m_val = Math.Abs(minAmenity / 1.5f);
                            }
                            else if (name == "ALRRR")
                            {
                                m_val = Math.Abs(minAmenity / 1.5f);
                            }
                        }
                        //else if (val == "wdbVal1")
                        //{
                        //    m_val = wdbVal1;
                        //}
                        //else if (val == "wdbVal2")
                        //{
                        //    m_val = wdbVal2;
                        //}
                        else if (int.TryParse(val, out m_index))
                        {
                            if (val.Contains("-"))
                            {
                                m_val = -1 * ParseEdgeValue(edges[name][Math.Abs(m_index)], name, edges, height, width);
                            }
                            else
                            {
                                m_val = ParseEdgeValue(edges[name][m_index], name, edges, height, width);
                            }
                        }
                        else if (val.Contains("_"))
                        {
                            var parts = val.Split('_');
                            m_val = ParseEdgeValue(edges[parts[0]][int.Parse(parts[1])], name, edges, height, width);
                        }
                    }
                    else
                    {
                        if (val == "HEIGHT")
                        {
                            m_val = height;
                        }
                        else if (val == "WIDTH")
                        {
                            m_val = width;
                        }
                        else if (val == "dbVal")
                        {
                            m_val = dbVal;
                        }
                        else if (val == "pbVal")
                        {
                            m_val = pbVal;
                        }
                        else if (val == "baVal")
                        {
                            m_val = baVal;
                        }
                        else if (val == "baaVal")
                        {
                            m_val = baaVal;
                        }
                        else if (val == "wcVal")
                        {
                            m_val = wcVal;
                        }
                        else if (val == "wccVal")
                        {
                            m_val = wccVal;
                        }
                        else if (val == "dbbVal")
                        {
                            m_val = dbbVal;
                        }
                        else if (val == "pbbVal")
                        {
                            m_val = pbbVal;
                        }
                        else if (val == "psbVal")
                        {
                            m_val = psbVal;
                        }
                        else if (val == "hwVal")
                        {
                            m_val = hwVal;
                        }
                        else if (val == "balcVal")
                        {
                            if (name == "AILRR" || name == "LRR")
                            {
                                m_val = Math.Abs(minAmenity / ParseEdgeValue(edges["LRR"][2], name, edges, height, width));
                            }
                            else if (name == "AILR" || name == "LR")
                            {
                                m_val = Math.Abs(minAmenity / ParseEdgeValue(edges["LR"][2], name, edges, height, width));
                            }
                        }
                        else if (val == "balcVal2")
                        {
                            if (name == "ALRR")
                            {
                                m_val = Math.Abs(minAmenity / 1.5f);
                            }
                            else if (name == "ALR")
                            {
                                m_val = Math.Abs(minAmenity / 1.5f);
                            }
                        }
                        else if (val.Contains("_"))
                        {
                            var parts = val.Split('_');
                            m_val = ParseEdgeValue(edges[parts[0]][int.Parse(parts[1])], name, edges, height, width);
                        }
                    }
                    return m_val;
                }
            }
            catch (Exception e)
            {
                Debug.Log(val + ": " + e);
                return 0;
            }
        }
        #endregion

        #region Panels and Modules Methods

        private List<GameObject> GetFacadePanelsForApartment(bool facade)
        {
            Material panelMaterial;
            List<GameObject> panels = new List<GameObject>();
            Vector3 localOrigin = !hasLeft ? new Vector3(-(Standards.TaggedObject.ConstructionFeatures["ExteriorWall"] - wallWidth), 0, 0) : Vector3.zero;

            int[] roomIndices = m3 ? Standards.TaggedObject.ModulesPerTypeM3[apartmentType] : Standards.TaggedObject.ModulesPerType[apartmentType];

            for (int i = 0; i < roomIndices.Length; i++)
            {
                GameObject panel = GameObject.CreatePrimitive(PrimitiveType.Cube);
                float length = 0;
                if (roomIndices[i] == 1)
                    length = lengths[roomIndices[i]][4];
                else
                    length = lengths[roomIndices[i]][0];
                if (i == 0)
                {
                    panel.transform.localScale = !hasLeft ? new Vector3(Mathf.Abs(length) + wallWidth / 2.0f + (Standards.TaggedObject.ConstructionFeatures["ExteriorWall"] - Standards.TaggedObject.ConstructionFeatures["PartyWall"] / 2), totalHeight, wallWidth) : new Vector3(Mathf.Abs(length) + wallWidth / 2.0f, totalHeight, facade ? wallWidth : Standards.TaggedObject.ConstructionFeatures["CorridorWall"]);
                }
                else
                {
                    panel.transform.localScale = new Vector3(Mathf.Abs(length), totalHeight, facade ? wallWidth : Standards.TaggedObject.ConstructionFeatures["CorridorWall"]);
                }
                panel.transform.SetParent(panelsParent);
                var temp_pos1 = new Vector3(localOrigin.x + panel.transform.localScale.x / 2.0f, totalHeight / 2.0f, facade ? outterHeight + panel.transform.localScale.z / 2.0f : Standards.TaggedObject.ConstructionFeatures["CorridorWall"] / 2.0f);
                panel.transform.localPosition = temp_pos1;
                panels.Add(panel);
                localOrigin += new Vector3(panel.transform.localScale.x, 0, 0);
                panel.transform.localScale *= modulesScale;
                panel.name = "Panel" + length.ToString("0.00");
                if (Standards.TaggedObject.panelsMaterials.ContainsKey(panel.name))
                {
                    panelMaterial = Standards.TaggedObject.panelsMaterials[panel.name];
                }
                else
                {
                    panelMaterial = new Material(Resources.Load("Materials/Modules/Orange") as Material);
                    panelMaterial.color = panelsGradient.Evaluate(length / Standards.TaggedObject.ConstructionFeatures["PanelLength"]);
                    Standards.TaggedObject.panelsMaterials.Add(panel.name, panelMaterial);
                }
                panel.GetComponent<MeshRenderer>().material = panelMaterial;
            }

            float leftOver = !hasRight ? m_width + partyWall / 2.0f - localOrigin.x + (Standards.TaggedObject.ConstructionFeatures["ExteriorWall"] - Standards.TaggedObject.ConstructionFeatures["PartyWall"] / 2) : m_width + partyWall / 2.0f - localOrigin.x;
            GameObject livingRoomPanel = GameObject.CreatePrimitive(PrimitiveType.Cube);
            livingRoomPanel.transform.localScale = new Vector3(leftOver, totalHeight, facade ? wallWidth : Standards.TaggedObject.ConstructionFeatures["CorridorWall"]);
            livingRoomPanel.transform.SetParent(panelsParent);
            var temp_pos = new Vector3(localOrigin.x + livingRoomPanel.transform.localScale.x / 2.0f, totalHeight / 2.0f, facade ? outterHeight + livingRoomPanel.transform.localScale.z / 2.0f : Standards.TaggedObject.ConstructionFeatures["CorridorWall"] / 2.0f);
            livingRoomPanel.transform.localPosition = temp_pos;
            livingRoomPanel.transform.localScale *= modulesScale;
            livingRoomPanel.name = "Panel" + leftOver.ToString("0.00");
            panels.Add(livingRoomPanel);
            if (Standards.TaggedObject.panelsMaterials.ContainsKey(livingRoomPanel.name))
            {
                panelMaterial = Standards.TaggedObject.panelsMaterials[livingRoomPanel.name];
            }
            else
            {
                panelMaterial = new Material(Resources.Load("Materials/Modules/Orange") as Material);
                panelMaterial.color = panelsGradient.Evaluate(leftOver / Standards.TaggedObject.ConstructionFeatures["PanelLength"]);
                Standards.TaggedObject.panelsMaterials.Add(livingRoomPanel.name, panelMaterial);
            }
            livingRoomPanel.GetComponent<MeshRenderer>().material = panelMaterial;

            return panels;
        }

        private List<GameObject> GetModulesForApartmentsMansion()
        {
            if (!Standards.TaggedObject.modulesMaterials.ContainsKey("Not Modular"))
            {
                var mat = new Material(Resources.Load("Materials/Modules/Orange") as Material);
                mat.color = Color.gray;
                Standards.TaggedObject.modulesMaterials.Add("Not Modular", mat);
            }
            Vector3 localOrigin = Vector3.zero;
            float m_height = height;
            Material moduleMaterial;

            List<GameObject> modules = new List<GameObject>();

            int[] roomsIndices = Standards.TaggedObject.mansionModules[apartmentType];

            if (roomsIndices.Contains(-1))
            {
                float length = outterWidth * 0.5f;
                for (int i = 0; i < 2; i++)
                {
                    GameObject module = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    module.transform.localScale = new Vector3(length, totalHeight, m_height + corridorWall);
                    module.transform.SetParent(verticalModulesParent);
                    module.transform.localPosition = localOrigin + module.transform.localScale * 0.5f;
                    modules.Add(module);
                    localOrigin += new Vector3(module.transform.localScale.x, 0, 0);
                    module.transform.localScale *= modulesScale;
                    module.name = GetModulesInclusions(module.transform);
                    if (Standards.TaggedObject.modulesMaterials.ContainsKey(module.name))
                    {
                        moduleMaterial = Standards.TaggedObject.modulesMaterials[module.name];
                    }
                    else
                    {
                        moduleMaterial = new Material(Resources.Load("Materials/Modules/Orange") as Material);
                        moduleMaterial.color = panelsGradient.Evaluate(BrydenWoodUtils.Remap(length, 2, 4, 0, 1));//length / Standards.TaggedObject.ConstructionFeatures["PanelLength"]);
                        moduleMaterial.color = new Color(moduleMaterial.color.r, moduleMaterial.color.g, moduleMaterial.color.b, 0.6f);
                        Standards.TaggedObject.modulesMaterials.Add(module.name, moduleMaterial);
                    }
                    module.GetComponent<MeshRenderer>().material = moduleMaterial;
                }
            }
            else
            {
                for (int i = 0; i < roomsIndices.Length; i++)
                {
                    GameObject module = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    float length = lengths[roomsIndices[i]][0];
                    if (i == 0)
                    {
                        if (roomsIndices.Length == 1)
                        {
                            module.transform.localScale = new Vector3(Mathf.Abs(length) + Standards.TaggedObject.ConstructionFeatures["PartyWall"] - wallWidth / 2.0f, totalHeight, m_height + corridorWall);
                        }
                        else
                        {
                            module.transform.localScale = new Vector3(Mathf.Abs(length) + Standards.TaggedObject.ConstructionFeatures["PartyWall"] / 2.0f - wallWidth / 2.0f, totalHeight, m_height + corridorWall);
                        }
                    }
                    else
                    {
                        module.transform.localScale = new Vector3(Mathf.Abs(length) + Standards.TaggedObject.ConstructionFeatures["PartyWall"] / 2.0f, totalHeight, m_height + corridorWall);
                    }
                    module.transform.SetParent(verticalModulesParent);
                    module.transform.localPosition = localOrigin + module.transform.localScale * 0.5f;
                    modules.Add(module);
                    localOrigin += new Vector3(module.transform.localScale.x, 0, 0);
                    module.transform.localScale *= modulesScale;
                    module.name = GetModulesInclusions(module.transform);
                    if (Standards.TaggedObject.modulesMaterials.ContainsKey(module.name))
                    {
                        moduleMaterial = Standards.TaggedObject.modulesMaterials[module.name];
                    }
                    else
                    {
                        moduleMaterial = new Material(Resources.Load("Materials/Modules/Orange") as Material);
                        moduleMaterial.color = panelsGradient.Evaluate(BrydenWoodUtils.Remap(length, 2, 4, 0, 1));//length / Standards.TaggedObject.ConstructionFeatures["PanelLength"]);
                        moduleMaterial.color = new Color(moduleMaterial.color.r, moduleMaterial.color.g, moduleMaterial.color.b, 0.6f);
                        Standards.TaggedObject.modulesMaterials.Add(module.name, moduleMaterial);
                    }
                    module.GetComponent<MeshRenderer>().material = moduleMaterial;
                }
            }

            return modules;
        }

        private List<GameObject> GetModulesForApartment()
        {
            if (!Standards.TaggedObject.modulesMaterials.ContainsKey("Not Modular"))
            {
                var mat = new Material(Resources.Load("Materials/Modules/Orange") as Material);
                mat.color = Color.gray;
                Standards.TaggedObject.modulesMaterials.Add("Not Modular", mat);
            }
            Vector3 localOrigin;
            float m_height;
            Material moduleMaterial;
            if (hasCorridor)
            {
                localOrigin = new Vector3(0, 0, -corridorWidth);
                m_height = height + corridorWidth;
            }
            else
            {
                localOrigin = Vector3.zero;
                m_height = height;
            }

            List<GameObject> modules = new List<GameObject>();

            int[] roomIndices = m3 ? Standards.TaggedObject.ModulesPerTypeM3[apartmentType] : Standards.TaggedObject.ModulesPerType[apartmentType];

            for (int i = 0; i < roomIndices.Length; i++)
            {
                GameObject module = GameObject.CreatePrimitive(PrimitiveType.Cube);
                float length = 0;
                if (roomIndices[i] == 1 || roomIndices[i] == 2)
                    length = lengths[roomIndices[i]][4];
                else
                    length = lengths[roomIndices[i]][0];
                if (i == 0)
                {
                    module.transform.localScale = new Vector3(Mathf.Abs(length) + Standards.TaggedObject.ConstructionFeatures["PartyWall"] / 2.0f - wallWidth / 2.0f, totalHeight, m_height + corridorWall);
                }
                else
                {
                    module.transform.localScale = new Vector3(Mathf.Abs(length), totalHeight, m_height + corridorWall);
                }
                //module.transform.localScale = new Vector3(Mathf.Abs(length)/* + wallWidth / 2.0f*/, totalHeight, m_height + corridorWall);
                module.transform.SetParent(verticalModulesParent);
                module.transform.localPosition = localOrigin + module.transform.localScale * 0.5f;
                modules.Add(module);
                localOrigin += new Vector3(module.transform.localScale.x, 0, 0);
                module.transform.localScale *= modulesScale;
                module.name = GetModulesInclusions(module.transform);
                if (Standards.TaggedObject.modulesMaterials.ContainsKey(module.name))
                {
                    moduleMaterial = Standards.TaggedObject.modulesMaterials[module.name];
                }
                else
                {
                    moduleMaterial = new Material(Resources.Load("Materials/Modules/Orange") as Material);
                    moduleMaterial.color = panelsGradient.Evaluate(BrydenWoodUtils.Remap(length, 2, 4, 0, 1));//length / Standards.TaggedObject.ConstructionFeatures["PanelLength"]);
                    moduleMaterial.color = new Color(moduleMaterial.color.r, moduleMaterial.color.g, moduleMaterial.color.b, 0.6f);
                    Standards.TaggedObject.modulesMaterials.Add(module.name, moduleMaterial);
                }
                module.GetComponent<MeshRenderer>().material = moduleMaterial;
            }

            float leftOver = m_width + partyWall / 2.0f - localOrigin.x;
            GameObject livingRoomModule = GameObject.CreatePrimitive(PrimitiveType.Cube);
            livingRoomModule.transform.localScale = new Vector3(leftOver, totalHeight, m_height + corridorWall);
            livingRoomModule.transform.SetParent(verticalModulesParent);
            livingRoomModule.transform.localPosition = localOrigin + livingRoomModule.transform.localScale * 0.5f;
            livingRoomModule.transform.localScale *= modulesScale;
            livingRoomModule.name = GetModulesInclusions(livingRoomModule.transform);
            modules.Add(livingRoomModule);
            if (Standards.TaggedObject.modulesMaterials.ContainsKey(livingRoomModule.name))
            {
                moduleMaterial = Standards.TaggedObject.modulesMaterials[livingRoomModule.name];
            }
            else
            {
                moduleMaterial = new Material(Resources.Load("Materials/Modules/Orange") as Material);
                moduleMaterial.color = panelsGradient.Evaluate(BrydenWoodUtils.Remap(leftOver, 2, 4, 0, 1));//leftOver / Standards.TaggedObject.ConstructionFeatures["PanelLength"]);
                moduleMaterial.color = new Color(moduleMaterial.color.r, moduleMaterial.color.g, moduleMaterial.color.b, 0.6f);
                Standards.TaggedObject.modulesMaterials.Add(livingRoomModule.name, moduleMaterial);
            }
            livingRoomModule.GetComponent<MeshRenderer>().material = moduleMaterial;

            return modules;
        }

        private string PrepareAptLayoutData(TextAsset aptLayoutData)
        {

            string[] aptDataLines = aptLayoutData.text.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < aptDataLines.Length; i++)
            {
                var cells = aptDataLines[i].Split(',');
                for (int j = 2; j < cells.Length; j++)
                {
                    if (m3)
                    {
                        foreach (var item in Standards.TaggedObject.customValsMansionM3[apartmentType])
                        {
                            cells[j] = cells[j].Replace(item.Key, item.Value.ToString());
                        }
                    }
                    else
                    {
                        foreach (var item in Standards.TaggedObject.customValsMansionM2[apartmentType])
                        {
                            cells[j] = cells[j].Replace(item.Key, item.Value.ToString());
                        }
                    }
                }
                aptDataLines[i] = string.Join(",", cells);
            }

            return string.Join("\r\n", aptDataLines);
        }

        private string GetModulesInclusions(Transform module)
        {
            string m_name = "Module:";
            List<string> completeInclusion = new List<string>();
            List<string> partialInclusion = new List<string>();

            Vector3[] boxOutline = new Vector3[]
            {
                module.rotation*(new Vector3(-module.localScale.x/2,-module.localScale.y/2,-module.localScale.z/2-0.05f)) + module.transform.position,
                module.rotation*(new Vector3(module.localScale.x/2,-module.localScale.y/2,-module.localScale.z/2-0.05f)) + module.transform.position,
                module.rotation*(new Vector3(module.localScale.x/2,-module.localScale.y/2,module.localScale.z/2+0.05f)) + module.transform.position,
                module.rotation*(new Vector3(-module.localScale.x/2,-module.localScale.y/2,module.localScale.z/2+0.05f)) + module.transform.position,
            };

            for (int i = 0; i < aptLayout.roomInclusion.Length; i++)
            {
                if (aptLayout.roomInclusion[i])
                {
                    int inclusionCounter = 0;
                    List<string> vertexInclusion = new List<string>();
                    Mesh m = rooms[i].polygon.GetComponent<MeshFilter>().mesh;
                    for (int j = 0; j < m.vertices.Length; j++)
                    {
                        Vector3 point = rooms[i].polygon.transform.localToWorldMatrix.MultiplyPoint3x4(m.vertices[j]) + new Vector3(0, 0.5f, 0);
                        if (Polygon.IsIncluded(boxOutline, point))
                        {
                            vertexInclusion.Add(j.ToString());
                            inclusionCounter++;
                        }
                    }

                    if (inclusionCounter != 0)
                    {
                        if (inclusionCounter == rooms[i].polygon.Count)
                            completeInclusion.Add(aptLayout.roomNames[i]/* + "-" + String.Join("-", vertexInclusion.ToArray())*/);
                        else
                            partialInclusion.Add(aptLayout.roomNames[i] + "-" + String.Join("-", vertexInclusion.ToArray()));
                    }
                }
            }

            m_name += String.Join("_", completeInclusion.ToArray()) + "|" + String.Join("_", partialInclusion.ToArray());
            return m_name;
        }
        #endregion

        #region Unity Editor
#if UNITY_EDITOR
        /// <summary>
        /// It creates the Apartment Layout asset (Editor only)
        /// </summary>
        public void CreateApartmentLayoutObject()
        {
            AptLayout aptLayout = ScriptableObject.CreateInstance<AptLayout>();
            aptLayout.aptType = this.aptLayout.aptType;//apartmentType + "_" + buildingType;
            aptLayout.roomColors = this.aptLayout.roomColors;
            aptLayout.roomInclusion = this.aptLayout.roomInclusion;
            aptLayout.roomNames = this.aptLayout.roomNames;
            aptLayout.types = this.aptLayout.types.ToArray();
            UnityEditor.AssetDatabase.CreateAsset(aptLayout, "Assets/" + aptLayout.aptType + ".asset");
            UnityEditor.AssetDatabase.SaveAssets();

        }
#endif
        #endregion
    }
}

﻿using BrydenWoodUnity.GeometryManipulation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace BrydenWoodUnity.DesignData
{
    /// <summary>
    /// A UnityEvent class for updated Design Data
    /// </summary>
    [System.Serializable]
    public class UpdatedGeometry : UnityEvent<BaseDesignData>
    {

    }

    /// <summary>
    /// A base class for all the Design Data related MonoBehaviour components
    /// </summary>
    public class BaseDesignData : MonoBehaviour
    {

        #region Events
        /// <summary>
        /// Triggered when the constraints evaluation has changed
        /// </summary>
        public static event OnConstraintsChanged constraintsChanged;
        protected virtual void ConstraintsChanged(IConstrainable sender, bool error)
        {
            if (constraintsChanged != null)
            {
                constraintsChanged(sender, error);
            }
        }

        /// <summary>
        /// Used when the Design Object has been selected
        /// </summary>
        /// <param name="sender">The selected object</param>
        public delegate void OnSelect(BaseDesignData sender);
        /// <summary>
        /// Triggered when the Design Object has been selected
        /// </summary>
        public static event OnSelect selected;
        /// <summary>
        /// Triggered when the Design Object has been deselected
        /// </summary>
        public static event OnSelect deselected;
        protected virtual void OnSelected()
        {
            if (selected != null)
            {
                selected(this);
            }
        }

        protected virtual void OnDeSelected()
        {
            if (deselected!=null)
            {
                deselected(this);
            }
        }

        
        /// <summary>
        /// Triggered when the geometry of the Design Object has been updated
        /// </summary>
        public UpdatedGeometry geometryUpdated;
        protected virtual void OnGeometryUpdated()
        {
            if (geometryUpdated != null)
            {
                geometryUpdated.Invoke(this);
            }
        }
        #endregion

        #region Public Variables
        [HideInInspector]
        public bool isSelected = false;
        [HideInInspector]
        public bool hasExtrusion = false;
        [HideInInspector]
        public bool extrusionCapped = false;
        [HideInInspector]
        public bool extrusionMoveable = false;
        [HideInInspector]
        public bool extrusionEditEdges = false;
        [HideInInspector]
        public bool extrusionVisible = true;
        [HideInInspector]
        public Polygon polygon;
        [HideInInspector]
        public EditableMesh editableMesh;
        #endregion

        #region Public Methods
        /// <summary>
        /// Selects the object
        /// </summary>
        public virtual void Select(bool triggerEvent = true)
        {

        }

        /// <summary>
        /// Deselects the object
        /// </summary>
        public virtual void DeSelect(bool triggerEvent = true)
        {

        }

        /// <summary>
        /// Returns information about the Design Object
        /// </summary>
        /// <returns>String Array</returns>
        public virtual string[] GetInfoText()
        {
            string[] info = new string[2] { "This is the base title", "This is the base data" };
            return info;
        }

        /// <summary>
        /// Returns information about the Design Object
        /// </summary>
        /// <returns>Dictionary</returns>
        public virtual Dictionary<string, object> GetInfoDict()
        {
            Dictionary<string, object> baseInfo = new Dictionary<string, object>();
            return baseInfo;
        }

        /// <summary>
        /// Sets the Polygon geometry of the Design Object
        /// </summary>
        /// <param name="polygon">The geometry</param>
        public virtual void SetPolygon(Polygon polygon)
        {
            this.polygon = polygon;
            //polygon.updated += OnPolygonUpdated;
            polygon.updated.AddListener(OnPolygonUpdated);
        }

        /// <summary>
        /// Sets the Editable Mesh geometry of the Design Object
        /// </summary>
        /// <param name="editableMesh">The geometry</param>
        public virtual void SetEditableMesh(EditableMesh editableMesh)
        {
            this.editableMesh = editableMesh;
            editableMesh.updated += OnEditableMeshUpdated;
            editableMesh.stoppedEditing += OnMeshStoppedEditing;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Called when the polygon geometry has been updated
        /// </summary>
        /// <param name="sender">The updated geometry</param>
        protected virtual void OnPolygonUpdated(Polygon sender)
        {

        }

        /// <summary>
        /// Called when the editable mesh geometry has been updated
        /// </summary>
        /// <param name="mesh">The updated geometry</param>
        protected virtual void OnEditableMeshUpdated(EditableMesh mesh)
        {

        }

        /// <summary>
        /// Called with the editable mesh geometry has stopped being updated
        /// </summary>
        /// <param name="mesh">The geometry that was being updated</param>
        protected virtual void OnMeshStoppedEditing(EditableMesh mesh)
        {

        }
        #endregion
    }
}

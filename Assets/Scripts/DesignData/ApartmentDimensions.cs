﻿using BrydenWoodUnity.DesignData;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BrydenWoodUnity.UIElements;

namespace BrydenWoodUnity.DesignData
{
    /// <summary>
    /// A MonoBehaviour component for handling the dimension display of the apartments
    /// </summary>
    public class ApartmentDimensions : MonoBehaviour
    {
        #region Public Variables
        [Header("Scene References:")]
        public Transform dimensionsCanvasParent;
        public InputField offsetInputField;
        public Camera topProjectionCamera;
        public Camera frontProjectionCamera;
        public Camera backProjectionCamera;
        [Header("Assets References:")]
        public GameObject dimensionPrefab;
        public ApartmentAreaRange[] apartAreas;
        [Header("Settings:")]
        public float lowerOffset = 1.5f;
        public float leftOffset = 1.5f;
        public float scale { get { return CalculateScale(); } }
        [HideInInspector]
        public ProceduralApartmentLayout proceduralApartment;
        #endregion

        #region Private Variables
        private Dictionary<string, DynamicDimensions> m_dimensions { get; set; }
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            proceduralApartment = GetComponent<ProceduralApartmentLayout>();

        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Turn dimensions on or off
        /// </summary>
        /// <param name="toggle">Whether the dimensions should be on or off</param>
        public void ToggleDimensions(bool toggle, ApartmentViewMode viewMode = ApartmentViewMode.Top)
        {
            DeleteDimensions();
            if (toggle)
            {
                switch (viewMode)
                {
                    case ApartmentViewMode.Top:
                        GenerateTopDimensions();//StartCoroutine(ResetDimensions());
                        break;
                    case ApartmentViewMode.Front:
                        GenerateElevationDimensions(WindowType.front);
                        break;
                    case ApartmentViewMode.Back:
                        GenerateElevationDimensions(WindowType.back);

                        break;
                }
                dimensionsCanvasParent.gameObject.SetActive(true);
            }
        }

        /// <summary>
        /// Deletes all apartment dimensions
        /// </summary>
        public void DeleteDimensions()
        {
            if (m_dimensions != null)
            {
                dimensionsCanvasParent.gameObject.SetActive(false);
                m_dimensions = null;
                List<GameObject> objsToDelete = new List<GameObject>();
                foreach (Transform tr in dimensionsCanvasParent)
                {
                    objsToDelete.Add(tr.gameObject);
                }
                for (int i = 0; i < objsToDelete.Count; i++)
                {
                    DestroyImmediate(objsToDelete[i]);
                }
            }
        }

        /// <summary>
        /// Generates the dimmensions for the elevation
        /// </summary>
        /// <param name="windowType">The type of the windows</param>
        public void GenerateElevationDimensions(WindowType windowType)
        {
            if (proceduralApartment.buildingType != BuildingType.Mansion && windowType == WindowType.back)
            {
                return;
            }

            m_dimensions = new Dictionary<string, DynamicDimensions>();
            WindowsPosition window;

            Dictionary<string, Dictionary<string, WindowsPosition>> windowDict = new Dictionary<string, Dictionary<string, WindowsPosition>>();
            if (proceduralApartment.buildingType == BuildingType.Mansion)
            {
                windowDict = Standards.TaggedObject.WindowsPositionsMansion;
            }
            else
            {
                windowDict = Standards.TaggedObject.WindowsPositions;
            }

            if (windowType == WindowType.front && proceduralApartment.buildingType == BuildingType.Linear)
            {
                //-----For Cill------//

                float height = Standards.TaggedObject.GetWindowCill(proceduralApartment.apartmentType, proceduralApartment.buildingType, proceduralApartment.m3);
                if (height != 0)
                {
                    DynamicDimensions dimension = Instantiate(dimensionPrefab, dimensionsCanvasParent).GetComponent<DynamicDimensions>();
                    dimension.gameObject.name = proceduralApartment.apartmentType + "_all_WindowCill_Window";
                    dimension.scale = scale;
                    Vector3 origin = proceduralApartment.transform.position - new Vector3(0.5f, 0, 0);
                    origin = new Vector3(origin.x, origin.y, 0);
                    Vector3 end = origin + Vector3.up * height;
                    end = new Vector3(end.x, end.y, 0);

                    dimension.startAnchor = GetActiveCamera().WorldToScreenPoint(origin);
                    dimension.endAnchor = GetActiveCamera().WorldToScreenPoint(end);
                    dimension.horizontal = true;
                    dimension.enableDrag = true;
                    dimension.upSide_outSide = false;
                    dimension.readOnly = proceduralApartment.readOnly;
                    dimension.SetLine();
                    dimension.valueChanged += OnDimensionChanged;
                    m_dimensions.Add(proceduralApartment.apartmentType + "_all_cill_Window", dimension);
                }
            }

            if (windowType == WindowType.front)
            {
                foreach (var item in windowDict[proceduralApartment.apartmentType])
                {
                    if (item.Key.Contains("LR"))
                    {
                        //--------For Living Room Window------//
                        DynamicDimensions dimension = Instantiate(dimensionPrefab, dimensionsCanvasParent).GetComponent<DynamicDimensions>();
                        dimension.gameObject.name = proceduralApartment.apartmentType + "_" + item.Key + "_WindowLRHeight_Window";
                        dimension.scale = scale;

                        float height = Standards.TaggedObject.GetLivingWindowHeight(proceduralApartment.apartmentType, proceduralApartment.buildingType, proceduralApartment.m3);
                        Vector3 origin = proceduralApartment.transform.position + new Vector3(0.5f + proceduralApartment.outterWidth, 0, 0);
                        origin = new Vector3(origin.x, origin.y, 0);
                        Vector3 end = origin + Vector3.up * height;
                        end = new Vector3(end.x, end.y, 0);

                        dimension.startAnchor = GetActiveCamera().WorldToScreenPoint(origin);
                        dimension.endAnchor = GetActiveCamera().WorldToScreenPoint(end);
                        dimension.horizontal = true;
                        dimension.enableDrag = true;
                        dimension.upSide_outSide = true;
                        dimension.readOnly = proceduralApartment.readOnly;
                        dimension.SetLine();
                        dimension.valueChanged += OnDimensionChanged;
                        m_dimensions.Add(proceduralApartment.apartmentType + "_" + item.Key + "_LRHeight_Window", dimension);
                    }
                }
            }

            if (windowType == WindowType.front && proceduralApartment.buildingType == BuildingType.Linear)
            {
                float height = Standards.TaggedObject.GetWindowHeight(proceduralApartment.apartmentType, proceduralApartment.buildingType, proceduralApartment.m3);
                if (height != 0)
                {
                    //--------For Living Room Window------//
                    DynamicDimensions dimension = Instantiate(dimensionPrefab, dimensionsCanvasParent).GetComponent<DynamicDimensions>();
                    dimension.gameObject.name = proceduralApartment.apartmentType + "_all_WindowHeight_Window";
                    dimension.scale = scale;
                    Vector3 origin = proceduralApartment.transform.position - new Vector3(0.5f, 0, 0) + Vector3.up * Standards.TaggedObject.GetWindowCill(proceduralApartment.apartmentType, proceduralApartment.buildingType, proceduralApartment.m3);
                    origin = new Vector3(origin.x, origin.y, 0);
                    Vector3 end = origin + Vector3.up * height;
                    end = new Vector3(end.x, end.y, 0);

                    dimension.startAnchor = GetActiveCamera().WorldToScreenPoint(origin);
                    dimension.endAnchor = GetActiveCamera().WorldToScreenPoint(end);
                    dimension.horizontal = true;
                    dimension.enableDrag = true;
                    dimension.upSide_outSide = false;
                    dimension.readOnly = proceduralApartment.readOnly;
                    dimension.SetLine();
                    dimension.valueChanged += OnDimensionChanged;
                    m_dimensions.Add(proceduralApartment.apartmentType + "_all_height_Window", dimension);
                }
            }

            for (int i = 0; i < proceduralApartment.rooms.Count; i++)
            {
                if (proceduralApartment.rooms[i].windowtype == windowType)
                {
                    if (windowDict.TryGetValue(proceduralApartment.apartmentType, out Dictionary<string, WindowsPosition> dict))
                    {
                        if (dict.TryGetValue(proceduralApartment.rooms[i].gameObject.name, out window))
                        {
                            if (window.windowType == windowType)
                            {
                                {
                                    //-----From walls------//
                                    DynamicDimensions dimension = Instantiate(dimensionPrefab, dimensionsCanvasParent).GetComponent<DynamicDimensions>();
                                    dimension.gameObject.name = proceduralApartment.apartmentType + "_" + proceduralApartment.rooms[i].gameObject.name + "_FromWall_Window";
                                    dimension.scale = scale;

                                    Vector3 origin = proceduralApartment.rooms[i].window.transform.position - new Vector3(window.width * 0.5f + window.fromWalls - proceduralApartment.wallWidth * 0.5f, 0, 0) + Vector3.down * 1.8f;
                                    origin = new Vector3(origin.x, -0.5f, origin.z);
                                    Vector3 end = origin + Vector3.right * (window.fromWalls - proceduralApartment.wallWidth * 0.5f);
                                    end = new Vector3(end.x, -0.5f, end.z);

                                    dimension.startAnchor = GetActiveCamera().WorldToScreenPoint(origin);
                                    dimension.endAnchor = GetActiveCamera().WorldToScreenPoint(end);
                                    dimension.horizontal = true;
                                    dimension.enableDrag = true;
                                    dimension.upSide_outSide = false;
                                    dimension.readOnly = proceduralApartment.readOnly;
                                    dimension.SetLine();
                                    dimension.valueChanged += OnDimensionChanged;
                                    m_dimensions.Add(proceduralApartment.apartmentType + "_" + proceduralApartment.rooms[i].gameObject.name + "_walls_Window", dimension);
                                }

                                {
                                    //-----Window Width------//
                                    DynamicDimensions dimension = Instantiate(dimensionPrefab, dimensionsCanvasParent).GetComponent<DynamicDimensions>();
                                    dimension.gameObject.name = proceduralApartment.apartmentType + "_" + proceduralApartment.rooms[i].gameObject.name + "_Width_Window";
                                    dimension.scale = scale;

                                    Vector3 origin = proceduralApartment.rooms[i].window.transform.position - new Vector3(window.width * 0.5f, 0, 0) + Vector3.up * 1.8f;
                                    origin = new Vector3(origin.x, proceduralApartment.totalHeight + 0.5f, origin.z);
                                    Vector3 end = origin + Vector3.right * window.width;
                                    end = new Vector3(end.x, proceduralApartment.totalHeight + 0.5f, end.z);

                                    dimension.startAnchor = GetActiveCamera().WorldToScreenPoint(origin);
                                    dimension.endAnchor = GetActiveCamera().WorldToScreenPoint(end);
                                    dimension.horizontal = true;
                                    dimension.enableDrag = true;
                                    dimension.upSide_outSide = true;
                                    dimension.readOnly = proceduralApartment.readOnly;
                                    dimension.SetLine();
                                    dimension.valueChanged += OnDimensionChanged;
                                    m_dimensions.Add(proceduralApartment.apartmentType + "_" + proceduralApartment.rooms[i].gameObject.name + "_width_Window", dimension);
                                }

                                {
                                    //-----Remainder-----//
                                    DynamicDimensions dimension = Instantiate(dimensionPrefab, dimensionsCanvasParent).GetComponent<DynamicDimensions>();
                                    dimension.gameObject.name = proceduralApartment.apartmentType + "_" + proceduralApartment.rooms[i].gameObject.name + "_Remainder_Window";
                                    dimension.scale = scale;
                                    Vector3 delta = Vector3.zero;
                                    if (window.origin > 0)
                                    {
                                        delta = proceduralApartment.rooms[i].vertices[window.origin] - proceduralApartment.rooms[i].vertices[window.origin - 1];
                                    }
                                    else
                                    {
                                        delta = proceduralApartment.rooms[i].vertices[window.origin] - proceduralApartment.rooms[i].vertices[window.origin + 1];
                                    }
                                    float width = delta.magnitude;
                                    Vector3 origin = proceduralApartment.rooms[i].window.transform.position + new Vector3(window.width * 0.5f, 0, 0) + Vector3.down * 1.8f;
                                    origin = new Vector3(origin.x, -0.5f, origin.z);
                                    Vector3 end = origin + Vector3.right * (width - window.fromWalls - window.width - proceduralApartment.wallWidth * 0.5f);
                                    end = new Vector3(end.x, -0.5f, end.z);

                                    dimension.startAnchor = GetActiveCamera().WorldToScreenPoint(origin);
                                    dimension.endAnchor = GetActiveCamera().WorldToScreenPoint(end);
                                    dimension.horizontal = true;
                                    dimension.enableDrag = true;
                                    dimension.upSide_outSide = false;
                                    dimension.readOnly = true;
                                    //dimension.readOnly = true;
                                    dimension.SetLine();
                                    dimension.valueChanged += OnDimensionChanged;
                                    m_dimensions.Add(proceduralApartment.apartmentType + "_" + proceduralApartment.rooms[i].gameObject.name + "_remainder_Window", dimension);
                                }
                            }
                        }
                    }
                }
            }
        }



        /// <summary>
        /// Generates the dimensions of the rooms
        /// </summary>
        public void GenerateTopDimensions()
        {
            m_dimensions = new Dictionary<string, DynamicDimensions>();
            { //For the main width dimension

                DynamicDimensions dimension = Instantiate(dimensionPrefab, dimensionsCanvasParent).GetComponent<DynamicDimensions>();
                dimension.gameObject.name = proceduralApartment.gameObject.name + "_Width";
                dimension.scale = scale;

                Vector3 origin = proceduralApartment.transform.position + new Vector3(Standards.TaggedObject.ConstructionFeatures["PartyWall"] / 2.0f, 0, 0);
                Vector3 end = origin + new Vector3(proceduralApartment.width, 0, 0);

                origin = new Vector3(origin.x, 0, lowerOffset * 2);
                end = new Vector3(end.x, 0, lowerOffset * 2);

                dimension.startAnchor = topProjectionCamera.WorldToScreenPoint(origin);
                dimension.endAnchor = topProjectionCamera.WorldToScreenPoint(end);
                dimension.horizontal = true;
                dimension.enableDrag = true;
                dimension.upSide_outSide = false;
                dimension.readOnly = proceduralApartment.readOnly && proceduralApartment.buildingType != BuildingType.Mansion;
                if (!dimension.readOnly)
                    dimension.SetAsCancelable();
                dimension.SetLine();
                dimension.valueChanged += OnDimensionChanged;
                //quick fix for position
                dimension.GetComponent<RectTransform>().position += new Vector3(0, 9f, 0);
                m_dimensions.Add("width", dimension);
            }

            { //For the main height dimension

                DynamicDimensions dimension = Instantiate(dimensionPrefab, dimensionsCanvasParent).GetComponent<DynamicDimensions>();
                dimension.gameObject.name = proceduralApartment.gameObject.name + "_Depth";
                dimension.scale = scale;

                Vector3 origin = proceduralApartment.transform.position + new Vector3(0, 0, Standards.TaggedObject.ConstructionFeatures["CorridorWall"]);
                Vector3 end = origin + new Vector3(0, 0, proceduralApartment.height);

                origin = new Vector3(leftOffset * 2, 0, origin.z);
                end = new Vector3(leftOffset * 2, 0, end.z);

                dimension.startAnchor = topProjectionCamera.WorldToScreenPoint(origin);
                dimension.endAnchor = topProjectionCamera.WorldToScreenPoint(end);
                dimension.horizontal = true;
                dimension.enableDrag = true;
                dimension.upSide_outSide = true;
                dimension.readOnly = true;
                dimension.SetLine();
                m_dimensions.Add("height", dimension);
            }

            for (int i = 0; i < proceduralApartment.rooms.Count; i++)
            {
                if (proceduralApartment.rooms[i].lengths[0] != 0 && proceduralApartment.rooms[i].showDimension)
                {
                    DynamicDimensions dimension = Instantiate(dimensionPrefab, dimensionsCanvasParent).GetComponent<DynamicDimensions>();
                    dimension.gameObject.name = proceduralApartment.rooms[i].gameObject.name;
                    dimension.scale = scale;

                    Vector3 origin = proceduralApartment.rooms[i].transform.TransformPoint(proceduralApartment.rooms[i].dimensionStart);
                    Vector3 end = proceduralApartment.rooms[i].transform.TransformPoint(proceduralApartment.rooms[i].dimensionEnd);

                    switch (proceduralApartment.rooms[i].dimensionSide)
                    {
                        case 0:
                            origin = new Vector3(leftOffset, 0, origin.z);
                            end = new Vector3(leftOffset, 0, end.z);
                            dimension.upSide_outSide = true;
                            break;
                        case 1:
                            origin = new Vector3(origin.x, 0, lowerOffset);
                            end = new Vector3(end.x, 0, lowerOffset);
                            dimension.upSide_outSide = false;
                            break;
                        case 2:
                            origin = new Vector3(proceduralApartment.width - leftOffset, 0, origin.z);
                            end = new Vector3(proceduralApartment.width - leftOffset, 0, end.z);
                            dimension.upSide_outSide = false;
                            break;
                        case 3:
                            origin = new Vector3(origin.x, 0, proceduralApartment.height - 2 * lowerOffset);
                            end = new Vector3(end.x, 0, proceduralApartment.height - 2 * lowerOffset);
                            dimension.upSide_outSide = true;
                            break;
                    }

                    dimension.startAnchor = topProjectionCamera.WorldToScreenPoint(origin);
                    dimension.endAnchor = topProjectionCamera.WorldToScreenPoint(end);
                    dimension.horizontal = true;
                    dimension.enableDrag = true;
                    if (!proceduralApartment.readOnly)
                    {
                        if (proceduralApartment.buildingType == BuildingType.Mansion)
                        {
                            dimension.readOnly = true;
                        }
                        else
                        {
                            if (dimension.gameObject.name == "DB" ||
                                dimension.gameObject.name == "PB" ||
                                dimension.gameObject.name == "BA" ||
                                dimension.gameObject.name == "BAA" ||
                                dimension.gameObject.name == "WC" ||
                                dimension.gameObject.name == "WCC" ||
                                dimension.gameObject.name == "DBB" ||
                                dimension.gameObject.name == "PBB" ||
                                dimension.gameObject.name == "PSB" ||
                                dimension.gameObject.name == "DBBB" ||
                                dimension.gameObject.name == "SB" )
                            {
                                dimension.readOnly = false;
                            }
                            else
                            {
                                dimension.readOnly = true;
                            }
                        }
                    }
                    else
                    {
                        dimension.readOnly = true;
                    }
                    dimension.SetLine();
                    dimension.valueChanged += OnDimensionChanged;
                    m_dimensions.Add(proceduralApartment.rooms[i].gameObject.name, dimension);
                }
            }
        }
        #endregion

        #region Private Methods

        private IEnumerator UpdateDims(bool front = false)
        {
            yield return StartCoroutine(proceduralApartment.ResetLayout());
            proceduralApartment.Initialize();
            proceduralApartment.ShowOutline(true);
            proceduralApartment.ToggleManufacturingSystem();
            if (front)
            {
                UpdateFrontLines();
            }
            else
            {
                UpdateTopLines();
            }
        }

        private IEnumerator UpdateDims2()
        {
            yield return StartCoroutine(proceduralApartment.ResetLayout());
            proceduralApartment.ReInitialize();
            proceduralApartment.ToggleManufacturingSystem();
            UpdateTopLines();
        }

        private void UpdateFrontLines()
        {
            Dictionary<string, Dictionary<string, WindowsPosition>> windowDict = new Dictionary<string, Dictionary<string, WindowsPosition>>();
            if (proceduralApartment.buildingType == BuildingType.Mansion)
            {
                windowDict = proceduralApartment.m3 ? Standards.TaggedObject.WindowsPositionsMansionM3 : Standards.TaggedObject.WindowsPositionsMansion;
            }
            else
            {
                windowDict = proceduralApartment.m3 ? Standards.TaggedObject.WindowsPositionsM3 : Standards.TaggedObject.WindowsPositions;
            }
            foreach (var item in m_dimensions)
            {
                var dimension = item.Value;
                string[] keys = item.Key.Split('_');
                if (keys[1] == "all")
                {
                    if (keys[2] == "cill")
                    {
                        float height = Standards.TaggedObject.GetWindowCill(proceduralApartment.apartmentType, proceduralApartment.buildingType, proceduralApartment.m3);
                        Vector3 origin = proceduralApartment.transform.position - new Vector3(0.5f, 0, 0);
                        origin = new Vector3(origin.x, origin.y, 0);
                        Vector3 end = origin + Vector3.up * height;
                        end = new Vector3(end.x, end.y, 0);

                        dimension.startAnchor = GetActiveCamera().WorldToScreenPoint(origin);
                        dimension.endAnchor = GetActiveCamera().WorldToScreenPoint(end);
                    }
                    else
                    {
                        float height = Standards.TaggedObject.GetWindowHeight(proceduralApartment.apartmentType, proceduralApartment.buildingType, proceduralApartment.m3);
                        Vector3 origin = proceduralApartment.transform.position - new Vector3(0.5f, 0, 0) + Vector3.up * Standards.TaggedObject.GetWindowCill(proceduralApartment.apartmentType, proceduralApartment.buildingType, proceduralApartment.m3);
                        origin = new Vector3(origin.x, origin.y, 0);
                        Vector3 end = origin + Vector3.up * height;
                        end = new Vector3(end.x, end.y, 0);

                        dimension.startAnchor = GetActiveCamera().WorldToScreenPoint(origin);
                        dimension.endAnchor = GetActiveCamera().WorldToScreenPoint(end);
                    }
                    dimension.SetLine();
                }
                else
                {
                    for (int i = 0; i < proceduralApartment.rooms.Count; i++)
                    {
                        if (proceduralApartment.rooms[i].gameObject.name == keys[1])
                        {
                            WindowsPosition window = windowDict[keys[0]][keys[1]];
                            if (item.Key.Contains("walls"))
                            {
                                Vector3 origin = proceduralApartment.rooms[i].window.transform.position - new Vector3(window.width * 0.5f + window.fromWalls - proceduralApartment.wallWidth * 0.5f, 0, 0) + Vector3.down * 1.8f;
                                origin = new Vector3(origin.x, -0.5f, origin.z);
                                Vector3 end = origin + Vector3.right * (window.fromWalls - proceduralApartment.wallWidth * 0.5f);
                                end = new Vector3(end.x, -0.5f, end.z);

                                dimension.startAnchor = GetActiveCamera().WorldToScreenPoint(origin);
                                dimension.endAnchor = GetActiveCamera().WorldToScreenPoint(end);
                            }
                            else if (item.Key.Contains("width"))
                            {
                                Vector3 origin = proceduralApartment.rooms[i].window.transform.position - new Vector3(window.width * 0.5f, 0, 0) + Vector3.up * 1.8f;
                                origin = new Vector3(origin.x, proceduralApartment.totalHeight + 0.5f, origin.z);
                                Vector3 end = origin + Vector3.right * window.width;
                                end = new Vector3(end.x, proceduralApartment.totalHeight + 0.5f, end.z);

                                dimension.startAnchor = GetActiveCamera().WorldToScreenPoint(origin);
                                dimension.endAnchor = GetActiveCamera().WorldToScreenPoint(end);
                            }
                            else if (item.Key.Contains("LRHeight"))
                            {
                                float height = window.height;
                                Vector3 origin = proceduralApartment.transform.position + new Vector3(0.5f + proceduralApartment.outterWidth, 0, 0);
                                origin = new Vector3(origin.x, origin.y, 0);
                                Vector3 end = origin + Vector3.up * height;
                                end = new Vector3(end.x, end.y, 0);

                                dimension.startAnchor = GetActiveCamera().WorldToScreenPoint(origin);
                                dimension.endAnchor = GetActiveCamera().WorldToScreenPoint(end);
                            }
                            else
                            {
                                Vector3 delta = Vector3.zero;
                                if (window.origin > 0)
                                {
                                    delta = proceduralApartment.rooms[i].vertices[window.origin] - proceduralApartment.rooms[i].vertices[window.origin - 1];
                                }
                                else
                                {
                                    delta = proceduralApartment.rooms[i].vertices[window.origin] - proceduralApartment.rooms[i].vertices[window.origin + 1];
                                }
                                float width = delta.magnitude;
                                Vector3 origin = proceduralApartment.rooms[i].window.transform.position + new Vector3(window.width * 0.5f, 0, 0) + Vector3.down * 1.8f;
                                origin = new Vector3(origin.x, -0.5f, origin.z);
                                Vector3 end = origin + Vector3.right * (width - window.fromWalls - window.width - proceduralApartment.wallWidth * 0.5f);
                                end = new Vector3(end.x, -0.5f, end.z);

                                dimension.startAnchor = GetActiveCamera().WorldToScreenPoint(origin);
                                dimension.endAnchor = GetActiveCamera().WorldToScreenPoint(end);
                            }
                            dimension.SetLine();
                        }
                    }
                }
            }
        }

        private void UpdateTopLines()
        {
            foreach (var item in m_dimensions)
            {
                var dimension = item.Value;
                if (item.Key == "width")
                { //For the main width dimension

                    Vector3 origin = proceduralApartment.transform.position;
                    Vector3 end = proceduralApartment.transform.position + new Vector3(proceduralApartment.width, 0, 0);

                    origin = new Vector3(origin.x, 0, lowerOffset * 2);
                    end = new Vector3(end.x, 0, lowerOffset * 2);

                    dimension.startAnchor = topProjectionCamera.WorldToScreenPoint(origin);
                    dimension.endAnchor = topProjectionCamera.WorldToScreenPoint(end);
                    dimension.SetLine();
                }
                else if (item.Key == "height")
                { //For the main width dimension

                    Vector3 origin = proceduralApartment.transform.position;
                    Vector3 end = proceduralApartment.transform.position + new Vector3(0, 0, proceduralApartment.height);

                    origin = new Vector3(leftOffset * 2, 0, origin.z);
                    end = new Vector3(leftOffset * 2, 0, end.z);

                    dimension.startAnchor = topProjectionCamera.WorldToScreenPoint(origin);
                    dimension.endAnchor = topProjectionCamera.WorldToScreenPoint(end);
                    dimension.SetLine();
                }
                else
                {
                    for (int i = 0; i < proceduralApartment.rooms.Count; i++)
                    {
                        if (proceduralApartment.rooms[i].lengths[0] != 0 && item.Key == proceduralApartment.rooms[i].gameObject.name)
                        {

                            Vector3 origin = proceduralApartment.rooms[i].transform.TransformPoint(proceduralApartment.rooms[i].dimensionStart);
                            Vector3 end = proceduralApartment.rooms[i].transform.TransformPoint(proceduralApartment.rooms[i].dimensionEnd);

                            switch (proceduralApartment.rooms[i].dimensionSide)
                            {
                                case 0:
                                    origin = new Vector3(leftOffset, 0, origin.z);
                                    end = new Vector3(leftOffset, 0, end.z);
                                    dimension.upSide_outSide = true;
                                    break;
                                case 1:
                                    origin = new Vector3(origin.x, 0, lowerOffset);
                                    end = new Vector3(end.x, 0, lowerOffset);
                                    dimension.upSide_outSide = false;
                                    break;
                                case 2:
                                    origin = new Vector3(proceduralApartment.width - leftOffset, 0, origin.z);
                                    end = new Vector3(proceduralApartment.width - leftOffset, 0, end.z);
                                    dimension.upSide_outSide = false;
                                    break;
                                case 3:
                                    origin = new Vector3(origin.x, 0, proceduralApartment.height - lowerOffset * 2);
                                    end = new Vector3(end.x, 0, proceduralApartment.height - lowerOffset * 2);
                                    dimension.upSide_outSide = true;
                                    break;
                            }

                            dimension.startAnchor = topProjectionCamera.WorldToScreenPoint(origin);
                            dimension.endAnchor = topProjectionCamera.WorldToScreenPoint(end);
                            dimension.SetLine();
                        }
                    }
                }
            }
        }

        private float CalculateScale()
        {
            float scale = 1;
            if (topProjectionCamera.gameObject.activeSelf)
            {
                var proj1 = topProjectionCamera.WorldToScreenPoint(Vector3.zero);
                var proj2 = topProjectionCamera.WorldToScreenPoint(new Vector3(1, 0, 0));
                scale = Vector3.Distance(proj1, proj2);
            }
            else if (frontProjectionCamera.gameObject.activeSelf)
            {
                var proj1 = frontProjectionCamera.WorldToScreenPoint(Vector3.zero);
                var proj2 = frontProjectionCamera.WorldToScreenPoint(new Vector3(1, 0, 0));
                scale = Vector3.Distance(proj1, proj2);
            }
            else
            {
                var proj1 = backProjectionCamera.WorldToScreenPoint(Vector3.zero);
                var proj2 = backProjectionCamera.WorldToScreenPoint(new Vector3(1, 0, 0));
                scale = Vector3.Distance(proj1, proj2);
            }
            return scale;
        }

        private void OnDimensionChanged(DynamicDimensions sender)
        {
            var dict = proceduralApartment.m3 ? Standards.TaggedObject.customValsM3 : Standards.TaggedObject.customVals;
            foreach (var item in m_dimensions)
            {
                if (item.Value == sender)
                {
                    if (!item.Key.Contains("Window"))
                    {
                        if (item.Key == "width")
                        {
                            proceduralApartment.width = sender.Value;
                            //for (int i = 0; i < apartAreas.Length; i++)
                            //{
                            //    if (apartAreas[i].type == proceduralApartment.apartmentType)
                            //    {
                            //        apartAreas[i].areaRange.text = (proceduralApartment.width * proceduralApartment.height).ToString();
                            //    }
                            //}
                            if (Standards.TaggedObject.balconies == Balconies.External)
                            {
                                var amenitiesArea = proceduralApartment.AmenitySpace;
                                if (proceduralApartment.m3)
                                {
                                    Standards.TaggedObject.TrySetAmenitiesAreaM3(proceduralApartment.apartmentType, amenitiesArea, false);
                                    Standards.TaggedObject.TrySetDesiredAreaM3(proceduralApartment.apartmentType, (proceduralApartment.width * proceduralApartment.height));
                                }
                                else
                                {
                                    Standards.TaggedObject.TrySetAmenitiesArea(proceduralApartment.apartmentType, amenitiesArea, false);
                                    Standards.TaggedObject.TrySetDesiredArea(proceduralApartment.apartmentType, (proceduralApartment.width * proceduralApartment.height));
                                }
                            }
                            else
                            {
                                var area = proceduralApartment.width * proceduralApartment.height;
                                var amenitiesArea = proceduralApartment.AmenitySpace;
                                if (proceduralApartment.m3)
                                {
                                    Standards.TaggedObject.TrySetAmenitiesAreaM3(proceduralApartment.apartmentType, amenitiesArea, false);
                                    Standards.TaggedObject.TrySetDesiredAreaM3(proceduralApartment.apartmentType, (area - amenitiesArea));
                                }
                                else
                                {
                                    Standards.TaggedObject.TrySetAmenitiesArea(proceduralApartment.apartmentType, amenitiesArea, false);
                                    Standards.TaggedObject.TrySetDesiredArea(proceduralApartment.apartmentType, (area - amenitiesArea));
                                }
                                Debug.Log("Test");
                            }
                        }
                        else if (item.Key == "height")
                        {
                            proceduralApartment.height = sender.Value;
                            offsetInputField.text = proceduralApartment.height.ToString();
                        }
                        else if (item.Key == "DB")
                        {
                            dict[proceduralApartment.apartmentType][0] = sender.Value;
                        }
                        else if (item.Key == "PB")
                        {
                            dict[proceduralApartment.apartmentType][1] = sender.Value;
                        }
                        else if (item.Key == "BA")
                        {
                            dict[proceduralApartment.apartmentType][2] = sender.Value;
                        }
                        else if (item.Key == "BAA")
                        {
                            dict[proceduralApartment.apartmentType][3] = sender.Value;
                        }
                        else if (item.Key == "WC")
                        {
                            dict[proceduralApartment.apartmentType][4] = sender.Value;
                        }
                        else if (item.Key == "WCC")
                        {
                            dict[proceduralApartment.apartmentType][5] = sender.Value;
                        }
                        else if (item.Key == "DBB")
                        {
                            dict[proceduralApartment.apartmentType][6] = sender.Value;
                        }
                        else if (item.Key == "PBB")
                        {
                            dict[proceduralApartment.apartmentType][7] = sender.Value;
                        }
                        else if (item.Key == "PSB")
                        {
                            dict[proceduralApartment.apartmentType][8] = sender.Value;
                        }
                        else if (item.Key == "DBBB")
                        {
                            dict[proceduralApartment.apartmentType][0] = sender.Value;
                        }
                        else if (item.Key == "SB")
                        {
                            dict[proceduralApartment.apartmentType][8] = sender.Value;
                        }
                        StartCoroutine(UpdateDims());
                    }
                    else
                    {
                        string[] keys = sender.gameObject.name.Split('_');
                        string aptType = keys[0];
                        string roomType = keys[1];
                        string lengthType = keys[2];
                        Dictionary<string, Dictionary<string, WindowsPosition>> winDict;
                        if (proceduralApartment.buildingType == BuildingType.Mansion)
                        {
                            winDict = proceduralApartment.m3 ? Standards.TaggedObject.WindowsPositionsMansionM3 : Standards.TaggedObject.WindowsPositionsMansion;
                        }
                        else
                        {
                            winDict = proceduralApartment.m3 ? Standards.TaggedObject.WindowsPositionsM3 : Standards.TaggedObject.WindowsPositions;
                        }
                        WindowsPosition window;
                        if (winDict.TryGetValue(aptType, out Dictionary<string, WindowsPosition> diction))
                        {
                            if (roomType == "all")
                            {
                                switch (lengthType)
                                {

                                    case "WindowHeight":
                                        foreach (var win in diction)
                                        {
                                            if (!win.Key.Contains("LR"))
                                            {
                                                win.Value.height = sender.Value;
                                            }
                                        }
                                        break;
                                    case "WindowCill":
                                        foreach (var win in diction)
                                        {
                                            if (!win.Key.Contains("LR"))
                                            {
                                                win.Value.cill = sender.Value;
                                            }
                                        }
                                        break;
                                }
                            }
                            else
                            {
                                if (diction.TryGetValue(roomType, out window))
                                {
                                    switch (lengthType)
                                    {
                                        case "FromWall":
                                            window.fromWalls = sender.Value;
                                            break;
                                        case "Width":
                                            window.width = sender.Value;
                                            break;
                                        case "WindowLRHeight":
                                            window.height = sender.Value;
                                            break;
                                    }
                                }
                            }
                        }
                        StartCoroutine(UpdateDims(true));
                    }
                    break;
                }
            }
        }

        Camera GetActiveCamera()
        {
            if (frontProjectionCamera.gameObject.activeSelf)
            {
                return frontProjectionCamera;
            }
            else
            {
                return backProjectionCamera;
            }
        }
        #endregion



    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.DesignData
{
    /// <summary>
    /// A MonoBehaviour component for displaying information when hovering over objects in the scene
    /// </summary>
    public class OnObjectHoverOver : MonoBehaviour
    {
        #region Public Variables
        [Tooltip("The text to display on hover-over")]
        public string notificationText;
        #endregion

        #region Private Variables
        private GameObject notificationsUI;
        #endregion

        #region Monobehaviour Methods
        void OnEnable()
        {
            notificationsUI = GameObject.FindGameObjectWithTag("ObjectHoverNotificationsUI");
        }

        private void OnMouseEnter()
        {
            if (notificationsUI != null)
            {
                notificationsUI.GetComponent<RectTransform>().position = Input.mousePosition;
                notificationsUI.transform.GetChild(0).GetComponent<Text>().text = notificationText;
            }
        }

        private void OnMouseExit()
        {
            if (notificationsUI != null)
            {
                notificationsUI.GetComponent<RectTransform>().position = Vector2.one * -10000;
                notificationsUI.transform.GetChild(0).GetComponent<Text>().text = string.Empty;
            }
        }
        #endregion
    }
}

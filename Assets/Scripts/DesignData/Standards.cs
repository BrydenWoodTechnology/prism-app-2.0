﻿using BrydenWoodUnity.UIElements;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BrydenWoodUnity.DesignData
{
    public class WindowsPosition
    {
        public int origin;
        public float fromWalls;
        public float width;
        public float height;
        public float cill;
        public WindowType windowType;
    }
    /// <summary>
    /// A MonoBehaviour which has all the static Standards and General Information
    /// </summary>
    public class Standards : Tagged<Standards>
    {
        public TextAsset mansionM2AptVariables;
        public TextAsset mansionM3AptVariables;
        [SerializeField]
        private AptLayout[] apartmentLayouts;
        /// <summary>
        /// Unity event for when the apartment area ranges have changed
        /// </summary>
        public OnAptAreasChanged onAptsAreasChanged;
        /// <summary>
        /// The procedural building manager of the project
        /// </summary>
        public ProceduralBuildingManager buildingManager;
        /// <summary>
        /// The UI element which handles the construction features
        /// </summary>
        public WallWidth constructionFeaturesElement;
        /// <summary>
        /// The dropdown for the type of amenities in the project
        /// </summary>
        public Dropdown balconiesDropdown;
        /// <summary>
        /// The type of amenities in the project
        /// </summary>
        public Balconies balconies = Balconies.External;

        /// <summary>
        /// The required structural wall dimensions based on height
        /// </summary>
        public float[] structuralWallDimensions;
        /// <summary>
        /// The minimum structural wall thickness in the project
        /// </summary>
        public float minStructuralWallWidth;
        /// <summary>
        /// The different heights for the tower in order to calculate the wall thicknesses and core sizes
        /// </summary>
        public int[] towerHeightsForCores;
        /// <summary>
        /// The required core sizes based on height
        /// </summary>
        public Vector3[] coreDimensions;

        public float roofDepth = 0.5f;
        public InputField roofDepthField;
        public float parapetHeight = 0.3f;
        public InputField parapetHeightField;

        public void SetRoofFieldValue(float roofDepth)
        {
            this.roofDepth = roofDepth;
            roofDepthField.SetValue(roofDepth.ToString("0.00"));
            buildingManager.OnRoofDepthChanged(roofDepth);
        }

        public void SetParapetFieldValue(float parapetHeight)
        {
            this.parapetHeight = parapetHeight;
            parapetHeightField.SetValue(parapetHeight.ToString("0.00"));
            buildingManager.OnParapetHeightChanged(parapetHeight);
        }

        public void SetRoofDepth(string val)
        {
            if (float.TryParse(val, out roofDepth))
            {
                buildingManager.OnRoofDepthChanged(roofDepth);
            }
        }

        public void SetParapetHeight(string val)
        {
            if (float.TryParse(val, out parapetHeight))
            {
                buildingManager.OnParapetHeightChanged(parapetHeight);
            }
        }

        public Dictionary<BuildingType, float[]> CoreSizesPerBuildingType = new Dictionary<BuildingType, float[]>()
        {
            { BuildingType.Linear, new float[] {4.8f,7 } },
            { BuildingType.Tower, new float[] {9.03f,7.71f } },
            { BuildingType.Mansion, new float[] {4.7f,7.8f } },
        };


        /// <summary>
        /// Evaluates the wall thicknesses against the required thickness for the structural walls
        /// </summary>
        public void EvaluateStructuralWalls()
        {
            float maxHeight = float.MinValue;

            for (int i = 0; i < buildingManager.proceduralBuildings.Count; i++)
            {
                if (buildingManager.proceduralBuildings[i].buildingType == BuildingType.Tower)
                    if (maxHeight < buildingManager.proceduralBuildings[i].maxLevel)
                        maxHeight = buildingManager.proceduralBuildings[i].maxLevel;
            }

            float requiredWall = 0.3f;

            if (maxHeight < towerHeightsForCores[0])
            {
                requiredWall = structuralWallDimensions[0];
            }
            else if (maxHeight >= towerHeightsForCores[0] && maxHeight < towerHeightsForCores[1])
            {
                requiredWall = structuralWallDimensions[1];
            }
            else
            {
                requiredWall = structuralWallDimensions[2];
            }

            if (requiredWall > ConstructionFeatures["PartyWall"])
            {
                if (!Notifications.TaggedObject.activeNotifications.Contains("TowerWall"))
                {
                    Notifications.TaggedObject.activeNotifications.Add("TowerWall");
                }
            }
            else
            {
                if (Notifications.TaggedObject.activeNotifications.Contains("TowerWall"))
                {
                    Notifications.TaggedObject.activeNotifications.Remove("TowerWall");
                }
            }
            Notifications.TaggedObject.UpdateNotifications();
        }

        /// <summary>
        /// The widths of the different walls
        /// </summary>
        public Dictionary<string, float> ConstructionFeatures = new Dictionary<string, float>()
        {
            {"PartyWall",0.3f },
            {"ExteriorWall", 0.45f },
            {"CorridorWall", 0.25f },
            {"PanelLength",10.5f },
            {"LineSpan", 6.5f },
        };

        /// <summary>
        /// The exxpanded names of the construction features (for reports)
        /// </summary>
        public Dictionary<string, string> ConstructionFeaturesNames = new Dictionary<string, string>()
        {
            {"PartyWall","Party Wall" },
            {"ExteriorWall", "Exterior Wall" },
            {"CorridorWall", "Corridor Wall" },
            {"PanelLength","Maximum Panel Length" },
            {"LineSpan", "Maximum Line-Load Span" },
        };

        /// <summary>
        /// The dictionary of the construction features for the report
        /// </summary>
        public Dictionary<string, float> ConstructionFeaturesReport
        {
            get
            {
                Dictionary<string, float> _dict = new Dictionary<string, float>();

                foreach (var constr in ConstructionFeatures)
                {
                    _dict.Add(ConstructionFeaturesNames[constr.Key], constr.Value);
                }

                return _dict;
            }
        }

        /// <summary>
        /// A dictionary with the minimum size per apartment type (excluding amenities areas)
        /// </summary>
        public Dictionary<string, double> apartmentTypesMinimumSizes = new Dictionary<string, double>()
        {
            {"Studio",37 },
            {"1b2p",50 },
            {"2b3p",61 },
            {"2b4p",70 },
            {"3b5p",86 },
            {"SpecialApt",10 }
        };

        public Dictionary<string, double> m3ApartmentTypesMinimumSizes = new Dictionary<string, double>()
        {
            {"Studio",37 },
            {"1b2p",59 },
            {"2b3p",78 },
            {"2b4p",87 },
            {"3b5p",105 },
            {"SpecialApt",10 }
        };

        /// <summary>
        /// The amenities areas as stored in run-time
        /// </summary>
        public Dictionary<string, float> amenitiesAreas = new Dictionary<string, float>()
        {
            {"Studio",0 },
            {"1b2p",0 },
            {"2b3p",0 },
            {"2b4p",0 },
            {"3b5p",0 },
            {"SpecialApt",0 }
        };

        /// <summary>
        /// The assigned areas for each of the apartment types
        /// </summary>
        public Dictionary<string, double> DesiredAreas
        {
            get
            {
                if (balconies == Balconies.External)
                {
                    return desiredAreas;
                }
                else
                {
                    Dictionary<string, double> temp = new Dictionary<string, double>();
                    foreach (var item in desiredAreas)
                    {
                        temp.Add(item.Key, item.Value + amenitiesAreas[item.Key]);
                    }
                    return temp;
                }
            }
            set
            {
                desiredAreas = value;
            }
        }

        public Dictionary<string, double> DesiredAreasM3
        {
            get
            {
                if (balconies == Balconies.External)
                {
                    return desiredAreasM3;
                }
                else
                {
                    Dictionary<string, double> temp = new Dictionary<string, double>();
                    foreach (var item in desiredAreasM3)
                    {
                        temp.Add(item.Key, item.Value + amenitiesAreas[item.Key]);
                    }
                    return temp;
                }
            }
            set
            {
                desiredAreasM3 = value;
            }
        }

        private Dictionary<string, double> desiredAreasM3 = new Dictionary<string, double>()
        {
            {"Studio",37 },
            {"1b2p",59 },
            {"2b3p",78 },
            {"2b4p",87 },
            {"3b5p",105 },
            {"SpecialApt",10 }
        };


        private Dictionary<string, double> desiredAreas = new Dictionary<string, double>()
        {
            {"Studio",37 },
            {"1b2p",50 },
            {"2b3p",61 },
            {"2b4p",70 },
            {"3b5p",86 },
            {"SpecialApt",10 }
        };

        /// <summary>
        /// A dictionary with the minimum size per apartment type (including amenities areas)
        /// </summary>
        public Dictionary<string, double> ApartmentTypesMinimumSizes
        {
            get
            {
                if (balconies == Balconies.External)
                {
                    return apartmentTypesMinimumSizes;
                }
                else
                {
                    Dictionary<string, double> temp = new Dictionary<string, double>();
                    foreach (var item in apartmentTypesMinimumSizes)
                    {
                        temp.Add(item.Key, item.Value + amenitiesAreas[item.Key]);
                    }
                    return temp;
                }
            }
        }

        public Dictionary<string, double> ApartmentTypesMinimumSizesM3
        {
            get
            {
                if (balconies == Balconies.External)
                {
                    return m3ApartmentTypesMinimumSizes;
                }
                else
                {
                    Dictionary<string, double> temp = new Dictionary<string, double>();
                    foreach (var item in m3ApartmentTypesMinimumSizes)
                    {
                        temp.Add(item.Key, item.Value + amenitiesAreas[item.Key]);
                    }
                    return temp;
                }
            }
        }

        /// <summary>
        /// A dictionary with the maximum size per apartment type
        /// </summary>
        public Dictionary<string, double> ApartmentTypesMaximumSizes = new Dictionary<string, double>()
        {
            {"Studio",40 },
            {"1b2p",55 },
            {"2b3p",67 },
            {"2b4p",77 },
            {"3b5p",94 },
            {"SpecialApt",10 }
        };

        public Dictionary<string, double> M3ApartmentTypesMaximumSizes = new Dictionary<string, double>()
        {
            {"Studio",40 },
            {"1b2p",70 },
            {"2b3p",90 },
            {"2b4p",100 },
            {"3b5p",120 },
            {"SpecialApt",10 }
        };

        /// <summary>
        /// The number of habitable rooms per apartment type
        /// </summary>
        public Dictionary<string, int> HabitableRoomsPerApartment = new Dictionary<string, int>()
        {
            {"SpecialApt",0 },
            {"Studio", 1 },
            {"1b2p",2 },
            {"2b3p",3 },
            {"2b4p",3 },
            {"3b5p",4 },
        };

        /// <summary>
        /// Presets of wall widths for different manufacturing systems
        /// </summary>
        public Dictionary<string, double[]> ManufacturingPresets = new Dictionary<string, double[]>()
        {
            //Exterior, Party, Corridor, PanelLength, LineSpan
            {"Traditional", new double[] {0.45,0.3,0.25,10.5f,6.5f} },
            {"HRS_Modules", new double[] { 0.413, 0.27, 0.27, 10.5f, 6.5f } },
            {"CLT_Modules", new double[] { 0.453, 0.43, 0.43, 10.5f, 6.5f } },
            {"Steel_Panels", new double[] { 0.475, 0.294, 0.294, 8, 4.6f } },
            {"Timber_Panels", new double[] { 0.453, 0.39, 0.39, 6, 4.6f } },
            {"CLT_Panels", new double[] { 0.453, 0.3, 0.3, 13, 6 } },
            {"Concrete_Panels", new double[] { 0.52, 0.35, 0.35, 8, 8.5 } },
            {"Platforms", new double[] { 0.5, 0.26, 0.26, 4.05, 7.5 } }
        };

        /// <summary>
        /// Standard colours for each apartment type
        /// </summary>
        public Dictionary<string, Color> ApartmentTypesColours = new Dictionary<string, Color>()
        {
            {"SpecialApt",Color.grey },
            {"Studio",new Color(0/255.0f,73/255.0f,73/255.0f)},
            {"1b2p",new Color(0/255.0f,146/255.0f,146/255.0f)},
            {"2b3p", new Color(255/255.0f,109/255.0f,182/255.0f) },
            {"2b4p", new Color(73/255.0f,0/255.0f,146/255.0f) },
            {"3b5p", new Color(182/255.0f,109/255.0f,255/255.0f) },
        };

        /// <summary>
        /// The indices of the vertices to be used for the dimensions
        /// int[0]: start
        /// int[1]: end
        /// int[2]: side of apartment to display the dimension (0:left, 1:under, 2:right, 3:above)
        /// </summary>
        public Dictionary<string, int[]> RoomsDimensionIndices = new Dictionary<string, int[]>()
    {
        {"BA" , new int[3] {0,1,1}},
        {"DB" , new int[3] {5,4,3}},
        {"PB" , new int[3] {5,4,3}},
        {"PBB" , new int[3] {0,1,3}},
        {"DBB" , new int[3] {0,1,3}},
        {"HW" , new int[3] {0,1,1}},
        {"ST" , new int[3] {0,1,1}},
        {"STT" , new int[3] {0,1,1}},
        {"HWW" , new int[3] {0,1,1}},
        {"BAA" , new int[3] {0,1,1}},
        {"WC" , new int[3] {0,1,1}},
        {"WCC" , new int[3] {0,1,1}},
        {"PSB" , new int[3] {0,1,3}},
        {"ADB" , new int[3] {0,1,3}},
        {"ADBB" , new int[3] {0,1,3}},
        {"APB" , new int[3] {0,1,3}},
        {"APBB" , new int[3] {0,1,3}},
        {"APSB" , new int[3] {0,1,3}},
        {"LR" , new int[3] {3,2,3}},
        {"LRR" , new int[3] {3,2,3}},
        {"ALR" , new int[3] {0,1,3}},
        {"ALRR" , new int[3] {0,1,3}},
        {"AILR" , new int[3] {0,1,3}},
        {"AILRR" , new int[3] {0,1,3}},
    };
        /// <summary>
        /// The indices of the vertices to be used for the dimensions
        /// int[0]: start
        /// int[1]: end
        /// int[2]: side of apartment to display the dimension (0:left, 1:under, 2:right, 3:above)
        /// </summary>
        public Dictionary<string, int[]> RoomsDimensionIndicesM3 = new Dictionary<string, int[]>()
    {
        {"BA" , new int[3] {0,1,1}},
        {"DB" , new int[3] {5,4,3}},
        {"PB" , new int[3] {5,4,3}},
        {"PBB" , new int[3] {0,1,3}},
        {"DBB" , new int[3] {0,1,3}},
        {"HW" , new int[3] {0,1,1}},
        {"ST" , new int[3] {0,1,1}},
        {"STT" , new int[3] {0,1,1}},
        {"HWW" , new int[3] {0,1,1}},
        {"BAA" , new int[3] {0,1,1}},
        {"WC" , new int[3] {0,1,1}},
        {"WCC" , new int[3] {0,1,1}},
        {"PSB" , new int[3] {0,1,3}},
        {"ADB" , new int[3] {0,1,3}},
        {"ADBB" , new int[3] {0,1,3}},
        {"APB" , new int[3] {0,1,3}},
        {"APBB" , new int[3] {0,1,3}},
        {"APSB" , new int[3] {0,1,3}},
        {"LR" , new int[3] {3,2,3}},
        {"LRR" , new int[3] {3,2,3}},
        {"ALR" , new int[3] {0,1,3}},
        {"ALRR" , new int[3] {0,1,3}},
        {"AILR" , new int[3] {0,1,3}},
        {"AILRR" , new int[3] {0,1,3}},
        {"DBBB" , new int[3] {5,4,3}},
        {"HWWW" , new int[3] {0,1,1}},
        {"HWWWW" , new int[3] {0,1,1}},
        {"SB" , new int[3] {0,1,3}},
        {"STTT" , new int[3] {0,1,1}},
        {"LRRR" , new int[3] {3,2,3}},
        {"STTTT" , new int[3] {0,1,1}},
        {"LRRRR" , new int[3] {3,2,3}},
        {"ALRRR",new int[3] {3,2,3} },
        {"ALRRRR",new int[3] {3,2,3} },
        {"AILRRR",new int[3] {3,2,3} },
        {"AILRRRR",new int[3] {3,2,3} },
    };

        /// <summary>
        /// The minimum widths for the different rooms
        /// </summary>
        public Dictionary<string, float> MinimumRoomWidths = new Dictionary<string, float>()
    {
        {"BA" , 2},
        {"DB" , 2.75f},
        {"PB" , 2.75f},
        {"PBB" , 2.55f},
        {"DBB" , 2.15f},
        {"HW" , 0.9f},
        {"HWW" , 0.9f},
        {"BAA" , 2},
        {"PSB" , 2.15f},
        {"LR" , 3.2f},
        {"LRR" ,3.2f},
        {"WC" , 1f},
        {"WCC" ,1f},
    };

        /// <summary>
        /// The minimum areas for the different rooms
        /// </summary>
        public Dictionary<string, Dictionary<string, float>> MinimumRoomAreas = new Dictionary<string, Dictionary<string, float>>()
    {
            {"Studio", new Dictionary<string, float>()
                {
                    {"DB" , 11.5f},
                    {"PB" , 11.5f},
                    {"PBB" , 11.5f},
                    {"DBB" , 7.5f},
                    {"PSB" , 7.5f},
                    {"ST" , 1.5f},
                    {"STT" , 1.5f},
                    {"ALR" , 5},
                    {"ALRR" , 5},
                    {"AILR" , 5},
                    {"AILRR" , 5},
                }
            },
            { "1b2p", new Dictionary<string, float>()
                {
                    {"DB" , 11.5f},
                    {"PB" , 11.5f},
                    {"PBB" , 11.5f},
                    {"DBB" , 7.5f},
                    {"PSB" , 7.5f},
                    {"ST" , 1.5f},
                    {"STT" , 1.5f},
                    {"ALR" , 5},
                    {"ALRR" , 5},
                    {"AILR" , 5},
                    {"AILRR" , 5},
                }
            },
            {"2b3p", new Dictionary<string, float>()
                {
                    {"DB" , 11.5f},
                    {"PB" , 11.5f},
                    {"PBB" , 11.5f},
                    {"DBB" , 7.5f},
                    {"PSB" , 7.5f},
                    {"ST" , 2f},
                    {"STT" , 2f},
                    {"ALR" , 6},
                    {"ALRR" , 6},
                    {"AILR" , 6},
                    {"AILRR" , 6},
                }
            },
            {"2b4p", new Dictionary<string, float>()
                {
                    {"DB" , 11.5f},
                    {"PB" , 11.5f},
                    {"PBB" , 11.5f},
                    {"DBB" , 7.5f},
                    {"PSB" , 7.5f},
                    {"ST" , 2f},
                    {"STT" , 2f},
                    {"ALR" , 7},
                    {"ALRR" , 7},
                    {"AILR" , 7},
                    {"AILRR" , 7},
                }
            },
            {"3b5p", new Dictionary<string, float>()
                {
                    {"DB" , 11.5f},
                    {"PB" , 11.5f},
                    {"PBB" , 11.5f},
                    {"DBB" , 7.5f},
                    {"PSB" , 7.5f},
                    {"ST" , 2.5f},
                    {"STT" , 2.5f},
                    {"ALR" , 8},
                    {"ALRR" , 8},
                    {"AILR" , 8},
                    {"AILRR" , 8},
                }
            },
    };

        /// <summary>
        /// Which apartments should display their dimensions
        /// </summary>
        public Dictionary<string, bool> RoomDimensionBools = new Dictionary<string, bool>()
    {
        {"BA" , true},
        {"DB" , true},
        {"PB" , true},
        {"PBB" , true},
        {"DBB" , true},
        {"HW" , true},
        {"ST" , true},
        {"STT" , true},
        {"HWW" , true},
        {"BAA" , true},
        {"WC" , true},
        {"WCC" , true},
        {"PSB" , true},
        {"ADB" , false},
        {"ADBB" , false},
        {"APB" , false},
        {"APBB" , false},
        {"APSB" , false},
        {"LR" , true},
        {"LRR" , true},
        {"ALR" , false},
        {"ALRR" , false},
        {"AILR" , false},
        {"AILRR" , false},
    };

        public Dictionary<string, bool> RoomDimensionBoolsM3 = new Dictionary<string, bool>()
    {
        {"BA" , true},
        {"DB" , true},
        {"PB" , true},
        {"PBB" , true},
        {"DBB" , true},
        {"HW" , true},
        {"ST" , true},
        {"STT" , true},
        {"HWW" , true},
        {"BAA" , true},
        {"WC" , true},
        {"WCC" , true},
        {"PSB" , true},
        {"ADB" , false},
        {"ADBB" , false},
        {"APB" , false},
        {"APBB" , false},
        {"APSB" , false},
        {"LR" , true},
        {"LRR" , true},
        {"ALR" , false},
        {"ALRR" , false},
        {"AILR" , false},
        {"AILRR" , false},
        {"DBBB" , true},
        {"HWWW" , true},
        {"HWWWW" , true},
        {"SB" , true},
        {"STTT" , true},
        {"LRRR" , true},
        {"STTTT" , true},
        {"LRRRR" , true},
        {"ALRRR",false },
            {"ALRRRR",false },
            {"AILRRR",false },
            {"AILRRRR",false },
    };

        /// <summary>
        /// Which apartments should display their dimensions
        /// </summary>
        public Dictionary<string, bool> RoomDimensionBoolsMansion = new Dictionary<string, bool>()
    {
        {"PSB" , true},
        {"DB" , true},
        {"DBB" , true},
        {"PB" , true},
        {"BA" , true},
        {"BAA" , true},
        {"BAAA" , true},
        {"HW" , true},
        {"HWW" , true},
        {"HWWW" , true},
        {"HWWWW" , true},
        {"ST" , true},
        {"LR" , true},
        {"STT" , false},
        {"LRR" , true},
        {"STTT" , false},
        {"LRRR" , true},
        {"BAAAA" , false},
        {"STTTT" , true},
        {"STTA" , true},
        {"LRRRR" , true},
        {"ALR" , false},
        {"ALRR" , false},
        {"ALRRR" , false},
        {"ALRRRR" , false},
    };

        public Dictionary<string, bool> RoomDimensionBoolsMansionM3 = new Dictionary<string, bool>()
    {
        {"PSB" , true},
        {"DB" , true},
        {"PB" , true},
        {"BA" , true},
        {"BAA" , true},
        {"HW" , true},
        {"HWW" , true},
        {"HWWW" , true},
        {"HWWWW" , true},
        {"BAAA" , true},
        {"BAAAA" , true},
        {"TS" , true},
        {"TSS" , true},
        {"TSSS" , false},
        {"TSSSS" , false},
        {"ST" , true},
        {"STT" , true},
        {"STTT" , false},
        {"LR" , true},
        {"LRR" , true},
        {"LRRR" , true},
        {"LRRRR" , true},
        {"ALR" , false},
        {"ALRR" , false},
        {"ALRRR" , false},
        {"ALRRRR" , false},
    };



        /// <summary>
        /// The indices of the vertices to be used for the dimensions of the Mansion Block apartments
        /// int[0]: start
        /// int[1]: end
        /// int[2]: side of apartment to display the dimension (0:left, 1:under, 2:right, 3:above)
        /// </summary>
        public Dictionary<string, int[]> RoomsDimensionIndicesMansion = new Dictionary<string, int[]>()
    {
        {"PSB" , new int[3] {0,1,1}},
        {"DB" , new int[3] {0,1,1}},
        {"DBB" , new int[3] {0,1,1}},
        {"PB" , new int[3] {0,1,1}},

        {"BA" , new int[3] {1,2,2}},
        {"BAA" , new int[3] {1,2,2}},
        {"BAAA" , new int[3] {1,2,2}},

        {"HW" , new int[3] {0,3,0}},
        {"HWW" , new int[3] {0,5,0}},
        {"HWWW" , new int[3] {0,7,0}},
        {"HWWWW" , new int[3] {0,5,0}},

        {"ST" , new int[3] {0,3,0}},
        {"LR" , new int[3] {3,4,2}},
        {"STT" , new int[3] {0,3,0}},
        {"LRR" , new int[3] {3,4,2}},
        {"STTT" , new int[3] {0,3,0}},
        {"LRRR" , new int[3] {5,6,2}},
        {"BAAAA" , new int[3] {0,1,3}},
        {"STTTT" , new int[3] {0,3,0}},
        {"STTA" , new int[3] {0,3,2}},
        {"LRRRR" , new int[3] {5,6,2}},

        {"ALR" , new int[3] {0,1,3}},
        {"ALRR" , new int[3] {0,1,3}},
        {"ALRRR" , new int[3] {0,1,3}},
        {"ALRRRR" , new int[3] {0,1,3}},
    };
        /// <summary>
        /// The indices of the vertices to be used for the dimensions of the Mansion Block M3 apartments
        /// int[0]: start
        /// int[1]: end
        /// int[2]: side of apartment to display the dimension (0:left, 1:under, 2:right, 3:above)
        /// </summary>
        public Dictionary<string, int[]> RoomsDimensionIndicesMansionM3 = new Dictionary<string, int[]>()
    {
        {"PSB" , new int[3] {0,1,1}},
        {"DB" , new int[3] {0,1,1}},
        {"PB" , new int[3] {0,1,1}},

        {"BA" , new int[3] {1,2,2}},
        {"BAA" , new int[3] {1,2,2}},

        {"HW" , new int[3] {0,3,0}},
        {"HWW" , new int[3] {0,7,0}},
        {"HWWW" , new int[3] {0,5,0}},
        {"HWWWW" , new int[3] {0,5,0}},

        {"BAAA" , new int[3] {0,3,0}},
        {"BAAAA" , new int[3] {0,3,0}},

        {"TS" , new int[3] {0,3,0}},
        {"TSS" , new int[3] {0,3,0}},
        {"TSSS" , new int[3] {0,1,1}},
        {"TSSSS" , new int[3] {0,1,1}},
        {"ST" , new int[3] {1,2,2}},
        {"STT" , new int[3] {1,2,2}},
        {"STTT" , new int[3] {0,3,0}},

        {"LR" , new int[3] {5,6,2}},
        {"LRR" , new int[3] {3,4,2}},
        {"LRRR" , new int[3] {5,6,2}},
        {"LRRRR" , new int[3] {7,8,2}},

        {"ALR" , new int[3] {0,1,3}},
        {"ALRR" , new int[3] {0,1,3}},
        {"ALRRR" , new int[3] {0,1,3}},
        {"ALRRRR" , new int[3] {0,1,3}},

    };

        /// <summary>
        /// The full names of each room type
        /// </summary>
        public Dictionary<string, string> RoomNames = new Dictionary<string, string>()
    {
        {"BA" , "Bathroom"},
        {"DB" , "Double Bedroom"},
        {"PB" , "Principal Bedroom"},
        {"PBB" , "Double Bedroom"},
        {"DBB" , "Single Bedroom"},
        {"HW" , "Hall"},
        {"ST" , "Storage"},
        {"STT" , "Storage"},
        {"HWW" , "Hall"},
        {"BAA" , "Bathroom"},
        {"WC" , "WC"},
        {"WCC" , "WC"},
        {"PSB" , "Single Bedroom"},
        {"ADB" , "Amenity"},
        {"ADBB" , "Amenity"},
        {"APB" , "Amenity"},
        {"APBB" , "Amenity"},
        {"APSB" , "Amenity"},
        {"LR" , "Kitchen/Living/Dining"},
        {"LRR" , "Kitchen/Living/Dining"},
        {"ALR" , "Amenity"},
        {"ALRR" , "Amenity"},
        {"AILR" , "Amenity"},
        {"AILRR" , "Amenity"},
    };

        public Dictionary<string, string> RoomNamesM3 = new Dictionary<string, string>()
    {
        {"BA" , "Bathroom"},
        {"DB" , "Double Bedroom"},
        {"PB" , "Principal Bedroom"},
        {"PBB" , "Double Bedroom"},
        {"DBB" , "Single Bedroom"},
        {"HW" , "Hall"},
        {"ST" , "Storage"},
        {"STT" , "Storage"},
        {"HWW" , "Hall"},
        {"BAA" , "Bathroom"},
        {"WC" , "WC"},
        {"WCC" , "WC"},
        {"PSB" , "Single Bedroom"},
        {"ADB" , "Amenity"},
        {"ADBB" , "Amenity"},
        {"APB" , "Amenity"},
        {"APBB" , "Amenity"},
        {"APSB" , "Amenity"},
        {"LR" , "Kitchen/Living/Dining"},
        {"LRR" , "Kitchen/Living/Dining"},
        {"ALR" , "Amenity"},
        {"ALRR" , "Amenity"},
        {"AILR" , "Amenity"},
        {"AILRR" , "Amenity"},
        {"DBBB" , "Double Bedroom"},
        {"HWWW" , "Hall"},
        {"HWWWW" , "Hall"},
        {"SB" , "Single Bedroom"},
        {"STTT" , "Storage"},
        {"LRRR" , "Kitchen/Living/Dining"},
        {"STTTT" , "Storage"},
        {"LRRRR" , "Kitchen/Living/Dining"},
            {"ALRRR","Amenity" },
            {"ALRRRR","Amenity" },
            {"AILRRR","Amenity" },
            {"AILRRRR","Amenity" },
    };

        public Dictionary<string, string> RoomNamesMansion = new Dictionary<string, string>()
    {
        {"PSB" , "Single Bedroom"},
        {"DB" , "Double Bedroom"},
        {"DBB" , "Double Bedroom"},
        {"PB" , "Principal Bedroom"},
        {"BA" , "Bathroom"},
        {"BAA" , "Bathroom"},
        {"BAAA" , "Bathroom"},
        {"HW" , "Hall"},
        {"HWW" , "Hall"},
        {"HWWW" , "Hall"},
        {"HWWWW" , "Hall"},
        {"ST" , "Storage"},
        {"LR" , "Kitchen/Living/Dining"},
        {"STT" , "Storage"},
        {"LRR" , "Kitchen/Living/Dining"},
        {"STTT" , "Storage"},
        {"LRRR" , "Kitchen/Living/Dining"},
        {"BAAAA" , "Bathroom"},
        {"STTTT" , "Storage"},
        {"STTA" , "Storage"},
        {"LRRRR" , "Kitchen/Living/Dining"},
        {"ALR" , "Amenity"},
        {"ALRR" , "Amenity"},
        {"ALRRR" , "Amenity"},
        {"ALRRRR" , "Amenity"},
    };

        public Dictionary<string, string> RoomNamesMansionM3 = new Dictionary<string, string>()
    {
        {"PSB" , "Single Bedroom"},
        {"DB" , "Double Bedroom"},
        {"PB" , "Principal Bedroom"},
        {"BA" , "Bathroom"},
        {"BAA" , "Bathroom"},
        {"HW" , "Hall"},
        {"HWW" , "Hall"},
        {"HWWW" , "Hall"},
        {"HWWWW" , "Hall"},
        {"BAAA" , "Bathroom"},
        {"BAAAA" , "Bathroom"},
        {"TS" , "Storage"},
        {"TSS" , "Storage"},
        {"TSSS" , "Storage"},
        {"TSSSS" , "Storage"},
        {"ST" , "Storage"},
        {"STT" , "Storage"},
        {"STTT" , "Storage"},
        {"LR" , "Kitchen/Living/Dining"},
        {"LRR" , "Kitchen/Living/Dining"},
        {"LRRR" , "Kitchen/Living/Dining"},
        {"LRRRR" , "Kitchen/Living/Dining"},
        {"ALR" , "Amenity"},
        {"ALRR" , "Amenity"},
        {"ALRRR" , "Amenity"},
        {"ALRRRR" , "Amenity"},
    };

        public Dictionary<string, int[]> mansionModules = new Dictionary<string, int[]>()
        {
            {"1b2p", new int[] {1} },
            {"2b3p", new int[] {0,1} },
            {"2b4p", new int[] {2,3} },
            {"3b5p", new int[] {-1} },
        };

        /// <summary>
        /// The modules indices per apartment type
        /// </summary>
        public Dictionary<string, int[]> ModulesPerType = new Dictionary<string, int[]>()
        {
            {"Studio",new int[] {0} },
            {"1b2p", new int[] {1} },
            {"2b3p", new int[] {1,4} },
            {"2b4p", new int[] {2,3} },
            {"3b5p", new int[] {2,3,12} }
        };

        public Dictionary<string, int[]> ModulesPerTypeM3 = new Dictionary<string, int[]>()
        {
            {"Studio",new int[] {0} },
            {"1b2p", new int[] {24} },
            {"2b3p", new int[] {24,27} },
            {"2b4p", new int[] {2,3} },
            {"3b5p", new int[] {2,3,12} }
        };

        /// <summary>
        /// The Line-Load indices per apartment types
        /// </summary>
        public Dictionary<string, string[]> LineLoadsPerType = new Dictionary<string, string[]>()
        {
            {"Studio",new string[] {"BA"} },
            {"1b2p", new string[] {"BA","DB", "LR" } },
            {"2b3p", new string[] { "BA", "DB","DBB", "LR" } },
            {"2b4p", new string[] { "PB", "PBB", "STT", "LRR" } },
            {"3b5p", new string[] { "PB", "PBB", "PSB", "STT","BAA", "LRR" } }
        };

        /// <summary>
        /// The starting point of the line loads of each room
        /// </summary>
        public Dictionary<string, int[][]> lineLoadStart = new Dictionary<string, int[][]>()
    {
        {"BA" , new int[][] { new int[] { 1, 0 } } },
        {"DB" , new int[][] { new int[] { 3, 1 } }},
        {"PB" , new int[][] { new int[] { 3, 1 } }},
        {"PBB" , new int[][] { new int[] { 1, 1 } }},
        {"DBB" , new int[][] { new int[] { 1, 1 } }},
        {"BAA" , new int[][] { new int[] { 1, 0 } } },
        {"STT" , new int[][] { new int[] { 1, 0 } } },
        {"PSB" , new int[][] { new int[] { 1, 1 } }},
        {"LR" , new int[][] { new int[] { 5, 0 } }},
        {"LRR" , new int[][] { new int[] { 5, 0 } }},

    };

        /// <summary>
        /// The colors for each of the different module types
        /// </summary>
        public Dictionary<string, Color> ModuleTypeColours = new Dictionary<string, Color>()
    {
        {"DBModule",/*new Color(1,0.5f,0)*/ new Color(255/255.0f,182/255.0f,119/255.0f)},
        {"PBModule", /*new Color(1,0.5f,0)*/ new Color(233/255.0f,179/255.0f,77/255.0f)},
        {"DBBModule", /*Color.blue */ new Color(108/255.0f,164/255.0f,166/255.0f)},
        {"PSBModule", /*Color.blue */new Color(108/255.0f,164/255.0f,166/255.0f)},
        {"PBBModule", /*Color.cyan*/ new Color(240/255.0f,211/255.0f,133/255.0f) },
        {"LivingRoomModule", /*Color.grey*/ new Color(158/255.0f,201/255.0f,203/255.0f) },
        {"Not Modular", Color.gray }
    };

        /// <summary>
        /// The list of rooms for each apartment type
        /// </summary>
        public Dictionary<string, bool[]> ApartmentTypeRooms = new Dictionary<string, bool[]>()
        {
            {"SpecialApt", new bool[] { } },
            {"Studio", new bool[] { true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false } },
            {"1b2p", new bool[] { true, true, false, false, false, true,true, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false}},
            {"2b3p", new bool[] { true, true, false, false, true, true, true, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false} },
            {"2b4p", new bool[] { true, false, true, true, false, false, false, true, true, true, false, false, false, false, false, false, false, false, false, true, false, false, false, false} },
            {"3b5p", new bool[] { true, false, true, true, false, false, false, true, true, true, false, true, true, false, false, false, false, false, false, true, false, false, false, false}},
        };

        public Dictionary<string, bool[]> ApartmentTypeRoomsM3 = new Dictionary<string, bool[]>()
        {
            {"SpecialApt", new bool[] { } },
            {"Studio", new bool[] { true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false,false,false,false,false } },
            {"1b2p", new bool[] { true,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,true,true,false,false,true,true,false,false,false,false,false,false}},
            {"2b3p", new bool[] { true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, true, true, false, false, true, true, false, false, false, false } },
            {"2b4p", new bool[] { true, false, true, true, false, false, false, true, true, true, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false } },
            {"3b5p", new bool[] { true,false,true,true,false,false,true,true,true,true,false,true,true,false,false,false,false,false,false,true,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false}},
        };

        public Dictionary<string, bool[]> ApartmentTypeRoomsMansion = new Dictionary<string, bool[]>()
        {
            {"SpecialApt", new bool[] { } },
            {"1b2p", new bool[] { false,true,false,false,true,false,false,true,false,false,false,true,true,false,false,false,false,false,false,false,false, false, false, false, false}},
            {"2b3p", new bool[] { true, true, false, false, true, false, false, false, true, false, false, false, false, true, true, false, false, false, false, false, false, false, false, false, false } },
            {"2b4p", new bool[] { false, false, true, true, false, true, true, false, false, true, false, false, false, false, false, true, true, false, false, false, false, false, false, false, false } },
            {"3b5p", new bool[] { true,false,true,true,false,true,false,false,false,false,true,false,false,false,false,false,false,true,true,true,true, false, false, false, false}},
        };

        public Dictionary<string, bool[]> ApartmentTypeRoomsMansionM3 = new Dictionary<string, bool[]>()
        {
            {"SpecialApt", new bool[] { } },
            {"1b2p", new bool[] { false,true,false,true,false,true,false,false,false,false,false,true,false,false,false,true,false,false,true,false,false,false,false,false,false,false}},
            {"2b3p", new bool[] { true, true, false, true, false, false, true, false, false, false, false, false, true, false, false, true, false, false, false, true, false, false, false, false, false, false } },
            {"2b4p", new bool[] { false, true, true, false, true, false, false, true, false, true, false, false, false, true, false, false, true, false, false, false, true, false, false, false, false, false } },
            {"3b5p", new bool[] { true,true,true,false,true,false,false,false,true,false,true,false,false,false,true,false,false,true,false,false,false,true,false,false,false,false}},
        };

        public Dictionary<BuildingType, Dictionary<string, AptLayout>> aptLayouts;

        /// <summary>
        /// The minimum sizes for each room
        /// </summary>
        public Dictionary<string, double> RoomTypesMinimumSizes = new Dictionary<string, double>()
    {
        { "DoubleBedroom", 11.50 },
        {"Bathroom", 4.30 },
        {"LivingRoom", 16.34 }
    };

        /// <summary>
        /// The list of rooms which have amenities and the index of the corresponding amenities
        /// </summary>
        public Dictionary<string, Dictionary<int, int>> amenitiesIndices = new Dictionary<string, Dictionary<int, int>>()
    {
        {"Exterior" ,//new int[] { 2, 13,5,14,3,15,4,16,12,17,20,22,21,23} },
            new Dictionary<int, int>()
            {
                { 1,13 },
                { 4,14 },
                { 2,15 },
                { 3,16 },
                { 12,17 },
                { 18,20 },
                { 19,21 }
            }
        },
        {"Interior" ,
            new Dictionary<int, int>()
            {
                { 18,22 },
                { 19,23 }
            }
        }//new int[] { 20,18,21,19} }
    };

        public Dictionary<string, Dictionary<int, int>> amenitiesIndicesM3 = new Dictionary<string, Dictionary<int, int>>()
    {
        {"Exterior" ,//new int[] { 2, 13,5,14,3,15,4,16,12,17,20,22,21,23} },
            new Dictionary<int, int>()
            {
                { 1,13 },
                { 4,14 },
                { 2,15 },
                { 3,16 },
                { 12,17 },
                { 18,20 },
                { 19,21 },
                { 29,32 },
                { 31, 33 }
            }
        },
        {"Interior" ,
            new Dictionary<int, int>()
            {
                { 18,22 },
                { 19,23 },
                { 29,34 },
                { 31, 35 }
            }
        }//new int[] { 20,18,21,19} }
    };

        public Dictionary<string, Dictionary<int, int>> amenitiesIndicesMansion = new Dictionary<string, Dictionary<int, int>>()
    {
        {"Exterior" ,//new int[] { 2, 13,5,14,3,15,4,16,12,17,20,22,21,23} },
            new Dictionary<int, int>()
            {
                { 12,21 },
                { 14,22 },
                { 16,23 },
                { 20,24 }
            }
        },
    };

        public Dictionary<string, Dictionary<int, int>> amenitiesIndicesMansionM3 = new Dictionary<string, Dictionary<int, int>>()
    {
        {"Exterior" ,//new int[] { 2, 13,5,14,3,15,4,16,12,17,20,22,21,23} },
            new Dictionary<int, int>()
            {
                { 18,22 },
                { 19,23 },
                { 20,24 },
                { 21,25 }
            }
        },
    };

        /// <summary>
        /// The colours of the rooms
        /// </summary>
        public Dictionary<string, Color> RoomTypesColors = new Dictionary<string, Color>()
    {
        { "DoubleBedroom", Color.cyan},
        {"Bathroom", Color.yellow },
        {"LivingRoom", new Color(253/255.0f,106/255.0f,2/255.0f) },

    };

        /// <summary>
        /// A list of all the valid load lines
        /// </summary>
        public Dictionary<string, List<string>> ValidLineLoads = new Dictionary<string, List<string>>()
        {

        };

        /// <summary>
        /// A list of all the panels materials
        /// </summary>
        public Dictionary<string, Material> panelsMaterials = new Dictionary<string, Material>();

        public Gradient platformsColours;

        /// <summary>
        /// A list of all the modules materials
        /// </summary>
        public Dictionary<string, Material> modulesMaterials = new Dictionary<string, Material>();

        /// <summary>
        /// The minimum areas for amenities per apartment type
        /// </summary>
        public Dictionary<string, float> MinimumAmenitiesArea = new Dictionary<string, float>()
        {
            {"Studio",5f },
            {"1b2p",5f },
            {"2b3p",6f },
            {"2b4p",7f },
            {"3b5p",8f },
            {"SpecialApt",0 }
        };

        /// <summary>
        /// The total amenities areas per apartment type
        /// </summary>
        public Dictionary<string, Dictionary<string, float>> TotalAmenitiesArea = new Dictionary<string, Dictionary<string, float>>()
        {
            {"Studio", new Dictionary<string, float>()
                {
                    { "ALR",0 },
                    {"AILR",0 }
                }
            },
            {"1b2p", new Dictionary<string, float>()
                {
                    { "ADB",0 },
                    { "ALR",0 },
                    {"AILR",0 }
                }
            },
            {"2b3p", new Dictionary<string, float>()
                {
                    { "ADBB",0 },
                    { "ADB",0 },
                    { "ALR",0 },
                    {"AILR",0 }
                }
            },
            {"2b4p", new Dictionary<string, float>()
                {
                    { "APBB",0 },
                    { "APB",0 },
                    { "ALRR",0 },
                    {"AILRR",0 }
                }
            },
            {"3b5p", new Dictionary<string, float>()
                {
                    { "APBB",0 },
                    { "APB",0 },
                    {"APSB",0 },
                    { "ALRR",0 },
                    {"AILRR",0 }
                }
            },
        };

        /// <summary>
        /// Evaluates the Amenities areas for each apartment type
        /// </summary>
        public void EvaluateAmenitiesArea()
        {

            List<string> aptTypes = new List<string>();

            foreach (var apt in TotalAmenitiesArea)
            {
                var totalArea = 0f;
                foreach (var room in apt.Value)
                {
                    totalArea += room.Value;
                }
                if (totalArea < MinimumAmenitiesArea[apt.Key])
                {
                    aptTypes.Add(apt.Key);
                }
            }

            if (aptTypes.Count > 0)
            {
                Notifications.TaggedObject.SetNotificationBody("Amenity", string.Format("Amenity in apartments {0} is too small!", String.Join(", ", aptTypes.ToArray())));
                if (!Notifications.TaggedObject.activeNotifications.Contains("Amenity"))
                {
                    Notifications.TaggedObject.activeNotifications.Add("Amenity");
                }
            }
            else
            {
                if (Notifications.TaggedObject.activeNotifications.Contains("Amenity"))
                {
                    Notifications.TaggedObject.activeNotifications.Remove("Amenity");
                }
            }
        }

        /// <summary>
        /// The maximum density per PTAL zone
        /// </summary>
        public Dictionary<string, float> ptalZones = new Dictionary<string, float>()
        {
            {"0",  110},
            {"1a",110 },
            {"1b",110 },
            {"2", 240 },
            {"3",240 },
            {"4",405 },
            {"5",405 },
            {"6a",405 },
            {"6b",405 }
        };

        /// <summary>
        /// Evaluates the widths of the different rooms
        /// </summary>
        public void EvaluateStandardRooms()
        {
            string[] _roomNames = new string[] { "DB", "PB", "BA", "BAA", "WC", "WCC", "DBB", "PBB", "PSB" };
            string[] aptTypes = customVals.Keys.ToArray();
            for (int i = 0; i < customVals["Studio"].Length; i++)
            {
                bool widthIssue = false;
                List<string> aptsWithIssues = new List<string>();
                for (int j = 0; j < aptTypes.Length; j++)
                {
                    if (customVals[aptTypes[j]][i] < MinimumRoomWidths[_roomNames[i]])
                    {
                        widthIssue = true;
                        aptsWithIssues.Add(aptTypes[j]);
                    }
                }

                if (widthIssue)
                {
                    Notifications.TaggedObject.SetNotificationBody(_roomNames[i], string.Format("The {0} in apartments {1} is too narrow!", RoomNames[_roomNames[i]], String.Join(", ", aptsWithIssues.ToArray())));
                    if (!Notifications.TaggedObject.activeNotifications.Contains(_roomNames[i]))
                    {
                        Notifications.TaggedObject.activeNotifications.Add(_roomNames[i]);
                    }
                }
                else
                {
                    if (Notifications.TaggedObject.activeNotifications.Contains(_roomNames[i]))
                    {
                        Notifications.TaggedObject.activeNotifications.Remove(_roomNames[i]);
                    }
                }
            }
            Notifications.TaggedObject.UpdateNotifications();
        }

        /// <summary>
        /// The flexible room lengths for each apartment type
        /// </summary>
        public Dictionary<string, float[]> customVals = new Dictionary<string, float[]>()
        {
            //db,pb,ba,baa,wc,wcc,dbb,pbb,psb
            {"SpecialApt",new float[] {3,3,2.25f,2.1f,1.35f,1.35f, 2.4f, 3.6f, 2.4f} },
            {"Studio", new float[] {3,3,2.25f,2.1f,1.35f,1.35f, 2.4f, 3.6f, 2.4f} },
            {"1b2p", new float[] {3,3,2.25f,2.1f,1.35f,1.35f, 2.4f, 3.6f, 2.4f} },
            {"2b3p", new float[] {3,3, 2.25f, 2.1f, 1.35f, 1.35f, 2.4f, 3.6f, 2.4f} },
            {"2b4p", new float[] {3,3, 2.25f, 2.1f, 1.35f, 1.35f, 3.65f, 3.6f, 2.4f} },
            {"3b5p", new float[] {3,3, 2.25f, 2.1f, 1.35f, 1.35f, 3.65f, 3.6f, 2.4f} },
        };
        public Dictionary<string, float[]> customValsM3 = new Dictionary<string, float[]>()
        {
            //db,pb,ba,baa,wc,wcc,dbb,pbb,psb, hwVal
            {"SpecialApt",new float[] {3.15f, 3, 2.6f, 2.75f, 1.35f, 1.35f, 2.4f, 3.65f, 2.6f, 2.2f} },
            {"Studio", new float[] { 3.15f, 3, 2.6f, 2.75f, 1.35f, 1.35f, 2.4f, 3.65f, 2.6f, 2.2f } },
            {"1b2p", new float[] { 3.15f, 3, 2.6f, 2.75f, 1.35f, 1.35f, 2.4f, 3.65f, 2.6f, 2.2f } },
            {"2b3p", new float[] { 3.15f, 3, 2.75f, 2.75f, 1.35f, 1.35f, 2.4f, 3.65f, 2.8f, 2.2f } },
            {"2b4p", new float[] { 3.15f, 3.65f, 2.6f, 2.75f, 1.35f, 1.35f, 2.4f, 3.75f, 2.6f, 2.3f } },
            {"3b5p", new float[] { 3.15f, 3.15f, 2.6f, 2.75f, 1.35f, 1.95f, 2.4f, 3.75f, 2.75f, 4.7f } },
        };
        public Dictionary<string, Dictionary<string, float>> customValsMansionM2;
        public Dictionary<string, Dictionary<string, float>> customValsMansionM3;

        public Dictionary<string, bool> roomsWindows = new Dictionary<string, bool>()
        {
            {"BA" ,false},
        {"DB" , true},
        {"PB" , true},
        {"PBB" , true},
        {"DBB" , true},
        {"HW" , false},
        {"ST" , false},
        {"STT" , false},
        {"HWW" , false},
        {"BAA" , false},
        {"WC" , false},
        {"WCC" , false},
        {"PSB" , true},
        {"ADB" , false},
        {"ADBB" , false},
        {"APB" , false},
        {"APBB" , false},
        {"APSB" , false},
        {"LR" , true},
        {"LRR" , true},
        {"ALR" , false},
        {"ALRR" , false},
        {"AILR" , false},
        {"AILRR" , false},
        };

        public Dictionary<string, bool> roomsWindowsM3 = new Dictionary<string, bool>()
        {
            {"BA" ,false},
        {"DB" , true},
        {"PB" , true},
        {"PBB" , true},
        {"DBB" , true},
        {"HW" , false},
        {"ST" , false},
        {"STT" , false},
        {"HWW" , false},
        {"BAA" , false},
        {"WC" , false},
        {"WCC" , false},
        {"PSB" , true},
        {"ADB" , false},
        {"ADBB" , false},
        {"APB" , false},
        {"APBB" , false},
        {"APSB" , false},
        {"LR" , true},
        {"LRR" , true},
        {"ALR" , false},
        {"ALRR" , false},
        {"AILR" , false},
        {"AILRR" , false},
        {"DBBB" , true},
        {"HWWW" , false},
        {"HWWWW" , false},
        {"SB" , true},
        {"STTT" , false},
        {"LRRR" , true},
        {"STTTT" , false},
        {"LRRRR" , true},
        {"ALRRR",false },
            {"ALRRRR",false },
            {"AILRRR",false },
            {"AILRRRR",false },
        };

        public Dictionary<string, bool> roomsWindowsMansion = new Dictionary<string, bool>()
        {
        {"PSB" ,true},
        {"DB" , true},
        {"DBB" , true},
        {"PB" , true},
        {"BA" , false},
        {"BAA" , false},
        {"HW" , false},
        {"HWW" , false},
        {"HWWW" , false},
        {"HWWWW" , false},
        {"ST" , false},
        {"LR" , true},
        {"STT" , false},
        {"LRR" , true},
        {"STTT" , false},
        {"LRRR" , true},
        {"BAAAA" , false},
        {"STTTT" , false},
        {"STTA" , false},
        {"LRRRR" , true},
        };

        public Dictionary<string, bool> roomsWindowsMansionM3 = new Dictionary<string, bool>()
        {
        {"SB" ,true},
        {"DB" , true},
        {"PB" , true},
        {"BA" , false},
        {"BAA" , false},
        {"HW" , false},
        {"HWW" , false},
        {"HWWW" , false},
        {"HWWWW" , false},
        {"BAAA" , false},
        {"BAAAA" , false},
        {"TS" , false},
        {"TSS" , false},
        {"TSSS" , false},
        {"TSSSS" , false},
        {"ST" , false},
        {"STT" , false},
        {"STTT" , false},
        {"LR" , true},
        {"LRR" , true},
        {"LRRR" , true},
        {"LRRRR" , true},
        };

        private Dictionary<string, float> windowCill = new Dictionary<string, float>()
        {
            {"Studio", 0 },
            {"1b2p", 0.8f },
            {"2b3p", 0.8f },
            {"2b4p", 0.8f },
            {"3b5p", 0.8f },
        };

        private Dictionary<string, float> LRWindowHeight = new Dictionary<string, float>()
        {
            {"Studio", 2.45f },
            {"1b2p", 2.45f },
            {"2b3p", 2.45f },
            {"2b4p", 2.45f },
            {"3b5p", 2.45f },
        };

        private Dictionary<string, float> windowHeight = new Dictionary<string, float>()
        {
            {"Studio", 0 },
            {"1b2p", 1.65f },
            {"2b3p", 1.65f },
            {"2b4p", 1.65f },
            {"3b5p", 1.65f },
        };

        public void SetWindowCill(string key, float value)
        {
            windowCill[key] = value;
            foreach (var item in WindowsPositions[key])
            {
                item.Value.cill = value;
            }
        }

        public float GetWindowCill(string key, BuildingType buildingType, bool m3)
        {
            if (buildingType == BuildingType.Mansion)
            {
                if (m3)
                {
                    float val = 0;
                    foreach (var item in WindowsPositionsMansionM3[key])
                    {
                        if (!item.Key.Contains("LR"))
                        {
                            val = item.Value.cill;
                            break;
                        }
                        else
                        {
                            val = 0;
                        }
                    }
                    return val;
                }
                else
                {
                    float val = 0;
                    foreach (var item in WindowsPositionsMansion[key])
                    {
                        if (!item.Key.Contains("LR"))
                        {
                            val = item.Value.cill;
                            break;
                        }
                        else
                        {
                            val = 0;
                        }
                    }
                    return val;
                }
            }
            else
            {
                float val = 0;
                foreach (var item in WindowsPositions[key])
                {
                    if (!item.Key.Contains("LR"))
                    {
                        val = item.Value.cill;
                        break;
                    }
                    else
                    {
                        val = 0;
                    }
                }
                return val;
            }
        }

        public void SetLivingWindowHeight(string key, float value)
        {
            LRWindowHeight[key] = value;
            foreach (var item in WindowsPositions[key])
            {
                if (item.Key == "LR" || item.Key == "LRR")
                    item.Value.height = value;
            }
        }

        public float GetLivingWindowHeight(string key, BuildingType buildingType, bool m3)
        {
            if (buildingType == BuildingType.Mansion)
            {
                if (m3)
                {
                    float val = 0;
                    foreach (var item in WindowsPositionsMansionM3[key])
                    {
                        if (item.Key.Contains("LR"))
                        {
                            val = item.Value.height;
                            break;
                        }
                        else
                        {
                            val = 0;
                        }
                    }
                    return val;
                }
                else
                {
                    float val = 0;
                    foreach (var item in WindowsPositionsMansion[key])
                    {
                        if (item.Key.Contains("LR"))
                        {
                            val = item.Value.height;
                            break;
                        }
                        else
                        {
                            val = 0;
                        }
                    }
                    return val;
                }
            }
            else
            {
                if (m3)
                {
                    float val = 0;
                    foreach (var item in WindowsPositionsM3[key])
                    {
                        if (item.Key.Contains("LR"))
                        {
                            val = item.Value.height;
                            break;
                        }
                        else
                        {
                            val = 0;
                        }
                    }
                    return val;
                }
                else
                {
                    float val = 0;
                    foreach (var item in WindowsPositions[key])
                    {
                        if (item.Key.Contains("LR"))
                        {
                            val = item.Value.height;
                            break;
                        }
                        else
                        {
                            val = 0;
                        }
                    }
                    return val;
                }
            }
        }

        public void SetWindowHeight(string key, float value)
        {
            windowHeight[key] = value;
            foreach (var item in WindowsPositions[key])
            {
                if (item.Key != "LR" && item.Key != "LRR")
                    item.Value.height = value;
            }
        }

        public float GetWindowHeight(string key, BuildingType buildingType, bool m3)
        {
            if (buildingType == BuildingType.Mansion)
            {
                if (m3)
                {
                    float val = 0;
                    foreach (var item in WindowsPositionsMansionM3[key])
                    {
                        if (!item.Key.Contains("LR"))
                        {
                            val = item.Value.height;
                            break;
                        }
                        else
                        {
                            val = 0;
                        }
                    }
                    return val;
                }
                else
                {
                    float val = 0;
                    foreach (var item in WindowsPositionsMansion[key])
                    {
                        if (!item.Key.Contains("LR"))
                        {
                            val = item.Value.height;
                            break;
                        }
                        else
                        {
                            val = 0;
                        }
                    }
                    return val;
                }
            }
            else
            {
                float val = 0;
                foreach (var item in WindowsPositions[key])
                {
                    if (!item.Key.Contains("LR"))
                    {
                        val = item.Value.height;
                        break;
                    }
                    else
                    {
                        val = 0;
                    }
                }
                return val;
            }
        }

        public Dictionary<string, Dictionary<string, WindowsPosition>> WindowsPositions = new Dictionary<string, Dictionary<string, WindowsPosition>>()
        {
            {"Studio" , new Dictionary<string, WindowsPosition>()
                {
                    { "LR" , new WindowsPosition() { origin = 3 , fromWalls = 1.42f , height= 2.45f, width = 2.45f, cill = 0, windowType = WindowType.front } }
                }
            },
            {"1b2p" , new Dictionary<string, WindowsPosition>()
                {
                    { "DB" , new WindowsPosition() { origin = 5 , fromWalls = 0.975f , height= 1.65f, width = 1.05f, cill = 0.8f, windowType = WindowType.front } },
                    { "LR" , new WindowsPosition() { origin = 3 , fromWalls = 0.82f , height= 2.45f, width = 2.45f, cill = 0, windowType = WindowType.front } }
                }
            },
            {"2b3p" , new Dictionary<string, WindowsPosition>()
                {
                    { "DB" , new WindowsPosition() { origin = 5 , fromWalls = 0.975f , height= 1.65f, width = 1.05f, cill = 0.8f, windowType = WindowType.front } },
                    { "DBB" , new WindowsPosition() { origin = 0 , fromWalls = 0.675f , height= 1.65f, width = 1.05f, cill = 0.8f, windowType = WindowType.front } },
                    { "LR" , new WindowsPosition() { origin = 3 , fromWalls = 0.385f , height= 2.45f, width = 2.45f, cill = 0 , windowType = WindowType.front} }
                }
            },
            {"2b4p" , new Dictionary<string, WindowsPosition>()
                {
                    { "PB" , new WindowsPosition() { origin = 5 , fromWalls = 0.975f , height= 1.65f, width = 1.05f, cill = 0.8f, windowType = WindowType.front } },
                    { "PBB" , new WindowsPosition() { origin = 0 , fromWalls = 1.275f , height= 1.65f, width = 1.05f, cill = 0.8f, windowType = WindowType.front } },
                    { "LRR" , new WindowsPosition() { origin = 3 , fromWalls = 0.41f , height= 2.45f, width = 2.45f, cill = 0, windowType = WindowType.front } }
                }
            },
            {"3b5p" , new Dictionary<string, WindowsPosition>()
                {
                    { "PB" , new WindowsPosition() { origin = 5 , fromWalls = 0.975f , height= 1.65f, width = 1.05f, cill = 0.8f, windowType = WindowType.front } },
                    { "PBB" , new WindowsPosition() { origin = 0 , fromWalls = 1.275f , height= 1.65f, width = 1.05f, cill = 0.8f, windowType = WindowType.front } },
                    { "PSB" , new WindowsPosition() { origin = 0 , fromWalls = 0.675f , height= 1.65f, width = 1.05f, cill = 0.8f, windowType = WindowType.front } },
                    { "LRR" , new WindowsPosition() { origin = 3 , fromWalls = 0.32f , height= 2.45f, width = 2.45f, cill = 0 , windowType = WindowType.front} }
                }
            },

        };

        public Dictionary<string, Dictionary<string, WindowsPosition>> WindowsPositionsM3 = new Dictionary<string, Dictionary<string, WindowsPosition>>()
        {
            {"Studio" , new Dictionary<string, WindowsPosition>()
                {
                    { "LR" , new WindowsPosition() { origin = 3 , fromWalls = 1.42f , height= 2.45f, width = 2.45f, cill = 0, windowType = WindowType.front } }
                }
            },
            {"1b2p" , new Dictionary<string, WindowsPosition>()
                {
                    { "DB" , new WindowsPosition() { origin = 5 , fromWalls = 0.975f , height= 1.65f, width = 1.05f, cill = 0.8f, windowType = WindowType.front } },
                    { "LR" , new WindowsPosition() { origin = 3 , fromWalls = 0.82f , height= 2.45f, width = 2.45f, cill = 0, windowType = WindowType.front } }
                }
            },
            {"2b3p" , new Dictionary<string, WindowsPosition>()
                {
                    { "DB" , new WindowsPosition() { origin = 5 , fromWalls = 0.975f , height= 1.65f, width = 1.05f, cill = 0.8f, windowType = WindowType.front } },
                    { "DBB" , new WindowsPosition() { origin = 0 , fromWalls = 0.675f , height= 1.65f, width = 1.05f, cill = 0.8f, windowType = WindowType.front } },
                    { "LR" , new WindowsPosition() { origin = 3 , fromWalls = 0.385f , height= 2.45f, width = 2.45f, cill = 0 , windowType = WindowType.front} }
                }
            },
            {"2b4p" , new Dictionary<string, WindowsPosition>()
                {
                    { "PB" , new WindowsPosition() { origin = 5 , fromWalls = 0.975f , height= 1.65f, width = 1.05f, cill = 0.8f, windowType = WindowType.front } },
                    { "PBB" , new WindowsPosition() { origin = 0 , fromWalls = 1.275f , height= 1.65f, width = 1.05f, cill = 0.8f, windowType = WindowType.front } },
                    { "LRR" , new WindowsPosition() { origin = 3 , fromWalls = 0.41f , height= 2.45f, width = 2.45f, cill = 0, windowType = WindowType.front } }
                }
            },
            {"3b5p" , new Dictionary<string, WindowsPosition>()
                {
                    { "PB" , new WindowsPosition() { origin = 5 , fromWalls = 0.975f , height= 1.65f, width = 1.05f, cill = 0.8f, windowType = WindowType.front } },
                    { "PBB" , new WindowsPosition() { origin = 0 , fromWalls = 1.275f , height= 1.65f, width = 1.05f, cill = 0.8f, windowType = WindowType.front } },
                    { "PSB" , new WindowsPosition() { origin = 0 , fromWalls = 0.675f , height= 1.65f, width = 1.05f, cill = 0.8f, windowType = WindowType.front } },
                    { "LRR" , new WindowsPosition() { origin = 3 , fromWalls = 0.32f , height= 2.45f, width = 2.45f, cill = 0 , windowType = WindowType.front} }
                }
            },

        };

        public Dictionary<string, Dictionary<string, WindowsPosition>> WindowsPositionsMansion = new Dictionary<string, Dictionary<string, WindowsPosition>>()
        {
            {"1b2p" , new Dictionary<string, WindowsPosition>()
                {
                    { "DB" , new WindowsPosition() { origin = 0 , fromWalls = 1.73f , height= 1.65f, width = 1.05f, cill = 0.8f, windowType = WindowType.back } },
                    { "LR" , new WindowsPosition() { origin = 5 , fromWalls = 1.03f , height= 2.45f, width = 2.45f, cill = 0, windowType = WindowType.front } }
                }
            },
            {"2b3p" , new Dictionary<string, WindowsPosition>()
                {
                    { "PSB" , new WindowsPosition() { origin = 0 , fromWalls = 0.68f , height= 1.65f, width = 1.05f, cill = 0.8f, windowType = WindowType.back } },
                    { "DB" , new WindowsPosition() { origin = 0 , fromWalls = 0.98f , height= 1.65f, width = 1.05f, cill = 0.8f, windowType = WindowType.back } },
                    { "LRR" , new WindowsPosition() { origin = 5 , fromWalls = 1.48f , height= 2.45f, width = 2.45f, cill = 0, windowType = WindowType.front } }
                }
            },
            {"2b4p" , new Dictionary<string, WindowsPosition>()
                {
                    { "DBB" , new WindowsPosition() { origin = 0 , fromWalls = 0.88f , height= 1.65f, width = 1.05f, cill = 0.8f, windowType = WindowType.back } },
                    { "PB" , new WindowsPosition() { origin = 0 , fromWalls = 1.23f , height= 1.65f, width = 1.05f, cill = 0.8f, windowType = WindowType.back } },
                    { "LRRR" , new WindowsPosition() { origin = 7 , fromWalls = 1.93f , height= 2.45f, width = 2.45f, cill = 0, windowType = WindowType.front } }
                }
            },
            {"3b5p" , new Dictionary<string, WindowsPosition>()
                {
                    { "PSB" , new WindowsPosition() { origin = 0 , fromWalls = 0.63f , height= 1.65f, width = 1.05f, cill = 0.8f, windowType = WindowType.back } },
                    { "DBB" , new WindowsPosition() { origin = 0 , fromWalls = 0.88f , height= 1.65f, width = 1.05f, cill = 0.8f, windowType = WindowType.back } },
                    { "PB" , new WindowsPosition() { origin = 0 , fromWalls = 0.98f , height= 1.65f, width = 1.05f, cill = 0.8f, windowType = WindowType.back } },
                    { "LRRRR" , new WindowsPosition() { origin = 7 , fromWalls = 2.83f , height= 2.45f, width = 2.45f, cill = 0, windowType = WindowType.front } }
                }
            },

        };

        public Dictionary<string, Dictionary<string, WindowsPosition>> WindowsPositionsMansionM3 = new Dictionary<string, Dictionary<string, WindowsPosition>>()
        {
            {"1b2p" , new Dictionary<string, WindowsPosition>()
                {
                    { "DB" , new WindowsPosition() { origin = 0 , fromWalls = 2.03f , height= 1.65f, width = 1.05f, cill = 0.8f } },
                    { "LR" , new WindowsPosition() { origin = 7 , fromWalls = 1.33f , height= 2.45f, width = 2.45f, cill = 0 } }
                }
            },
            {"2b3p" , new Dictionary<string, WindowsPosition>()
                {
                    { "PSB" , new WindowsPosition() { origin = 0 , fromWalls = 0.8f , height= 1.65f, width = 1.05f, cill = 0.8f } },
                    { "DB" , new WindowsPosition() { origin = 0 , fromWalls = 1.4f , height= 1.65f, width = 1.05f, cill = 0.8f } },
                    { "LRR" , new WindowsPosition() { origin = 5 , fromWalls = 2.03f , height= 2.45f, width = 2.45f, cill = 0 } }
                }
            },
            {"2b4p" , new Dictionary<string, WindowsPosition>()
                {
                    { "DB" , new WindowsPosition() { origin = 0 , fromWalls = 1.3f , height= 1.65f, width = 1.05f, cill = 0.8f } },
                    { "PB" , new WindowsPosition() { origin = 0 , fromWalls = 1.35f , height= 1.65f, width = 1.05f, cill = 0.8f } },
                    { "LRRR" , new WindowsPosition() { origin = 7 , fromWalls = 2.48f , height= 2.45f, width = 2.45f, cill = 0 } }
                }
            },
            {"3b5p" , new Dictionary<string, WindowsPosition>()
                {
                    { "PSB" , new WindowsPosition() { origin = 0 , fromWalls = 0.75f , height= 1.65f, width = 1.05f, cill = 0.8f } },
                    { "DB" , new WindowsPosition() { origin = 0 , fromWalls = 1.08f , height= 1.65f, width = 1.05f, cill = 0.8f } },
                    { "PB" , new WindowsPosition() { origin = 0 , fromWalls = 1.05f , height= 1.65f, width = 1.05f, cill = 0.8f } },
                    { "LRRRR" , new WindowsPosition() { origin = 9 , fromWalls = 3.23f , height= 2.45f, width = 2.45f, cill = 0 } }
                }
            },

        };

        /// <summary>
        /// The modules of each apartment type which have a fixed size
        /// </summary>
        public Dictionary<string, float[]> FixedModuleWidths
        {
            get
            {
                return new Dictionary<string, float[]>()
                {
                    {"Studio", new float[] { customVals["Studio"][2] } },
                    {"1b2p", new float[] { customVals["1b2p"][0] } },
                    {"2b3p", new float[] { customVals["2b3p"][0], 2.4f } },
                    {"2b4p", new float[] { customVals["2b4p"][1], 3.6f } },
                    {"3b5p", new float[] { customVals["3b5p"][1], 3.6f,2.4f } }
                };
            }
        }

        /// <summary>
        /// The different line load-spans per apartment type
        /// </summary>
        public Dictionary<string, float[][]> FixedLineLoadsSpans
        {
            get
            {
                return new Dictionary<string, float[][]>()
                {
                    {"Studio", new float[][] { new float[] { customVals["Studio"][2] } , new float[] {} } },
                    {"1b2p", new float[][] { new float[] { customVals["1b2p"][2], 2.4f } , new float[] {customVals["1b2p"][0] } } },
                    {"2b3p", new float[][] { new float[] { customVals["2b3p"][2], 2.4f } , new float[] {customVals["2b3p"][0], 2.4f } } },
                    {"2b4p", new float[][] { new float[] { customVals["2b4p"][2] + 0.75f, 1.5f + customVals["2b4p"][3] } , new float[] {customVals["2b4p"][1], 3.6f } } },
                    {"3b5p", new float[][] { new float[] { customVals["3b5p"][2] + 0.75f, 1.5f + customVals["3b5p"][3], customVals["3b5p"][5] } , new float[] {customVals["3b5p"][1], 3.6f, 2.4f } } },
                };
            }
        }

        public Dictionary<string, float> mansionWidth = new Dictionary<string, float>()
        {
            {"1b2p",4.5f },
            {"2b3p",5.4f },
            {"2b4p",6.3f },
            {"3b5p",8.1f },
        };

        public Dictionary<string, float> mansionWidthM3 = new Dictionary<string, float>()
        {
            {"1b2p",5.1f },
            {"2b3p",6.5f },
            {"2b4p",7.4f },
            {"3b5p",8.9f },
        };

        // Use this for initialization
        void Awake()
        {
            EvaluateStandardRooms();
            EvaluateAmenitiesArea();
            GetAptVariables(ref customValsMansionM2, mansionM2AptVariables);
            GetAptVariables(ref customValsMansionM3, mansionM3AptVariables);

            //-------Populate Apt Layouts------//
            aptLayouts = new Dictionary<BuildingType, Dictionary<string, AptLayout>>();
            for (int i = 0; i < apartmentLayouts.Length; i++)
            {
                BuildingType key = apartmentLayouts[i].buildingType;
                string aptKey = apartmentLayouts[i].aptType + "_" + (apartmentLayouts[i].m3 ? "M3" : "M2");
                //if (apartmentLayouts[i].m3)
                //{
                //    string var = string.Empty;
                //    for (int j = 0; j < apartmentLayouts[i].roomInclusion.Length; j++)
                //    {
                //        var += "," + apartmentLayouts[i].roomInclusion[j];
                //    }
                //    var = var.ToLower();
                //    Debug.Log(aptKey + "_" + var);
                //}

                if (aptLayouts.ContainsKey(key))
                {
                    aptLayouts[key].Add(aptKey, apartmentLayouts[i]);
                }
                else
                {
                    aptLayouts.Add(key, new Dictionary<string, AptLayout>() { { aptKey, apartmentLayouts[i] } });
                }
            }
            SetRoomInclusions();
        }

        public void SetRoomInclusions()
        {
            foreach (var item in aptLayouts)
            {
                if (item.Key == BuildingType.Mansion)
                {
                    foreach (var layout in item.Value)
                    {
                        string key = layout.Key.Split('_')[0];
                        if (layout.Value.m3)
                        {
                            layout.Value.roomInclusion = ApartmentTypeRoomsMansionM3[key];
                        }
                        else
                        {
                            layout.Value.roomInclusion = ApartmentTypeRoomsMansion[key];
                        }
                    }
                }
                else
                {
                    foreach (var layout in item.Value)
                    {
                        string key = layout.Key.Split('_')[0];
                        if (layout.Value.m3)
                        {
                            layout.Value.roomInclusion = ApartmentTypeRoomsM3[key];
                        }
                        else
                        {
                            layout.Value.roomInclusion = ApartmentTypeRooms[key];
                        }
                    }
                }
            }
        }

        public void GetAptVariables(ref Dictionary<string, Dictionary<string, float>> dict, TextAsset textAsset)
        {
            dict = new Dictionary<string, Dictionary<string, float>>();
            var lines = textAsset.text.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            string[] keys = lines[0].Split(',');
            for (int i = 1; i < lines.Length; i++)
            {
                string[] cells = lines[i].Split(',');
                string aptType = cells[0];
                Dictionary<string, float> variables = new Dictionary<string, float>();
                for (int j = 1; j < cells.Length; j++)
                {
                    variables.Add(keys[j], float.Parse(cells[j]));
                }
                dict.Add(aptType, variables);
            }
        }

        // Update is called once per frame
        void Update()
        {

        }


        /// <summary>
        /// Called by the Unity UI to set the type of the amenities
        /// </summary>
        /// <param name="value">The type of the amenitites</param>
        public void SetBalconies(int value)
        {
            var temp_balc = (int)balconies;
            if (value != temp_balc)
            {
                if (balconies == Balconies.Inset)
                {
                    foreach (var apt in ApartmentTypeRooms)
                    {
                        if (apt.Key != "SpecialApt")
                        {
                            foreach (var index in amenitiesIndices["Interior"])
                            {
                                if (amenitiesIndices["Exterior"].ContainsKey(index.Key))
                                {
                                    if (apt.Value[index.Value])
                                    {
                                        apt.Value[amenitiesIndices["Exterior"][index.Key]] = true;
                                    }
                                }
                                apt.Value[index.Value] = false;
                            }
                        }
                    }
                    foreach (var apt in ApartmentTypeRoomsM3)
                    {
                        if (apt.Key != "SpecialApt")
                        {
                            foreach (var index in amenitiesIndicesM3["Interior"])
                            {
                                if (amenitiesIndicesM3["Exterior"].ContainsKey(index.Key))
                                {
                                    if (apt.Value[index.Value])
                                    {
                                        apt.Value[amenitiesIndicesM3["Exterior"][index.Key]] = true;
                                    }
                                }
                                apt.Value[index.Value] = false;
                            }
                        }
                    }
                }
                else
                {
                    foreach (var apt in ApartmentTypeRooms)
                    {
                        if (apt.Key != "SpecialApt")
                        {
                            foreach (var index in amenitiesIndices["Exterior"])
                            {
                                if (amenitiesIndices["Interior"].ContainsKey(index.Key))
                                {
                                    if (apt.Value[index.Value])
                                    {
                                        apt.Value[amenitiesIndices["Interior"][index.Key]] = true;
                                    }
                                }
                                apt.Value[index.Value] = false;
                            }
                        }
                    }
                    foreach (var apt in ApartmentTypeRoomsM3)
                    {
                        if (apt.Key != "SpecialApt")
                        {
                            foreach (var index in amenitiesIndicesM3["Exterior"])
                            {
                                if (amenitiesIndicesM3["Interior"].ContainsKey(index.Key))
                                {
                                    if (apt.Value[index.Value])
                                    {
                                        apt.Value[amenitiesIndicesM3["Interior"][index.Key]] = true;
                                    }
                                }
                                apt.Value[index.Value] = false;
                            }
                        }
                    }
                }
            }
            SetRoomInclusions();
            balconies = (Balconies)value;
        }

        /// <summary>
        /// Sets the minimum area per apartment type
        /// </summary>
        /// <param name="minAreas">The minimum area of each apartment type</param>
        public void SetAptMinimumAreas(Dictionary<string, double> minAreas)
        {
            apartmentTypesMinimumSizes = minAreas;
            if (onAptsAreasChanged != null)
            {
                onAptsAreasChanged.Invoke(this, true);
            }
        }

        /// <summary>
        /// Attempts to set the minimum area of apartment type
        /// </summary>
        /// <param name="key">The apartment type</param>
        /// <param name="value">The area</param>
        /// <returns>Whether it succeeded or not</returns>
        public bool TrySetMinimumArea(string key, double value)
        {
            if (apartmentTypesMinimumSizes.ContainsKey(key))
            {
                apartmentTypesMinimumSizes[key] = value;
                if (value > desiredAreas[key])
                {
                    desiredAreas[key] = value;
                }
                ApartmentTypesMaximumSizes[key] = ApartmentTypesMinimumSizes[key] + ApartmentTypesMinimumSizes[key] * 0.1;
                if (onAptsAreasChanged != null)
                {
                    onAptsAreasChanged.Invoke(this, true);
                }
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Sets the assigned are per apartment type
        /// </summary>
        /// <param name="key">Apartment Type</param>
        /// <param name="value">Assigned Area</param>
        /// <returns>Boolean</returns>
        public bool TrySetDesiredArea(string key, double value)
        {
            if (desiredAreas.ContainsKey(key))
            {
                desiredAreas[key] = value;
                return true;
            }
            else
                return false;
        }

        public bool TrySetDesiredAreaM3(string key, double value)
        {
            if (desiredAreasM3.ContainsKey(key))
            {
                desiredAreasM3[key] = value;
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Sets the maximum area per apartment type
        /// </summary>
        /// <param name="maxAreas">The maximum area of each apartment type</param>
        public void SetAptMaximumAreas(Dictionary<string, double> maxAreas)
        {
            ApartmentTypesMaximumSizes = maxAreas;
            if (onAptsAreasChanged != null)
            {
                onAptsAreasChanged.Invoke(this, true);
            }
        }

        /// <summary>
        /// Attempts to set the maximum area of apartment type
        /// </summary>
        /// <param name="key">The apartment type</param>
        /// <param name="value">The area</param>
        /// <returns>Whether it succeeded or not</returns>
        public bool TrySetMaximumArea(string key, double value)
        {
            if (ApartmentTypesMaximumSizes.ContainsKey(key))
            {
                ApartmentTypesMaximumSizes[key] = value;

                if (onAptsAreasChanged != null)
                {
                    onAptsAreasChanged.Invoke(this, true);
                }
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Attempts to set the amanity area of apartment type
        /// </summary>
        /// <param name="key">The apartment type</param>
        /// <param name="value">The area</param>
        /// <param name="triggerEvent">Whether there should be an event triggered</param>
        /// <returns>Whether it succeeded or not</returns>
        public bool TrySetAmenitiesArea(string key, double value, bool triggerEvent = true)
        {
            if (amenitiesAreas.ContainsKey(key))
            {
                if (value == 0)
                {
                    amenitiesAreas[key] = (float)value;
                }
                else
                {
                    amenitiesAreas[key] = MinimumAmenitiesArea[key];
                }

                ApartmentTypesMaximumSizes[key] = ApartmentTypesMinimumSizes[key] + ApartmentTypesMinimumSizes[key] * 0.1;
                if (onAptsAreasChanged != null)
                {
                    onAptsAreasChanged.Invoke(this, triggerEvent);
                }
                return true;
            }
            else
                return false;
        }

        public bool TrySetAmenitiesAreaM3(string key, double value, bool triggerEvent = true)
        {
            if (amenitiesAreas.ContainsKey(key))
            {
                if (value == 0)
                {
                    amenitiesAreas[key] = (float)value;
                }
                else
                {
                    amenitiesAreas[key] = MinimumAmenitiesArea[key];
                }

                M3ApartmentTypesMaximumSizes[key] = M3ApartmentTypesMaximumSizes[key] + M3ApartmentTypesMaximumSizes[key] * 0.1;
                if (onAptsAreasChanged != null)
                {
                    onAptsAreasChanged.Invoke(this, triggerEvent);
                }
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Sets the corresponding construction feature values based on a given title
        /// </summary>
        /// <param name="title">The construction features preset title</param>
        public void SetConstructionFeaturesToUI(string title)
        {
            if (title == "Custom")
            {
                if (constructionFeaturesElement.constructionTypesDropdown.options[constructionFeaturesElement.constructionTypesDropdown.value].text != "Custom")
                {
                    constructionFeaturesElement.constructionTypesDropdown.options.Add(new Dropdown.OptionData("Custom"));
                    constructionFeaturesElement.constructionTypesDropdown.SetValue(constructionFeaturesElement.constructionTypesDropdown.options.Count - 1);
                    constructionFeaturesElement.constructionTypesDropdown.RefreshShownValue();
                }
            }
            else
            {
                for (int i = 0; i < constructionFeaturesElement.constructionTypesDropdown.options.Count; i++)
                {
                    if (constructionFeaturesElement.constructionTypesDropdown.options[i].text == title)
                    {
                        constructionFeaturesElement.constructionTypesDropdown.SetValue(i);
                        constructionFeaturesElement.constructionTypesDropdown.RefreshShownValue();
                    }
                }
            }

            constructionFeaturesElement.partyWall.SetValue(ConstructionFeatures["PartyWall"].ToString());
            constructionFeaturesElement.exteriorWall.SetValue(ConstructionFeatures["ExteriorWall"].ToString());
            constructionFeaturesElement.corridorWall.SetValue(ConstructionFeatures["CorridorWall"].ToString());
            constructionFeaturesElement.panelLength.SetValue(ConstructionFeatures["PanelLength"].ToString());
            constructionFeaturesElement.lineLoadSpan.SetValue(ConstructionFeatures["LineSpan"].ToString());

            ReportData.TaggedObject.ConstructionFeaturesName = constructionFeaturesElement.constructionTypesDropdown.options[constructionFeaturesElement.constructionTypesDropdown.value].text;
            ReportData.TaggedObject.ConstructionFeatures = Standards.TaggedObject.ConstructionFeaturesReport;

            balconiesDropdown.SetValue((int)balconies);
        }

        /// <summary>
        /// The title of the current construction features preset
        /// </summary>
        public string ConstructionFeaturesTitle
        {
            get
            {
                return constructionFeaturesElement.constructionTypesDropdown.options[constructionFeaturesElement.constructionTypesDropdown.value].text;
            }
        }

        public Dictionary<string, Color> BasementUseColours = new Dictionary<string, Color>()
        {
            {"Plant", Color.gray },
            {"Parking",Color.gray},
            {"Circulation",Color.gray },
            {"Cycle Storage",Color.gray},
        };


        public Dictionary<string, Color> PodiumUseColours = new Dictionary<string, Color>()
        {
            {"Shops (A1)", Color.gray },
            {"Financial / Professional Services (A2)", Color.gray },
            {"Restaurants / Cafes (A3)", Color.gray },
            {"Drinking Establishments  (A4)",Color.gray },
            {"Hot Food Takeaways (A5)",Color.gray },
            {"Business (B1)", Color.gray },
            {"General Industrial (B2)",Color.gray },
            {"Storage / Distribution (B8)", Color.gray },
            {"Hotels (C1)", Color.gray },
            {"Residential Institutions (C2)", Color.gray},
            {"Secure Residential Institutions (C2A)",Color.gray},
            {"Non-residential Institutions (D1)",Color.gray },
            {"Assembly / Leisure (D2)", Color.gray },
        };
    }

    /// <summary>
    /// A UnityEvent class for when the apartment standards change
    /// </summary>
    [System.Serializable]
    public class OnAptAreasChanged : UnityEvent<Standards, bool>
    {

    }
}

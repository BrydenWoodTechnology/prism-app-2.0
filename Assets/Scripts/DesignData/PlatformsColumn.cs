﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BrydenWoodUnity.DesignData
{
    /// <summary>
    /// A MonoBehaviour component for the columns of the Platforms' structure
    /// </summary>
    public class PlatformsColumn : MonoBehaviour
    {
        #region Public Variables
        [Tooltip("The platforms frame component")]
        public PlatformsFrame platformsFrame;
        #endregion

        #region Monobehaviour Methods
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Called when a trigger collider enters the collider of the column
        /// </summary>
        /// <param name="other">The collider which entered</param>
        public void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.name.Contains("window"))
            {
                platformsFrame.AddWindowsCollision(other.gameObject);
            }
        }
        #endregion
    }
}

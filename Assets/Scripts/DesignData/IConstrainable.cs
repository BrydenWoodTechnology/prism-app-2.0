﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrydenWoodUnity.DesignData
{
    #region Delegates and Events
    /// <summary>
    /// Used when the evaluation of the constraints has changed
    /// </summary>
    /// <param name="sender">The Design Object that was evaluated</param>
    /// <param name="error">Whether there is an error</param>
    public delegate void OnConstraintsChanged(IConstrainable sender, bool error);
    #endregion

    #region Interfaces
    /// <summary>
    /// Interface to be implemented by Design Objects which need to be checked against constraints
    /// </summary>
    public interface IConstrainable
    {
        /// <summary>
        /// Returns the notification for the Design Object
        /// </summary>
        /// <returns>String</returns>
        string GetNotification();
        /// <summary>
        /// Called to highlight the object if there is an error
        /// </summary>
        /// <param name="show">Whether there is an error</param>
        void HighlightErrorObject(bool show);
        /// <summary>
        /// Called when an object with error is selected
        /// </summary>
        void ErrorSelect();
    }
    #endregion
}

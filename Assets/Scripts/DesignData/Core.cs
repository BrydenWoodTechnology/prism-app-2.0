﻿using BrydenWoodUnity.GeometryManipulation;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace BrydenWoodUnity.DesignData
{
    /// <summary>
    /// A MonoBehvaiour component for the Core of the buildings
    /// </summary>
    public class Core : MonoBehaviour
    {
        #region Public Variables
        public ProceduralFloor floor;
        public Polygon centreLine;
        public OnCoreMoved onMoved;
        public OnCoreDeleted onDeleted;
        public int id;
        public float param;

        public Material selectedMaterial;
        public Material originalMaterial { get; set; }
        #endregion

        #region Monobehaviour Methods
        // Use this for initialization
        void Start()
        {
            originalMaterial = Resources.Load("Materials/CoreStandardMaterial") as Material;
            selectedMaterial = Resources.Load("Materials/CoreSelectedMaterial") as Material;
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// It initializes the component
        /// </summary>
        /// <param name="floor">The floor that the core will belong to</param>
        /// <param name="centreLine">The centre-line of the floor</param>
        /// <param name="id">The id of the core</param>
        /// <param name="param">The parameter along the centre-line</param>
        public void Initialize(ProceduralFloor floor, Polygon centreLine, int id, float param)
        {
            this.id = id;
            this.floor = floor;
            this.centreLine = centreLine;
            this.param = param;
            onMoved = new OnCoreMoved();
            onDeleted = new OnCoreDeleted();
        }

        /// <summary>
        /// Called when the core has been moved
        /// </summary>
        /// <param name="param">The new parameter of the core along the centre-line</param>
        public void OnMoved(float param)
        {
            this.param = param;
            if (onMoved != null) onMoved.Invoke(new CoreMoveEventArgs { coreId = id, coreParam = param });
        }

        /// <summary>
        /// Called when the core has been deleted
        /// </summary>
        public void OnDelete()
        {
            if (onDeleted != null) onDeleted.Invoke(new CoreDeletedEventArgs { coreId = id });
        }

        /// <summary>
        /// Called when the core has been selected
        /// </summary>
        public void Select()
        {
            GetComponent<MeshRenderer>().sharedMaterial = selectedMaterial;
        }

        /// <summary>
        /// Called when the core has be deselected
        /// </summary>
        public void Deselect()
        {
            GetComponent<MeshRenderer>().sharedMaterial = originalMaterial;
        }
        #endregion
    }

    #region Events
    /// <summary>
    /// A Unity Event for when the core has been moved
    /// </summary>
    [Serializable]
    public class OnCoreMoved : UnityEvent<CoreMoveEventArgs>
    {

    }

    /// <summary>
    /// A Unity Event for when the core has been deleted
    /// </summary>
    [Serializable]
    public class OnCoreDeleted: UnityEvent<CoreDeletedEventArgs>
    {

    }

    /// <summary>
    /// The event arguments for when the core has been moved
    /// </summary>
    public struct CoreMoveEventArgs
    {
        public int coreId;
        public float coreParam;
    }

    /// <summary>
    /// The event arguments for when the core has been added
    /// </summary>
    public struct CoreAddedEventArgs
    {
        public float coreParam;
    }

    /// <summary>
    /// The event arguments for when the core has been deleted
    /// </summary>
    public struct CoreDeletedEventArgs
    {
        public int coreId;
    }
    #endregion
}

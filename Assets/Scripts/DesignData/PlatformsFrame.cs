﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BrydenWoodUnity.DesignData
{
    /// <summary>
    /// A MonoBehaviour component for the Platform's frames
    /// </summary>
    public class PlatformsFrame : MonoBehaviour
    {
        #region Public Variables
        [Header("References:")]
        public Transform clmn1;
        public Transform clmn2;
        public Transform clmn3;
        public Transform clmn4;
        public Transform clmn5;
        public Transform beam;
        public Material standardMaterial;
        public Material errorMaterial;

        [Header("Settings:")]
        public float height = 3.2f;
        public float width = 15.6f;
        public float leftSpan = 7.8f;
        public float rightSpan = 7.8f;
        public float elementSizeWidth = 0.1f;
        public float intermediateOffset1 = 0;
        public float intermediateOffset2 = 0;
        public bool justColumns = false;
        public bool setIntermediateClmns = false;

        public HashSet<GameObject> windows;
        public List<GameObject> halls;
        #endregion

        #region Public Properties
        public Vector3 start
        {
            get
            {
                return clmn1.position;
            }
        }
        public Vector3 end
        {
            get
            {
                return clmn2.position;
            }
        }
        #endregion

        #region Monobehaviour Methods
        // Start is called before the first frame update
        void Start()
        {
            windows = new HashSet<GameObject>();
            halls = new List<GameObject>();
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods

        /// <summary>
        /// It sets up the frame of the platforms
        /// </summary>
        public void SetUpFrame()
        {
            width = leftSpan + rightSpan;
            if (windows != null)
                windows.Clear();
            else windows = new HashSet<GameObject>();

            clmn1.localPosition = Vector3.right * -1 * leftSpan + Vector3.up * height * 0.5f;
            clmn2.localPosition = Vector3.right * rightSpan + Vector3.up * height * 0.5f;
            clmn3.localPosition = Vector3.zero + Vector3.up * height * 0.5f;

            beam.localPosition = (clmn1.localPosition + (clmn2.localPosition - clmn1.localPosition) * 0.5f) + Vector3.up * height * 0.5f;

            clmn1.localEulerAngles = Vector3.zero;
            clmn2.localEulerAngles = Vector3.zero;
            clmn3.localEulerAngles = Vector3.zero;
            beam.localEulerAngles = Vector3.zero;

            clmn1.localScale = new Vector3(elementSizeWidth, height, elementSizeWidth);
            clmn2.localScale = new Vector3(elementSizeWidth, height, elementSizeWidth);
            clmn3.localScale = new Vector3(elementSizeWidth, height, elementSizeWidth);
            beam.localScale = new Vector3(width, elementSizeWidth, elementSizeWidth);

            beam.gameObject.SetActive(!justColumns);
            clmn3.gameObject.SetActive(justColumns);

            if (setIntermediateClmns)
            {
                clmn4.gameObject.SetActive(true);
                clmn4.localPosition = clmn1.localPosition + Vector3.right * intermediateOffset1;
                clmn4.localEulerAngles = Vector3.zero;
                clmn4.localScale = clmn1.localScale;

                clmn5.gameObject.SetActive(true);
                clmn5.localPosition = clmn2.localPosition + Vector3.left * intermediateOffset2;
                clmn5.localEulerAngles = Vector3.zero;
                clmn5.localScale = clmn2.localScale;
            }
        }

        /// <summary>
        /// It sets the width of the frame
        /// </summary>
        /// <param name="width">The new width</param>
        public void SetWidth(float width)
        {
            this.width = width;
            leftSpan = width * 0.5f;
            rightSpan = width * 0.5f;
            SetUpFrame();
        }

        /// <summary>
        /// It sets the left span of the frame
        /// </summary>
        /// <param name="leftSpan">The new left span</param>
        public void SetLeftSpan(float leftSpan)
        {
            this.leftSpan = leftSpan;
            width = leftSpan + rightSpan;
            SetUpFrame();
        }

        /// <summary>
        /// It sets the right span of the frame
        /// </summary>
        /// <param name="rightSpan">The new right span</param>
        public void SetRightSpan(float rightSpan)
        {
            this.rightSpan = rightSpan;
            width = leftSpan + rightSpan;
            SetUpFrame();
        }

        /// <summary>
        /// It sets the height of the frame
        /// </summary>
        /// <param name="height">The new height of the frame</param>
        public void SetHeight(float height)
        {
            this.height = height;
            SetUpFrame();
        }

        /// <summary>
        /// It sets the dimensions of the frame
        /// </summary>
        /// <param name="width">The new width of the frame</param>
        /// <param name="height">The new height of the frame</param>
        public void SetDimensions(float width, float height)
        {
            this.width = width;
            leftSpan = width * 0.5f;
            rightSpan = width * 0.5f;
            this.height = height;
            SetUpFrame();
        }

        /// <summary>
        /// Adds a window if the frame collides with it
        /// </summary>
        /// <param name="window">The window which collides with the frame</param>
        public void AddWindowsCollision(GameObject window)
        {
            if (!windows.Contains(window))
            {
                windows.Add(window);
            }
            if (windows.Count > 0)
            {
                var renderers = GetComponentsInChildren<MeshRenderer>();
                for (int i = 0; i < renderers.Length; i++)
                {
                    renderers[i].sharedMaterial = errorMaterial;
                }
                var objectHovers = GetComponentsInChildren<OnObjectHoverOver>();
                for (int i = 0; i < objectHovers.Length; i++)
                {
                    objectHovers[i].enabled = true;
                }
            }
        }

        /// <summary>
        /// Adds a hall if the frame collides with it
        /// </summary>
        /// <param name="hall">The hall which collides with the frame</param>
        public void AddHallCollision(GameObject hall)
        {
            if (!halls.Contains(hall))
            {
                halls.Add(hall);
            }
            if (halls.Count>0)
            {
                var renderers = GetComponentsInChildren<MeshRenderer>();
                for (int i = 0; i < renderers.Length; i++)
                {
                    renderers[i].sharedMaterial = errorMaterial;
                }
                var objectHovers = GetComponentsInChildren<OnObjectHoverOver>();
                for (int i=0; i<objectHovers.Length; i++)
                {
                    objectHovers[i].enabled = true;
                }
            }
        }
        #endregion
    }
}

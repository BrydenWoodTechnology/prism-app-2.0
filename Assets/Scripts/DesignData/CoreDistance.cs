﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BrydenWoodUnity.DesignData
{
    /// <summary>
    /// A MonoBehaviour component for displaying the distance between two cores
    /// </summary>
    public class CoreDistance : MonoBehaviour
    {
        #region Public Variables
        public LineRenderer lineRenderer;
        public TMPro.TMP_Text text;
        public Transform start;
        public Transform end;
        public Material distanceMaterialRed;
        public Vector3[] endPoints;
        #endregion

        #region Monobehaviour Methods
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region public Methods
        /// <summary>
        /// Sets the cores between which to display the distance
        /// </summary>
        /// <param name="positions">The positions of the cores</param>
        /// <param name="coreLength">The length of the cores</param>
        public void SetCores(Vector3[] positions, float coreLength)
        {
            positions[0] += (positions[1] - positions[0]).normalized * coreLength * 0.5f;
            positions[positions.Length - 1] += (positions[positions.Length - 2] - positions[positions.Length - 1]).normalized * coreLength * 0.5f;
            endPoints = positions;
            lineRenderer.positionCount = positions.Length;
            if (endPoints.Length == 2)
                transform.position = endPoints[0] + (endPoints[1] - endPoints[0]) * 0.5f;
            else
                transform.position = endPoints[1];

            lineRenderer.SetPositions(endPoints);
            double dist = 0;
            for (int i = 0; i < endPoints.Length - 1; i++)
            {
                dist += Math.Round(Vector3.Distance(endPoints[i], endPoints[i + 1]), 1);
            }
            text.text = dist + " m";
            start.position = endPoints[0];
            end.position = endPoints[endPoints.Length - 1];
            start.localScale = new Vector3(0.5f, 6, 0.5f);
            end.localScale = new Vector3(0.5f, 6, 0.5f);
            if (dist>60)
            {
                start.GetComponent<MeshRenderer>().sharedMaterial = distanceMaterialRed;
                end.GetComponent<MeshRenderer>().sharedMaterial = distanceMaterialRed;
                lineRenderer.sharedMaterial = distanceMaterialRed;
            }
        }
        #endregion
    }
}

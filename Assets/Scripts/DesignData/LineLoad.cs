﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BrydenWoodUnity.DesignData
{
    /// <summary>
    /// Monobehaviour component for the Line Loads
    /// </summary>
    public class LineLoad : MonoBehaviour
    {
        #region Public Fields and Properties
        [Header("Assets references:")]
        public Material green;
        public Material orange;
        public Material red;

        [Header("Scene References:")]
        public ProceduralRoom room;

        [Header("Featrues:")]
        public bool issue = true;
        public bool isFixed = false;
        public bool markedAsRed = false;
        public Material allignMaterial;
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            //GetComponent<MeshRenderer>().material = orange;
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnDestroy()
        {
        }
        private void OnTriggerEnter(Collider other)
        {
            if (!isFixed && !markedAsRed)
            {
                if ((other.gameObject.layer == 17 && gameObject.layer == 18) || (other.gameObject.layer == 18 && gameObject.layer == 17))
                {
                    allignMaterial = green;
                    GetComponent<MeshRenderer>().material = green;
                    issue = false;
                    if (room != null)
                    {
                        string aptType = room.aptLayout.apartmentType;
                        string roomName = room.gameObject.name;
                    }
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (!isFixed && !markedAsRed)
            {
                if ((other.gameObject.layer == 17 && gameObject.layer == 18) || (other.gameObject.layer == 18 && gameObject.layer == 17))
                {
                    allignMaterial = orange;
                    GetComponent<MeshRenderer>().material = orange;
                    //issue = true;
                    //if (room != null)
                    //{
                    //    string aptType = room.aptLayout.apartmentType;
                    //    string roomName = room.gameObject.name;

                    //    if (Standards.TaggedObject.ValidLineLoads.ContainsKey(aptType))
                    //    {
                    //        if (Standards.TaggedObject.ValidLineLoads[aptType].Contains(roomName))
                    //        {
                    //            Standards.TaggedObject.ValidLineLoads[aptType].Remove(roomName);
                    //        }
                    //    }
                    //}
                }
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Sets the color of the Line-Load to red
        /// </summary>
        /// <param name="red">Whether the line-load should be read or not</param>
        public void MarkAsRed(bool red)
        {
            markedAsRed = red;
            if (red)
            {
                GetComponent<MeshRenderer>().material = this.red;
            }
            else
            {
                GetComponent<MeshRenderer>().material = allignMaterial;
            }
        }

        /// <summary>
        /// Sets whether there is an issue with the line loads
        /// </summary>
        /// <param name="issue">Toggle</param>
        public void SetIssue(bool issue)
        {
            if (issue)
            {
                GetComponent<MeshRenderer>().material = red;
                issue = true;
            }
            else
            {
                GetComponent<MeshRenderer>().material = green;
                issue = false;
            }
        }
        #endregion
    }
}

﻿using BrydenWoodUnity.GeometryManipulation;
using BrydenWoodUnity.UIElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrydenWoodUnity.DesignData
{

    /// <summary>
    /// Class for saving data for the site
    /// </summary>
    public class SiteState
    {
        public float latitude { get; set; }
        public float longitude { get; set; }
        public bool deletedExisting { get; set; }
        public float[] vertices { get; set; }
        public ExtraMassingState podiums { get; set; }
        public ExtraMassingState basements { get; set; }


        public SiteState(float latitude, float longitude, bool deletedExisting)
        {
            this.latitude = latitude;
            this.longitude = longitude;
            this.deletedExisting = deletedExisting;
        }
    }

    [Serializable]
    public class ExtraMassingState
    {
        public bool isCustom { get; set; }
        public float[] vertices { get; set; }
        public int floors { get; set; }
        public float f2f { get; set; }
        public float offset { get; set; }
        public Dictionary<string, float> percentages { get; set; }
        public int[] buildingIndices { get; set; }
    }

    //[Serializable]
    //public class PodiumState
    //{
    //    public bool isCustom { get; set; }
    //    public float[] vertices { get; set; }
    //    public int floors { get; set; }
    //    public float f2f { get; set; }
    //    public float offset { get; set; }
    //    public Dictionary<string, float> percentages { get; set; }
    //}

    //[Serializable]
    //public class BasementState
    //{
    //    public bool isCustom { get; set; }
    //    public float[] vertices { get; set; }
    //    public int floors { get; set; }
    //    public float f2f { get; set; }
    //    public float offset { get; set; }
    //    public Dictionary<string, float> percentages { get; set; }
    //}

    /// <summary>
    /// Class for saving data about the floor layout
    /// </summary>
    public class FloorLayoutState
    {
        public int buildingIndex { get; set; }
        public int floorIndex { get; set; }
        public float[] centreLine { get; set; }
        public bool closed { get; set; }
        public bool isCopy { get; set; }
        public bool isCustom { get; set; }
        public bool isBasement { get; set; }
        public bool isPodium { get; set; }
        public float depth { get; set; }
        public float leftAptDepth { get; set; }
        public float rightAptDepth { get; set; }
        public float footPrintOffset { get; set; }
        public List<ApartmentState> apartmentStates { get; set; }
        public List<KeyValuePair<int, float>> customParams { get; set; }
        public float minApartmentWidth { get; set; }
        public float corridorWidth { get; set; }
        public bool isTower { get; set; }
        public bool isMansion { get; set; }
        public string[] apartmentTypes { get; set; }
        public float[] position { get; set; }
        public float[] euler { get; set; }
        public float[] coreParams { get; set; }
        public float[] customExterior { get; set; }
        public MansionBlock[] mansionBlocks { get; set; }

        public FloorLayoutState()
        {
            apartmentStates = new List<ApartmentState>();
        }

        public FloorLayoutState(int buildingIndex, int floorIndex, bool closed, bool isCopy, float depth, float corridorWidth, bool isTower, bool isMansion, string[] apartmentTypes, bool isBasement, bool isPodium)
        {
            this.buildingIndex = buildingIndex;
            this.floorIndex = floorIndex;
            this.closed = closed;
            this.isCopy = isCopy;
            this.depth = depth;
            rightAptDepth = -1;
            leftAptDepth = -1;
            this.corridorWidth = corridorWidth;
            apartmentStates = new List<ApartmentState>();
            this.isBasement = isBasement;
            this.isPodium = isPodium;
            this.isMansion = isMansion;
        }

        public FloorLayoutState(int buildingIndex, int floorIndex, bool closed, bool isCopy, float leftDepth, float rightDepth, float corridorWidth, bool isTower, bool isMansion, string[] apartmentTypes, bool isBasement, bool isPodium)
        {
            this.buildingIndex = buildingIndex;
            this.floorIndex = floorIndex;
            this.closed = closed;
            this.isCopy = isCopy;
            depth = -1;
            rightAptDepth = rightDepth;
            leftAptDepth = leftDepth;
            this.corridorWidth = corridorWidth;
            apartmentStates = new List<ApartmentState>();
            this.isBasement = isBasement;
            this.isPodium = isPodium;
            this.isMansion = isMansion;
        }

        public void SetCustomExterior(Polygon polygon)
        {
            customExterior = new float[polygon.Count * 3];
            for (int i = 0; i < polygon.Count; i++)
            {
                customExterior[i * 3] = polygon[i].currentPosition.x;
                customExterior[i * 3 + 1] = polygon[i].currentPosition.y;
                customExterior[i * 3 + 2] = polygon[i].currentPosition.z;
            }
        }
    }

    /// <summary>
    /// Class for saving data about the apartment
    /// </summary>
    public class ApartmentState
    {
        public string name { get; set; }
        public string type { get; set; }
        public int id { get; set; }
        public bool isCorner { get; set; }
        public bool isExterior { get; set; }
        public float width { get; set; }
        public float depth { get; set; }
        public float[] vertices { get; set; }
        public int[] triangles { get; set; }
        public int corridorIndex { get; set; }
        public bool hasCorridor { get; set; }
        public bool flipped { get; set; }
        public bool isM3 { get; set; }

        public ApartmentState()
        {

        }
    }

    /// <summary>
    /// Class for saving data about the Standards and General Information
    /// </summary>
    public class StandardsState
    {
        public Dictionary<string, double> DesiredAreas { get; set; }
        public Dictionary<string, double> DesiredAreasM3 { get; set; }
        public Dictionary<string, double> ApartmentTypesMinimumSizes { get; set; }
        public Dictionary<string, float> AmenitiesAreas { get; set; }
        public Dictionary<string, double> ApartmentTypesMaximumSizes { get; set; }
        public Dictionary<string, bool[]> ApartmentTypeRooms { get; set; }
        public Dictionary<string, bool[]> ApartmentTypeRoomsM3 { get; set; }
        public Dictionary<string, bool[]> ApartmentTypeRoomsMansion { get; set; }
        public Dictionary<string, bool[]> ApartmentTypeRoomsMansionM3 { get; set; }
        public Dictionary<string, float> ConstructionFeatures { get; set; }
        public string ConstructionFeaturesTitle { get; set; }
        public Dictionary<string, float[]> customVals { get; set; }
        public Dictionary<string, float[]> customValsM3 { get; set; }
        public Dictionary<string, List<string>> ValidLineLoads { get; set; }
        public Dictionary<string, Dictionary<string, float>> TotalAmenitiesArea { get; set; }
        public int Balconies { get; set; }
        public Dictionary<string, Dictionary<string, WindowsPosition>> WindowsPositions { get; set; }
        public Dictionary<string, Dictionary<string, WindowsPosition>> WindowsPositionsM3 { get; set; }
        public Dictionary<string, Dictionary<string, WindowsPosition>> WindowsPositionsMansion { get; set; }
        public Dictionary<string, Dictionary<string, WindowsPosition>> WindowsPositionsMansionM3 { get; set; }
        public float roofDepth { get; set; }
        public float parapetHeight { get; set; }

        public StandardsState()
        {

        }
    }

    /// <summary>
    /// Class for saving data about the geometries and centre-lines
    /// </summary>
    public class GeometryState
    {
        public List<FloorLayoutState> states { get; set; }

        public GeometryState()
        {
            states = new List<FloorLayoutState>();
        }
    }

    /// <summary>
    /// Class for saving data about the brief of each building
    /// </summary>
    public class BriefState
    {
        public List<BuildingData> buildings { get; set; }

        public BriefState()
        {
            buildings = new List<BuildingData>();
        }
    }

    /// <summary>
    /// Class for combining all the saved data
    /// </summary>
    public class Save
    {
        public GeometryState geometryState { get; set; }
        public BriefState briefState { get; set; }
        public SiteState siteState { get; set; }
        public StandardsState standardsState { get; set; }

        public Save()
        {

        }
    }
}

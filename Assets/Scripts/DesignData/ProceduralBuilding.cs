﻿using BrydenWoodUnity.GeometryManipulation;
using BrydenWoodUnity.Lighting;
using BrydenWoodUnity.UIElements;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using BT = BrydenWoodUnity.GeometryManipulation.ThreeJs;

namespace BrydenWoodUnity.DesignData
{
    /// <summary>
    /// A MonoBehaviour component for buildings
    /// </summary>
    public class ProceduralBuilding : MonoBehaviour
    {
        #region Public Properties
        [Header("Prefabs:")]
        public GameObject proceduralFloorPrefab;
        public GameObject proceduralTowerPrefab;
        public GameObject proceduralMansionPrefab;
        public GameObject coreDistancePrefab;

        [Header("Scene References:")]
        public ProceduralBuildingManager buildingManager;
        public BuildingElement buildingElement;

        [Header("Settings:")]
        public PreviewMode previewMode = PreviewMode.Buildings;
        public BuildingType buildingType { get; private set; }
        public CoreAllignment coreAllignment = CoreAllignment.Centre;
        public LinearTypology typology = LinearTypology.Single;
        public float extraFloorDepth;
        public bool useCanvas = true;
        public float distanceOffset = 1.0f;
        public float lineWidth = 0.5f;
        public float offsetDistanceLine = 4;
        public float totalHeight;
        public float baseHeight
        {
            get
            {
                if (sitePodium != null)
                {
                    return sitePodium.floors * sitePodium.floor2Floor;
                }
                else return 0;
            }
        }
        public SiteBasement siteBasement;
        public SitePodium sitePodium;
        public CoreLock coreLock = CoreLock.CorridorWall;
        public float[] coreParameters;
        public float coreWidth = 4.8f;
        public float coreLength = 7f;
        public string numbersFormat { get { return buildingManager.numbersFormat; } }

        public int maxLevel
        {
            get
            {
                int levels = 0;

                for (int i = 0; i < floors.Count; i++)
                {
                    if (levels < floors[i].levels.Last())
                        levels = floors[i].levels.Last();
                }

                return levels;
            }
        }

        public int Index
        {
            get
            {
                return buildingManager.proceduralBuildings.IndexOf(this);
            }
        }

        private Vector3 _coreDimensions;
        //public Vector3 currentCoreDimensions
        //{
        //    get
        //    {
        //        Vector3 _dims = new Vector3(8, 3, 8);
        //        float structuralWall = 0.3f;
        //        var lvls = maxLevel;
        //        if (lvls < Standards.TaggedObject.towerHeightsForCores[0])
        //        {
        //            _coreDimensions = Standards.TaggedObject.coreDimensions[0];
        //            structuralWall = Standards.TaggedObject.structuralWallDimensions[0];
        //        }
        //        else if (lvls >= Standards.TaggedObject.towerHeightsForCores[0] && lvls < Standards.TaggedObject.towerHeightsForCores[1])
        //        {
        //            _coreDimensions = Standards.TaggedObject.coreDimensions[1];
        //            structuralWall = Standards.TaggedObject.structuralWallDimensions[1];
        //        }
        //        else
        //        {
        //            _coreDimensions = Standards.TaggedObject.coreDimensions[2];
        //            structuralWall = Standards.TaggedObject.structuralWallDimensions[2];
        //        }
        //        Standards.TaggedObject.EvaluateStructuralWalls();
        //        return _coreDimensions;
        //    }
        //    set
        //    {
        //        _coreDimensions = value;
        //    }
        //}


        public Balconies balconies { get { return Standards.TaggedObject.balconies; } }
        public ProceduralFloor currentFloor { get; set; }
        public List<ProceduralFloor> floors { get; set; }
        public ProceduralFloor basement { get; set; }
        public ProceduralFloor podium { get; set; }
        //public List<FloorLayoutState> floorLayoutStates { get; set; }
        public Polygon masterPolyline { get; set; }
        public bool Placed { get; set; }

        /// <summary>
        /// The maximum apartment width based on the brief of the current building
        /// </summary>
        public float currentMaximumWidth
        {
            get
            {
                var percentages = buildingElement.buildingData.percentages.ToList();//floors.Select(x => x.percentages).ToList();
                List<string> distinctTypes = new List<string>();
                int counter = 0;
                foreach (var item in percentages)
                {
                    foreach (var perc in item)
                    {
                        if (!distinctTypes.Contains(perc.Key) && perc.Key != "Other" && perc.Key != "Commercial")
                        {
                            distinctTypes.Add(perc.Key);
                            counter++;
                        }
                    }
                }

                float maxDepth = Math.Max(floors[0].MaxLeftAptDepth, floors[0].MaxRightAptDepth);

                if (counter != 0)
                {

                    distinctTypes = distinctTypes.OrderBy(x => Standards.TaggedObject.DesiredAreas[x]).ToList();
                    distinctTypes.Reverse();

                    return ((float)Standards.TaggedObject.DesiredAreas[distinctTypes[0]] / maxDepth) + Standards.TaggedObject.ConstructionFeatures["PartyWall"];
                }
                else
                    return ((float)Standards.TaggedObject.DesiredAreas["1b2p"] / maxDepth) + Standards.TaggedObject.ConstructionFeatures["PartyWall"];
            }
        }

        /// <summary>
        /// Whether at least one of its floors has geometry
        /// </summary>
        public bool hasGeometry
        {
            get
            {
                int count = 0;
                for (int i = 0; i < floors.Count; i++)
                {
                    if (floors[i].hasGeometry)
                    {
                        count++;
                    }
                }
                return count != 0;
            }
        }

        public float prevMinimumWidth { get; set; }

        /// <summary>
        /// The polyline parameters of the cores for the current building
        /// </summary>
        public List<float> currentCoresParameters
        {
            get
            {
                return floors[0].coresParams;
            }
        }

        /// <summary>
        /// The length between the corridors for the current floor
        /// </summary>
        public float currentCoreLength
        {
            get
            {
                return floors[0].coreLength;
            }
        }

        public float corridorWidth { get; set; }

        //private float m_leftAptDepth;
        //public float leftApartmentDepth
        //{
        //    get
        //    {
        //        return m_leftAptDepth;
        //    }
        //    set
        //    {
        //        m_leftAptDepth = value;
        //        if (floors != null)
        //        {
        //            for (int i = 0; i < floors.Count; i++)
        //            {
        //                floors[i].leftAptDepth = value;
        //            }
        //        }
        //    }
        //}

        public bool populateApartments { get; set; }
        public string buildingName { get; set; }
        public int index
        {
            get
            {
                return buildingManager.proceduralBuildings.IndexOf(this);
            }
        }
        public Transform repRootParent { get; set; }
        public List<GameObject> apartmentsInteriors { get; private set; }
        public ManufacturingSystem manufacturingSystem { get; private set; }
        public Text distanceUI { get; private set; }
        public Transform distanceUIParent { get; set; }
        public Material distancesMaterial { get; private set; }
        public List<ProceduralTower> towerConfigurations { get; set; }
        public int numberOfFloors
        {
            get
            {
                if (floors != null && floors.Count > 0)
                {
                    if (floors[floors.Count - 1] != null && floors[floors.Count - 1].levels != null && floors[floors.Count - 1].levels.Length > 0)
                    {
                        return floors.Last().levels.Last() + 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else return 0;
            }
        }
        public float MaxLeftAptDepth
        {
            get
            {
                float max = float.MinValue;
                for (int i = 0; i < floors.Count; i++)
                {
                    if (max < floors[i].leftAptDepth)
                    {
                        max = floors[i].leftAptDepth;
                    }
                }
                return max;
            }
        }

        public float MinLeftAptDepth
        {
            get
            {
                float min = float.MaxValue;
                for (int i = 0; i < floors.Count; i++)
                {
                    if (min > floors[i].leftAptDepth)
                    {
                        min = floors[i].leftAptDepth;
                    }
                }
                if (min < coreWidth)
                {
                    min = coreWidth;
                }
                return min;
            }
        }

        public float MaxRightAptDepth
        {
            get
            {
                float max = float.MinValue;
                for (int i = 0; i < floors.Count; i++)
                {
                    if (max < floors[i].rightAptDepth)
                    {
                        max = floors[i].rightAptDepth;
                    }
                }
                return max;
            }
        }
        public float MinRightAptDepth
        {
            get
            {
                float min = float.MaxValue;
                for (int i = 0; i < floors.Count; i++)
                {
                    if (min > floors[i].rightAptDepth)
                    {
                        min = floors[i].rightAptDepth;
                    }
                }
                if (min < coreWidth)
                {
                    min = coreWidth;
                }
                return min;
            }
        }

        public float PodiumArea
        {
            get
            {
                if (podium == null)
                {
                    return 0;
                }
                else
                {
                    if (podium.hasGeometry)
                    {
                        return podium.exteriorPolygon.Area * podium.levels.Length;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }

        public float BasementArea
        {
            get
            {
                if (basement == null)
                {
                    return 0;
                }
                else
                {
                    if (basement.hasGeometry)
                    {
                        return basement.exteriorPolygon.Area * basement.levels.Length;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }
        #endregion

        #region Private Fields and Properties
        private Polygon sitePolygon { get; set; }
        private ProceduralFloor lastAddedFloor { get; set; }
        private float plantRoomArea { get; set; }
        private WaitForEndOfFrame waitFrame { get; set; }
        public GameObject heightLine { get; private set; }
        private GameObject currentDistance { get; set; }
        private Transform distancesParent { get; set; }
        private float GEA = 0;
        private float GIA = 0;
        private float NIA = 0;
        private float GrossToNet = 0;
        private float ApartmentsArea = 0;
        private float FacadeArea = 0;
        private float FacadePerimeter = 0;
        private float WallToFloor = 0;
        private float NumberOfPeople = 0;
        private float Levels = 0;
        private Transform coreDistanceParent;
        private bool showCoreDistances;
        #endregion

        #region MonoBehaviour Methods
        public void Start()
        {
            //floors = new List<ProceduralFloor>();
            //floorLayoutStates = new List<FloorLayoutState>();

        }

        private void OnDestroy()
        {
            if (distanceUI != null)
            {
                Destroy(distanceUI.gameObject);
            }
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Initializes the building instance
        /// </summary>
        public void Initialize()
        {
            floors = new List<ProceduralFloor>();
            waitFrame = new WaitForEndOfFrame();
            distancesMaterial = Resources.Load("Materials/DistanceMaterial") as Material;
            distancesMaterial.color = new Color(0.93f, 0.15f, 0.43f);
            distancesMaterial.SetColor("_EmissionColor", new Color(0.93f, 0.15f, 0.43f));
            distancesMaterial.EnableKeyword("_EMISSION");
            coreWidth = Standards.TaggedObject.CoreSizesPerBuildingType[buildingType][0];
            coreLength = Standards.TaggedObject.CoreSizesPerBuildingType[buildingType][1];
        }

        /// <summary>
        /// It sets the type of the building
        /// </summary>
        /// <param name="buildingType">The new building type</param>
        public void SetBuildingType(BuildingType buildingType)
        {
            SetBuildingType(buildingType, Standards.TaggedObject.CoreSizesPerBuildingType[buildingType][0], Standards.TaggedObject.CoreSizesPerBuildingType[buildingType][1]);
        }

        /// <summary>
        /// It sets the type of the building
        /// </summary>
        /// <param name="buildingType">The new building type</param>
        /// <param name="coreWidth">The new core width</param>
        /// <param name="coreLength">The new core length</param>
        public void SetBuildingType(BuildingType buildingType, float coreWidth, float coreLength)
        {
            this.buildingType = buildingType;
            this.coreWidth = coreWidth;
            this.coreLength = coreLength;
        }

        /// <summary>
        /// Refreshes the building instance
        /// </summary>
        /// <param name="offsetDistance">The apartment depth</param>
        /// <returns>IEnumerator</returns>
        public IEnumerator Refresh(float offsetDistance, bool recalculateCoreParams = false)
        {
            float maxWidth = currentMaximumWidth;
            prevMinimumWidth = maxWidth;
            if (floors[0].coresParams != null)
                coreParameters = floors[0].coresParams.ToArray(); ;
            //leftApartmentDepth = offsetDistance;
            if (floors != null && floors.Count > 0)
            {
                if (basement != null)
                {
                    switch (buildingType)
                    {
                        case BuildingType.Tower:
                            var towerBasement = basement as ProceduralTower;
                            towerBasement.yVal = 0;
                            towerBasement.transform.position = new Vector3(basement.transform.position.x, towerBasement.yVal, basement.transform.position.z);
                            towerBasement.aptDepth = offsetDistance;
                            towerBasement.baseTower = floors[0] as ProceduralTower;
                            towerBasement.GenerateGeometries();
                            yield return waitFrame;
                            buildingManager.apartmentTypesChart.designData = towerBasement;
                            break;
                        case BuildingType.Mansion:
                            var mansionBasement = basement as ProceduralMansion;
                            mansionBasement.yVal = 0;
                            mansionBasement.transform.position = new Vector3(basement.transform.position.x, mansionBasement.yVal, basement.transform.position.z);
                            mansionBasement.aptDepth = offsetDistance;
                            mansionBasement.baseFloor = floors[0] as ProceduralMansion;
                            mansionBasement.GenerateGeometries();
                            yield return waitFrame;
                            buildingManager.apartmentTypesChart.designData = mansionBasement;
                            break;
                        case BuildingType.Linear:
                            basement.yVal = 0;// basement.levels.Length * basement.floor2floor * -1f;
                            basement.transform.position = new Vector3(basement.transform.position.x, basement.yVal, basement.transform.position.z);
                            if (basement.centreLine == null)
                            {
                                var polygon = UseMasterPolyline(basement.yVal);
                                SetCentreLineToFloor(basement, polygon);
                            }
                            basement.leftAptDepth = floors[0].leftAptDepth;
                            yield return StartCoroutine(basement.GenerateGeometries(maxWidth));
                            buildingManager.apartmentTypesChart.designData = basement;
                            GetCoreDistances();
                            break;
                    }

                    //if (towerBasement != null)
                    //{

                    //}
                    //else
                    //{
                    //    basement.yVal = 0;// basement.levels.Length * basement.floor2floor * -1f;
                    //    basement.transform.position = new Vector3(basement.transform.position.x, basement.yVal, basement.transform.position.z);
                    //    if (basement.centreLine == null)
                    //    {
                    //        var polygon = UseMasterPolyline(basement.yVal);
                    //        SetCentreLineToFloor(basement, polygon);
                    //    }
                    //    basement.leftAptDepth = floors[0].leftAptDepth;
                    //    yield return StartCoroutine(basement.GenerateGeometries(maxWidth));
                    //    buildingManager.apartmentTypesChart.designData = basement;
                    //}
                }

                if (podium != null)
                {
                    switch (buildingType)
                    {
                        case BuildingType.Tower:
                            var towerPodium = podium as ProceduralTower;
                            towerPodium.yVal = baseHeight;
                            towerPodium.transform.position = new Vector3(towerPodium.transform.position.x, towerPodium.yVal, towerPodium.transform.position.z);
                            towerPodium.aptDepth = offsetDistance;
                            towerPodium.baseTower = floors[0] as ProceduralTower;
                            towerPodium.GenerateGeometries();
                            yield return waitFrame;
                            buildingManager.apartmentTypesChart.designData = towerPodium;
                            break;
                        case BuildingType.Mansion:
                            var mansionPodium = podium as ProceduralMansion;
                            mansionPodium.yVal = baseHeight;
                            mansionPodium.transform.position = new Vector3(mansionPodium.transform.position.x, mansionPodium.yVal, mansionPodium.transform.position.z);
                            mansionPodium.aptDepth = offsetDistance;
                            mansionPodium.baseFloor = floors[0] as ProceduralMansion;
                            mansionPodium.GenerateGeometries();
                            yield return waitFrame;
                            buildingManager.apartmentTypesChart.designData = mansionPodium;
                            break;
                        case BuildingType.Linear:
                            podium.yVal = baseHeight;
                            podium.transform.position = new Vector3(podium.transform.position.x, podium.yVal, podium.transform.position.z);
                            if (podium.centreLine == null)
                            {
                                var polygon = UseMasterPolyline(podium.yVal);
                                SetCentreLineToFloor(podium, polygon);
                            }
                            podium.leftAptDepth = floors[0].leftAptDepth;
                            yield return StartCoroutine(podium.GenerateGeometries(maxWidth));
                            buildingManager.apartmentTypesChart.designData = podium;
                            break;
                    }
                }

                for (int i = 0; i < floors.Count; i++)
                {
                    floors[i].corridorWidth = corridorWidth;
                    switch (buildingType)
                    {
                        case BuildingType.Tower:
                            var tower = floors[i] as ProceduralTower;
                            tower.aptDepth = offsetDistance;
                            tower.GenerateGeometries();
                            yield return waitFrame;
                            buildingManager.apartmentTypesChart.designData = tower;
                            break;
                        case BuildingType.Mansion:
                            var mansion = floors[i] as ProceduralMansion;
                            //mansion.aptDepth = offsetDistance;
                            mansion.GenerateGeometries();
                            yield return waitFrame;
                            buildingManager.apartmentTypesChart.designData = mansion;
                            break;
                        case BuildingType.Linear:
                            var item = floors[i];
                            var perc = floors[i].percentages;
                            item.percentages = perc;
                            GetCoreDistances();
                            if (podium == null)
                            {
                                if (i == 0)
                                {
                                    if (item.centreLine == null)
                                    {
                                        var polygon = UseMasterPolyline(i);
                                        SetCentreLineToFloor(item, polygon);
                                    }
                                    //item.leftAptDepth = offsetDistance;
                                    yield return StartCoroutine(item.GenerateGeometries(maxWidth, recalculateCoreParams));
                                    buildingManager.apartmentTypesChart.designData = item;
                                }
                                else
                                {
                                    var yVal = buildingElement.GetYForLevel(item.levels[0], i);
                                    item.yVal = yVal;
                                    item.transform.position = new Vector3(item.transform.position.x, yVal, item.transform.position.z);
                                    if (item.centreLine == null)
                                    {
                                        var polygon = UseMasterPolyline(item.id);
                                        SetCentreLineToFloor(item, polygon);
                                    }
                                    //item.leftAptDepth = offsetDistance;
                                    yield return StartCoroutine(item.GenerateGeometries(floors[0], maxWidth));
                                    buildingManager.apartmentTypesChart.designData = item;
                                }
                            }
                            else
                            {
                                var yVal = buildingElement.GetYForLevel(item.levels[0], i);
                                item.yVal = yVal;
                                item.transform.position = new Vector3(item.transform.position.x, yVal, item.transform.position.z);
                                if (item.centreLine == null)
                                {
                                    var polygon = UseMasterPolyline(item.id);
                                    SetCentreLineToFloor(item, polygon);
                                }
                                //item.leftAptDepth = offsetDistance;
                                if (i == 0)
                                {
                                    yield return StartCoroutine(item.GenerateGeometries(maxWidth));
                                }
                                else
                                {
                                    yield return StartCoroutine(item.GenerateGeometries(floors[0], maxWidth));
                                }
                                buildingManager.apartmentTypesChart.designData = item;
                            }
                            break;
                    }
                }
                //floors[0].GeneratePlantRoom();

                if (heightLine == null)
                {
                    if (buildingType == BuildingType.Linear)
                        AddHeightLine(masterPolyline);
                    else
                        AddHeightLine();
                }
                UpdateHeightLine(masterPolyline);
                GetCoreDistances();               
                buildingManager.CheckIfBuildingOverPodium(this);
                buildingManager.CheckIfBuildingOverBasement(this);
                buildingManager.CheckSystemConstraints(this);
            }
        }

        /// <summary>
        /// It deletes the podium of the building
        /// </summary>
        public void DeletePodium()
        {
            if (podium != null)
            {
                Destroy(podium.gameObject);
                podium = null;
                for (int i = 0; i < floors.Count; i++)
                {
                    floors[i].UpdatePosition();
                }

                buildingElement.DeletePodium();
            }
        }

        /// <summary>
        /// It deletes the basement of the building
        /// </summary>
        public void DeleteBasement()
        {
            if (basement != null)
            {
                Destroy(basement.gameObject);
                basement = null;
                buildingElement.DeleteBasement();
            }
        }

        /// <summary>
        /// It generates the basement of the building
        /// </summary>
        /// <param name="loaded">Whether the basement is being loaded from a previous configuration</param>
        /// <returns>IEnumerator</returns>
        public IEnumerator GenerateBasement(bool loaded = false)
        {
            if (basement != null)
            {
                switch (buildingType)
                {
                    case BuildingType.Tower:
                        var tower = basement as ProceduralTower;
                        tower.yVal = 0;
                        tower.transform.position = new Vector3(tower.transform.position.x, tower.yVal, tower.transform.position.z);
                        tower.aptDepth = (floors[0] as ProceduralTower).aptDepth;
                        tower.baseTower = floors[0] as ProceduralTower;
                        tower.GenerateGeometries();
                        yield return waitFrame;
                        buildingManager.apartmentTypesChart.designData = tower;
                        break;
                    case BuildingType.Mansion:
                        var mansion = basement as ProceduralMansion;
                        mansion.yVal = 0;
                        mansion.transform.position = new Vector3(mansion.transform.position.x, mansion.yVal, mansion.transform.position.z);
                        mansion.aptDepth = (floors[0] as ProceduralMansion).aptDepth;
                        mansion.baseFloor = floors[0] as ProceduralMansion;
                        mansion.GenerateGeometries();
                        yield return waitFrame;
                        buildingManager.apartmentTypesChart.designData = mansion;
                        break;
                    case BuildingType.Linear:
                        basement.yVal = 0;
                        basement.transform.position = new Vector3(basement.transform.position.x, basement.yVal, basement.transform.position.z);
                        if (basement.centreLine == null)
                        {
                            var polygon = UseMasterPolyline(basement.yVal);
                            SetCentreLineToFloor(basement, polygon);
                        }
                        basement.leftAptDepth = floors[0].leftAptDepth;
                        basement.rightAptDepth = floors[0].rightAptDepth;
                        yield return StartCoroutine(basement.GenerateGeometries(currentMaximumWidth));
                        buildingManager.apartmentTypesChart.designData = basement;
                        break;
                }
            }
        }

        /// <summary>
        /// It loads the basement of the building
        /// </summary>
        public void LoadBasement()
        {
            if (basement != null)
            {
                switch (buildingType)
                {
                    case BuildingType.Tower:
                        var tower = basement as ProceduralTower;
                        tower.yVal = 0;
                        tower.transform.position = new Vector3(tower.transform.position.x, tower.yVal, tower.transform.position.z);
                        tower.aptDepth = (floors[0] as ProceduralTower).aptDepth;
                        tower.baseTower = floors[0] as ProceduralTower;
                        tower.GenerateGeometries();
                        //yield return waitFrame;
                        buildingManager.apartmentTypesChart.designData = tower;
                        break;
                    case BuildingType.Mansion:
                        var mansion = basement as ProceduralMansion;
                        mansion.yVal = 0;
                        mansion.transform.position = new Vector3(mansion.transform.position.x, mansion.yVal, mansion.transform.position.z);
                        mansion.aptDepth = (floors[0] as ProceduralMansion).aptDepth;
                        mansion.baseFloor = floors[0] as ProceduralMansion;
                        mansion.GenerateGeometries();
                        buildingManager.apartmentTypesChart.designData = mansion;
                        break;
                    case BuildingType.Linear:
                        basement.yVal = 0;
                        basement.transform.position = new Vector3(basement.transform.position.x, basement.yVal, basement.transform.position.z);
                        if (basement.centreLine == null)
                        {
                            var polygon = UseMasterPolyline(basement.yVal);
                            SetCentreLineToFloor(basement, polygon);
                        }
                        basement.leftAptDepth = floors[0].leftAptDepth;
                        basement.rightAptDepth = floors[0].rightAptDepth;
                        //yield return StartCoroutine(basement.GenerateGeometries(currentMaximumWidth));
                        buildingManager.apartmentTypesChart.designData = basement;
                        break;
                }
            }
        }

        /// <summary>
        /// It generates the podium of the building
        /// </summary>
        /// <param name="state">The floor save state if the floor was loaded</param>
        /// <returns>IEnumerator</returns>
        public IEnumerator GeneratePodium(FloorLayoutState state = null)
        {
            if (podium != null)
            {
                var tower = podium as ProceduralTower;
                var mansion = podium as ProceduralMansion;
                if (tower != null)
                {
                    tower.yVal = baseHeight;
                    tower.transform.position = new Vector3(tower.transform.position.x, tower.yVal, tower.transform.position.z);
                    tower.aptDepth = (floors[0] as ProceduralTower).aptDepth;
                    tower.baseTower = floors[0] as ProceduralTower;
                    tower.GenerateGeometries();
                    yield return waitFrame;
                    for (int i = 0; i < floors.Count; i++)
                    {
                        floors[i].UpdatePosition(state != null/*baseHeight + tower.levels.Length * tower.floor2floor + 0.5f*/);
                    }
                    buildingManager.apartmentTypesChart.designData = tower;
                }
                else if (mansion != null)
                {
                    mansion.yVal = baseHeight;
                    mansion.transform.position = new Vector3(mansion.transform.position.x, mansion.yVal, mansion.transform.position.z);
                    mansion.aptDepth = (floors[0] as ProceduralMansion).aptDepth;
                    mansion.baseFloor = floors[0] as ProceduralMansion;
                    mansion.GenerateGeometries();
                    yield return waitFrame;
                    for (int i = 0; i < floors.Count; i++)
                    {
                        floors[i].UpdatePosition(state != null/*baseHeight + tower.levels.Length * tower.floor2floor + 0.5f*/);
                    }
                    buildingManager.apartmentTypesChart.designData = tower;
                }
                else
                {
                    podium.yVal = baseHeight;
                    podium.transform.position = new Vector3(podium.transform.position.x, podium.yVal, podium.transform.position.z);
                    if (podium.centreLine == null)
                    {
                        var polygon = UseMasterPolyline(baseHeight);
                        SetCentreLineToFloor(podium, polygon);
                    }

                    for (int i = 0; i < floors.Count; i++)
                    {
                        //if (i == 0)
                        //{
                        //var polygon = UseMasterPolyline(baseHeight + podium.levels.Length * podium.floor2floor + 0.5f);
                        //SetCentreLineToFloor(floors[i], polygon, state != null);
                        //floors[i].UpdateFloorOutline(buildingManager.previewMode);
                        //}
                        //else
                        //{
                        floors[i].UpdatePosition(state != null/*baseHeight + podium.levels.Length * podium.floor2floor + 0.5f*/);
                        //}
                    }

                    podium.leftAptDepth = floors[0].leftAptDepth;
                    podium.rightAptDepth = floors[0].rightAptDepth;
                    yield return StartCoroutine(podium.GenerateGeometries(currentMaximumWidth));
                    podium.UpdateFloorOutline(buildingManager.previewMode);
                    buildingManager.apartmentTypesChart.designData = podium;
                }
                //UpdateHeightLine(masterPolyline);

                if (state != null && state.customExterior != null && state.customExterior.Length > 0)
                {
                    podium.hasCustomExterior = true;
                    //   buildingManager.LoadCustomPodiumFloor(state);
                }
            }
        }

        /// <summary>
        /// It loads the podium of the building
        /// </summary>
        /// <param name="state">The floor save state</param>
        public void LoadPodium(FloorLayoutState state)
        {
            if (podium != null)
            {
                switch (buildingType)
                {
                    case BuildingType.Tower:
                        var tower = podium as ProceduralTower;
                        tower.yVal = baseHeight;
                        tower.transform.position = new Vector3(tower.transform.position.x, tower.yVal, tower.transform.position.z);
                        tower.aptDepth = (floors[0] as ProceduralTower).aptDepth;
                        tower.baseTower = floors[0] as ProceduralTower;
                        for (int i = 0; i < floors.Count; i++)
                        {
                            floors[i].UpdatePosition();
                        }
                        buildingManager.apartmentTypesChart.designData = tower;
                        break;
                    case BuildingType.Mansion:
                        var mansion = podium as ProceduralMansion;
                        mansion.yVal = baseHeight;
                        mansion.transform.position = new Vector3(mansion.transform.position.x, mansion.yVal, mansion.transform.position.z);
                        mansion.aptDepth = (floors[0] as ProceduralMansion).aptDepth;
                        mansion.baseFloor = floors[0] as ProceduralMansion;
                        for (int i = 0; i < floors.Count; i++)
                        {
                            floors[i].UpdatePosition();
                        }
                        buildingManager.apartmentTypesChart.designData = mansion;
                        break;
                    case BuildingType.Linear:
                        podium.yVal = baseHeight;
                        podium.transform.position = new Vector3(podium.transform.position.x, podium.yVal, podium.transform.position.z);
                        if (podium.centreLine == null)
                        {
                            var polygon = UseMasterPolyline(baseHeight);
                            SetCentreLineToFloor(podium, polygon);
                        }

                        for (int i = 0; i < floors.Count; i++)
                        {
                            floors[i].UpdatePosition(true);
                        }

                        podium.leftAptDepth = floors[0].leftAptDepth;
                        podium.rightAptDepth = floors[0].rightAptDepth;
                        buildingManager.apartmentTypesChart.designData = podium;
                        break;
                }

                if (state != null && state.customExterior != null && state.customExterior.Length > 0)
                {
                    podium.hasCustomExterior = true;
                }
            }
        }

        /// <summary>
        /// Populates the apartments of the floors with their interior
        /// </summary>
        /// <returns>IEnumerator</returns>
        public IEnumerator PopulateInterior()
        {
            populateApartments = !populateApartments;
            yield return StartCoroutine(UpdateInternalLayout());
        }

        /// <summary>
        /// Populates the apartments of the floors with their interior
        /// </summary>
        /// <param name="show">Turn interior on or off</param>
        /// <returns>IEnumerator</returns>
        public IEnumerator PopulateInterior(bool show)
        {
            populateApartments = show;
            yield return StartCoroutine(UpdateInternalLayout());
        }

        /// <summary>
        /// Updates the interior of the apartments of the floors
        /// </summary>
        /// <returns>IEnumerator</returns>
        public IEnumerator UpdateInternalLayout()
        {
            yield return UpdateInternalLayout(populateApartments);
        }

        /// <summary>
        /// Used to resolve a Special Apartment to its neighbours
        /// </summary>
        /// <param name="aptsToCombine">The apartments to be combined</param>
        public void OnResolveApartmentWithNext(List<BaseDesignData> aptsToCombine)
        {
            buildingManager.SetSwapApartments(aptsToCombine);
            buildingManager.CombineApartments();
        }

        /// <summary>
        /// Returns a detailed list of all the modules in the building
        /// </summary>
        /// <returns>List of strings</returns>
        public List<string> GetModuleDetails()
        {
            List<string> lines = new List<string>();
            //ModuleID,ModuleWidth,ModuleDepth,ModuleHeight,ApartmentID,ApartmentType

            for (int i = 0; i < apartmentsInteriors.Count; i++)
            {
                for (int j = 0; j < apartmentsInteriors[i].transform.GetChild(1).childCount; j++)
                {
                    var obj = apartmentsInteriors[i].transform.GetChild(1).GetChild(j);
                    lines.Add(string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}", obj.name, obj.transform.localScale.x, obj.transform.localScale.z, obj.transform.localScale.y, "N/A", apartmentsInteriors[i].GetComponent<ProceduralApartmentLayout>().apartmentType, obj.transform.position.x, obj.transform.position.y, obj.transform.position.z, obj.transform.eulerAngles.x, obj.transform.eulerAngles.y, obj.transform.eulerAngles.z));
                }
            }

            return lines;
        }

        /// <summary>
        /// It gets the geometry details of the apartments
        /// </summary>
        /// <returns>List of String</returns>
        public List<string> GetApartmentDetails()
        {
            List<string> lines = new List<string>();
            for (int i = 0; i < floors.Count; i++)
            {
                if (!floors[i].percentages.ContainsKey("Commercial") && !floors[i].percentages.ContainsKey("Other"))
                {
                    var aptCoords = floors[i].apartmentsOutlinesCoordinates;
                    for (int j = 0; j < aptCoords.Length; j++)
                    {
                        lines.Add(aptCoords[j]);
                    }
                }
            }
            return lines;
        }

        /// <summary>
        /// It gets the geometry details of the cores
        /// </summary>
        /// <returns>List of String</returns>
        public List<string> GetCoresDetails()
        {
            List<string> lines = new List<string>();
            for (int i = 0; i < floors.Count; i++)
            {
                var coreLines = floors[i].coresDescriptions;
                for (int j = 0; j < coreLines.Length; j++)
                {
                    lines.Add(coreLines[j]);
                }
            }
            return lines;
        }

        /// <summary>
        /// It gets the geometry details of the floors' outlines
        /// </summary>
        /// <returns>List of String</returns>
        public List<string> GetFloorOutlineDetails()
        {
            List<string> lines = new List<string>();
            for (int i = 0; i < floors.Count; i++)
            {
                var coreLines = floors[i].outlineCoordinates;
                for (int j = 0; j < coreLines.Length; j++)
                {
                    lines.Add(coreLines[j]);
                }
            }
            return lines;
        }

        /// <summary>
        /// Returns the number for each different module type and size
        /// </summary>
        /// <returns>String Int Dictionary</returns>
        public Dictionary<string, int> GetModuleSchedule()
        {
            Dictionary<string, int> modSched = new Dictionary<string, int>();

            for (int i = 0; i < apartmentsInteriors.Count; i++)
            {
                for (int j = 0; j < apartmentsInteriors[i].transform.GetChild(1).childCount; j++)
                {
                    var obj = apartmentsInteriors[i].transform.GetChild(1).GetChild(j);
                    string name = obj.name + ":" + obj.transform.localScale.x + "," + obj.transform.localScale.z + "," + obj.transform.localScale.y;
                    if (modSched.ContainsKey(name))
                    {
                        modSched[name]++;
                    }
                    else
                    {
                        modSched.Add(name, 1);
                    }
                }
            }

            return modSched;
        }

        /// <summary>
        /// Returns the different types and numbers of panels required for the building
        /// </summary>
        /// <returns>String Int Dictionary</returns>
        public Dictionary<string, int> GetPanelSchedule()
        {
            Dictionary<string, int> panSched = new Dictionary<string, int>();

            for (int i = 0; i < apartmentsInteriors.Count; i++)
            {
                for (int j = 0; j < apartmentsInteriors[i].transform.GetChild(2).childCount; j++)
                {
                    var obj = apartmentsInteriors[i].transform.GetChild(2).GetChild(j);
                    string name = "Panel:" + (obj.transform.localScale.x * obj.transform.localScale.z * obj.transform.localScale.y) / 0.15f + ":" + obj.transform.localScale.x + "," + obj.transform.localScale.z + "," + obj.transform.localScale.y;
                    if (panSched.ContainsKey(name))
                    {
                        panSched[name]++;
                    }
                    else
                    {
                        panSched.Add(name, 1);
                    }
                }
            }

            return panSched;
        }

        /// <summary>
        /// Logs the Undo-able resolve action
        /// </summary>
        /// <param name="action">The action to be logged</param>
        public void SetResolveAction(ResolveAction action)
        {
            buildingManager.SetResolveAction(action);
        }

        /// <summary>
        /// Called when this building has been deleted
        /// </summary>
        public void Delete()
        {
            Destroy(repRootParent.gameObject);
        }

        /// <summary>
        /// Called when this building has been selected
        /// </summary>
        public void Select()
        {
            StartCoroutine(OnSelect());
        }

        /// <summary>
        /// Called when this building is being deselected
        /// </summary>
        /// <returns>IEnumerator</returns>
        public IEnumerator Deselect()
        {

            if (previewMode == PreviewMode.Floors)
            {
                if (populateApartments)
                {
                    populateApartments = false;
                    yield return StartCoroutine(UpdateInternalLayout(false));
                }
                for (int i = 0; i < floors.Count; i++)
                {
                    if (floors[i].centreLine != null)
                    {
                        floors[i].centreLine.gameObject.SetActive(false);
                    }
                    floors[i].gameObject.SetActive(true);
                    floors[i].ToggleChildren(false);
                }
            }
        }

        /// <summary>
        /// Called when a floor from this building has been deleted
        /// </summary>
        /// <param name="floor">The deleted floor</param>
        public void OnFloorDeleted(ProceduralFloor floor)
        {
            floors.Remove(floor);
        }

        /// <summary>
        /// Called when a floor from this building has been selected
        /// </summary>
        /// <param name="floor">The selected floor</param>
        /// <returns>IEnumerator</returns>
        public IEnumerator OnFloorSelected(ProceduralFloor floor)
        {
            if (currentFloor != floor)
            {
                if (previewMode == PreviewMode.Floors)
                {
                    if (populateApartments)
                    {
                        yield return StartCoroutine(UpdateInternalLayout(false));
                    }


                    if (currentFloor.centreLine != null)
                    {
                        currentFloor.centreLine.gameObject.SetActive(false);
                    }
                    currentFloor.gameObject.SetActive(true);
                    currentFloor.ToggleChildren(false);

                    if (floor.centreLine != null)
                    {
                        floor.centreLine.gameObject.SetActive(true);
                    }
                    floor.ToggleChildren(true);

                    if (populateApartments)
                    {
                        yield return StartCoroutine(UpdateInternalLayout(true));
                    }
                }
                currentFloor = floor;

                buildingManager.SetSelectedFloor(this, floors.IndexOf(currentFloor));

                //StartCoroutine(buildingManager.SetSelectedFloor(this, floors.IndexOf(currentFloor)));

            }
        }

        /// <summary>
        /// It toggles the facade of the building
        /// </summary>
        /// <param name="show">Whether the facade of the building should show</param>
        public void ShowFacade(bool show)
        {
            for (int i = 0; i < floors.Count; i++)
            {
                floors[i].exteriorPolygon.gameObject.SetActive(show);
            }
            if (apartmentsInteriors != null)
            {
                for (int i = 0; i < apartmentsInteriors.Count; i++)
                {
                    apartmentsInteriors[i].transform.GetChild(4).gameObject.SetActive(show);
                }
            }
        }

        /// <summary>
        /// Returns the total number of levels for this building
        /// </summary>
        /// <returns>INT</returns>
        public int GetTotalNumberOfLevels()
        {
            return buildingElement.buildingData.heightPerLevel.Count;
        }

        /// <summary>
        /// Returns the minimum floor to floor height for this building
        /// </summary>
        /// <returns>FLOAT</returns>
        public float GetMinimumFloorToFloor()
        {
            float f2fMin = float.MaxValue;
            for (int i = 1; i < buildingElement.buildingData.floorHeights.Count; i++)
            {
                if (buildingElement.buildingData.floorHeights[i] < f2fMin)
                {
                    f2fMin = buildingElement.buildingData.floorHeights[i];
                }
            }
            return f2fMin;
        }

        /// <summary>
        /// Request an overall update from the procedural building manager
        /// </summary>
        public void RequestOverallUpdate()
        {
            StartCoroutine(buildingManager.OverallUpdate());
        }

        /// <summary>
        /// Return Raw Data fro each floor as a list of string lines
        /// </summary>
        /// <returns>List of strings</returns>
        public List<string> GetPerFloorRawData()
        {
            List<string> lines = new List<string>();

            GEA = 0;
            GIA = 0;
            NIA = 0;
            GrossToNet = 0;
            ApartmentsArea = 0;
            FacadeArea = 0;
            FacadePerimeter = 0;
            WallToFloor = 0;
            NumberOfPeople = 0;
            Levels = 0;

            int[] aptTypes = new int[Standards.TaggedObject.ApartmentTypesMinimumSizes.Keys.Count];

            for (int i = 0; i < floors.Count; i++)
            {
                GEA += floors[i].GEA * floors[i].levels.Length;
                GIA += floors[i].GIA * floors[i].levels.Length;
                NIA += floors[i].NIA * floors[i].levels.Length;
                ApartmentsArea += floors[i].apartmentsArea * floors[i].levels.Length;
                FacadeArea += floors[i].facadeArea * floors[i].levels.Length;
                FacadePerimeter += floors[i].facadePerimeter * floors[i].levels.Length;
                NumberOfPeople += floors[i].numberOfPeople * floors[i].levels.Length;
                var aptNums = floors[i].apartmentNumbers;
                for (int j = 0; j < aptNums.Count; j++)
                {
                    aptTypes[j] += aptNums[j] * floors[i].levels.Length;
                }
            }

            GrossToNet = NIA / GIA;
            WallToFloor = (FacadeArea / GEA) * 100;
            Levels = floors[floors.Count - 1].levels[floors[floors.Count - 1].levels.Length - 1] + 1;

            var aptTyp = aptTypes.Select(x => x.ToString()).ToArray();

            lines.Add(index + "," + buildingName + ",," + string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13}", Math.Round(GEA), Math.Round(GIA), Math.Round(NIA), Math.Round(GrossToNet * 100, 2), Math.Round(ApartmentsArea), Math.Round(FacadeArea), Math.Round(FacadePerimeter), Math.Round(WallToFloor, 2), NumberOfPeople, "", Levels, "", "", string.Join(",", aptTyp)));


            for (int i = 0; i < floors.Count; i++)
            {
                for (int j = 0; j < floors[i].levels.Length; j++)
                {
                    lines.Add(index + "," + buildingName + "," + floors[i].key + "," + floors[i].GetFloorStats(j));
                }
            }
            return lines;
        }

        /// <summary>
        /// Populates the interior of the apartments of all floors for exporting them
        /// </summary>
        /// <returns>IEnumerator</returns>
        public IEnumerator PopulateForExport()
        {
            for (int i = 0; i < floors.Count; i++)
            {
                floors[i].PopulateApartments(true, repRootParent, true);
            }
            yield return waitFrame;
            apartmentsInteriors = new List<GameObject>();
            for (int i = 0; i < floors.Count; i++)
            {
                int floorNum = floors[i].levels.Length;
                List<GameObject> objs = new List<GameObject>();
                for (int j = 0; j < floorNum; j++)
                {
                    if (j > 0)
                    {
                        objs.Add(Instantiate(repRootParent.GetChild(i).gameObject));
                        objs[j].transform.position += new Vector3(0, floors[i].floor2floor * j, 0);
                    }
                    else
                    {
                        objs.Add(repRootParent.GetChild(i).gameObject);
                    }
                }

                for (int j = 0; j < objs.Count; j++)
                {
                    if (j > 0)
                    {
                        objs[j].transform.SetParent(repRootParent);
                    }
                    for (int k = 0; k < objs[j].transform.childCount; k++)
                    {
                        apartmentsInteriors.Add(objs[j].transform.GetChild(k).gameObject);
                    }
                }
            }
        }

        /// <summary>
        /// Returns the maximum module width for this building
        /// </summary>
        /// <returns>Float</returns>
        public float GetMaximumModuleWidth()
        {
            float maxWidth = float.MinValue;

            for (int i = 1; i < floors.Count; i++)
            {
                float width = floors[i].GetMaximumModuleWidth();
                if (width > maxWidth)
                {
                    maxWidth = width;
                }
            }

            return maxWidth;
        }

        /// <summary>
        /// Returns the maximum line-load span in the building
        /// </summary>
        /// <returns>Float</returns>
        public float GetMaximumLineLoadSpan()
        {
            float maxSpan = float.MinValue;

            for (int i = 1; i < floors.Count; i++)
            {
                float width = floors[i].GetMaximumLineLoadSpan();
                if (width > maxSpan)
                {
                    maxSpan = width;
                }
            }

            return maxSpan;
        }

        /// <summary>
        /// Returns the mximum panel width for this building
        /// </summary>
        /// <returns>Float</returns>
        public float GetMaximumPanelLength()
        {
            float maxWidth = float.MinValue;

            for (int i = 0; i < floors.Count; i++)
            {
                float width = floors[i].GetMaximumPanelWidth();
                if (width > maxWidth)
                {
                    maxWidth = width;
                }
            }

            return maxWidth;
        }

        /// <summary>
        /// Updates the interior shown for each apartment in this building
        /// </summary>
        /// <param name="show">Whether the interiors should be shown or not</param>
        /// <returns>IEnumerator</returns>
        public IEnumerator UpdateInternalLayout(bool show)
        {
            if (repRootParent.childCount > 0)
            {
                for (int i = 0; i < repRootParent.childCount; i++)
                {
                    Destroy(repRootParent.GetChild(i).gameObject);
                }
            }

            if (show)
            {
                for (int i = 0; i < floors.Count; i++)
                {
                    floors[i].PopulateApartments(show, repRootParent, previewMode == PreviewMode.Floors);
                    floors[i].ShowPlatforms(false);
                }

                yield return waitFrame;                
                switch (previewMode)
                {
                    case PreviewMode.Floors:
                        apartmentsInteriors = new List<GameObject>();
                        for (int i = 0; i < repRootParent.childCount; i++)
                        {
                            for (int j = 0; j < repRootParent.GetChild(i).childCount; j++)
                            {
                                apartmentsInteriors.Add(repRootParent.GetChild(i).GetChild(j).gameObject);
                            }
                        }
                        break;
                    case PreviewMode.Buildings:
                        apartmentsInteriors = new List<GameObject>();
                        for (int i = 0; i < floors.Count; i++)
                        {
                            if (repRootParent.childCount < floors.Count)
                                break;
                            int floorNum = floors[i].levels.Length;
                            List<GameObject> objs = new List<GameObject>();
                            for (int j = 0; j < floorNum; j++)
                            {
                                if (j > 0 )
                                {
                                    try
                                    {
                                        objs.Add(Instantiate(repRootParent.GetChild(i).gameObject));
                                        objs[j].transform.position += new Vector3(0, floors[i].floor2floor * j, 0);
                                    }
                                    catch (Exception e)
                                    {                                        
                                        Debug.Log(e);
                                    }
                                }
                                else
                                {
                                    try
                                    {
                                        objs.Add(repRootParent.GetChild(i).gameObject);
                                    }
                                    catch (Exception e)
                                    {
                                        Debug.Log(repRootParent.name + ":  " + repRootParent.childCount);
                                        Debug.Log(e);

                                    }
                                    
                                }
                            }

                            for (int j = 0; j < objs.Count; j++)
                            {
                                if (j > 0)
                                {
                                    objs[j].transform.SetParent(repRootParent);
                                }
                                for (int k = 0; k < objs[j].transform.childCount; k++)
                                {
                                    apartmentsInteriors.Add(objs[j].transform.GetChild(k).gameObject);
                                }
                            }
                        }
                        break;
                    case PreviewMode.Apartments:
                        break;
                }
            }
            else
            {
                for (int i = 0; i < repRootParent.childCount; i++)
                {
                    DestroyImmediate(repRootParent.GetChild(i).gameObject);
                }

                if (previewMode == PreviewMode.Floors)
                {
                    if (currentFloor.hasGeometry)
                    {
                        currentFloor.PopulateApartments(show);
                        currentFloor.ShowPlatforms(false);
                    }
                }
                else if (previewMode == PreviewMode.Buildings)
                {
                    for (int i = 0; i < floors.Count; i++)
                    {
                        if (floors[i].hasGeometry)
                        {
                            floors[i].PopulateApartments(show);
                            floors[i].ShowPlatforms(false);
                        }
                    }
                    //foreach (var item in floors)
                    //{
                    //    if (item.hasGeometry)
                    //    {
                    //        item.PopulateApartments(show);
                    //    }
                    //}
                }
                yield return waitFrame;
            }
        }

        /// <summary>
        /// Sets the displayed manufacturing system
        /// </summary>
        /// <param name="manufacturingSystem">The selected manufacturing system</param>
        public IEnumerator ToggleManufacturingSystem(ManufacturingSystem manufacturingSystem)
        {
            this.manufacturingSystem = manufacturingSystem;
            switch (previewMode)
            {
                case PreviewMode.Buildings:
                    if (populateApartments)
                    {
                        for (int i = 0; i < floors.Count; i++)
                        {
                            floors[i].SetManufacturingSystem(manufacturingSystem);
                            
                        }

                        //foreach (var item in floors)
                        //{
                        //    item.SetManufacturingSystem(manufacturingSystem);
                        //}
                        switch (manufacturingSystem)
                        {
                            case ManufacturingSystem.VolumetricModules:
                                for (int i = 0; i < apartmentsInteriors.Count; i++)
                                {
                                    var procedural = apartmentsInteriors[i].transform;
                                    for (int k = 1; k <= 5; k++)
                                    {
                                        if (k == 1)
                                        {
                                            procedural.GetChild(k).gameObject.SetActive(true);
                                        }
                                        else
                                        {
                                            procedural.GetChild(k).gameObject.SetActive(false);
                                        }
                                    }
                                }
                                for (int i = 0; i < floors.Count; i++)
                                {
                                    floors[i].SetTransparentApartmentMaterial(false);
                                    floors[i].platformsParent.gameObject.SetActive(false);
                                    if (floors[i].corridorsCopies!=null)
                                    {
                                        floors[i].corridorsCopies.SetActive(false);                                      
                                    }
                                    
                                   
                                }
                                
                                if (buildingManager.sitePolygon!=null)
                                    buildingManager.sitePolygon.GetComponent<MeshRenderer>().receiveShadows = false;
                                break;
                            case ManufacturingSystem.Panels:
                                for (int i = 0; i < apartmentsInteriors.Count; i++)
                                {
                                    var procedural = apartmentsInteriors[i].transform;
                                    for (int k = 1; k <= 5; k++)
                                    {
                                        if (k == 2)
                                        {
                                            procedural.GetChild(k).gameObject.SetActive(true);
                                        }
                                        else
                                        {
                                            procedural.GetChild(k).gameObject.SetActive(false);
                                        }
                                    }
                                }
                                for (int i = 0; i < floors.Count; i++)
                                {
                                    //floors[i].SetTransparentApartmentMaterial(true);
                                    floors[i].platformsParent.gameObject.SetActive(false);
                                }
                                if (buildingManager.sitePolygon != null)
                                    buildingManager.sitePolygon.GetComponent<MeshRenderer>().receiveShadows = true;
                                break;
                            case ManufacturingSystem.LineLoads:
                                for (int i = 0; i < apartmentsInteriors.Count; i++)
                                {
                                    var procedural = apartmentsInteriors[i].transform;
                                    for (int k = 1; k <= 5; k++)
                                    {
                                        if (k == 3)
                                        {
                                            procedural.GetChild(k).gameObject.SetActive(true);
                                        }
                                        else
                                        {
                                            procedural.GetChild(k).gameObject.SetActive(false);
                                        }
                                    }
                                }
                                for (int i = 0; i < floors.Count; i++)
                                {
                                    floors[i].SetTransparentApartmentMaterial(true);
                                    floors[i].platformsParent.gameObject.SetActive(false);
                                }
                                if (buildingManager.sitePolygon != null)
                                    buildingManager.sitePolygon.GetComponent<MeshRenderer>().receiveShadows = true;
                                break;
                            case ManufacturingSystem.Platforms:
                                for (int i = 0; i < floors.Count; i++)
                                {
                                    floors[i].SetTransparentApartmentMaterial(true);
                                    if(floors[i].platformsParent==null){// in case on loaded building this is null

                                        floors[i].platformsParent = new GameObject("Platforms_Parent").transform;
                                        floors[i].platformsParent.localPosition = Vector3.zero;
                                        floors[i].platformsParent.localEulerAngles = Vector3.zero;
                                        floors[i].platformsParent.SetParent(floors[i].transform);
                                        var platFormsLeftSpan = floors[i].PlatformsLeftSpan;
                                        var platFormsRightSpan = floors[i].PlatformsRightSpan;
                                        
                                    }
                                    floors[i].platformsParent.gameObject.SetActive(true);

                                }
                                yield return waitFrame;
                                for (int i = 0; i < apartmentsInteriors.Count; i++)
                                {
                                    var procedural = apartmentsInteriors[i].transform;
                                    for (int k = 1; k <= 5; k++)
                                    {
                                        if (k == 5 || k == 4)
                                        {
                                            procedural.GetChild(k).gameObject.SetActive(true);
                                        }
                                        else
                                        {
                                            procedural.GetChild(k).gameObject.SetActive(false);
                                        }
                                    }
                                }
                                if (buildingManager.sitePolygon != null)
                                    buildingManager.sitePolygon.GetComponent<MeshRenderer>().receiveShadows = true;
                                break;
                            case ManufacturingSystem.Off:
                                for (int i = 0; i < apartmentsInteriors.Count; i++)
                                {
                                    var procedural = apartmentsInteriors[i].transform;
                                    for (int k = 1; k <= 5; k++)
                                    {
                                        if (k == 5)
                                        {
                                            procedural.GetChild(k).gameObject.SetActive(true);
                                        }
                                        else
                                        {
                                            procedural.GetChild(k).gameObject.SetActive(false);
                                        }
                                    }
                                }
                                for (int i = 0; i < floors.Count; i++)
                                {
                                    floors[i].SetTransparentApartmentMaterial(false);
                                    floors[i].platformsParent.gameObject.SetActive(false);
                                }
                                if (buildingManager.sitePolygon != null)
                                    buildingManager.sitePolygon.GetComponent<MeshRenderer>().receiveShadows = true;
                                break;
                        }
                    }
                    break;
                case PreviewMode.Floors:
                    if (populateApartments)
                    {
                        for (int i = 0; i < floors.Count; i++)
                        {
                            floors[i].SetManufacturingSystem(manufacturingSystem);
                        }
                        //foreach (var item in floors)
                        //{
                        //    item.SetManufacturingSystem(manufacturingSystem);
                        //}
                    }
                    break;
            }
            yield return waitFrame;
        }

        /// <summary>
        /// It gets the numbers for the platforms system
        /// </summary>
        /// <returns>Dictionary of String and Integer</returns>
        public Dictionary<string, int> GetPlatformsNumbers()
        {
            Dictionary<string, int> platNumbers = new Dictionary<string, int>();
            for (int i = 0; i < floors.Count; i++)
            {
                if (floors[i].bays != null)
                {
                    for (int j = 0; j < floors[i].bays.Count; j++)
                    {
                        var bay = (float)Math.Round(floors[i].bays[j], 2);
                        if (platNumbers.ContainsKey(bay.ToString()))
                        {
                            platNumbers[bay.ToString()]++;
                        }
                        else
                        {
                            platNumbers.Add(bay.ToString(), 1);
                        }
                    }
                }
            }
            return platNumbers;
        }

        /// <summary>
        /// Returns the numbers for all different modules
        /// </summary>
        /// <returns>String - Float Dictionary</returns>
        public Dictionary<string, float> GetVerticalModulesNumber()
        {
            if (populateApartments)
            {
                Dictionary<string, float> modulesSizes = new Dictionary<string, float>();
                for (int i = 0; i < apartmentsInteriors.Count; i++)
                {
                    for (int j = 0; j < apartmentsInteriors[i].transform.GetChild(1).childCount; j++)
                    {
                        var obj = apartmentsInteriors[i].transform.GetChild(1).GetChild(j);
                        if (modulesSizes.ContainsKey(obj.gameObject.name))
                        {
                            modulesSizes[obj.gameObject.name] += 1;//(obj.transform.localScale.x * obj.transform.localScale.z);
                        }
                        else
                        {
                            modulesSizes.Add(obj.gameObject.name, 1/*(obj.transform.localScale.x * obj.transform.localScale.z)*/);
                        }
                    }
                }
                return modulesSizes;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Returns the numbers for all different panels
        /// </summary>
        /// <returns>String - Int Dictionary</returns>
        public Dictionary<string, int> GetPanelsNumbers()
        {
            if (populateApartments)
            {
                Dictionary<string, int> panelsSizes = new Dictionary<string, int>();
                for (int i = 0; i < apartmentsInteriors.Count; i++)
                {
                    for (int j = 0; j < apartmentsInteriors[i].transform.GetChild(2).childCount; j++)
                    {
                        var obj = apartmentsInteriors[i].transform.GetChild(2).GetChild(j);
                        if (panelsSizes.ContainsKey(obj.gameObject.name))
                        {
                            panelsSizes[obj.gameObject.name]++;
                        }
                        else
                        {
                            panelsSizes.Add(obj.gameObject.name, 1);
                        }
                    }
                }
                return panelsSizes;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Returns the sizes for all different Modules
        /// </summary>
        /// <returns>String - Int Dictionary</returns>
        public Dictionary<string, int> GetVerticalModulesSizes()
        {
            if (populateApartments)
            {
                Dictionary<string, int> modulesSizes = new Dictionary<string, int>();
                for (int i = 0; i < apartmentsInteriors.Count; i++)
                {
                    for (int j = 0; j < apartmentsInteriors[i].transform.GetChild(1).childCount; j++)
                    {
                        var obj = apartmentsInteriors[i].transform.GetChild(1).GetChild(j);
                        var m_name = string.Format("{0}/{1}/{2}", obj.gameObject.name, obj.localScale.x, obj.localScale.z);
                        if (modulesSizes.ContainsKey(m_name))
                        {
                            modulesSizes[m_name]++;//Add( new float[] { obj.localScale.x, obj.localScale.z });
                        }
                        else
                        {
                            modulesSizes.Add(m_name, 1);//new List<float[]>() { new float[] { obj.localScale.x, obj.localScale.z } });
                        }
                    }
                }
                return modulesSizes;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// It gets the sizes of the platforms system
        /// </summary>
        /// <returns>Dictionary of String and Integer</returns>
        public Dictionary<string, int> GetPlatformsSizes()
        {
            Dictionary<string, int> platSizes = new Dictionary<string, int>();

            for (int i = 0; i < floors.Count; i++)
            {
                if (floors[i].bays != null)
                {
                    for (int j = 0; j < floors[i].bays.Count; j++)
                    {
                        try
                        {
                            var bay = (float)Math.Round(floors[i].bays[j], 2);
                            var frame = floors[i].platformsParent.GetChild(j).GetComponent<PlatformsFrame>();
                            var m_name = string.Format("{0}/{1}/{2}", "PlatformsFrame", bay, frame.width);
                            if (platSizes.ContainsKey(m_name))
                            {
                                platSizes[m_name]++;
                            }
                            else
                            {
                                platSizes.Add(m_name, 1);
                            }
                        }
                        catch(Exception e)
                        {
                            Debug.Log(e);
                        }
                    }
                }
            }

            return platSizes;
        }

        /// <summary>
        /// Returns the sizes for all different facade panels
        /// </summary>
        /// <returns>String - Int Dictionary</returns>
        public Dictionary<string, int> GetFacadePanelsSizes()
        {
            if (populateApartments)
            {
                Dictionary<string, int> panelsSizes = new Dictionary<string, int>();
                for (int i = 0; i < apartmentsInteriors.Count; i++)
                {
                    for (int j = 0; j < apartmentsInteriors[i].transform.GetChild(2).childCount; j++)
                    {
                        var obj = apartmentsInteriors[i].transform.GetChild(2).GetChild(j);
                        var m_name = string.Format("{0}/{1}/{2}", obj.gameObject.name, obj.localScale.x, obj.localScale.y);
                        if (panelsSizes.ContainsKey(m_name))
                        {
                            panelsSizes[m_name]++;//Add( new float[] { obj.localScale.x, obj.localScale.z });
                        }
                        else
                        {
                            panelsSizes.Add(m_name, 1);//new List<float[]>() { new float[] { obj.localScale.x, obj.localScale.z } });
                        }
                    }
                }
                return panelsSizes;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Returns the sizes for the volumetric modules
        /// </summary>
        /// <returns>String - List of float Dictionary</returns>
        public Dictionary<string, List<float>> GetVolumetricSizes()
        {
            if (populateApartments)
            {
                Dictionary<string, List<float>> panelsSizes = new Dictionary<string, List<float>>();
                for (int i = 0; i < apartmentsInteriors.Count; i++)
                {
                    for (int j = 0; j < apartmentsInteriors[i].transform.GetChild(1).childCount; j++)
                    {
                        var obj = apartmentsInteriors[i].transform.GetChild(1).GetChild(j);
                        if (panelsSizes.ContainsKey(obj.gameObject.name))
                        {
                            panelsSizes[obj.gameObject.name].Add((float)Math.Round(obj.localScale.x, 2));
                        }
                        else
                        {
                            panelsSizes.Add(obj.gameObject.name, new List<float> { (float)Math.Round(obj.localScale.x, 2) });
                        }
                    }
                }
                return panelsSizes;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Returns the maximum apartment depth for this building
        /// </summary>
        /// <returns>FLOAT</returns>
        public float GetMaxDepth()
        {
            float maxDepth = float.MinValue;
            for (int i = 0; i < floors.Count; i++)
            {
                if (floors[i].hasGeometry)
                {
                    var depth = floors[i].MaxAptDepth;
                    if (maxDepth < depth)
                    {
                        maxDepth = depth;
                    }
                }
            }
            return maxDepth;
        }

        /// <summary>
        /// Returns the sizes for the facade panels
        /// </summary>
        /// <returns>String - List of float Dictionary</returns>
        public Dictionary<string, List<float>> GetPanelsSizes()
        {
            if (populateApartments)
            {
                Dictionary<string, List<float>> panelsSizes = new Dictionary<string, List<float>>();
                for (int i = 0; i < apartmentsInteriors.Count; i++)
                {
                    for (int j = 0; j < apartmentsInteriors[i].transform.GetChild(2).childCount; j++)
                    {
                        var obj = apartmentsInteriors[i].transform.GetChild(2).GetChild(j);
                        if (panelsSizes.ContainsKey(obj.gameObject.name))
                        {
                            panelsSizes[obj.gameObject.name].Add((float)Math.Round(obj.localScale.x, 2));
                        }
                        else
                        {
                            panelsSizes.Add(obj.gameObject.name, new List<float> { (float)Math.Round(obj.localScale.x, 2) });
                        }
                    }
                }
                return panelsSizes;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Returns the number of apartments for this building
        /// </summary>
        /// <returns>String - Float Dictionary</returns>
        public Dictionary<string, float> GetApartmentNumbers(bool perLevel = false)
        {
            Dictionary<string, float> areas = new Dictionary<string, float>();

            for (int i = 0; i < floors.Count; i++)
            {
                var floor = floors[i];
                if (floor != null && floor.apartments != null)
                {
                    var stats = floor.ApartmentStats;
                    foreach (var item in stats)
                    {
                        if (areas.ContainsKey(item.Key))
                        {
                            if (perLevel)
                            {
                                areas[item.Key] += item.Value[0];
                            }
                            else
                            {
                                areas[item.Key] += item.Value[0] * floor.levels.Length;
                            }
                        }
                        else
                        {
                            if (perLevel)
                            {
                                areas.Add(item.Key, item.Value[0]);
                            }
                            else
                            {
                                areas.Add(item.Key, item.Value[0] * floor.levels.Length);
                            }
                        }
                    }
                }
            }
            return areas;
        }

        /// <summary>
        /// Returns the areas for the apartments in this building
        /// </summary>
        /// <returns>String - Float Dictionary</returns>
        public Dictionary<string, float> GetApartmentAreas()
        {
            Dictionary<string, float> areas = new Dictionary<string, float>();

            for (int i = 0; i < floors.Count; i++)
            {
                var floor = floors[i];
                if (floor != null && floor.apartments != null)
                {
                    var stats = floor.ApartmentStats;
                    foreach (var item in stats)
                    {
                        if (areas.ContainsKey(item.Key))
                        {
                            areas[item.Key] += item.Value[1] * floor.levels.Length;
                        }
                        else
                        {
                            areas.Add(item.Key, item.Value[1] * floor.levels.Length);
                        }
                    }
                }
            }
            return areas;
        }

        /// <summary>
        /// Returns the number of apartments for the selected floor
        /// </summary>
        /// <returns>String - Float Dictionary</returns>
        public Dictionary<string, float> GetCurrentApartmentNumbers()
        {
            Dictionary<string, float> areas = new Dictionary<string, float>();

            var floor = currentFloor;
            if (floor != null && floor.apartments != null)
            {
                var stats = floor.ApartmentStats;
                foreach (var item in stats)
                {
                    if (areas.ContainsKey(item.Key))
                    {
                        areas[item.Key] += item.Value[0];// * floor.levels.Length;
                    }
                    else
                    {
                        areas.Add(item.Key, item.Value[0]);// * floor.levels.Length);
                    }
                }
            }

            return areas;
        }

        /// <summary>
        /// Returns the number of apartments for this building as string
        /// </summary>
        /// <returns>String</returns>
        public string GetApartmentNumbersText()
        {
            string info = String.Empty;

            var areas = GetApartmentNumbers();
            float totalAptNumber = 0.0f;
            foreach (var item in areas)
            {
                totalAptNumber += item.Value;
            }
            info = "Total = " + totalAptNumber + "\n";
            int north = 0;
            int east = 0;
            int south = 0;
            int west = 0;
            int dualAspect = 0;
            int m3 = 0;
            for (int i = 0; i < floors.Count; i++)
            {
                floors[i].GetApartmentAspects(out int m_north, out int m_east, out int m_south, out int m_west, out int m_dualAspect, out int m_m3);
                north += m_north;
                east += m_east;
                south += m_south;
                west += m_west;
                dualAspect += m_dualAspect;
                m3 += m_m3;
            }
            info += string.Format("North Facing Apts =  {0} ({6}%)\r\n" +
                    "East Facing Apts =  {1} ({7}%)\r\n" +
                    "South Facing Apts =  {2} ({8}%)\r\n" +
                    "West Facing Apts =  {3} ({9}%)\r\n" +
                    "Dual Aspect Apts =  {4} ({10}%)\r\n" +
                    "M3 Apts = {5} ({11}%)\r\n",
                    north, east, south, west, dualAspect, m3,
                    Mathf.Round(100 * (north / (float)totalAptNumber)),
                    Mathf.Round(100 * (east / (float)totalAptNumber)),
                    Mathf.Round(100 * (south / (float)totalAptNumber)),
                    Mathf.Round(100 * (west / (float)totalAptNumber)),
                    Mathf.Round(100 * (dualAspect / (float)totalAptNumber)),
                    Mathf.Round(100 * (m3 / (float)totalAptNumber)));
            foreach (var item in areas)
            {
                info += string.Format("{0} = {1}, {2}%\n", item.Key, item.Value, Math.Round((item.Value / totalAptNumber) * 100, 2));
            }

            return info;
        }

        /// <summary>
        /// Returns the apartments of a specific type for this building
        /// </summary>
        /// <param name="type">The apartment type</param>
        /// <returns>List of Apartments</returns>
        public List<ApartmentUnity> GetApartments(string type)
        {
            List<ApartmentUnity> apartments = new List<ApartmentUnity>();

            for (int i = 0; i < floors.Count; i++)
            {
                var floor = floors[i];
                for (int j = 0; j < floor.apartments.Count; j++)
                {
                    if (floor.apartments[j].ApartmentType == type)
                    {
                        apartments.Add(floor.apartments[j]);
                    }
                }
            }

            return apartments;
        }

        /// <summary>
        /// Gets the number of apartments per aspect
        /// </summary>
        /// <param name="north">The number of apartments facing North</param>
        /// <param name="east">The number of apartments facing East</param>
        /// <param name="south">The number of apartments facing South</param>
        /// <param name="west">The number of apartments facing West</param>
        /// <param name="dualAspect">The number of apartments which are dual aspect</param>
        /// <param name="m3">The number of apartments which are M3 compliant</param>
        public void GetApartmentAspects(out int north, out int east, out int south, out int west, out int dualAspect, out int m3)
        {
            north = 0;
            east = 0;
            south = 0;
            west = 0;
            dualAspect = 0;
            m3 = 0;
            for (int i = 0; i < floors.Count; i++)
            {
                int m_northApts = 0;
                int m_eastApts = 0;
                int m_southApts = 0;
                int m_westApts = 0;
                int m_dualAspect = 0;
                int m_3 = 0;

                floors[i].GetApartmentAspects(out m_northApts, out m_eastApts, out m_southApts, out m_westApts, out m_dualAspect, out m_3);

                north += m_northApts;
                east += m_eastApts;
                south += m_southApts;
                west += m_westApts;
                dualAspect += m_dualAspect;
                m3 += m_3;
            }
        }

        /// <summary>
        /// Returns the statistics for this building
        /// </summary>
        /// <param name="GEA">Gross External Area</param>
        /// <param name="GIA">Gross Internal Area</param>
        /// <param name="NIA">NET Internal Area</param>
        /// <param name="totalHeight">The total height</param>
        /// <param name="unitDensity">The density as units per hectare</param>
        /// <param name="habDensity">The density as habitable rooms per hectare</param>
        /// <param name="plantRoomArea">The plant-room area</param>
        /// <param name="aptAreas">The areas of the apartments</param>
        /// <param name="aptNumbers">The numbers of the apartments</param>
        /// <param name="commercialArea">The area of the commercial use</param>
        /// <param name="facadeArea">The area of the facade</param>
        /// <param name="groundFloorArea">The total area of the ground floor</param>
        /// <param name="otherArea">The area of the ground floor which is not specifically assigned a use</param>
        /// <param name="receptionArea">The area of the ground floor which is assigned for reception</param>
        public void GetCurrentBuildingStats(out float GEA, out float GIA, out float NIA, out float totalHeight, out float unitDensity, out float habDensity, out float plantRoomArea, out Dictionary<string, float> aptAreas, out Dictionary<string, float> aptNumbers, out float groundFloorArea, out float receptionArea, out float commercialArea, out float otherArea, out float facadeArea)
        {
            GEA = 0.0f;
            GIA = 0.0f;
            NIA = 0.0f;
            totalHeight = 0.0f;
            unitDensity = 0.0f;
            habDensity = 0.0f;
            groundFloorArea = 0f;
            aptAreas = new Dictionary<string, float>();
            aptNumbers = new Dictionary<string, float>();
            plantRoomArea = 0;
            receptionArea = 0;
            commercialArea = 0;
            otherArea = 0;
            facadeArea = 0;

            if (floors != null && floors.Count > 0)
            {

                groundFloorArea = floors[0].GEA;
                var site = buildingManager.sitePolygon;
                if (site != null)
                {
                    for (int i = 0; i < floors.Count; i++)
                    {
                        if (floors[i].apartments != null)
                        {
                            unitDensity += (floors[i].apartments.Count * floors[i].levels.Length);
                            habDensity += (floors[i].habitRoomsCount * floors[i].levels.Length);
                        }
                    }
                }

                totalHeight = 0;
                for (int i = 0; i < floors.Count; i++)
                {
                    try
                    {
                        var floor = floors[i];
                        GIA += floor.GIA * floor.levels.Length;
                        GEA += floor.GEA * floor.levels.Length;
                        NIA += floor.NIA * floor.levels.Length;
                        facadeArea += floors[i].facadeArea * floors[i].levels.Length;
                        if (i == 0)
                        {
                            if (floors[i].percentages.ContainsKey("Commercial"))
                                commercialArea = floors[i].percentages["Commercial"] / 100f * (floors[0].GIA - receptionArea);
                            else if (floors[i].percentages.ContainsKey("Other"))
                                otherArea = floors[i].percentages["Other"] / 100f * (floors[0].GIA - receptionArea);
                        }
                        else
                        {
                            if (floors[i].percentages.ContainsKey("Commercial"))
                                commercialArea = floors[i].percentages["Commercial"] / 100f * (floors[i].GIA);
                            else if (floors[i].percentages.ContainsKey("Other"))
                                otherArea = floors[i].percentages["Other"] / 100f * (floors[i].GIA);
                        }
                        totalHeight += floor.levels.Length * floor.floor2floor;
                        var stats = floor.ApartmentStats;

                        foreach (var item in stats)
                        {
                            if (aptAreas.ContainsKey(item.Key))
                            {
                                aptAreas[item.Key] += item.Value[1] * floor.levels.Length;
                            }
                            else
                            {
                                aptAreas.Add(item.Key, item.Value[1] * floor.levels.Length);
                            }

                            if (aptNumbers.ContainsKey(item.Key))
                            {
                                aptNumbers[item.Key] += item.Value[0] * floor.levels.Length;
                            }
                            else
                            {
                                aptNumbers.Add(item.Key, item.Value[0] * floor.levels.Length);
                            }
                        }
                }
                    catch (Exception e)
                {
                    Debug.Log(e);
                }
            }

                totalHeight += (floors.Count - 1) * 0.5f;
                totalHeight += Standards.TaggedObject.roofDepth + Standards.TaggedObject.parapetHeight;
                plantRoomArea = GetBuildingPlantArea();
                receptionArea = GetBuildingReceptionArea();
            }
        }

        /// <summary>
        /// Returns the statistics for the apartments in this building
        /// </summary>
        /// <returns>String - Floar Array Dictionary</returns>
        public Dictionary<string, float[]> GetApartmentStats()
        {
            Dictionary<string, float[]> stats = new Dictionary<string, float[]>();

            for (int i = 1; i < floors.Count; i++)
            {
                var m_stats = floors[i].ApartmentStats;
                foreach (var item in m_stats)
                {
                    if (stats.ContainsKey(item.Key))
                    {
                        stats[item.Key][0] += item.Value[0] * floors[i].levels.Length;
                        stats[item.Key][1] += item.Value[1] * floors[i].levels.Length;
                    }
                    else
                    {
                        stats.Add(item.Key, new float[] { item.Value[0] * floors[i].levels.Length, item.Value[1] * floors[i].levels.Length });
                    }
                }
            }

            return stats;
        }

        /// <summary>
        /// Returns the statistics for this building
        /// </summary>
        /// <param name="NIA">Net Internal Area</param>
        /// <param name="GIA">Gross Internal Area</param>
        /// <returns>String Array</returns>
        public string[] GetCurrentBuildingStats(out float NIA, out float GIA)
        {
            string[] info = new string[2];
            GIA = 0.0f;
            NIA = 0.0f;
            if (floors != null && floors.Count > 0)
            {
                info[0] = string.Format("Summary - {0}", buildingName);
                var site = buildingManager.sitePolygon;
                float unitsPerHectare = -1;
                float habRoomsPerHectare = -1;
                if (site != null)
                {
                    for (int i = 0; i < floors.Count; i++)
                    {
                        if (floors[i].apartments != null)
                        {
                            unitsPerHectare += (floors[i].apartments.Count * floors[i].levels.Length);
                            habRoomsPerHectare += (floors[i].habitRoomsCount * floors[i].levels.Length);
                        }
                    }
                    unitsPerHectare /= (site.Area / 10000.0f);
                    habRoomsPerHectare /= (site.Area / 10000.0f);
                }
                GEA = 0.0f;

                int numberOfLevels = 0;
                float totalHeight = 0;
                float facadeArea = 0;
                for (int i = 0; i < floors.Count; i++)
                {
                    try
                    {
                        var floor = floors[i];
                        GIA += floor.GIA * floor.levels.Length;
                        GEA += floor.GEA * floor.levels.Length;
                        NIA += floor.NIA * floor.levels.Length;
                        numberOfLevels += floor.levels.Length;
                        totalHeight += floor.levels.Length * floor.floor2floor;
                        facadeArea += floor.facadeArea * floor.levels.Length;
                    }
                    catch (Exception e)
                    {
                        Debug.Log(e);
                    }
                }

                totalHeight += (floors.Count - 1) * 0.5f;
                float plantRoomArea = GetBuildingPlantArea();
                float receptionArea = GetBuildingReceptionArea();
                float otherArea = GetOtherArea(receptionArea);
                float commercialArea = GetCommercialArea(receptionArea);

                info[1] = string.Format(
                    "GEA = {0}m\xB2 / {13}ft\xB2\n" +
                    "GIA = {1}m\xB2 / {14}ft\xB2\n" +
                    "NIA = {2}m\xB2 / {15}ft\xB2\n" +
                    "Net to gross = {8}%\n" +
                    "GIA to GEA = {9}%\n" +
                    "Plant-Room Area = {4}m\xB2 / {16}ft\xB2\n" +
                    "Entrance = {11}m\xB2 / {17}ft\xB2\n" +
                    "Other = {12}m\xB2 / {18}ft\xB2\n" +
                    "Commercial = {19}m\xB2 / {20}ft\xB2\n" +
                    "Number of levels = {3}\n" +
                    "Building height = {5}m\n" +
                    "Wall to floor = {21}",
                    String.Format(numbersFormat, Mathf.Round(GEA)),
                    String.Format(numbersFormat, Mathf.Round(GIA)),
                    String.Format(numbersFormat, Mathf.Round(NIA)),
                    numberOfLevels,
                    String.Format(numbersFormat, Math.Round(plantRoomArea)),
                    totalHeight,
                    (unitsPerHectare != -1) ? Math.Round(unitsPerHectare).ToString() : "N/A",
                    (habRoomsPerHectare != -1) ? Math.Round(habRoomsPerHectare).ToString() : "N/A",
                    Math.Round((Mathf.Round(NIA) / Mathf.Round(GIA)) * 100, 2),
                    Math.Round((Mathf.Round(GIA) / Mathf.Round(GEA)) * 100, 2),
                    (site != null) ? Math.Round((floors[0].GEA / site.Area) * 100, 2).ToString() : "N/A",
                    String.Format(numbersFormat, Math.Round(receptionArea)),
                    String.Format(numbersFormat, Math.Round(otherArea)),
                    String.Format(numbersFormat, Mathf.Round(GEA * ProceduralBuildingManager.SqMeterToSqFoot)),
                    String.Format(numbersFormat, Mathf.Round(GIA * ProceduralBuildingManager.SqMeterToSqFoot)),
                    String.Format(numbersFormat, Mathf.Round(NIA * ProceduralBuildingManager.SqMeterToSqFoot)),
                    String.Format(numbersFormat, Math.Round(plantRoomArea * ProceduralBuildingManager.SqMeterToSqFoot)),
                    String.Format(numbersFormat, Math.Round(receptionArea * ProceduralBuildingManager.SqMeterToSqFoot)),
                    String.Format(numbersFormat, Math.Round((otherArea) * ProceduralBuildingManager.SqMeterToSqFoot)),
                    String.Format(numbersFormat, Math.Round((commercialArea))),
                    String.Format(numbersFormat, Math.Round((commercialArea) * ProceduralBuildingManager.SqMeterToSqFoot)),
                    Math.Round(facadeArea / GEA, 2)
                    );

                info[1] += "\n";
                info[1] += GetApartmentNumbersText();
                info[1] += "\n";
                info[1] += GetBriefDifference();
            }
            return info;
        }

        /// <summary>
        /// Gets the values for the system analysis
        /// </summary>
        /// <param name="minF2f">The minimum floor to floor height in the building</param>
        /// <param name="maxF2f">The maximum floor to floor height in the building</param>
        /// <param name="maxModule">The maximum Module Width / Panel Length in the building</param>
        /// <param name="maxSpan">The maximum line-load span in the building</param>
        /// <param name="storiesNum">The number of stories in the building</param>
        public void GetSystemNumbers(ref float minF2f, ref float maxF2f, ref float maxModule, ref float maxSpan, ref int storiesNum)
        {
            storiesNum = buildingElement.buildingData.heightPerLevel.Count;

            minF2f = float.MaxValue;
            maxF2f = float.MinValue;
            for (int i = 0; i < buildingElement.buildingData.floorHeights.Count; i++)
            {
                if (buildingElement.buildingData.floorHeights[i] < minF2f)
                {
                    minF2f = buildingElement.buildingData.floorHeights[i];
                }
                if (buildingElement.buildingData.floorHeights[i] > maxF2f)
                {
                    maxF2f = buildingElement.buildingData.floorHeights[i];
                }
            }

            maxModule = float.MinValue;
            maxSpan = float.MinValue;
            for (int i = 1; i < floors.Count; i++)
            {
                float width = 0;
                float span = 0;
                floors[i].GetSystemNumbers(ref width, ref span);
                if (width > maxModule) maxModule = width;
                if (span > maxSpan) maxSpan = span;
            }


        }

        /// <summary>
        /// Returns the difference between the original brief set and the current state
        /// </summary>
        /// <returns>String</returns>
        public string GetBriefDifference()
        {
            string data = "";

            Dictionary<string, float> areas = new Dictionary<string, float>();
            Dictionary<string, float> percentages = new Dictionary<string, float>();
            float totalPercentages = 0;
            float totalArea = 0;
            try
            {
                for (int i = 1; i < floors.Count; i++)
                {
                    foreach (var item in floors[i].percentages)
                    {
                        if (percentages.ContainsKey(item.Key))
                        {
                            percentages[item.Key] += item.Value;
                        }
                        else
                        {
                            percentages.Add(item.Key, item.Value);
                        }
                        totalPercentages += item.Value;
                    }


                    var floor = floors[i];
                    if (floor != null && floor.apartments != null)
                    {
                        for (int j = 0; j < floor.apartments.Count; j++)
                        {
                            if (areas.ContainsKey(floor.apartments[j].ApartmentType))
                            {
                                areas[floor.apartments[j].ApartmentType] += floor.apartments[j].Area * floors[i].levels.Length;
                            }
                            else
                            {
                                areas.Add(floor.apartments[j].ApartmentType, floor.apartments[j].Area * floors[i].levels.Length);
                            }
                            totalArea += floor.apartments[j].Area * floors[i].levels.Length;
                        }
                    }
                }

                if (areas.Count > 0)
                {
                    foreach (var item in percentages)
                    {
                        if (item.Key != "Commercial" && item.Key != "Other")
                        {
                            var perc = percentages[item.Key];
                            perc /= totalPercentages;
                            perc *= 100;
                            perc = (float)Math.Round(perc, 1);
                            var area = areas[item.Key];
                            area /= totalArea;
                            area *= 100;
                            area = (float)Math.Round(area, 1);
                            data += string.Format("{0} = Brief {1}% vs Current {2}%\n", item.Key, Math.Round(perc, 1), Math.Round(area, 1));
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }

            return data;
        }

        /// <summary>
        /// Returns the total plant area needed for the building
        /// </summary>
        /// <returns>Float</returns>
        public float GetBuildingPlantArea()
        {
            float area = 0;
            for (int i = 0; i < floors.Count; i++)
            {
                area += (floors[i].GIA * floors[i].levels.Length) * 0.05f;
            }
            return area;
        }

        /// <summary>
        /// Returns the reception area required for the building
        /// </summary>
        /// <returns>Float</returns>
        public float GetBuildingReceptionArea()
        {
            float area = 0;
            for (int i = 0; i < floors.Count; i++)
            {
                area += (floors[i].GIA * floors[i].levels.Length) * 0.01f;
            }
            return area;
        }

        /// <summary>
        /// Returns the area which is not assigned a specific use
        /// </summary>
        /// <param name="receptionArea">The reception area of the ground floor</param>
        /// <returns>Float</returns>
        public float GetOtherArea(float receptionArea)
        {
            float area = 0;
            for (int i = 0; i < floors.Count; i++)
            {
                try
                {
                    if (floors[i].percentages.ContainsKey("Other"))
                    {
                        if (i == 0)
                        {
                            area += (floors[i].percentages["Other"] / 100f) * (floors[i].GIA - receptionArea);
                        }
                        else
                        {
                            area += (floors[i].percentages["Other"] / 100f) * floors[i].GIA;
                        }
                    }
                }
               catch (Exception e)
                {
                    Debug.Log(e);
                }
            }
            return area;
        }

        /// <summary>
        /// Returns the commercial area
        /// </summary>
        /// <param name="receptionArea">The reception area of the ground floor</param>
        /// <returns>Float</returns>
        public float GetCommercialArea(float receptionArea)
        {
            float area = 0;
            for (int i = 0; i < floors.Count; i++)
            {
                if (floors[i].percentages.ContainsKey("Commercial"))
                {
                    if (i == 0)
                    {
                        area += (floors[i].percentages["Commercial"] / 100f) * (floors[i].GIA - receptionArea);
                    }
                    else
                    {
                        area += (floors[i].percentages["Commercial"] / 100f) * floors[i].GIA;
                    }
                }
            }
            return area;
        }

        /// <summary>
        /// Offsets the centreline and creates a geometry for the floors of this building
        /// </summary>
        /// <param name="depth">The apartment depth</param>
        /// <param name="maxWidth">The maximum apartment width</param>
        /// <returns>IEnumerator</returns>
        public IEnumerator Offset(float depth, float maxWidth)
        {
            prevMinimumWidth = maxWidth;
            plantRoomArea = 0;
            //leftApartmentDepth = depth;
            //baseHeight = masterPolyline[0].currentPosition.y;
            //if (basement != null)
            //{
            //    var yVal = basement.levels.Length * basement.floor2floor * -1f;
            //    basement.yVal = yVal;
            //    basement.transform.position = new Vector3(basement.transform.position.x, yVal, basement.transform.position.z);
            //    var polygon = UseMasterPolyline(basement.id);
            //    if (basement.centreLine == null)
            //    {
            //        SetCentreLineToFloor(basement, polygon);
            //    }
            //    basement.apartmentDepth = depth;
            //    yield return StartCoroutine(basement.GenerateGeometries(prevMinimumWidth));
            //    buildingManager.apartmentTypesChart.designData = basement;
            //}

            //if (podium != null)
            //{
            //    var yVal = baseHeight;
            //    podium.yVal = yVal;
            //    podium.transform.position = new Vector3(podium.transform.position.x, yVal, podium.transform.position.z);
            //    var polygon = UseMasterPolyline(0);
            //    if (podium.centreLine == null)
            //    {
            //        SetCentreLineToFloor(podium, polygon);
            //    }
            //    podium.apartmentDepth = depth;
            //    yield return StartCoroutine(podium.GenerateGeometries(prevMinimumWidth));
            //    buildingManager.apartmentTypesChart.designData = podium;
            //}

            for (int i = 0; i < floors.Count; i++)
            {
                var item = floors[i];
                if (!item.hasGeometry)
                {
                    //if (podium == null)
                    //{
                    if (i == 0)
                    {
                        var polygon = UseMasterPolyline(item.id);
                        if (item.centreLine == null)
                        {
                            SetCentreLineToFloor(item, polygon);
                        }
                        item.SetAptDepth(depth);
                        yield return StartCoroutine(item.GenerateGeometries(prevMinimumWidth, true));
                        buildingManager.apartmentTypesChart.designData = item;
                    }
                    else
                    {
                        var yVal = buildingElement.GetYForLevel(item.levels[0], i);
                        item.yVal = yVal;
                        item.transform.position = new Vector3(item.transform.position.x, yVal, item.transform.position.z);
                        var polygon = UseMasterPolyline(item.id);
                        polygon.transform.SetParent(transform);
                        polygon.gameObject.name = "CopyMaster_" + i;
                        if (item.centreLine == null)
                        {
                            SetCentreLineToFloor(item, polygon);
                        }
                        item.SetAptDepth(depth);
                        yield return StartCoroutine(item.GenerateGeometries(floors[0], prevMinimumWidth));
                        buildingManager.apartmentTypesChart.designData = item;
                    }
                    //}
                    //else
                    //{
                    //    var yVal = buildingElement.GetYForLevel(item.levels[0], i);
                    //    item.yVal = yVal;
                    //    item.transform.position = new Vector3(item.transform.position.x, yVal, item.transform.position.z);
                    //    var polygon = UseMasterPolyline(item.id+1);
                    //    polygon.transform.SetParent(transform);
                    //    polygon.gameObject.name = "CopyMaster_" + i;
                    //    if (item.centreLine == null)
                    //    {
                    //        SetCentreLineToFloor(item, polygon);
                    //    }
                    //    item.apartmentDepth = depth;
                    //    yield return StartCoroutine(item.GenerateGeometries(podium, prevMinimumWidth));
                    //    buildingManager.apartmentTypesChart.designData = item;
                    //}
                }
                plantRoomArea += ((item.GIA * item.levels.Length) * 0.05f);
            }
            if (heightLine == null)
            {
                AddHeightLine(masterPolyline);
            }
            UpdateHeightLine(masterPolyline);
            GetCoreDistances();

            //yield return StartCoroutine(GetPodiumAndBasement(podium != null, basement != null));
            if (podium != null)
                yield return StartCoroutine(GeneratePodium());
            if (basement != null)
                yield return StartCoroutine(GenerateBasement());

            if (previewMode == PreviewMode.Buildings)
                buildingManager.SetSelectedFloor(this, floors.Count - 1);

            buildingManager.CheckIfBuildingOverPodium(this);
            buildingManager.CheckIfBuildingOverBasement(this);
            //floors[0].GeneratePlantRoom(plantRoomArea);
            buildingManager.CheckSystemConstraints(this);
            coreParameters = floors[0].coresParams.ToArray();
        }

        /// <summary>
        /// Called when the roof settings have changed
        /// </summary>
        public void OnRoofSettingsChanged()
        {
            floors[floors.Count - 1].OnRoofSettingsChanged();
            UpdateHeightLine(masterPolyline);
        }

        /// <summary>
        /// Generates the distances between the cores
        /// </summary>
        public void GetCoreDistances()
        {
            if (masterPolyline != null)
            {
                if (coreDistanceParent != null)
                {
                    DestroyImmediate(coreDistanceParent.gameObject);
                }
                coreDistanceParent = new GameObject("CoreDistanceParent").transform;
                coreDistanceParent.localPosition = Vector3.zero;
                coreDistanceParent.localEulerAngles = Vector3.zero;
                coreDistanceParent.SetParent(transform);
                float totalHeight = 0;// floors[0].floor2floor;
                for (int i = 0; i < floors.Count; i++)
                {
                    try
                    {
                        totalHeight += floors[i].levels.Length * floors[i].floor2floor;
                    }
                    catch (Exception e)
                    {
                        Debug.Log(e);
                    }
                }
                //For the transfer decks
                totalHeight += (floors.Count - 1) * 0.5f;
                //if (podium != null)
                //{
                //    totalHeight += podium.levels.Length * podium.floor2floor + 0.5f;
                //}
                for (int i = 0; i < floors[0].coresParams.Count - 1; i++)
                {
                    var obj = Instantiate(coreDistancePrefab, coreDistanceParent);
                    float yOffset = totalHeight + 2;
                    Vector3[] points = masterPolyline.PointsOnCurve(new float[] { floors[0].coresParams[i], floors[0].coresParams[i + 1] });
                    for (int j = 0; j < points.Length; j++)
                    {
                        points[j] += Vector3.up * yOffset;
                    }
                    obj.GetComponent<CoreDistance>().SetCores(points, coreLength);
                }
                coreDistanceParent.gameObject.SetActive(showCoreDistances);
            }
        }

        /// <summary>
        /// Toggles the distances between the cores
        /// </summary>
        /// <param name="show">Whether the distances should be shown</param>
        public void ShowCoreDistances(bool show)
        {
            if (coreDistanceParent != null)
            {
                showCoreDistances = show;
                coreDistanceParent.gameObject.SetActive(show);
            }
        }

        //public IEnumerator GetPodiumAndBasement(bool podium, bool basement)
        //{

        //}

        /// <summary>
        /// Called when the preview mode changes
        /// </summary>
        /// <param name="mode">The current preview mode</param>
        public void OnPreviewModeChanged(PreviewMode mode)
        {
            this.previewMode = mode;
        }

        /// <summary>
        /// Sets the master polyline for this building
        /// </summary>
        /// <param name="polygon">The polygon to be used as the master one</param>
        public void SetMasterPolyline(Polygon polygon)
        {
            transform.position = new Vector3(polygon[0].currentPosition.x, baseHeight, polygon[0].currentPosition.z);
            polygon.transform.SetParent(transform);
            polygon.gameObject.name = "MasterPolyline_" + index;
            if (masterPolyline != null)
            {
                Destroy(masterPolyline);
                if (floors[0].centreLine != null)
                {
                    Destroy(floors[0].centreLine.gameObject);
                    floors[0].centreLine = null;
                }
                for (int i = 0; i < floors.Count; i++)
                {
                    if (floors[i].centreLine)
                    {
                        Destroy(floors[i].centreLine.gameObject);
                        floors[i].centreLine = null;
                    }
                }
            }
            masterPolyline = polygon;
            polygon.updated.AddListener(UpdateHeightLine);
           
        }

        /// <summary>
        /// Updates the line display of the total height of the building
        /// </summary>
        /// <param name="poly">The polygon which triggered this update</param>
        public void UpdateHeightLine(Polygon poly)
        {
            if (heightLine != null)
            {
                offsetDistanceLine = Standards.TaggedObject.ConstructionFeatures["ExteriorWall"] * 2;
                if (buildingType == BuildingType.Linear)
                {
                    if (poly != null)
                    {
                        heightLine.gameObject.SetActive(true);
                        float totalHeight = 0;// floors[0].floor2floor;
                        for (int i = 0; i < floors.Count; i++)
                        {
                            try
                            {
                                totalHeight += floors[i].levels.Length * floors[i].floor2floor;
                            }
                            catch (Exception e)
                            {
                                Debug.Log(e);
                            }
                        }
                        //For the transfer decks
                        totalHeight += (floors.Count - 1) * 0.5f;
                        totalHeight += Standards.TaggedObject.parapetHeight + Standards.TaggedObject.roofDepth;
                        Vector3 heightDiff = Vector3.zero;
                        if (podium != null)
                        {
                            heightDiff = Vector3.up * (podium.levels.Length * podium.floor2floor + 0.5f);
                            totalHeight += podium.levels.Length * podium.floor2floor + 0.5f;
                        }
                        Vector3 start = (poly[0].currentPosition + (poly[0].currentPosition - poly[1].currentPosition).normalized * offsetDistanceLine) - heightDiff;
                        Vector3 end = start + Vector3.up * totalHeight;
                        UpdateDistance(start, end, heightLine);
                    }
                    else
                    {
                        heightLine.gameObject.SetActive(false);
                    }
                }
                else if (buildingType == BuildingType.Mansion)
                {
                    heightLine.gameObject.SetActive(true);
                    float totalHeight = 0;//floors[0].floor2floor;
                    for (int i = 0; i < floors.Count; i++)
                    {
                        try
                        {
                            totalHeight += floors[i].levels.Length * floors[i].floor2floor;
                        }
                        catch (Exception e)
                        {
                            Debug.Log(e);
                        }
                    }
                    //For the transfer decks
                    totalHeight += (floors.Count - 1) * 0.5f;
                    totalHeight += Standards.TaggedObject.parapetHeight + Standards.TaggedObject.roofDepth;
                    if (podium != null)
                    {
                        totalHeight += podium.levels.Length * podium.floor2floor + 0.5f;
                    }
                    var corner = (podium == null || podium.exteriorPolygon == null) ? (floors[0] as ProceduralMansion).exteriorPolygon[0].currentPosition : (podium as ProceduralMansion).baseFloor.exteriorPolygon[0].currentPosition - Vector3.up * (podium.levels.Length * podium.floor2floor);
                    Vector3 start = corner + (corner - (podium == null ? floors[0].transform.position : podium.transform.position)).normalized * offsetDistanceLine;
                    Vector3 end = start + Vector3.up * totalHeight;
                    UpdateDistance(start, end, heightLine);
                }
                else
                {
                    heightLine.gameObject.SetActive(true);
                    float totalHeight = 0;//floors[0].floor2floor;
                    for (int i = 0; i < floors.Count; i++)
                    {
                        try
                        {
                            totalHeight += floors[i].levels.Length * floors[i].floor2floor;
                        }
                        catch (Exception e)
                        {
                            Debug.Log(e);
                        }
                    }
                    //For the transfer decks
                    totalHeight += (floors.Count - 1) * 0.5f;
                    totalHeight += Standards.TaggedObject.parapetHeight + Standards.TaggedObject.roofDepth;
                    if (podium != null)
                    {
                        totalHeight += podium.levels.Length * podium.floor2floor + 0.5f;
                    }
                    var corner = (podium == null || podium.exteriorPolygon == null) ? (floors[0] as ProceduralTower).exteriorPolygon[0].currentPosition : (podium as ProceduralTower).baseTower.exteriorPolygon[0].currentPosition - Vector3.up * (podium.levels.Length * podium.floor2floor);
                    Vector3 start = corner + (corner - (podium == null ? floors[0].transform.position : podium.transform.position)).normalized * offsetDistanceLine;
                    Vector3 end = start + Vector3.up * totalHeight;
                    UpdateDistance(start, end, heightLine);
                }
            }
        }

        /// <summary>
        /// Generates the floors of the building
        /// </summary>
        public void GenerateFloors()
        {
            if (buildingType == BuildingType.Tower || buildingType == BuildingType.Mansion)
            {
                if (podium != null)
                {
                    StartCoroutine(GeneratePodium());
                }
                if (basement != null)
                {
                    StartCoroutine(GenerateBasement());
                }
            }
        }

        /// <summary>
        /// Called to delete a floor of this building
        /// </summary>
        /// <param name="floorIndex">The index of the floor to be deleted</param>
        public void DeleteFloor(int floorIndex)
        {
            if (floors.Count > floorIndex)
            {
                currentFloor = floors[floorIndex];
                Destroy(currentFloor.gameObject);
                currentFloor = null;
                floors.RemoveAt(floorIndex);
            }
        }

        /// <summary>
        /// Called when this building (as a tower) has been placed
        /// </summary>
        public void PlaceTower()
        {
            Placed = true;
            if (buildingType == BuildingType.Tower)
            {
                for (int i = 0; i < floors.Count; i++)
                {
                    floors[i].SetPreviewMode(buildingManager.previewMode);
                }
                AddHeightLine();
                UpdateHeightLine(null);
                StartCoroutine(OnSelect());
            }
        }

        /// <summary>
        /// Called when this building (as a Mansion Block) has been placed
        /// </summary>
        public void PlaceMansionBlock()
        {
            Placed = true;
            if (buildingType == BuildingType.Mansion)
            {
                for (int i = 0; i < floors.Count; i++)
                {
                    floors[i].SetPreviewMode(buildingManager.previewMode);
                }
                AddHeightLine();
                UpdateHeightLine(null);
                StartCoroutine(OnSelect());
            }
        }

        /// <summary>
        /// Adds a tower type floor to this building
        /// </summary>
        /// <param name="floorIndex">The index of the floor to be added</param>
        /// <param name="name">The name of the floor to be added</param>
        /// <param name="aptTypes">The list of apartment types for the added floor</param>
        /// <returns>Procedural Tower Floor</returns>
        public ProceduralTower AddTowerFloor(int floorIndex, string name, string[] aptTypes)
        {
            if (floors == null)
            {
                floors = new List<ProceduralFloor>();
            }

            ProceduralTower m_tower = Instantiate(proceduralTowerPrefab, transform).GetComponent<ProceduralTower>();
            m_tower.transform.localPosition += new Vector3(0, 5, 0);
            m_tower.building = this;
            m_tower.gameObject.name = name + ":" + index + "_Floor_" + floorIndex;
            float aptDepth = 6.8f;
            float.TryParse(buildingManager.offsetInputField.text, out aptDepth);
            m_tower.aptDepth = 6.8f;
            currentFloor = m_tower;
            m_tower.key = Guid.NewGuid().ToString();
            lastAddedFloor = m_tower;
            int[] levels = new int[(int)totalHeight];
            for (int i = 1; i <= levels.Length; i++)
            {
                levels[i - 1] = i;
            }
            currentFloor.levels = levels;
            m_tower.Initialize(aptTypes, new Vector3(coreWidth, currentFloor.floor2floor, coreLength));
            floors.Add(currentFloor);
            return m_tower;
            //if (floors.Count == 1)
            //{
            //    AddTowerGroundFloor(0, name, configurationType, m_tower);
            //}
        }

        /// <summary>
        /// Adds a mansion block type floor to this building
        /// </summary>
        /// <param name="floorIndex">The index of the floor to be added</param>
        /// <param name="name">The name of the floor to be added</param>
        /// <param name="blocksPanel">The UI panel which controls the blocks</param>
        /// <returns>Procedural Mansion Block</returns>
        public ProceduralMansion AddMansionFloor(int floorIndex, string name, BlocksPanel blocksPanel)
        {
            if (floors == null)
            {
                floors = new List<ProceduralFloor>();
            }

            ProceduralMansion m_mansion = Instantiate(proceduralMansionPrefab, transform).GetComponent<ProceduralMansion>();
            m_mansion.building = this;
            m_mansion.gameObject.name = name + ":" + index + "_Floor_" + floorIndex;
            float aptDepth = 6.8f;
            float.TryParse(buildingManager.offsetInputField.text, out aptDepth);
            m_mansion.aptDepth = 6.8f;
            currentFloor = m_mansion;
            m_mansion.key = Guid.NewGuid().ToString();
            lastAddedFloor = m_mansion;
            int[] levels = new int[(int)totalHeight];
            for (int i = 1; i <= levels.Length; i++)
            {
                levels[i - 1] = i;
            }
            currentFloor.levels = levels;
            m_mansion.Initialize();
            blocksPanel.blocksChanged.RemoveAllListeners();
            blocksPanel.blocksChanged.AddListener(m_mansion.OnBlocksChanged);
            floors.Add(currentFloor);
            return m_mansion;
        }

        ///// <summary>
        ///// Adds a tower ground floor to this building
        ///// </summary>
        ///// <param name="floorIndex"></param>
        ///// <param name="name"></param>
        ///// <param name="configurationType"></param>
        ///// <param name="tower"></param>
        //public void AddTowerGroundFloor(int floorIndex, string name, string configurationType, ProceduralTower tower)
        //{
        //    if (floors == null)
        //    {
        //        floors = new List<ProceduralFloor>();
        //    }

        //    ProceduralTower m_tower = Instantiate(proceduralTowerPrefab, transform).GetComponent<ProceduralTower>();
        //    m_tower.floor2floor = 4.5f;
        //    m_tower.building = this;
        //    m_tower.gameObject.name = name + ":" + index + "_Floor_" + floorIndex;
        //    float aptDepth = 6.8f;
        //    float.TryParse(buildingManager.offsetInputField.text, out aptDepth);
        //    m_tower.apartmentDepth = 6.8f;
        //    m_tower.key = Guid.NewGuid().ToString();
        //    lastAddedFloor = m_tower;
        //    int[] levels = new int[] { 0 };
        //    m_tower.levels = levels;
        //    m_tower.Initialize(tower);
        //    m_tower.transform.SetAsFirstSibling();
        //    floors.Insert(0, m_tower);
        //}

        /// <summary>
        /// Adds a floor to this building
        /// </summary>
        /// <param name="floorIndex">The index of the floor to be added</param>
        /// <param name="name">The name of the floor to be added</param>
        /// <returns>Procedural Floor</returns>
        public ProceduralFloor AddFloor(int floorIndex, string name)
        {
            if (buildingType == BuildingType.Linear)
            {
                if (floors == null)
                {
                    floors = new List<ProceduralFloor>();
                }
                if (floors.Count > floorIndex)
                {
                    currentFloor = floors[floorIndex];
                    Destroy(currentFloor.gameObject);
                    currentFloor = null;
                    floors.RemoveAt(floorIndex);
                }

                ProceduralFloor m_floor = Instantiate(proceduralFloorPrefab, transform).GetComponent<ProceduralFloor>();
                m_floor.transform.localPosition = Vector3.zero;
                m_floor.transform.localEulerAngles = Vector3.zero;
                m_floor.transform.localScale = Vector3.one;

                m_floor.Initialize();
                m_floor.gameObject.name = name + ":" + index + "_Floor_" + floorIndex;
                m_floor.building = this;
                float aptDepth = 6.8f;
                float.TryParse(buildingManager.offsetInputField.text, out aptDepth);
                m_floor.leftAptDepth = 6.8f;
                m_floor.rightAptDepth = 6.8f;
                currentFloor = m_floor;
                m_floor.key = Guid.NewGuid().ToString();//index + "," + floorIndex;
                lastAddedFloor = m_floor;

                floors.Add(currentFloor);
                return currentFloor;
            }
            else
            {
                return AddTowerFloor(floorIndex, name, new string[] { "2b4p", "2b4p", "1b2p", "2b4p", "2b4p", "1b2p" });
            }
        }

        /// <summary>
        /// Loads a floor to this building
        /// </summary>
        /// <param name="floorIndex">The index of the floor to be added</param>
        /// <param name="name">The name of the floor to be added</param>
        /// <param name="percentages">The percentages of the floor to be added</param>
        /// <param name="f2f">The floor to floor height of the floor to be added</param>
        /// <param name="levels">The levels of the floor to be added</param>
        /// <param name="leftDepth">The depth of the apartments on the left site of the corridor</param>
        /// <param name="rightDepth">The depth of the apartments on the right side of the corridor</param>
        /// <returns>Procedural Floor</returns>
        public ProceduralFloor LoadFloor(int floorIndex, string name, Dictionary<string, float> percentages, float f2f, int[] levels, float leftDepth, float rightDepth)
        {
            if (floors == null)
            {
                floors = new List<ProceduralFloor>();
            }
            if (floors.Count > floorIndex)
            {
                currentFloor = floors[floorIndex];
                Destroy(currentFloor.gameObject);
                currentFloor = null;
                floors.RemoveAt(floorIndex);
            }

            ProceduralFloor m_floor = Instantiate(proceduralFloorPrefab, transform).GetComponent<ProceduralFloor>();
            m_floor.transform.localPosition = Vector3.zero;
            m_floor.transform.localEulerAngles = Vector3.zero;
            m_floor.transform.localScale = Vector3.one;

            m_floor.Initialize();
            m_floor.gameObject.name = name + ":" + index + "_Floor_" + floorIndex;
            //m_floor.id = floorIndex;
            m_floor.percentages = percentages;
            m_floor.floor2floor = f2f;
            m_floor.levels = levels;
            m_floor.building = this;
            m_floor.leftAptDepth = leftDepth;
            m_floor.rightAptDepth = rightDepth;
            //float aptDepth = 6.8f;
            //float.TryParse(buildingManager.offsetInputField.text, out aptDepth);
            //m_floor.leftAptDepth = 6.8f;
            //m_floor.rightAptDepth = 6.8f;
            currentFloor = m_floor;
            m_floor.key = Guid.NewGuid().ToString();
            lastAddedFloor = m_floor;

            floors.Add(currentFloor);
            return currentFloor;
        }

        /// <summary>
        /// Loads a tower floor to this building
        /// </summary>
        /// <param name="floorIndex">The index of the floor to be added</param>
        /// <param name="name">The name of the floor to be added</param>
        /// <param name="percentages">The percentages of the floor to be added</param>
        /// <param name="f2f">The floor to floor height of the floor to be added</param>
        /// <param name="levels">The levels of the floor to be added</param>
        /// <param name="aptTypes">The list of apartment types for the floor to be added</param>
        /// <returns>Procedural Tower</returns>
        public ProceduralTower LoadTowerFloor(int floorIndex, string name, Dictionary<string, float> percentages, float f2f, int[] levels, string[] aptTypes)
        {
            if (floors == null)
            {
                floors = new List<ProceduralFloor>();
            }
            if (floors.Count > floorIndex)
            {
                currentFloor = floors[floorIndex];
                Destroy(currentFloor.gameObject);
                currentFloor = null;
                floors.RemoveAt(floorIndex);
            }

            //currentCoreDimensions = new Vector3(currentCoreDimensions.x, f2f, currentCoreDimensions.z);
            ProceduralTower m_tower = Instantiate(proceduralTowerPrefab, transform).GetComponent<ProceduralTower>();
            m_tower.transform.localPosition += new Vector3(0, buildingElement.GetYForLevel(), 0);
            m_tower.building = this;
            m_tower.gameObject.name = name + ":" + index + "_Floor_" + floorIndex;
            float aptDepth = 6.8f;
            float.TryParse(buildingManager.offsetInputField.text, out aptDepth);
            m_tower.aptDepth = 6.8f;
            currentFloor = m_tower;
            m_tower.key = Guid.NewGuid().ToString();
            lastAddedFloor = m_tower;
            currentFloor.levels = levels;
            m_tower.Initialize(aptTypes, new Vector3(coreWidth, currentFloor.floor2floor, coreLength));
            m_tower.percentages = percentages;
            floors.Add(currentFloor);
            return m_tower;
        }

        /// <summary>
        /// Adds a tower floor to this building
        /// </summary>
        /// <param name="floorIndex">The index of the floor to be added</param>
        /// <param name="name">The name of the floor to be added</param>
        /// <param name="levels">The levels of the floor to be added</param>
        /// <param name="f2f">The floor to floor height of the floor to be added</param>
        /// <param name="aptTypes">The list of apartment types for the floor to be added</param>
        /// <returns>Procedural Tower</returns>
        public ProceduralTower AddTowerFloor(int floorIndex, string name, int[] levels, float f2f, string[] aptTypes)
        {
            if (floors == null)
            {
                floors = new List<ProceduralFloor>();
            }
            //currentCoreDimensions = new Vector3(currentCoreDimensions.x, f2f, currentCoreDimensions.z);
            ProceduralTower m_tower = Instantiate(proceduralTowerPrefab, transform).GetComponent<ProceduralTower>();
            m_tower.transform.localPosition += new Vector3(0, buildingElement.GetYForLevel(), 0);
            m_tower.building = this;
            m_tower.gameObject.name = name + ":" + index + "_Floor_" + floorIndex;
            float aptDepth = 6.8f;
            float.TryParse(buildingManager.offsetInputField.text, out aptDepth);
            m_tower.aptDepth = 6.8f;
            currentFloor = m_tower;
            m_tower.key = Guid.NewGuid().ToString();
            lastAddedFloor = m_tower;
            currentFloor.levels = levels;
            m_tower.Initialize(aptTypes, new Vector3(coreWidth, currentFloor.floor2floor, coreLength));
            m_tower.percentages = FloorElement.GetTowerPercentages(aptTypes);
            m_tower.floor2floor = f2f;
            floors.Add(currentFloor);
            return m_tower;
        }

        /// <summary>
        /// Adds a floor to this building
        /// </summary>
        /// <param name="floorIndex">The index of the floor to be added</param>
        /// <param name="name">The name of the floor to be added</param>
        /// <param name="levels">The levels of the floor to be added</param>
        /// <param name="f2f">The floor to floor height of the floor to be added</param>
        /// <param name="percentages">The percentages of the floor to be added</param>
        /// <returns>Procedural Floor</returns>
        public ProceduralFloor AddFloor(int floorIndex, string name, int[] levels, float f2f, Dictionary<string, float> percentages)
        {
            if (floors == null)
            {
                floors = new List<ProceduralFloor>();
            }
            if (floors.Count > floorIndex)
            {
                currentFloor = floors[floorIndex];
                Destroy(currentFloor.gameObject);
                currentFloor = null;
                floors.RemoveAt(floorIndex);
            }

            ProceduralFloor m_floor = Instantiate(proceduralFloorPrefab, transform).GetComponent<ProceduralFloor>();
            m_floor.transform.localPosition = Vector3.zero;
            m_floor.transform.localEulerAngles = Vector3.zero;
            m_floor.transform.localScale = Vector3.one;
            m_floor.gameObject.name = name + ":" + index + "_Floor_" + floorIndex;
            //m_floor.id = floorIndex;
            //m_floor.transform.position = new Vector3(0, buildingManager.designInputTabs.GetCurrentYValue(), 0);
            m_floor.Initialize();
            m_floor.levels = levels;
            m_floor.percentages = percentages;
            m_floor.floor2floor = f2f;
            //m_floor.buildingManager = buildingManager;
            m_floor.building = this;
            float aptDepth = 6.8f;
            float.TryParse(buildingManager.offsetInputField.text, out aptDepth);
            m_floor.leftAptDepth = 6.8f;
            m_floor.rightAptDepth = 6.8f;
            currentFloor = m_floor;
            m_floor.key = Guid.NewGuid().ToString();
            lastAddedFloor = m_floor;

            floors.Add(currentFloor);
            return currentFloor;
        }

        /// <summary>
        /// It sets the polygon for the basement of the building
        /// </summary>
        /// <param name="polygon">The new polygon</param>
        public void SetBasementPolygon(Polygon polygon)
        {
            if (basement != null)
            {
                polygon.transform.SetParent(basement.transform);
                Extrusion extrusion = Instantiate(buildingManager.extrusionPrefab, polygon.transform).GetComponent<Extrusion>();
                extrusion.totalHeight = basement.levels.Length * basement.floor2floor * -1;
                extrusion.Initialize(polygon);
                polygon.extrusion = extrusion;
                Material mat = new Material(Resources.Load("Materials/OutlineCutoff") as Material);
                mat.SetFloat("_Modulo", basement.levels.Length);
                mat.SetFloat("_UseTexture", 1);
                polygon.SetOriginalMaterial(mat);
                polygon.GetComponent<MeshRenderer>().enabled = false;
            }
        }

        /// <summary>
        /// It adds a basement to the building
        /// </summary>
        /// <param name="levels">The levels of the basement</param>
        /// <param name="f2f">The floor to floor height of the basement</param>
        /// <param name="percentages">The mix of the basement of the building</param>
        /// <param name="footPrintOffset">The offset from the building's footprint</param>
        /// <returns>Procedural Floor</returns>
        public ProceduralFloor AddBasement(int[] levels, float f2f, Dictionary<string, float> percentages, float footPrintOffset = 0)
        {
            if (basement != null)
            {
                Destroy(basement.gameObject);
                basement = null;
            }
            float aptDepth = 6.8f;
            switch (buildingType)
            {
                case BuildingType.Linear:
                    ProceduralFloor m_floor = Instantiate(proceduralFloorPrefab, transform).GetComponent<ProceduralFloor>();
                    m_floor.transform.localPosition = Vector3.zero;
                    m_floor.transform.localEulerAngles = Vector3.zero;
                    m_floor.transform.localScale = Vector3.one;
                    m_floor.gameObject.name = name + ":" + index + "_Basement";
                    m_floor.Initialize();
                    m_floor.levels = levels;
                    m_floor.percentages = percentages;
                    m_floor.floor2floor = f2f;
                    m_floor.isBasement = true;
                    m_floor.footPrintOffset = footPrintOffset;
                    //m_floor.buildingManager = buildingManager;
                    m_floor.building = this;
                    aptDepth = 6.8f;
                    float.TryParse(buildingManager.offsetInputField.text, out aptDepth);
                    m_floor.leftAptDepth = 6.8f;
                    m_floor.rightAptDepth = 6.8f;
                    //currentFloor = m_floor;
                    basement = m_floor;
                    m_floor.key = Guid.NewGuid().ToString();
                    lastAddedFloor = m_floor;

                    return m_floor;
                case BuildingType.Tower:
                    ProceduralTower m_tower = Instantiate(proceduralTowerPrefab, transform).GetComponent<ProceduralTower>();
                    m_tower.transform.localPosition = Vector3.zero;
                    m_tower.transform.localEulerAngles = Vector3.zero;
                    m_tower.transform.localScale = Vector3.one;
                    m_tower.gameObject.name = name + ":" + index + "_Basement";
                    m_tower.Initialize();
                    m_tower.levels = levels;
                    m_tower.percentages = percentages;
                    m_tower.floor2floor = f2f;
                    m_tower.isBasement = true;
                    m_tower.footPrintOffset = footPrintOffset;
                    //m_tower.buildingManager = buildingManager;
                    m_tower.building = this;
                    aptDepth = 6.8f;
                    float.TryParse(buildingManager.offsetInputField.text, out aptDepth);
                    m_tower.aptDepth = 6.8f;
                    //currentFloor = m_tower;
                    basement = m_tower;
                    m_tower.key = Guid.NewGuid().ToString();
                    lastAddedFloor = m_tower;

                    return m_tower;
                case BuildingType.Mansion:
                    ProceduralMansion m_mansion = Instantiate(proceduralMansionPrefab, transform).GetComponent<ProceduralMansion>();
                    m_mansion.transform.localPosition = Vector3.zero;
                    m_mansion.transform.localEulerAngles = Vector3.zero;
                    m_mansion.transform.localScale = Vector3.one;
                    m_mansion.gameObject.name = name + ":" + index + "_Basement";
                    m_mansion.Initialize();
                    m_mansion.levels = levels;
                    m_mansion.percentages = percentages;
                    m_mansion.floor2floor = f2f;
                    m_mansion.isBasement = true;
                    m_mansion.footPrintOffset = footPrintOffset;
                    //m_mansion.buildingManager = buildingManager;
                    m_mansion.building = this;
                    aptDepth = 11.77f;
                    float.TryParse(buildingManager.offsetInputField.text, out aptDepth);
                    m_mansion.aptDepth = 6.8f;
                    //currentFloor = m_mansion;
                    basement = m_mansion;
                    m_mansion.key = Guid.NewGuid().ToString();
                    lastAddedFloor = m_mansion;

                    return m_mansion;
                default:
                    return null;
            }
        }

        /// <summary>
        /// Called when the width of the core has changed
        /// </summary>
        /// <param name="width"></param>
        public void OnCoreWidthChanged(float width)
        {
            coreWidth = width;
            buildingManager.Refresh();
        }

        /// <summary>
        /// Called when the length of the core has changed
        /// </summary>
        /// <param name="length"></param>
        public void OnCoreLengthChanged(float length)
        {
            coreLength = length;
            buildingManager.Refresh();
        }

        /// <summary>
        /// Sets the mix of the basement
        /// </summary>
        /// <param name="percentages"></param>
        public void SetBasementUse(Dictionary<string, float> percentages)
        {
            if (basement != null)
            {
                basement.percentages = percentages;
            }
        }

        /// <summary>
        /// Adds a podium to the building
        /// </summary>
        /// <param name="levels">The levels of the podium</param>
        /// <param name="f2f">The floor to floor height of the podium</param>
        /// <param name="percentages">The mix of the podium</param>
        /// <param name="footPrintOffset">The offset from the building's footprint</param>
        /// <returns>Procedural Floor</returns>
        public ProceduralFloor AddPodium(int[] levels, float f2f, Dictionary<string, float> percentages, float footPrintOffset = 0)
        {
            if (podium != null)
            {
                Destroy(podium.gameObject);
                podium = null;
            }
            float aptDepth = 6.8f;
            switch (buildingType)
            {
                case BuildingType.Linear:
                    ProceduralFloor m_floor = Instantiate(proceduralFloorPrefab, transform).GetComponent<ProceduralFloor>();
                    m_floor.transform.localPosition = Vector3.zero;
                    m_floor.transform.localEulerAngles = Vector3.zero;
                    m_floor.transform.localScale = Vector3.one;
                    m_floor.gameObject.name = name + ":" + index + "_Podium";
                    m_floor.Initialize();
                    m_floor.levels = levels;
                    m_floor.percentages = percentages;
                    m_floor.floor2floor = f2f;
                    m_floor.isPodium = true;
                    m_floor.footPrintOffset = footPrintOffset;
                    //m_floor.buildingManager = buildingManager;
                    m_floor.building = this;
                    aptDepth = 6.8f;
                    float.TryParse(buildingManager.offsetInputField.text, out aptDepth);
                    m_floor.leftAptDepth = 6.8f;
                    m_floor.rightAptDepth = 6.8f;
                    //currentFloor = m_floor;
                    podium = m_floor;
                    m_floor.key = Guid.NewGuid().ToString();
                    lastAddedFloor = m_floor;

                    return m_floor;
                case BuildingType.Tower:
                    ProceduralTower m_tower = Instantiate(proceduralTowerPrefab, transform).GetComponent<ProceduralTower>();
                    m_tower.transform.localPosition = Vector3.zero;
                    m_tower.transform.localEulerAngles = Vector3.zero;
                    m_tower.transform.localScale = Vector3.one;
                    m_tower.gameObject.name = name + ":" + index + "_Podium";
                    m_tower.Initialize();
                    m_tower.levels = levels;
                    m_tower.percentages = percentages;
                    m_tower.floor2floor = f2f;
                    m_tower.isPodium = true;
                    m_tower.footPrintOffset = footPrintOffset;
                    //m_tower.buildingManager = buildingManager;
                    m_tower.building = this;
                    aptDepth = 6.8f;
                    float.TryParse(buildingManager.offsetInputField.text, out aptDepth);
                    m_tower.aptDepth = 6.8f;
                    //currentFloor = m_tower;
                    podium = m_tower;
                    m_tower.key = Guid.NewGuid().ToString();
                    lastAddedFloor = m_tower;

                    return m_tower;
                case BuildingType.Mansion:
                    ProceduralMansion m_mansion = Instantiate(proceduralMansionPrefab, transform).GetComponent<ProceduralMansion>();
                    m_mansion.transform.localPosition = Vector3.zero;
                    m_mansion.transform.localEulerAngles = Vector3.zero;
                    m_mansion.transform.localScale = Vector3.one;
                    m_mansion.gameObject.name = name + ":" + index + "_Podium";
                    m_mansion.Initialize();
                    m_mansion.levels = levels;
                    m_mansion.percentages = percentages;
                    m_mansion.floor2floor = f2f;
                    m_mansion.isPodium = true;
                    m_mansion.footPrintOffset = footPrintOffset;
                    //m_mansion.buildingManager = buildingManager;
                    m_mansion.building = this;
                    aptDepth = 11.77f;
                    float.TryParse(buildingManager.offsetInputField.text, out aptDepth);
                    m_mansion.aptDepth = 6.8f;
                    //currentFloor = m_mansion;
                    podium = m_mansion;
                    m_mansion.key = Guid.NewGuid().ToString();
                    lastAddedFloor = m_mansion;

                    return m_mansion;
                default:
                    return null;
            }
        }

        /// <summary>
        /// Sets the mix of the podium
        /// </summary>
        /// <param name="percentages">The mix of the podium</param>
        /// <param name="buildingPanel">The UI panel which controls the buildings</param>
        public void SetPodiumUse(Dictionary<string, float> percentages, BuildingPanel buildingPanel)
        {
            if (podium != null)
            {
                podium.percentages = percentages;
            }
            else
            {
                int[] levels;
                float f2f;
                float offset;
                buildingPanel.GetPodiumFeatures(out levels, out f2f, out offset);
                AddPodium(levels, f2f, percentages, offset);
            }
        }

        /// <summary>
        /// Sets the mix of the basement
        /// </summary>
        /// <param name="percentages">The mix of the basement</param>
        /// <param name="buildingPanel">The UI panel which controls the buildings</param>
        public void SetBasementUse(Dictionary<string, float> percentages, BuildingPanel buildingPanel)
        {
            if (basement != null)
            {
                basement.percentages = percentages;
            }
            else
            {
                int[] levels;
                float f2f;
                float offset;
                buildingPanel.GetBasementFeatures(out levels, out f2f, out offset);
                AddBasement(levels, f2f, percentages, offset);
            }
        }

        /// <summary>
        /// Called when the features of the podium have changed
        /// </summary>
        /// <param name="buildingPanel">The UI panel which controls the buildings</param>
        public void UpdatePodiumFeatures(BuildingPanel buildingPanel)
        {
            int[] levels;
            float f2f;
            float offset;
            buildingPanel.GetPodiumFeatures(out levels, out f2f, out offset);
            if (podium != null)
            {
                podium.UpdateLevels(f2f, levels, offset);
            }
            else
            {
                AddPodium(levels, f2f, new Dictionary<string, float>(), offset);
            }
        }

        /// <summary>
        /// Called when the features of the basement have changed
        /// </summary>
        /// <param name="buildingPanel">The UI panel which controls the buildings</param>
        public void UpdateBasementFeatures(BuildingPanel buildingPanel)
        {
            int[] levels;
            float f2f;
            float offset;
            buildingPanel.GetBasementFeatures(out levels, out f2f, out offset);
            if (basement != null)
            {
                basement.UpdateLevels(f2f, levels, offset);
            }
            else
            {
                AddBasement(levels, f2f, new Dictionary<string, float>(), offset);
            }
        }

        /// <summary>
        /// Sets the selected floor for this building
        /// </summary>
        /// <param name="floorIndex">The index of the selected floor</param>
        public void SetCurrentFloor(int floorIndex)
        {
            currentFloor = floors[floorIndex];
        }

        /// <summary>
        /// Adds a floor with a custom polygon to this building
        /// </summary>
        /// <param name="offsetDistance">The offset distance for the floor's centreline</param>
        /// <param name="polygon">The custom polygon</param>
        public void AddCustomFloorPolygon(float offsetDistance, PolygonSegment polygon)
        {
            int floorIndex = floors.IndexOf(currentFloor);
            if (floors.Count > floorIndex)
            {
                currentFloor = floors[floorIndex];
                if (currentFloor.centreLine != null)
                {
                    Destroy(currentFloor.centreLine.gameObject);
                }
            }

            ProceduralFloor m_floor = currentFloor;
            m_floor.transform.position = new Vector3(0, buildingElement.GetYForLevel(floorIndex), 0);
            polygon.transform.SetParent(m_floor.transform);
            polygon.gameObject.name = "CustomPolygon_" + floorIndex;
            m_floor.building = this;
            m_floor.Initialize(polygon, true);
            //m_floor.id = floorIndex;
            //m_floor.key = Guid.NewGuid().ToString();
            lastAddedFloor = m_floor;

            List<float> vertices = new List<float>();
            for (int i = 0; i < currentFloor.centreLine.Count; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    vertices.Add(currentFloor.centreLine[i].currentPosition[j]);
                }
            }

            if (m_floor.hasGeometry)
            {
                StartCoroutine(m_floor.GenerateGeometries(currentMaximumWidth));
            }
        }

        /// <summary>
        /// Sets the depth of the apartments for the floors
        /// </summary>
        /// <param name="depth">The new apartment depth</param>
        public void SetOffsetDistance(float depth)
        {
            for (int i = 0; i < floors.Count; i++)
            {
                floors[i].SetAptDepth(depth);
            }
        }

        /// <summary>
        /// Returns the depths of the apartments
        /// </summary>
        /// <returns>Float</returns>
        public float GetOffsetDistance()
        {
            if (floors[0] as ProceduralTower != null)
            {
                return (floors[0] as ProceduralTower).aptDepth;
            }
            else if (floors[0] as ProceduralMansion != null)
            {
                return 11.77f;
            }
            else
            {
                return 7.2f;
            }
        }

        /// <summary>
        /// Loads the geometries for a floor of this building
        /// </summary>
        /// <param name="depth">The apartment depth</param>
        /// <param name="floor2floor">The floor to floor height</param>
        /// <param name="state">The save state of the floor</param>
        public void LoadGeometries(float depth, float floor2floor, FloorLayoutState state)
        {
            currentFloor = floors[state.floorIndex];
            currentFloor.SetAptDepth(depth);
            if (currentFloor as ProceduralMansion != null)
            {
                currentFloor.GetComponent<ProceduralMansion>().aptDepth=depth;
            }
            coreParameters = state.coreParams;
            //leftApartmentDepth = depth;
            currentFloor.floor2floor = floor2floor;
            if (!state.isTower && !state.isMansion)
            {
                if (state.isCopy)
                {
                    SetCentreLineToFloor(currentFloor, UseMasterPolyline(state.floorIndex));
                    currentFloor.projectedCores = true;
                    currentFloor.LoadGeometries(state, floors[0]);
                }
                else if (state.isCustom)
                {
                    currentFloor.projectedCores = true;
                    currentFloor.LoadGeometries(state, floors[0]);
                }
                else
                {
                    SetCentreLineToFloor(currentFloor, UseMasterPolyline(state.floorIndex));
                    currentFloor.LoadGeometries(state, null);
                }
                if (heightLine == null && masterPolyline != null)
                {
                    AddHeightLine(masterPolyline);
                    UpdateHeightLine(masterPolyline);
                    GetCoreDistances();
                }
            }
            else
            {
                //if (state.floorIndex == 0)
                //{
                //    currentFloor.LoadGeometries(state, floors[1]);
                //}
                //else
                {
                    currentFloor.LoadGeometries(state, null);
                }
            }
        }

        /// <summary>
        /// Loads the geometries for the floors
        /// </summary>
        /// <param name="leftDepth">The depth of the apartments on the left side of the corridor</param>
        /// <param name="rightDepth">The depth of the apartments on the right side of the corridor</param>
        /// <param name="floor2floor">The floor to floor height of the floor</param>
        /// <param name="state">The floor save state to be loaded</param>
        public void LoadGeometries(float leftDepth, float rightDepth, float floor2floor, FloorLayoutState state)
        {
            if (!state.isBasement && !state.isPodium)
            {
                currentFloor = floors[state.floorIndex];
                currentFloor.SetAptDepth(leftDepth, rightDepth);
                //leftApartmentDepth = depth;
                currentFloor.floor2floor = floor2floor;
                if (!state.isTower)
                {
                    if (state.isCopy)
                    {
                        SetCentreLineToFloor(currentFloor, UseMasterPolyline(state.floorIndex));
                        currentFloor.projectedCores = true;
                        currentFloor.LoadGeometries(state, floors[0]);
                    }
                    else if (state.isCustom)
                    {
                        currentFloor.projectedCores = true;
                        currentFloor.LoadGeometries(state, floors[0]);
                    }
                    else
                    {
                        SetCentreLineToFloor(currentFloor, UseMasterPolyline(state.floorIndex));
                        currentFloor.LoadGeometries(state, null);
                    }
                    if (heightLine == null && masterPolyline != null)
                    {
                        AddHeightLine(masterPolyline);
                        UpdateHeightLine(masterPolyline);
                        GetCoreDistances();
                    }
                }
                else
                {
                    if (state.floorIndex == 0)
                    {
                        currentFloor.LoadGeometries(state, floors[1]);
                    }
                    else
                    {
                        currentFloor.LoadGeometries(state, null);
                    }
                }
            }
            else if (state.isPodium)
            {
                if (podium != null)
                {
                    LoadPodium(state);
                    //StartCoroutine(GeneratePodium(state));
                    podium.LoadGeometries(state, floors[0]);
                }
            }
            else if (state.isBasement)
            {
                if (basement != null)
                {
                    LoadBasement();
                    //StartCoroutine(GenerateBasement(true));
                    basement.LoadGeometries(state, floors[0]);
                }
            }
        }

        /// <summary>
        /// Called when the floor to floor height or the levels of a floor have changed
        /// </summary>
        public void OnFloorHeightChanged()
        {
            UpdateHeightLine(masterPolyline);
            GetCoreDistances();
            //floors[0].UpdatePlantRoom();
            buildingManager.CheckSystemConstraints(this);
            StartCoroutine(buildingManager.OverallUpdate());
        }

        /// <summary>
        /// Adds a height line to this building
        /// </summary>
        /// <param name="poly">The polygon from which the heightline will get its position</param>
        public void AddHeightLine(Polygon poly)
        {
            if (heightLine != null)
            {
                Destroy(heightLine);
                heightLine = null;
            }

            currentDistance = new GameObject("Distance_" + index);
            heightLine = currentDistance;
            currentDistance.transform.SetParent(transform);

            GameObject text = Instantiate(Resources.Load("Geometry/TextMesh") as GameObject, currentDistance.transform);
            text.transform.Rotate(text.transform.right, 90);
            TextMesh mText = text.GetComponent<TextMesh>();
            mText.characterSize = 1;

            if (useCanvas)
            {
                mText.GetComponent<MeshRenderer>().enabled = false;
                GameObject distUI = Instantiate(Resources.Load("GUI/DistanceText") as GameObject, distanceUIParent);
                distanceUI = distUI.GetComponent<Text>();
                distUI.GetComponent<Text>().color = distancesMaterial.color;
                distUI.GetComponent<UiRepresentation>().Initialize(text.transform);
            }

            GameObject line1 = new GameObject("Line1");
            line1.transform.SetParent(currentDistance.transform);
            LineRenderer l1 = line1.AddComponent<LineRenderer>();
            l1.material = distancesMaterial;
            l1.useWorldSpace = true;
            l1.positionCount = 2;
            l1.startWidth = lineWidth;
            l1.endWidth = lineWidth;
            l1.SetPosition(0, (poly[0].currentPosition + (poly[0].currentPosition - poly[1].currentPosition).normalized * offsetDistanceLine));
            l1.SetPosition(1, (poly[0].currentPosition + (poly[0].currentPosition - poly[1].currentPosition).normalized * offsetDistanceLine));
        }

        /// <summary>
        /// Adds a height line to the building
        /// </summary>
        public void AddHeightLine()
        {
            if (heightLine != null)
            {
                Destroy(heightLine);
                heightLine = null;
            }

            currentDistance = new GameObject("Distance_" + index);
            heightLine = currentDistance;
            currentDistance.transform.SetParent(transform);
            currentDistance.transform.position = transform.position;

            GameObject text = Instantiate(Resources.Load("Geometry/TextMesh") as GameObject, currentDistance.transform);
            text.transform.Rotate(text.transform.right, 90);
            TextMesh mText = text.GetComponent<TextMesh>();
            mText.characterSize = 1;

            if (useCanvas)
            {
                mText.GetComponent<MeshRenderer>().enabled = false;
                GameObject distUI = Instantiate(Resources.Load("GUI/DistanceText") as GameObject, distanceUIParent);
                distanceUI = distUI.GetComponent<Text>();
                distUI.GetComponent<Text>().color = distancesMaterial.color;
                distUI.GetComponent<UiRepresentation>().Initialize(text.transform);
            }

            GameObject line1 = new GameObject("Line1");
            line1.transform.SetParent(currentDistance.transform);
            LineRenderer l1 = line1.AddComponent<LineRenderer>();
            l1.material = distancesMaterial;
            l1.useWorldSpace = true;
            l1.positionCount = 2;
            l1.startWidth = lineWidth;
            l1.endWidth = lineWidth;
            var corner = floors[0].exteriorPolygon[0].currentPosition;
            l1.SetPosition(0, (corner + (corner - floors[0].transform.position).normalized * offsetDistanceLine));
            l1.SetPosition(1, (corner + (corner - floors[0].transform.position).normalized * offsetDistanceLine));
        }

        /// <summary>
        /// Sets the site-wide basement under this building
        /// </summary>
        /// <param name="siteBasement">The site-wide basement</param>
        public void SetBasement(SiteBasement siteBasement)
        {
            this.siteBasement = siteBasement;
        }
        /// <summary>
        /// Removes the site-wide basement under this building
        /// </summary>
        public void RemoveSiteBasement()
        {
            siteBasement = null;
        }
        /// <summary>
        /// Removes the site-wide podium under this building
        /// </summary>
        public void RemoveSitePodium()
        {
            sitePodium = null;
            transform.position = new Vector3(transform.position.x, baseHeight, transform.position.z);
            if (basement != null)
            {
                basement.transform.position = new Vector3(basement.transform.position.x, 0, basement.transform.position.z);
                basement.UpdateFloorOutline(buildingManager.previewMode);
                if (basement.hasCustomExterior)
                {
                    basement.exteriorPolygon.UpdateVertexPositions();
                }
            }
            if (podium != null)
            {
                podium.UpdateFloorOutline(buildingManager.previewMode);
                if (podium.hasCustomExterior)
                {
                    podium.exteriorPolygon.UpdateVertexPositions();
                }
            }
            for (int i = 0; i < floors.Count; i++)
            {
                floors[i].UpdatePosition();
            }
        }
        /// <summary>
        /// Sets the site-wide podium under this building
        /// </summary>
        /// <param name="sitePodium">The site-wide podium</param>
        public void SetPodium(SitePodium sitePodium)
        {
            this.sitePodium = sitePodium;
            //var bHeight = baseHeight;
            //this.sitePodium = sitePodium;
            //var newBaseHeight = this.sitePodium == null ? 0f : sitePodium.floors * sitePodium.floor2Floor;
            //if (Math.Round(bHeight, 2) != Math.Round(newBaseHeight, 2))
            //{
            //baseHeight = newBaseHeight;
            transform.position = new Vector3(transform.position.x, baseHeight, transform.position.z);
            if (basement != null)
            {
                basement.transform.position = new Vector3(basement.transform.position.x, 0, basement.transform.position.z);
                basement.UpdateFloorOutline(buildingManager.previewMode);
            }
            if (podium != null)
            {
                podium.UpdateFloorOutline(buildingManager.previewMode);
            }
            for (int i = 0; i < floors.Count; i++)
            {
                floors[i].UpdatePosition();
            }
            //}
        }
        /// <summary>
        /// Returns a Three.Js representation of the building
        /// </summary>
        /// <returns>Three.Js Object 3D</returns>
        public BT.Object3d GetThreeJsObject()
        {
            BT.Object3d obj = new BT.Object3d();
            obj.userData.Add("type", "Building");
            obj.userData.Add("buildingIndex", index.ToString());
            for (int i = 0; i < floors.Count; i++)
            {
                obj.children.AddRange(floors[i].GetThreeJsObjects());
            }
            if (podium != null)
            {
                obj.children.AddRange(podium.GetThreeJsObjects());
            }
            if (basement != null)
            {
                obj.children.AddRange(basement.GetThreeJsObjects());
            }
            return obj;
        }
        #endregion

        #region Private Methods
        private IEnumerator OnSelect()
        {
            yield return StartCoroutine(buildingManager.OnBuildingSelected(this));
            buildingManager.SetSelectedFloor(this, floors.IndexOf(currentFloor));
            if (previewMode == PreviewMode.Floors)
            {
                if (currentFloor.centreLine != null)
                {
                    currentFloor.centreLine.gameObject.SetActive(true);
                }
                currentFloor.ToggleChildren(true);
            }
        }

        private void UpdateDistance(Vector3 start, Vector3 end, GameObject distance)
        {
            float dist = Vector3.Distance(start, end);

            TextMesh mText = distance.transform.GetChild(0).GetComponent<TextMesh>();
            mText.text = dist.ToString("0.00");
            distance.transform.GetChild(0).position = start + (end - start) / 2 + Vector3.Cross((end - start), Vector3.right).normalized * distanceOffset;

            if (useCanvas)
            {
                distanceUI.GetComponent<RectTransform>().position = Camera.main.WorldToScreenPoint(distance.transform.GetChild(0).position);
                distanceUI.text = dist.ToString("0.00");
            }

            float angle = Vector3.Angle(Vector3.right, end - start);
            if (Vector3.Cross((end - start), Vector3.right).y < 0)
            {
                angle = -angle;
            }
            distance.transform.GetChild(0).eulerAngles = new Vector3(90, -angle, 0);

            LineRenderer l1 = distance.transform.GetChild(1).GetComponent<LineRenderer>();
            l1.SetPosition(0, start);
            l1.SetPosition(1, end);
        }



        private void SetCentreLineToFloor(ProceduralFloor m_floor, Polygon polygon, bool loaded = false)
        {
            if (!loaded)
            {
                m_floor.Initialize(polygon);
            }
            else
            {
                m_floor.Load(polygon);
            }
            polygon.transform.SetParent(m_floor.transform);
        }

        private Polygon UseMasterPolyline(int index)
        {
            if (index - 1 >= 0)
            {
                float yValue = buildingElement.GetYForLevel(index);
                Polygon toBeCopied = masterPolyline;
                return buildingManager.polygonDrawer.CopyPolygon(toBeCopied, yValue + baseHeight);
            }
            else return masterPolyline;
        }

        private Polygon UseMasterPolyline(float yValue)
        {
            Polygon toBeCopied = masterPolyline;
            return buildingManager.polygonDrawer.CopyPolygon(toBeCopied, yValue);
        }
        #endregion
    }

    /// <summary>
    /// The alignment of the core in relation to the corridor and the external wall
    /// </summary>
    public enum CoreLock
    {
        /// <summary>
        /// The core will be aligned with the corridor wall
        /// </summary>
        CorridorWall = 0,
        /// <summary>
        /// The core will be aligned with the external wall
        /// </summary>
        ExternalWall = 1
    }
}

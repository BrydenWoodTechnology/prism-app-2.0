﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BrydenWoodUnity.GeometryManipulation;
using BrydenWoodUnity.UIElements;
using System.Linq;
using System;
using BT = BrydenWoodUnity.GeometryManipulation.ThreeJs;

namespace BrydenWoodUnity.DesignData
{
    /// <summary>
    /// A Monobehaviour component for the apartment
    /// </summary>
    public class ApartmentUnity : BaseDesignData, IConstrainable
    {
        #region Public Fields and Properties
        [SerializeField, Header("Scene References:")]
        private ProceduralFloor proceduralFloor;
        public ProceduralFloor ProceduralFloor { get { return proceduralFloor; } set { proceduralFloor = value; } }

        [SerializeField, Header("Apartment Characteristics:")]
        private int id;
        public int Id { get { return id; } set { id = value; } }
        [SerializeField]
        private string apartmentType;
        public string ApartmentType { get { return apartmentType; } set { apartmentType = value; } }


        public bool error = false;
        [SerializeField]
        private bool isCorner;
        public bool IsCorner
        {
            get { return isCorner; }
            set
            {
                isCorner = value;
                if (isCorner)
                {
                    if (editableMesh != null)
                        editableMesh.editEdges = false;
                }
            }
        }

        public bool isM3 = false;

        public bool IsRectangular
        {
            get
            {
                bool ortho = true;
                if (editableMesh != null)
                {
                    for (int i = 0; i < editableMesh.Count; i++)
                    {
                        int prev = i - 1;
                        if (prev < 0) prev = editableMesh.Count - 1;
                        int next = (i + 1) % editableMesh.Count;
                        float angle = Vector3.Angle(editableMesh[prev].currentPosition - editableMesh[i].currentPosition, editableMesh[next].currentPosition - editableMesh[i].currentPosition);
                        if (Mathf.Abs(angle - 90) > 1)
                        {
                            return false;
                        }
                    }
                }
                else if (polygon != null)
                {
                    for (int i = 0; i < polygon.Count; i++)
                    {
                        int prev = i - 1;
                        if (prev < 0) prev = polygon.Count - 1;
                        int next = (i + 1) % polygon.Count;
                        float angle = Vector3.Angle(polygon[prev].currentPosition - polygon[i].currentPosition, polygon[next].currentPosition - polygon[i].currentPosition);
                        if (Mathf.Abs(angle - 90) > 1)
                        {
                            return false;
                        }
                    }
                }
                return ortho;
            }
        }

        public bool isExterior = true;
        public bool isCoreResidual;
        public List<ApartmentUnity> closeOnes;
        public int activeCorridorIndex = -1;
        public bool flipped;
        [SerializeField]
        private bool unresolvable;
        public bool isUnresolvable
        {
            get { return unresolvable; }
            set
            {
                unresolvable = value;
                if (unresolvable)
                {
                    editableMesh.editEdges = false;
                }
            }
        }
        public bool unresolvableOnLeft = false;
        public bool unresolvableOnRight = false;
        public GameObject resLeft;
        public GameObject resRight;
        public bool hasLeft;
        public bool hasRight;
        public bool hasCoreLeft;
        public bool hasCoreRight;


        public Vector3 SnappingDirection
        {
            get
            {
                if (editableMesh != null)
                {
                    if (editableMesh.alteredAxis)
                    {
                        return transform.forward;
                    }
                    else
                    {
                        return (flipped) ? -transform.right : transform.right;
                    }
                }
                else return Vector3.zero;
            }
        }
        public bool TowerApartment = false;

        public float FacadeLength { get { return GetFacadeLength(); } }
        public int numberOfPeople
        {
            get
            {
                int num = 0;
                if (apartmentType != "Studio")
                {
                    num = (int)Char.GetNumericValue(apartmentType[apartmentType.Length - 2]);
                }
                else
                {
                    num = 2;
                }
                return (num > 0) ? num : 0;
            }
        }
        public float Width
        {
            get
            {
                if (editableMesh != null && editableMesh.vertices.Count > 3)
                {
                    return (float)Math.Round(Vector3.Distance(editableMesh.vertices[0].currentLocalPosition, editableMesh.vertices[3].currentLocalPosition), 2);
                }
                else if (polygon != null && polygon.vertices.Count > 3)
                {
                    return (float)Math.Round(Vector3.Distance(polygon.vertices[0].currentLocalPosition, polygon.vertices[3].currentLocalPosition), 2);
                }
                else
                {
                    return 0f;
                }
            }
        }
        public float Depth
        {
            get
            {
                if (editableMesh != null)
                {
                    if (editableMesh.Count >= 4)
                    {
                        return (float)Math.Round(Vector3.Distance(editableMesh.vertices[0].currentLocalPosition, editableMesh.vertices[1].currentLocalPosition), 2);
                    }
                    else return 7.2f;
                }
                else if (polygon != null)
                {
                    if (polygon.Count >= 4)
                        return (float)Math.Round(Vector3.Distance(polygon.vertices[0].currentLocalPosition, polygon.vertices[1].currentLocalPosition), 2);
                    else return 7.2f;
                }
                else
                {
                    return 7.2f;
                }
            }
        }
        public float Area
        {
            get
            {
                if (editableMesh != null)
                {
                    if (editableMesh.Count == 4)
                    {
                        float value = 0.0f;

                        var dim1 = Width - (Standards.TaggedObject.ConstructionFeatures["PartyWall"]);
                        var dim2 = Depth - Standards.TaggedObject.ConstructionFeatures["CorridorWall"];
                        value = dim1 * dim2;

                        return (float)Math.Round(value);
                    }
                    else
                    {
                        return (float)Math.Round(editableMesh.area);
                    }
                }
                else if (polygon != null)
                {
                    if (polygon.Count == 4)
                    {
                        float value = 0.0f;

                        var dim1 = Width - (Standards.TaggedObject.ConstructionFeatures["PartyWall"]);
                        var dim2 = Depth - Standards.TaggedObject.ConstructionFeatures["CorridorWall"];
                        value = dim1 * dim2;

                        return (float)Math.Round(value);
                    }
                    else
                    {
                        return (float)Math.Round(polygon.Area);
                    }
                }
                else
                {
                    float value = 0.0f;

                    var dim1 = Width - (Standards.TaggedObject.ConstructionFeatures["PartyWall"]);
                    var dim2 = Depth - Standards.TaggedObject.ConstructionFeatures["CorridorWall"];
                    value = dim1 * dim2;

                    return (float)Math.Round(value);
                }
            }
        }
        public GameObject Representation { get; private set; }
        public static string InfoLabels
        {
            get
            {
                List<string> vals = new List<string>() { "OverallArea", "FloorArea", "FacadeArea", "NumberOfPeople", "Floor2Floor", "Level", "ApartmentDepth", "ApartmentWidth", "ApartmentType" };
                return string.Join(",", vals.ToArray());
            }
        }
        public bool HasCorridor { get; set; }
        public float NIA
        {
            get
            {
                if (editableMesh != null)
                {
                    if (editableMesh.Count == 4)
                        return (Vector3.Distance(editableMesh[0].currentPosition, editableMesh[3].currentPosition) - Standards.TaggedObject.ConstructionFeatures["PartyWall"]) * (Vector3.Distance(editableMesh[0].currentPosition, editableMesh[1].currentPosition) - Standards.TaggedObject.ConstructionFeatures["CorridorWall"]);
                    else
                        return editableMesh.area;

                }
                else if (polygon != null)
                {
                    if (polygon.Count == 4)
                        return (Vector3.Distance(polygon[0].currentPosition, editableMesh[3].currentPosition) - Standards.TaggedObject.ConstructionFeatures["PartyWall"]) * (Vector3.Distance(editableMesh[0].currentPosition, editableMesh[1].currentPosition) - Standards.TaggedObject.ConstructionFeatures["CorridorWall"]);
                    else
                        return polygon.Area;
                }
                else
                {
                    return 0;
                }
            }
        }
        [SerializeField]
        private int orientation = -1;
        public int Orientation
        {
            get
            {
                if (apartmentType == "SpecialApt" || (editableMesh != null && editableMesh.Count != 4))
                {
                    return -1;
                }
                float angle = Vector3.Angle(Vector3.forward, transform.forward);
                Vector3 cross = Vector3.Cross(Vector3.forward, transform.forward);
                if (cross.y > 0)
                {
                    if (angle < 45)
                    {
                        orientation = 0;
                    }
                    else if (angle > 45 && angle < 135)
                    {
                        orientation = 1;
                    }
                    else
                    {
                        orientation = 2;
                    }
                }
                else
                {
                    if (angle < 45)
                    {
                        orientation = 0;
                    }
                    else if (angle > 45 && angle < 135)
                    {
                        orientation = 3;
                    }
                    else
                    {
                        orientation = 2;
                    }
                }
                return orientation;
            }
        }

        [SerializeField]
        private bool isDualAspect = false;
        public bool IsDualAspect
        {
            get
            {
                if (apartmentType == "SpecialApt" || (editableMesh != null && editableMesh.Count != 4))
                {
                    return false;
                }
                isDualAspect = false;
                if (!hasCoreLeft && !hasLeft)
                {
                    isDualAspect = true;
                }
                if (!hasCoreRight && !hasRight)
                {
                    isDualAspect = true;
                }
                return isDualAspect;
            }
        }
        #endregion

        #region Private Fields and Properties
        private string errorMessage { get; set; }
        private GameObject ManipUI;
        private bool ShowExtensions { get; set; }
        private int DrawIndex { get; set; }
        private List<GameObject> Colliders { get; set; }
        private WaitForEndOfFrame WaitFrame { get; set; }
        private GameObject signCentre { get; set; }
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        /// <summary>
        /// Called when the gameObject of the apartment is destroyed
        /// </summary>
        public void OnDestroy()
        {
            //apartment.areaChanged -= OnAreaChanged;
            if (editableMesh != null)
            {
                editableMesh.updated -= OnEditableMeshUpdated;
                editableMesh.stoppedEditing -= OnMeshStoppedEditing;
            }
            base.ConstraintsChanged(this, false);
            if (proceduralFloor != null)
            {
                proceduralFloor.apartments.Remove(this);
                proceduralFloor.OnApartmentsChanged();
            }
            ApartmentAreaRange.areaChanged -= CheckAgainstConstraints;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Initializes the instance
        /// </summary>
        /// <param name="id">The id of the apartment</param>
        /// <param name="apartmentType">The apartment type</param>
        /// <param name="floor">The procedural floor to which it belongs</param>
        /// <param name="closeOnes">The list of apartments which are next to it or on the same corridor side</param>
        public void Initialize(int id, string apartmentType, ProceduralFloor floor = null, List<ApartmentUnity> closeOnes = null)
        {
            this.id = id;//apartment.id;
            if (floor != null)
            {
                proceduralFloor = floor;
            }
            this.apartmentType = apartmentType;
            if (closeOnes != null)
            {
                closeOnes.Remove(this);
                this.closeOnes = closeOnes;
            }
            ApartmentAreaRange.areaChanged += CheckAgainstConstraints;
            WaitFrame = new WaitForEndOfFrame();
            var m = Orientation;
            var b = IsDualAspect;
        }

        /// <summary>
        /// Set the apartments which are next to it or on the same corridor side
        /// </summary>
        /// <param name="closeOnes">The list of apartments</param>
        public void SetCloseOnes(List<ApartmentUnity> closeOnes)
        {
            closeOnes = closeOnes.OrderBy(x => Vector3.Distance(transform.position, x.transform.position)).ToList();
            closeOnes = closeOnes.Where(x => x.ApartmentType != "SpecialApt" && x.activeCorridorIndex == activeCorridorIndex && x != this).ToList();//Mathf.Abs(x.transform.eulerAngles.y-transform.eulerAngles.y)<5).ToList();
            this.closeOnes = closeOnes;
            ProceduralBuildingManager.apartmentsChanged += RecalculateCloseOnes;
        }

        /// <summary>
        /// Returns the bounding box of the closest apartments (this one excluded)
        /// </summary>
        /// <returns>Unity Bounds</returns>
        public Bounds GetCloseOnesBounds()
        {
            Bounds bounds = closeOnes[0].editableMesh.meshExtrusion.GetComponent<Renderer>().bounds;
            for (int i = 1; i < closeOnes.Count; i++)
            {
                bounds.Encapsulate(closeOnes[i].editableMesh.meshExtrusion.GetComponent<Renderer>().bounds);
            }
            return bounds;
        }

        /// <summary>
        /// Returns the collider of the tower floor which encompasses all other apartments
        /// </summary>
        /// <returns>Collider</returns>
        public Collider GetCloseOnesCollider()
        {
            var tower = proceduralFloor as ProceduralTower;
            if (tower != null)
            {
                Vector3 size = Vector3.zero;
                var bnds = BrydenWoodUtils.CalculateBoundsXZ(tower.GetApartmentsCorners(this), tower.transform, out size);
                Vector3 centre = Vector3.zero;
                for (int i = 0; i < bnds.Length; i++)
                {
                    int nex = (i + 1) % bnds.Length;
                    centre += bnds[i];
                    //Gizmos.DrawSphere(bnds[i], 1.5f);
                }
                centre /= bnds.Length;
                return tower.SetBoxCollider(centre, size + new Vector3(0, 3, 0));
            }
            else return null;
        }

        /// <summary>
        /// Returns the statistics about the apartment
        /// </summary>
        /// <param name="level">The level to which it belongs</param>
        /// <param name="f2f">The floor to floor height</param>
        /// <param name="apartmentDepth">The depth of the apartment</param>
        /// <returns>STRING</returns>
        public string GetPerApartmentStats(int level, float f2f, float apartmentDepth)
        {
            //"OverallArea", "FloorArea", "FacadeArea", "NumberOfPeople", "Floor2Floor", "Level", "ApartmentDepth", "ApartmentWidth", "ApartmentType"

            List<string> data = new List<string>();
            data.Add(Area.ToString());
            if (Representation != null)
            {
                data.Add(Representation.GetComponent<ProceduralApartmentLayout>().NIA.ToString());
            }
            else
            {
                data.Add("0");
            }
            data.Add((GetFacadeLength() * f2f).ToString());
            data.Add(numberOfPeople.ToString());
            data.Add(f2f.ToString());
            data.Add(level.ToString());
            data.Add(apartmentDepth.ToString());
            data.Add((Area / apartmentDepth).ToString());
            data.Add(apartmentType);

            return string.Join(",", data.ToArray());
        }

        /// <summary>
        /// Recalculates the apartments which are next to it or the ones on the same corridor side
        /// </summary>
        public void RecalculateCloseOnes()
        {
            if (closeOnes != null && this != null)
            {
                try
                {
                    for (int i = 0; i < closeOnes.Count; i++)
                    {
                        if (closeOnes[i] == null)
                        {
                            closeOnes.RemoveAt(i);
                        }
                    }
                    closeOnes = closeOnes.OrderBy(x => Vector3.Distance(transform.position, x.transform.position)).ToList();
                }
                catch (Exception e)
                {
                    Debug.Log(e);
                }
            }
        }

        /// <summary>
        /// Combines this apartment with the next one (if this is an unresolved apartment)
        /// </summary>
        public void ResolveWithNext()
        {
            if (closeOnes.Count > 0)
            {
                ApartmentUnity nextOne = closeOnes[0];
                proceduralFloor.OnResolveApartmentWithNext(new List<BaseDesignData>() { nextOne, this });
            }
        }

        /// <summary>
        /// Returns the maximum module width for this apartment
        /// </summary>
        /// <returns>Float</returns>
        public float GetMaximumModuleWidth()
        {
            float maxWidth = float.MinValue;

            float[] fixedWidths;
            if (Standards.TaggedObject.FixedModuleWidths.TryGetValue(apartmentType, out fixedWidths))
            {
                float totalFixedWidth = 0;
                for (int i = 0; i < fixedWidths.Length; i++)
                {
                    if (fixedWidths[i] > maxWidth)
                        maxWidth = fixedWidths[i];
                    totalFixedWidth += fixedWidths[i];
                }
                float residualWidth = Width - totalFixedWidth;
                if (residualWidth > maxWidth)
                    maxWidth = residualWidth;
            }

            return maxWidth;
        }

        /// <summary>
        /// Returns the maximum span between line-loads in this apartment
        /// </summary>
        /// <returns>Float</returns>
        public float GetMaximumLineLoadSpan()
        {
            float maxSpan = float.MinValue;

            float[][] fixedWidths;
            if (Standards.TaggedObject.FixedLineLoadsSpans.TryGetValue(apartmentType, out fixedWidths))
            {
                for (int i = 0; i < fixedWidths.Length; i++)
                {
                    float totalFixedWidth = 0;
                    for (int j = 0; j < fixedWidths[i].Length; j++)
                    {
                        if (fixedWidths[i][j] > maxSpan)
                            maxSpan = fixedWidths[i][j];
                        totalFixedWidth += fixedWidths[i][j];
                    }
                    float residualWidth = Width - totalFixedWidth;
                    if (residualWidth > maxSpan)
                        maxSpan = residualWidth;
                }
            }

            return maxSpan;
        }


        /// <summary>
        /// Returns the maximum panel length for this apartment
        /// </summary>
        /// <returns>Float</returns>
        public float GetMaximumPanelLength()
        {
            float maxWidth = float.MinValue;
            if (Width > proceduralFloor.building.buildingManager.proceduralApartment.maxPanelWidth)
            {
                float[] fixedWidths;
                if (Standards.TaggedObject.FixedModuleWidths.TryGetValue(apartmentType, out fixedWidths))
                {
                    float totalFixedWidth = 0;
                    for (int i = 0; i < fixedWidths.Length; i++)
                    {
                        if (fixedWidths[i] > maxWidth)
                            maxWidth = fixedWidths[i];
                        totalFixedWidth += fixedWidths[i];
                    }
                    float residualWidth = Width - totalFixedWidth;
                    if (residualWidth > maxWidth)
                        maxWidth = residualWidth;
                }
            }
            else
            {
                if (Width > maxWidth)
                {
                    maxWidth = Width;
                }
            }
            return maxWidth;
        }

        /// <summary>
        /// Redistributes the length of the corridor side equally to all apartments on it (if this is an unresolved apartment)
        /// </summary>
        /// <returns>IEnumerator</returns>
        public IEnumerator AutoResolveAndRetain()
        {
            if (!isUnresolvable)
            {
                if (closeOnes != null && closeOnes.Count > 0)
                {
                    Vector3 corDir = editableMesh.vertices[3].currentPosition - closeOnes.Last().editableMesh.vertices[0].currentPosition;
                    if (corDir.magnitude<0.5)
                    {
                        corDir = editableMesh.vertices[0].currentPosition - closeOnes.Last().editableMesh.vertices[3].currentPosition;
                    }
                    var angle = Vector3.Angle(corDir, (editableMesh.vertices[0].currentPosition - editableMesh.vertices[3].currentPosition));
                    Vector3 start = closeOnes.Last().editableMesh.vertices[3].currentPosition;
                    Vector3 end = editableMesh.vertices[0].currentPosition;
                    if (angle > 90)
                    {
                        start = closeOnes.Last().editableMesh.vertices[0].currentPosition;
                        end = editableMesh.vertices[3].currentPosition;
                        Vector3 dir = (end - start).normalized;
                        float length = Vector3.Distance(start, end);
                        float newWidth = length / (closeOnes.Count + 1);

                        for (int i = closeOnes.Count - 1; i >= 0; i--)
                        {

                            if (closeOnes[i] != null)
                            {
                                closeOnes[i].transform.position = start + dir * newWidth;
                                Vector3 moveVector = dir * newWidth;
                                Vector3 depth = closeOnes[i].editableMesh.vertices[2].currentPosition - closeOnes[i].editableMesh.vertices[3].currentPosition;

                                closeOnes[i].editableMesh.vertices[0].SetPosition(start);
                                closeOnes[i].editableMesh.vertices[1].SetPosition(closeOnes[i].editableMesh.vertices[0].currentPosition + depth);
                                for (int j = 0; j < closeOnes[i].editableMesh.vertices.Count; j++)
                                {
                                    closeOnes[i].editableMesh.vertices[j].UpdatePosition();
                                }
                                start += (moveVector);
                            }
                        }

                        transform.position = start + dir * newWidth;
                        Vector3 move = dir * newWidth;
                        Vector3 m_depth = editableMesh.vertices[2].currentPosition - editableMesh.vertices[3].currentPosition;

                        editableMesh.vertices[0].SetPosition(start);
                        editableMesh.vertices[1].SetPosition(editableMesh.vertices[0].currentPosition + m_depth);
                        for (int i = 0; i < editableMesh.vertices.Count; i++)
                        {
                            editableMesh.vertices[i].UpdatePosition();
                        }
                        start += move;

                        Selector.TaggedObject.ClearSelect();
                    }
                    else
                    {
                        Vector3 dir = (end - start).normalized;
                        float length = Vector3.Distance(start, end);
                        float newWidth = length / (closeOnes.Count + 1);

                        for (int i = closeOnes.Count - 1; i >= 0; i--)
                        {

                            if (closeOnes[i] != null)
                            {
                                closeOnes[i].transform.position = start;
                                Vector3 moveVector = dir * newWidth;
                                Vector3 depth = closeOnes[i].editableMesh.vertices[2].currentPosition - closeOnes[i].editableMesh.vertices[3].currentPosition;

                                closeOnes[i].editableMesh.vertices[0].SetPosition(closeOnes[i].transform.position + moveVector);
                                closeOnes[i].editableMesh.vertices[1].SetPosition(closeOnes[i].editableMesh.vertices[0].currentPosition + depth);
                                for (int j = 0; j < closeOnes[i].editableMesh.vertices.Count; j++)
                                {
                                    closeOnes[i].editableMesh.vertices[j].UpdatePosition();
                                }
                                start += (moveVector);
                            }
                        }

                        transform.position = start;
                        Vector3 move = dir * newWidth;
                        Vector3 m_depth = editableMesh.vertices[2].currentPosition - editableMesh.vertices[3].currentPosition;

                        editableMesh.vertices[0].SetPosition(transform.position + move);
                        editableMesh.vertices[1].SetPosition(editableMesh.vertices[0].currentPosition + m_depth);
                        for (int i = 0; i < editableMesh.vertices.Count; i++)
                        {
                            editableMesh.vertices[i].UpdatePosition();
                        }
                        start += move;

                        Selector.TaggedObject.ClearSelect();
                    }

                }

                float area = 0;
                if (editableMesh != null)
                    area = editableMesh.area;
                else if (polygon != null)
                    area = polygon.Area;

                var sortedAreas = Standards.TaggedObject.ApartmentTypesMinimumSizes.OrderBy(x => x.Value).ToList();
                string type = String.Empty;
                for (int i = 0; i < sortedAreas.Count; i++)
                {
                    if (sortedAreas[i].Key != "SpecialApt")
                    {
                        if (area >= sortedAreas[i].Value)
                            type = sortedAreas[i].Key;
                    }
                }

                if (!String.IsNullOrEmpty(type))
                {
                    OnApartmentTypeChanged(type, isM3);
                }
                else
                {
                    OnApartmentTypeChanged("1b2p", false);
                }

                proceduralFloor.RequestOverallUpdate();
            }
            yield return WaitFrame;
        }

        /// <summary>
        /// Distributes the width of this apartment to its close ones and then removes this apartment (if this is an unresolved apartment)
        /// </summary>
        /// <returns>IEnumerator</returns>
        public IEnumerator ResolveWithAll()
        {
            if (!isUnresolvable)
            {
                if (closeOnes != null && closeOnes.Count > 0)
                {
                    ResolveAction resolveAction = new ResolveAction();

                    resolveAction.aptNames = new string[closeOnes.Count + 1];
                    resolveAction.aptTypes = new string[closeOnes.Count + 1];
                    resolveAction.aptVertices = new Vector3[closeOnes.Count + 1][];
                    resolveAction.aptCorridorIndex = new int[closeOnes.Count + 1];
                    resolveAction.aptIndex = new int[closeOnes.Count + 1];
                    resolveAction.flipped = new bool[closeOnes.Count + 1];
                    resolveAction.aptHasCorridor = new bool[closeOnes.Count + 1];

                    bool normalDirection = true;
                    Vector3 start = closeOnes.Last().editableMesh.vertices[3].currentPosition;
                    Vector3 end = editableMesh.vertices[0].currentPosition;
                    Vector3 helper = editableMesh[0].currentPosition - editableMesh[3].currentPosition;
                    float angle = Vector3.Angle(end - start, helper);
                    if (angle > 90 || Vector3.Distance(end, start) < 0.001f)
                    {
                        normalDirection = false;
                        start = closeOnes.Last().editableMesh.vertices[0].currentPosition;
                        end = editableMesh[0].currentPosition;
                    }
                    Vector3 dir = (end - start).normalized;
                    float length = Vector3.Distance(editableMesh[0].currentPosition, editableMesh[3].currentPosition);
                    float extraWidth = length / closeOnes.Count;

                    for (int i = closeOnes.Count - 1; i >= 0; i--)
                    {
                        resolveAction.aptNames[(closeOnes.Count - 1) - i] = closeOnes[i].gameObject.name;
                        resolveAction.aptTypes[(closeOnes.Count - 1) - i] = closeOnes[i].ApartmentType;
                        resolveAction.aptHasCorridor[(closeOnes.Count - 1) - i] = closeOnes[i].HasCorridor;
                        resolveAction.aptCorridorIndex[(closeOnes.Count - 1) - i] = closeOnes[i].activeCorridorIndex;
                        resolveAction.aptIndex[(closeOnes.Count - 1) - i] = closeOnes[i].Id;
                        resolveAction.flipped[(closeOnes.Count - 1) - i] = closeOnes[i].flipped;
                        resolveAction.aptVertices[(closeOnes.Count - 1) - i] = new Vector3[closeOnes[i].editableMesh.vertices.Count];
                        for (int j = 0; j < resolveAction.aptVertices[(closeOnes.Count - 1) - i].Length; j++)
                        {
                            resolveAction.aptVertices[(closeOnes.Count - 1) - i][j] = closeOnes[i].editableMesh.vertices[j].currentPosition;
                        }

                        if (closeOnes[i] != null)
                        {
                            if (normalDirection)
                            {
                                closeOnes[i].transform.position = start;
                                float newWidth = closeOnes[i].Width + extraWidth;
                                Vector3 moveVector = dir * newWidth;
                                Vector3 depth = closeOnes[i].editableMesh.vertices[2].currentPosition - closeOnes[i].editableMesh.vertices[3].currentPosition;

                                closeOnes[i].editableMesh.vertices[0].SetPosition(closeOnes[i].transform.position + moveVector);
                                closeOnes[i].editableMesh.vertices[1].SetPosition(closeOnes[i].editableMesh.vertices[0].currentPosition + depth);
                                for (int j = 0; j < closeOnes[i].editableMesh.vertices.Count; j++)
                                {
                                    closeOnes[i].editableMesh.vertices[j].UpdatePosition();
                                }

                                closeOnes[i].CheckAgainstConstraints();
                                start += (moveVector);
                            }
                            else
                            {
                                float newWidth = closeOnes[i].Width + extraWidth;
                                Vector3 moveVector = dir * newWidth;
                                Vector3 depth = closeOnes[i].editableMesh.vertices[2].currentPosition - closeOnes[i].editableMesh.vertices[3].currentPosition;

                                closeOnes[i].transform.position = start + moveVector;

                                closeOnes[i].editableMesh.vertices[0].SetPosition(start);
                                closeOnes[i].editableMesh.vertices[1].SetPosition(closeOnes[i].editableMesh.vertices[0].currentPosition + depth);
                                for (int j = 0; j < closeOnes[i].editableMesh.vertices.Count; j++)
                                {
                                    closeOnes[i].editableMesh.vertices[j].UpdatePosition();
                                }

                                Debug.DrawLine(start, start + moveVector, Color.green, 50000, false);

                                closeOnes[i].CheckAgainstConstraints();
                                start += (moveVector);
                            }
                        }
                    }

                    resolveAction.aptNames[closeOnes.Count] = gameObject.name;
                    resolveAction.aptTypes[closeOnes.Count] = apartmentType;
                    resolveAction.aptCorridorIndex[closeOnes.Count] = activeCorridorIndex;
                    resolveAction.aptIndex[closeOnes.Count] = id;
                    resolveAction.flipped[closeOnes.Count] = flipped;
                    resolveAction.aptVertices[closeOnes.Count] = new Vector3[editableMesh.vertices.Count];
                    for (int j = 0; j < resolveAction.aptVertices[closeOnes.Count].Length; j++)
                    {
                        resolveAction.aptVertices[closeOnes.Count][j] = editableMesh.vertices[j].currentPosition;
                    }
                    proceduralFloor.SetResolveAction(resolveAction);
                    Selector.TaggedObject.ClearSelect();
                    Destroy(gameObject);
                    resolveAction.resultingApts = closeOnes;
                }
                proceduralFloor.RequestOverallUpdate();
            }
            yield return StartCoroutine(proceduralFloor.OnApartmentsChangedCoroutine());
        }

        /// <summary>
        /// Distributes the width of this apartment to its close ones and then removes this apartment (if this is an unresolved apartment)
        /// </summary>
        /// <returns>IEnumerator</returns>
        public IEnumerator AutoResolveWithAll()
        {
            if (closeOnes != null && closeOnes.Count > 0)
            {
                ResolveAction resolveAction = new ResolveAction();

                resolveAction.aptNames = new string[closeOnes.Count + 1];
                resolveAction.aptTypes = new string[closeOnes.Count + 1];
                resolveAction.flipped = new bool[closeOnes.Count + 1];
                resolveAction.aptVertices = new Vector3[closeOnes.Count + 1][];
                resolveAction.aptCorridorIndex = new int[closeOnes.Count + 1];
                resolveAction.aptIndex = new int[closeOnes.Count + 1];
                resolveAction.aptHasCorridor = new bool[closeOnes.Count + 1];
                resolveAction.floor = proceduralFloor;

                bool normalDirection = true;
                Vector3 start = closeOnes.Last().editableMesh.vertices[3].currentPosition;
                Vector3 end = editableMesh.vertices[0].currentPosition;
                Vector3 helper = editableMesh[0].currentPosition - editableMesh[3].currentPosition;
                float angle = Vector3.Angle(end - start, helper);
                if (angle > 90 || Vector3.Distance(end, start) < 0.001f)
                {
                    normalDirection = false;
                    start = closeOnes.Last().editableMesh.vertices[0].currentPosition;
                    end = editableMesh.vertices[3].currentPosition;
                }
                Vector3 dir = (end - start).normalized;
                float length = Vector3.Distance(editableMesh[3].currentPosition, editableMesh[0].currentPosition);
                float extraWidth = length / closeOnes.Count;

                for (int i = closeOnes.Count - 1; i >= 0; i--)
                {
                    resolveAction.aptNames[(closeOnes.Count - 1) - i] = closeOnes[i].gameObject.name;
                    resolveAction.aptTypes[(closeOnes.Count - 1) - i] = closeOnes[i].ApartmentType;
                    resolveAction.flipped[(closeOnes.Count - 1) - i] = closeOnes[i].flipped;
                    resolveAction.aptHasCorridor[(closeOnes.Count - 1) - i] = closeOnes[i].HasCorridor;
                    resolveAction.aptCorridorIndex[(closeOnes.Count - 1) - i] = closeOnes[i].activeCorridorIndex;
                    resolveAction.aptIndex[(closeOnes.Count - 1) - i] = closeOnes[i].Id;
                    resolveAction.aptVertices[(closeOnes.Count - 1) - i] = new Vector3[closeOnes[i].editableMesh.vertices.Count];

                    for (int j = 0; j < resolveAction.aptVertices[(closeOnes.Count - 1) - i].Length; j++)
                    {
                        resolveAction.aptVertices[(closeOnes.Count - 1) - i][j] = closeOnes[i].editableMesh.vertices[j].currentPosition;
                    }

                    if (closeOnes[i] != null)
                    {

                        if (normalDirection)
                        {
                            closeOnes[i].transform.position = start;
                            float newWidth = closeOnes[i].Width + extraWidth;
                            Vector3 moveVector = dir * newWidth;
                            Vector3 depth = closeOnes[i].editableMesh.vertices[2].currentPosition - closeOnes[i].editableMesh.vertices[3].currentPosition;
                            closeOnes[i].editableMesh.vertices[0].SetPosition(closeOnes[i].transform.position + moveVector);
                            closeOnes[i].editableMesh.vertices[1].SetPosition(closeOnes[i].editableMesh.vertices[0].currentPosition + depth);
                            for (int j = 0; j < closeOnes[i].editableMesh.vertices.Count; j++)
                            {
                                closeOnes[i].editableMesh.vertices[j].UpdatePosition();
                            }
                            closeOnes[i].CheckAgainstConstraints();
                            start += (moveVector);
                        }
                        else
                        {
                            float newWidth = closeOnes[i].Width + extraWidth;
                            Vector3 moveVector = dir * newWidth;
                            Vector3 depth = closeOnes[i].editableMesh.vertices[2].currentPosition - closeOnes[i].editableMesh.vertices[3].currentPosition;
                            Debug.DrawLine(start, start + moveVector);
                            closeOnes[i].transform.position = start + moveVector;

                            closeOnes[i].editableMesh.vertices[0].SetPosition(start);
                            closeOnes[i].editableMesh.vertices[1].SetPosition(closeOnes[i].editableMesh.vertices[0].currentPosition + depth);
                            for (int j = 0; j < closeOnes[i].editableMesh.vertices.Count; j++)
                            {
                                closeOnes[i].editableMesh.vertices[j].UpdatePosition();
                            }
                            closeOnes[i].CheckAgainstConstraints();
                            start += (moveVector);
                        }
                    }
                }

                resolveAction.aptNames[closeOnes.Count] = gameObject.name;
                resolveAction.aptTypes[closeOnes.Count] = apartmentType;
                resolveAction.aptCorridorIndex[closeOnes.Count] = activeCorridorIndex;
                resolveAction.aptIndex[closeOnes.Count] = id;
                resolveAction.flipped[closeOnes.Count] = flipped;
                resolveAction.aptVertices[closeOnes.Count] = new Vector3[editableMesh.vertices.Count];
                for (int j = 0; j < resolveAction.aptVertices[closeOnes.Count].Length; j++)
                {
                    resolveAction.aptVertices[closeOnes.Count][j] = editableMesh.vertices[j].currentPosition;
                }
                //buildingManager.SetResolveAction(resolveAction);
                proceduralFloor.SetResolveAction(resolveAction);
                Selector.TaggedObject.ClearSelect();
                Destroy(gameObject);
                resolveAction.resultingApts = closeOnes;
            }
            //StartCoroutine(buildingManager.OverallUpdate());
            proceduralFloor.RequestOverallUpdate();
            yield return WaitFrame;
        }

        /// <summary>
        /// Called when the apartment type has changed
        /// </summary>
        /// <param name="type">The new type</param>
        /// <param name="isM3">Whether this apartment is M3 compliant</param>
        public void OnApartmentTypeChanged(string type, bool isM3)
        {
            if (type != apartmentType || this.isM3 != isM3)
            {
                this.isM3 = isM3;
                if (ProceduralFloor as ProceduralMansion != null)
                {
                    (ProceduralFloor as ProceduralMansion).SetM3Apartment(this, isM3);
                    return;
                }
                if (ProceduralFloor as ProceduralTower != null)
                {
                    (ProceduralFloor as ProceduralTower).SetM3Apartment(this, isM3);
                    return;
                }
                var str = gameObject.name.Split('_');
                str[str.Length - 1] = type;
                gameObject.name = string.Join("_", str);
                apartmentType = type;
                if (polygon != null)
                    polygon.SetTypeColor(Standards.TaggedObject.ApartmentTypesColours[type]);
                if (editableMesh != null)
                    editableMesh.SetTypeColor(Standards.TaggedObject.ApartmentTypesColours[type]);
                OnGeometryUpdated();
                CheckAgainstConstraints();
                proceduralFloor.building.buildingManager.OnApartmentChanged();
                proceduralFloor.RequestOverallUpdate();
                OnSelected();
            }
        }

        /// <summary>
        /// Sets the outline polygon of the apartment
        /// </summary>
        /// <param name="polygon">The new polygon</param>
        public override void SetPolygon(Polygon polygon)
        {
            this.polygon = polygon;
            //polygon.updated += OnPolygonUpdated;
            polygon.updated.AddListener(OnPolygonUpdated);
            polygon.selected += Select;
            polygon.deselected += DeSelect;
            polygon.extrusion.selected += Select;
            polygon.extrusion.deselected += DeSelect;
            OnAreaChanged();
            Color m_color;
            if (Standards.TaggedObject.ApartmentTypesColours.TryGetValue(apartmentType, out m_color))
            {
                polygon.SetTypeColor(m_color);
            }
            else
            {
                polygon.SetTypeColor(Color.gray);
            }
        }

        /// <summary>
        /// Checks which apartments are its direct neighbours
        /// </summary>
        public void CheckNeighbours()
        {
            Vector3 origin = editableMesh.meshExtrusion.GetComponent<MeshCollider>().bounds.center;
            Ray r = new Ray(origin, transform.right);
            RaycastHit hit;
            if (Physics.Raycast(r, out hit, Width * 0.5f + 0.1f))
            {
                if (hit.collider.transform.parent.parent.GetComponent<ApartmentUnity>() != null)
                {
                    hasRight = true;
                    hasCoreRight = false;
                    var delta = Math.Round(hit.collider.transform.parent.parent.eulerAngles.y - transform.eulerAngles.y);
                    if (Math.Abs(delta) > 2)
                    {

                        unresolvableOnRight = true;
                        editableMesh.constrainUnresolvedR = true;
                    }
                }
                else
                {
                    if (hit.collider.gameObject.name.Contains("Core"))
                    {
                        unresolvableOnRight = true;
                        editableMesh.constrainUnresolvedR = true;
                        hasCoreRight = true;
                    }
                    else
                    {
                        hasCoreRight = false;
                        hasRight = false;
                        unresolvableOnRight = true;
                        editableMesh.constrainUnresolvedR = true;
                    }
                }
            }
            else
            {
                hasCoreRight = false;
                hasRight = false;
                unresolvableOnRight = true;
                editableMesh.constrainUnresolvedR = true;
            }

            Debug.DrawLine(origin, r.GetPoint(Width / 2), Color.blue, 1000);

            r = new Ray(origin, -transform.right);
            if (Physics.Raycast(r, out hit, Width * 0.5f + 0.1f))
            {
                if (hit.collider.transform.parent.parent.GetComponent<ApartmentUnity>() != null)
                {
                    hasLeft = true;
                    hasCoreLeft = false;
                    var delta = Math.Round(hit.collider.transform.parent.parent.eulerAngles.y - transform.eulerAngles.y);
                    if (Math.Abs(delta) > 2)
                    {
                        unresolvableOnLeft = true;
                        editableMesh.constrainUnresolvedL = true;
                    }
                }
                else
                {
                    if (hit.collider.gameObject.name.Contains("Core"))
                    {
                        unresolvableOnLeft = true;
                        editableMesh.constrainUnresolvedL = true;
                        hasCoreLeft = true;
                    }
                    else
                    {
                        hasCoreLeft = false;
                        hasLeft = false;
                        unresolvableOnLeft = true;
                        editableMesh.constrainUnresolvedL = true;
                    }
                }
            }
            else
            {
                hasLeft = false;
                hasCoreLeft = false;
                unresolvableOnLeft = true;
                editableMesh.constrainUnresolvedL = true;
            }

            if (flipped)
            {
                var temp = hasRight;
                hasRight = hasLeft;
                hasLeft = temp;
            }

            Debug.DrawLine(origin, r.GetPoint(Width / 2), Color.red, 1000);
        }

        /// <summary>
        /// Populates the apartment with its internal layout (rooms)
        /// </summary>
        /// <param name="representationPrefab">The prefab to be used for the population (ProceduralApartmentLayout)</param>
        /// <param name="depth">The depth of the apartment</param>
        /// <param name="representationParent">The parent object for the representation</param>
        /// <param name="keepMesh">Whether to keep the outter mesh active</param>
        public void PopulateInternalLayout(GameObject representationPrefab, float depth, Transform representationParent, bool keepMesh = false)
        {
            if (editableMesh.vertices.Count == 4 && Area >= Standards.TaggedObject.ApartmentTypesMinimumSizes[apartmentType] && IsRectangular)
            {
                Representation = Instantiate(representationPrefab);
                var apartmentLayout = Representation.GetComponent<ProceduralApartmentLayout>();
                apartmentLayout.m3 = isM3;
                apartmentLayout.buildingType = proceduralFloor.building.buildingType;
                if (ProceduralFloor.typology == LinearTypology.DeckAccess)
                {
                    apartmentLayout.SingleLoaded = true;
                }
                float wallWidth = apartmentLayout.wallWidth;
                apartmentLayout.corridorWidth = proceduralFloor.corridorWidth;
                apartmentLayout.hasCorridor = HasCorridor;
                //apartmentLayout.dbVal = aVal;
                //apartmentLayout.pbVal = bVal;
                apartmentLayout.width = Width - (Standards.TaggedObject.ConstructionFeatures["PartyWall"]);
                apartmentLayout.height = Depth - (Standards.TaggedObject.ConstructionFeatures["CorridorWall"]);//proceduralFloor.apartmentDepth;
                apartmentLayout.apartmentType = apartmentType;
                apartmentLayout.hasLeft = hasLeft || hasCoreLeft;
                apartmentLayout.hasRight = hasRight || hasCoreRight;
                List<bool> rooms = Standards.TaggedObject.ApartmentTypeRooms[apartmentType].ToList();
                if (ApartmentType != "SpecialApt")
                {
                    if (proceduralFloor.building.buildingType == BuildingType.Mansion)
                    {
                        if (isM3)
                            apartmentLayout.SetRooms(Standards.TaggedObject.aptLayouts[BuildingType.Mansion][apartmentType + "_M3"]);
                        else
                            apartmentLayout.SetRooms(Standards.TaggedObject.aptLayouts[BuildingType.Mansion][apartmentType + "_M2"]);
                    }
                    else
                    {
                        if (isM3)
                            apartmentLayout.SetRooms(Standards.TaggedObject.aptLayouts[BuildingType.Linear][apartmentType + "_M3"]);
                        else
                            apartmentLayout.SetRooms(Standards.TaggedObject.aptLayouts[BuildingType.Linear][apartmentType + "_M2"]);
                    }
                }
                else
                {
                    apartmentLayout.SetRooms(true);
                }
                apartmentLayout.totalHeight = proceduralFloor.floor2floor;
                apartmentLayout.modulesScale = 1;
                apartmentLayout.Initialize(false, isExterior);
                apartmentLayout.SetLineLoads();
                apartmentLayout.CombineRoomsMeshes(apartmentType);
                Representation.transform.SetParent(transform);
                Representation.transform.localEulerAngles = Vector3.zero;
                Representation.transform.localPosition = editableMesh.vertices[3].currentLocalPosition;
                Representation.transform.SetParent(representationParent);
                apartmentLayout.CheckIfPlatformsCollision(ProceduralFloor.platformsParent);
                apartmentLayout.transform.GetChild(4).gameObject.SetActive(false);
                if (apartmentLayout.verticalModulesParent != null)
                    apartmentLayout.verticalModulesParent.gameObject.SetActive(false);
                if (apartmentLayout.panelsParent != null)
                    apartmentLayout.panelsParent.gameObject.SetActive(false);
                if (apartmentLayout.lineLoadsParent != null)
                    apartmentLayout.lineLoadsParent.gameObject.SetActive(false);

                if (flipped)
                {
                    Representation.transform.position = editableMesh.vertices[0].currentPosition;
                    Representation.transform.localScale = new Vector3(-1, 1, 1);
                }
                editableMesh.gameObject.SetActive(keepMesh);
                editableMesh.meshExtrusion.transform.localScale = new Vector3(1, 1, 1.005f);
            }
            else
            {
                Debug.Log("Area Problem!");
                
                //GameObject problem = GameObject.FindGameObjectWithTag("error");
                //problem.transform.GetChild(0).gameObject.SetActive(true);
               
            }
        }

        /// <summary>
        /// Flips the internal layout (rooms)
        /// </summary>
        /// <param name="flip">Whether to flip the interior</param>
        public void FlipInterior(bool flip)
        {
            flipped = flip;
            CheckNeighbours();
            if (Representation != null)
            {
                Representation.GetComponent<ProceduralApartmentLayout>().hasLeft = hasLeft || hasCoreLeft;
                Representation.GetComponent<ProceduralApartmentLayout>().hasRight = hasRight || hasCoreRight;

                if (flipped)
                {
                    Representation.transform.position = editableMesh.vertices[0].currentPosition;
                    Representation.transform.localScale = new Vector3(-1, 1, 1);
                }
                else
                {
                    Representation.transform.position = editableMesh.vertices[3].currentPosition;
                    Representation.transform.localScale = new Vector3(1, 1, 1);
                }
            }
        }

        /// <summary>
        /// Changes the axes on which the apartment can be extended
        /// </summary>
        /// <param name="alter">Whether they should change</param>
        /// <param name="flippedInterior">Whether the interior has been flipped</param>
        /// <param name="constrainedSide">Whether there is a constrained side</param>
        public void AlterExtensionAxes(bool alter, bool flippedInterior, bool constrainedSide)
        {
            if (editableMesh != null)
            {
                editableMesh.alteredAxis = alter;
                editableMesh.flippedInterior = flippedInterior;
                editableMesh.constrainedSide = constrainedSide;
            }
        }

        /// <summary>
        /// Sets the current manufacturing system to be displayed
        /// </summary>
        /// <param name="manufacturingSystem">The new manufacturing system</param>
        public void SetManufacturingSystem(ManufacturingSystem manufacturingSystem)
        {
            if (Representation != null)
            {
                var apartmentLayout = Representation.GetComponent<ProceduralApartmentLayout>();
                try
                {
                    switch (manufacturingSystem)
                    {
                        case ManufacturingSystem.Off:
                            apartmentLayout.verticalModulesParent.gameObject.SetActive(false);
                            apartmentLayout.panelsParent.gameObject.SetActive(false);
                            apartmentLayout.lineLoadsParent.gameObject.SetActive(false);
                            Representation.transform.GetChild(4).gameObject.SetActive(true);
                            Representation.transform.GetChild(5).gameObject.SetActive(true);
                            editableMesh.SetTypeMaterialAsMain();
                            if (proceduralFloor.building.buildingManager.previewMode == PreviewMode.Floors)
                            {
                                editableMesh.TypeHighlight(false);
                                editableMesh.gameObject.SetActive(true);
                            }
                            else
                            {
                                editableMesh.TypeHighlight(false);
                                editableMesh.gameObject.SetActive(false);
                            }
                            break;
                        case ManufacturingSystem.VolumetricModules:
                            apartmentLayout.verticalModulesParent.gameObject.SetActive(true);
                            apartmentLayout.panelsParent.gameObject.SetActive(false);
                            apartmentLayout.lineLoadsParent.gameObject.SetActive(false);
                            Representation.transform.GetChild(4).gameObject.SetActive(false);
                            Representation.transform.GetChild(5).gameObject.SetActive(false);
                            editableMesh.SetTypeMaterialAsMain();
                            editableMesh.TypeHighlight(false);
                            editableMesh.gameObject.SetActive(false);
                            break;
                        case ManufacturingSystem.Panels:
                            apartmentLayout.verticalModulesParent.gameObject.SetActive(false);
                            apartmentLayout.panelsParent.gameObject.SetActive(true);
                            apartmentLayout.lineLoadsParent.gameObject.SetActive(false);
                            Representation.transform.GetChild(4).gameObject.SetActive(false);
                            Representation.transform.GetChild(5).gameObject.SetActive(false);
                            editableMesh.originalMaterial = proceduralFloor.originalMaterial;
                            editableMesh.TypeHighlight(false);
                            editableMesh.gameObject.SetActive(false);
                            break;
                        case ManufacturingSystem.LineLoads:
                            apartmentLayout.verticalModulesParent.gameObject.SetActive(false);
                            apartmentLayout.panelsParent.gameObject.SetActive(false);
                            apartmentLayout.lineLoadsParent.gameObject.SetActive(true);
                            Representation.transform.GetChild(4).gameObject.SetActive(false);
                            Representation.transform.GetChild(5).gameObject.SetActive(false);
                            editableMesh.originalMaterial = proceduralFloor.originalMaterial;
                            editableMesh.TypeHighlight(false);
                            editableMesh.gameObject.SetActive(true);
                            break;
                        case ManufacturingSystem.Platforms:
                            apartmentLayout.verticalModulesParent.gameObject.SetActive(false);
                            apartmentLayout.panelsParent.gameObject.SetActive(false);
                            apartmentLayout.lineLoadsParent.gameObject.SetActive(false);
                            Representation.transform.GetChild(4).gameObject.SetActive(true);
                            Representation.transform.GetChild(5).gameObject.SetActive(true);
                            editableMesh.SetTypeMaterialAsMain();
                            if (proceduralFloor.building.buildingManager.previewMode == PreviewMode.Floors)
                            {
                                editableMesh.TypeHighlight(false);
                                editableMesh.gameObject.SetActive(true);
                            }
                            else
                            {
                                editableMesh.TypeHighlight(false);
                                editableMesh.gameObject.SetActive(false);
                            }
                            break;
                    }
                }
                catch (Exception e)
                {
                    UnityEngine.Debug.Log(e);
                }
            }
        }

        /// <summary>
        /// Returns the numbers and statistics regarding the Vertical Modules
        /// </summary>
        /// <returns>Dictionary</returns>
        public Dictionary<string, int> GetVerticalModulesNumbers()
        {
            if (Representation != null)
            {
                Dictionary<string, int> modulesNumbers = new Dictionary<string, int>();

                for (int i = 0; i < Representation.GetComponent<ProceduralApartmentLayout>().verticalModules.Count; i++)
                {
                    var module = Representation.GetComponent<ProceduralApartmentLayout>().verticalModules[i];
                    if (modulesNumbers.Keys.Contains(module.name))
                    {
                        modulesNumbers[module.name]++;
                    }
                    else
                    {
                        modulesNumbers.Add(module.name, 1);
                    }
                }
                return modulesNumbers;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Toggles the internal layout representation
        /// </summary>
        /// <param name="show">Whether the representation should be on or off</param>
        public void ToggleRepresentation(bool show)
        {
            if (Representation != null)
            {
                Representation.SetActive(show);
                editableMesh.gameObject.SetActive(!show);
                if (!show)
                {
                    editableMesh.meshExtrusion.transform.localScale = Vector3.one;
                }
            }
        }

        /// <summary>
        /// Sets the editable mesh (outline) of the apartment
        /// </summary>
        /// <param name="editableMesh">The outline of the apartment</param>
        public override void SetEditableMesh(EditableMesh editableMesh)
        {
            this.editableMesh = editableMesh;
            editableMesh.updated += OnEditableMeshUpdated;
            editableMesh.stoppedEditing += OnMeshStoppedEditing;
            editableMesh.selected += Select;
            editableMesh.deselected += DeSelect;
            editableMesh.GetExtrusion();
            if (editableMesh.GetComponent<MeshRenderer>().material.shader == Shader.Find("Standard"))
            {
                editableMesh.GetComponent<MeshRenderer>().material.shader = Shader.Find("MK/Toon/Free");
                editableMesh.GetComponent<MeshRenderer>().material.SetFloat("_ShadowIntensity", 0.65f);
            }
            if (editableMesh.meshExtrusion != null)
            {
                editableMesh.meshExtrusion.selected += Select;
                editableMesh.meshExtrusion.deselected += DeSelect;
            }
            OnAreaChanged();
            editableMesh.SetTypeMaterialAsMain();
        }

        /// <summary>
        /// Sets a new geometry for the apartment
        /// </summary>
        /// <param name="newVerts">The new mesh vertices</param>
        public void SetGeometry(List<MeshVertex> newVerts)
        {
            editableMesh.RemoveListeners();
            transform.position = newVerts.Last().currentPosition;
            Vector2[] verts = new Vector2[newVerts.Count];
            Vector3[] vert3D = new Vector3[newVerts.Count];
            for (int i = 0; i < verts.Length; i++)
            {
                newVerts[i].transform.SetParent(editableMesh.transform);
                newVerts[i].transform.SetSiblingIndex(i);
                newVerts[i].gameObject.SetActive(true);
                newVerts[i].index = i;
                newVerts[i].UpdatePosition();
                newVerts[i].gameObject.name = "MeshVertex_" + i;
                newVerts[i].gameObject.SetActive(true);
                vert3D[i] = newVerts[i].currentPosition + new Vector3(0, 0.01f, 0);
                vert3D[i] = transform.InverseTransformPoint(vert3D[i]);
                verts[i] = new Vector2(vert3D[i].x, vert3D[i].z);
            }

            int[] tris;
            if (verts.Length == 4)
            {
                tris = new int[] { 0, 2, 1, 0, 3, 2 };
            }
            else
            {
                var triang = new Sebastian.Geometry.Triangulator(new Sebastian.Geometry.Polygon(verts));// new TriangulatorSimple(verts);
                tris = triang.Triangulate();
            }

            Mesh mesh = new Mesh();
            mesh.vertices = vert3D;
            int[] triangles;
            mesh.triangles = tris;
            triangles = tris;
            editableMesh.mesh = mesh;
            editableMesh.SetVertices(newVerts);
            editableMesh.UpdateMeshGeometry();
            editableMesh.CalculateEdges();
            editableMesh.SetExtrusionHeight(proceduralFloor.floor2floor);
            editableMesh.OnUpdated();
            CheckForUpgrade();
            //StartCoroutine(buildingManager.OverallUpdate());
            proceduralFloor.RequestOverallUpdate();
            CheckAgainstConstraints();
        }

        /// <summary>
        /// Destroys the apartment
        /// </summary>
        /// <param name="destroyVertices">Whether the apartment vertices should be destroyed</param>
        public void DestroyApartment(bool destroyVertices = false)
        {
            try
            {
                //apartment.areaChanged -= OnAreaChanged;
                if (proceduralFloor != null)
                {
                    proceduralFloor.apartments.Remove(this);
                }
                ApartmentAreaRange.areaChanged -= CheckAgainstConstraints;
                editableMesh.DestroyMesh(destroyVertices);
                Destroy(gameObject);

            }
            catch (Exception e)
            {
                Debug.Log(e);
            }
        }

        /// <summary>
        /// Returns a notifcations regarding the errors of the apartment
        /// </summary>
        /// <returns>String</returns>
        public string GetNotification()
        {
            return errorMessage;
        }

        /// <summary>
        /// Highlight the apartment if there is an error regarding its area
        /// </summary>
        /// <param name="show">Toggle the highlight</param>
        public void HighlightErrorObject(bool show)
        {
            if (!isSelected)
            {
                if (polygon != null)
                    polygon.ErrorHighlight(show);
                else if (editableMesh != null)
                    editableMesh.ErrorHighlight(show);
            }
            else if (!show && isSelected)
            {
                if (polygon != null)
                    polygon.SelectHighlight(true);
                else if (editableMesh != null)
                    editableMesh.SelectHighlight(true);
            }
        }

        /// <summary>
        /// Highlight the apartment using its type colour
        /// </summary>
        /// <param name="show">Toggle the highlight</param>
        public void HighlightType(bool show)
        {
            if (polygon != null)
                polygon.TypeHighlight(show);
            else if (editableMesh != null)
                editableMesh.TypeHighlight(show);
        }

        /// <summary>
        /// Select the apartment
        /// </summary>
        public override void Select(bool triggerEvent = true)
        {
            if (polygon != null)
            {
                polygon.SelectHighlight(true);
            }
            else if (editableMesh != null)
            {
                editableMesh.SelectHighlight(true);
            }
            if (error)
            {
                var sign = GameObject.Find("MovableNotificationsSign");
                signCentre = new GameObject();
                signCentre.transform.position = editableMesh.meshExtrusion.GetComponent<Collider>().bounds.center;
                sign.GetComponent<UiRepresentation>().Initialize(signCentre.transform);
                sign.GetComponent<UiRepresentation>().UpdateUIPosition();
            }
            isSelected = true;
            if (triggerEvent)
                OnSelected();
        }

        /// <summary>
        /// Deselect the apartment
        /// </summary>
        public override void DeSelect(bool triggerEvent = true)
        {
            if (polygon != null)
                polygon.SelectHighlight(false);
            else if (editableMesh != null)
                editableMesh.SelectHighlight(false);
            isSelected = false;
            if (error)
            {
                if (signCentre != null)
                {
                    Destroy(signCentre);
                }
                var sign = GameObject.Find("MovableNotificationsSign");
                sign.GetComponent<RectTransform>().position = new Vector2(-10000, -10000);
                sign.GetComponent<UiRepresentation>().sceneObjects.Clear();
            }
            if (triggerEvent)
                OnDeSelected();
        }

        /// <summary>
        /// Select this apartment if there is an error regarding its area
        /// </summary>
        public void ErrorSelect()
        {
            if (gameObject.activeSelf)
            {
                Select();
            }
        }

        /// <summary>
        /// Returns information about the apartment
        /// </summary>
        /// <returns>String Arrayy</returns>
        public override string[] GetInfoText()
        {
            string title = string.Format("{0} Info:\r\n", gameObject.name);

            string info = "";
            info += string.Format("Apartment id: {0}\r\n", id);
            info += string.Format("Apartment type: {0}\r\n", apartmentType);
            info += string.Format("Apartment area: {0}m\xB2\r\n", Math.Round(Area));
            info += string.Format("Number of People: {0}\r\n", numberOfPeople);

            return new string[2] { title, info };
        }

        /// <summary>
        /// Returns information about the apartment
        /// </summary>
        /// <returns>Dictionary</returns>
        public override Dictionary<string, object> GetInfoDict()
        {
            return base.GetInfoDict();
        }

        /// <summary>
        /// Returns the corners of the apartment
        /// </summary>
        /// <returns>Vector Array</returns>
        public Vector3[] GetCorners()
        {
            return editableMesh.GetCorners();
        }

        /// <summary>
        /// Moves the apartment to a new location
        /// </summary>
        /// <param name="to">The new location</param>
        /// <param name="reverse">The direction (negative or positive)</param>
        public void Move(Vector3 to, bool reverse = false)
        {
            transform.position = to;
            for (int i = 0; i < editableMesh.vertices.Count; i++)
            {
                editableMesh.vertices[i].UpdatePosition();
            }
        }

        /// <summary>
        /// Returns the save-state of the apartment
        /// </summary>
        /// <returns>Apartment Save State</returns>
        public ApartmentState GetApartmentState()
        {
            ApartmentState state = new ApartmentState();

            state.name = gameObject.name;
            state.hasCorridor = HasCorridor;
            state.type = apartmentType;
            state.isCorner = IsCorner;
            state.isExterior = isExterior;
            state.width = Width;
            state.depth = Depth;
            state.id = id;
            state.corridorIndex = activeCorridorIndex;
            state.flipped = flipped;
            state.isM3 = isM3;

            List<float> vertices = new List<float>();
            for (int i = 0; i < editableMesh.vertices.Count; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    vertices.Add(editableMesh.vertices[i].currentPosition[j]);
                }
            }

            List<int> tris = new List<int>();

            if (editableMesh != null)
            {
                for (int i = 0; i < editableMesh.mesh.triangles.Length; i++)
                {
                    tris.Add(editableMesh.mesh.triangles[i]);
                }
            }

            state.triangles = tris.ToArray();
            state.vertices = vertices.ToArray();

            return state;
        }

        /// <summary>
        /// Checks to see whether this apartment complies with the constraints
        /// </summary>
        public void CheckAgainstConstraints()
        {
            errorMessage = String.Empty;
            float wallsArea = 2 * ((Standards.TaggedObject.ConstructionFeatures["PartyWall"] / 2.0f) * Depth) + (Standards.TaggedObject.ConstructionFeatures["CorridorWall"] * Width);
            if (apartmentType != "SpecialApt")
            {
                double minArea;
                double maxArea;
                double m_area = Area;
                Dictionary<string, double> minAreas = isM3 ? Standards.TaggedObject.ApartmentTypesMinimumSizesM3 : Standards.TaggedObject.ApartmentTypesMinimumSizes;
                Dictionary<string, double> maxAreas = isM3 ? Standards.TaggedObject.M3ApartmentTypesMaximumSizes : Standards.TaggedObject.ApartmentTypesMaximumSizes;
                if (minAreas.TryGetValue(apartmentType, out minArea) && maxAreas.TryGetValue(apartmentType, out maxArea))
                {
                    try {
                        bool minAreaError = Math.Round(m_area) < Math.Round(minArea);
                        bool maxAreaError = Math.Round(m_area) > Math.Round(maxArea);
                        bool depthError = Depth > 8;

                        if (!minAreaError && !maxAreaError && !depthError)
                        {
                            errorMessage = String.Empty;
                            error = false;
                            HighlightErrorObject(error);
                        }

                        if (minAreaError)
                        {
                            errorMessage = gameObject.name + " is too small!\r\n" + apartmentType + " should be minimum " + minAreas[apartmentType] + "m\xB2\r\n";
                            error = true;
                            HighlightErrorObject(error);
                        }

                        if (maxAreaError)
                        {
                            errorMessage = gameObject.name + " is too big!\r\n" + apartmentType + " should be maximum " + maxAreas[apartmentType] + "m\xB2\r\n";
                            error = true;
                        }

                        if (depthError && ProceduralFloor.building.buildingType != BuildingType.Mansion)
                        {
                            errorMessage += gameObject.name + " is too deep!\r\nDepth should not exceed 8 m\r\n";
                            error = true;
                        }
                        base.ConstraintsChanged(this, error);

                    }
                    catch(Exception e)
                    {
                        Debug.Log(e);
                    }

                    
                }
                if (isSelected && error)
                {
                    var sign = GameObject.Find("MovableNotificationsSign");
                    //sign.GetComponent<RectTransform>().position = Camera.main.WorldToScreenPoint(editableMesh.meshExtrusion.GetComponent<Collider>().bounds.center);
                    if (signCentre == null)
                    {
                        signCentre = new GameObject();
                    }
                    signCentre.transform.position = editableMesh.meshExtrusion.GetComponent<Collider>().bounds.center;
                    sign.GetComponent<UiRepresentation>().Initialize(signCentre.transform);
                    sign.GetComponent<UiRepresentation>().UpdateUIPosition();
                }
                else if (isSelected && !error)
                {
                    if (signCentre != null)
                    {
                        Destroy(signCentre);
                    }
                    var sign = GameObject.Find("MovableNotificationsSign");
                    sign.GetComponent<RectTransform>().position = new Vector2(-10000, -10000);
                    sign.GetComponent<UiRepresentation>().sceneObjects.Clear();
                }
            }
        }

        /// <summary>
        /// Returns the Three.Js representation of this apartment
        /// </summary>
        /// <returns>Three.Js Object3D</returns>
        public BT.Object3d GetThreeJsObject()
        {
            BT.Object3d obj = new BT.Object3d();
            obj.type = "Mesh";
            BT.BoxGeometry boxGeometry = new BT.BoxGeometry(1, 1, 1);
            obj.geometry = boxGeometry.uuid;
            obj._geometry = boxGeometry;
            Vector3 position = transform.TransformPoint(Vector3.zero + new Vector3(Width * 0.5f, proceduralFloor.floor2floor * 0.5f, Depth * 0.5f));
            obj.position = new BT.Vector3(position.x, position.y, position.z);
            obj.rotation = new BT.Euler(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z);
            obj.scale = new BT.Vector3(Width, proceduralFloor.floor2floor, Depth);
            obj.userData.Add("type", "Apartment");
            obj.userData.Add("apartmentType", apartmentType);
            obj.userData.Add("buildingIndex", proceduralFloor.building.index.ToString());
            obj.userData.Add("floorIndex", proceduralFloor.building.floors.IndexOf(proceduralFloor).ToString());
            return obj;
        }
        #endregion

        #region Private Methods
        private void CheckForUpgrade()
        {
            double minArea;
            double maxArea;
            var types = Standards.TaggedObject.ApartmentTypesMinimumSizes.Keys.ToList();
            int typeIndex = types.IndexOf(apartmentType);
            if (Standards.TaggedObject.ApartmentTypesMinimumSizes.TryGetValue(apartmentType, out minArea) && Standards.TaggedObject.ApartmentTypesMaximumSizes.TryGetValue(apartmentType, out maxArea))
            {
                if (Math.Round(Area) > Math.Round(maxArea))
                {
                    if (apartmentType != "SpecialApt")
                    {
                        if (typeIndex + 1 < types.Count - 1)
                        {
                            apartmentType = types[typeIndex + 1];
                            editableMesh.SetTypeColor(Standards.TaggedObject.ApartmentTypesColours[apartmentType]);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < types.Count - 1; i++)
                        {
                            if (Math.Round(Area) > Math.Round(Standards.TaggedObject.ApartmentTypesMinimumSizes[types[i]]))
                            {
                                apartmentType = types[i];
                            }
                        }
                        editableMesh.SetTypeColor(Standards.TaggedObject.ApartmentTypesColours[apartmentType]);
                    }
                }
            }
        }

        private float GetFacadeLength()
        {
            float facadeArea = 0;

            if (!IsCorner)
            {
                facadeArea = Vector3.Distance(editableMesh.vertices[1].currentPosition, editableMesh.vertices[2].currentPosition);
                Debug.DrawLine(editableMesh.vertices[1].currentPosition, editableMesh.vertices[2].currentPosition, Color.red, 1000, false);
            }
            else
            {

            }

            return facadeArea;
        }

        /// <summary>
        /// Called when the polygon of the apartment has been updated
        /// </summary>
        /// <param name="sender">The polygon which has been updated</param>
        protected override void OnPolygonUpdated(Polygon sender)
        {
            OnAreaChanged();
        }

        /// <summary>
        /// Called when the editable mesh of the apartment has been updated
        /// </summary>
        /// <param name="mesh">The editable mesh that has been updated</param>
        protected override void OnEditableMeshUpdated(EditableMesh mesh)
        {
            OnAreaChanged();
        }

        /// <summary>
        /// Called when the editable mesh has stopped being updated
        /// </summary>
        /// <param name="mesh">The editable mesh which was being updated</param>
        protected override void OnMeshStoppedEditing(EditableMesh mesh)
        {
            BrydenWoodUtils.MoveParentIndependently(transform, mesh[3].currentPosition);
            mesh.UpdateMeshPosition(mesh[3].currentPosition);
        }

        private void OnAreaChanged()
        {
            OnGeometryUpdated();
            CheckAgainstConstraints();
        }

        private float CalculateNIA()
        {
            if (Representation != null)
            {
                return Representation.GetComponent<ProceduralApartmentLayout>().NIA;
            }
            else
                return 0;
        }
        #endregion
    }
}

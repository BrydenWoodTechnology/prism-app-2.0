﻿using BrydenWoodUnity.GeometryManipulation;
using BrydenWoodUnity.UIElements;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BrydenWoodUnity.DesignData
{
    /// <summary>
    /// A Monobehaviour component for creating a Mansion Block
    /// </summary>
    public class ProceduralMansion : ProceduralFloor
    {
        #region Public Fields and Properties
        [HideInInspector]
        public List<MansionBlock> blocks;
        public float aptDepth;
        public ProceduralMansion baseFloor;
        public float baseHeight { get { return building.baseHeight; } }
        public override float GIA
        {
            get
            {
                if (baseFloor == null)
                {
                    var gia = 0.0f;
                    if (apartments != null)
                    {
                        for (int i = 0; i < apartments.Count; i++)
                        {
                            gia += apartments[i].editableMesh.area;
                        }
                    }
                    gia += (coreWidth * coreLength);
                    return Mathf.Round(gia);
                }
                else if (baseFloor != null)
                    return baseFloor.GIA;
                else return 0;
            }
        }
        #endregion

        #region Private Fields and Properties
        private Vector3[] blockOrigins;
        private Transform blocksParent;
        private List<Vector3> revExtVertices;
        private List<Vector3> extVertices;
        private Vector3[] extCorners;
        private GameObject[] cores;
        #endregion

        #region MonoBehaviour Methods
        private void Start()
        {
            //Initialize(new List<MansionBlock>()
            //{
            //    new MansionBlock(),new MansionBlock(new string[] {"3b5p","2b4p" }),new MansionBlock(new string[] {"1b2p","1b2p" })
            //});
            //GenerateGeometries(PreviewMode.Apartments);
        }

        private void Update()
        {

        }
        #endregion

        #region Public Methods

        public void GeneratePodiumPolygon(ProceduralMansion mansion)
        {
            if (mansion.exteriorPolygon != null)
            {
                if (exteriorPolygon != null)
                {
                    Destroy(exteriorPolygon.gameObject);
                    exteriorPolygon = null;
                }

                exteriorPolygon = Instantiate(polygonPrefab).GetComponent<Polygon>();
                exteriorPolygon.transform.SetParent(transform);
                exteriorPolygon.gameObject.name = "ExteriorEnvelopePolygon";
                exteriorPolygon.GetComponent<MeshRenderer>().enabled = false;
                exteriorPolygon.transform.localPosition = Vector3.zero;

                List<PolygonVertex> verts = new List<PolygonVertex>();

                for (int i = 0; i < mansion.extCorners.Length; i++)
                {
                    PolygonVertex vert = Instantiate(polygonVertexPrefab, exteriorPolygon.transform).GetComponent<PolygonVertex>();
                    vert.Initialize(i);
                    vert.UpdateLocalPosition(new Vector3(mansion.extCorners[i].x, 0, mansion.extCorners[i].z));
                    verts.Add(vert);
                }

                var pts = PolygonVertex.ToVectorArray(verts);

                pts = Polygon.OffsetPoints(pts, true, -footPrintOffset - Standards.TaggedObject.ConstructionFeatures["ExteriorWall"]);

                for (int i = 0; i < verts.Count; i++)
                {
                    verts[i].UpdatePosition(pts[i]);
                }

                exteriorPolygon.Initialize(verts, true, true, false);
                Extrusion outterEnvelope = Instantiate(extrusionPrefab, exteriorPolygon.transform).GetComponent<Extrusion>();
                outterEnvelope.capped = true;
                outterEnvelope.totalHeight = levels.Length * floor2floor;
                outterEnvelope.Initialize(exteriorPolygon);
                exteriorPolygon.extrusion = outterEnvelope;
                Material material;
                //material = new Material(Resources.Load("Materials/OutlineCutoff") as Material);
                //material.SetFloat("_Modulo", levels.Length);
                material = new Material(Resources.Load("Materials/Envelope") as Material);
                material.color = Color.gray;
                //material.SetFloat("_Modulo", levels.Length);
                //material.SetFloat("_UseTexture", 0);
                exteriorPolygon.SetOriginalMaterial(material);
                exteriorPolygon.transform.localEulerAngles = Vector3.zero;
            }
        }

        public void GenerateBasementPolygon(ProceduralMansion mansion)
        {
            if (mansion.exteriorPolygon != null)
            {
                if (exteriorPolygon != null)
                {
                    Destroy(exteriorPolygon.gameObject);
                    exteriorPolygon = null;
                }

                exteriorPolygon = Instantiate(polygonPrefab).GetComponent<Polygon>();
                exteriorPolygon.transform.SetParent(transform);
                exteriorPolygon.gameObject.name = "ExteriorEnvelopePolygon";
                exteriorPolygon.GetComponent<MeshRenderer>().enabled = false;
                exteriorPolygon.transform.localPosition = Vector3.zero;

                List<PolygonVertex> verts = new List<PolygonVertex>();

                for (int i = 0; i < mansion.extCorners.Length; i++)
                {
                    PolygonVertex vert = Instantiate(polygonVertexPrefab, exteriorPolygon.transform).GetComponent<PolygonVertex>();
                    vert.Initialize(i);
                    vert.UpdateLocalPosition(new Vector3(mansion.extCorners[i].x, 0, mansion.extCorners[i].z));
                    verts.Add(vert);
                }

                var pts = PolygonVertex.ToVectorArray(verts);

                pts = Polygon.OffsetPoints(pts, true, -footPrintOffset - Standards.TaggedObject.ConstructionFeatures["ExteriorWall"]);

                for (int i = 0; i < verts.Count; i++)
                {
                    verts[i].UpdatePosition(pts[i]);
                }

                exteriorPolygon.Initialize(verts, true, true, false);
                Extrusion outterEnvelope = Instantiate(extrusionPrefab, exteriorPolygon.transform).GetComponent<Extrusion>();
                outterEnvelope.totalHeight = levels.Length * floor2floor * -1;
                outterEnvelope.Initialize(exteriorPolygon);
                exteriorPolygon.extrusion = outterEnvelope;
                Material material;
                material = new Material(Resources.Load("Materials/OutlineCutoff") as Material);
                material.SetFloat("_Modulo", levels.Length);
                material.SetFloat("_UseTexture", 1);
                exteriorPolygon.SetOriginalMaterial(material);
                exteriorPolygon.transform.localEulerAngles = Vector3.zero;
            }
        }

        public void SetM3Apartment(ApartmentUnity apt, bool isM3)
        {
            for (int i = 0; i < apartments.Count; i++)
            {
                if (apartments[i] == apt)
                {
                    int blockIndex = Mathf.FloorToInt(i * 0.5f);
                    if (i % 2 == 0)
                    {
                        blocks[blockIndex].isFirstApartmentM3 = isM3;
                    }
                    else
                    {
                        blocks[blockIndex].isSecondApartmentM3 = isM3;
                    }
                    break;
                }
            }
            GenerateGeometries();
            StartCoroutine(building.buildingManager.OverallUpdate());
        }

        public override void UpdatePosition(bool loaded = false)
        {
            yVal = building.buildingElement.GetYForLevel(levels[0], id);
            if (hasGeometry)
            {
                transform.position = new Vector3(transform.position.x, yVal, transform.position.z);
                if (apartments != null)
                {
                    for (int i = 0; i < apartments.Count; i++)
                    {
                        apartments[i].editableMesh.UpdateVerticesPositions();
                    }
                }
                UpdateFloorOutline(building.buildingManager.previewMode);
                SetPreviewMode(building.buildingManager.previewMode);
                building.OnFloorHeightChanged();
            }
        }
        public override void UpdateFloorOutline(PreviewMode mode)
        {
            if (!isBasement)
            {
                List<Line> lines = new List<Line>();
                float offsetDist = Standards.TaggedObject.ConstructionFeatures["ExteriorWall"] + 0.1f;
                switch (mode)
                {
                    case PreviewMode.Floors:
                        if (hasCustomExterior)
                        {
                            for (int i = 0; i < exteriorPolygon.Count; i++)
                            {
                                int next = (i + 1) % exteriorPolygon.Count;
                                lines.Add(new Line(exteriorPolygon[i].currentPosition, exteriorPolygon[next].currentPosition, Color.black, Vector3.down, offsetDist));
                            }
                        }
                        else
                        {
                            for (int i = 0; i < exteriorPolygon.Count; i++)
                            {
                                int next = (i + 1) % exteriorPolygon.Count;
                                lines.Add(new Line(exteriorPolygon[i].currentPosition, exteriorPolygon[next].currentPosition, Color.black, Vector3.up, 0.1f));
                            }
                        }
                        break;
                    case PreviewMode.Buildings:

                        for (int m = 0; m < levels.Length; m++)
                        {
                            Vector3 addHeight = new Vector3(0, m * floor2floor, 0);
                            if (hasCustomExterior)
                            {
                                for (int i = 0; i < exteriorPolygon.Count; i++)
                                {
                                    int next = (i + 1) % exteriorPolygon.Count;
                                    lines.Add(new Line(exteriorPolygon[i].currentPosition + addHeight, exteriorPolygon[next].currentPosition + addHeight, Color.black, Vector3.down, offsetDist));
                                }
                            }
                            else
                            {
                                if (exteriorPolygon != null)
                                {
                                    for (int i = 0; i < exteriorPolygon.Count; i++)
                                    {
                                        int next = (i + 1) % exteriorPolygon.Count;
                                        lines.Add(new Line(exteriorPolygon[i].currentPosition + addHeight, exteriorPolygon[next].currentPosition + addHeight, Color.black, Vector3.up, 0.1f));
                                    }
                                }
                            }
                        }
                        break;
                }
                drawBuildingLines.AddBuildingLines(new KeyValuePair<string, List<Line>>(this.key, lines));
            }
        }
        public override void Initialize()
        {
            blocks = new List<MansionBlock>();
            drawBuildingLines = Camera.main.GetComponent<DrawBuildingLines>();
        }
        public void Initialize(List<MansionBlock> blocks)
        {
            this.blocks = blocks;
            CalculatePercentages();
            drawBuildingLines = Camera.main.GetComponent<DrawBuildingLines>();
        }

        public void GenerateGeometries(bool loading = false)
        {
            GenerateGeometries(building.buildingManager.previewMode, loading);
        }

        public void OnBlocksChanged(List<MansionBlock> mansionBlocks)
        {
            blocks = mansionBlocks;
            CalculatePercentages();
            if (hasGeometry)
            {
                GenerateGeometries();
            }
        }

        public void GenerateGeometries(PreviewMode mode, bool loading = false)
        {
            transform.eulerAngles = Vector3.zero;
            if (baseFloor == null)
            {
                if (building.podium != null)
                {
                    transform.position = new Vector3(transform.position.x, building.baseHeight + building.podium.levels.Length * building.podium.floor2floor + 0.5f, transform.position.z);
                }
                else
                {
                    transform.position = new Vector3(transform.position.x, building.baseHeight, transform.position.z);
                }
                GenerateAllBlocks();
                GetPlatformsBays();
                GenerateExteriorPolygon();
                hasGeometry = true;
                for (int i = 0; i < apartments.Count; i++)
                {
                    apartments[i].geometryUpdated.AddListener(OnApartmentUpdated);
                }

                if (!loading)
                {
                    if (building.podium != null)
                    {
                        (building.podium as ProceduralMansion).GenerateGeometries(mode);
                    }
                    if (building.basement != null)
                    {
                        (building.basement as ProceduralMansion).GenerateGeometries(mode);
                    }
                }
                if (platformsParent != null && building.manufacturingSystem != ManufacturingSystem.Platforms)
                    platformsParent.gameObject.SetActive(false);
            }
            else
            {
                if (isPodium)
                {

                    apartments = new List<ApartmentUnity>();
                    GetCores();
                    if (!hasCustomExterior)
                    {
                        GeneratePodiumPolygon(baseFloor);
                    }
                }
                else if (isBasement)
                {
                    apartments = new List<ApartmentUnity>();
                    GetCores();
                    if (!hasCustomExterior)
                        GenerateBasementPolygon(baseFloor);
                }
            }
            transform.localEulerAngles = Vector3.zero;
            hasGeometry = true;
            SetPreviewMode(mode);
        }

        public override void UpdateLevels(float f2f, int[] levels)
        {
            this.levels = levels;
            floor2floor = f2f;
            if (hasGeometry)
            {
                this.levels = levels;
                floor2floor = f2f;
                GenerateExteriorPolygon();
                UpdateFloorOutline(building.buildingManager.previewMode);
                SetPreviewMode(building.buildingManager.previewMode);
                building.OnFloorHeightChanged();
            }
        }

        public override void UpdateLevels(float f2f, int[] levels, float offset)
        {
            if (isPodium || isBasement)
            {
                this.levels = levels;
                floor2floor = f2f;
                footPrintOffset = offset;
                if (hasGeometry)
                {
                    if (isBasement)
                    {
                        GenerateBasementPolygon(baseFloor);
                    }
                    else
                    {
                        GeneratePodiumPolygon(baseFloor);
                    }
                    SetPreviewMode(building.buildingManager.previewMode);
                    UpdateFloorOutline(building.buildingManager.previewMode);
                    building.OnFloorHeightChanged();
                    for (int i = 0; i < building.floors.Count; i++)
                    {
                        building.floors[i].UpdatePosition();
                    }
                }
            }
        }

        public override void GenerateExteriorPolygon(float deltaOffset = 0)
        {

            if (exteriorPolygon == null)
            {
                revExtVertices.Reverse();
                extVertices.AddRange(revExtVertices);
                //for (int i=0; i<extVertices.Count; i++)
                //{
                //    var obj = new GameObject(i.ToString());
                //    obj.transform.position = extVertices[i];
                //}
                //var pts = points.Distinct().ToArray();
                var offsetPoints = Polygon.OffsetPoints(extVertices.ToArray().RemoveDuplicates(0.05f), true, -Standards.TaggedObject.ConstructionFeatures["ExteriorWall"]);

                exteriorPolygon = Instantiate(polygonPrefab).GetComponent<Polygon>();
                exteriorPolygon.transform.SetParent(transform);
                exteriorPolygon.gameObject.name = "ExteriorEnvelopePolygon";
                exteriorPolygon.GetComponent<MeshRenderer>().enabled = false;
                exteriorPolygon.transform.localPosition = Vector3.zero;
                //DestroyImmediate(exteriorPolygon.gameObject);
                //exteriorPolygon = null;
                List<PolygonVertex> verts = new List<PolygonVertex>();

                for (int i = 0; i < offsetPoints.Length; i++)
                {
                    PolygonVertex vert = Instantiate(polygonVertexPrefab, exteriorPolygon.transform).GetComponent<PolygonVertex>();
                    vert.Initialize(i);
                    vert.UpdatePosition(offsetPoints[i]);
                    verts.Add(vert);
                }

                exteriorPolygon.Initialize(verts, true, true, false);

                Extrusion outterEnvelope = Instantiate(extrusionPrefab, exteriorPolygon.transform).GetComponent<Extrusion>();
                outterEnvelope.totalHeight = floor2floor;
                outterEnvelope.Initialize(exteriorPolygon);
                exteriorPolygon.extrusion = outterEnvelope;
                Material material;
                material = Resources.Load("Materials/Envelope") as Material;
                exteriorPolygon.SetOriginalMaterial(material);
                exteriorPolygon.transform.localEulerAngles = Vector3.zero;
                //if (OnOutterEnvelopeChanged != null)
                //{
                //    OnOutterEnvelopeChanged(this);
                //}
            }
            else
            {
                Destroy(exteriorPolygon.gameObject);
                exteriorPolygon = null;
                GenerateExteriorPolygon(deltaOffset);
            }
        }

        public override FloorLayoutState GetLayoutState()
        {
            UpdateLayoutState();
            return saveState;
        }

        public override void UpdateLayoutState()
        {
            saveState = new FloorLayoutState(building.index, id, false, false, aptDepth, corridorWidth, false, true, null, isBasement, isPodium);
            saveState.mansionBlocks = blocks.ToArray();
            saveState.centreLine = null;
            saveState.isCustom = false;
            saveState.customParams = null;
            saveState.apartmentStates = new List<ApartmentState>();
            saveState.isMansion = true;
            saveState.position = new float[] { transform.position[0], transform.position[1], transform.position[2] };
            saveState.euler = new float[] { transform.eulerAngles[0], transform.eulerAngles[1], transform.eulerAngles[2] };
            if (!isPodium && !isBasement)
            {
                for (int i = 0; i < apartments.Count; i++)
                {
                    saveState.apartmentStates.Add(apartments[i].GetApartmentState());
                }
            }
            saveState.footPrintOffset = footPrintOffset;
            if (hasCustomExterior)
            {
                saveState.SetCustomExterior(exteriorPolygon);
            }
            saveState.minApartmentWidth = building.prevMinimumWidth;
        }

        public override void LoadGeometries(FloorLayoutState loadState, ProceduralFloor masterFloor)
        {
            baseFloor = masterFloor as ProceduralMansion;
            if (baseFloor == null)
            {
                yVal = loadState.position[1];
                transform.position = new Vector3(loadState.position[0], loadState.position[1], loadState.position[2]);

                if (aptParent != null)
                {
                    DestroyImmediate(aptParent);
                }
                blocks = loadState.mansionBlocks.ToList();
                GenerateGeometries(true);
                CalculatePercentages();
            }
            else
            {
                if (isPodium)
                {
                    yVal = building.baseHeight;
                    transform.position = new Vector3(loadState.position[0], yVal, loadState.position[2]);
                    GenerateGeometries();
                }
                else if (isBasement)
                {
                    yVal = 0;
                    transform.position = new Vector3(loadState.position[0], yVal, loadState.position[2]);
                    GenerateGeometries();
                }
            }
        }

        public void OnAptLengthChanged(float aptDepth)
        {
            this.aptDepth = aptDepth;
            if (hasGeometry)
                GenerateGeometries(building.previewMode);
        }

        public override void GetApartmentAspects(out int north, out int east, out int south, out int west, out int dualAspect, out int m3)
        {
            north = 0;
            east = 0;
            south = 0;
            west = 0;
            m3 = 0;
            if (hasGeometry && !isPodium && !isBasement && apartments != null)
            {
                for (int i = 0; i < apartments.Count; i++)
                {
                    int orientation = apartments[i].Orientation;
                    switch (orientation)
                    {
                        case 0:
                            north++;
                            break;
                        case 1:
                            east++;
                            break;
                        case 2:
                            south++;
                            break;
                        case 3:
                            west++;
                            break;
                    }
                    if (apartments[i].isM3) m3++;
                }
            }
            north *= levels.Length;
            east *= levels.Length;
            south *= levels.Length;
            west *= levels.Length;
            m3 *= levels.Length;
            if (apartments != null)
                dualAspect = apartments.Count * levels.Length;
            else
                dualAspect = 0;
        }
        #endregion

        #region Private Methods
        private void GetCores()
        {
            if (coresParent != null)
            {
                DestroyImmediate(coresParent);
            }
            coresParent = new GameObject("CoresParent");
            coresParent.transform.SetParent(transform);
            coresParent.transform.localPosition = Vector3.zero;
            coresParent.transform.localEulerAngles = Vector3.zero;
            for (int i = 0; i < baseFloor.coresParent.transform.childCount; i++)
            {
                var _obj = baseFloor.coresParent.transform.GetChild(i).gameObject;
                var obj = Instantiate(_obj);
                obj.transform.SetParent(coresParent.transform);
                obj.transform.localScale = new Vector3(_obj.transform.localScale.x, levels.Length * floor2floor, _obj.transform.localScale.z);
                obj.transform.localPosition = new Vector3(_obj.transform.localPosition.x, /*yVal + */obj.transform.localScale.y * 0.5f, _obj.transform.localPosition.z);
                obj.transform.localEulerAngles = _obj.transform.localEulerAngles;
            }
        }

        private void CalculatePercentages()
        {
            percentages = new Dictionary<string, float>();
            Dictionary<string, int> apts = new Dictionary<string, int>();
            int counter = 0;
            for (int i = 0; i < blocks.Count; i++)
            {
                if (apts.ContainsKey(blocks[i].firstApartment))
                {
                    apts[blocks[i].firstApartment]++;
                }
                else
                {
                    apts.Add(blocks[i].firstApartment, 1);
                }
                if (apts.ContainsKey(blocks[i].secondApartment))
                {
                    apts[blocks[i].secondApartment]++;
                }
                else
                {
                    apts.Add(blocks[i].secondApartment, 1);
                }
                counter += 2;
            }
            foreach (var item in apts)
            {
                percentages.Add(item.Key, item.Value / (float)counter);
            }
        }
        private void OnApartmentUpdated(BaseDesignData sender)
        {
            UpdateExteriorPolygon();
        }

        private void UpdateExteriorPolygon()
        {

        }
        private void GetBlockOrigins()
        {
            Vector3 origin = Vector3.zero;
            blockOrigins = new Vector3[blocks.Count];
            blockOrigins[0] = origin;
            for (int i = 1; i < blocks.Count; i++)
            {
                float x = blocks[i - 1].aptDepths[0] + blocks[i - 1].aptDepths[1] + coreWidth +/* Standards.TaggedObject.ConstructionFeatures["PartyWall"] + */Standards.TaggedObject.ConstructionFeatures["CorridorWall"] * 2;
                origin += new Vector3(x, 0, 0);
                blockOrigins[i] = origin;
            }
        }

        private void GenerateAllBlocks()
        {
            if (blocksParent != null)
            {
                DestroyImmediate(blocksParent.gameObject);
            }
            blocksParent = new GameObject("BlocksParent").transform;
            blocksParent.SetParent(transform);
            blocksParent.localPosition = Vector3.zero;

            if (platformsParent != null)
            {
                DestroyImmediate(platformsParent.gameObject);
            }
            platformsParent = new GameObject("PlatformsParent").transform;
            platformsParent.SetParent(transform);
            platformsParent.localPosition = Vector3.zero;

            if (coresParent != null)
            {
                Destroy(coresParent);
            }
            coresParent = new GameObject("Cores");
            coresParent.transform.SetParent(transform);
            coresParent.transform.localPosition = Vector3.zero;
            cores = new GameObject[blocks.Count];

            apartments = new List<ApartmentUnity>(blocks.Count * 2);
            floorVertices = new List<GeometryVertex>();
            extVertices = new List<Vector3>();
            revExtVertices = new List<Vector3>();
            extCorners = new Vector3[4];

            GetBlockOrigins();
            for (int i = 0; i < blocks.Count; i++)
            {
                GenerateBlock(i);
            }
            for (int i = 0; i < cores.Length; i++)
            {
                cores[i].transform.SetParent(coresParent.transform);
            }
            blocksParent.localEulerAngles = Vector3.zero;
            platformsParent.localEulerAngles = Vector3.zero;
            coresParent.transform.localEulerAngles = Vector3.zero;
        }

        private void GenerateBlock(int index)
        {
            var blockObject = new GameObject("Block_" + index);
            blockObject.transform.SetParent(blocksParent);
            blockObject.transform.localEulerAngles = Vector3.zero;
            Vector3[] aptVerts = new Vector3[4];
            float offsetDist = 0;
            float aptEnvelopeWidth = 0;
            float defaultLength = 11.77f;
            float a = (defaultLength + Standards.TaggedObject.ConstructionFeatures["PartyWall"] - coreLength) * 0.5f;
            float b = (defaultLength + Standards.TaggedObject.ConstructionFeatures["PartyWall"] - coreLength) * 0.5f;

            //---------For the first apartment---------//;
            offsetDist = blocks[index].aptDepths[0] + Standards.TaggedObject.ConstructionFeatures["CorridorWall"];
            aptEnvelopeWidth = (float)/*(Standards.TaggedObject.ApartmentTypesMinimumSizes[blocks[index].aptTypes[0]] / blocks[index].aptDepths[0])*/aptDepth + Standards.TaggedObject.ConstructionFeatures["PartyWall"];
            aptVerts[0] = new Vector3(offsetDist, 0, 0);
            aptVerts[1] = new Vector3(offsetDist, 0, aptEnvelopeWidth);
            aptVerts[2] = new Vector3(0, 0, aptEnvelopeWidth);
            aptVerts[3] = Vector3.zero;
            var apt = CreateApartment(blocks[index].aptTypes[0], aptVerts, apartments.Count, blockObject.transform);
            apt.isM3 = blocks[index].isM3[0];
            apt.transform.localPosition = new Vector3(Standards.TaggedObject.ConstructionFeatures["PartyWall"] * 0.5f /*+ blocks[index].aptDepths[0]*/, 0, -(defaultLength + Standards.TaggedObject.ConstructionFeatures["PartyWall"]) * 0.5f);
            //apt.transform.localEulerAngles = new Vector3(0, -90, 0);
            apt.flipped = true;
            apt.editableMesh.mansionApt = true;
            apartments.Add(apt);

            //---------For the core---------//
            var core = GameObject.CreatePrimitive(PrimitiveType.Cube);
            core.name = "Core";
            core.transform.SetParent(blockObject.transform);
            core.transform.localPosition = new Vector3(Standards.TaggedObject.ConstructionFeatures["PartyWall"] * 0.5f + offsetDist + coreWidth * 0.5f, floor2floor * 0.5f, 0);
            core.transform.localScale = new Vector3(coreWidth, floor2floor, coreLength);
            cores[index] = core;


            //---------For the second apartment---------//
            float offsetDist2 = blocks[index].aptDepths[1] + Standards.TaggedObject.ConstructionFeatures["CorridorWall"];
            aptEnvelopeWidth = (float)/*(Standards.TaggedObject.ApartmentTypesMinimumSizes[blocks[index].aptTypes[1]] / blocks[index].aptDepths[1])*/aptDepth + Standards.TaggedObject.ConstructionFeatures["PartyWall"];
            aptVerts[0] = new Vector3(offsetDist2, 0, 0);
            aptVerts[1] = new Vector3(offsetDist2, 0, aptEnvelopeWidth);
            aptVerts[2] = new Vector3(0, 0, aptEnvelopeWidth);
            aptVerts[3] = Vector3.zero;
            var apt2 = CreateApartment(blocks[index].aptTypes[1], aptVerts, apartments.Count, blockObject.transform);
            apt2.isM3 = blocks[index].isM3[1];
            apt2.transform.localPosition = new Vector3(Standards.TaggedObject.ConstructionFeatures["PartyWall"] * 0.5f + offsetDist + coreWidth, 0, -(defaultLength + Standards.TaggedObject.ConstructionFeatures["PartyWall"]) * 0.5f);
            //apt2.transform.localEulerAngles = new Vector3(0, 90, 0);
            apt2.editableMesh.mansionApt = true;
            apartments.Add(apt2);

            blockObject.transform.localPosition = blockOrigins[index];

            //----------For Platforms Structure---------//
            int intermediateNum = Mathf.CeilToInt(aptEnvelopeWidth / 4.05f);
            float step = aptEnvelopeWidth / intermediateNum;

            for (int i = 0; i <= intermediateNum; i++)
            {

                if (step * i >= a && step * i <= a + coreLength)
                {
                    if (step * i - a > 0.8f)
                    {
                        GameObject corePlatformsExtraObject = Instantiate(platformsFramePrefab, platformsParent);
                        Vector3 posextra = apt.editableMesh[0].currentPosition + (apt.editableMesh[3].currentPosition - apt.editableMesh[0].currentPosition) * 0.5f + Vector3.forward * a;
                        posextra.x = core.transform.position.x;
                        corePlatformsExtraObject.transform.position = posextra;
                        corePlatformsExtraObject.transform.localEulerAngles = Vector3.zero;//LookAt(corePlatInter.transform.position + (corePlat2.transform.position - corePlat1.transform.position));
                        var corePlatExtra = corePlatformsExtraObject.GetComponent<PlatformsFrame>();
                        corePlatExtra.leftSpan = coreWidth * 0.5f;
                        corePlatExtra.rightSpan = coreWidth * 0.5f;
                        corePlatExtra.height = floor2floor;
                        corePlatExtra.SetUpFrame();
                    }

                    if (a + coreLength - step * i > 0.8f)
                    {
                        GameObject corePlatformsExtraObject = Instantiate(platformsFramePrefab, platformsParent);
                        Vector3 posextra = apt.editableMesh[0].currentPosition + (apt.editableMesh[3].currentPosition - apt.editableMesh[0].currentPosition) * 0.5f + Vector3.forward * (a + coreLength);
                        posextra.x = core.transform.position.x;
                        corePlatformsExtraObject.transform.position = posextra;
                        corePlatformsExtraObject.transform.localEulerAngles = Vector3.zero;//LookAt(corePlatInter.transform.position + (corePlat2.transform.position - corePlat1.transform.position));
                        var corePlatExtra = corePlatformsExtraObject.GetComponent<PlatformsFrame>();
                        corePlatExtra.leftSpan = coreWidth * 0.5f;
                        corePlatExtra.rightSpan = coreWidth * 0.5f;
                        corePlatExtra.height = floor2floor;
                        corePlatExtra.SetUpFrame();
                    }

                    GameObject corePlatformsObject = Instantiate(platformsFramePrefab, platformsParent);
                    Vector3 pos1 = apt.editableMesh[0].currentPosition + (apt.editableMesh[3].currentPosition - apt.editableMesh[0].currentPosition) * 0.5f + Vector3.forward * step * i;
                    pos1.x = core.transform.position.x;
                    corePlatformsObject.transform.position = pos1;
                    corePlatformsObject.transform.localEulerAngles = Vector3.zero;//LookAt(corePlatInter.transform.position + (corePlat2.transform.position - corePlat1.transform.position));
                    var corePlat = corePlatformsObject.GetComponent<PlatformsFrame>();
                    corePlat.leftSpan = offsetDist + coreWidth * 0.5f;
                    corePlat.rightSpan = offsetDist2 + coreWidth * 0.5f;
                    corePlat.intermediateOffset1 = offsetDist;
                    corePlat.intermediateOffset2 = offsetDist2;
                    corePlat.setIntermediateClmns = true;
                    corePlat.height = floor2floor;
                    corePlat.SetUpFrame();
                }
                else if (step * i < a || step * i > a + coreLength)
                {
                    GameObject apt1Platform = Instantiate(platformsFramePrefab, platformsParent);
                    Vector3 pos1 = apt.editableMesh[0].currentPosition + (apt.editableMesh[3].currentPosition - apt.editableMesh[0].currentPosition) * 0.5f + Vector3.forward * step * i;
                    apt1Platform.transform.position = pos1;
                    apt1Platform.transform.localEulerAngles = Vector3.zero;//LookAt(corePlatInter.transform.position + (corePlat2.transform.position - corePlat1.transform.position));
                    var apt1Plat = apt1Platform.GetComponent<PlatformsFrame>();
                    apt1Plat.SetDimensions(offsetDist, floor2floor);

                    GameObject apt2Platform = Instantiate(platformsFramePrefab, platformsParent);
                    Vector3 pos2 = apt2.editableMesh[0].currentPosition + (apt2.editableMesh[3].currentPosition - apt2.editableMesh[0].currentPosition) * 0.5f + Vector3.forward * step * i;
                    apt2Platform.transform.position = pos2;
                    apt2Platform.transform.localEulerAngles = Vector3.zero;//LookAt(corePlatInter.transform.position + (corePlat2.transform.position - corePlat1.transform.position));
                    var apt2Plat = apt2Platform.GetComponent<PlatformsFrame>();
                    apt2Plat.SetDimensions(offsetDist, floor2floor);
                }
            }

            //Vector3 coreBase = new Vector3(core.transform.position.x, core.transform.position.y - floor2floor * 0.5f, core.transform.position.z);
            //GameObject corePlat1 = Instantiate(platformsFramePrefab, platformsParent);
            //corePlat1.transform.position = coreBase + new Vector3(0, 0, -coreLength * 0.5f);
            //corePlat1.transform.localEulerAngles = Vector3.zero;//LookAt(coreBase);
            //var plt1 = corePlat1.GetComponent<PlatformsFrame>();
            //plt1.leftSpan = offsetDist + coreWidth * 0.5f;
            //plt1.rightSpan = offsetDist2 + coreWidth * 0.5f;
            //plt1.height = floor2floor;
            //plt1.SetUpFrame();

            //GameObject corePlat2 = Instantiate(platformsFramePrefab, platformsParent);
            //corePlat2.transform.position = coreBase + new Vector3(0, 0, coreLength * 0.5f);
            //corePlat2.transform.localEulerAngles = Vector3.zero;//LookAt(corePlat2.transform.position + (corePlat2.transform.position - coreBase));
            //var plt2 = corePlat2.GetComponent<PlatformsFrame>();
            //plt2.leftSpan = offsetDist + coreWidth * 0.5f;
            //plt2.rightSpan = offsetDist2 + coreWidth * 0.5f;
            //plt2.height = floor2floor;
            //plt2.SetUpFrame();

            //int intermediateNum = Mathf.CeilToInt(coreLength / 4.05f);
            //float step = coreLength / intermediateNum;
            //for (int i = 1; i < intermediateNum; i++)
            //{
            //    GameObject corePlatInter = Instantiate(platformsFramePrefab, platformsParent);
            //    corePlatInter.transform.position = corePlat1.transform.position + (corePlat2.transform.position - corePlat1.transform.position).normalized * step * i;
            //    corePlatInter.transform.localEulerAngles = Vector3.zero;//LookAt(corePlatInter.transform.position + (corePlat2.transform.position - corePlat1.transform.position));
            //    var pltInter = corePlatInter.GetComponent<PlatformsFrame>();
            //    pltInter.leftSpan = offsetDist + coreWidth * 0.5f;
            //    pltInter.rightSpan = offsetDist2 + coreWidth * 0.5f;
            //    pltInter.height = floor2floor;
            //    pltInter.SetUpFrame();
            //}

            //Debug.Log(aptEnvelopeWidth - coreLength);

            //GameObject apt1Plat1 = Instantiate(platformsFramePrefab, platformsParent);
            //apt1Plat1.transform.position = apt.editableMesh[0].currentPosition + (apt.editableMesh[3].currentPosition - apt.editableMesh[0].currentPosition) * 0.5f;
            //apt1Plat1.transform.localEulerAngles = Vector3.zero;//LookAt(apt1Plat1.transform.position + (apt.editableMesh[1].currentPosition - apt.editableMesh[2].currentPosition) * 0.5f);
            //var apt1plt1 = apt1Plat1.GetComponent<PlatformsFrame>();
            //apt1plt1.SetDimensions(offsetDist, floor2floor);
            //apt1plt1.SetUpFrame();

            //GameObject apt1Plat2 = Instantiate(platformsFramePrefab, platformsParent);
            //apt1Plat2.transform.position = apt.editableMesh[1].currentPosition + (apt.editableMesh[2].currentPosition - apt.editableMesh[1].currentPosition) * 0.5f;
            //apt1Plat2.transform.localEulerAngles = Vector3.zero;//LookAt(apt1Plat1.transform.position + (apt.editableMesh[1].currentPosition - apt.editableMesh[2].currentPosition) * 0.5f);
            //var apt1plt2 = apt1Plat2.GetComponent<PlatformsFrame>();
            //apt1plt2.SetDimensions(offsetDist, floor2floor);
            //apt1plt2.SetUpFrame();

            //GameObject apt2Plat1 = Instantiate(platformsFramePrefab, platformsParent);
            //apt2Plat1.transform.position = apt2.editableMesh[0].currentPosition + (apt2.editableMesh[3].currentPosition - apt2.editableMesh[0].currentPosition) * 0.5f;
            //apt2Plat1.transform.localEulerAngles = Vector3.zero;//LookAt(apt1Plat1.transform.position + (apt.editableMesh[1].currentPosition - apt.editableMesh[2].currentPosition) * 0.5f);
            //var apt2plt1 = apt2Plat1.GetComponent<PlatformsFrame>();
            //apt2plt1.SetDimensions(offsetDist2, floor2floor);
            //apt2plt1.SetUpFrame();

            //GameObject apt2Plat2 = Instantiate(platformsFramePrefab, platformsParent);
            //apt2Plat2.transform.position = apt2.editableMesh[1].currentPosition + (apt2.editableMesh[2].currentPosition - apt2.editableMesh[1].currentPosition) * 0.5f;
            //apt2Plat2.transform.localEulerAngles = Vector3.zero;//LookAt(apt1Plat1.transform.position + (apt.editableMesh[1].currentPosition - apt.editableMesh[2].currentPosition) * 0.5f);
            //var apt2plt2 = apt2Plat2.GetComponent<PlatformsFrame>();
            //apt2plt2.SetDimensions(offsetDist2, floor2floor);
            //apt2plt2.SetUpFrame();


            //----------For Exterior Polygon-----------//

            extVertices.Add(apt.editableMesh[2].currentPosition);
            extVertices.Add(apt.editableMesh[1].currentPosition);

            extVertices.Add(apt.editableMesh[1].currentPosition + Vector3.back * (aptEnvelopeWidth - a - coreLength));
            extVertices.Add(apt2.editableMesh[2].currentPosition + Vector3.back * (aptEnvelopeWidth - b - coreLength));

            extVertices.Add(apt2.editableMesh[2].currentPosition);
            extVertices.Add(apt2.editableMesh[1].currentPosition);// + (index != blocks.Count - 1 ? Vector3.right * Standards.TaggedObject.ConstructionFeatures["PartyWall"] : Vector3.zero));

            revExtVertices.Add(apt.editableMesh[3].currentPosition);
            revExtVertices.Add(apt.editableMesh[0].currentPosition);

            revExtVertices.Add(apt.editableMesh[0].currentPosition + Vector3.forward * a);
            revExtVertices.Add(apt2.editableMesh[3].currentPosition + Vector3.forward * b);

            revExtVertices.Add(apt2.editableMesh[3].currentPosition);
            revExtVertices.Add(apt2.editableMesh[0].currentPosition);// + (index != blocks.Count - 1 ? Vector3.right * Standards.TaggedObject.ConstructionFeatures["PartyWall"] : Vector3.zero));

            extCorners[0] = transform.InverseTransformPoint(extVertices[0]);
            extCorners[1] = transform.InverseTransformPoint(extVertices.Last());
            extCorners[2] = transform.InverseTransformPoint(revExtVertices.Last());
            extCorners[3] = transform.InverseTransformPoint(revExtVertices[0]);
        }

        private void GetPlatformsBays()
        {
            if (platformsParent != null)
            {
                HashSet<float> zVals = new HashSet<float>();
                for (int i = 0; i < platformsParent.childCount; i++)
                {
                    if (!zVals.Contains(platformsParent.GetChild(i).localPosition.z))
                    {
                        zVals.Add(platformsParent.GetChild(i).localPosition.z);
                    }
                }
                var listZ = zVals.ToList();
                listZ.Sort();
                bays = new List<float>();
                for (int i = 0; i < listZ.Count - 1; i++)
                {
                    bays.Add(Mathf.Abs(listZ[i + 1] - listZ[i]));
                }
            }
        }

        private Vector3[] RemoveDuplicatePoints(List<Vector3> points)
        {
            return null;
        }

        private ApartmentUnity CreateApartment(string type, Vector3[] verts, int i, Transform parent, string name = "", int[] _tris = null)
        {
            GameObject apartment = Instantiate(apartmentPrefab, parent);
            if (string.IsNullOrEmpty(name))
            {
                apartment.name = "Apartment_" + i + "_" + type + name;
            }
            else
            {
                apartment.name = name;
            }
            apartment.transform.localPosition = Vector3.zero;

            ApartmentUnity aptU = apartment.GetComponent<ApartmentUnity>();
            aptU.HasCorridor = false;
            aptU.hasExtrusion = true;
            aptU.extrusionMoveable = false;
            aptU.extrusionEditEdges = true;
            aptU.extrusionCapped = false;
            aptU.isExterior = true;
            aptU.Initialize(i, type);
            aptU.ProceduralFloor = this;
            //AddApartment(aptU);

            GameObject meshObject = new GameObject("MeshObject");
            meshObject.transform.SetParent(apartment.transform);
            meshObject.transform.localPosition = Vector3.zero;
            meshObject.transform.localEulerAngles = Vector3.zero;
            meshObject.layer = 8;
            var filter = meshObject.AddComponent<MeshFilter>();
            var renderer = meshObject.AddComponent<MeshRenderer>();
            var collider = meshObject.AddComponent<MeshCollider>();

            Mesh mesh = new Mesh();
            Vector3[] meshVerts = new Vector3[verts.Length];
            Vector2[] uvs = new Vector2[verts.Length];
            for (int j = 0; j < meshVerts.Length; j++)
            {
                meshVerts[j] = meshObject.transform.InverseTransformPoint(verts[j]);
                uvs[j] = new Vector2(meshVerts[j].x, meshVerts[j].z);
            }
            mesh.vertices = meshVerts;
            bool flip = true;
            if (_tris == null)
            {
                if (flip)
                {
                    mesh.triangles = new int[] { 0, 2, 1, 0, 3, 2 };
                }
                else
                {
                    mesh.triangles = new int[] { 1, 2, 0, 2, 3, 0 };
                }
            }
            else
            {
                mesh.triangles = _tris;
            }
            mesh.uv = uvs;
            mesh.uv2 = uvs;
            mesh.uv3 = uvs;
            mesh.uv4 = uvs;
            mesh.RecalculateNormals();
            mesh.RecalculateBounds();

            filter.sharedMesh = mesh;
            collider.sharedMesh = mesh;
            renderer.material = originalMaterial;

            var editMesh = meshObject.AddComponent<EditableMesh>();
            editMesh.capped = false;
            editMesh.editEdges = true;
            editMesh.moveable = false;
            editMesh.originalMaterial = originalMaterial;
            editMesh.typeMaterial = new Material(typeMaterial);
            editMesh.SetTypeColor(Standards.TaggedObject.ApartmentTypesColours[type]);
            editMesh.Initialize(editMesh.GetComponent<MeshFilter>().sharedMesh, editMesh.transform);
            var temp_verts = editMesh.vertices.Select(x => (GeometryVertex)x).ToList();
            //floorVertices.AddRange(temp_verts);
            aptU.SetEditableMesh(editMesh);
            aptU.editableMesh.TypeHighlight(false);
            editMesh.meshExtrusion.GetComponent<MeshCollider>().convex = true;
            aptU.CheckAgainstConstraints();
            return aptU;
        }
        #endregion
    }

    /// <summary>
    /// A class for Mansion Blocks
    /// </summary>
    public class MansionBlock
    {
        /// <summary>
        /// The different types of apartments of the block
        /// </summary>
        public string[] aptTypes;
        /// <summary>
        /// Whether the apartment is M3 compatible
        /// </summary>
        public bool[] isM3;
        /// <summary>
        /// The depths of the apartments
        /// </summary>
        public float[] aptDepths;
        /// <summary>
        /// The points which form the exterior of the mansion
        /// </summary>
        public Vector3[] pointsToExterior;
        /// <summary>
        /// The lengths of the apartments
        /// </summary>
        public float[] aptLengths;
        /// <summary>
        /// The type of the first apartment
        /// </summary>
        public string firstApartment
        {
            get { return aptTypes[0]; }
            set
            {
                aptTypes[0] = value;
                aptDepths[0] = isM3[0] ? Standards.TaggedObject.mansionWidthM3[aptTypes[0]] : Standards.TaggedObject.mansionWidth[aptTypes[0]];
            }
        }
        /// <summary>
        /// The type of the second apartment
        /// </summary>
        public string secondApartment
        {
            get { return aptTypes[1]; }
            set
            {
                aptTypes[1] = value;
                aptDepths[1] = isM3[1] ? Standards.TaggedObject.mansionWidthM3[aptTypes[1]] : Standards.TaggedObject.mansionWidth[aptTypes[1]];
            }
        }
        /// <summary>
        /// The maximum length of the apartments
        /// </summary>
        public float MaxAptLength
        {
            get
            {
                return Mathf.Max(aptLengths);
            }
        }
        /// <summary>
        /// Whether the first apartment is M3 compatible
        /// </summary>
        public bool isFirstApartmentM3
        {
            get { return isM3[0]; }
            set
            {
                isM3[0] = value;
                aptDepths[0] = isM3[0] ? Standards.TaggedObject.mansionWidthM3[aptTypes[0]] : Standards.TaggedObject.mansionWidth[aptTypes[0]];
            }
        }
        /// <summary>
        /// Whether the second apartment is M3 compatible
        /// </summary>
        public bool isSecondApartmentM3
        {
            get { return isM3[1]; }
            set
            {
                isM3[1] = value;
                aptDepths[1] = isM3[1] ? Standards.TaggedObject.mansionWidthM3[aptTypes[1]] : Standards.TaggedObject.mansionWidth[aptTypes[1]];
            }
        }

        #region Constructors
        public MansionBlock()
        {
            aptTypes = new string[2]
            {
                "1b2p","1b2p"
            };

            isM3 = new bool[2]
            {
                false, false
            };

            aptDepths = new float[2]
            {
                Standards.TaggedObject.mansionWidth[aptTypes[0]],
                Standards.TaggedObject.mansionWidth[aptTypes[1]]
            };

            aptLengths = new float[2]
            {
                11.77f,
                11.77f
            };
        }

        public MansionBlock(string[] aptTypes)
        {
            this.aptTypes = aptTypes;
            aptDepths = new float[2]
            {
                Standards.TaggedObject.mansionWidth[aptTypes[0]],
                Standards.TaggedObject.mansionWidth[aptTypes[1]]
            };

            aptLengths = new float[2]
            {
                11.77f,
                11.77f
            };

            isM3 = new bool[2]
            {
                false, false
            };
        }
        #endregion

        /// <summary>
        /// The apartment types that can fit in a Mansion Block
        /// </summary>
        public static string[] ApartmentTypes = new string[]
        {
            "1b2p","2b3p","2b4p","3b5p"
        };


    }
}

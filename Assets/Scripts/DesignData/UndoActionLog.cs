﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.DesignData
{
    /// <summary>
    /// A MonoBehaviour component for logging all the undo actions
    /// </summary>
    public class UndoActionLog : MonoBehaviour
    {
        #region Public Fields and Properties
        [Header("Scene References:")]
        public Button undoButton;
        #endregion

        #region Private Fields and Properties
        private UndoAction[] actions { get; set; }
        private UndoAction lastAction { get; set; }
        #endregion

        #region Events
        /// <summary>
        /// Used when an undo action is called
        /// </summary>
        /// <param name="lastAction"></param>
        public delegate void OnUndo(UndoAction lastAction);
        /// <summary>
        /// Triggered when an undo action is called
        /// </summary>
        public static event OnUndo undo;
        #endregion


        #region MonoBehaviour Methods
        private void OnDestroy()
        {
            if (undo != null)
            {
                foreach (Delegate del in undo.GetInvocationList())
                {
                    undo -= (OnUndo)del;
                }
            }
        }

        private void Start()
        {
            //actions = new List<CombAction>();
            actions = new ResolveAction[100];
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Adds an undo action
        /// </summary>
        /// <param name="m_action"></param>
        public void AddAction(UndoAction m_action)
        {
            PushActions(true);
            lastAction = m_action;
            actions[0] = lastAction;
            undoButton.interactable = true;
        }

        /// <summary>
        /// Clears all the undo actions
        /// </summary>
        public void Clear()
        {
            actions = new ResolveAction[100];
            lastAction = null;
        }

        /// <summary>
        /// Undoes an action
        /// </summary>
        public void Undo()
        {
            if (undo != null)
            {
                undo(lastAction);
            }
            PushActions(false);
            lastAction = actions[0];
            undoButton.interactable = actions[0] != null;
        }
        #endregion

        #region Private Methods
        private void PushActions(bool forward)
        {
            UndoAction[] temp_actions = new UndoAction[actions.Length];
            if (forward)
            {
                for (int i = 1; i < actions.Length; i++)
                {
                    if (actions[i - 1] != null)
                    {
                        temp_actions[i] = actions[i - 1];
                    }
                    else
                    {
                        temp_actions[i] = null;
                    }
                }
            }
            else
            {
                for (int i = 0; i < actions.Length - 1; i++)
                {
                    if (actions[i + 1] != null)
                    {
                        temp_actions[i] = actions[i + 1];
                    }
                    else
                    {
                        temp_actions[i] = null;
                    }
                }
            }

            actions = temp_actions;
        }
        #endregion
    }

    /// <summary>
    /// Undo action regarding the combination of apartments
    /// </summary>
    public class ResolveAction : UndoAction
    {
        public ProceduralFloor floor;
        public List<ApartmentUnity> resultingApts;
        public string[] aptNames;
        public string[] aptTypes;
        public int[] aptIndex;
        public bool[] flipped;
        public bool[] isExteriorCorner;
        public int[] aptCorridorIndex;
        public Vector3[][] aptVertices;
        public List<KeyValuePair<int, int>>[] aptVertexIndex;
        public bool[] aptHasCorridor;
        public ResolveAction()
        {
            resultingApts = new List<ApartmentUnity>();
        }
    }

    /// <summary>
    /// Undo action regarding the swapping of apartments
    /// </summary>
    public class SwapAction : UndoAction
    {
        public ProceduralFloor floor;
        public Vector3[] prevPos;
        public string[] aptNames;
        public SwapAction()
        {

        }
    }

    /// <summary>
    /// Abstract class for undo actions
    /// </summary>
    public abstract class UndoAction
    {

    }

}

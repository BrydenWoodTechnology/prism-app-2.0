﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using BrydenWoodUnity.DesignData;
/// <summary>
/// A MonoBehaviour component which generates the apartment layout asset
/// </summary>
public class ProceduralAptLayoutGenerator : MonoBehaviour
{
    #region Public Variables
    public string fileSuffix;
    public List<Color> colours = new List<Color>();
    public List<string> roomTypes = new List<string>();
    public TextAsset variables;
    public TextAsset rules;
    public TextAsset inclusion;
    #endregion

    #region Private Variables
    List<string> roomNames = new List<string>();
    string[] roomInclusion;
    #endregion

    #region Public Methods
#if UNITY_EDITOR
    /// <summary>
    /// It generates the apartment layout asset (Editor only)
    /// </summary>
    public void GenerateLayouts()
    {     
        string typeVariables = variables.text;
        roomInclusion = inclusion.text.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries); 

        string[] ruleLines = rules.text.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
        for(int i=0; i<ruleLines.Length; i++)
        {
            roomNames.Add(ruleLines[i].Split(',')[0]);
        }

        string[] apartments = typeVariables.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
        for (int i = 1; i <apartments.Length; i++)
        {
            string[] aptVar = apartments[i].Split(',');
            string[] tempBool = roomInclusion[i-1].Split(',');
            bool[] roomInclusionBool = new bool[tempBool.Length];
            for(int j=0; j<tempBool.Length; j++)
            {
                roomInclusionBool[j] = bool.Parse(tempBool[j]);
            }

            AptLayout aptLayout = ScriptableObject.CreateInstance<AptLayout>();
            aptLayout.buildingType = BuildingType.Mansion;
            aptLayout.aptType = aptVar[0];
            aptLayout.roomColors = colours.ToArray();
            aptLayout.roomInclusion = roomInclusionBool;
            aptLayout.roomNames = roomNames.ToArray();
            aptLayout.types = roomTypes.ToArray();
            aptLayout.roomData = rules;

            UnityEditor.AssetDatabase.CreateAsset(aptLayout, "Assets/Resources/AptLayoutData/" + aptLayout.aptType + fileSuffix + ".asset");
            UnityEditor.AssetDatabase.SaveAssets();

        }


       
        
    }
#endif

    #endregion
}

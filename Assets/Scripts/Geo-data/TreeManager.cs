using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using BrydenWoodUnity;
/// <summary>
/// A component to control instantiation of tree models based on a given dataset
/// </summary>
public class TreeManager : MonoBehaviour
{
    #region Public Variables
    public static TreeManager Instance;
    public List<string> treeNames;
    public Dictionary<string, TreeSeasons> trees = new Dictionary<string, TreeSeasons>();
    public Dictionary<string, GameObject> treeGos = new Dictionary<string, GameObject>();
    public Image progress;
    public string springFile;
    public string summerFile;
    public string autumnFile;
    public string winterFile;
    #endregion

    #region Private Variables
    string fullPath;
    AssetBundle myLoadedAssetBundle;
    string currentSeason;
    bool browserOnline = true;
    #endregion

    #region Delegates and Events
    /// <summary>
    /// Delegate for when the trees are loaded
    /// </summary>
    public delegate void OnTreesLoaded();
    /// <summary>
    /// Event triggered when the trees are loaded
    /// </summary>
    public static event OnTreesLoaded onTreesLoaded;
    #endregion

    #region Monobehaviour methods
    // Start is called before the first frame update
    void Start()
    {
        Instance = this;        

        PopulateTreesFromAssetBundle("_Spring");

        HtmlUIManager.OnBrowserOnLine += OnBrowserOffline;
    }
    #endregion

    #region Private Methods
    /// <summary>
    /// Load objects from an asset bundle in standalone/editor 
    /// based on https://learn.unity.com/tutorial/introduction-to-asset-bundles#6028bab6edbc2a750bf5b8a9
    /// </summary>
    /// <param name="path"></param>
    /// <param name="season"></param>
    void LoadFromAssetBundle(string path, string season)
    {
        progress.fillAmount = 0;
        progress.transform.parent.gameObject.SetActive(true);


        myLoadedAssetBundle = AssetBundle.LoadFromFile(path);
        if (myLoadedAssetBundle == null)
        {
            Debug.Log("Failed to load AssetBundle!");
            return;
        }
        for (int i = 0; i < treeNames.Count; i++)
        {
            var tree = myLoadedAssetBundle.LoadAsset<GameObject>(treeNames[i] + season);

            if (tree == null)
            {
                tree = myLoadedAssetBundle.LoadAsset<GameObject>("Other" + season);
            }
            
            
            treeGos.Add(treeNames[i], tree);
            progress.fillAmount = i / float.Parse(treeNames.Count.ToString());            
        }
        progress.fillAmount = 1;
        progress.transform.parent.gameObject.SetActive(false);
        //myLoadedAssetBundle.Unload(true);
        if(onTreesLoaded != null)
        {
            onTreesLoaded();
        }

        myLoadedAssetBundle.Unload(false);
        
    }
    /// <summary>
    /// Load objects from an asset bundle in webGL
    /// based on https://learn.unity.com/tutorial/introduction-to-asset-bundles#6028be40edbc2a112d4f4fe5
    /// </summary>
    /// <param name="path"></param>
    /// <param name="season"></param>
    /// <returns></returns>
    IEnumerator LoadFromAssetBundleWeb(string path, string season)
    {
        if (path.Contains("://") || path.Contains(":///"))
        {            
            progress.transform.parent.gameObject.SetActive(true);

            UnityWebRequest request = UnityWebRequestAssetBundle.GetAssetBundle(path);
            //progress.fillAmount = 0.3f;
            progress.fillAmount = request.downloadProgress;

            var operation = request.SendWebRequest();
            while (!operation.isDone)
            {               
                float downloadDataProgress = request.downloadProgress * 100;

                progress.fillAmount = downloadDataProgress / 100.0f;

                //print("Download: " + downloadDataProgress);
                yield return null;
            }

            //yield return request.SendWebRequest();

            if (request.isNetworkError || request.isHttpError)
            {
                Debug.Log(request.error);
            }
            else
            {
                myLoadedAssetBundle = DownloadHandlerAssetBundle.GetContent(request);
                for (int i = 0; i < treeNames.Count; i++)
                {
                    var tree = myLoadedAssetBundle.LoadAsset<GameObject>(treeNames[i] + season);

                    if (tree == null)
                    {
                        tree = myLoadedAssetBundle.LoadAsset<GameObject>("Other"+season);
                    }

                  
                    treeGos.Add(treeNames[i], tree);                    
                    //progress.fillAmount = i / float.Parse(treeNames.Count.ToString());
                    
                }
                
                progress.transform.parent.gameObject.SetActive(false);

                if (onTreesLoaded != null)
                {
                    onTreesLoaded();
                }
                
                myLoadedAssetBundle.Unload(false);
                // Frees the memory from the web stream
                request.Dispose();
            }

        }
    }
    /// <summary>
    /// Load asset bundle and cache to memory
    /// </summary>
    /// <param name="BundleURL"></param>
    /// <param name="season"></param>
    /// <param name="version"></param>
    /// <returns></returns>
    IEnumerator DownloadAndCacheAssetBundle(string BundleURL, string season, int version)
    {
        progress.fillAmount = 0;
        progress.transform.parent.gameObject.SetActive(true);
        // Wait for the Caching system to be ready
        while (!Caching.ready)
            yield return null;

        progress.fillAmount = 0.2f;

        // Load the AssetBundle file from Cache if it exists with the same version or download and store it in the cache
        using (WWW www = WWW.LoadFromCacheOrDownload(BundleURL, version))
        {
            yield return www;
            if (www.error != null)
                throw new Exception("WWW download had an error:" + www.error);
            myLoadedAssetBundle = www.assetBundle;
            //if (AssetName == "")
            //    Instantiate(myLoadedAssetBundle.mainAsset);
            //else
            //    Instantiate(myLoadedAssetBundle.LoadAsset(AssetName));

            for (int i = 0; i < treeNames.Count; i++)
            {
                var tree = myLoadedAssetBundle.LoadAsset<GameObject>(treeNames[i] + season);

                if (tree == null)
                {
                    tree = myLoadedAssetBundle.LoadAsset<GameObject>("Other" + season);
                }
                treeGos.Add(treeNames[i], tree);
                progress.fillAmount = i / float.Parse(treeNames.Count.ToString());
            }

            // Unload the AssetBundles compressed contents to conserve memory
            myLoadedAssetBundle.Unload(false);

            // Frees the memory from the web stream
            www.Dispose();
        } // memory is freed from the web stream (www.Dispose() gets called implicitly)
    }


    /// <summary>
    /// Change LOD percentages 
    /// </summary>
    /// <param name="go"></param>
    void ChangeLODsettings(GameObject go)
    {
#if UNITY_EDITOR
        GameObject nSp = Instantiate(go);
        nSp.name = go.name + "_L";
        LODGroup lodGroup = nSp.GetComponent<LODGroup>();
        if (lodGroup != null)
        {
            //Debug.Log(lodGroup.);

            SerializedObject obj = new SerializedObject(lodGroup);

            SerializedProperty valArrProp = obj.FindProperty("m_LODs.Array");
            for (int j = 0; valArrProp.arraySize > j; j++)
            {
                SerializedProperty sHeight = obj.FindProperty("m_LODs.Array.data[" + j.ToString() + "].screenRelativeHeight");

                if (j == 0)
                {
                    sHeight.doubleValue = 0.75;
                }
                if (j == 1)
                {
                    sHeight.doubleValue = 0.4;
                }
                if (j == 2)
                {
                    sHeight.doubleValue = 0.25;
                }
                if (j == 3)
                {
                    sHeight.doubleValue = 0.02;
                }
            }
            obj.ApplyModifiedProperties();
        }
#endif
    }

    #endregion

    #region Public Methods
    /// <summary>
    /// Populate trees for a given season from an asset bundle
    /// </summary>
    /// <param name="season"></param>

    public void PopulateTreesFromAssetBundle(string season)
    {
        treeGos.Clear();
        if (myLoadedAssetBundle != null)
        {
            myLoadedAssetBundle.Unload(true);
            Debug.Log("unload asset bundle");
        }

        currentSeason = season;

        string fileName = springFile;
        Debug.Log(season);
        if (season.Contains("_Spring"))
        {
            fileName = springFile;
        }
        else if (season.Contains("_Summer"))
        {
            fileName = summerFile;
        }
        else if (season.Contains("_Autumn"))
        {
            fileName = autumnFile;
        }
        else
        {
            fileName = winterFile;
        }


#if UNITY_EDITOR
        if (!String.IsNullOrEmpty(fileName))
        {
            var path1 = Path.Combine(Path.Combine(Application.streamingAssetsPath, "AssetBundles"), fileName);
            if (File.Exists(path1))
            {
                LoadFromAssetBundle(path1, season);
            }
        }

#elif UNITY_WEBGL
        //--------------------check path------------------------//
        if (!String.IsNullOrEmpty(fileName)){
            StartCoroutine(LoadFromAssetBundleWeb(Path.Combine(Path.Combine(Application.streamingAssetsPath, "AssetBundles"), fileName),season));
            //StartCoroutine(DownloadAndCacheAssetBundle(Path.Combine(Path.Combine(Application.streamingAssetsPath, "AssetBundles"), fileName),season,1));
        }
            
       
#endif
    }


    /// <summary>
    /// Show an indication when the internet connection is lost
    /// </summary>
    /// <param name="offline"></param>
    public void OnBrowserOffline(bool offline)
    {
        Debug.Log("trees offline");
        if (!offline)
        {
            browserOnline = false;
            Debug.Log(browserOnline);
        }
        else
        {
            if (!browserOnline)
            {
                if (progress.fillAmount > 0 && progress.fillAmount < 1)
                {
                    PopulateTreesFromAssetBundle(currentSeason);
                }
                browserOnline = true;
            }
            Debug.Log(browserOnline);
        }
    }
    #endregion
}

#region TREE SEASONS CLASS
public class TreeSeasons
{
    public GameObject spring;
    public GameObject summer;
    public GameObject autumn;
    public GameObject winter;
}
#endregion
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mapbox.Utils;
using Mapbox.Unity.Map;
/// <summary>
/// A component to visualise live weather data on the UI
/// </summary>
public class LiveWeatherUIController : MonoBehaviour
{
    #region Public Variables
    public WeatherAPI weatherAPI;
    public AbstractMap map;
    public Text text;
    public List<Button> weatherButtons = new List<Button>();
    #endregion

    #region Private Variables
    Dictionary<string, string> weather = new Dictionary<string, string>();
    #endregion

    #region Monobehaviour Methods
    // Start is called before the first frame update
    void Start()
    {
        WeatherAPI.onWeatherLoaded += VisualiseWeather;
    }
    #endregion

    #region Public Methods
    /// <summary>
    /// Initiate process to get live weather data
    /// </summary>
    public void GetLiveWeather()
    {
        if (map.CenterLatitudeLongitude.x != 0 && map.CenterLatitudeLongitude.y != 0)
        {
            weather = weatherAPI.LoadWeather(map.CenterLatitudeLongitude);
        }
        else
        {
            text.text = "Load map first!";
        }

    }
    /// <summary>
    /// Visualise received data on the UI
    /// </summary>
    public void VisualiseWeather()
    {
        text.text = weather["info"];

        for (int i = 0; i < weatherButtons.Count; i++)
        {
            //if (weatherButtons[i].name.Contains(weather["type"]))
            //{
            //    weatherButtons[i].onClick.Invoke();                
            //}

            if (weather["type"].Contains("broken") || weather["type"].Contains("clear") || weather["type"].Contains("scattered") || weather["type"].Contains("few"))
            {
                weatherButtons[0].onClick.Invoke();
            }
            else if (weather["type"].Contains("overcast"))
            {
                weatherButtons[1].onClick.Invoke();
            }
            else if (weather["type"].Contains("rain"))
            {
                if (weather["type"].Contains("light"))
                {
                    weatherButtons[2].onClick.Invoke();
                }
                else if (weather["type"].Contains("heavy"))
                {
                    weatherButtons[3].onClick.Invoke();
                }
            }
            else if (weather["type"].Contains("drizzle"))
            {
                weatherButtons[2].onClick.Invoke();
            }
            else if (weather["type"].Contains("snow"))
            {
                weatherButtons[4].onClick.Invoke();
            }
        }
    }
    #endregion
}

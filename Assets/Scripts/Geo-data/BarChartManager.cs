﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/// <summary>
/// A monobehaniour component to create a 3D barchart per year
/// </summary>
public class BarChartManager : MonoBehaviour
{
    #region Public Variables
    public string feature;
    public Transform co2Parent;
    public Transform pm10Parent;
    public Transform pm25Parent;
    public Transform noxParent;
    public Material material;
    public GameObject barprefab;
    #endregion

    #region Private Variables
    Dictionary<int, float> co2 = new Dictionary<int, float>();
    Dictionary<int, float> p10 = new Dictionary<int, float>();
    Dictionary<int, float> p25 = new Dictionary<int, float>();
    Dictionary<int, float> nox = new Dictionary<int, float>();
    float heightCo2 = 0;
    float heightP10 = 0;
    float heightP25 = 0;
    float heightNox = 0;
    #endregion

    #region Monobehaviour Methods
    // Start is called before the first frame update
    void Start()
    {
        MapHandler.mapLoaded += ClearYears;
        FeaturesManager.Instance.maps[1].OnUpdated += MapUpdated;
    }

    // Update is called once per frame
    void Update()
    {

    }
    #endregion

    #region Public Methods
    /// <summary>
    /// Collect data to generate chart
    /// </summary>
    /// <param name="datasetName"></param>
    /// <param name="year"></param>
    /// <param name="value"></param>
    public void CollectData(string datasetName, int year, float value)
    {
        StartCoroutine(GenerateBar(datasetName, year, value));


    }

    /// <summary>
    /// Clear bar chart
    /// </summary>
    public void ClearYears()
    {
        ClearParent(co2Parent);
        ClearParent(pm10Parent);
        ClearParent(pm25Parent);
        ClearParent(noxParent);
        heightCo2 = 0;
        heightNox = 0;
        heightP10 = 0;
        heightP25 = 0;
        co2.Clear();
        p10.Clear();
        p25.Clear();
        nox.Clear();
    }
    #endregion

    #region Private Methods
    /// <summary>
    /// Create bar chart
    /// </summary>
    /// <param name="datasetName"></param>
    /// <param name="year"></param>
    /// <param name="value"></param>
    /// <returns></returns>
    IEnumerator GenerateBar(string datasetName, int year, float value)
    {
        yield return new WaitForSeconds(3f);


        if (datasetName == "CO2")
        {
            if (!co2.ContainsKey(year))
            {

                GameObject newBar = Instantiate(barprefab);
                newBar.GetComponent<TextMeshPro>().text = year.ToString();
                newBar.transform.GetChild(0).localScale = new Vector3(50f, value, 50f);
                newBar.transform.parent = co2Parent;
                newBar.transform.localPosition = new Vector3(0, heightCo2, 0);
                if (value > 10)
                {
                    heightCo2 += value;
                }
                else
                {
                    heightCo2 += 10f;
                }


                co2.Add(year, value);

                int index = 0;
                for (int i = 0; i < FeaturesManager.Instance.mapData[feature].Legend.Count; i++)
                {
                    if (year == int.Parse(FeaturesManager.Instance.mapData[feature].Legend[i]))
                    {
                        index = i;
                    }
                }

                Color color = new Color();
                ColorUtility.TryParseHtmlString(FeaturesManager.Instance.mapData[feature].Colours[index + 1], out color);
                Material mat = Instantiate(material);
                mat.color = color;
                newBar.transform.GetChild(0).GetChild(0).GetComponent<MeshRenderer>().material = mat;

            }
            else if (co2.ContainsKey(year) && co2[year] < value)
            {
                co2[year] = value;
                for (int i = 0; i < co2Parent.childCount; i++)
                {
                    if (co2Parent.GetChild(i).GetComponent<TextMeshPro>().text == year.ToString())
                    {
                        float prevVal = co2Parent.GetChild(i).transform.GetChild(0).localScale.y;
                        co2Parent.GetChild(i).transform.GetChild(0).localScale = new Vector3(50f, value, 50f);
                        heightCo2 = heightCo2 - prevVal + value;
                    }
                }
            }

        }
        else if (datasetName == "PM10")
        {
            if (!p10.ContainsKey(year))
            {
                GameObject newBar = Instantiate(barprefab);
                newBar.GetComponent<TextMeshPro>().text = year.ToString();
                newBar.transform.GetChild(0).localScale = new Vector3(50f, value, 50f);
                newBar.transform.parent = pm10Parent;
                newBar.transform.localPosition = new Vector3(0, heightP10, 0);
                if (value > 10)
                {
                    heightP10 += value;
                }
                else
                {
                    heightP10 += 10f;
                }
                if (p10.ContainsKey(year))
                {
                    p10[year] = value;
                }
                else
                {
                    p10.Add(year, value);
                }
                int index = 0;
                for (int i = 0; i < FeaturesManager.Instance.mapData[feature].Legend.Count; i++)
                {
                    if (year == int.Parse(FeaturesManager.Instance.mapData[feature].Legend[i]))
                    {
                        index = i;
                    }
                }

                Color color = new Color();
                ColorUtility.TryParseHtmlString(FeaturesManager.Instance.mapData[feature].Colours[index + 1], out color);
                Material mat = Instantiate(material);
                mat.color = color;
                newBar.transform.GetChild(0).GetChild(0).GetComponent<MeshRenderer>().material = mat;

            }
            else if (p10.ContainsKey(year) && p10[year] < value)
            {
                p10[year] = value;
                for (int i = 0; i < pm10Parent.childCount; i++)
                {
                    if (pm10Parent.GetChild(i).GetComponent<TextMeshPro>().text == year.ToString())
                    {
                        float prevVal = pm10Parent.GetChild(i).transform.GetChild(0).localScale.y;
                        pm10Parent.GetChild(i).transform.GetChild(0).localScale = new Vector3(50f, value, 50f);
                        heightP10 = heightP10 - prevVal + value;
                    }
                }
            }

        }
        else if (datasetName == "PM25")
        {

            if (!p25.ContainsKey(year))
            {
                GameObject newBar = Instantiate(barprefab);
                newBar.GetComponent<TextMeshPro>().text = year.ToString();
                newBar.transform.GetChild(0).localScale = new Vector3(50f, value, 50f);
                newBar.transform.parent = pm25Parent;
                newBar.transform.localPosition = new Vector3(0, heightP25, 0);
                if (value > 10)
                {
                    heightP25 += value;
                }
                else
                {
                    heightP25 += 10f;
                }
                if (p25.ContainsKey(year))
                {
                    p25[year] = value;
                }
                else
                {
                    p25.Add(year, value);
                }
                int index = 0;
                for (int i = 0; i < FeaturesManager.Instance.mapData[feature].Legend.Count; i++)
                {
                    if (year == int.Parse(FeaturesManager.Instance.mapData[feature].Legend[i]))
                    {
                        index = i;
                    }
                }

                Color color = new Color();
                ColorUtility.TryParseHtmlString(FeaturesManager.Instance.mapData[feature].Colours[index + 1], out color);
                Material mat = Instantiate(material);
                mat.color = color;
                newBar.transform.GetChild(0).GetChild(0).GetComponent<MeshRenderer>().material = mat;
            }
            else if (p25.ContainsKey(year) && p25[year] < value)
            {
                p25[year] = value;
                for (int i = 0; i < pm25Parent.childCount; i++)
                {
                    if (pm25Parent.GetChild(i).GetComponent<TextMeshPro>().text == year.ToString())
                    {
                        float prevVal = pm25Parent.GetChild(i).transform.GetChild(0).localScale.y;
                        pm25Parent.GetChild(i).transform.GetChild(0).localScale = new Vector3(50f, value, 50f);
                        heightP25 = heightP25 - prevVal + value;
                    }
                }
            }


        }
        else if (datasetName == "NOx")
        {

            if (!nox.ContainsKey(year))
            {
                GameObject newBar = Instantiate(barprefab);
                newBar.GetComponent<TextMeshPro>().text = year.ToString();
                newBar.transform.GetChild(0).localScale = new Vector3(50f, value, 50f);
                newBar.transform.parent = noxParent;
                newBar.transform.localPosition = new Vector3(0, heightNox, 0);
                if (value > 10)
                {
                    heightNox += value;
                }
                else
                {
                    heightNox += 10f;
                }

                if (nox.ContainsKey(year))
                {
                    nox[year] = value;
                }
                else
                {
                    nox.Add(year, value);
                }


                int index = 0;
                for (int i = 0; i < FeaturesManager.Instance.mapData[feature].Legend.Count; i++)
                {
                    if (year == int.Parse(FeaturesManager.Instance.mapData[feature].Legend[i]))
                    {
                        index = i;
                    }
                }

                Color color = new Color();
                ColorUtility.TryParseHtmlString(FeaturesManager.Instance.mapData[feature].Colours[index + 1], out color);
                Material mat = Instantiate(material);
                mat.color = color;
                newBar.transform.GetChild(0).GetChild(0).GetComponent<MeshRenderer>().material = mat;
            }



        }
        else if (nox.ContainsKey(year) && nox[year] < value)
        {
            nox[year] = value;
            for (int i = 0; i < noxParent.childCount; i++)
            {
                if (noxParent.GetChild(i).GetComponent<TextMeshPro>().text == year.ToString())
                {
                    float prevVal = noxParent.GetChild(i).transform.GetChild(0).localScale.y;
                    noxParent.GetChild(i).transform.GetChild(0).localScale = new Vector3(50f, value, 50f);
                    heightNox = heightNox - prevVal + value;
                }
            }
        }
    }

    
    /// <summary>
    /// Delete children in parent
    /// </summary>
    /// <param name="parent"></param>
    void ClearParent(Transform parent)
    {
        for (int i = 1; i < parent.childCount; i++)
        {
            Destroy(parent.GetChild(i).gameObject);
        }
    }
    /// <summary>
    /// Run when map is updated
    /// </summary>
    void MapUpdated()
    {

    }
    #endregion
}

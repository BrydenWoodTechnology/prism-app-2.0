﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mapbox.Unity.MeshGeneration.Components;
using Mapbox.Examples;

/// <summary>
/// A MonoBehaviour component to change an item's material based on property value
/// </summary>
public class CustomMaterialModifier : MonoBehaviour
{
    #region Public Variables
    public Material singleMat;
    public Material singleOppositeMat;
    public Material doubleMat;
    public Material roadMat;
    public string parameter = "directiona";
    #endregion

    #region Monobehaviour Methods
    // Start is called before the first frame update
    void Start()
    {
        singleMat = Resources.Load<Material>("GeodataPrefabs/1wayarrow");
        singleOppositeMat = Resources.Load<Material>("GeodataPrefabs/1wayarrowOpp");
        doubleMat = Resources.Load<Material>("GeodataPrefabs/2wayarrow");
        roadMat = Resources.Load<Material>("GeodataPrefabs/BasicRoadMaterial");

        ChangeMaterial();
    }
    #endregion

    #region Private Methods
    /// <summary>
    /// Change object's material based on property
    /// </summary>
    void ChangeMaterial()
    {
        string value = GetComponent<FeatureBehaviour>().Data.Properties[parameter].ToString();
        //Debug.Log(value);
        Material[] materials = new Material[2];
        if (value.Contains("bothDirections"))
        {

            materials[0] = doubleMat;
            materials[1] = roadMat;
        }
        else if (value.Contains("inOppositeDirection"))
        {

            materials[0] = singleOppositeMat;
            materials[1] = roadMat;
        }
        else
        {
            materials[0] = singleMat;
            materials[1] = roadMat;
        }
        GetComponent<MeshRenderer>().materials = materials;
    }
#endregion
}

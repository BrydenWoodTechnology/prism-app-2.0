﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mapbox.Unity.MeshGeneration.Components;
using Mapbox.Examples;
/// <summary>
/// Component to change sprites based on data property
/// </summary>
public class SpriteSwapper : MonoBehaviour
{
    #region Public Variables
    public string parameter;
    public List<Texture> sprites = new List<Texture>();
    #endregion

    #region Monobehaviour Methods
    // Start is called before the first frame update
    void Start()
    {
        GetProperty();   
    }
    #endregion

    #region Private Methods
    /// <summary>
    /// Load a texture based on the object's data property
    /// </summary>
    void GetProperty()
    {
        bool found = false;
        string value = transform.parent.GetComponentInParent<FeatureBehaviour>().Data.Properties[parameter].ToString();
        for(int i=0; i<sprites.Count; i++)
        {
            if (value.Contains(sprites[i].name))
            {
                GetComponent<MeshRenderer>().material.mainTexture = sprites[i];
                found = true;
            }
        }

        if (!found)
        {
            gameObject.SetActive(false);
        }
    }
    #endregion
}

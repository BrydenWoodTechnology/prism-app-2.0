﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// A component to cale text based on distance from camera
/// </summary>
public class TextScaler : MonoBehaviour {
    #region Public Variables
    [Tooltip("Distance of the MeshObject with no calculateScale")]
    public float distance = 7.0f;
    public float minDistance = 100f;
#endregion

    #region Private Variables
    Vector3 startScale;
    #endregion

    #region Monobehaviour Methods
    void Start()
    {
        startScale = transform.localScale;
    }

    void Update()
    {
        scale();
    }
    #endregion

    #region Private Methods
    /// <summary>
    /// Scale text according to the camera distance
    /// </summary>
    void scale()
    {
        float dist = Vector3.Distance(Camera.main.transform.position, transform.position);
        if (dist > minDistance)
        {
            Vector3 newScale = startScale * (dist / distance);
            transform.localScale = newScale;
        }
       
    }
    #endregion
}

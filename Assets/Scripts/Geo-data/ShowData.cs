using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mapbox.Unity.MeshGeneration.Components;
using Mapbox.Examples;
using TMPro;
#region OBJECT TYPE
/// <summary>
/// Describes the type and ability to interact with the object
/// </summary>
public enum ObjectType{
    selectableObject,
    staticObject,
    staticDataObject
};
#endregion
/// <summary>
/// A component to store and control object properties
/// </summary>
public class ShowData : MonoBehaviour {
    #region Public Variables
    [Tooltip("Type of object")]
    public ObjectType objectType = ObjectType.selectableObject;

    [Tooltip("Parameter to look for")]
    public List<string> m_parameters;

    public string description;
    public bool movePrefab = false;
    public List<string> attributes = new List<string>();
    public List<GameObject> prefabs = new List<GameObject>();
    #endregion

    #region  Monobehaviour Methods
    // Use this for initialization
    void Start () {

       switch (objectType)
       {
            case ObjectType.selectableObject:
                transform.GetChild(0).gameObject.AddComponent<MyFeaturesSelectionDetector>();
                break;
            case ObjectType.staticDataObject:
                ExtractData();
                break;
       }
            
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    #endregion

    #region Public Methods
    /// <summary>
    ///  Show object's data as a 3D text
    /// </summary>
    /// <param name="parameter"></param>
    public void ShowObjectData(string parameter){
        string value = GetComponent<FeatureBehaviour>().Data.Properties[parameter].ToString();
       // Debug.Log(value);
        GameObject text = Resources.Load<GameObject>("GeodataPrefabs/LabelText");
        GameObject _text = Instantiate(text, transform);
        _text.GetComponent<TextMeshPro>().text = parameter + ":   " + value;
        _text.transform.parent = gameObject.transform;
        _text.transform.position = gameObject.transform.GetChild(0).position;
        _text.transform.position += new Vector3(0,20,0);
        StartCoroutine(CloseData(_text));
    }

    /// <summary>
    /// Translate data object
    /// </summary>
    public void MovePrefab()
    {
        if (movePrefab)
        {
            transform.GetChild(0).GetComponent<RectTransform>().position += new Vector3(0, transform.GetChild(1).transform.localScale.y + 15f, 0);
        }
    }
    #endregion

    #region Private Methods
    /// <summary>
    /// Turn off a data object
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    IEnumerator CloseData(GameObject obj)
    {
        yield return new WaitForSeconds(4f);
        Destroy(obj);
    }
    /// <summary>
    /// Extract data from the vector entity and reduce to single text description
    /// </summary>
    void ExtractData()
    {
        TextMeshPro text = transform.GetChild(0).GetComponent<TextMeshPro>();
        for (int i=0; i<m_parameters.Count; i++)
        {
            string value = GetComponentInParent<FeatureBehaviour>().Data.Properties[m_parameters[i]].ToString();            
            text.text += value.Replace('"', ' ') + " ";

            if (attributes.Count > 0)
            {
                for( int k=0; k<attributes.Count; k++)
                {
                    if (value.Contains(attributes[k]))
                    {
                        prefabs[k].SetActive(true);
                    }
                   
                }
            }
        }
        text.text += description;
    }

    #endregion
}


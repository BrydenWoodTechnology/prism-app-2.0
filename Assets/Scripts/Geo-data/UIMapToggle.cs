﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.InteropServices;
using BrydenWoodUnity.UIElements;
using Mapbox.Utils;
using Mapbox.Unity.Map;
/// <summary>
/// A component to control interactions for the toggles related to the map data layers
/// </summary>
public class UIMapToggle : MonoBehaviour {
    #region Imports
    [DllImport("__Internal")]
    private static extern void PopupOpenerCaptureClick(string link);
    #endregion

    #region Public Variables
    public Text layername;
    public Text layerInfo;
    public Image checkmark;
    public Toggle toggle;
    public GameObject error;
    public Button linkButton;
    public GameObject legend;
    public GameObject info;
    public GameObject attrText;
    public GameObject prefab3Dtoggle;
    public GameObject seasonsPanel;
    #endregion

    #region Monobehaviour Methods
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    #endregion

    #region Private Methods
    /// <summary>
    /// Create a legend for the given dataset
    /// </summary>
    /// <param name="feature"></param>
    /// <param name="geodata"></param>
    void ActivateLegend(string feature, Geodata geodata)
    {
        if (geodata.Legend != null)
        {
            legend.SetActive(true);
            GameObject item = Resources.Load<GameObject>("GeodataPrefabs/LegendItem");
            List<string> values = geodata.Legend;
            for (int i = 0; i < values.Count; i++)
            {
                GameObject _item = Instantiate(item, legend.transform);
                _item.GetComponentInChildren<Text>().text = values[i];
                Color color = new Color();
                ColorUtility.TryParseHtmlString(geodata.Colours[i + 1], out color);
                _item.GetComponentInChildren<Image>().color = color;
            }

            LayoutRebuilder.ForceRebuildLayoutImmediate(legend.GetComponentInParent<RectTransform>());
            LayoutRebuilder.ForceRebuildLayoutImmediate(GetComponentInParent<RectTransform>());
        }
    }
    
    #endregion

    #region Public Methods
    /// <summary>
    /// Initialise toggle data and generate relevant components
    /// </summary>
    /// <param name="datasetName"></param>
    /// <param name="datasetInfo"></param>
    /// <param name="colorText"></param>
    /// <param name="url"></param>
    /// <param name="geodata"></param>
    public void Initialise(string datasetName, string datasetInfo, string colorText, string url, Geodata geodata)
    {
        layername.text = datasetName.Replace('_',' ');
        layerInfo.text = datasetInfo;
        Color color = new Color();
        ColorUtility.TryParseHtmlString(colorText, out color);
        checkmark.color = color;
        
        ActivateLegend(datasetName, geodata);

        //linkButton.GetComponent<GlobalTooltip>().tooltipMessage = url;
        linkButton.onClick.AddListener(delegate
        {
            PopupOpenerCaptureClick(url);
        });

        if (geodata.Is3D)
        {
            prefab3Dtoggle.SetActive(true);
            prefab3Dtoggle.GetComponent<Toggle>().onValueChanged.AddListener(delegate
            {
                int index = geodata.MapIndex;
                VectorSubLayerProperties vectorSubLayerProperties = FeaturesManager.Instance.maps[index].VectorData.FindFeatureSubLayerWithName(geodata.UnityMapFeatures[1]);
                bool isactive = prefab3Dtoggle.GetComponent<Toggle>().isOn;
                vectorSubLayerProperties.SetActive(isactive);

            });
        }

        if (geodata.Seasons)
        {
            seasonsPanel.SetActive(true);
        }       

        LayoutRebuilder.ForceRebuildLayoutImmediate(info.GetComponent<RectTransform>());
        LayoutRebuilder.ForceRebuildLayoutImmediate(GetComponent<RectTransform>());
    }

    /// <summary>
    /// Change the name of an attribute
    /// </summary>
    /// <param name="attr"></param>
    public void ChangeAttributeText(string attr)
    {
        attrText.GetComponent<Text>().text = attr;
        attrText.SetActive(true);
    }
    #endregion
}

﻿//-----------------------------------------------------------------------
// <copyright file="LineMeshModifier.cs" company="Mapbox">
//     Copyright (c) 2016 Mapbox. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using Mapbox.Unity.Map;

namespace Mapbox.Unity.MeshGeneration.Modifiers
{
    using System.Collections.Generic;
    using UnityEngine;
    using Mapbox.Unity.MeshGeneration.Data;
    /// <summary>
    /// A MeshModifier class to modify line based on a data property
    /// </summary>
    [CreateAssetMenu(menuName = "Mapbox/Modifiers/Attribute Based Line Modifier")]
    public class AttributeBasedLineModifier : MeshModifier
    {
        #region Public Variables
        [SerializeField]
        AttributeBasedLineOptions _options;
        public override ModifierType Type { get { return ModifierType.Preprocess; } }
        #endregion

        #region Private Variables
        private float _scaledWidth;       
        private float _scale = 1;
        private float height = 2;
        private int _counter = 0;
        #endregion

        #region Public Methods
        /// <summary>
        /// Set modifier properties
        /// </summary>
        /// <param name="properties"></param>
        public override void SetProperties(ModifierProperties properties)
        {
            _options = (AttributeBasedLineOptions)properties;
            _options.PropertyHasChanged += UpdateModifier;

        }
        /// <summary>
        /// Run modifier
        /// </summary>
        /// <param name="feature"></param>
        /// <param name="md"></param>
        /// <param name="scale"></param>
        public override void Run(VectorFeatureUnity feature, MeshData md, float scale)
        {            
            if (feature.Data.GetProperties().ContainsKey(_options.Parameter))
            {
                string value = feature.Data.GetProperties()[_options.Parameter].ToString();
               // Debug.Log(value);
                if (value == "")
                {
                    value = "1m";
                }
                string[] _width = value.Split('m');
                _scaledWidth = float.Parse(_width[0]) * scale;
            }
            else
            {
                _scaledWidth = _options.Width * scale;
            }
            //Debug.Log(_scaledWidth);
            ExtureLine(feature, md);
        }
        /// <summary>
        /// Get scale factor based on property value
        /// </summary>
        /// <param name="feature"></param>
        /// <param name="md"></param>
        /// <param name="tile"></param>
        public override void Run(VectorFeatureUnity feature, MeshData md, UnityTile tile = null)
        {
            if (feature.Data.GetProperties().ContainsKey(_options.Parameter) )
            {
                string value = feature.Data.GetProperties()[_options.Parameter].ToString();
                //Debug.Log(value);
                if (value == "")
                {
                    value = "1m";
                }
                string[] _width = value.Split('m');
                _scaledWidth = tile != null ? float.Parse(_width[0]) * tile.TileScale : float.Parse(_width[0]);
            }
            else
            {
                _scaledWidth = tile != null ? _options.Width * tile.TileScale : _options.Width;
            }
           // Debug.Log(_scaledWidth);
            ExtureLine(feature, md);
        }

        /// <summary>
        /// Extrude based on property
        /// </summary>
        /// <param name="feature"></param>
        /// <param name="md"></param>
        /// <param name="tile"></param>
        public void Extrude(VectorFeatureUnity feature, MeshData md, UnityTile tile = null)
        {
            _counter = 0;
            if (md.Vertices.Count == 0 || feature == null || feature.Points.Count < 1)
                return;

            if (tile != null)
                _scale = tile.TileScale;

            float maxHeight = 1.0f;
            float minHeight = 0.0f;


            //Modificatipon to extrude based on attribute
            maxHeight = 2 * _scale;
            //if (feature.Data.GetProperties().ContainsKey(_options.Parameter))
            //{
            //    string value = feature.Data.GetProperties()[_options.Parameter].ToString();
            //    // Debug.Log(value);
            //    if (value == "")
            //    {
            //        value = "1m";
            //    }
            //    string[] _width = value.Split('m');
            //    maxHeight = float.Parse(_width[0]);
            //}
            //else
            //{
            //    maxHeight = 2 * _scale;
            //}
            minHeight = 1 * _scale;
            height = (maxHeight - minHeight);

            //Set roof height 
            GenerateRoofMesh(md, minHeight, maxHeight);

            GenerateWallMesh(md);

        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Extrude line 
        /// </summary>
        /// <param name="feature"></param>
        /// <param name="md"></param>
        private void ExtureLine(VectorFeatureUnity feature, MeshData md)
        {
            if (feature.Points.Count < 1)
                return;

            foreach (var roadSegment in feature.Points)
            {
                var mdVertexCount = md.Vertices.Count;
                var roadSegmentCount = roadSegment.Count;
                for (int i = 1; i < roadSegmentCount * 2; i++)
                {
                    md.Edges.Add(mdVertexCount + i);
                    md.Edges.Add(mdVertexCount + i - 1);
                }
                md.Edges.Add(mdVertexCount);
                md.Edges.Add(mdVertexCount + (roadSegmentCount * 2) - 1);

                var newVerticeList = new Vector3[roadSegmentCount * 2];
                var newNorms = new Vector3[roadSegmentCount * 2];
                var uvList = new Vector2[roadSegmentCount * 2];
                var newTangents = new Vector4[roadSegmentCount * 2];
                Vector3 norm;
                var lastUv = 0f;
                var p1 = Constants.Math.Vector3Zero;
                var p2 = Constants.Math.Vector3Zero;
                var p3 = Constants.Math.Vector3Zero;
                for (int i = 1; i < roadSegmentCount; i++)
                {
                    p1 = roadSegment[i - 1];
                    p2 = roadSegment[i];
                    p3 = p2;
                    if (i + 1 < roadSegmentCount)
                        p3 = roadSegment[i + 1];

                    if (i == 1)
                    {
                        norm = GetNormal(p1, p1, p2) * _scaledWidth; //road width
                        newVerticeList[0] = (p1 + norm);
                        newVerticeList[roadSegmentCount * 2 - 1] = (p1 - norm);
                        newNorms[0] = Constants.Math.Vector3Up;
                        newNorms[roadSegmentCount * 2 - 1] = Constants.Math.Vector3Up;
                        uvList[0] = new Vector2(0, 0);
                        uvList[roadSegmentCount * 2 - 1] = new Vector2(1, 0);
                        newTangents[0] = new Vector4(p2.x - p1.x, p2.y - p1.y, p2.z - p1.z, 1).normalized;
                        newTangents[roadSegmentCount * 2 - 1] = newTangents[0];
                    }

                    var dist = Vector3.Distance(p1, p2);
                    lastUv += dist;
                    norm = GetNormal(p1, p2, p3) * _scaledWidth;
                    newVerticeList[i] = (p2 + norm);
                    newVerticeList[2 * roadSegmentCount - 1 - i] = (p2 - norm);
                    newNorms[i] = Constants.Math.Vector3Up;
                    newNorms[2 * roadSegmentCount - 1 - i] = Constants.Math.Vector3Up;

                    uvList[i] = new Vector2(0, lastUv);
                    uvList[2 * roadSegmentCount - 1 - i] = new Vector2(1, lastUv);

                    newTangents[i] = new Vector4(p2.x - p1.x, p2.y - p1.y, p2.z - p1.z, 1).normalized;
                    newTangents[2 * roadSegmentCount - 1 - i] = newTangents[i];
                }

                md.Vertices.AddRange(newVerticeList);
                md.Normals.AddRange(newNorms);
                md.UV[0].AddRange(uvList);
                md.Tangents.AddRange(newTangents);
                var lineTri = new List<int>();
                var n = roadSegmentCount;

                for (int i = 0; i < n - 1; i++)
                {
                    lineTri.Add(mdVertexCount + i);
                    lineTri.Add(mdVertexCount + i + 1);
                    lineTri.Add(mdVertexCount + 2 * n - 1 - i);

                    lineTri.Add(mdVertexCount + i + 1);
                    lineTri.Add(mdVertexCount + 2 * n - i - 2);
                    lineTri.Add(mdVertexCount + 2 * n - i - 1);
                }

                if (md.Triangles.Count < 1)
                    md.Triangles.Add(new List<int>());
                md.Triangles[0].AddRange(lineTri);
            }

            Extrude(feature, md);
        }

        /// <summary>
        /// Get normal
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="newPos"></param>
        /// <param name="p2"></param>
        /// <returns></returns>
        private Vector3 GetNormal(Vector3 p1, Vector3 newPos, Vector3 p2)
        {
            if (newPos == p1 || newPos == p2)
            {
                var n = (p2 - p1).normalized;
                return new Vector3(-n.z, 0, n.x);
            }

            var b = (p2 - newPos).normalized + newPos;
            var a = (p1 - newPos).normalized + newPos;
            var t = (b - a).normalized;

            if (t == Mapbox.Unity.Constants.Math.Vector3Zero)
            {
                var n = (p2 - p1).normalized;
                return new Vector3(-n.z, 0, n.x);
            }

            return new Vector3(-t.z, 0, t.x);
        }
             
        /// <summary>
        /// Create sides
        /// </summary>
        /// <param name="md"></param>
        protected virtual void GenerateWallMesh(MeshData md)
        {
            md.Vertices.Capacity = _counter + md.Edges.Count * 2;
            float d = 0f;
            Vector3 v1;
            Vector3 v2;
            int ind = 0;
            Vector3 wallDir;

            //if (_options.extrusionGeometryType != ExtrusionGeometryType.RoofOnly)
            //{
            //    _counter = md.Edges.Count;
            //    var wallTri = new List<int>(_counter * 3);
            //    var wallUv = new List<Vector2>(_counter * 2);
            //    Vector3 norm = Constants.Math.Vector3Zero;

            //    md.Vertices.Capacity = md.Vertices.Count + _counter * 2;
            //    md.Normals.Capacity = md.Normals.Count + _counter * 2;

            //    for (int i = 0; i < _counter; i += 2)
            //    {
            //        v1 = md.Vertices[md.Edges[i]];
            //        v2 = md.Vertices[md.Edges[i + 1]];
            //        ind = md.Vertices.Count;
            //        md.Vertices.Add(v1);
            //        md.Vertices.Add(v2);
            //        md.Vertices.Add(new Vector3(v1.x, v1.y - height, v1.z));
            //        md.Vertices.Add(new Vector3(v2.x, v2.y - height, v2.z));

            //        //d = (v2 - v1).magnitude;
            //        d = Mathf.Sqrt((v2.x - v1.x) + (v2.y - v1.y) + (v2.z - v1.z));
            //        norm = Vector3.Normalize(Vector3.Cross(v2 - v1, md.Vertices[ind + 2] - v1));
            //        md.Normals.Add(norm);
            //        md.Normals.Add(norm);
            //        md.Normals.Add(norm);
            //        md.Normals.Add(norm);

            //        wallDir = (v2 - v1).normalized;
            //        md.Tangents.Add(wallDir);
            //        md.Tangents.Add(wallDir);
            //        md.Tangents.Add(wallDir);
            //        md.Tangents.Add(wallDir);

            //        wallUv.Add(new Vector2(0, 0));
            //        wallUv.Add(new Vector2(d, 0));
            //        wallUv.Add(new Vector2(0, -height));
            //        wallUv.Add(new Vector2(d, -height));

            //        wallTri.Add(ind);
            //        wallTri.Add(ind + 1);
            //        wallTri.Add(ind + 2);

            //        wallTri.Add(ind + 1);
            //        wallTri.Add(ind + 3);
            //        wallTri.Add(ind + 2);
            //    }

            //    // TODO: Do we really need this?
            //    if (_separateSubmesh)
            //    {
            //        md.Triangles.Add(wallTri);
            //    }
            //    else
            //    {
            //        md.Triangles.Capacity = md.Triangles.Count + wallTri.Count;
            //        md.Triangles[0].AddRange(wallTri);
            //    }
            //    md.UV[0].AddRange(wallUv);
            //}
        }
        /// <summary>
        /// Create top
        /// </summary>
        /// <param name="md"></param>
        /// <param name="minHeight"></param>
        /// <param name="maxHeight"></param>
        protected virtual void GenerateRoofMesh(MeshData md, float minHeight, float maxHeight)
        {
           
                _counter = md.Vertices.Count;
              
                for (int i = 0; i < _counter; i++)
                {
                   md.Vertices[i] = new Vector3(md.Vertices[i].x, md.Vertices[i].y + maxHeight, md.Vertices[i].z);
                }
                       
                
            
        }

        #endregion
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mapbox.Unity.MeshGeneration.Components;
/// <summary>
/// A component to pass extrusion data based on a given parameter.
/// </summary>
public class ExtrudePrefab : MonoBehaviour
{
    #region Public Variables
    public string parameter;
    #endregion

    #region Monobehaviour Methods
    // Start is called before the first frame update
    void Start()
    {
        Extrude();
    }
    #endregion

    #region Private Methods
    /// <summary>
    /// Gets the remapped value and scales the object accordingly
    /// </summary>
    void Extrude()
    {       
        float value = float.Parse(transform.parent.GetComponentInParent<FeatureBehaviour>().Data.Properties[parameter].ToString());       
        float remapValue = value.Remap(1.5f, 26f, 15, 50);
        transform.localScale += new Vector3(0,remapValue, 0);
        transform.position += new Vector3(0, remapValue / 2f, 0);
        transform.GetComponentInParent<ShowData>().MovePrefab();
    }
    #endregion#
}

﻿//-----------------------------------------------------------------------
// <copyright file="GameObjectModifier.cs" company="Mapbox">
//     Copyright (c) 2016 Mapbox. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mapbox.Unity.MeshGeneration.Data;
using Mapbox.Unity.MeshGeneration.Components;
using Mapbox.Unity.MeshGeneration.Interfaces;
using Mapbox.Unity.MeshGeneration.Modifiers;
using System.Collections.Generic;
using Mapbox.Unity.Map;
using System;
using TMPro;
/// <summary>
/// A Game ObjectModifier class to create a bar chart
/// </summary>
[CreateAssetMenu(menuName = "Mapbox/Modifiers/Bar Chart Prefab Modifier")]
public class GeodataBarChartModifier : GameObjectModifier
{
    #region Public Variables
    public string datasetName;
    public string feature;
    public List<string> parameters = new List<string>();
    public float offset;
    public float domainStart;
    public float domainEnd;
    //public GameObject prefab;
    #endregion

    #region Private Variables
    Dictionary<int, float> values = new Dictionary<int, float>();

    private Dictionary<GameObject, GameObject> _objects;
    [SerializeField]
    private SpawnPrefabOptions _options;
    private List<GameObject> _prefabList = new List<GameObject>();
    #endregion

    #region Public Methods
    /// <summary>
    /// Initialize the data structure
    /// </summary>
    public override void Initialize()
    {
        if (_objects == null)
        {
            _objects = new Dictionary<GameObject, GameObject>();
        }
    }
    /// <summary>
    /// Set modifier's properties
    /// </summary>
    /// <param name="properties"></param>
    public override void SetProperties(ModifierProperties properties)
    {
        _options = (SpawnPrefabOptions)properties;
        _options.PropertyHasChanged += UpdateModifier;
    }
    /// <summary>
    /// Create the chart for all objects and all tiles 
    /// </summary>
    /// <param name="ve"></param>
    /// <param name="tile"></param>
    public override void Run(VectorEntity ve, UnityTile tile)
    {
        if (_options.prefab == null)
        {
            return;
        }

        GameObject go = null;

        if (_objects.ContainsKey(ve.GameObject))
        {
            go = _objects[ve.GameObject];
        }
        else
        {
            go = Instantiate(_options.prefab);
            _prefabList.Add(go);
            _objects.Add(ve.GameObject, go);
            go.transform.SetParent(ve.GameObject.transform, false);
        }

        PositionScaleRectTransform(ve, tile, go);
        CollectData(go);

        if (_options.AllPrefabsInstatiated != null)
        {
            _options.AllPrefabsInstatiated(_prefabList);
        }
    }
    /// <summary>
    /// Scale object according to the mapped value
    /// </summary>
    /// <param name="ve"></param>
    /// <param name="tile"></param>
    /// <param name="go"></param>
    public void PositionScaleRectTransform(VectorEntity ve, UnityTile tile, GameObject go)
    {
        RectTransform goRectTransform;
        IFeaturePropertySettable settable = null;
        var centroidVector = new Vector3();
        foreach (var point in ve.Feature.Points[0])
        {
            centroidVector += point;
        }
        centroidVector = centroidVector / ve.Feature.Points[0].Count;

        go.name = ve.Feature.Data.Id.ToString();

        goRectTransform = go.GetComponent<RectTransform>();
        if (goRectTransform == null)
        {
            go.transform.localPosition = centroidVector;
            if (_options.scaleDownWithWorld)
            {
                go.transform.localScale = _options.prefab.transform.localScale * (tile.TileScale);
            }
        }
        else
        {
            goRectTransform.anchoredPosition3D = centroidVector;
            if (_options.scaleDownWithWorld)
            {
                goRectTransform.localScale = _options.prefab.transform.localScale * (tile.TileScale);
            }
        }

        //go.transform.localScale = Constants.Math.Vector3One;

        settable = go.GetComponent<IFeaturePropertySettable>();
        if (settable != null)
        {
            settable.Set(ve.Feature.Properties);
        }
    }
    /// <summary>
    /// Clear cache and data structures
    /// </summary>
    public override void ClearCaches()
    {
        base.ClearCaches();
        foreach (var gameObject in _objects.Values)
        {
            Destroy(gameObject);
        }

        foreach (var gameObject in _prefabList)
        {
            Destroy(gameObject);
        }
    }
    /// <summary>
    /// Gather all data for the chart
    /// </summary>
    /// <param name="bar"></param>
    public void CollectData(GameObject bar)
    {
        for(int i=0; i<parameters.Count; i+=2)
        {
            int par = int.Parse(bar.GetComponentInParent<FeatureBehaviour>().Data.Properties[parameters[i]].ToString());
            Debug.Log(par);
            Debug.Log(parameters[i + 1]);
            float value = float.Parse(bar.GetComponentInParent<FeatureBehaviour>().Data.Properties[parameters[i+1]].ToString());
            if (!values.ContainsKey(par))
            {
                values.Add(par, value);
            }            
        }

        //GameObject bar = Instantiate(prefab, transform);
        
        float height = 0;
        foreach(KeyValuePair<int,float> keyValuePair in values)
        {
            if (bar.transform.childCount == 2)
            {
                bar.transform.GetChild(1).GetComponent<TextMeshPro>().text = keyValuePair.Key.ToString();
                float remapValue = keyValuePair.Value.Remap(domainStart, domainEnd, 0f, 10f);
                bar.transform.GetChild(1).GetChild(0).localScale = new Vector3(4f, remapValue, 4f);
                height += keyValuePair.Value;
            }
            else
            {
                GameObject newBar = Instantiate(bar.transform.GetChild(1).gameObject, bar.transform);
                newBar.GetComponent<TextMeshPro>().text = keyValuePair.Key.ToString();
                float remapValue = keyValuePair.Value.Remap(domainStart, domainEnd, 0f, 10f);
                newBar.transform.GetChild(0).localScale = new Vector3(4f, remapValue, 4f);
                newBar.transform.position += new Vector3(0, height, 0);
                height += keyValuePair.Value;
            }
        }

        bar.transform.position += new Vector3(offset, 0, 0);
    }
    #endregion
}

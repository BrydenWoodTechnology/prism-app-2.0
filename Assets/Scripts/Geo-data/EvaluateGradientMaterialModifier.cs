﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mapbox.Unity.MeshGeneration.Components;
using Mapbox.Examples;

/// <summary>
/// A component to evaluate property value in colour of gradient
/// </summary>
public class EvaluateGradientMaterialModifier : MonoBehaviour
{
    #region Public parameters
    public string parameter;
    public Gradient gradient;
    #endregion

    #region Monobehaviour Methods
    // Start is called before the first frame update
    void Start()
    {
        ChangeColour();       
    }

    private void Update()
    {
       
    }
    #endregion

    #region Public Methods
    /// <summary>
    /// Change the object's colour based on given parameter and gradient
    /// </summary>
    public void ChangeColour()
    {
        if (GetComponentInParent<FeatureBehaviour>().Data.Properties.ContainsKey(parameter))
        {
            string value = GetComponentInParent<FeatureBehaviour>().Data.Properties[parameter].ToString();
            float colorValue = 0;
            if (value != "")
            {
                string[] width = value.Split('m');
                colorValue = float.Parse(width[0]).Remap(2, 25, 0, 1);
            }

            Color color = gradient.Evaluate(colorValue);
            GetComponentInParent<MeshRenderer>().material.color = color;
        }
    }
    #endregion
}

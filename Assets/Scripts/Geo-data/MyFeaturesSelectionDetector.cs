﻿//-----------------------------------------------------------------------
// <copyright file="FeaturesSelectionDetector.cs" company="Mapbox">
//     Copyright (c) 2016 Mapbox. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Mapbox.Examples
{
    using UnityEngine;
    using Mapbox.Unity.MeshGeneration.Data;
    /// <summary>
    /// A component to detect mouse selection
    /// </summary>
    public class MyFeaturesSelectionDetector : MonoBehaviour
    {
        #region Private Variables
        private FeatureUiMarker _marker;
        private VectorEntity _feature;
        #endregion

        #region Public Methods
        public void OnMouseUpAsButton()
        {
            //_marker.Show(_feature);
            //if(transform.parent.tag=="Tree")
            //{
            //    transform.parent.GetComponent<ShowData>().ShowObjectData("species_name");
            //}
            
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Initialise marker and feature
        /// </summary>
        /// <param name="marker"></param>
        /// <param name="ve"></param>
        internal void Initialize(FeatureUiMarker marker, VectorEntity ve)
        {
            _marker = marker;
            _feature = ve;
        }
        #endregion
    }
}

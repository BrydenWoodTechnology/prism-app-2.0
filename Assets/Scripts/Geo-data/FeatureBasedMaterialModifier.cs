﻿//-----------------------------------------------------------------------
// <copyright file="MaterialModifier.cs" company="Mapbox">
//     Copyright (c) 2016 Mapbox. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Mapbox.Unity.MeshGeneration.Modifiers
{
    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;
    using Mapbox.Unity.MeshGeneration.Components;
    using Mapbox.Unity.MeshGeneration.Data;
    using Mapbox.Unity.Map;
    using System;

    #region Public Enums
    /// <summary>
    /// Options for different visualisation types of the feature
    /// </summary>
    public enum VisualisationType
    {
        colourProperty,
        gradientProperty,
        multiMaterialProperty,
        calculatedProperty
    }
    #endregion
    /// <summary>
    /// Component based on Mapbox GameObjectModifier
    /// Texture Modifier is a basic modifier which simply adds a TextureSelector script to the features.
    /// Logic is all pushed into this TextureSelector mono behaviour to make it's easier to change it in runtime.
    /// </summary>
    [CreateAssetMenu(menuName = "Mapbox/Modifiers/Land Material Modifier")]
    public class FeatureBasedMaterialModifier : GameObjectModifier
    {
        #region Public Variables
        [SerializeField]
        GeometryMaterialOptions _options;

        [Header("Visualisation Settings")]
        public VisualisationType VisualisationType;
        public string feature;
        public string parameter;       
        public Material material;
        [Header("Gradient Property Case Settings")]
        public Gradient gradient;
        public float domainStart= 2f;
        public float domainEnd = 25f;
        [Header("Multi Material Property Case Settings")]
        public List<Material> materials = new List<Material>();
        public List<string> conditions = new List<string>();
        #endregion

        #region Public Methods
        /// <summary>
        /// Set modifier's properties
        /// </summary>
        /// <param name="properties"></param>
        public override void SetProperties(ModifierProperties properties)
        {
            _options = (GeometryMaterialOptions)properties;
            _options.PropertyHasChanged += UpdateModifier;
        }
        /// <summary>
        /// Unlink properties
        /// </summary>
        public override void UnbindProperties()
        {
            _options.PropertyHasChanged -= UpdateModifier;
        }
        /// <summary>
        /// Get the object's render mode as stated in properties
        /// </summary>
        /// <param name="val"></param>
        /// <returns>(float)  render mode</returns>
        private float GetRenderMode(float val)
        {
            return Mathf.Approximately(val, 1.0f) ? 0f : 3f;
        }
        /// <summary>
        /// Change material based on computed parameter
        /// </summary>
        /// <param name="ve"></param>
        /// <param name="tile"></param>
        public override void Run(VectorEntity ve, UnityTile tile)
        {
            var min = Math.Min(_options.materials.Length, ve.MeshFilter.mesh.subMeshCount);
            var mats = new Material[min];

            _options.style = StyleTypes.Color;

            if (_options.style == StyleTypes.Custom)
            {
                for (int i = 0; i < min; i++)
                {
                    mats[i] = _options.customStyleOptions.materials[i].Materials[UnityEngine.Random.Range(0, _options.customStyleOptions.materials[i].Materials.Length)];
                }
            }
            else if (_options.style == StyleTypes.Satellite)
            {
                for (int i = 0; i < min; i++)
                {
                    mats[i] = _options.materials[i].Materials[UnityEngine.Random.Range(0, _options.materials[i].Materials.Length)];
                }

                mats[0].mainTexture = tile.GetRasterData();
                mats[0].mainTextureScale = new Vector2(1f, 1f);
            }
            else
            {
                float renderMode = 0.0f;
                switch (_options.style)
                {
                    case StyleTypes.Light:
                        renderMode = GetRenderMode(_options.lightStyleOpacity);
                        break;
                    case StyleTypes.Dark:
                        renderMode = GetRenderMode(_options.darkStyleOpacity);
                        break;
                    case StyleTypes.Color:
                        renderMode = GetRenderMode(_options.colorStyleColor.a);
                        break;
                    default:
                        break;
                }
                for (int i = 0; i < min; i++)
                {
                    //mats[i] = _options.materials[i].Materials[UnityEngine.Random.Range(0, _options.materials[i].Materials.Length)];
                    //mats[i].SetFloat("_Mode", renderMode);
                    switch (VisualisationType)
                    {
                        case VisualisationType.colourProperty:
                            string value = ve.Feature.Data.GetProperties()[parameter].ToString();
                            //Debug.Log(value);
                            int index = 0;
                            for (int j = 0; j < FeaturesManager.Instance.mapData[feature].Legend.Count; j++)
                            {
                                if (value.Contains(FeaturesManager.Instance.mapData[feature].Legend[j]))
                                {
                                    index = j;                                    
                                }
                            }

                            Color color = new Color();
                            ColorUtility.TryParseHtmlString(FeaturesManager.Instance.mapData[feature].Colours[index + 1], out color);
                            mats[i] = Instantiate(material);
                            mats[i].color = color;
                            break;

                        case VisualisationType.gradientProperty:                           
                            if (ve.Feature.Data.GetProperties().ContainsKey(parameter))
                            {
                                string _value = ve.Feature.Data.GetProperties()[parameter].ToString();                                
                                float colorValue = 0;
                                if (_value != "")
                                {
                                    string[] width = _value.Split('m');
                                    colorValue = float.Parse(width[0]).Remap(domainStart, domainEnd, 0, 1);
                                    //Debug.Log(colorValue);
                                }
                                Color colour = gradient.Evaluate(colorValue);
                                mats[i] = Instantiate(material);
                                mats[i].color = colour;
                            }
                            else
                            {
                                mats[i] = Instantiate(material);
                            }
                            break;

                        case VisualisationType.multiMaterialProperty:
                            string value_ = ve.Feature.Data.GetValue(parameter).ToString();
                            bool found = false;
                            for(int k=0; k<conditions.Count; k++)
                            {
                                if (value_.Contains(conditions[k]))
                                {
                                    mats[i] = materials[k];
                                    found = true;
                                }                               
                            }
                            if (!found)
                            {
                                mats[i] = material;
                            }      
                            break;

                        case VisualisationType.calculatedProperty:                            
                            string m_value = ve.Feature.Data.GetProperties()[parameter].ToString();
                            float floors = float.Parse(m_value) / 4.0f;
                           // Debug.Log(floors);
                            int _index = 0;
                            bool valuefound = false;
                            for (int j = 0; j < FeaturesManager.Instance.mapData[feature].Legend.Count; j++)
                            {
                                if (FeaturesManager.Instance.mapData[feature].Legend[j].Contains("-"))
                                {
                                    string[] bounds = FeaturesManager.Instance.mapData[feature].Legend[j].Split('-');
                                    if (floors > int.Parse(bounds[0]) && floors <= int.Parse(bounds[1]))
                                    {
                                        _index = j;
                                        //Debug.Log(_index);
                                        valuefound = true;
                                    }
                                }
                               
                            }
                            if (!valuefound)
                            {
                                _index = FeaturesManager.Instance.mapData[feature].Legend.Count - 1;
                            }

                            Color _color = new Color();
                            ColorUtility.TryParseHtmlString(FeaturesManager.Instance.mapData[feature].Colours[_index + 1], out _color);
                            mats[i] = Instantiate(material);
                            mats[i].color = _color;
                            break;
                    }
                   
                }
            }
            ve.MeshRenderer.materials = mats;
        }
    }
    #endregion

}


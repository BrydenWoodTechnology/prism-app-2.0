﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Classs for utility methods
/// </summary>
public static class ExtensionMethods
{
    #region Public Mehods
    /// <summary>
    /// Remap domain
    /// </summary>
    /// <param name="value"></param>
    /// <param name="from1"></param>
    /// <param name="to1"></param>
    /// <param name="from2"></param>
    /// <param name="to2"></param>
    /// <returns></returns>
    public static float Remap(this float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }
    #endregion
}



﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// A component to jitter and object's scale to create a more natural effect in a dataset
/// </summary>
public class ScaleJitter : MonoBehaviour {
    #region Public Variables
    public double height;
    #endregion

    #region Monobehaviour Methods
    // Use this for initialization
    void Start () {
        if(transform.GetChild(0).localScale == new Vector3(1,1,1))
            jitterHeight();
    }
    #endregion

    #region Private Methods
    /// <summary>
    /// Scale object with a random factor
    /// </summary>
    void jitterHeight()
    {
        //height = UnityEngine.Random.value / 100;
        //double newHeight = 1 + height;
        //transform.GetChild(0).localScale = new Vector3(0, float.Parse(newHeight.ToString()), 0);
    }
    #endregion
}

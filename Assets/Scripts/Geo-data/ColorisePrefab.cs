﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mapbox.Utils;
using Mapbox.Unity.Map;
using Mapbox.Unity.MeshGeneration.Factories;
using Mapbox.Unity.Utilities;
using Mapbox.Unity.MeshGeneration.Components;
using Mapbox.Examples;
using TMPro;
/// <summary>
/// A monobehaviour component to change an item's colur based on property value
/// </summary>
public class ColorisePrefab : MonoBehaviour {
    #region Public Variables
    public string parameter;
    public string feature;
    #endregion

    #region Monobehaviour Methods
    // Use this for initialization
    void Start () {
        Colorise();
	}
    #endregion

    #region Private Methods
    /// <summary>
    /// Change item's colour
    /// </summary>
    void Colorise()
    {
        string value = GetComponentInParent<FeatureBehaviour>().Data.Properties[parameter].ToString();
        //Debug.Log(FeaturesManager.Instance.mapData.Count);
        int index = FeaturesManager.Instance.mapData[feature].Legend.IndexOf(value);
        Color color = new Color();
        ColorUtility.TryParseHtmlString(FeaturesManager.Instance.mapData[feature].Colours[index + 1], out color);
        gameObject.GetComponent<MeshRenderer>().material.color = color;
    }
    #endregion
}

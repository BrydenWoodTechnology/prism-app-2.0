﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// A component to control light data
/// </summary>
public class LightModifier : MonoBehaviour
{
    #region Public Variables
    public GameObject light;
    #endregion

    #region Monobehaviour Methods
    // Start is called before the first frame update
    void Start()
    {
        
    }    

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(transform.position, Camera.main.transform.position) < 200)
        {
            light.SetActive(true);
        }
        else
        {
            light.SetActive(false);
        }
    }
    #endregion

    #region Public Methods
    /// <summary>
    /// Activate/ deactivate light on this location
    /// </summary>
    /// <param name="isOn"></param>
    public void ActivateLight(bool isOn)
    {
        light.SetActive(isOn);
    }
    #endregion
}

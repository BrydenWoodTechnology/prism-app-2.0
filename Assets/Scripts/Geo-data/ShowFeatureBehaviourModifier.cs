﻿//-----------------------------------------------------------------------
// <copyright file="FeatureBehaviourModifier.cs" company="Mapbox">
//     Copyright (c) 2016 Mapbox. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Mapbox.Unity.MeshGeneration.Modifiers
{
    using Mapbox.Unity.MeshGeneration.Data;
    using Mapbox.Unity.MeshGeneration.Components;
    using UnityEngine;
    using System.Collections.Generic;
    /// <summary>
    /// A GameObjectModifier to extract feature data
    /// </summary>
    [CreateAssetMenu(menuName = "Mapbox/Modifiers/Add Show Feature Behaviour Modifier")]
    public class ShowFeatureBehaviourModifier : GameObjectModifier
    {
        #region Public Variables
        public string parameter;
        public string feature;
        #endregion

        #region Private Variables
        private Dictionary<GameObject, MyFeatureBehaviour> _features;
        private MyFeatureBehaviour _tempFeature;
        #endregion

        #region Public Methods
        /// <summary>
        /// Initialise data structure
        /// </summary>
        public override void Initialize()
        {
            if (_features == null)
            {
                _features = new Dictionary<GameObject, MyFeatureBehaviour>();
            }
        }
        /// <summary>
        /// Get all features and their properties
        /// </summary>
        /// <param name="ve"></param>
        /// <param name="tile"></param>
        public override void Run(VectorEntity ve, UnityTile tile)
        {
            if (_features.ContainsKey(ve.GameObject))
            {
                _features[ve.GameObject].Initialize(ve, parameter, feature);
            }
            else
            {
                _tempFeature = ve.GameObject.AddComponent<MyFeatureBehaviour>();
                _features.Add(ve.GameObject, _tempFeature);
                _tempFeature.Initialize(ve, parameter, feature);
            }
        }
        #endregion
    }
}
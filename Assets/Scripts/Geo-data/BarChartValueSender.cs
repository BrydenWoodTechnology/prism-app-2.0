﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mapbox.Unity.MeshGeneration.Data;
using Mapbox.Unity.MeshGeneration.Components;
using Mapbox.Unity.MeshGeneration.Modifiers;

/// <summary>
/// A GameObjectModifier component to fetch and send data for bar chart
/// </summary>
[CreateAssetMenu(menuName = "Mapbox/Modifiers/Chart Sender Feature Behaviour Modifier")]
public class BarChartValueSender : GameObjectModifier
{
    #region Public Variables
    public string datasetName;
    public string parameter1;
    public string parameter2;
    public float domainStart;
    public float domainEnd;
    #endregion

    #region Private Variables
    private Dictionary<GameObject, FeatureBehaviour> _features;
    private FeatureBehaviour _tempFeature;
    #endregion

    #region Public Methods
    /// <summary>
    /// Initialize data collection
    /// </summary>
    public override void Initialize()
    {
        if (_features == null)
        {
            _features = new Dictionary<GameObject, FeatureBehaviour>();
        }
    }
    /// <summary>
    /// Run modifier to get, remap and send values
    /// </summary>
    /// <param name="ve"></param>
    /// <param name="tile"></param>
    public override void Run(VectorEntity ve, UnityTile tile)
    {
        if (_features.ContainsKey(ve.GameObject))
        {
            _features[ve.GameObject].Initialize(ve);

            int par = int.Parse(_features[ve.GameObject].Data.Properties[parameter1].ToString());
            float value = float.Parse(_features[ve.GameObject].Data.Properties[parameter2].ToString());
            value = value.Remap(domainStart, domainEnd, 0f, 30f);
            if (value < 1)
                value = 1;
            FeaturesManager.Instance.barChart.CollectData(datasetName, par, value);
        }    
        else
        {
            _tempFeature = ve.GameObject.AddComponent<FeatureBehaviour>();
            _features.Add(ve.GameObject, _tempFeature);
            _tempFeature.Initialize(ve);

            int par = int.Parse(_tempFeature.Data.Properties[parameter1].ToString());
            float value = float.Parse(_tempFeature.Data.Properties[parameter2].ToString());
            value = value.Remap(domainStart, domainEnd, 0f, 30f);
            if (value < 1)
                value = 1;
            FeaturesManager.Instance.barChart.CollectData(datasetName, par, value);
        }
    }
    #endregion
}

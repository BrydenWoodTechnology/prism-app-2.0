﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using Newtonsoft.Json;
using TMPro;
using UnityEngine;


/// <summary>
/// Class to handle Weather Information from https://openweathermap.org/api . 
/// </summary>
public class WeatherAPI : MonoBehaviour
{
    #region Public Variables
    [HideInInspector]
    public GameObject parent;
    #endregion

    #region Private Variables
    string apiKey = "&appid = <your API key>"; //add your api key here
    //----------Live Weather--------------//
    private WeatherData mydata;  
    string jsonstring;  
    
    //----------Forecast--------------//
    private WeatherDataForecast mydataF;
    #endregion

    #region Delegates and Events
    /// <summary>
    /// Delegate for when weather data is loaded
    /// </summary>
    public delegate void OnWeatherLoaded();
    /// <summary>
    /// Event triggered when weather data is loaded
    /// </summary>
    public static event OnWeatherLoaded onWeatherLoaded;
    #endregion

    #region Public Methods
    /// <summary>
    /// Fetch weather data
    /// </summary>
    /// <param name="latlon"></param>
    /// <returns>(dictionary) with weather data response</returns>
    public Dictionary<string,string> LoadWeather(Mapbox.Utils.Vector2d latlon)
    {
        Dictionary<string, string> weather = new Dictionary<string, string>();
        string lat = latlon.x.ToString();
        string lon = latlon.y.ToString();      
        
        string myURL = "https://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lon + "&appid=7512559067c03fd425cb8c5fb2ca2894" + "&units=metric";
        StartCoroutine(GetCall(myURL, weather));

        return weather;
    }

    /// <summary>
    /// Spawn prefabs to simulate weather conditions
    /// </summary>
    /// <param name="prefab"></param>
    /// <param name="emission"></param>
    /// <param name="dx"></param>
    /// <param name="dy"></param>
    public void InstantiatePrefabs(ParticleSystem prefab, float emission, float dx, float dy)
    {
        ParticleSystem ps = Instantiate(prefab);
        ps.transform.position = new Vector3(ps.transform.position.x - dx, ps.transform.position.y + dy, transform.position.z);
        ps.transform.SetParent(transform, true);
        ps.transform.localPosition += new Vector3(20, 0, 0);
        var snowMapEmission = ps.emission;
        snowMapEmission.rateOverTime = 10f;
        ps.Play();
    }

    #endregion

    #region Monobehaviour Methods
    public void Start()
    {

    }
    #endregion

    #region Private Methods
    /// <summary>
    /// Makes a call to the API and extracts relevant information for the given location
    /// </summary>
    /// <param name="myURL"></param>
    /// <param name="weather"></param>
    /// <returns></returns>
    IEnumerator GetCall(string myURL, Dictionary<string, string> weather)
    {
        WWW www = new WWW(myURL);
        yield return www;

        string dataAsJSON = www.text;

        if (www.error == null || www.error == "")
        {
            //Debug.Log(dataAsJSON);
        }
        else
        {
            Debug.Log("Error: " + www.error);
        }

        mydata = new WeatherData();
        mydata = JsonUtility.FromJson<WeatherData>(dataAsJSON);       
     
        string info = "";
        info += string.Format("{0}\r\n", mydata.weather[0].description.ToUpper());
        info += string.Format("{0}\r\n", "Temperature [°C] : " + (mydata.main.temp));
        info += string.Format("{0}\r\n", "Wind speed [m/s] : " + mydata.wind.speed);
        info += string.Format("{0}\r\n", "Visibility [m] : " + mydata.visibility);
        info += string.Format("{0}\r\n", "Humidity [%] : " + mydata.main.humidity);
        

        weather.Add("type" , mydata.weather[0].description);
        weather.Add("info" , info);

        onWeatherLoaded();
    }   
    /// <summary>
    /// Fetch forecast data for the given location
    /// </summary>
    /// <param name="lat"></param>
    /// <param name="lon"></param>
    /// <param name="transform"></param>
    void Forecast(string lat, string lon, Transform transform)
    {
        Mesh cloudMesh = new Mesh();
        string myURL = "http://api.openweathermap.org/data/2.5/forecast?lat=" + lat + "&lon=" + lon + "&appid=7512559067c03fd425cb8c5fb2ca2894";
        StartCoroutine(ReadJsonForecast(myURL, transform));
    }
    /// <summary>
    /// Serialise forecast response in json format
    /// </summary>
    /// <param name="url"></param>
    /// <param name="transform"></param>
    /// <returns></returns>
    IEnumerator ReadJsonForecast(string url, Transform transform)
    {
        WWW www = new WWW(url);
        yield return www;

        string dataAsJSON = www.text;

        if (www.error == null || www.error == "")
        {
            Debug.Log(dataAsJSON);
        }
        else
        {
            Debug.Log("Error: " + www.error);
        }


        mydataF = new WeatherDataForecast();
        mydataF = JsonUtility.FromJson<WeatherDataForecast>(dataAsJSON);

        Debug.Log(mydataF.list[0].clouds.all);        
    }
    #endregion

}

#region  WEATHER DATA SERIALISATION CLASSES 
// CURRENT WEATHER
[Serializable]
public class Coord
{
    public float lon;
    public float lat;
}

[Serializable]
public class Weather
{
    public int id;
    public string main;
    public string description;
    public string icon;
}

[Serializable]
public class Main
{
    public float temp;
    public float pressure;
    public int humidity;
    public double temp_min;
    public double temp_max;
    public double sea_level;
    public double grnd_level;
    public double temp_kf;

}

[Serializable]
public class Wind
{
    public double speed;
    public float deg;
    public float gust;
}

[Serializable]
public class Clouds
{
    public int all;
}

[Serializable]
public class Rain
{
    public int __invalid_name__3h;
}

[Serializable]
public class Snow
{
    public int __invalid_name__3h;
}

[Serializable]
public class Sys
{
    public int type;
    public int id;
    public double message;
    public string country;
    public int sunrise;
    public int sunset;
    public string pod;
}

[Serializable]
public class City
{
    public int id;
    public string name;
    public Coord coord;
    public string country;
}

[Serializable]
public class ListData
{
    public int dt;
    public Main main;
    public List<Weather> weather;
    public Clouds clouds;
    public Wind wind;
    public Snow snow;
    public Rain rain;
    public Sys sys;
    public string dt_txt;
}

[Serializable]
public class ListCities
{
    public int id;
    public string name;
    public Coord coord;
    public Main main;
    public int dt;
    public Wind wind;
    public Sys sys;
    public object rain;
    public object snow;
    public Clouds clouds;
    public List<Weather> weather;
}

[Serializable]
public class WeatherData
{
    public Coord coord;
    public List<Weather> weather;
    public string @base;
    public Main main;
    public int visibility;
    public Wind wind;
    public Clouds clouds;
    public Rain rain;
    public Snow snow;
    public int dt;
    public Sys sys;
    public int id;
    public string name;
    public int cod;

}

// 5 DAY FORECAST
[Serializable]
public class WeatherDataForecast
{
    public string cod;
    public double message;
    public int cnt;
    public List<ListData> list;
    public City city;
}

// Several Cities
[Serializable]
public class RootObjectSeveralCities
{
    public string message;
    public string cod;
    public int count;
    public List<ListCities> list;
}



//Air Pollution
[Serializable]
public class Location
{
    public int latitude;
    public double longitude;
}

[Serializable]
public class Datum
{
    public float precision;
    public float pressure;
    public float value;
}

[Serializable]
public class CarbonMonoxide
{
    public DateTime time;
    public Location location;
    public List<Datum> data;
}

#endregion




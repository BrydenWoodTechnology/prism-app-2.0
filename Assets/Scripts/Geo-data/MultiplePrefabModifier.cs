﻿//-----------------------------------------------------------------------
// <copyright file="PrefabModifier.cs" company="Mapbox">
//     Copyright (c) 2016 Mapbox. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Mapbox.Unity.MeshGeneration.Modifiers
{
    using UnityEngine;
    using Mapbox.Unity.MeshGeneration.Data;
    using Mapbox.Unity.MeshGeneration.Components;
    using Mapbox.Unity.MeshGeneration.Interfaces;
    using System.Collections.Generic;
    using Mapbox.Unity.Map;
    using System;

    /// <summary>
    /// Class based on mapbox gameObject modifier class to spawn multiple prefabs according to data driven property
    /// </summary>
    [CreateAssetMenu(menuName = "Mapbox/Modifiers/Multiple Prefab Modifier")]
    public class MultiplePrefabModifier : GameObjectModifier
    {
        #region Private Variables
        private Dictionary<GameObject, GameObject> _objects;
        [SerializeField]
        private SpawnMultiplePrefabOptions _options;
        private List<GameObject> _prefabList = new List<GameObject>();
        #endregion

        #region Public Methods
        /// <summary>
        /// Initialise data structure
        /// </summary>
        public override void Initialize()
        {
            if (_objects == null)
            {
                _objects = new Dictionary<GameObject, GameObject>();
            }
        }
        /// <summary>
        /// Set modifier's properties
        /// </summary>
        /// <param name="properties"></param>
        public override void SetProperties(ModifierProperties properties)
        {
            _options = (SpawnMultiplePrefabOptions)properties;
            _options.PropertyHasChanged += UpdateModifier;
        }
        /// <summary>
        /// Spawn prefab based on given parameter's criteria
        /// </summary>
        /// <param name="ve"></param>
        /// <param name="tile"></param>
        public override void Run(VectorEntity ve, UnityTile tile)
        {
           
            if (_options.prefabs == null)
            {
                return;
            }

            GameObject go = null;

            if (_objects.ContainsKey(ve.GameObject))
            {
                go = _objects[ve.GameObject];
            }
            else
            {
                //Modification to randomly pick one prefab and jitter height
                //int index = getRandomIndex(_options.prefabs.Count);
                //go = Instantiate(_options.prefabs[index]);

                //Modification to load the correct type of tree from library
                string id = ve.Feature.Properties["display_name"].ToString();
                id = id.Replace(" ", "_");           
               // GameObject prefab = TreeManager.Instance.tree[id].spring; //for all seasons
                GameObject prefab = TreeManager.Instance.treeGos[id]; //for single season
                go = Instantiate(prefab);
               

                _prefabList.Add(go);
                _objects.Add(ve.GameObject, go);
                go.transform.SetParent(ve.GameObject.transform, false);
            }

            PositionScaleRectTransform(ve, tile, go);

            if (_options.AllPrefabsInstatiated != null)
            {
                _options.AllPrefabsInstatiated(_prefabList);
            }
            
            
        }
       

     
        /// <summary>
        ///  Translate and transform prefab 
        /// </summary>
        /// <param name="ve"></param>
        /// <param name="tile"></param>
        /// <param name="go"></param>
        public void PositionScaleRectTransform(VectorEntity ve, UnityTile tile, GameObject go)
        {
            RectTransform goRectTransform;
            IFeaturePropertySettable settable = null;
            var centroidVector = new Vector3();
            foreach (var point in ve.Feature.Points[0])
            {
                centroidVector += point;
            }
            centroidVector = centroidVector / ve.Feature.Points[0].Count;

            go.name = ve.Feature.Data.Id.ToString();

            goRectTransform = go.GetComponent<RectTransform>();
            if (goRectTransform == null)
            {
                go.transform.localPosition = centroidVector;
                if (_options.scaleDownWithWorld)
                {
                    //go.transform.localScale = _options.prefab.transform.localScale * (tile.TileScale);
                    go.transform.localScale = go.transform.localScale * (tile.TileScale);
                }
            }
            else
            {
                goRectTransform.anchoredPosition3D = centroidVector;
                if (_options.scaleDownWithWorld)
                {
                    //goRectTransform.localScale = _options.prefabs[0].transform.localScale * (tile.TileScale);
                    goRectTransform.localScale = goRectTransform.localScale * (tile.TileScale);
                }
            }

            //go.transform.localScale = Constants.Math.Vector3One;

            settable = go.GetComponent<IFeaturePropertySettable>();
            if (settable != null)
            {
                settable.Set(ve.Feature.Properties);
            }
        }
        /// <summary>
        /// Clear memory cache
        /// </summary>
        public override void ClearCaches()
        {
            base.ClearCaches();
            foreach (var gameObject in _objects.Values)
            {
                Destroy(gameObject);
            }

            foreach (var gameObject in _prefabList)
            {
                Destroy(gameObject);
            }
        }
#endregion

        #region Private Methods
        /// <summary>
        /// Get a  random index of the existing data
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        int getRandomIndex(int count)
        {
            var random = new System.Random();
            List<int> values = new List<int>();
            for (int i = 0; i < count; i++)
            {
                values.Add(i);
            }
            int index = random.Next(values.Count);
            return index;
        }
        /// <summary>
        /// Jitter height to create a more natural effect
        /// </summary>
        /// <param name="initialHeight"></param>
        /// <returns></returns>
        double jitterHeight(float initialHeight)
        {
            var random = new System.Random();
            return random.NextDouble();
        }
        #endregion
    }
}

﻿//-----------------------------------------------------------------------
// <copyright file="SpawnPrefabOptions.cs" company="Mapbox">
//     Copyright (c) 2016 Mapbox. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Mapbox.Unity.Map
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using Mapbox.Unity.MeshGeneration.Modifiers;
    using System;
    using Mapbox.Unity.Map;
    /// <summary>
    /// A modifier properties component to set the properties of the SpawnMultiplePrefabModifier
    /// </summary>
    [Serializable]
    public class SpawnMultiplePrefabOptions : ModifierProperties
    {
        #region Public Variables
        public override Type ModifierType
        {
            get
            {
                return typeof(PrefabModifier);
            }
        }

        public List<GameObject> prefabs=new List<GameObject>();
        public bool scaleDownWithWorld = true;
        [NonSerialized]
        public Action<List<GameObject>> AllPrefabsInstatiated = delegate { };
        #endregion
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// A component to control interaction of a UI Toggle
/// </summary>
public class SeasonToggleExtension : MonoBehaviour
{
    #region Public Variables
    public string season;
    #endregion

    #region Monobehaviour Methods
    // Start is called before the first frame update
    void Start()
    {
        TreeManager.onTreesLoaded += ActivateToggles;
        AddListener();
       
    }
    #endregion

    #region Public Methods
    /// <summary>
    /// Activate toggle
    /// </summary>
    public void ActivateToggles()
    {
        GetComponent<Toggle>().interactable = true;        
    }
    /// <summary>
    /// Deactivate tgoggle
    /// </summary>
    public void DectivateToggles()
    {
        GetComponent<Toggle>().interactable = false;
    }
    /// <summary>
    /// Add listener to the toggle to populate assets
    /// </summary>
    public void AddListener()
    {
        GetComponent<Toggle>().onValueChanged.AddListener(delegate
        {
            GetComponent<Toggle>().group.BroadcastMessage("DectivateToggles");
            if (GetComponent<Toggle>().isOn)
            {
                TreeManager.Instance.PopulateTreesFromAssetBundle(season);
            }


        });
    }
    #endregion
}

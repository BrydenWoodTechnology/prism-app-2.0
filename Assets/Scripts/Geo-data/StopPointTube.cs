﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine.Networking;
using TFLData;
/// <summary>
/// A component to spawn stops based on TFL data
/// </summary>
public class StopPointTube : MonoBehaviour
{
    #region Public Variables
    public TextMeshPro nameText;
    public TextMeshPro modeText;
    public GameObject modeObject;
    public GameObject cylinder;

    public GameObject crowdingCube;
    public TextMeshPro crowdingText;

    public GameObject tube;
    public GameObject crowd;
    #endregion

    #region Private Variables
    string apiKey = ""; //add your api key here
    string stopName;
    string id;
    string lineName;
    string mode;
    #endregion

    #region Monobehaviour Methods
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    #endregion

    #region Private Methods
    /// <summary>
    /// Get Passenger Count for a specific location
    /// </summary>
    /// <param name="url"></param>
    /// <returns></returns>
    private IEnumerator PassengerCountCall(string url)
    {      
        string result = "";
        yield return StartCoroutine(QueryTFL(url, value => result = value));

        CrowdData entities = JsonConvert.DeserializeObject<CrowdData>(result);

        int passengerCount = 0;
        for(int i =0; i< entities.lines.Count; i++)
        {
            if (entities.lines[i].id == lineName)
            {
                for(int j=0; j< entities.lines[i].crowding.passengerFlows.Count; j++)
                {
                    //string time = entities.lines[i].crowding.passengerFlows[j].timeSlice;
                    passengerCount += entities.lines[i].crowding.passengerFlows[j].value;  
                }
            }
        }
        crowdingText.text = passengerCount.ToString();
        crowdingText.transform.position += new Vector3(0f, passengerCount / 100f, 0f);
        crowdingCube.transform.localScale = new Vector3(10f, passengerCount/100f, 10f);
        //crowdingCube.SetActive(true);

        entities = new CrowdData();
    }
    /// <summary>
    /// Fetch data from TFL API
    /// </summary>
    /// <param name="dataUrl"></param>
    /// <param name="result"></param>
    /// <returns></returns>
    IEnumerator QueryTFL(string dataUrl, System.Action<string> result)
    {
        using (UnityWebRequest www = UnityWebRequest.Get(dataUrl))
        {
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.isDone)
                {
                    string jsonResult =
                        System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
                    //Debug.Log(jsonResult);
                    result(jsonResult);
                }

            }
        }
    }
    #endregion

    #region Public Methods
    /// <summary>
    /// Spawn stop
    /// </summary>
    /// <param name="_id"></param>
    /// <param name="_name"></param>
    /// <param name="_line"></param>
    /// <param name="_mode"></param>
    /// <param name="crowding"></param>
    public void Initialise(string _id, string _name, string _line, string _mode, bool crowding)
    {
        id = _id;
        mode = _mode;
        stopName = _name;
        nameText.text = stopName;
        lineName = _line;

        if (crowding)
        {
            tube.SetActive(false);
            crowd.SetActive(true);
            GetCrowdingData();
        }
        else
        {
            tube.SetActive(true);
            crowd.SetActive(false);
            if (mode == "overground")
            {
                modeText.text = "OVERGROUND";
                modeObject.GetComponent<MeshRenderer>().material = Resources.Load<Material>("GeodataPrefabs/overgroundMaterial");
                cylinder.GetComponent<MeshRenderer>().material = Resources.Load<Material>("GeodataPrefabs/overgroundMaterial");
            }
            else if (mode == "dlr")
            {
                modeText.text = "DLR";
                modeObject.GetComponent<MeshRenderer>().material = Resources.Load<Material>("GeodataPrefabs/dlrMaterial");
                cylinder.GetComponent<MeshRenderer>().material = Resources.Load<Material>("GeodataPrefabs/dlrMaterial");
            }
        }

    }
    /// <summary>
    /// Get crowding data for specific stop
    /// </summary>
    public void GetCrowdingData()
    {
        string url = "https://api.tfl.gov.uk/StopPoint/" + id + "/Crowding/" + lineName + "?direction=inbound&app_id=4a516f5a&app_key=" + apiKey;
        //make call for passenger data
        StartCoroutine(PassengerCountCall(url));
    }
    #endregion
}

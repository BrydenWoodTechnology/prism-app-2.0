﻿//-----------------------------------------------------------------------
// <copyright file="LineMeshModifierOptions.cs" company="Mapbox">
//     Copyright (c) 2016 Mapbox. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using Mapbox.Unity.SourceLayers;
using UnityEngine;

namespace Mapbox.Unity.Map
{
    using Mapbox.Unity.MeshGeneration.Modifiers;
    using System;
    /// <summary>
    /// A ModifierProperties class for the AttributeBasedLineModifier
    /// </summary>
    [Serializable]
    public class AttributeBasedLineOptions : ModifierProperties, ISubLayerLineGeometryOptions
    {
        #region Public Variables
        /// <summary>
        /// Modifier type
        /// </summary>
        public override Type ModifierType
        {
            get
            {
                return typeof(LineMeshModifier);
            }
        }

        [Tooltip("Width of the line feature.")]
        public float Width = 1.0f;

        [Tooltip("Parameter of the line feature.")]
        public string Parameter = "roadWidthA";
        #endregion

        #region Public Methods
        /// <summary>
        /// Sets the width of the mesh generated for line features.
        /// </summary>
        /// <param name="width">Width of the mesh generated for line features.</param>
        public void SetLineWidth(float width)
        {
            if (Width != width)
            {
                Width = width;
                HasChanged = true;
            }
        }

        /// <summary>
        /// Sets the parameter based on wich the width of the mesh for line features is modified.
        /// </summary>
        /// <param name="parameter">Parameter to modify the width of the mesh generated for line features.</param>
        public void SetLineParameter(string parameter)
        {
            if (Parameter != parameter)
            {
                Parameter = parameter;
                HasChanged = true;
            }
        }
        #endregion
    }
}


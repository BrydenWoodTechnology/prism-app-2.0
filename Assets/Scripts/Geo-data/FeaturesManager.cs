using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mapbox.Utils;
using Mapbox.Unity.Map;
using Mapbox.Unity.MeshGeneration;
using KDTree;
using Mapbox.Unity.MeshGeneration.Data;
using Mapbox.Unity.MeshGeneration.Modifiers;
using Mapbox.Unity.MeshGeneration.Components;
using Mapbox.Examples;
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine.Networking;
using TFLData;

/// <summary>
/// Monobehaviour component to load and handle data manipulations on 3D mapbox map
/// </summary>
public class FeaturesManager : MonoBehaviour
{
    #region Public Variables
    public static FeaturesManager Instance;

    [Header("Data:")]
    public List<AbstractMap> maps = new List<AbstractMap>();
    public Dictionary<string, Geodata> mapData = new Dictionary<string, Geodata>();
    public int bufferRadius = 500;
    public GameObject pin;

    [Header("Prefabs:")]
    public Material clipMaterial;
    public GameObject togglePrefab;

    [Header("Parents:")]
    public Transform geodataParent;
    public GameObject contextParent;
    public GameObject goodGrowthParent;
    public GameObject ProtectionParent;
    public BarChartManager barChart;

    public static GameObject fullMap;

    public string planningServer = "https://maps.london.gov.uk/gla/rest/services/apps/planning_data_map_03/MapServer/";
    public string airQualityServer = "https://maps.london.gov.uk/gla/rest/services/apps/Air_Quality_map_service_01/MapServer/";
    #endregion

    #region Private Variables         
    Dictionary<string, UIMapToggle> toggles = new Dictionary<string, UIMapToggle>();
    Dictionary<string, GameObject> arcgisLoadedLayers = new Dictionary<string, GameObject>();
    Dictionary<string, GameObject> tflLoadedLayers = new Dictionary<string, GameObject>();
   
    UIMapToggle lastSelectedToggle;

    bool mapHasLoaded = false;
    int firsttimeloadingmap = 0;
    #endregion

    #region Monobehaviour Methods
    private void Awake()
    {
        Instance = this;
        fullMap = gameObject;
    }

    /// <summary>
    /// Start is called before the first frame update
    /// </summary>
    void Start()
    {
        MapHandler.locationChanged += PinOnMap;

        TreeManager.onTreesLoaded += UpdateMap;

        HtmlUIManager.OnLatLonReceived += OnMapLatLonReceived;

        maps[3].OnInitialized += MapLoaded;



        //map = GetComponent<AbstractMap>();
        StartCoroutine(LoadGeodataIndex("GeoData.json"));

        //LoadGlaMapData("Conservation_Areas", 107);

        maps[0].OnUpdated += ReloadARCGISlayers;
    }
    #endregion

    #region Private Methods

    #region Load json map data
    //______________________________________________________________________________//
    //----------------------------- GEODATA JSON LOADING ---------------------------//
    /// <summary>
    /// Load json from streaming assets folder
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    IEnumerator LoadGeodataIndex(string fileName)
    {
        string filePath = System.IO.Path.Combine(Application.streamingAssetsPath, fileName);
        string result;

        if (filePath.Contains("://") || filePath.Contains(":///"))
        {
            WWW www = new WWW(filePath);
            yield return www;
            result = www.text;

        }
        else
            result = System.IO.File.ReadAllText(filePath);

        mapData = ReadDataIndex(result);
    }

    /// <summary>
    /// Read json with data layers information
    /// </summary>
    /// <param name="file"></param>
    /// <returns>data layers as dictionary</returns>
    Dictionary<string, Geodata> ReadDataIndex(string file)
    {
        Dictionary<string, Geodata> dataTable = new Dictionary<string, Geodata>();
        JArray jarray = new JArray();
        try
        {
            jarray = JArray.Parse(file);
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
        for (int i = 0; i < jarray.Count; i++)
        {
            try
            {
                string data = jarray[i].ToString();
                Geodata geodata = JsonConvert.DeserializeObject<Geodata>(data);
                dataTable.Add(geodata.Name, geodata);

                //setup UI
                GameObject toggle = Instantiate(togglePrefab);
                if (geodata.Category == "Context")
                {
                    toggle.transform.parent = contextParent.transform;
                }
                else if (geodata.Category == "Protection")
                {
                    toggle.transform.parent = ProtectionParent.transform;
                }
                else
                {
                    toggle.transform.parent = goodGrowthParent.transform;
                }
                toggle.transform.localScale = new Vector3(1, 1, 1);
                toggle.GetComponent<UIMapToggle>().Initialise(geodata.Name, geodata.Description, geodata.Colours[0], geodata.URL, geodata);
                toggle.name = geodata.Name;

                //add listener
                toggle.GetComponent<UIMapToggle>().toggle.GetComponent<Toggle>().onValueChanged.AddListener(delegate
                {
                    bool active = toggle.GetComponent<UIMapToggle>().info.activeSelf;
                    toggle.GetComponent<UIMapToggle>().info.SetActive(!active);
                    lastSelectedToggle = toggle.GetComponent<UIMapToggle>();
                    //force update layout  
                    LayoutRebuilder.ForceRebuildLayoutImmediate(toggle.GetComponentInParent<RectTransform>());
                    LayoutRebuilder.ForceRebuildLayoutImmediate(toggle.GetComponentInParent<RectTransform>());
                    ActivateFeature(geodata.Name, toggle.GetComponent<UIMapToggle>().toggle.isOn);

                });

                //have layer open when loading map
                if (geodata.IsOn)
                {
                    toggle.GetComponent<UIMapToggle>().toggle.isOn = true;
                    toggle.GetComponent<UIMapToggle>().toggle.onValueChanged.Invoke(true);
                }

                toggles.Add(geodata.Name, toggle.GetComponent<UIMapToggle>());
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }
        }
        // rebuild layout of ui parent objects to prevent overlay
        LayoutRebuilder.ForceRebuildLayoutImmediate(contextParent.GetComponent<RectTransform>());
        Canvas.ForceUpdateCanvases();


        return dataTable;
    }

    #endregion

    #region ARCGIS query
    //______________________________________________________________________________//
    //-------------------------------- ARCGIS GLA QUERY ----------------------------//
    /// <summary>
    /// Form the query for ARCGIS API to get data as json
    /// </summary>
    /// <param name="feature"></param>
    /// <param name="code"></param>
    void LoadGlaMapData(string feature, int code)
    {
        Vector2d latlon = maps[0].CenterLatitudeLongitude;

        string dataUrl = planningServer + code + "/query?geometry=" + latlon.y + "%2C" + latlon.x + "&geometryType=esriGeometryPoint&inSR=%7B%22wkt%22+%3A+%22GEOGCS%5B%5C%22GCS_WGS_1984%5C%22%2CDATUM%5B%5C%22D_WGS_1984%5C%22%2C+SPHEROID%5B%5C%22WGS_1984%5C%22%2C6378137%2C298.257223563%5D%5D%2CPRIMEM%5B%5C%22Greenwich%5C%22%2C0%5D%2C+UNIT%5B%5C%22Degree%5C%22%2C0.017453292519943295%5D%5D%22%7D&returnGeometry=true&outSR=%7B%22wkt%22+%3A+%22GEOGCS%5B%5C%22GCS_WGS_1984%5C%22%2CDATUM%5B%5C%22D_WGS_1984%5C%22%2C+SPHEROID%5B%5C%22WGS_1984%5C%22%2C6378137%2C298.257223563%5D%5D%2CPRIMEM%5B%5C%22Greenwich%5C%22%2C0%5D%2C+UNIT%5B%5C%22Degree%5C%22%2C0.017453292519943295%5D%5D%22%7D&returnCountOnly=false&queryByDistance=" + bufferRadius + "&distance=" + bufferRadius + "&units=esriSRUnit_Meter&datumTransformation=1984&f=json";
        if (feature.Contains("Emission"))
            dataUrl = airQualityServer + code + "/query?geometry=" + latlon.y + "%2C" + latlon.x + "&geometryType=esriGeometryPoint&inSR=%7B%22wkt%22+%3A+%22GEOGCS%5B%5C%22GCS_WGS_1984%5C%22%2CDATUM%5B%5C%22D_WGS_1984%5C%22%2C+SPHEROID%5B%5C%22WGS_1984%5C%22%2C6378137%2C298.257223563%5D%5D%2CPRIMEM%5B%5C%22Greenwich%5C%22%2C0%5D%2C+UNIT%5B%5C%22Degree%5C%22%2C0.017453292519943295%5D%5D%22%7D&returnGeometry=true&outSR=%7B%22wkt%22+%3A+%22GEOGCS%5B%5C%22GCS_WGS_1984%5C%22%2CDATUM%5B%5C%22D_WGS_1984%5C%22%2C+SPHEROID%5B%5C%22WGS_1984%5C%22%2C6378137%2C298.257223563%5D%5D%2CPRIMEM%5B%5C%22Greenwich%5C%22%2C0%5D%2C+UNIT%5B%5C%22Degree%5C%22%2C0.017453292519943295%5D%5D%22%7D&returnCountOnly=false&queryByDistance=" + bufferRadius + "&distance=" + bufferRadius + "&units=esriSRUnit_Meter&datumTransformation=1984&f=json";

        StartCoroutine(GetData(dataUrl, feature));
    }
    /// <summary>
    /// Nested coroutine to wait until query is done and then generate geometry
    /// </summary>
    /// <param name="dataUrl"></param>
    /// <param name="feature"></param>
    /// <returns></returns>
    IEnumerator GetData(string dataUrl, string feature)
    {
        string result = "";
        yield return StartCoroutine(QueryArcGIS(dataUrl, value => result = value));

        RootObject entities = JsonConvert.DeserializeObject<RootObject>(result);
        Debug.Log(feature + "  :  " + entities.features.Count);
        if (entities.features != null && entities.features.Count > 0)
        {
            GenerateGeometry(entities, feature);
            toggles[feature].error.SetActive(false);
        }
        else
        {
            toggles[feature].error.SetActive(true);
            LayoutRebuilder.ForceRebuildLayoutImmediate(lastSelectedToggle.GetComponent<RectTransform>());
            LayoutRebuilder.ForceRebuildLayoutImmediate(lastSelectedToggle.transform.GetChild(1).GetComponent<RectTransform>());
            LayoutRebuilder.ForceRebuildLayoutImmediate(lastSelectedToggle.GetComponentInParent<RectTransform>());

            arcgisLoadedLayers.Add(feature, null);
        }
        entities = new RootObject();
    }

    /// <summary>
    /// Query ARCGIS API
    /// </summary>
    /// <param name="dataUrl"></param>
    /// <param name="result"></param>
    /// <returns>request response</returns>
    IEnumerator QueryArcGIS(string dataUrl, System.Action<string> result)
    {
        using (UnityWebRequest www = UnityWebRequest.Get(dataUrl))
        {
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.isDone)
                {
                    string jsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
                    //Debug.Log(jsonResult);
                    result(jsonResult);
                }

            }
        }
    }

    /// <summary>
    /// Generate geometry from the deserialised received data
    /// </summary>
    /// <param name="items"></param>
    /// <param name="feature"></param>
    void GenerateGeometry(RootObject items, string feature)
    {
        GameObject parent = new GameObject(feature);
        parent.transform.parent = geodataParent;

        for (int i = 0; i < items.features.Count; i++)
        {
            List<Vector3> vertices = new List<Vector3>();
            List<Vector2> vertices2d = new List<Vector2>();
            for (int j = 0; j < items.features[i].geometry.rings[0].Count; j++)
            {
                Vector2d latlon = new Vector2d(items.features[i].geometry.rings[0][j][1], items.features[i].geometry.rings[0][j][0]);
                Vector3 point = maps[0].GeoToWorldPosition(latlon, true);
                vertices.Add(new Vector3(point.x, -point.z, 0));
                vertices2d.Add(new Vector2(point.x, -point.z));
            }
            TriangulatorSimple triangulator = new TriangulatorSimple(vertices2d.ToArray());
            int[] indices = triangulator.Triangulate();

            Mesh mesh = new Mesh();
            mesh.vertices = vertices.ToArray();
            mesh.triangles = indices;
            mesh.RecalculateNormals();
            mesh.RecalculateBounds();

            GameObject obj = new GameObject();
            obj.AddComponent<MeshRenderer>();
            obj.GetComponent<MeshRenderer>().material = clipMaterial;
            obj.AddComponent<MeshFilter>();
            obj.GetComponent<MeshFilter>().mesh = mesh;
            obj.transform.localEulerAngles += new Vector3(-90, 0, 0);
            obj.transform.parent = parent.transform;

            Color color = new Color();
            ColorUtility.TryParseHtmlString(mapData[feature].Colours[0], out color);
            obj.GetComponent<MeshRenderer>().material.color = color;

            obj.AddComponent<DataAttributes>();
            obj.GetComponent<DataAttributes>().attributes.Add("sitename", items.features[i].attributes.sitename);


        }

        parent.transform.position += new Vector3(0, 0.1f, 0);
        arcgisLoadedLayers.Add(feature, parent);
    }

    #endregion

    #region TFL API
    /// <summary>
    /// Handles the call from tfl website 
    /// </summary>
    /// <param name="name"></param>
    public void FetchTFLData(string feature)
    {
        StartCoroutine(DoTheCall(feature));
    }
    /// <summary>
    /// Makes a call to TFL API
    /// </summary>
    /// <param name="feature"></param>
    /// <returns></returns>
    private IEnumerator DoTheCall(string feature)
    {
        double lat = maps[0].CenterLatitudeLongitude.x;
        double lon = maps[0].CenterLatitudeLongitude.y;
        string dataUrl = "https://api.tfl.gov.uk/StopPoint?stopTypes=NaptanMetroStation,NaptanRailStation&radius=500&modes=tube,dlr,overground%2Cdlr&returnLines=true&categories=none&lat=" + lat + "&lon=" + lon + "&app_key=08cc7d4f0159ce2a91386325ddc4f9ec&app_id=4a516f5a";
        string result = "";
        yield return StartCoroutine(QueryArcGIS(dataUrl, value => result = value));

        TFL entities = JsonConvert.DeserializeObject<TFL>(result);
        try
        {
            if (entities.stopPoints != null && entities.stopPoints.Count > 0)
            {
                SpawnStops(entities, feature);
                toggles[feature].error.SetActive(false);
            }
            else
            {
                toggles[feature].error.SetActive(true);
                LayoutRebuilder.ForceRebuildLayoutImmediate(lastSelectedToggle.GetComponent<RectTransform>());
                LayoutRebuilder.ForceRebuildLayoutImmediate(lastSelectedToggle.transform.GetChild(1).GetComponent<RectTransform>());
                LayoutRebuilder.ForceRebuildLayoutImmediate(lastSelectedToggle.GetComponentInParent<RectTransform>());

                tflLoadedLayers.Add(feature, null);
            }
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }

        entities = new TFL();

    }

    /// <summary>
    /// Spawn tube stops based on the type of service (underground, overground, dlr)
    /// </summary>
    /// <param name="data"></param>
    /// <param name="feature"></param>
    void SpawnStops(TFL data, string feature)
    {

        GameObject prefab = Resources.Load<GameObject>("GeodataPrefabs/TubeStopPrefab");

        GameObject parent = new GameObject();
        parent.transform.parent = geodataParent;
        parent.name = feature;

        for (int i = 0; i < data.stopPoints.Count; i++)
        {
            GameObject stop = Instantiate(prefab, parent.transform);

            string tubeLine = "";
            string mode = "";
            for (int j = 0; j < data.stopPoints[i].lineModeGroups.Count; j++)
            {
                if (data.stopPoints[i].lineModeGroups[j].modeName == "tube" || data.stopPoints[i].lineModeGroups[j].modeName == "dlr" || data.stopPoints[i].lineModeGroups[j].modeName == "overground")
                {
                    tubeLine = data.stopPoints[i].lineModeGroups[j].lineIdentifier[0];
                    mode = data.stopPoints[i].lineModeGroups[j].modeName;
                }
            }

            Vector2d latlon = new Vector2d(data.stopPoints[i].lat, data.stopPoints[i].lon);
            Vector3 position = maps[0].GeoToWorldPosition(latlon, true);
            stop.transform.position = position;

            if (feature == "Tube_Stops")
            {
                stop.GetComponent<StopPointTube>().Initialise(data.stopPoints[i].id, data.stopPoints[i].commonName, tubeLine, mode, false);
            }
            else if (feature == "Passenger_Counts")
            {
                stop.GetComponent<StopPointTube>().Initialise(data.stopPoints[i].id, data.stopPoints[i].commonName, tubeLine, mode, true);
            }


        }

        tflLoadedLayers.Add(feature, parent);
    }

    #endregion

    #endregion

    #region Public Methods

    #region UI methods
    //______________________________________________________________________________//
    //----------------------------- UI INVOKED FUNCTIONS ---------------------------//
    /// <summary>
    /// Open or close dataset from UI toggle
    /// </summary>
    /// <param name="feature"></param>
    public void ActivateFeature(string feature, bool isOn)
    {

        if (mapData[feature].Source == "Mapbox")
        {
            
            int index = mapData[feature].MapIndex;
            for (int j = 0; j < mapData[feature].UnityMapFeatures.Count; j++)
            {
                VectorSubLayerProperties vectorSubLayerProperties = maps[index].VectorData.FindFeatureSubLayerWithName(mapData[feature].UnityMapFeatures[j]);
                //isactive = vectorSubLayerProperties.coreOptions.isActive;
                vectorSubLayerProperties.SetActive(isOn);
                if (mapData[feature].Chart)
                {
                    barChart.transform.GetChild(0).gameObject.SetActive(isOn);
                }
            }
            if (mapData[feature].CloseMapFeatures != null)
            {
                for (int j = 0; j < mapData[feature].CloseMapFeatures.Count; j++)
                {
                    VectorSubLayerProperties vectorSubLayerProperties = maps[index].VectorData.FindFeatureSubLayerWithName(mapData[feature].CloseMapFeatures[j]);
                    //isactive = vectorSubLayerProperties.coreOptions.isActive;
                    vectorSubLayerProperties.SetActive(!isOn);

                }
            }
        }
        else if (mapData[feature].Source == "ARCGIS")
        {
            if (!arcgisLoadedLayers.ContainsKey(feature))
            {
                LoadGlaMapData(mapData[feature].Name, mapData[feature].Code);
            }
            else
            {
                if (arcgisLoadedLayers[feature] != null)
                {
                    //bool isactive = arcgisLoadedLayers[feature].activeSelf;
                    arcgisLoadedLayers[feature].SetActive(isOn);
                }

            }
        }
        else if (mapData[feature].Source == "TflApi")
        {
            if (!tflLoadedLayers.ContainsKey(feature))
            {
                FetchTFLData(mapData[feature].Name);
            }
            else
            {
                if (tflLoadedLayers[feature] != null)
                {

                    tflLoadedLayers[feature].SetActive(isOn);
                }

            }
        }

    }

    /// <summary>
    /// Make map toggles interactable
    /// </summary>
    public void ActivateMapToggles()
    {
        activateButtonInsideParent(contextParent.transform);
        activateButtonInsideParent(goodGrowthParent.transform);
        activateButtonInsideParent(ProtectionParent.transform);
    }

    /// <summary>
    /// Reload arcGIS layers when reloading the map
    /// </summary>
    public void ReloadARCGISlayers()
    {
        ClearParent(geodataParent.gameObject);
        ClearParent(geodataParent.gameObject);

        Dictionary<string, GameObject> tempLayers = new Dictionary<string, GameObject>();
        foreach (KeyValuePair<string, GameObject> keyValuePair in arcgisLoadedLayers)
        {
            tempLayers.Add(keyValuePair.Key, keyValuePair.Value);
        }
        foreach (KeyValuePair<string, GameObject> keyValuePair in tflLoadedLayers)
        {
            tempLayers.Add(keyValuePair.Key, keyValuePair.Value);
        }
        arcgisLoadedLayers = new Dictionary<string, GameObject>();
        tflLoadedLayers = new Dictionary<string, GameObject>();
        foreach (KeyValuePair<string, GameObject> keyValuePair in tempLayers)
        {
            if (mapData[keyValuePair.Key].Source == "ARCGIS")
            {
                LoadGlaMapData(mapData[keyValuePair.Key].Name, mapData[keyValuePair.Key].Code);
            }
            else if (mapData[keyValuePair.Key].Source == "TflApi")
            {
                FetchTFLData(mapData[keyValuePair.Key].Name);
            }

        }
    }
    #endregion
      
    #region Map Styles
    //______________________________________________________________________________//
    //--------------------------- MAP STYLE METHODS -----------------------------//
    /// <summary>
    /// Toggle between flat and elevated terrain.
    /// </summary>
    public void ChangeTerrain()
    {
        if (GetComponent<AbstractMap>().Terrain.ElevationType == ElevationLayerType.FlatTerrain)
        {
            GetComponent<AbstractMap>().Terrain.ElevationType = ElevationLayerType.TerrainWithElevation;
        }
        else
        {
            GetComponent<AbstractMap>().Terrain.ElevationType = ElevationLayerType.FlatTerrain;
        }

    }

    string initialStyle = "mapbox://styles/prismapp/ck6aopqy03dbh1is4t0t14gio";
    string blankStyle = "mapbox://styles/prismapp/ck6mg77ro14i91imzlrdq6imy";
    /// <summary>
    /// Toggle between custom and blank map style image.
    /// </summary>
    public void ChangeStyle()
    {
        if (maps[0].ImageLayer.LayerSourceId == initialStyle)
        {
            maps[0].ImageLayer.SetLayerSource(blankStyle);
        }
        else
        {
            maps[0].ImageLayer.SetLayerSource(initialStyle);
        }
    }

    /// <summary>
    /// Add a pin on the centre of the map.
    /// </summary>
    public void PinOnMap(float lat, float lon)
    {
        Vector2d mapCentre = new Vector2d(lat, lon);
        pin.transform.position = maps[0].GeoToWorldPosition(mapCentre);
    }

    /// <summary>
    /// Reset a Mapbox map
    /// </summary>
    void UpdateMap()
    {
        firsttimeloadingmap++;
        if (firsttimeloadingmap > 1)
        {
            if (mapHasLoaded)
            {
                maps[3].ResetMap();
            }           
        }

    }

    /// <summary>
    /// Activates UI toggle group when a geocoding response is received
    /// </summary>
    /// <param name="latlon"></param>
    public void OnMapLatLonReceived(string latlon)
    {
        ActivateMapToggles();
    }
    /// <summary>
    /// Log to memory that map has finished loading
    /// </summary>
    void MapLoaded()
    {
        mapHasLoaded = true;
    }
    #endregion  

    #region Helpers   

    /// <summary>
    /// Wait to turn a text component off
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    IEnumerator WaitToClose(Text text)
    {
        yield return new WaitForSeconds(0.5f);
        text.enabled = false;
    }
    /// <summary>
    /// Delete all objects inside a UI element
    /// </summary>
    /// <param name="obj"></param>
    public void ClearUIParent(GameObject obj)
    {
        for (int i = 0; i < obj.GetComponent<RectTransform>().childCount; i++)
        {
            Destroy(obj.GetComponent<RectTransform>().GetChild(i).gameObject);
        }
    }
    /// <summary>
    /// Delete all objects inside parent
    /// </summary>
    /// <param name="obj"></param>
    public void ClearParent(GameObject obj)
    {
        for (int i = 0; i < obj.GetComponent<Transform>().childCount; i++)
        {
            Destroy(obj.GetComponent<Transform>().GetChild(i).gameObject);
        }
    }
    /// <summary>
    /// Force immediate layout rebuild to UI elements
    /// </summary>
    /// <param name="obj"></param>
    public void RebuildLayoutUIParent(GameObject obj)
    {
        LayoutRebuilder.ForceRebuildLayoutImmediate(obj.GetComponent<RectTransform>());
    }
    /// <summary>
    /// Activate toggles
    /// </summary>
    /// <param name="parent"></param>
    void activateButtonInsideParent(Transform parent)
    {
        for (int i = 0; i < parent.childCount; i++)
        {
            parent.GetChild(i).GetComponent<UIMapToggle>().toggle.interactable = true;
        }
    }
    #endregion

    #endregion
}

#region Deserialisation Classes
// Json UI classes
/// <summary>
/// Class to store geodata index
/// </summary>
public class Geodata
{
    public string Name { get; set; }
    public string Source { get; set; }
    public string Category { get; set; }
    public string MapID { get; set; }
    public string Type { get; set; }
    public string Structure { get; set; }
    public List<string> UnityMapFeatures { get; set; }
    public List<string> CloseMapFeatures { get; set; }
    public List<string> Colours { get; set; }
    public List<string> Parameters { get; set; }
    public Values Values { get; set; }
    public List<string> Legend { get; set; }
    public string Description { get; set; }
    public string URL { get; set; }
    public int Code { get; set; }
    public int MapIndex { get; set; }
    public bool IsOn { get; set; }
    public bool Is3D { get; set; }
    public bool Chart { get; set; }
    public bool Seasons { get; set; }
}
/// <summary>
/// Class to store values in the geodata index
/// </summary>
public class Values
{
    public Dictionary<string, List<string>> values { get; set; }
}



// GLA ARC GIS deserialisation classes
/// <summary>
/// GLA ARC GIS site properties
/// </summary>
public class FieldAliases
{
    public string sitename { get; set; }
}
/// <summary>
/// GLA ARC GIS api spatial reference
/// </summary>
public class SpatialReference
{
    public int wkid { get; set; }
    public int latestWkid { get; set; }
}
/// <summary>
/// GLA ARC GIS files properties
/// </summary>
public class Field
{
    public string name { get; set; }
    public string type { get; set; }
    public string alias { get; set; }
    public int length { get; set; }
}
/// <summary>
/// GLA ARC GIS feature attributes
/// </summary>
public class Attributes
{
    public string sitename { get; set; }
}
/// <summary>
/// GLA ARC GIS geometru properties
/// </summary>
public class Geometry
{
    public List<List<List<double>>> rings { get; set; }
}
/// <summary>
/// GLA ARC GIS feature properties
/// </summary>
public class Feature
{
    public Attributes attributes { get; set; }
    public Geometry geometry { get; set; }
}
/// <summary>
/// GLA ARC GIS root object
/// </summary>
public class RootObject
{
    public string displayFieldName { get; set; }
    public FieldAliases fieldAliases { get; set; }
    public string geometryType { get; set; }
    public SpatialReference spatialReference { get; set; }
    public List<Field> fields { get; set; }
    public List<Feature> features { get; set; }
}

namespace TFLData
{
    // TFL API deserialisation classes
    //public class Crowding
    //{
    //    public string invalid_type { get; set; }
    //}

    /// <summary>
    /// TFL API - Describe a line object
    /// </summary>
    public class Line
    {
        public string invalid_type { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string uri { get; set; }
        public string type { get; set; }
        public Crowding crowding { get; set; }
        public string routeType { get; set; }
        public string status { get; set; }
    }
    /// <summary>
    /// TFL API - Describe a group of lines
    /// </summary>
    public class LineGroup
    {
        public string invalid_type { get; set; }
        public string naptanIdReference { get; set; }
        public string stationAtcoCode { get; set; }
        public List<string> lineIdentifier { get; set; }
    }
    /// <summary>
    /// TFL API - Line mode group
    /// </summary>
    public class LineModeGroup
    {
        public string _invalid_type { get; set; }
        public string modeName { get; set; }
        public List<string> lineIdentifier { get; set; }
    }

    //public class Child
    //{
    //    public string invalid_type { get; set; }
    //    public string naptanId { get; set; }
    //    public List<object> modes { get; set; }
    //    public string icsCode { get; set; }
    //    public string stationNaptan { get; set; }
    //    public List<object> lines { get; set; }
    //    public List<object> lineGroup { get; set; }
    //    public List<LineModeGroup> lineModeGroups { get; set; }
    //    public bool status { get; set; }
    //    public string id { get; set; }
    //    public string commonName { get; set; }
    //    public string placeType { get; set; }
    //    public List<object> additionalProperties { get; set; }
    //    public List<object> children { get; set; }
    //    public double lat { get; set; }
    //    public double lon { get; set; }
    //    public string hubNaptanCode { get; set; }
    //}

    /// <summary>
    /// TFL API- stop point
    /// </summary>
    public class StopPoint
    {
        public string invalid_type { get; set; }
        public string naptanId { get; set; }
        public List<string> modes { get; set; }
        public string icsCode { get; set; }
        public string stopType { get; set; }
        public string stationNaptan { get; set; }
        public List<Line> lines { get; set; }
        public List<LineGroup> lineGroup { get; set; }
        public List<LineModeGroup> lineModeGroups { get; set; }
        public bool status { get; set; }
        public string id { get; set; }
        public string commonName { get; set; }
        public double distance { get; set; }
        public string placeType { get; set; }
        public List<object> additionalProperties { get; set; }
        public List<Child> children { get; set; }
        public double lat { get; set; }
        public double lon { get; set; }
        public string hubNaptanCode { get; set; }
    }
    /// <summary>
    /// TFL API - root object
    /// </summary>
    public class TFL
    {
        public string invalid_type { get; set; }
        public List<double> centrePoint { get; set; }
        public List<StopPoint> stopPoints { get; set; }
        public int pageSize { get; set; }
        public int total { get; set; }
        public int page { get; set; }
    }




    /// <summary>
    /// TFL API - passenger flow
    /// </summary>
    public class PassengerFlow
    {
        public string __invalidtype { get; set; }
        public string timeSlice { get; set; }
        public int value { get; set; }
    }
    /// <summary>
    /// TFL API - train loading
    /// </summary>
    public class TrainLoading
    {
        public string __invalidtype { get; set; }
        public string line { get; set; }
        public string lineDirection { get; set; }
        public string platformDirection { get; set; }
        public string direction { get; set; }
        public string naptanTo { get; set; }
        public string timeSlice { get; set; }
        public int value { get; set; }
    }
    /// <summary>
    /// TFL API - crowding
    /// </summary>
    public class Crowding
    {
        public string __invalidtype { get; set; }
        public List<PassengerFlow> passengerFlows { get; set; }
        public List<TrainLoading> trainLoadings { get; set; }
    }
    /// <summary>
    /// TFL API - crowd data property
    /// </summary>
    public class AdditionalProperty
    {
        public string __invalidtype { get; set; }
        public string category { get; set; }
        public string key { get; set; }
        public string sourceSystemKey { get; set; }
        public string value { get; set; }
    }
    /// <summary>
    /// TFL API - crowd data property
    /// </summary>
    public class AdditionalProperty2
    {
        public string __invalidtype { get; set; }
        public string category { get; set; }
        public string key { get; set; }
        public string sourceSystemKey { get; set; }
        public string value { get; set; }
    }
    /// <summary>
    /// TFL API - crowd data child
    /// </summary>
    public class Child
    {
        public string __invalidtype { get; set; }
        public string naptanId { get; set; }
        public string indicator { get; set; }
        public string stopLetter { get; set; }
        public List<string> modes { get; set; }
        public string icsCode { get; set; }
        public string stopType { get; set; }
        public string stationNaptan { get; set; }
        public List<object> lines { get; set; }
        public List<object> lineGroup { get; set; }
        public List<object> lineModeGroups { get; set; }
        public bool status { get; set; }
        public string id { get; set; }
        public string commonName { get; set; }
        public string placeType { get; set; }
        public List<AdditionalProperty2> additionalProperties { get; set; }
        public List<object> children { get; set; }
        public double lat { get; set; }
        public double lon { get; set; }
    }
    /// <summary>
    /// TFL API - crowd data
    /// </summary>
    public class CrowdData
    {
        public string __invalidtype { get; set; }
        public string naptanId { get; set; }
        public List<string> modes { get; set; }
        public string icsCode { get; set; }
        public string stopType { get; set; }
        public string stationNaptan { get; set; }
        public List<Line> lines { get; set; }
        public List<LineGroup> lineGroup { get; set; }
        public List<LineModeGroup> lineModeGroups { get; set; }
        public bool status { get; set; }
        public string id { get; set; }
        public string commonName { get; set; }
        public string placeType { get; set; }
        public List<AdditionalProperty> additionalProperties { get; set; }
        public List<Child> children { get; set; }
        public double lat { get; set; }
        public double lon { get; set; }
    }
}

#endregion
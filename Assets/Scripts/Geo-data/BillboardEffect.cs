﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A monobehaviour component to make an item face the camera at all times
/// </summary>
public class BillboardEffect : MonoBehaviour {
    #region Private Variables
    Transform target;
	// Use this for initialization
	void Start () {
        
	}

    #endregion

    #region Private Methods
    // Update is called once per frame
    void Update () {
        target = GameObject.Find("Main Camera").transform;
        Vector3 lookTarget = gameObject.transform.position + (gameObject.transform.position - target.position);
        gameObject.transform.LookAt(lookTarget);
    }
    #endregion
}

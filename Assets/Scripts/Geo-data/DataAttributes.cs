﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class to store data in dictionary
/// </summary>
public class DataAttributes : MonoBehaviour {
    #region Public Variables
    public Dictionary<string, string> attributes = new Dictionary<string, string>();
    #endregion
}

﻿//-----------------------------------------------------------------------
// <copyright file="FeatureBehaviour.cs" company="Mapbox">
//     Copyright (c) 2016 Mapbox. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Mapbox.Unity.MeshGeneration.Components
{
    using UnityEngine;
    using UnityEngine.UI;
    using System.Linq;
    using Mapbox.Unity.MeshGeneration.Data;
    using System;
    /// <summary>
    /// A component to control feature behaviour based on its properties
    /// </summary>
    public class MyFeatureBehaviour : MonoBehaviour
    {
        #region Public Variables
        public VectorEntity VectorEntity;
        public Transform Transform;
        public VectorFeatureUnity Data;

        [Multiline(5)]
        public string DataString;
        #endregion

        #region Public Methods
        /// <summary>
        /// Show object's data on the inspector
        /// </summary>
        public void ShowDebugData()
        {
            DataString = string.Join("\r\n", Data.Properties.Select(x => x.Key + " - " + x.Value.ToString()).ToArray());
        }
        /// <summary>
        /// Visualise data as geolocated points
        /// </summary>
        public void ShowDataPoints()
        {
            foreach (var item in VectorEntity.Feature.Points)
            {
                for (int i = 0; i < item.Count; i++)
                {
                    var go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                    go.name = i.ToString();
                    go.transform.SetParent(transform, false);
                    go.transform.localPosition = item[i];
                }
            }
        }
        /// <summary>
        /// Initialise data collection
        /// </summary>
        /// <param name="ve"></param>
        /// <param name="parameter"></param>
        /// <param name="dataset"></param>
        public void Initialize(VectorEntity ve, string parameter, string dataset)
        {
            VectorEntity = ve;
            Transform = transform;
            Data = ve.Feature;
            showDataInUI(parameter, dataset, Data);
        }
        /// <summary>
        /// Initialise data collection
        /// </summary>
        /// <param name="feature"></param>
        /// <param name="parameter"></param>
        /// <param name="dataset"></param>
        public void Initialize(VectorFeatureUnity feature, string parameter, string dataset)
        {
            Transform = transform;
            Data = feature;
            showDataInUI(parameter, dataset, Data);
        }
        /// <summary>
        /// Show object's data in the respective dataset in the UI 
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="dataset"></param>
        /// <param name="data"></param>
        public void showDataInUI(string parameter, string dataset, VectorFeatureUnity data)
        {
            string value = data.Properties[parameter].ToString();
            GameObject.Find(dataset).GetComponent<UIMapToggle>().ChangeAttributeText(value);
        }
        #endregion
    }
}

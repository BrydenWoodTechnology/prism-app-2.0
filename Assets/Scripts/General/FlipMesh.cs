﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A MonoBehaviour component for flipping meshes in Editor
/// </summary>
public class FlipMesh : MonoBehaviour {

    #region Monobehaviour Methods
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    #endregion

    #region Public Methods
    /// <summary>
    /// Flips the shared mesh of the Mesh Filter
    /// </summary>
    public void FlipSharedMesh()
    {
        Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
        int[] tris = new int[mesh.triangles.Length];
        for (int i = 0; i < tris.Length; i += 3)
        {
            tris[i] = mesh.triangles[i + 2];
            tris[i + 1] = mesh.triangles[i + 1];
            tris[i + 2] = mesh.triangles[i];
        }
        mesh.triangles = tris;
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
    }
    #endregion
}

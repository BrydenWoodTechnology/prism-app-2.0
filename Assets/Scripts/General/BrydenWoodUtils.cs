﻿using BrydenWoodUnity.GeometryManipulation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity
{
    /// <summary>
    /// A class with Unity utilities
    /// </summary>
    public static class BrydenWoodUtils
    {

        //As seen https://stackoverflow.com/questions/1841246/c-sharp-splitting-an-array
        public static void Split<T>(T[] array, int index, out T[] first, out T[] second)
        {
            first = array.Take(index).ToArray();
            second = array.Skip(index).ToArray();
        }

        public static void SplitMidPoint<T>(T[] array, out T[] first, out T[] second)
        {
            Split(array, array.Length / 2, out first, out second);
        }

        /// <summary>
        /// Method to move the parent of objects without moving the children
        /// </summary>
        /// <param name="transform">The parent to be moved</param>
        /// <param name="position">The new location</param>
        public static void MoveParentIndependently(Transform transform, Vector3 position)
        {
            GameObject obj = new GameObject();
            List<Transform> children = new List<Transform>();

            for (int i = 0; i < transform.childCount; i++) children.Add(transform.GetChild(i));

            for (int i = 0; i < children.Count; i++)
            {
                children[i].SetParent(obj.transform);
            }

            transform.position = position;

            for (int i = 0; i < children.Count; i++)
            {
                children[i].SetParent(transform);
            }

            UnityEngine.Object.Destroy(obj);
        }

        /// <summary>
        /// Sets a new parent to allo children
        /// </summary>
        /// <param name="original">The original parent</param>
        /// <param name="newParent">The new parent</param>
        public static void SetNewParentToChildren(Transform original, Transform newParent)
        {
            List<Transform> children = new List<Transform>();
            for (int i = 0; i < original.childCount; i++)
            {
                children.Add(original.GetChild(i));
            }

            for (int i = 0; i < children.Count; i++)
            {
                children[i].SetParent(newParent);
            }
        }

        /// <summary>
        /// Calculates the XZ oriented bounding box of a list of points
        /// </summary>
        /// <param name="points">The list of points</param>
        /// <returns>The corners of the box as an Array of Points</returns>
        public static Vector3[] CalculateBoundsXZ(Vector3[] points)
        {
            float xMin = float.MaxValue;
            float xMax = float.MinValue;
            float zMin = float.MaxValue;
            float zMax = float.MinValue;

            for (int i = 0; i < points.Length; i++)
            {
                if (points[i].x > xMax) xMax = points[i].x;
                if (points[i].x < xMin) xMin = points[i].x;
                if (points[i].z > zMax) zMax = points[i].z;
                if (points[i].z < zMin) zMin = points[i].z;
            }

            return new Vector3[4]
            {
                new Vector3(xMin,0,zMin),
                new Vector3(xMax,0,zMin),
                new Vector3(xMax,0,zMin),
                new Vector3(xMin,0,zMax)
            };
        }

        /// <summary>
        /// Calculates the bounding box of a list of points which is parallel to XZ plane and oriented according to a transform
        /// </summary>
        /// <param name="points">The list of points</param>
        /// <param name="transform">The transform to take the orientation from</param>
        /// <param name="size">The size of the bounding box</param>
        /// <returns>The corners of the box as an Array of Points</returns>
        public static Vector3[] CalculateBoundsXZ(List<Vector3> points, Transform transform, out Vector3 size)
        {
            size = Vector3.zero;
            float xMin = float.MaxValue;
            float xMax = float.MinValue;
            float zMin = float.MaxValue;
            float zMax = float.MinValue;

            for (int i = 0; i < points.Count; i++)
            {
                var pt = transform.InverseTransformPoint(points[i]);
                if (pt.x > xMax) xMax = pt.x;
                if (pt.x < xMin) xMin = pt.x;
                if (pt.z > zMax) zMax = pt.z;
                if (pt.z < zMin) zMin = pt.z;
            }
            size = new Vector3(xMax, 0, zMax) - new Vector3(xMin, 0, zMin);
            return new Vector3[4]
            {
                /*transform.TransformPoint(*/new Vector3(xMin,0,zMin),
                /*transform.TransformPoint(*/new Vector3(xMax,0,zMin),
                /*transform.TransformPoint(*/new Vector3(xMax,0,zMax),
                /*transform.TransformPoint(*/new Vector3(xMin,0,zMax)
            };
        }

        /// <summary>
        /// Checks whether an object is a List
        /// </summary>
        /// <param name="o">The object to be checked</param>
        /// <returns>Whether the object is a List</returns>
        public static bool IsList(object o)
        {
            if (o == null) return false;
            return o is IList &&
                   o.GetType().IsGenericType &&
                   o.GetType().GetGenericTypeDefinition().IsAssignableFrom(typeof(List<>));
        }

        /// <summary>
        /// Calculates the line-height in pixels of the content of a UnityUI Text component
        /// Taken from https://answers.unity.com/questions/1379443/how-can-i-calculate-the-height-of-one-line-in-a-te.html
        /// </summary>
        /// <param name="text">The UnityUI Text component</param>
        /// <returns>Float</returns>
        //As taken from https://answers.unity.com/questions/1379443/how-can-i-calculate-the-height-of-one-line-in-a-te.html
        public static float CalculateLineHeight(Text text)
        {
            var extents = (text.cachedTextGenerator.rectExtents.size * 0.5f);
            var lineHeight = text.cachedTextGeneratorForLayout.GetPreferredHeight("A", text.GetGenerationSettings(extents));

            return lineHeight;
        }

        /// <summary>
        /// Checks whether an object is a Dictionary
        /// </summary>
        /// <param name="o">The object to be checked</param>
        /// <returns>Whether the object is a Dictionary</returns>
        public static bool IsDictionary(object o)
        {
            if (o == null) return false;
            return o is IDictionary &&
                   o.GetType().IsGenericType &&
                   o.GetType().GetGenericTypeDefinition().IsAssignableFrom(typeof(Dictionary<,>));
        }

        /// <summary>
        /// Remaps a value from one domain to another
        /// </summary>
        /// <param name="value">Value to be remapped</param>
        /// <param name="from1">Start of first domain</param>
        /// <param name="to1">End of first domain</param>
        /// <param name="from2">Start of second domain</param>
        /// <param name="to2">End of second domain</param>
        /// <returns></returns>
        public static float Remap(float value, float from1, float to1, float from2, float to2)
        {
            return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
        }

        /// <summary>
        /// Projects a point on the curve between 2 points
        /// </summary>
        /// <param name="start">First point of curve</param>
        /// <param name="end">Second point of curve</param>
        /// <param name="myPoint">The point to project</param>
        /// <param name="snapDist">Threshold for snapping the point if it is close to the ends of the line</param>
        /// <param name="clamped">Whether the projection should be clamped to the line</param>
        /// <returns>The projected point</returns>
        public static Vector3 ProjectOnCurve(Vector3 start, Vector3 end, Vector3 myPoint, float snapDist = 0.0f, bool clamped = false)
        {
            Vector3 projectedPoint = Vector3.Project(myPoint - start, end - start) + start;
            float a = Vector3.Distance(start, projectedPoint);
            float b = Vector3.Distance(end, projectedPoint);
            float c = Vector3.Distance(end, start);

            if (clamped)
            {
                if (a > c || b > c)
                {
                    if (a > b)
                    {
                        return end;
                    }
                    else
                    {
                        return start;
                    }
                }
                else
                {
                    if (a <= snapDist)
                    {
                        return start;
                    }
                    else if (b <= snapDist)
                    {
                        return end;
                    }
                    else
                    {
                        return projectedPoint;
                    }
                }
            }
            else
            {
                return projectedPoint;
            }
        }

        /// <summary>
        /// Checks if a point is inside a Box GameObject
        /// </summary>
        /// <param name="box">The Box GameObject</param>
        /// <param name="point">The Point</param>
        /// <returns>Boolean</returns>
        public static bool IncludedInBox(GameObject box, Vector3 point)
        {
            bool included = false;

            Vector3 relativePos = box.transform.InverseTransformPoint(point);

            if (
                relativePos.x >= -box.transform.localScale.x / 2 && relativePos.x <= box.transform.localScale.x / 2 &&
                relativePos.y >= -box.transform.localScale.y / 2 && relativePos.y <= box.transform.localScale.y / 2 &&
                relativePos.z >= -box.transform.localScale.z / 2 && relativePos.z <= box.transform.localScale.z / 2
                )
                included = true;

            return included;
        }

        /// <summary>
        /// Projects a point on the curve between 2 points
        /// </summary>
        /// <param name="start">First point of curve</param>
        /// <param name="end">Second point of curve</param>
        /// <param name="myPoint">The point to project</param>
        /// <param name="lineParam">Returns the (0-1) parameter of the point on the line</param>
        /// /// <param name="snapDist">Threshold for snapping the point if it is close to the ends of the line</param>
        /// <param name="clamped">Whether the projection should be clamped to the line</param>
        /// <returns>The projected point (Vector3)</returns>
        public static Vector3 ProjectOnCurve(Vector3 start, Vector3 end, Vector3 myPoint, out float lineParam, float snapDist = 0.0f, bool clamped = false)
        {
            Vector3 projectedPoint = Vector3.Project(myPoint - start, end - start) + start;
            float a = Vector3.Distance(start, projectedPoint);
            float b = Vector3.Distance(end, projectedPoint);
            float c = Vector3.Distance(end, start);

            if (clamped)
            {
                if (a > c || b > c)
                {
                    if (a > b)
                    {
                        lineParam = 1;
                        return end;
                    }
                    else
                    {
                        lineParam = 0;
                        return start;
                    }
                }
                else
                {
                    if (a <= snapDist)
                    {
                        lineParam = 0;
                        return start;
                    }
                    else if (b <= snapDist)
                    {
                        lineParam = 1;
                        return end;
                    }
                    else
                    {
                        lineParam = a / c;
                        return projectedPoint;
                    }
                }
            }
            else
            {
                lineParam = a / c;
                return projectedPoint;
            }
        }

        /// <summary>
        /// Return the content of a file
        /// </summary>
        /// <param name="filePath">The file path</param>
        /// <param name="result">The delegate to receive the content</param>
        /// <returns>IEnumerator</returns>
        public static IEnumerator GetFileContent(string filePath, Action<string> result)
        {
            string content = "";
            //As Taken from https://stackoverflow.com/questions/43693213/application-streamingassetspath-and-webgl-build

            //-----For vertexData------//
            if (filePath.Contains("://") || filePath.Contains(":///"))
            {
                WWW www = new WWW(filePath);
                yield return www;
                content = www.text;
                if (!String.IsNullOrEmpty(content))
                {
                    yield return null;
                }
            }
            else
            {
                content = File.ReadAllText(filePath);
                yield return null;
            }
            result(content);
        }

        /// <summary>
        /// Converts Lat-Lon coordinates to XY coordinates
        /// </summary>
        /// <param name="lat1">Origin Latitude</param>
        /// <param name="lon1">Origin Longitude</param>
        /// <param name="lat2">Point Latitude</param>
        /// <param name="lon2">Point Longitude</param>
        /// <returns>Vector3</returns>
        //Based on http://mathforum.org/library/drmath/view/51833.html
        public static Vector3 XZFromLatLon(double lat1, double lon1, double lat2, double lon2)
        {
            float R = 6367000.0f; //Earth's Radius
            float x = (float)((lon2 - lon1) * Math.Cos(lat1) * Math.PI * R / 180);
            float z = (float)((lat2 - lat1) * Math.PI * R / 180);
            return new Vector3(x, 0, z);
        }

        /// <summary>
        /// Sets the layer to all children
        /// </summary>
        /// <param name="parent">The parent of the children</param>
        /// <param name="layer">The new layer</param>
        public static void SetLayerToChildren(this Transform parent, int layer)
        {
            if (!parent.gameObject.name.Contains("LineLoad"))
                parent.gameObject.layer = layer;
            for (int i = 0; i < parent.childCount; i++)
            {
                parent.GetChild(i).SetLayerToChildren(layer);
            }
        }

        public static void ClearNull<T>(ref List<T> list)
        {
            for (int i=list.Count-1; i>=0; i--)
            {
                if (list[i] == null)
                    list.RemoveAt(i);
            }
        }

        public static List<Mesh> MeshFromObj(string data)
        {
            List<Mesh> meshes = new List<Mesh>();

            string[] lines = data.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            Mesh m = null;

            List<Vector3> verts = new List<Vector3>();
            List<int> tris = new List<int>();
            List<Vector3> normals = new List<Vector3>();
            List<Vector2> uvs = new List<Vector2>();
            if (m != null)
            {
                m.vertices = verts.ToArray();
                m.triangles = tris.ToArray();
                if (normals.Count > 0)
                    m.normals = normals.ToArray();
                else
                    m.RecalculateNormals();
                var temp_uvs1 = uvs.ToArray();
                m.uv = temp_uvs1;
                m.uv2 = temp_uvs1;
                m.RecalculateBounds();
                meshes.Add(m);
            }
            m = new Mesh();
            m.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
            verts = new List<Vector3>();
            tris = new List<int>();
            normals = new List<Vector3>();
            for (int i = 0; i < lines.Length; i++)
            {
                var cells = lines[i].Split(' ');
                switch (cells[0])
                {
                    case "g":
                        if (m != null)
                        {
                            m.vertices = verts.ToArray();
                            m.triangles = tris.ToArray();
                            if (normals.Count > 0)
                                m.normals = normals.ToArray();
                            else
                                m.RecalculateNormals();
                            var temp_uvs1 = uvs.ToArray();
                            m.uv = temp_uvs1;
                            m.uv2 = temp_uvs1;
                            m.RecalculateBounds();
                            meshes.Add(m);
                        }
                        m = new Mesh();
                        m.name = cells[1];
                        m.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
                        verts = new List<Vector3>();
                        tris = new List<int>();
                        normals = new List<Vector3>();
                        break;
                    case "o":
                        if (m != null)
                        {
                            m.vertices = verts.ToArray();
                            m.triangles = tris.ToArray();
                            if (normals.Count > 0)
                                m.normals = normals.ToArray();
                            else
                                m.RecalculateNormals();
                            var temp_uvs2 = uvs.ToArray();
                            m.uv = temp_uvs2;
                            m.uv2 = temp_uvs2;
                            m.RecalculateBounds();
                            meshes.Add(m);
                        }
                        m = new Mesh();
                        m.name = cells[1];
                        m.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
                        verts = new List<Vector3>();
                        tris = new List<int>();
                        normals = new List<Vector3>();
                        break;
                    case "v":
                        verts.Add(new Vector3(-float.Parse(cells[1]), float.Parse(cells[2]), float.Parse(cells[3])));
                        break;
                    case "vt":
                        uvs.Add(new Vector2(float.Parse(cells[1]), float.Parse(cells[2])));
                        break;
                    case "vn":
                        normals.Add(new Vector3(float.Parse(cells[1]), float.Parse(cells[2]), float.Parse(cells[3])));
                        break;
                    case "f":
                        if (cells.Length > 4)
                        {
                            ResourcesLoader.TaggedObject.NotificationHelp("The mesh you are trying to import has not been triangulated!");
                            throw new Exception("The mesh you are trying to import has not been triangulated!");
                        }
                        tris.AddRange(new List<int>() { int.Parse(cells[3].Split('/')[0]) - 1, int.Parse(cells[2].Split('/')[0]) - 1, int.Parse(cells[1].Split('/')[0]) - 1 });
                        break;
                }

            }
            m.vertices = verts.ToArray();
            m.triangles = tris.ToArray();
            if (normals.Count > 0)
                m.normals = normals.ToArray();
            else
                m.RecalculateNormals();
            var temp_uvs = uvs.ToArray();
            m.uv = temp_uvs;
            m.uv2 = temp_uvs;
            m.RecalculateBounds();
            meshes.Add(m);

            return meshes;
        }


        //Based on http://www.cs.swan.ac.uk/~cssimon/line_intersection.html
        public static bool LineLineIntersection(Vector3 pt1, Vector3 pt2, Vector3 pt3, Vector3 pt4, out Vector3 intersectionPoint)
        {
            var num1 = (pt3.z - pt4.z) * (pt1.x - pt3.x) + (pt4.x - pt3.x) * (pt1.z - pt3.z);
            var denum1 = (pt4.x - pt3.x) * (pt1.z - pt2.z) - (pt1.x - pt2.x) * (pt4.z - pt3.z);
            var num2 = (pt1.z - pt2.z) * (pt1.x - pt3.x) + (pt2.x - pt1.x) * (pt1.z - pt3.z);
            var denum2 = (pt4.x - pt3.x) * (pt1.z - pt2.z) - (pt1.x - pt2.x) * (pt4.z - pt3.z);

            if (denum1 == 0 || denum2 == 0)
            {
                intersectionPoint = Vector3.zero;
                return false;
            }
            else
            {
                var ta = num1 / denum1;
                var tb = num2 / denum2;

                intersectionPoint = pt1 + (pt2 - pt1) * ta;

                return ta <= 1 && ta >= 0 && tb <= 1 && tb >= 0;
            }
        }

        public static bool RayRayIntersection(Vector3 pt1, Vector3 dir1, Vector3 pt3, Vector3 dir3, out Vector3 intersectionPoint)
        {
            Vector3 pt2 = pt1 + dir1;
            Vector3 pt4 = pt3 + dir3;

            var num1 = (pt3.z - pt4.z) * (pt1.x - pt3.x) + (pt4.x - pt3.x) * (pt1.z - pt3.z);
            var denum1 = (pt4.x - pt3.x) * (pt1.z - pt2.z) - (pt1.x - pt2.x) * (pt4.z - pt3.z);
            var num2 = (pt1.z - pt2.z) * (pt1.x - pt3.x) + (pt2.x - pt1.x) * (pt1.z - pt3.z);
            var denum2 = (pt4.x - pt3.x) * (pt1.z - pt2.z) - (pt1.x - pt2.x) * (pt4.z - pt3.z);

            if (denum1 == 0 || denum2 == 0)
            {
                intersectionPoint = Vector3.zero;
                return false;
            }
            else
            {
                var ta = num1 / denum1;
                var tb = num2 / denum2;

                intersectionPoint = pt1 + (pt2 - pt1) * ta;

                return true;
            }
        }

        public static List<Vector3> RemoveCollinearPoints(List<Vector3> original)
        {
            List<Vector3> points = new List<Vector3>();

            for (int i = 0; i < original.Count; i++)
            {
                int next = (i + 1) % original.Count;
                int prev = i - 1 < 0 ? original.Count - 1 : i - 1;

                var angle = Vector3.Angle(original[next] - original[i], original[prev] - original[i]);
                if (Mathf.Abs(180 - angle) > 1)
                    points.Add(original[i]);
            }

            return points;
        }

        public static void ShiftList<T>(this List<T> list, int shift)
        {
            if (shift > 0)
            {
                List<T> tempStart = new List<T>();
                for (int i = 0; i < shift; i++)
                {
                    tempStart.Add(list[i]);
                }

                for (int i = shift; i < list.Count; i++)
                {
                    list[i - shift] = list[i];
                }

                for (int i = 0; i < shift; i++)
                {
                    list[list.Count - (shift - i)] = tempStart[i];
                }
            }
        }
    }

    public static class Extensions
    {
        private const double Epsilon = 1e-10;

        public static bool IsZero(this double d)
        {
            return Math.Abs(d) < Epsilon;
        }

        public static void SetLayerToChildren(this GameObject obj, int layer)
        {
            obj.layer = layer;
            for (int i = 0; i < obj.transform.childCount; i++)
            {
                obj.transform.GetChild(i).gameObject.SetLayerToChildren(layer);
            }
        }

        public static Vector3[] ToVectorArray(this List<GeometryVertex> pts)
        {
            Vector3[] points = new Vector3[pts.Count];
            for (int i = 0; i < points.Length; i++)
            {
                points[i] = pts[i].currentPosition;
            }
            return points;
        }

        public static Vector3[] RemoveDuplicates(this Vector3[] pts, float threshold = 0.0f)
        {
            List<Vector3> temp = new List<Vector3>();
            temp.Add(pts[0]);
            for (int i = 1; i < pts.Length; i++)
            {
                bool duplicate = false;
                for (int j=0; j<temp.Count; j++)
                {
                    float dist = Vector3.Distance(pts[i], temp[j]);
                    if (dist<=threshold)
                    {
                        duplicate = true;
                        break;
                    }
                }
                if (!duplicate) temp.Add(pts[i]);
            }

            return temp.ToArray();
        }
    }
}

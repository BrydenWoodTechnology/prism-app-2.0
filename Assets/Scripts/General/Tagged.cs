﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BrydenWoodUnity
{
    /// <summary>
    /// A Monobehaviour component with functionality similar to that of a Singleton
    /// </summary>
    /// <typeparam name="T">The type of Singleton</typeparam>
    public class Tagged<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static object mLock = new object();
        private static T _taggedObj;
        private static string toFind;
        private static string[] namespaces;
        private static bool quitting = false;

        /// <summary>
        /// The requested object
        /// </summary>
        public static T TaggedObject
        {
            get
            {
                if (quitting)
                {
                    return null;
                }
                lock (mLock)
                {
                    if (_taggedObj == null)
                    {
                        if (String.IsNullOrEmpty(toFind))
                        {
                            namespaces = typeof(T).ToString().Split('.');
                            toFind = namespaces[namespaces.Length - 1];
                        }
                        _taggedObj = GameObject.FindGameObjectWithTag(toFind).GetComponent<T>();
                        if (_taggedObj==null)
                        {
                            throw new Exception("You need to set the tag for this object: " + toFind);
                        }
                        return _taggedObj;
                    }
                    else
                    {
                        return _taggedObj;
                    }
                }
            }
        }

        private void OnApplicationQuit()
        {
            quitting = true;
        }

        private void OnEnable()
        {
            quitting = false;
        }
    }
}

﻿using BrydenWoodUnity.DesignData;
using BrydenWoodUnity.UIElements;
using Mapbox.Unity.Map;
using Mapbox.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PTAL_Viz : MonoBehaviour {
    #region Public Variables
    public AbstractMap ptalMap;
    public MapHandler mapHandler;
    public ProceduralBuildingManager buildingManager;
    public LayerMask layerMask;
    public Toggle toggle;
    public bool show = false;
    #endregion

    #region Private Variables
    private bool checkIfLoaded;
    private int prevChildCount = -1;
    #endregion

    #region Monobehaviour Methods
    // Use this for initialization
    void Start () {
        MapHandler.locationChanged += OnLocationChanged;
	}
	
	// Update is called once per frame
	void LateUpdate () {

    }
    #endregion

    #region Public Methods
    public void TogglePTAL(bool show)
    {
        this.show = show;
        toggle.SetValue(show);
        checkIfLoaded = true;
        foreach (var item in ptalMap.GetComponentsInChildren<Renderer>())
        {
            item.enabled = show;
        }
    }

    public void OnLocationChanged(float lat, float lon)
    {
        if (show)
        {
            LoadMap();
        }
    }

    public void LoadMap()
    {
        if (mapHandler.latitude.text != null && mapHandler.longitude.text != null)
        {
            Vector2d latlong = new Vector2d(float.Parse(mapHandler.latitude.text.Replace(" ", string.Empty)), float.Parse(mapHandler.longitude.text.Replace(" ", string.Empty)));
            ptalMap.SetCenterLatitudeLongitude(latlong);
        }

        if (ptalMap.transform.childCount == 0)
        {
            ptalMap.SetUpMap();
        }
        else
        {
            Vector2d latlong = new Vector2d(float.Parse(mapHandler.latitude.text), float.Parse(mapHandler.longitude.text));
            ptalMap.UpdateMap(latlong, ptalMap.Zoom);
        }
    }

    public void LoadMap(bool turnOffAFterLoad)
    {
        checkIfLoaded = true;
        show = turnOffAFterLoad;
        if (mapHandler.latitude.text != null && mapHandler.longitude.text != null)
        {
            Vector2d latlong = new Vector2d(float.Parse(mapHandler.latitude.text.Replace(" ", string.Empty)), float.Parse(mapHandler.longitude.text.Replace(" ", string.Empty)));
            ptalMap.SetCenterLatitudeLongitude(latlong);
        }

        if (ptalMap.transform.childCount == 0)
        {
            ptalMap.SetUpMap();
        }
        else
        {
            Vector2d latlong = new Vector2d(float.Parse(mapHandler.latitude.text), float.Parse(mapHandler.longitude.text));
            ptalMap.UpdateMap(latlong, ptalMap.Zoom);
        }
    }
    #endregion
}

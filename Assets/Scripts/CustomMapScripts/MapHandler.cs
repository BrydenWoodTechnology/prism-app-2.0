﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mapbox.Unity.Map;
using Mapbox.Utils;
using Mapbox.Geocoding;
using System.Globalization;
using System;
using Mapbox.Unity;
using Mapbox.Examples;
using Mapbox.Json;
using Mapbox.Utils.JsonConverters;

/// <summary>
/// A Unity component for handling map's functionality
/// </summary>
public class MapHandler : MonoBehaviour
{
    #region Public Variables

    [Header("Mapbox Objects")]
    [Tooltip("The gameobject with the abstract map script")]
    public List<Transform> citySimulators = new List<Transform>();
    [Header("UI Objects")]
    [Tooltip("The button for loading the map")]
    public Button load;
    [Tooltip("The toggle for showing the map pin")]
    public Toggle mapPin;
    [Tooltip("Input field for latitude")]
    public InputField longitude;
    [Tooltip("Input field for longitude")]
    public InputField latitude;
    [Tooltip("Input field for post-code")]
    public InputField postcode;
    [Tooltip("The forward Geocoder Component")]
    public ForwardGeocodeUserInput _searchLocation;
    [Header("Site & Existing building")]
    [Tooltip("The button for deleting existing buildings")]
    public Button delExisting;
    [Tooltip("The layer mask which is used for chacking which buildings are within the site boundary")]
    public int layerMask;
    [Tooltip("Whether the buildings have been deleted")]
    public bool hasDeleted = false;
    [Tooltip("Whether the map has loaded")]
    public bool hasMapLoaded = false;
    #endregion

    #region Private Variables
    string _searchInput = "";
    List<Feature> _features;
    ForwardGeocodeResource _resource;
    bool _isSearching = false;
    List<Transform> existing;
    bool hasSetFocus = false;
    private Vector2 latLon;
    #endregion

    #region Public Properties
    public float currentLat { get; set; }
    public float currentLon { get; set; }
    #endregion

    #region Private Properties
    private bool checkIfLoaded { get; set; }
    private int prevChildCount { get; set; }
    #endregion

    #region Delegates and Events
    /// <summary>
    /// Delegate for when the location has changed
    /// </summary>
    /// <param name="lat">The new latitude</param>
    /// <param name="lon">The new longitude</param>
    public delegate void OnLocationChanged(float lat, float lon);
    /// <summary>
    /// Event triggered when the location has changed
    /// </summary>
    public static event OnLocationChanged locationChanged;

    /// <summary>
    /// Delegate for when the map has loaded
    /// </summary>
    public delegate void OnMapLoaded();
    /// <summary>
    /// Event triggered when the map has loaded
    /// </summary>
    public static event OnMapLoaded mapLoaded;
    #endregion

    #region Monobehaviour methods
    private void OnDestroy()
    {
        if (locationChanged != null)
        {
            foreach (Delegate del in locationChanged.GetInvocationList())
            {
                locationChanged -= (OnLocationChanged)del;
            }
        }
    }

    // Use this for initialization
    void Start()
    {
        load.onClick.AddListener(LoadMap);
        delExisting.onClick.AddListener(DeleteExisting);
        _searchLocation.OnGeocoderResponse += SearchLocation_OnGeocoderResponse;
        longitude.text = (-0.098274).ToString();
        latitude.text = (51.512482).ToString();
        layerMask = 1 << 31;
        existing = new List<Transform>();
        _resource = new ForwardGeocodeResource("");

        HtmlUIManager.OnLatLonReceived += OnMapLatLonReceived;
    }


    // Update is called once per frame
    void Update()
    {
        if (checkIfLoaded)
        {
            int counter = 0;
            foreach (Transform tile in citySimulators[0])
            {
                foreach (Transform b in tile)
                {
                    if (Physics.Raycast(b.GetComponent<MeshRenderer>().bounds.center, -Vector3.up, Mathf.Infinity, layerMask))
                    {
                        counter++;
                    }
                }
            }
            if (prevChildCount == counter)
            {
                DeleteExisting();
            }
            else
            {
                prevChildCount = counter;
            }
        }
    }
    #endregion

    #region Public Method

    /// <summary>
    /// Sets a new latitude and longitude
    /// </summary>
    /// <param name="lat">The new latitude</param>
    /// <param name="lon">The new longitude</param>
    public void SetLatLon(float lat, float lon)
    {
        latLon = new Vector2((float)lat, (float)lon);
        latitude.text = latLon.x.ToString();
        longitude.text = latLon.y.ToString();
        LoadMap();
        currentLat = latLon.x;
        currentLon = latLon.y;
        if (locationChanged != null)
        {
            locationChanged(latLon.x, latLon.y);
        }
       
    }

    /// <summary>
    /// Loads the map
    /// </summary>
    public void LoadMap()
    {
        if (latitude.text != null && longitude.text != null)
        {
            Vector2d latlong = new Vector2d(float.Parse(latitude.text.Replace(" ", string.Empty)), float.Parse(longitude.text.Replace(" ", string.Empty)));
            currentLat = (float)latlong.x;
            currentLon = (float)latlong.y;
            for(int i=0; i<citySimulators.Count; i++)
            {
                citySimulators[i].GetComponent<AbstractMap>().SetCenterLatitudeLongitude(latlong);
            }
           
        }
        for (int i = 0; i < citySimulators.Count; i++)
        {
            if (citySimulators[i].childCount == 0)
            {
                citySimulators[i].GetComponent<AbstractMap>().SetUpMap();
            }
            else
            {
                Vector2d latlong = new Vector2d(float.Parse(latitude.text), float.Parse(longitude.text));
                citySimulators[i].GetComponent<AbstractMap>().UpdateMap(latlong, citySimulators[i].GetComponent<AbstractMap>().Zoom);
            }
        }
        mapPin.isOn = true;
        
        hasMapLoaded = true;

        
    }

    /// <summary>
    /// Deletes the buildings which fall within the site boundary
    /// </summary>
    public void DeleteExisting()
    {
        for (int j = 0; j < citySimulators.Count; j++)
        {
            int counter = 0;
            foreach (Transform tile in citySimulators[j])
            {
                var renderers = tile.GetComponentsInChildren<MeshRenderer>();
                for (int i = 0; i < renderers.Length; i++)
                {
                    if (Physics.Raycast(renderers[i].bounds.center, -Vector3.up, Mathf.Infinity, layerMask))
                    {
                        existing.Add(renderers[i].transform);
                        renderers[i].gameObject.SetActive(false);
                        counter++;
                    }
                }
                var colliders = tile.GetComponentsInChildren<SphereCollider>();
                for (int i = 0; i < colliders.Length; i++)
                {
                    if (Physics.Raycast(colliders[i].bounds.center, -Vector3.up, Mathf.Infinity, layerMask))
                    {
                        existing.Add(colliders[i].transform);
                        colliders[i].transform.parent.parent.gameObject.SetActive(false);
                        counter++;
                    }
                }
                //foreach (Transform b in tile)
                //{

                //}
            }

            if (counter == 0)
            {
                checkIfLoaded = true;
            }
            else
            {
                hasDeleted = true;
                checkIfLoaded = false;
            }
        }
    }

    /// <summary>
    /// Deletes the buildings which exist within the site boundary after loading has finished
    /// </summary>
    public void DeleteAfterLoad()
    {
        foreach (Transform tile in citySimulators[0])
        {

            foreach (Transform b in tile)
            {
                if (Physics.Raycast(b.GetComponent<MeshRenderer>().bounds.center, -Vector3.up, Mathf.Infinity, layerMask))
                {
                    existing.Add(b);
                    b.gameObject.SetActive(false);
                }
            }
        }
        hasDeleted = true;
        checkIfLoaded = false;
    }

    /// <summary>
    /// Sets new latitude and longitude
    /// </summary>
    /// <param name="latlon">The new latitude and longitude separated by a comma</param>
    public void OnMapLatLonReceived(string latlon)
    {
        string[] coords = latlon.Split(',');
        SetLatLon(float.Parse(coords[0]), float.Parse(coords[1]));
        LoadMap();
        postcode.text = "";
    }
    #endregion

    #region LOAD MAP WITH POSTCODE
    void SearchLocation_OnGeocoderResponse(ForwardGeocodeResponse response)
    {
        latLon = new Vector2((float)response.Features[0].Center.x, (float)response.Features[0].Center.y);
        latitude.text = latLon.x.ToString();
        longitude.text = latLon.y.ToString();
        LoadMap();
        currentLat = latLon.x;
        currentLon = latLon.y;
        if (locationChanged != null)
        {
            locationChanged(latLon.x, latLon.y);
        }
       
    }


    /*
    void OnPostcode()
    {
        print("lala");
        _searchInput = postcode.text;
        string oldSearchInput = _searchInput;

        bool changed = oldSearchInput != _searchInput;
        if (changed)
        {
            HandleUserInput(_searchInput);
        }

        if (_features.Count > 0)
        {
            
            for (int i = 0; i < _features.Count; i++)
            {
                Feature feature = _features[i];
                string coordinates = feature.Center.x.ToString(CultureInfo.InvariantCulture) + ", " +
                                            feature.Center.y.ToString(CultureInfo.InvariantCulture);

                //abreviated coords for display in the UI
                string truncatedCoordinates = feature.Center.x.ToString("F2", CultureInfo.InvariantCulture) + ", " +
                    feature.Center.y.ToString("F2", CultureInfo.InvariantCulture);

                //split feature name and add elements until the maxButtonContentLenght is exceeded
                string[] featureNameSplit = feature.PlaceName.Split(',');
                string buttonContent = "";
                int maxButtonContentLength = 30;
                for (int j = 0; j < featureNameSplit.Length; j++)
                {
                    if (buttonContent.Length + featureNameSplit[j].Length < maxButtonContentLength)
                    {
                        if (String.IsNullOrEmpty(buttonContent))
                        {
                            buttonContent = featureNameSplit[j];
                        }
                        else
                        {
                            buttonContent = buttonContent + "," + featureNameSplit[j];
                        }
                    }
                }

                if (buttonContent.Length < maxButtonContentLength + 15)
                {
                    buttonContent = buttonContent + "," + " (" + truncatedCoordinates + ")";
                }


                if (GUILayout.Button(buttonContent))
                {
                    postcode.text = coordinates;
                    //postcode.serializedObject.ApplyModifiedProperties();
                    //EditorUtility.SetDirty(postcode.serializedObject.targetObject);

                    //Close();
                }
            }
        }
        else
        {
            if (_isSearching)
                GUILayout.Label("Searching...");
            else
                GUILayout.Label("No search results");
        }


        if (!hasSetFocus)
        {
            //GUI.FocusControl(searchFieldName);
            hasSetFocus = true;
        }
    }

    void HandleUserInput(string searchString)
    {
        _features = new List<Feature>();
        _isSearching = true;

        if (!string.IsNullOrEmpty(searchString))
        {
            _resource.Query = searchString;
            MapboxAccess.Instance.Geocoder.Geocode(_resource, HandleGeocoderResponse);
        }
    }

    void HandleGeocoderResponse(ForwardGeocodeResponse res)
    {
        //null if no internet connection
        if (res != null)
        {
            //null if invalid token
            if (res.Features != null)
            {
                _features = res.Features;
            }
        }
        _isSearching = false;


        //_hasResponse = true;
        //_coordinate = res.Features[0].Center;
        //Response = res;
        //if (OnGeocoderResponse != null)
        //{
        //	OnGeocoderResponse(this, EventArgs.Empty);
        //}
    }
    */
    #endregion
}

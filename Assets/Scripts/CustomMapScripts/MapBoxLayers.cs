﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mapbox.Unity.Map;

/// <summary>
/// A Unity component to control the visualization of map's buildings
/// </summary>
public class MapBoxLayers : MonoBehaviour {

    #region Public Variables
    /// <summary>
    /// The main rendering camera
    /// </summary>
    public Camera my_camera;
    #endregion

    #region Private Variables
    bool visualize;
    #endregion

    #region Monobehaviour Methods
    // Use this for initialization
    void Start () {
        visualize = true;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    #endregion

    #region Public Methods
    /// <summary>
    /// Toggles whether the map's buildings should be shown or not
    /// </summary>
    public void BuildingsVisualizer()
    {
        visualize = !visualize;
        if (visualize)
        {
            my_camera.cullingMask |= (1 << 13);
        }
        else
        {
            my_camera.cullingMask &= ~(1 << 13);
        }
    }
    #endregion
}

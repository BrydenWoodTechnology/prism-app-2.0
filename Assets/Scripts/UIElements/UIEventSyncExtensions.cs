﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A class with extensions Unity UI elements
    /// </summary>
    //As taken from https://forum.unity.com/threads/change-the-value-of-a-toggle-without-triggering-onvaluechanged.275056/
    public static class UIEventSyncExtensions
    {

        static Slider.SliderEvent emptySliderEvent = new Slider.SliderEvent();
        /// <summary>
        /// Sets the value of the slider without triggering an event
        /// </summary>
        /// <param name="instance">The slider instance</param>
        /// <param name="value">The new value</param>
        public static void SetValue(this Slider instance, float value)
        {
            var originalEvent = instance.onValueChanged;
            instance.onValueChanged = emptySliderEvent;
            instance.value = value;
            instance.onValueChanged = originalEvent;
        }

        static Toggle.ToggleEvent emptyToggleEvent = new Toggle.ToggleEvent();
        /// <summary>
        /// Sets the value of the toggle without triggering an event
        /// </summary>
        /// <param name="instance">The toggle instance</param>
        /// <param name="value">The new value</param>
        public static void SetValue(this Toggle instance, bool value)
        {
            var originalEvent = instance.onValueChanged;
            instance.onValueChanged = emptyToggleEvent;
            instance.isOn = value;
            instance.onValueChanged = originalEvent;
        }

        static InputField.OnChangeEvent emptyInputFieldEvent = new InputField.OnChangeEvent();
        /// <summary>
        /// Sets the value of the input field without triggering an event
        /// </summary>
        /// <param name="instance">The input field instance</param>
        /// <param name="value">The new value</param>
        public static void SetValue(this InputField instance, object value)
        {
            var originalEvent = instance.onValueChanged;
            instance.onValueChanged = emptyInputFieldEvent;
            if (value != null)
                instance.text = value.ToString();
            instance.onValueChanged = originalEvent;
        }

        static Dropdown.DropdownEvent emptyDropdownFieldEvent = new Dropdown.DropdownEvent();
        /// <summary>
        /// Sets the value of the dropdown without triggering an event
        /// </summary>
        /// <param name="instance">The dropdown instance</param>
        /// <param name="value">The new value</param>
        public static void SetValue(this Dropdown instance, int value)
        {
            var originalEvent = instance.onValueChanged;
            instance.onValueChanged = emptyDropdownFieldEvent;
            instance.value = value;
            instance.onValueChanged = originalEvent;
        }

        // TODO: Add more UI types here.

    }
}

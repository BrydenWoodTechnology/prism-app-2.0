﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BrydenWoodUnity.DesignData;
using System;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for  managing the modifications of individual design data entities
    /// </summary>
    public class IndividualModificationsManager : MonoBehaviour
    {
        #region Public Fields and Properties
        [Header("Scene References")]
        public Dropdown m2TypeDropdown;
        public Toggle m2Toggle;
        public Toggle m3Toggle;
        public Text typeText;
        public Button swapButton;
        public Button combineButton;
        public Button resolveToAll;
        public Button resolveToNext;
        public Button undoCombineButton;
        public Toggle flipInterior;

        public ProceduralBuildingManager buildingManager;
        #endregion

        private BaseDesignData selectedGeometry;

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            BaseDesignData.selected += OnSelect;
            Selector.deselect += OnDeselect;
            m2TypeDropdown.ClearOptions();
            var types = Standards.TaggedObject.ApartmentTypesMinimumSizes;
            foreach (var item in types)
            {
                if (item.Key != "Commercial" && item.Key != "Other")
                {
                    if (item.Key == "SpecialApt")
                    {
                        m2TypeDropdown.options.Add(new Dropdown.OptionData("Unresolved"));
                    }
                    else
                    {
                        m2TypeDropdown.options.Add(new Dropdown.OptionData(item.Key));
                    }
                }
            }
            m2TypeDropdown.RefreshShownValue();

            m2TypeDropdown.onValueChanged.AddListener(ChangeType);
            m2TypeDropdown.interactable = false;
            m2Toggle.interactable = false;
            m2Toggle.onValueChanged.AddListener(SetM2Type);
            m3Toggle.interactable = false;
            m3Toggle.onValueChanged.AddListener(SetM3Type);
            swapButton.interactable = false;
            combineButton.interactable = false;
            resolveToAll.interactable = false;
            resolveToNext.interactable = false;
            //undoCombineButton.interactable = false;
            flipInterior.interactable = false;
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnDestroy()
        {
            BaseDesignData.selected -= OnSelect;
            Selector.deselect -= OnDeselect;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Called from the Unity UI to flip the interior of an apartment
        /// </summary>
        /// <param name="flip">Whether to flip the interior</param>
        public void OnFlipInterior(bool flip)
        {
            var apt = selectedGeometry as ApartmentUnity;
            apt.FlipInterior(flip);
        }

        /// <summary>
        /// Called from the Unity UI when the Resolve All button is pressed
        /// </summary>
        public void OnResolveToAll()
        {
            var apt = selectedGeometry as ApartmentUnity;
            if (apt != null)
            {
                StartCoroutine(apt.ResolveWithAll());
            }
        }

        public void OnApartmentSelected(ApartmentUnity apartmentUnity)
        {
            int index = 0;

            if (apartmentUnity.ProceduralFloor.building.buildingType == BuildingType.Mansion)
            {
                var types = Standards.TaggedObject.ApartmentTypesMinimumSizes;
                m2TypeDropdown.ClearOptions();
                foreach (var item in types)
                {
                    if (item.Key != "Commercial" && item.Key != "Other" && item.Key != "Studio")
                    {
                        if (item.Key == apartmentUnity.ApartmentType)
                        {
                            index = m2TypeDropdown.options.Count;
                        }
                        if (item.Key == "SpecialApt")
                        {
                            m2TypeDropdown.options.Add(new Dropdown.OptionData("Unresolved"));
                        }
                        else
                        {
                            m2TypeDropdown.options.Add(new Dropdown.OptionData(item.Key));
                        }
                    }
                }
                m2TypeDropdown.SetValue(index);

                if (apartmentUnity.isM3)
                {
                    m3Toggle.SetValue(true);
                    m2Toggle.SetValue(false);
                }
                else
                {
                    m3Toggle.SetValue(false);
                    m2Toggle.SetValue(true);
                }
                m2TypeDropdown.RefreshShownValue();
            }
            else
            {
                var types = Standards.TaggedObject.ApartmentTypesMinimumSizes;
                m2TypeDropdown.ClearOptions();
                foreach (var item in types)
                {
                    if (item.Key != "Commercial" && item.Key != "Other")
                    {
                        if (item.Key == apartmentUnity.ApartmentType)
                        {
                            index = m2TypeDropdown.options.Count;
                        }
                        if (item.Key == "SpecialApt")
                        {
                            m2TypeDropdown.options.Add(new Dropdown.OptionData("Unresolved"));
                        }
                        else
                        {
                            m2TypeDropdown.options.Add(new Dropdown.OptionData(item.Key));
                        }
                    }
                }
                m2TypeDropdown.SetValue(index);

                if (apartmentUnity.isM3)
                {
                    m3Toggle.SetValue(true);
                    m2Toggle.SetValue(false);
                }
                else
                {
                    m3Toggle.SetValue(false);
                    m2Toggle.SetValue(true);
                }
                m2TypeDropdown.RefreshShownValue();
            }
        }

        /// <summary>
        /// Called from the Unity UI to change the type of the selected apartments
        /// </summary>
        /// <param name="value">The index of the selected type</param>
        public void ChangeType(int value)
        {
            var selectedGeoms = Selector.TaggedObject.selected;
            if (selectedGeoms != null && selectedGeoms.Count > 0)
            {
                for (int i = 0; i < selectedGeoms.Count; i++)
                {
                    var apt = (ApartmentUnity)selectedGeoms[i];
                    if (apt != null)
                    {
                        string type = m2TypeDropdown.options[value].text;
                        apt.OnApartmentTypeChanged((type == "Unresolved") ? "SpecialApt" : type, false);
                    }
                }
            }
        }

        public void SetM3Type(bool isM3)
        {
            if (isM3)
            {
                var selectedGeoms = Selector.TaggedObject.selected;
                if (selectedGeoms != null && selectedGeoms.Count > 0)
                {
                    for (int i = 0; i < selectedGeoms.Count; i++)
                    {
                        var apt = (ApartmentUnity)selectedGeoms[i];
                        if (apt != null)
                        {
                            string type = m2TypeDropdown.options[m2TypeDropdown.value].text;
                            apt.OnApartmentTypeChanged((type == "Unresolved") ? "SpecialApt" : type, true);
                        }
                    }
                }
            }
        }

        public void SetM2Type(bool isM2)
        {
            if (isM2)
            {
                var selectedGeoms = Selector.TaggedObject.selected;
                if (selectedGeoms != null && selectedGeoms.Count > 0)
                {
                    for (int i = 0; i < selectedGeoms.Count; i++)
                    {
                        var apt = (ApartmentUnity)selectedGeoms[i];
                        if (apt != null)
                        {
                            string type = m2TypeDropdown.options[m2TypeDropdown.value].text;
                            apt.OnApartmentTypeChanged((type == "Unresolved") ? "SpecialApt" : type, false);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Called from the Unity UI when the resolve to Next button is pressed
        /// </summary>
        public void OnResolveToNext()
        {
            var apt = selectedGeometry as ApartmentUnity;
            if (apt != null)
            {
                apt.ResolveWithNext();
            }
        }

        /// <summary>
        /// Called when a design data entity is selected
        /// </summary>
        /// <param name="designData">The selected entity</param>
        public void OnSelect(BaseDesignData designData)
        {
            selectedGeometry = designData;
            var infos = designData.GetInfoText();
            swapButton.interactable = true;
            //combineButton.interactable = true;
            //undoCombineButton.interactable = true;
            flipInterior.interactable = true;
            try
            {
                var apt = selectedGeometry as ApartmentUnity;

                if (apt != null)
                {
                    m2TypeDropdown.interactable = true;
                    m2Toggle.interactable = true;
                    m3Toggle.interactable = true;
                    flipInterior.interactable = true;
                    flipInterior.SetValue(apt.flipped);
                    OnApartmentSelected(designData as ApartmentUnity);
                    //for (int i = 0; i < m2TypeDropdown.options.Count; i++)
                    //{
                    //    if (m2TypeDropdown.options[i].text == apt.ApartmentType)
                    //    {
                    //        m2TypeDropdown.SetValue(i);
                    //    }
                    //}
                    if (apt.ApartmentType == "SpecialApt")
                    {
                        resolveToAll.interactable = true;
                        resolveToNext.interactable = true;
                    }
                    else
                    {
                        resolveToAll.interactable = false;
                        resolveToNext.interactable = false;
                    }
                }
                else
                {
                    var room = selectedGeometry as RoomUnity;

                    m2TypeDropdown.interactable = false;
                    m2Toggle.interactable = false;
                    m3Toggle.interactable = false;
                    swapButton.interactable = false;
                    combineButton.interactable = false;
                    resolveToAll.interactable = false;
                    resolveToNext.interactable = false;
                    //undoCombineButton.interactable = false;
                    flipInterior.interactable = false;
                }
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }
        }

        /// <summary>
        /// Called when a design data entity is deselected
        /// </summary>
        public void OnDeselect()
        {
            m2TypeDropdown.interactable = false;
            m2Toggle.interactable = false;
            m3Toggle.interactable = false;
            swapButton.interactable = false;
            combineButton.interactable = false;
            resolveToAll.interactable = false;
            resolveToNext.interactable = false;
            //undoCombineButton.interactable = false;
            flipInterior.interactable = false;
        }
        #endregion
    }
}

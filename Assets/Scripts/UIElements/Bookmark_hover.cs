﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for detecting if the pointer is over a bookmark 
    /// </summary>
    public class Bookmark_hover : MonoBehaviour
    {
        #region Public Fields and Properties
        public RectTransform BookmarkText;
        #endregion

        #region Private Fields and Properties
        private RectTransform rect;
        private Vector3 originalPos;
        private float xPos;
        private WaitForSeconds displayDelay;
        private bool hovering;
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            rect = transform.GetComponent<RectTransform>();
            originalPos = transform.localPosition;
            xPos = transform.localPosition.x;
            displayDelay = new WaitForSeconds(0.3f);
            hovering = false;
            BookmarkText.gameObject.SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {

        }
        public void OnMouseOver()
        {
            hovering = true;
            transform.localScale = new Vector3(1.2f, 1.4f, 1);
            float width = rect.sizeDelta.x;
            float new_width = width * 1.2f;
            float diff = new_width - width;
            transform.localPosition = new Vector3(xPos - diff, originalPos.y, originalPos.z);
            StartCoroutine(ShowText(BookmarkText, true));
        }

        public void OnMouseExit()
        {
            hovering = false;
            transform.localScale = new Vector3(1, 1, 1);
            transform.localPosition = originalPos;
            //StartCoroutine(ShowText (infoText, false));
            BookmarkText.gameObject.SetActive(false);
        }
        #endregion

        #region Private Methods 
        private IEnumerator ShowText(RectTransform text, bool ONOFF)
        {
            yield return displayDelay;
            if (hovering == true)
            {
                text.gameObject.SetActive(ONOFF);
            }
        }
        #endregion
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BrydenWoodUnity.UIElements
{
    public class CustomPolygonPanel : MonoBehaviour
    {
        public CustomPolygonBriefPanel briefPanel;
        public OnBriefChanged onBriedChanged;
        public bool isBasement;
        public bool isPodium;
        // Use this for initialization
        void Start()
        {
            briefPanel.isBasement = isBasement;
            briefPanel.isPodium = isPodium;
            if (onBriedChanged == null)
                onBriedChanged = new OnBriefChanged();
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void OnBriefChanged()
        {
            if (briefPanel != null)
            {
                onBriedChanged.Invoke(new BriefChangedEventArgs
                {
                    index = transform.GetSiblingIndex(),
                    use = briefPanel.GetPercentages()
                });
            }
        }
    }
}

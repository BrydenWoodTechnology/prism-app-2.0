﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    [RequireComponent(typeof(Slider))]
    public class ColorSlider : MonoBehaviour
    {
        public RawImage img;
        public Gradient gradient;
        public OnColourChanged colourChanged;
        // Use this for initialization
        void Awake()
        {
            Texture2D texture = new Texture2D((int)img.rectTransform.rect.width, (int)img.rectTransform.rect.height);
            for (int i=0; i<texture.width; i++)
            {
                for (int j=0; j<texture.height; j++)
                {
                    texture.SetPixel(i, j, gradient.Evaluate((float)i / texture.width));
                }
            }
            texture.Apply();
            img.texture = texture;
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void ResetSliderColour()
        {
            GetComponent<Slider>().value = 0.88f;
        }

        public void OnValueChanged(float val)
        {
            colourChanged.Invoke(gradient.Evaluate(val));
        }

        [Serializable]
        public class OnColourChanged : UnityEvent<Color>
        {
            
        }
    }
}

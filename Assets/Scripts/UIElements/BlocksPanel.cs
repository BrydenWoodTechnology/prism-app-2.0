﻿using BrydenWoodUnity.DesignData;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    public class BlocksPanel : MonoBehaviour
    {
        [Header("Scene References:")]
        public Transform blockElementsParent;
        public Button addBlockButton;
        public InputField apartmentDepthField;
        public InputField levelsField;
        public InputField floor2FloorField;
        public InputField coreWidthField;
        public InputField coreLengthField;

        [Header("Prefabs:")]
        public GameObject blockElementPrefab;

        [Header("Properties:")]
        public List<BlockElement> blockElements;
        public ProceduralMansion proceduralMansion;
        public BuildingElement buildingElement;
        public float f2f = 3.2f;
        public int[] levels = new int[] { 0 - 3 };
        public float aptLength = 11.77f;
        public List<MansionBlock> mansionBlocks { get; private set; }

        public OnBlocksChanged blocksChanged;
        // Start is called before the first frame update
        void Start()
        {
            mansionBlocks = new List<MansionBlock>();
            blockElements = new List<BlockElement>();
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void ClearPrevious()
        {
            List<GameObject> toDelete = new List<GameObject>();
            for (int i = 0; i < blockElementsParent.childCount - 1; i++)
            {
                toDelete.Add(blockElementsParent.GetChild(i).gameObject);
            }

            for (int i = 0; i < toDelete.Count; i++)
            {
                DestroyImmediate(toDelete[i]);
            }
            if (mansionBlocks != null)
                mansionBlocks.Clear();
        }

        public void OnFloorLevelsChanged(string val)
        {
            List<int> lvl = new List<int>();
            string[] values = val.Split('-');
            int initLevel = 0;
            int lastLevel = 0;
            if (values.Length == 1)
            {
                if (int.TryParse(values[0], out initLevel))
                {
                    levels = new int[] { initLevel };
                }
                else
                {
                    Debug.Log("Error with levels");
                    return;
                }
            }
            else if (values.Length > 1)
            {
                if (int.TryParse(values[0], out initLevel) && int.TryParse(values[1], out lastLevel))
                {
                    for (int i = initLevel; i <= lastLevel; i++)
                    {
                        lvl.Add(i);
                    }
                    levels = lvl.ToArray();
                }
                else
                {
                    Debug.Log("Error with levels");
                    return;
                }
            }
            else
            {
                Debug.Log("Error with levels");
                return;
            }
            proceduralMansion.UpdateLevels(f2f, levels);
            buildingElement.buildingData.levels = new List<List<int>>() { levels.ToList() };
        }

        public void OnAptLengthChanged(string val)
        {
            float depth = 11.77f;
            if (float.TryParse(val, out depth))
            {
                buildingElement.buildingData.apartmentDepth = depth;
                proceduralMansion.OnAptLengthChanged(depth);
            }
            else Debug.Log("Issue with apartment depth value!");
        }

        public void OnCoreWidthChanged(string val)
        {
            float width = 4.8f;
            if (float.TryParse(val, out width))
            {
                buildingElement.buildingData.coreWidth = width;
                buildingElement.proceduralBuilding.OnCoreWidthChanged(width);
            }
            else Debug.Log("Issue with core width value!");
        }

        public void OnCoreLengthChanged(string val)
        {
            float length = 7f;
            if (float.TryParse(val, out length))
            {
                buildingElement.buildingData.coreLength = length;
                buildingElement.proceduralBuilding.OnCoreLengthChanged(length);
            }
            else Debug.Log("Issue with core length value!");
        }

        public void OnFloorToFloorChanged(string val)
        {
            if (float.TryParse(val, out f2f))
            {
                proceduralMansion.UpdateLevels(f2f, levels);
            }
            else
            {
                Debug.Log("Error with floor 2 floor height");
            }
        }

        public void AddBlock()
        {
            var blockElement = Instantiate(blockElementPrefab, blockElementsParent).GetComponent<BlockElement>();
            blockElements.Add(blockElement);
            blockElement.blocksPanel = this;
            blockElement.MansionBlock = new MansionBlock();
            mansionBlocks.Add(blockElement.MansionBlock);
            addBlockButton.transform.SetAsLastSibling();
            buildingElement.buildingData.mansionBlocks = mansionBlocks.ToArray();
            BlocksChanged();
        }

        public void AddBlock(MansionBlock block, bool loading = false)
        {
            var blockElement = Instantiate(blockElementPrefab, blockElementsParent).GetComponent<BlockElement>();
            blockElements.Add(blockElement);
            blockElement.blocksPanel = this;
            blockElement.MansionBlock = block;
            if (mansionBlocks == null)
            {
                mansionBlocks = new List<MansionBlock>();
            }
            mansionBlocks.Add(block);
            if (!loading)
                buildingElement.buildingData.mansionBlocks = mansionBlocks.ToArray();
            addBlockButton.transform.SetAsLastSibling();
        }

        public void SetMansionBlocks(List<MansionBlock> blocks)
        {
            for (int i = 0; i < blocks.Count; i++)
            {
                AddBlock(blocks[i]);
            }
        }

        public void RemoveBlock(BlockElement sender)
        {
            int index = blockElements.IndexOf(sender);
            blockElements.Remove(sender);
            mansionBlocks.RemoveAt(index);
            Destroy(sender.gameObject);
            addBlockButton.transform.SetAsLastSibling();
            BlocksChanged();
        }

        public void ChangeBlock(BlockElement sender)
        {
            BlocksChanged();
        }

        public void OnElementSwitchedPosition()
        {
            mansionBlocks.Clear();
            blockElements.Clear();
            for (int i = 0; i < blockElementsParent.childCount - 1; i++)
            {
                blockElements.Add(blockElementsParent.GetChild(i).GetComponent<BlockElement>());
                mansionBlocks.Add(blockElementsParent.GetChild(i).GetComponent<BlockElement>().MansionBlock);
            }
            BlocksChanged();
        }

        private void BlocksChanged()
        {
            buildingElement.buildingData.mansionBlocks = mansionBlocks.ToArray();
            if (blocksChanged != null)
                blocksChanged.Invoke(mansionBlocks);
        }
    }

    [Serializable]
    public class OnBlocksChanged : UnityEvent<List<MansionBlock>>
    {

    }
}

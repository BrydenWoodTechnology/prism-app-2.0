﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for Tooltip of the UI Elements
    /// </summary>
    public class Tooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        #region Public Fields and Properties
        [Header("Tooltip related info:")]
        [Tooltip("The text to be desplayed")]
        public string text;
        [Tooltip("The color of the text")]
        public Color textColor = Color.black;
        [Tooltip("The offset in the position pof the text (local)")]
        public Vector2 offset = new Vector2(30, 70);
        [Tooltip("The delay in appearing (milliseconds)")]
        public float delay = 1000;

        [Space(10)]
        [Header("Prefabs and materials:")]
        [Tooltip("The Text prefab to be instantiated")]
        public GameObject textPrefab;
        #endregion

        #region Private Fields and Properties
        private GameObject m_tooltip;
        private DateTime entered;
        private bool over = false;
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (over)
            {
                if ((DateTime.Now - entered).TotalMilliseconds > 500)
                {
                    if (m_tooltip == null)
                    {
                        m_tooltip = Instantiate(textPrefab, transform);
                        var t = m_tooltip.GetComponent<Text>();
                        t.color = textColor;
                        t.text = text;
                        m_tooltip.GetComponent<RectTransform>().localPosition = offset;
                    }
                }
            }
        }
        #endregion

        #region Interface Methods
        public void OnPointerEnter(PointerEventData eventData)
        {
            over = true;
            entered = DateTime.Now;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            over = false;
            if (m_tooltip != null)
            {
                Destroy(m_tooltip);
            }
        }
        #endregion
    }
}

﻿using BrydenWoodUnity.DesignData;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for managing the floor elements
    /// </summary>
    public class FloorPanel : MonoBehaviour
    {
        #region PUblic Fields and Properties
        [Header("Scene References:")]
        public Transform floorElementHolder;
        public GameObject floorElementPrefab;
        public BriefPanel briefPanel;
        public BuildingPanel buildingPanel;
        public int currentElementIndex;
        public Button customPlButton;
        public Button addFloorButton;

        [Header("Runtime References:")]
        public FloorElement currentElement;
        public BuildingData currentBuilding;

        public int Count { get { return floorElementHolder.childCount - 1; } }
        #endregion

        #region Events
        /// <summary>
        /// Used when a floor element has changed
        /// </summary>
        /// <param name="index">The index of the floor element</param>
        /// <param name="element">The element that changed</param>
        public delegate void OnFloorChanged(int index, FloorElement element);
        /// <summary>
        /// Triggeres when a floor element has changed
        /// </summary>
        public static event OnFloorChanged floorChanged;
        /// <summary>
        /// Triggered when a floor element has been deleted
        /// </summary>
        public static event OnFloorChanged floorDeleted;
        #endregion

        #region MonoBehaviour Methods
        private void OnDestroy()
        {
            if (floorChanged != null)
            {
                foreach (Delegate eh in floorChanged.GetInvocationList())
                {
                    floorChanged -= (OnFloorChanged)eh;
                }
            }
            if (floorDeleted != null)
            {
                foreach (Delegate eh in floorDeleted.GetInvocationList())
                {
                    floorDeleted -= (OnFloorChanged)eh;
                }
            }
            RemoveEvents();
        }

        // Use this for initialization
        void Start()
        {
            AddEvents();
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        public void OnCoreAlignmentChanged(CoreAllignment coreAllignment, LinearTypology linearTypology)
        {
            for(int i=0; i<floorElementHolder.childCount; i++)
            {
                var element = floorElementHolder.GetChild(i).GetComponent<FloorElement>();
                if (element!=null)
                {
                    element.OnCoreAlignmentChanged(coreAllignment, linearTypology);
                }
            }
        }
        public bool CheckLevelOverlap()
        {
            bool overlap = false;

            if (floorElementHolder.childCount > 2)
            {
                for (int i = 1; i < floorElementHolder.childCount - 1; i++)
                {
                    var e1 = floorElementHolder.GetChild(i - 1).GetComponent<FloorElement>();
                    var e2 = floorElementHolder.GetChild(i).GetComponent<FloorElement>();
                    if (e1.levels != null && e1.levels.Count > 0)
                    {
                        if (e2.levels != null && e2.levels.Count > 0)
                        {
                            if (e1.levels[e1.levels.Count - 1] - e2.levels[0] != -1)
                            {
                                overlap = true;
                            }
                        }
                    }
                }
            }

            return overlap;
        }

        /// <summary>
        /// Clears the list of floor elements
        /// </summary>
        public void ClearPrevious()
        {
            List<GameObject> toDelete = new List<GameObject>();
            for (int i = 0; i < floorElementHolder.childCount - 1; i++)
            {
                toDelete.Add(floorElementHolder.GetChild(i).gameObject);
            }

            for (int i = 0; i < toDelete.Count; i++)
            {
                DestroyImmediate(toDelete[i]);
            }
        }

        /// <summary>
        /// Adds a floor element to the list
        /// </summary>
        public void AddFloorElement()
        {
            if (currentElement != null)
            {
                currentElement.ToggleRemoveButton(false);
                currentElement.Deselect();
            }
            if (briefPanel != null)
            {
                briefPanel.ClearPrevious();
            }
            GameObject obj = Instantiate(floorElementPrefab, floorElementHolder);
            obj.transform.SetSiblingIndex(floorElementHolder.childCount - 2);
            obj.name = "FloorElement_" + (floorElementHolder.childCount - 1);
            currentElement = obj.GetComponent<FloorElement>();
            obj.GetComponent<FloorElement>().floorPanel = this;
            floorElementHolder.GetComponent<RectTransform>().sizeDelta = new Vector3(floorElementHolder.GetComponent<RectTransform>().sizeDelta.x, floorElementHolder.childCount * floorElementPrefab.GetComponent<RectTransform>().sizeDelta.y);
            currentElement.proceduralFloor = buildingPanel.OnFloorAdded(currentElement, floorElementHolder.childCount - 2);
            currentElement.proceduralFloor.Select();
            obj.GetComponent<Toggle>().SetValue(true);
        }

        /// <summary>
        /// Sets the selected floor element
        /// </summary>
        /// <param name="index">The index of the selected floor element</param>
        public void SetSelected(int index)
        {
            //floorElementHolder.GetChild(index).GetComponent<Toggle>().isOn = true;
            floorElementHolder.GetChild(index).GetComponent<FloorElement>().Selected(true);
            buildingPanel.designInputTabs.floorIndex = index;
        }

        /// <summary>
        /// Adds a floor element to the list, with set values
        /// </summary>
        /// <param name="levels">The levels of the floor element</param>
        /// <param name="floorHeight">The floor to floor height of the element</param>
        /// <param name="triggerEvent">Whether an event should be triggered</param>
        public void AddFloorElement(List<int> levels, float floorHeight, ProceduralFloor floor, bool triggerEvent = true)
        {
            if (currentElement != null)
            {
                currentElement.ToggleRemoveButton(false);
                currentElement.Deselect();
            }
            GameObject obj = Instantiate(floorElementPrefab, floorElementHolder);
            obj.transform.SetSiblingIndex(floorElementHolder.childCount - 2);
            obj.name = "FloorElement_" + (floorElementHolder.childCount - 1);
            currentElement = obj.GetComponent<FloorElement>();
            obj.GetComponent<FloorElement>().floorPanel = this;
            currentElement.levels = levels;
            currentElement.SetValues(levels, floorHeight, 7.2f, 7.2f);

            if (levels.Count == 1 && levels[0] == 0)
            {
                currentElement.SetAsGroundFloor();
            }
            currentElement.proceduralFloor = floor;
            floor.Select();
            if (briefPanel != null)
            {
                briefPanel.ClearPrevious();
                foreach (var item in floor.percentages)
                {
                    briefPanel.AddBriefElement(item.Key, item.Value);
                }
            }

            floorElementHolder.GetComponent<RectTransform>().sizeDelta = new Vector3(floorElementHolder.GetComponent<RectTransform>().sizeDelta.x, floorElementHolder.childCount * floorElementPrefab.GetComponent<RectTransform>().sizeDelta.y);
        }

        public void LoadTowerFloorElement(List<int> levels, float floorHeight, Dictionary<string, float> percentages, ProceduralFloor floor, bool triggerEvent = true)
        {
            if (currentElement != null)
            {
                currentElement.ToggleRemoveButton(false);
                currentElement.Deselect();
            }
            GameObject obj = Instantiate(floorElementPrefab, floorElementHolder);
            obj.transform.SetSiblingIndex(floorElementHolder.childCount - 2);
            obj.name = "FloorElement_" + (floorElementHolder.childCount - 1);
            currentElement = obj.GetComponent<FloorElement>();
            obj.GetComponent<FloorElement>().floorPanel = this;
            currentElement.levels = levels;
            var tower = floor as ProceduralTower;

            currentElement.SetValues(levels, floorHeight, 7.2f, 7.2f, (tower != null) ? tower.aptTypesList : null);
            if (levels.Count == 1 && levels[0] == 0)
            {
                currentElement.SetAsGroundFloor();
            }
            currentElement.proceduralFloor = floor;
            if (triggerEvent)
            {
                floor.Select();
            }
            if (briefPanel != null)
            {
                briefPanel.ClearPrevious();
                foreach (var item in floor.percentages)
                {
                    briefPanel.AddBriefElement(item.Key, item.Value);
                }
            }

            floorElementHolder.GetComponent<RectTransform>().sizeDelta = new Vector3(floorElementHolder.GetComponent<RectTransform>().sizeDelta.x, floorElementHolder.childCount * floorElementPrefab.GetComponent<RectTransform>().sizeDelta.y);
        }

        public void LoadFloorElement(List<int> levels, float floorHeight, Dictionary<string, float> percentages, ProceduralFloor floor, bool triggerEvent = true)
        {
            if (currentElement != null)
            {
                currentElement.ToggleRemoveButton(false);
                currentElement.Deselect();
            }
            GameObject obj = Instantiate(floorElementPrefab, floorElementHolder);
            obj.transform.SetSiblingIndex(floorElementHolder.childCount - 2);
            obj.name = "FloorElement_" + (floorElementHolder.childCount - 1);
            currentElement = obj.GetComponent<FloorElement>();
            obj.GetComponent<FloorElement>().floorPanel = this;
            currentElement.levels = levels;
            currentElement.proceduralFloor = floor;
            currentElement.LoadValues(levels, floorHeight, floor.leftAptDepth, floor.rightAptDepth);


            if (levels.Count == 1 && levels[0] == 0)
            {
                currentElement.SetAsGroundFloor();
            }
            if (briefPanel != null)
            {

                briefPanel.ClearPrevious();
                //floor.Select();
                foreach (var item in percentages)
                {
                    briefPanel.LoadBriefElement(item.Key, item.Value);
                }
            }

            floorElementHolder.GetComponent<RectTransform>().sizeDelta = new Vector3(floorElementHolder.GetComponent<RectTransform>().sizeDelta.x, floorElementHolder.childCount * floorElementPrefab.GetComponent<RectTransform>().sizeDelta.y);
        }

        public void AddFloorElement(List<int> levels, float floorHeight, ProceduralFloor floor, bool generatePercentages, bool triggerEvent = true)
        {
            if (currentElement != null)
            {
                currentElement.ToggleRemoveButton(false);
                currentElement.Deselect();
            }
            GameObject obj = Instantiate(floorElementPrefab, floorElementHolder);
            obj.transform.SetSiblingIndex(floorElementHolder.childCount - 2);
            obj.name = "FloorElement_" + (floorElementHolder.childCount - 1);
            currentElement = obj.GetComponent<FloorElement>();
            obj.GetComponent<FloorElement>().floorPanel = this;
            currentElement.levels = levels;
            currentElement.SetValues(levels, floorHeight, 7.2f, 7.2f);
            if (levels.Count == 1 && levels[0] == 0)
            {
                currentElement.SetAsGroundFloor();
            }
            currentElement.proceduralFloor = floor;
            if (triggerEvent)
            {
                floor.Select();
            }
            if (generatePercentages && briefPanel != null)
            {
                briefPanel.ClearPrevious();
                foreach (var item in floor.percentages)
                {
                    briefPanel.AddBriefElement(item.Key, item.Value);
                }
            }

            floorElementHolder.GetComponent<RectTransform>().sizeDelta = new Vector3(floorElementHolder.GetComponent<RectTransform>().sizeDelta.x, floorElementHolder.childCount * floorElementPrefab.GetComponent<RectTransform>().sizeDelta.y);
        }

        public void AddFloorElement(List<int> levels, float floorHeight, ProceduralTower floor, bool generatePercentages, bool triggerEvent = true)
        {
            if (currentElement != null)
            {
                currentElement.ToggleRemoveButton(false);
                currentElement.Deselect();
            }
            GameObject obj = Instantiate(floorElementPrefab, floorElementHolder);
            obj.transform.SetSiblingIndex(floorElementHolder.childCount - 2);
            obj.name = "FloorElement_" + (floorElementHolder.childCount - 1);
            currentElement = obj.GetComponent<FloorElement>();
            obj.GetComponent<FloorElement>().floorPanel = this;
            currentElement.levels = levels;
            var tower = floor as ProceduralTower;

            currentElement.SetValues(levels, floorHeight, 7.2f, 7.2f, (tower != null) ? tower.aptTypesList : null);
            if (levels.Count == 1 && levels[0] == 0)
            {
                currentElement.SetAsGroundFloor();
            }
            currentElement.proceduralFloor = floor;
            if (triggerEvent)
            {
                floor.Select();
            }
            if (generatePercentages && briefPanel != null)
            {
                briefPanel.ClearPrevious();
                foreach (var item in floor.percentages)
                {
                    briefPanel.AddBriefElement(item.Key, item.Value);
                }
            }

            floorElementHolder.GetComponent<RectTransform>().sizeDelta = new Vector3(floorElementHolder.GetComponent<RectTransform>().sizeDelta.x, floorElementHolder.childCount * floorElementPrefab.GetComponent<RectTransform>().sizeDelta.y);
        }


        public void AddGroundFloor(ProceduralFloor floor)
        {
            AddFloorElement(new List<int>() { 0 }, 4.5f, floor, true);
        }

        public void AddSecondFloor()
        {
            addFloorButton.onClick.Invoke();
        }

        /// <summary>
        /// Called when a floor element is deleted
        /// </summary>
        /// <param name="sender">The deleted floor element</param>
        public void OnFloorElementDeleted(FloorElement sender)
        {
            StartCoroutine(RemoveFloor(sender));
        }

        /// <summary>
        /// Removes a floor element from the list
        /// </summary>
        /// <param name="sender">The floor element to be removed</param>
        /// <returns></returns>
        public IEnumerator RemoveFloor(FloorElement sender)
        {
            int index = -1;
            for (int i = 0; i < floorElementHolder.childCount - 1; i++)
            {
                if (floorElementHolder.GetChild(i).GetComponent<FloorElement>() == sender)
                {
                    Destroy(floorElementHolder.GetChild(i).gameObject);
                    index = i;
                }
            }
            if (index > 0)
            {
                floorElementHolder.GetChild(index - 1).GetComponent<FloorElement>().ToggleRemoveButton(true);
            }
            floorElementHolder.GetComponent<RectTransform>().sizeDelta = new Vector3(floorElementHolder.GetComponent<RectTransform>().sizeDelta.x, floorElementHolder.childCount * floorElementPrefab.GetComponent<RectTransform>().sizeDelta.y);
            buildingPanel.OnFloorDeleted(index);
            yield return new WaitForEndOfFrame();
            if (floorElementHolder.childCount > 1)
                SetSelected(floorElementHolder.childCount - 2);
        }

        /// <summary>
        /// Called when a floor element is selected
        /// </summary>
        /// <param name="sender"></param>
        public void OnFloorElementSelected(FloorElement sender)
        {
            if (currentElement != null)
            {
                currentElement.Deselect();
            }
            currentElement = sender;
            for (int i = 0; i < floorElementHolder.childCount - 1; i++)
            {
                if (floorElementHolder.GetChild(i).GetComponent<FloorElement>() == sender)
                {
                    currentElementIndex = i;
                    buildingPanel.designInputTabs.floorIndex = currentElementIndex;
                    if (briefPanel != null)
                    {
                        briefPanel.ClearPrevious();
                        if (sender.proceduralFloor.percentages != null)
                        {
                            foreach (var item in sender.proceduralFloor.percentages)
                            {
                                briefPanel.AddBriefElement(item.Key, item.Value);
                            }
                        }
                    }
                }
            }
        }

        public void OnFloorValuesChanged(FloorElement sender)
        {
            bool isFromPanel = false;
            for (int i = 0; i < floorElementHolder.childCount - 1; i++)
            {
                if (floorElementHolder.GetChild(i).GetComponent<FloorElement>() == sender)
                {
                    isFromPanel = true;
                    if (isFromPanel)
                    {
                        currentElementIndex = i;
                        buildingPanel.UpdateFloorValues(currentElementIndex, sender.levels, sender.floorHeight, sender.leftOffset, sender.rightOffset, sender.aptTypes);
                    }
                }
            }

            if (isFromPanel)
            {
                if (currentElementIndex < floorElementHolder.childCount - 2)
                {
                    var floorElement = floorElementHolder.GetChild(currentElementIndex + 1).GetComponent<FloorElement>();
                    var delta = floorElement.GetLevelsDelta();
                    List<int> levels = new List<int>();
                    for (int i = 0; i <= delta; i++)
                    {
                        levels.Add(floorElementHolder.GetChild(currentElementIndex).GetComponent<FloorElement>().levels.Last() + 1 + i);
                    }
                    floorElement.SetLevelsWithEvent(levels);
                }
            }
        }

        public bool CheckForMaxLevel(int delta)
        {
            if (buildingPanel.currentElement.buildingData.levels != null && buildingPanel.currentElement.buildingData.levels.Count > 1 && buildingPanel.currentElement.buildingData.levels.Last() != null)
            {
                var currentMaxLevel = buildingPanel.currentElement.buildingData.levels.Last().Last();
                var newMaxLevel = currentMaxLevel + delta;
                var towerHeights = Standards.TaggedObject.towerHeightsForCores;
                int currentTier = 0;
                int nextTier = 0;

                if (currentMaxLevel < towerHeights[0])
                {
                    currentTier = 0;
                }
                else if (currentMaxLevel >= towerHeights[0] && currentMaxLevel < towerHeights[1])
                {
                    currentTier = 1;
                }
                else
                {
                    currentTier = 2;
                }

                if (newMaxLevel < towerHeights[0])
                {
                    nextTier = 0;
                }
                else if (newMaxLevel >= towerHeights[0] && newMaxLevel < towerHeights[1])
                {
                    nextTier = 1;
                }
                else
                {
                    nextTier = 2;
                }

                return nextTier != currentTier;
            }
            else
            {
                return false;
            }
        }

        public void OnBriefChanged()
        {
            if (briefPanel != null)
            {
                currentElement.proceduralFloor.percentages = briefPanel.GetPercentages();
                buildingPanel.OnBriefChanged(currentElementIndex, currentElement.proceduralFloor.percentages);
            }
        }
        #endregion

        #region Private Methods
        private void AddEvents()
        {
            FloorElement.deleted += OnFloorElementDeleted;
            FloorElement.selected += OnFloorElementSelected;
            FloorElement.changed += OnFloorValuesChanged;
        }

        private void RemoveEvents()
        {
            FloorElement.deleted -= OnFloorElementDeleted;
            FloorElement.selected -= OnFloorElementSelected;
            FloorElement.changed -= OnFloorValuesChanged;
        }
        #endregion
    }
}

﻿using BrydenWoodUnity.DesignData;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour Tagged for prompting the user to refresh the whole project
    /// </summary>
    public class RefreshPopUp : Tagged<RefreshPopUp>
    {
        public ProceduralBuildingManager buildingManager;
        public List<KeyValuePair<ICancelable, string[]>> changedValues = new List<KeyValuePair<ICancelable, string[]>>();

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            //changedValues = new Dictionary<ICancelable, string[]>();
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Called when the new values should be submitted and the project refreshed
        /// </summary>
        public void SubmitValues()
        {
            foreach (var item in changedValues)
            {              
                item.Key.SubmitValue(item.Value[1]);
            }
            changedValues.Clear();
            GetComponent<MovePanelOnCanvas>().OnToggle(false);
        }

        /// <summary>
        /// Called when the new values should be discarded
        /// </summary>
        public void CancelValues()
        {
            foreach (var item in changedValues)
            {
                item.Key.ResetValue(item.Value[0]);
            }
            changedValues.Clear();
            GetComponent<MovePanelOnCanvas>().OnToggle(false);
        }

        /// <summary>
        /// Adds a value to the list of updated values
        /// </summary>
        /// <param name="sender">The object with the updated value</param>
        /// <param name="value">The new and the old values</param>
        public virtual void AddChangedValue(ICancelable sender, string[] value)
        {
            if (buildingManager.proceduralBuildings.Count > 0)
            {
                GetComponent<MovePanelOnCanvas>().OnToggle(true);
                changedValues.Add(new KeyValuePair<ICancelable, string[]>(sender, value));
            }
            else
            {
                changedValues.Add(new KeyValuePair<ICancelable, string[]>(sender, value));
                SubmitValues();
            }
        }
        #endregion
    }
}

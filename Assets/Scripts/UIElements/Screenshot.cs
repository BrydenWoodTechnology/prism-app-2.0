﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    public class Screenshot : MonoBehaviour
    {
        public GameObject[] uiElements;
        public ResourcesLoader resourcesLoader;
        public Camera mainCam;
        public Camera topCam;

        private Camera cam;
        private bool[] alreadyOn;

        private void Start()
        {
            alreadyOn = new bool[uiElements.Length];
        }

        public void ScreenShot()
        {
            StartCoroutine(TakeScreenShot());
        }

        private IEnumerator TakeScreenShot()
        {
            if (mainCam.gameObject.activeSelf) cam = mainCam;
            else cam = topCam;
            for (int i = 0; i < uiElements.Length; i++)
            {
                alreadyOn[i] = uiElements[i].activeSelf;
                uiElements[i].SetActive(false);
            }
            yield return new WaitForEndOfFrame();

            Texture2D screenShot = new Texture2D(cam.pixelWidth, cam.pixelHeight, TextureFormat.RGB24, false);
            screenShot.ReadPixels(new Rect(0, 0, cam.pixelWidth, cam.pixelHeight), 0, 0);
            screenShot.Apply();

            resourcesLoader.ExportScreenShot(screenShot.EncodeToPNG());

            yield return new WaitForEndOfFrame();
            for (int i = 0; i < uiElements.Length; i++)
            {
                uiElements[i].SetActive(alreadyOn[i]);
            }
        }
    }
}

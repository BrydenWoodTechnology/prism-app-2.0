﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for moving UI elements on Canvas between two fixed positions
    /// </summary>
    public class MovePanelOnCanvas : MonoBehaviour
    {
        #region Public Fields and Properties
        public Vector3 onPosition;
        public Vector3 offPosition;
        public bool inCoroutine = false;
        public GameObject[] objectsToClose;
        #endregion

        private bool[] openObjects;

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            openObjects = new bool[objectsToClose.Length];
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Toggles the position of the UI element
        /// </summary>
        /// <param name="show">On or Off</param>
        public void OnToggle(bool show)
        {
            if (inCoroutine) { }
            else
            {
                if (show)
                {
                    GetComponent<RectTransform>().anchoredPosition = onPosition;
                    for (int i = 0; i < objectsToClose.Length; i++)
                    {
                        objectsToClose[i].SetActive(openObjects[i]);
                    }
                }
                else
                {
                    GetComponent<RectTransform>().anchoredPosition = offPosition;
                    for (int i=0; i<objectsToClose.Length; i++)
                    {
                        openObjects[i] = objectsToClose[i].activeSelf;
                        objectsToClose[i].SetActive(false);                
                    }
                    if (GetComponent<PanelDefaultState>() != null)
                        GetComponent<PanelDefaultState>().ReturnToDefault();
                }
            }
        }
        #endregion
    }
}

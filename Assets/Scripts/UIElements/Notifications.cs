﻿using BrydenWoodUnity.DesignData;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for managing the general notifications
    /// </summary>
    public class Notifications : Tagged<Notifications>
    {
        #region Public Fields and Properties
        [Header("Scene References:")]
        public Text body;

        [Header("User Input:")]
        public string[] notificationTitles;
        public string[] notificationsBody;

        [Header("Events:")]
        public NotificationsEvent OnNotificationsUpdate;

        [HideInInspector]
        public List<string> activeNotifications;
        public List<string> roomNotifications;
        #endregion

        #region Private Fields and Properties
        private Dictionary<string, string> nots;
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Awake()
        {
            nots = new Dictionary<string, string>();
            for (int i=0; i<notificationTitles.Length; i++)
            {
                nots.Add(notificationTitles[i], notificationsBody[i]);
            }
            activeNotifications = new List<string>();
            roomNotifications = new List<string>();
            UpdateNotifications();
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        public void SetNotificationBody(string key, string body)
        {
            if (nots != null)
            {
                int index = -1;
                for (int i = 0; i < notificationTitles.Length; i++)
                {
                    if (notificationTitles[i] == key)
                    {
                        index = i;
                        nots[notificationTitles[i]] = body;
                    }
                }
                notificationsBody[index] = body;
            }
        }

        /// <summary>
        /// Updates the notifications displayed
        /// </summary>
        public void UpdateNotifications()
        {
            string bodyText = "";
            ReportData.TaggedObject.generalWarnings = new string[activeNotifications.Count];
            for (int i=0; i<activeNotifications.Count; i++)
            {
                if (nots.ContainsKey(activeNotifications[i]))
                {
                    ReportData.TaggedObject.generalWarnings[i] = nots[activeNotifications[i]];
                    bodyText += string.Format("- {0}\r\n\r\n", nots[activeNotifications[i]]);
                }
            }
            for (int i=0;i<roomNotifications.Count; i++)
            {
                bodyText += string.Format("- {0}\r\n\r\n", roomNotifications[i]);
            }
            body.text = bodyText;
            if (OnNotificationsUpdate != null)
            {
                OnNotificationsUpdate.Invoke(activeNotifications.Count != 0);
            }
        }

        public void AddRoomNotification(string notification)
        {
            if (!roomNotifications.Contains(notification)) roomNotifications.Add(notification);
        }

        public void RemoveRoomNotification(string notification)
        {
            if (roomNotifications.Contains(notification)) roomNotifications.Remove(notification);
        }
        #endregion
    }
}

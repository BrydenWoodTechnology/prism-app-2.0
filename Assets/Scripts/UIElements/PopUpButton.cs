﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for controlling pop-up elements via buttons
    /// </summary>
    public class PopUpButton : MonoBehaviour
    {
        #region Public Fields and Properties
        [Header("Scene References:")]
        public GameObject panel;
        public Image image;

        [Header("User Input:")]
        public Color on;
        public Color off;
        public bool pressed = false;
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            SetPanel(pressed);
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Called when the button is pressed
        /// </summary>
        public void OnPressed()
        {
            pressed = !pressed;
            SetPanel(pressed);
        }

        /// <summary>
        /// Toggles the pop-up panel
        /// </summary>
        /// <param name="show">On or Off</param>
        public void SetPanel(bool show)
        {
            if (panel != null)
            {
                panel.SetActive(show);
            }
            if (show)
            {
                image.color = on;
            }
            else
            {
                image.color = off;
            }
        }
        #endregion
    }
}

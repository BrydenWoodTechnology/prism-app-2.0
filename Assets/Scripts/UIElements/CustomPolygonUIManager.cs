﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using BrydenWoodUnity.DesignData;
using BrydenWoodUnity.GeometryManipulation;
using System;

namespace BrydenWoodUnity.UIElements
{
    public class CustomPolygonUIManager : MonoBehaviour
    {
        public ProceduralBuildingManager buildingManager;
        public GameObject customPolygonPrefab;
        public bool isBasement;
        public bool isPodium;
        public OnCustomPolygonChanged onCustomPolygonChanged;
        public OnBriefChanged onBriefChanged;

        private void Start()
        {

        }
        private void OnEnable()
        {
            //buildingManager.addBasementCustomPolygonInUI += AddCustomBasementPolygon;
            //buildingManager.removeUIbasementCustomPolygon += RemoveCustomPolygon;
        }
        private void OnDisable()
        {
            //buildingManager.addBasementCustomPolygonInUI -= AddCustomBasementPolygon;
            //buildingManager.removeUIbasementCustomPolygon -= RemoveCustomPolygon;
        }
        public void AddCustomBasementPolygon()
        {
            UICustomPolygonSettings newpolygon = Instantiate(customPolygonPrefab, transform).GetComponent<UICustomPolygonSettings>();
            newpolygon.polygonName.text = "Custom Polygon " + transform.childCount;
            newpolygon.onCustomPolygonChanged = new OnCustomPolygonChanged();
            newpolygon.onCustomPolygonChanged.AddListener(OnCustomPolygonChanged);
            newpolygon.GetComponent<CustomPolygonPanel>().onBriedChanged.AddListener(OnCustomPolygonBriefChanged);
            newpolygon.GetComponent<CustomPolygonPanel>().isBasement = isBasement;
            newpolygon.GetComponent<CustomPolygonPanel>().isPodium = isPodium;
            Debug.Log("Polygon Added");
        }

        public void RemoveCustomPolygon(int index)
        {
            Destroy(transform.GetChild(index - 1));
        }

        public void AddCustomPodiumPolygon()
        {
            UICustomPolygonSettings newpolygon = Instantiate(customPolygonPrefab, transform).GetComponent<UICustomPolygonSettings>();
            newpolygon.polygonName.text = "Custom Polygon " + transform.childCount;
            newpolygon.titleMix.text = "Podium Mix (%)";
            newpolygon.onCustomPolygonChanged = new OnCustomPolygonChanged();
            newpolygon.onCustomPolygonChanged.AddListener(OnCustomPolygonChanged);
            newpolygon.GetComponent<CustomPolygonPanel>().onBriedChanged.AddListener(OnCustomPolygonBriefChanged);
            newpolygon.GetComponent<CustomPolygonPanel>().isBasement = isBasement;
            newpolygon.GetComponent<CustomPolygonPanel>().isPodium = isPodium;
            Debug.Log("Polygon Added");
        }

        public void OnCustomPolygonChanged(CustomPolygonEventArgs args)
        {
            if (onCustomPolygonChanged == null) onCustomPolygonChanged = new OnCustomPolygonChanged();
            onCustomPolygonChanged.Invoke(args);
        }

        public void OnCustomPolygonBriefChanged(BriefChangedEventArgs args)
        {
            if (onBriefChanged == null) onBriefChanged = new OnBriefChanged();
            onBriefChanged.Invoke(args);
        }
    }

    [Serializable]
    public class OnCustomPolygonChanged : UnityEvent<CustomPolygonEventArgs> { }

    [Serializable]
    public struct CustomPolygonEventArgs
    {
        public int index;
        public int floors;
        public float floor2Floor;
        public Dictionary<string, float> use;
    }
}

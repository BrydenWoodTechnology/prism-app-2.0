﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component which enables the dragging of UI elements
    /// </summary>
    public class DragPanel : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
    {
        #region Public Fields and Properties
        public bool moveParent = false;
        #endregion

        #region Private FIelds and Properties
        private Vector3 initPos;
        private Vector3 diff;
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void OnDrag(PointerEventData eventData)
        {
            if (moveParent)
            {
                transform.parent.GetComponent<RectTransform>().localPosition = Input.mousePosition + diff;
            }
            else
            {
                GetComponent<RectTransform>().localPosition = Input.mousePosition + diff;
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (moveParent)
            {
                diff = transform.parent.GetComponent<RectTransform>().localPosition - Input.mousePosition;
            }
            else
            {

                diff = GetComponent<RectTransform>().localPosition - Input.mousePosition;
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            diff = Vector3.zero;
        }
        #endregion
    }
}

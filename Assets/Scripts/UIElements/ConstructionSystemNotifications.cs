﻿using BrydenWoodUnity.DesignData;
using BrydenWoodUnity.UIElements;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component which manages the notifications egarding the construction systems
    /// </summary>
    public class ConstructionSystemNotifications : MonoBehaviour
    {
        #region Public Fields and Properties
        public ConstructionSystem system;
        public Text body;
        public Image[] lights;
        public List<int> activeNotifications;
        public List<int> activeWarnings;

        public string[] notificationTitles;
        public string[] notificationsBody;

        public string[] warningBody;

        public Text notificationsText;
        public Text warningsText;
        #endregion

        #region Private Fields and Properties
        private List<string> nots;
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Initializes the instance
        /// </summary>
        public void Initialize()
        {
            //nots = new List<string>();
            //for (int i = 0; i < notificationTitles.Length; i++)
            //{
            //    nots.Add(notificationsBody[i]);
            //}
            //activeNotifications = new List<int>();
            ////UpdateNotifications();
        }

        public void UpdateLights(int[] vals)
        {
            if (vals.Length != lights.Length) return;
            else
            {
                for (int i = 0; i < vals.Length; i++)
                {
                    switch (vals[i])
                    {
                        case 0:
                            lights[i].color = Color.red;
                            if (!activeNotifications.Contains(i)) activeNotifications.Add(i);
                            if (activeWarnings.Contains(i)) activeWarnings.Remove(i);
                            break;
                        case 1:
                            lights[i].color = Color.yellow;
                            if (activeNotifications.Contains(i)) activeNotifications.Remove(i);
                            if (!activeWarnings.Contains(i)) activeWarnings.Add(i);
                            break;
                        case 2:
                            lights[i].color = Color.green;
                            if (activeNotifications.Contains(i)) activeNotifications.Remove(i);
                            if (activeWarnings.Contains(i)) activeWarnings.Remove(i);
                            break;
                    }
                }
            }
        }

        public string[] GetFeasibilityWarnings(int[] vals)
        {
            string[] warnings = new string[vals.Length];

            if (vals.Length != lights.Length) return null;
            else
            {
                for (int i = 0; i < vals.Length; i++)
                {
                    if (vals[i] == 0)
                    {
                        warnings[i] = GetNotification(i);
                    }
                    else
                    {
                        warnings[i] = String.Empty;
                    }
                }
            }

            return warnings;
        }

        public void ShowWarning(int index)
        {
            if (activeWarnings.Contains(index))
            {
                string notification = warningBody[index];
                switch (index)
                {
                    case 0:
                        for (int i = 0; i < system.storiesNumber.greenRange.Length; i++)
                        {
                            notification += (" " + system.storiesNumber.greenRange[i].Split('-')[0] + "-" + system.storiesNumber.greenRange[i].Split('-')[1]);
                            if (i < system.storiesNumber.greenRange.Length - 1)
                            {
                                notification += " and ";
                            }
                        }
                        notification += " levels!";
                        break;
                    case 1:
                        for (int i = 0; i < system.floor2Floor.greenRange.Length; i++)
                        {
                            notification += (" " + system.floor2Floor.greenRange[i].Split('-')[0] + "-" + system.floor2Floor.greenRange[i].Split('-')[1]);
                            if (i < system.floor2Floor.greenRange.Length - 1)
                            {
                                notification += " and ";
                            }
                        }
                        notification += " m!";
                        break;
                    case 2:
                        for (int i = 0; i < system.partyWall.greenRange.Length; i++)
                        {
                            notification += (" " + float.Parse(system.partyWall.greenRange[i].Split('-')[0]) * 1000 + "-" + float.Parse(system.partyWall.greenRange[i].Split('-')[1]) * 1000);
                            if (i < system.partyWall.greenRange.Length - 1)
                            {
                                notification += " and ";
                            }
                        }
                        notification += " mm!";
                        break;
                    case 3:
                        for (int i = 0; i < system.exteriorWall.greenRange.Length; i++)
                        {
                            notification += (" " + float.Parse(system.exteriorWall.greenRange[i].Split('-')[0]) * 1000 + "-" + float.Parse(system.exteriorWall.greenRange[i].Split('-')[1]) * 1000);
                            if (i < system.exteriorWall.greenRange.Length - 1)
                            {
                                notification += " and ";
                            }
                        }
                        notification += " mm!";
                        break;
                    case 4:
                        for (int i = 0; i < system.moduleWidth.greenRange.Length; i++)
                        {
                            notification += (" " + float.Parse(system.moduleWidth.greenRange[i].Split('-')[0]) + "-" + float.Parse(system.moduleWidth.greenRange[i].Split('-')[1]));
                            if (i < system.moduleWidth.RedRange.Length - 1)
                            {
                                notification += " and ";
                            }
                        }
                        notification += " m!";
                        break;
                    case 5:
                        for (int i = 0; i < system.lineLoadSpan.greenRange.Length; i++)
                        {
                            notification += (" " + float.Parse(system.lineLoadSpan.greenRange[i].Split('-')[0]) + "-" + float.Parse(system.lineLoadSpan.greenRange[i].Split('-')[1]));
                            if (i < system.lineLoadSpan.RedRange.Length - 1)
                            {
                                notification += " and ";
                            }
                        }
                        notification += " m!";
                        break;
                }
                warningsText.color = new Color(255 / 255f, 124 / 255f, 0);
                warningsText.text = notification;
            }
        }

        public void ShowNotification(int index)
        {
            if (activeNotifications.Contains(index))
            {
                string notification = notificationsBody[index];
                switch (index)
                {
                    case 0:
                        for (int i = 0; i < system.storiesNumber.RedRange.Length; i++)
                        {
                            CalculateNotification(ref notification, system.storiesNumber.RedRange[i].Split('-')[0], system.storiesNumber.RedRange[i].Split('-')[1], false);
                            //notification += (" " + system.storiesNumber.RedRange[i].Split('-')[0] + "-" + system.storiesNumber.RedRange[i].Split('-')[1]);
                            if (i < system.storiesNumber.RedRange.Length - 1)
                            {
                                notification += " and ";
                            }
                        }
                        notification += " levels!";
                        break;
                    case 1:
                        for (int i = 0; i < system.floor2Floor.RedRange.Length; i++)
                        {
                            CalculateNotification(ref notification, system.floor2Floor.RedRange[i].Split('-')[0], system.floor2Floor.RedRange[i].Split('-')[1], false);
                            //notification += (" " + system.floor2Floor.RedRange[i].Split('-')[0] + "-" + system.floor2Floor.RedRange[i].Split('-')[1]);
                            if (i < system.floor2Floor.RedRange.Length - 1)
                            {
                                notification += " and ";
                            }
                        }
                        notification += " m!";
                        break;
                    case 2:
                        for (int i = 0; i < system.partyWall.RedRange.Length; i++)
                        {
                            CalculateNotification(ref notification, system.partyWall.RedRange[i].Split('-')[0], system.partyWall.RedRange[i].Split('-')[1], true);
                            //notification += (" " + float.Parse(system.partyWall.RedRange[i].Split('-')[0]) * 1000 + "-" + float.Parse(system.partyWall.RedRange[i].Split('-')[1]) * 1000);
                            if (i < system.partyWall.RedRange.Length - 1)
                            {
                                notification += " and ";
                            }
                        }
                        notification += " mm!";
                        break;
                    case 3:
                        for (int i = 0; i < system.exteriorWall.RedRange.Length; i++)
                        {
                            CalculateNotification(ref notification, system.exteriorWall.RedRange[i].Split('-')[0], system.exteriorWall.RedRange[i].Split('-')[1], true);
                            //notification += (" " + float.Parse(system.exteriorWall.RedRange[i].Split('-')[0]) * 1000 + "-" + float.Parse(system.exteriorWall.RedRange[i].Split('-')[1]) * 1000);
                            if (i < system.exteriorWall.RedRange.Length - 1)
                            {
                                notification += " and ";
                            }
                        }
                        notification += " mm!";
                        break;
                    case 4:
                        for (int i = 0; i < system.moduleWidth.RedRange.Length; i++)
                        {
                            CalculateNotification(ref notification, system.moduleWidth.RedRange[i].Split('-')[0], system.moduleWidth.RedRange[i].Split('-')[1], false);
                            //notification += (" " + float.Parse(system.moduleWidth.RedRange[i].Split('-')[0]) + "-" + float.Parse(system.moduleWidth.RedRange[i].Split('-')[1]));
                            if (i < system.moduleWidth.RedRange.Length - 1)
                            {
                                notification += " and ";
                            }
                        }
                        notification += " m!";
                        break;
                    case 5:
                        for (int i = 0; i < system.lineLoadSpan.RedRange.Length; i++)
                        {
                            CalculateNotification(ref notification, system.lineLoadSpan.RedRange[i].Split('-')[0], system.lineLoadSpan.RedRange[i].Split('-')[1], false);
                            //notification += (" " + float.Parse(system.lineLoadSpan.RedRange[i].Split('-')[0]) + "-" + float.Parse(system.lineLoadSpan.RedRange[i].Split('-')[1]));
                            if (i < system.lineLoadSpan.RedRange.Length - 1)
                            {
                                notification += " and ";
                            }
                        }
                        notification += " m!";
                        break;
                }
                //notificationsText.color = Color.red;
                notificationsText.color = new Color(0.93f, 0.15f, 0.4f, 1f);
                notificationsText.text = notification;
            }
        }

        private void CalculateNotification(ref string body, string val0, string val1, bool milim)
        {
            var v1 = float.Parse(val0) * (milim ? 1000 : 1);
            var v2 = float.Parse(val1) * (milim ? 1000 : 1);
            if (v1==0)
            {
                body += " < " + v2.ToString();
            }
            else if (v2 == 100)
            {
                body += " > " + v1.ToString();
            }
        }

        public string GetNotification(int index)
        {
            string notification = notificationsBody[index];
            switch (index)
            {
                case 0:
                    for (int i = 0; i < system.storiesNumber.RedRange.Length; i++)
                    {
                        notification += (" " + system.storiesNumber.RedRange[i].Split('-')[0] + "-" + system.storiesNumber.RedRange[i].Split('-')[1]);
                        if (i < system.storiesNumber.RedRange.Length - 1)
                        {
                            notification += " and ";
                        }
                    }
                    notification += " stories";
                    break;
                case 1:
                    for (int i = 0; i < system.floor2Floor.RedRange.Length; i++)
                    {
                        notification += (" " + system.floor2Floor.RedRange[i].Split('-')[0] + "-" + system.floor2Floor.RedRange[i].Split('-')[1]);
                        if (i < system.floor2Floor.RedRange.Length - 1)
                        {
                            notification += " and ";
                        }
                    }
                    notification += " m";
                    break;
                case 2:
                    for (int i = 0; i < system.partyWall.RedRange.Length; i++)
                    {
                        notification += (" " + float.Parse(system.partyWall.RedRange[i].Split('-')[0]) * 1000 + "-" + float.Parse(system.partyWall.RedRange[i].Split('-')[1]) * 1000);
                        if (i < system.partyWall.RedRange.Length - 1)
                        {
                            notification += " and ";
                        }
                    }
                    notification += " mm";
                    break;
                case 3:
                    for (int i = 0; i < system.exteriorWall.RedRange.Length; i++)
                    {
                        notification += (" " + float.Parse(system.exteriorWall.RedRange[i].Split('-')[0]) * 1000 + "-" + float.Parse(system.exteriorWall.RedRange[i].Split('-')[1]) * 1000);
                        if (i < system.exteriorWall.RedRange.Length - 1)
                        {
                            notification += " and ";
                        }
                    }
                    notification += " mm";
                    break;
                case 4:
                    for (int i = 0; i < system.moduleWidth.RedRange.Length; i++)
                    {
                        notification += (" " + float.Parse(system.moduleWidth.RedRange[i].Split('-')[0]) + "-" + float.Parse(system.moduleWidth.RedRange[i].Split('-')[1]));
                        if (i < system.moduleWidth.RedRange.Length - 1)
                        {
                            notification += " and ";
                        }
                    }
                    notification += " m";
                    break;
                case 5:
                    for (int i = 0; i < system.lineLoadSpan.RedRange.Length; i++)
                    {
                        notification += (" " + float.Parse(system.lineLoadSpan.RedRange[i].Split('-')[0]) + "-" + float.Parse(system.lineLoadSpan.RedRange[i].Split('-')[1]));
                        if (i < system.lineLoadSpan.RedRange.Length - 1)
                        {
                            notification += " and ";
                        }
                    }
                    notification += " m";
                    break;
            }
            return notification;
        }

        public void ClearNotification()
        {
            notificationsText.text = String.Empty;
        }

        /// <summary>
        /// Adds notifications to the list
        /// </summary>
        /// <param name="title">The list of titles for the notifications to be added</param>
        public void AddNotification(int index)
        {
            //for (int i = 0; i < title.Length; i++)
            //{
            //    if (!activeNotifications.Contains(title[i]))
            //    {
            //        activeNotifications.Add(title[i]);
            //    }
            //}
            //if (!activeNotifications.Contains(index)) activeNotifications.Add(index);
            //UpdateNotifications();
        }

        /// <summary>
        /// Removes notfications from the list
        /// </summary>
        /// <param name="title">The list of titles for the notifications to be removed</param>
        public void RemoveNotification(string[] title)
        {
            //for (int i = 0; i < title.Length; i++)
            //{
            //    if (activeNotifications.Contains(title[i]))
            //    {
            //        activeNotifications.Remove(title[i]);
            //    }
            //}
            //if (!activeNotifications.Contains(index)) activeNotifications.Add(index);
            //UpdateNotifications();
        }

        /// <summary>
        /// Updates the displayed notifications
        /// </summary>
        //public void UpdateNotifications()
        //{
        //    //string bodyText = "";
        //    ////ReportData.TaggedObject.constructionSystems[system.name].warnings = new string[activeNotifications.Count];
        //    //for (int i = 0; i < activeNotifications.Count; i++)
        //    //{
        //    //    if (nots.ContainsKey(activeNotifications[i]))
        //    //    {
        //    //        string notification = nots[activeNotifications[i]];
        //    //        switch (activeNotifications[i])
        //    //        {
        //    //            case "height":
        //    //                notification += (" " + system.storiesNumber.RedRange.Split('-')[0] + " and " + system.storiesNumber.RedRange.Split('-')[1] + "stories");
        //    //                break;
        //    //            case "partywall":
        //    //                notification += (" " + float.Parse(system.partyWall.RedRange.Split('-')[0]) * 1000 + " and "+ float.Parse(system.partyWall.RedRange.Split('-')[1]) * 1000 + "mm");
        //    //                break;
        //    //            case "f2f":
        //    //                notification += (" " + system.floor2Floor.RedRange.Split('-')[0] + " and "+ system.floor2Floor.RedRange.Split('-')[1] + " m");
        //    //                break;
        //    //            case "exteriorwall":
        //    //                notification += (" " + float.Parse(system.partyWall.RedRange.Split('-')[0]) * 1000 + " and "+ float.Parse(system.partyWall.RedRange.Split('-')[1]) * 1000 + " mm");
        //    //                break;
        //    //            case "module":
        //    //                notification += (" " + float.Parse(system.moduleWidth.RedRange.Split('-')[0]) + " and " + float.Parse(system.moduleWidth.RedRange.Split('-')[1]) + " m");
        //    //                break;
        //    //        }

        //    //        //ReportData.TaggedObject.constructionSystems[system.name].warnings[i] = notification;
        //    //        bodyText += string.Format("- {0}\r\n\r\n", notification);
        //    //    }
        //    //}
        //    //body.text = bodyText;
        //}

        //public string GetNotification(string title)
        //{
        //    //string notification = nots[title];
        //    //switch (title)
        //    //{
        //    //    case "height":
        //    //        notification += (" " + system.storiesNumber.RedRange.Split('-')[0] + " and " + system.storiesNumber.RedRange.Split('-')[1] + "stories");
        //    //        break;
        //    //    case "partywall":
        //    //        notification += (" " + float.Parse(system.partyWall.RedRange.Split('-')[0]) * 1000 + " and " + float.Parse(system.partyWall.RedRange.Split('-')[1]) * 1000 + "mm");
        //    //        break;
        //    //    case "f2f":
        //    //        notification += (" " + system.floor2Floor.RedRange.Split('-')[0] + " and " + system.floor2Floor.RedRange.Split('-')[1] + " m");
        //    //        break;
        //    //    case "exteriorwall":
        //    //        notification += (" " + float.Parse(system.partyWall.RedRange.Split('-')[0]) * 1000 + " and " + float.Parse(system.partyWall.RedRange.Split('-')[1]) * 1000 + " mm");
        //    //        break;
        //    //    case "module":
        //    //        notification += (" " + float.Parse(system.moduleWidth.RedRange.Split('-')[0]) + " and " + float.Parse(system.moduleWidth.RedRange.Split('-')[1]) + " m");
        //    //        break;
        //    //}
        //    //return notification;
        //}
        #endregion
    }
}

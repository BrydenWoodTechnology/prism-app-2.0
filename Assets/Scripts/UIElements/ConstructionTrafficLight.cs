﻿using BrydenWoodUnity.DesignData;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    public enum SystemCategory
    {
        Modular = 0,
        Panelized = 1,
        Platforms = 2
    }
    /// <summary>
    /// A MonoBehaviour component for evaluating the manufacturing system and displaying the results
    /// </summary>
    public class ConstructionTrafficLight : MonoBehaviour
    {
        #region Public Fields and Properties
        public SystemCategory systemCategory = SystemCategory.Modular;
        public Text notificationsText;
        public List<ConstructionSystem> constructionSystems;
        public List<Transform> lights;
        #endregion

        #region Private Fields and Properties
        private string[] feasibility;
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            feasibility = new string[] { "Not Achievable", "Achievable", "Ideal" };
            lights = new List<Transform>();
            if (constructionSystems.Count > 0)
            {
                //if (ReportData.TaggedObject.constructionSystems == null)
                //{
                //    ReportData.TaggedObject.constructionSystems = new Dictionary<string, ConstructionSystemData>();
                //}

                lights.Add(transform.GetChild(0));
                lights[0].GetChild(0).GetComponent<Text>().text = constructionSystems[0].name.Replace('_', ' ');
                lights[0].GetComponent<ConstructionSystemNotifications>().system = constructionSystems[0];
                lights[0].GetComponent<ConstructionSystemNotifications>().notificationsText = notificationsText;
                lights[0].GetComponent<ConstructionSystemNotifications>().warningsText = notificationsText;
                lights[0].GetComponent<ConstructionSystemNotifications>().Initialize();

                for (int i = 1; i < constructionSystems.Count; i++)
                {
                    var obj = Instantiate(lights[0], transform);
                    lights.Add(obj.transform);
                    obj.GetChild(0).GetComponent<Text>().text = constructionSystems[i].name.Replace('_', ' ');
                    lights[i].GetComponent<ConstructionSystemNotifications>().system = constructionSystems[i];
                    lights[i].GetComponent<ConstructionSystemNotifications>().notificationsText = notificationsText;
                    lights[i].GetComponent<ConstructionSystemNotifications>().warningsText = notificationsText;
                    lights[i].GetComponent<ConstructionSystemNotifications>().Initialize();
                }
            }
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Updates the evaluation of the manufacturing systems
        /// </summary>
        /// <param name="levelNumber">Number of levels for the building</param>
        /// <param name="minFloor2Floor">Floor to floor height</param>
        public void UpdateTrafficSystem(int levelNumber, double minFloor2Floor, double maxFloor2Floor, float maxItemWidth, float maxLineLoadSpan)
        {
            for (int i = 0; i < constructionSystems.Count; i++)
            {
                int storiesEval = constructionSystems[i].EvaluateStories(levelNumber);
                int f2fEval = constructionSystems[i].EvaluateFloorToFloor(minFloor2Floor, maxFloor2Floor);
                int partyWallEval = constructionSystems[i].EvaluatePartyWall(Standards.TaggedObject.ConstructionFeatures["PartyWall"]);
                int exteriorWallEval = constructionSystems[i].EvaluateExteriorWall(Standards.TaggedObject.ConstructionFeatures["ExteriorWall"]);
                int moduleWidthEval = constructionSystems[i].EvaluateModuleWidth(maxItemWidth);
                int lineLoadSpanEval = constructionSystems[i].EvaluateLineLoadSpan(maxLineLoadSpan);

                List<string> titles = new List<string>();
                List<string> removeTitles = new List<string>();
                int[] results = new int[6]
                {
                    storiesEval-1,
                    f2fEval-1,
                    partyWallEval-1,
                    exteriorWallEval-1,
                    moduleWidthEval-1,
                    lineLoadSpanEval-1
                };

                lights[i].GetComponent<ConstructionSystemNotifications>().UpdateLights(results);
            }
        }

        public void GetBuildingEvaluation(int levelNumber, double minFloor2Floor, double maxFloor2Floor, float maxItemWidth, float maxLineLoadSpan, ref Dictionary<string, ConstructionSystemData> perSystemEval)
        {
            for (int i = 0; i < constructionSystems.Count; i++)
            {
                int storiesEval = constructionSystems[i].EvaluateStories(levelNumber);
                int f2fEval = constructionSystems[i].EvaluateFloorToFloor(minFloor2Floor, maxFloor2Floor);
                int partyWallEval = constructionSystems[i].EvaluatePartyWall(Standards.TaggedObject.ConstructionFeatures["PartyWall"]);
                int exteriorWallEval = constructionSystems[i].EvaluateExteriorWall(Standards.TaggedObject.ConstructionFeatures["ExteriorWall"]);
                int moduleWidthEval = constructionSystems[i].EvaluateModuleWidth(maxItemWidth);
                int lineLoadSpanEval = constructionSystems[i].EvaluateLineLoadSpan(maxLineLoadSpan);

                List<string> titles = new List<string>();
                List<string> removeTitles = new List<string>();
                int[] results = new int[6]
                {
                    storiesEval-1,
                    f2fEval-1,
                    partyWallEval-1,
                    exteriorWallEval-1,
                    moduleWidthEval-1,
                    lineLoadSpanEval-1
                };

                perSystemEval.Add(constructionSystems[i].name, new ConstructionSystemData()
                {
                    feasibility = results,
                    warnings = lights[i].GetComponent<ConstructionSystemNotifications>().GetFeasibilityWarnings(results)
                });
            }
        }
        #endregion
    }


    /// <summary>
    /// A class to hold data and constraints about each distinct manufacturing system
    /// </summary>
    [System.Serializable]
    public class ConstructionSystem
    {
        public string name;
        public Stories storiesNumber;
        public Floor2Floor floor2Floor;
        public PartyWall partyWall;
        public ExteriorWall exteriorWall;
        public ModuleWidth moduleWidth;
        public LineLoadSpan lineLoadSpan;

        public int EvaluateStories(int val)
        {

            bool red = false;
            bool green = false;
            bool yellow = false;

            for (int i = 0; i < storiesNumber.RedRange.Length; i++)
            {
                float redLower = float.Parse(storiesNumber.RedRange[i].Split('-')[0]);
                float redUpper = float.Parse(storiesNumber.RedRange[i].Split('-')[1]);
                if (val >= redLower && val <= redUpper)
                {
                    red = true;
                }
            }

            for (int i = 0; i < storiesNumber.greenRange.Length; i++)
            {
                float greenLower = float.Parse(storiesNumber.greenRange[i].Split('-')[0]);
                float greenUpper = float.Parse(storiesNumber.greenRange[i].Split('-')[1]);
                if (val >= greenLower && val <= greenUpper)
                {
                    green = true;
                }
            }

            for (int i = 0; i < storiesNumber.yellowRange.Length; i++)
            {
                float yellowLower = float.Parse(storiesNumber.yellowRange[i].Split('-')[0]);
                float yellowUpper = float.Parse(storiesNumber.yellowRange[i].Split('-')[1]);
                if (val >= yellowLower && val <= yellowUpper)
                {
                    yellow = true;
                }
            }

            if (red)
            {
                return 1;
            }
            else if (green)
            {
                return 3;
            }
            else if (yellow)
            {
                return 2;
            }
            else return 3;
        }

        public int EvaluateFloorToFloor(double minVal, double maxVal)
        {
            bool red = false;
            bool green = false;
            bool yellow = false;

            for (int i = 0; i < floor2Floor.RedRange.Length; i++)
            {
                float redLower = float.Parse(floor2Floor.RedRange[i].Split('-')[0]);
                float redUpper = float.Parse(floor2Floor.RedRange[i].Split('-')[1]);
                if ((minVal >= redLower && minVal <= redUpper) || (maxVal >= redLower && maxVal <= redUpper))
                {
                    red = true;
                }
            }

            for (int i = 0; i < floor2Floor.greenRange.Length; i++)
            {
                float greenLower = float.Parse(floor2Floor.greenRange[i].Split('-')[0]);
                float greenUpper = float.Parse(floor2Floor.greenRange[i].Split('-')[1]);
                if (minVal >= greenLower && minVal <= greenUpper && maxVal >= greenLower && maxVal <= greenUpper)
                {
                    green = true;
                }
            }

            for (int i = 0; i < floor2Floor.yellowRange.Length; i++)
            {
                float yellowLower = float.Parse(floor2Floor.yellowRange[i].Split('-')[0]);
                float yellowUpper = float.Parse(floor2Floor.yellowRange[i].Split('-')[1]);
                if ((minVal >= yellowLower && minVal <= yellowUpper) || (maxVal >= yellowLower && maxVal <= yellowUpper))
                {
                    yellow = true;
                }
            }

            if (red)
            {
                return 1;
            }
            else if (green)
            {
                return 3;
            }
            else if (yellow)
            {
                return 2;
            }
            else return 3;
        }

        public int EvaluatePartyWall(double val)
        {
            bool red = false;
            bool green = false;
            bool yellow = false;

            for (int i = 0; i < partyWall.RedRange.Length; i++)
            {
                float redLower = float.Parse(partyWall.RedRange[i].Split('-')[0]);
                float redUpper = float.Parse(partyWall.RedRange[i].Split('-')[1]);
                if (val >= redLower && val <= redUpper)
                {
                    red = true;
                }
            }

            for (int i = 0; i < partyWall.greenRange.Length; i++)
            {
                float greenLower = float.Parse(partyWall.greenRange[i].Split('-')[0]);
                float greenUpper = float.Parse(partyWall.greenRange[i].Split('-')[1]);
                if (val >= greenLower && val <= greenUpper)
                {
                    green = true;
                }
            }

            for (int i = 0; i < partyWall.yellowRange.Length; i++)
            {
                float yellowLower = float.Parse(partyWall.yellowRange[i].Split('-')[0]);
                float yellowUpper = float.Parse(partyWall.yellowRange[i].Split('-')[1]);
                if (val >= yellowLower && val <= yellowUpper)
                {
                    yellow = true;
                }
            }

            if (red)
            {
                return 1;
            }
            else if (green)
            {
                return 3;
            }
            else if (yellow)
            {
                return 2;
            }
            else return 3;
        }

        public int EvaluateExteriorWall(double val)
        {
            bool red = false;
            bool green = false;
            bool yellow = false;

            for (int i = 0; i < exteriorWall.RedRange.Length; i++)
            {
                float redLower = float.Parse(exteriorWall.RedRange[i].Split('-')[0]);
                float redUpper = float.Parse(exteriorWall.RedRange[i].Split('-')[1]);
                if (val >= redLower && val <= redUpper)
                {
                    red = true;
                }
            }

            for (int i = 0; i < exteriorWall.greenRange.Length; i++)
            {
                float greenLower = float.Parse(exteriorWall.greenRange[i].Split('-')[0]);
                float greenUpper = float.Parse(exteriorWall.greenRange[i].Split('-')[1]);
                if (val >= greenLower && val <= greenUpper)
                {
                    green = true;
                }
            }

            for (int i = 0; i < exteriorWall.yellowRange.Length; i++)
            {
                float yellowLower = float.Parse(exteriorWall.yellowRange[i].Split('-')[0]);
                float yellowUpper = float.Parse(exteriorWall.yellowRange[i].Split('-')[1]);
                if (val >= yellowLower && val <= yellowUpper)
                {
                    yellow = true;
                }
            }

            if (red)
            {
                return 1;
            }
            else if (green)
            {
                return 3;
            }
            else if (yellow)
            {
                return 2;
            }
            else return 3;
        }

        public int EvaluateModuleWidth(double val)
        {
            bool red = false;
            bool green = false;
            bool yellow = false;

            for (int i = 0; i < moduleWidth.RedRange.Length; i++)
            {
                float redLower = float.Parse(moduleWidth.RedRange[i].Split('-')[0]);
                float redUpper = float.Parse(moduleWidth.RedRange[i].Split('-')[1]);
                if (val >= redLower && val <= redUpper)
                {
                    red = true;
                }
            }

            for (int i = 0; i < moduleWidth.greenRange.Length; i++)
            {
                float greenLower = float.Parse(moduleWidth.greenRange[i].Split('-')[0]);
                float greenUpper = float.Parse(moduleWidth.greenRange[i].Split('-')[1]);
                if (val >= greenLower && val <= greenUpper)
                {
                    green = true;
                }
            }

            for (int i = 0; i < moduleWidth.yellowRange.Length; i++)
            {
                float yellowLower = float.Parse(moduleWidth.yellowRange[i].Split('-')[0]);
                float yellowUpper = float.Parse(moduleWidth.yellowRange[i].Split('-')[1]);
                if (val >= yellowLower && val <= yellowUpper)
                {
                    yellow = true;
                }
            }

            if (red)
            {
                return 1;
            }
            else if (green)
            {
                return 3;
            }
            else if (yellow)
            {
                return 2;
            }
            else return 3;
        }

        public int EvaluateLineLoadSpan(float val)
        {

            bool red = false;
            bool green = false;
            bool yellow = false;

            for (int i = 0; i < lineLoadSpan.RedRange.Length; i++)
            {
                float redLower = float.Parse(lineLoadSpan.RedRange[i].Split('-')[0]);
                float redUpper = float.Parse(lineLoadSpan.RedRange[i].Split('-')[1]);
                if (val >= redLower && val <= redUpper)
                {
                    red = true;
                }
            }

            for (int i = 0; i < lineLoadSpan.greenRange.Length; i++)
            {
                float greenLower = float.Parse(lineLoadSpan.greenRange[i].Split('-')[0]);
                float greenUpper = float.Parse(lineLoadSpan.greenRange[i].Split('-')[1]);
                if (val >= greenLower && val <= greenUpper)
                {
                    green = true;
                }
            }

            for (int i = 0; i < lineLoadSpan.yellowRange.Length; i++)
            {
                float yellowLower = float.Parse(lineLoadSpan.yellowRange[i].Split('-')[0]);
                float yellowUpper = float.Parse(lineLoadSpan.yellowRange[i].Split('-')[1]);
                if (val >= yellowLower && val <= yellowUpper)
                {
                    yellow = true;
                }
            }

            if (red)
            {
                return 1;
            }
            else if (green)
            {
                return 3;
            }
            else if (yellow)
            {
                return 2;
            }
            else return 3;
        }
    }

    /// <summary>
    /// Number of stories constraint
    /// </summary>
    [System.Serializable]
    public class Stories
    {
        public string[] greenRange;
        public string[] yellowRange;
        public string[] RedRange;
    }

    /// <summary>
    /// Floor to floor height constraint
    /// </summary>
    [System.Serializable]
    public class Floor2Floor
    {
        public string[] greenRange;
        public string[] yellowRange;
        public string[] RedRange;
    }

    /// <summary>
    /// Party wall thickness constraint
    /// </summary>
    [System.Serializable]
    public class PartyWall
    {
        public string[] greenRange;
        public string[] yellowRange;
        public string[] RedRange;
    }

    /// <summary>
    /// Exterior wall thickness constraint
    /// </summary>
    [System.Serializable]
    public class ExteriorWall
    {
        public string[] greenRange;
        public string[] yellowRange;
        public string[] RedRange;
    }

    [System.Serializable]
    public class ModuleWidth
    {
        public string[] greenRange;
        public string[] yellowRange;
        public string[] RedRange;
    }

    [System.Serializable]
    public class LineLoadSpan
    {
        public string[] greenRange;
        public string[] yellowRange;
        public string[] RedRange;
    }
}

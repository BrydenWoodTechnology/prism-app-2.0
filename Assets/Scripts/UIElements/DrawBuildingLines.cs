﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for drawing GL-Lines for the buildings
    /// </summary>
    public class DrawBuildingLines : MonoBehaviour
    {
        #region Public Fields and Properties
        [Header("Assets References:")]
        public Material buildingLineMaterial;
        public Color selectedColor = Color.green;
        private Material buildingLineMaterialSelected;

        public Dictionary<string, List<Line>> buildingLinesToDraw;
        public Dictionary<string, List<Line>> plantRoomLines;
        public List<string> selectedKey { get; set; }
        public Material plantRoomMaterial { get; set; }
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            buildingLinesToDraw = new Dictionary<string, List<Line>>();
            plantRoomLines = new Dictionary<string, List<Line>>();
            selectedKey = new List<string>();
            buildingLineMaterialSelected = new Material(buildingLineMaterial);
            plantRoomMaterial = new Material(buildingLineMaterial);
            plantRoomMaterial.color = Color.black;
            buildingLineMaterialSelected.color = selectedColor;
        }

        // Update is called once per frame
        void Update()
        {

        }

        // To show the lines in the game window whne it is running
        void OnPostRender()
        {
            DrawConnectingLines();
        }


        // To show the lines in the editor
        void OnDrawGizmos()
        {
            DrawConnectingLines();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Adds building lines to the list
        /// </summary>
        /// <param name="lines">The lines to be added</param>
        public void AddBuildingLines(KeyValuePair<string, List<Line>> lines)
        {
            if (buildingLinesToDraw.ContainsKey(lines.Key))
            {
                buildingLinesToDraw[lines.Key] = lines.Value;
            }
            else
            {
                buildingLinesToDraw.Add(lines.Key, lines.Value);
            }
        }

        /// <summary>
        /// Removes building lines from the list
        /// </summary>
        /// <param name="key">The key of the lines to be removed</param>
        public void RemoveBuildingLines(string key)
        {
            if (buildingLinesToDraw.ContainsKey(key))
            {
                buildingLinesToDraw.Remove(key);
            }
        }

        /// <summary>
        /// Removes plant-room lines from the list
        /// </summary>
        /// <param name="key">The key of the lines to be removed</param>
        public void RemovePlantRoomLines(string key)
        {
            if (plantRoomLines.ContainsKey(key))
            {
                plantRoomLines.Remove(key);
            }
        }

        /// <summary>
        /// Adds plant-room lines to the list
        /// </summary>
        /// <param name="lines">The lines to be added</param>
        public void AddPlantRoomLines(KeyValuePair<string, List<Line>> lines)
        {
            if (plantRoomLines.ContainsKey(lines.Key))
            {
                plantRoomLines[lines.Key] = lines.Value;
            }
            else
            {
                plantRoomLines.Add(lines.Key, lines.Value);
            }
        }
        #endregion

        #region Private Methods
        void DrawConnectingLines()
        {
            if (buildingLinesToDraw != null)
            {
                foreach (var item in buildingLinesToDraw)
                {

                    if (selectedKey.Contains(item.Key))
                    {
                        for (int i = 0; i < item.Value.Count; i++)
                        {
                            DrawLineSelected(item.Value[i]);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < item.Value.Count; i++)
                        {
                            DrawLine(item.Value[i]);
                        }
                    }
                }
            }

            if (plantRoomLines != null)
            {
                foreach (var item in plantRoomLines)
                {
                    for (int i = 0; i < item.Value.Count; i++)
                    {
                        DrawPlantLine(item.Value[i]);
                    }
                }
            }
        }

        private void DrawLine(Line line)
        {
            buildingLineMaterial.color = line.color;
            GL.Begin(GL.LINES);
            buildingLineMaterial.SetPass(0);
            GL.Color(new Color(buildingLineMaterial.color.r, buildingLineMaterial.color.g, buildingLineMaterial.color.b, buildingLineMaterial.color.a));
            GL.Vertex3(line.start.x, line.start.y, line.start.z);
            GL.Vertex3(line.end.x, line.end.y, line.end.z);
            GL.End();
        }

        private void DrawPlantLine(Line line)
        {
            plantRoomMaterial.color = line.color;
            GL.Begin(GL.LINES);
            plantRoomMaterial.SetPass(0);
            GL.Color(new Color(plantRoomMaterial.color.r, plantRoomMaterial.color.g, plantRoomMaterial.color.b, plantRoomMaterial.color.a));
            GL.Vertex3(line.start.x, line.start.y, line.start.z);
            GL.Vertex3(line.end.x, line.end.y, line.end.z);
            GL.End();
        }

        private void DrawLineSelected(Line line)
        {
            buildingLineMaterialSelected.color = selectedColor;
            GL.Begin(GL.LINES);
            buildingLineMaterial.SetPass(0);
            GL.Color(new Color(buildingLineMaterialSelected.color.r, buildingLineMaterialSelected.color.g, buildingLineMaterialSelected.color.b, buildingLineMaterialSelected.color.a));
            GL.Vertex3(line.start.x, line.start.y, line.start.z);
            GL.Vertex3(line.end.x, line.end.y, line.end.z);
            GL.End();
        }
        #endregion
    }

    /// <summary>
    /// A class to facilitate the drawing of GL lines
    /// </summary>
    public class Line
    {
        public Vector3 start;
        public Vector3 end;
        public Color color;
        public Vector3 Direction
        {
            get
            {
                return (end - start).normalized;
            }
        }
        public float Length
        {
            get
            {
                return Vector3.Distance(start, end);
            }
        }
        public Line()
        {

        }

        public Line(Vector3 start, Vector3 end)
        {
            this.start = start;
            this.end = end;
        }

        public Line(Vector3 start, Vector3 end, Color color)
        {
            this.start = start;
            this.end = end;
            this.color = color;
        }

        public Line(Vector3 start, Vector3 end, Color color, Vector3 normal, float offseDist)
        {
            Vector3 cross = Vector3.Cross(end - start, normal).normalized * offseDist;
            this.start = start + cross;
            this.end = end + cross;
            this.color = color;
        }

        public Line Reverse()
        {
            var e = end;
            var s = start;
            start = e;
            end = s;
            return this;
        }

        //As seen here: https://rosettacode.org/wiki/Find_the_intersection_of_two_lines#C.23
        public static bool Intersection(Line line1, Line line2, out Vector3 point)
        {
            line2.start = new Vector3(line2.start.x, line1.start.y, line2.start.z);
            line2.end = new Vector3(line2.end.x, line1.end.y, line2.end.z);

            float a1 = line1.end.z - line1.start.z;
            float b1 = line1.start.x - line1.end.x;
            float c1 = a1 * line1.start.x + b1 * line1.start.y;

            float a2 = line2.end.z - line2.start.z;
            float b2 = line2.start.x - line2.end.x;
            float c2 = a2 * line2.start.x + b2 * line2.start.y;

            float delta = a1 * b2 - a2 * b1;
            point = Vector3.zero;
            if (delta == 0) return false;
            else
            {
                point = new Vector3((b2*c1-b1*c2)/delta,line1.start.y,(a1*c2 - a2*c1)/delta);
                return true;
            }
        }
    }
}

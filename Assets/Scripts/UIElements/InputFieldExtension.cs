﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// Extends the Input Field class from Unity
    /// </summary>
    public class InputFieldExtension : MonoBehaviour, IPointerClickHandler, IDeselectHandler
    {
        #region Events
        /// <summary>
        /// Used when the input field is selected
        /// </summary>
        /// <param name="inputfield">The selected input field</param>
        public delegate void OnSelected(InputField inputfield);
        /// <summary>
        /// Triggered when the input field is selected
        /// </summary>
        public event OnSelected selected;
        #endregion

        #region MonoBehaviour Methods
        private void OnDestroy()
        {
            if (selected != null)
            {
                foreach (Delegate eh in selected.GetInvocationList())
                {
                    selected -= (OnSelected)eh;
                }
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (selected != null)
            {
                selected(GetComponent<InputField>());
            }
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void OnDeselect(BaseEventData eventData)
        {
            GetComponent<InputField>().onEndEdit.Invoke(GetComponent<InputField>().text);
        }
        #endregion
    }
}

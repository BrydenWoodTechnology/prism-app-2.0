﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BrydenWoodUnity.DesignData;
using System;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for controlling the information displayed about the selected item
    /// </summary>
    public class SelectedInfoDisplay : MonoBehaviour
    {
        #region Public Fields and Properties
        [Header("Scene References:")]
        public Text title;
        public Text info;
        public Text errorNotification;
        public VariationDisplay aptVariationDisplay;
        public ProceduralBuildingManager buildingManager;
        #endregion

        #region Private Fields and Properties
        private BaseDesignData selectedGeometry;
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            BaseDesignData.selected += OnSelect;
            Selector.deselect += OnDeselect;
            //OnDeselect();
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnDestroy()
        {
            BaseDesignData.selected -= OnSelect;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Called when a Design Data object is selected
        /// </summary>
        /// <param name="designData">The selected Design Data object</param>
        public void OnSelect(BaseDesignData designData)
        {
            var apt = designData as ApartmentUnity;
            if (apt != null)
            {
                title.gameObject.SetActive(true);
                info.gameObject.SetActive(true);
                errorNotification.gameObject.SetActive(true);
                aptVariationDisplay.gameObject.SetActive(true);
                selectedGeometry = designData;
                selectedGeometry.geometryUpdated.AddListener(OnSelectedUpdate);
                var infos = designData.GetInfoText();
                title.text = infos[0];
                info.text = infos[1];
                errorNotification.text = apt.GetNotification();
                aptVariationDisplay.UpdateVariationDisplay(apt.ApartmentType, new List<ApartmentUnity>() { apt });
            }
        }

        public void SetSelected(ProceduralApartmentLayout aptLayout)
        {
            title.gameObject.SetActive(true);
            info.gameObject.SetActive(true);
            errorNotification.gameObject.SetActive(true);
            aptVariationDisplay.gameObject.SetActive(true);
            var infos = aptLayout.GetInfoText();
            title.text = infos[0];
            info.text = infos[1];
            errorNotification.text = String.Empty;
            aptVariationDisplay.UpdateVariationDisplay(aptLayout.apartmentType, aptLayout);
        }

        /// <summary>
        /// Called when the Design Data object is deselected
        /// </summary>
        public void OnDeselect()
        {
            if (selectedGeometry != null)
            {
                selectedGeometry.geometryUpdated.RemoveListener(OnSelectedUpdate);
            }
            selectedGeometry = null;
            title.text = "";
            info.text = "";
            title.gameObject.SetActive(false);
            info.gameObject.SetActive(false);
            errorNotification.gameObject.SetActive(false);
            aptVariationDisplay.gameObject.SetActive(false);
        }

        /// <summary>
        /// Called when the selected Design Data object is updated
        /// </summary>
        /// <param name="sender"></param>
        public void OnSelectedUpdate(BaseDesignData sender)
        {
            var infos = sender.GetInfoText();
            title.text = infos[0];
            info.text = infos[1];
            var apt = sender as ApartmentUnity;
            if (apt != null)
            {
                errorNotification.text = apt.GetNotification();
                aptVariationDisplay.UpdateVariationDisplay(apt.ApartmentType, new List<ApartmentUnity>() { apt });
            }
        }
        #endregion


    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BrydenWoodUnity.UIElements
{
    public class BuildingBriefPanel : BriefPanel
    {
        public BuildingPanel buildingPanel;
        public bool isBasement;
        public bool isPodium;

        public override void AddBriefElement()
        {
            bool containsOtherThanCommercial = false;
            foreach (Transform item in briefElementHolder)
            {
                var briefElement = item.GetComponent<BriefElement>();
                if (briefElement != null)
                {
                    if (briefElement.type == "Commercial")
                    {
                        containsOtherThanCommercial = true;
                    }
                }
            }
            if (briefElementHolder.childCount != 1)
            {
                containsOtherThanCommercial = true;
            }
            GameObject obj = Instantiate(briefElementPrefab, briefElementHolder);
            obj.name = "BriefElement_" + (briefElementHolder.childCount - 1);
            obj.GetComponent<BriefElement>().Initialize(isBasement, isPodium);
            obj.GetComponent<BriefElement>().onChanged.AddListener(OnBriefElementChanged);
            obj.GetComponent<BriefElement>().onDeleted.AddListener(OnBriefElementDeleted);
            obj.transform.SetSiblingIndex(briefElementHolder.childCount - 2);
            //if (containsOtherThanCommercial)
            //{
            //    obj.GetComponent<BriefElement>().RemoveCommercialFromList();
            //}

            buildingPanel.OnBriefChanged(this);
        }

        public override void AddBriefElement(string type, float percentage)
        {
            GameObject obj = Instantiate(briefElementPrefab, briefElementHolder);
            obj.name = "BriefElement_" + (briefElementHolder.childCount - 1);
            obj.GetComponent<BriefElement>().Initialize(isBasement, isPodium, false);
            obj.GetComponent<BriefElement>().onChanged.AddListener(OnBriefElementChanged);
            obj.GetComponent<BriefElement>().onDeleted.AddListener(OnBriefElementDeleted);
            obj.transform.SetSiblingIndex(briefElementHolder.childCount - 2);
            //bool isGroundFloor = false;
            //if (siteBasementPanel.currentElement.levels != null && siteBasementPanel.currentElement.levels.Count > 0)
            //{
            //    isGroundFloor = floorPanel.currentElement.levels.Count == 1 && floorPanel.currentElement.levels[0] == 0;
            //}
            obj.GetComponent<BriefElement>().SetValues(type, percentage, false);
            briefElementHolder.GetComponent<RectTransform>().sizeDelta = new Vector3(briefElementHolder.GetComponent<RectTransform>().sizeDelta.x, briefElementHolder.childCount * 50);
            if (type == "Commercial" || type == "Other")
            {
                addBriefButton.interactable = false;
                addBriefButton.GetComponent<UITooltip>().tooltip.text = "You cannot have another type together with Comm/Other!";
            }
            else
            {
                addBriefButton.interactable = true;
                addBriefButton.GetComponent<UITooltip>().tooltip.text = "";
            }
        }

        public override void LoadBriefElement(string type, float percentage)
        {
            GameObject obj = Instantiate(briefElementPrefab, briefElementHolder);
            obj.name = "BriefElement_" + (briefElementHolder.childCount - 1);
            obj.transform.SetSiblingIndex(briefElementHolder.childCount - 2);
            obj.GetComponent<BriefElement>().onChanged.AddListener(OnBriefElementChanged);
            obj.GetComponent<BriefElement>().onDeleted.AddListener(OnBriefElementDeleted);
            //bool isGroundFloor = floorPanel.currentElement.levels.Count == 1 && floorPanel.currentElement.levels[0] == 0;
            obj.GetComponent<BriefElement>().LoadValues(type, percentage, false);
            briefElementHolder.GetComponent<RectTransform>().sizeDelta = new Vector3(briefElementHolder.GetComponent<RectTransform>().sizeDelta.x, briefElementHolder.childCount * 50);
            if (type == "Commercial" || type == "Other")
            {
                addBriefButton.interactable = false;
                addBriefButton.GetComponent<UITooltip>().tooltip.text = "You cannot have another type together with Comm/Other!";
            }
            else
            {
                addBriefButton.interactable = true;
                addBriefButton.GetComponent<UITooltip>().tooltip.text = "";
            }
        }

        public override void OnBriefElementChanged(BriefElement sender)
        {
            if (sender.type == "Commercial" || sender.type == "Other")
            {
                addBriefButton.interactable = false;
                addBriefButton.GetComponent<UITooltip>().tooltip.text = "You cannot have another type together with Comm/Other!";
            }
            else
            {
                addBriefButton.interactable = true;
                addBriefButton.GetComponent<UITooltip>().tooltip.text = "";
            }
            buildingPanel.OnBriefChanged(this);
        }

        protected override IEnumerator RemoveElement(BriefElement sender)
        {
            for (int i = 0; i < briefElementHolder.childCount - 1; i++)
            {
                if (briefElementHolder.GetChild(i).GetComponent<BriefElement>() == sender)
                {
                    Destroy(briefElementHolder.GetChild(i).gameObject);
                }
            }
            yield return new WaitForEndOfFrame();
            buildingPanel.OnBriefChanged(this);
        }

        protected override void AddEvents()
        {
            
        }
    }
}

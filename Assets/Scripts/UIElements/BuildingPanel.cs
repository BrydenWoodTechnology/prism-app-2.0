﻿using BrydenWoodUnity.DesignData;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for managing the building elements
    /// </summary>
    public class BuildingPanel : MonoBehaviour
    {
        #region Public Fields and Properties
        [Header("Scene References:")]
        public Transform buildingElementHolder;
        public GameObject buildingElementPrefab;
        public FloorPanel floorPanel;
        public FloorPanel towerFloorPanel;
        public BlocksPanel blocksPanel;
        public BuildingElement currentElement;
        public Button addBuildingButton;
        public Dropdown coreAllignmentDropdown;
        public Dropdown coreLockDropDown;
        public Dropdown typologyDropdown;
        public InputField buildingNameInputField;
        public InputField towerApartmentDepthField;
        public InputField mansionApartmentDepthField;
        public InputField corridorWidthField;
        public InputField podiumFloorsInputField;
        public InputField podiumFloor2FloorInputField;
        public InputField podiumOffsetInputField;
        public BriefPanel podiumBriefPanel;
        public BriefPanel basementBriefPanel;
        public InputField basementFloorsInputField;
        public InputField basementFloor2FloorInputField;
        public InputField basementOffsetInputField;
        public DesignInputTabs designInputTabs;
        public GameObject linearBuildingUI;
        public GameObject towerBuildingUI;
        public GameObject mansionBuildingUI;
        public Button placeButton;
        public Button rotateButton;
        public int currentElementIndex;
        public bool loading;
        public object waitFrame;

        public int floorIndex { get { return floorPanel.currentElementIndex; } }
        #endregion

        #region MonoBehaviour Methods
        private void OnDestroy()
        {
            RemoveEvents();
        }

        // Use this for initialization
        void Start()
        {
            AddEvents();
            waitFrame = new WaitForEndOfFrame();
        }
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Called when a building element is deleted
        /// </summary>
        /// <param name="sender">The deleted building element</param>
        public void OnBuildingDeleted(BuildingElement sender)
        {
            StartCoroutine(OnBuildingElementDeleted(sender));
        }

        /// <summary>
        /// Called when a floor element is deleted
        /// </summary>
        /// <param name="floorIndex">The index of the deleted floor element</param>
        public void OnFloorDeleted(int floorIndex)
        {
            currentElement.OnFloorDeleted(floorIndex);
        }

        /// <summary>
        /// Called when the brief of a floor element has changed
        /// </summary>
        /// <param name="floorIndex">The index of the floor element</param>
        /// <param name="percentages">The new brief percentages</param>
        public void OnBriefChanged(int floorIndex, Dictionary<string, float> percentages)
        {
            if (floorIndex < currentElement.buildingData.percentages.Count)
            {
                currentElement.buildingData.percentages[floorIndex] = percentages;
            }
            else
            {
                currentElement.buildingData.percentages.Add(percentages);
            }
            designInputTabs.OnBriefElementChanged(floorIndex, percentages);
        }

        /// <summary>
        /// Called from the Unity UI to add a linear building element to the list
        /// </summary>
        public void AddLinearBuildingElement()
        {
            if (currentElement != null)
            {
                currentElement.Deselect();
            }

            linearBuildingUI.SetActive(true);
            towerBuildingUI.SetActive(false);
            mansionBuildingUI.SetActive(false);
            buildingNameInputField.SetValue("");
            floorPanel.ClearPrevious();
            GameObject obj = Instantiate(buildingElementPrefab, buildingElementHolder);
            obj.transform.SetSiblingIndex(buildingElementHolder.childCount - 2);
            obj.name = "BuildingElement_" + (buildingElementHolder.childCount - 1);
            currentElement = obj.GetComponent<BuildingElement>();
            currentElement.buildingName = (buildingElementHolder.childCount - 1).ToString();

            float aptDepth = 7.2f;
            float crdrWidth = 1.2f;
            //float.TryParse(apartmentDepthField.text, out aptDepth);
            float.TryParse(corridorWidthField.text, out crdrWidth);

            currentElement.Initialize(crdrWidth, BuildingType.Linear);
            currentElementIndex = buildingElementHolder.childCount - 2;

            //if (coreAllignmentDropdown.options.Count == 2)
            //{
            //    coreAllignmentDropdown.options.Insert(0, new Dropdown.OptionData("Centre"));
            //    coreAllignmentDropdown.SetValue(0);
            //    coreAllignmentDropdown.RefreshShownValue();
            //}

            coreAllignmentDropdown.SetValue(0);
            coreLockDropDown.SetValue(0);
            typologyDropdown.SetValue(0);
            coreAllignmentDropdown.onValueChanged.RemoveAllListeners();
            coreLockDropDown.onValueChanged.RemoveAllListeners();
            typologyDropdown.onValueChanged.RemoveAllListeners();
            buildingNameInputField.onValueChanged.RemoveAllListeners();
            coreAllignmentDropdown.onValueChanged.AddListener(currentElement.OnCoreAllignmentChanged);
            coreLockDropDown.onValueChanged.AddListener(currentElement.OnCoreLockChanged);
            typologyDropdown.onValueChanged.AddListener(currentElement.OnAspectChanged);
            buildingNameInputField.onValueChanged.AddListener(currentElement.OnBuildingNameChanged);

            podiumBriefPanel.ClearPrevious();
            podiumFloorsInputField.SetValue(string.Empty);
            podiumFloor2FloorInputField.SetValue(string.Empty);
            podiumOffsetInputField.SetValue(string.Empty);
            basementBriefPanel.ClearPrevious();
            basementFloorsInputField.SetValue(string.Empty);
            basementFloor2FloorInputField.SetValue(string.Empty);
            basementOffsetInputField.SetValue(string.Empty);

            currentElement.typologyDropdown = typologyDropdown;
            currentElement.coreAlignmentDropdown = coreAllignmentDropdown;
            currentElement.floorPanel = floorPanel;
            currentElement.coreLockDropdown = coreLockDropDown;
            currentElement.buildingNameInputField = buildingNameInputField;
            currentElement.corridorWidthField = corridorWidthField;
            currentElement.buildingPanel = this;
            currentElement.SetProceduralBuilding(designInputTabs.OnBuildingAdded(currentElement));
            currentElement.buildingData.coreWidth = Standards.TaggedObject.CoreSizesPerBuildingType[BuildingType.Linear][0];
            currentElement.buildingData.coreLength = Standards.TaggedObject.CoreSizesPerBuildingType[BuildingType.Linear][1];
            if (!loading)
            {
                //AddGroundFloor();
                AddSecondFloor();
            }
            obj.GetComponent<Toggle>().SetValue(true);
        }

        public void AddMansionBuildingElement()
        {
            if (currentElement != null)
            {
                currentElement.Deselect();
            }
            linearBuildingUI.SetActive(false);
            towerBuildingUI.SetActive(false);
            mansionBuildingUI.SetActive(true);
            buildingNameInputField.SetValue("");
            blocksPanel.ClearPrevious();


            GameObject obj = Instantiate(buildingElementPrefab, buildingElementHolder);
            obj.transform.SetSiblingIndex(buildingElementHolder.childCount - 2);
            obj.name = "BuildingElement_" + (buildingElementHolder.childCount - 1);
            currentElement = obj.GetComponent<BuildingElement>();
            currentElement.buildingName = (buildingElementHolder.childCount - 1).ToString();

            currentElement.Initialize(0, BuildingType.Mansion);
            currentElementIndex = buildingElementHolder.childCount - 2;

            buildingNameInputField.onValueChanged.RemoveAllListeners();
            buildingNameInputField.onValueChanged.AddListener(currentElement.OnBuildingNameChanged);

            podiumBriefPanel.ClearPrevious();
            podiumFloorsInputField.SetValue(string.Empty);
            podiumFloor2FloorInputField.SetValue(string.Empty);
            podiumOffsetInputField.SetValue(string.Empty);
            basementBriefPanel.ClearPrevious();
            basementFloorsInputField.SetValue(string.Empty);
            basementFloor2FloorInputField.SetValue(string.Empty);
            basementOffsetInputField.SetValue(string.Empty);

            currentElement.SetProceduralBuilding(designInputTabs.OnBuildingAdded(currentElement));
            currentElement.buildingPanel = this;
            blocksPanel.proceduralMansion = currentElement.proceduralBuilding.AddMansionFloor(0, "Mansion", blocksPanel);
            blocksPanel.buildingElement = currentElement;
            currentElement.buildingData.floorHeights = new List<float>() { 3.2f };
            currentElement.buildingData.coreWidth = Standards.TaggedObject.CoreSizesPerBuildingType[BuildingType.Mansion][0];
            currentElement.buildingData.coreLength = Standards.TaggedObject.CoreSizesPerBuildingType[BuildingType.Mansion][1];
            obj.GetComponent<Toggle>().SetValue(true);
        }

        /// <summary>
        /// Called from the Unity UI to add a tower building element to the list
        /// </summary>
        public void AddTowerBuilding()
        {
            if (currentElement != null)
            {
                currentElement.Deselect();
            }
            //placeButton.interactable = true;
            //placeButton.transform.GetChild(0).GetComponent<Text>().text = "Place";
            //rotateButton.interactable = false;
            linearBuildingUI.SetActive(false);
            towerBuildingUI.SetActive(true);
            mansionBuildingUI.SetActive(false);
            buildingNameInputField.SetValue("");
            towerFloorPanel.ClearPrevious();
            GameObject obj = Instantiate(buildingElementPrefab, buildingElementHolder);
            obj.transform.SetSiblingIndex(buildingElementHolder.childCount - 2);
            obj.name = "BuildingElement_" + (buildingElementHolder.childCount - 1);
            currentElement = obj.GetComponent<BuildingElement>();
            currentElement.buildingName = (buildingElementHolder.childCount - 1).ToString();

            float aptDepth = 7.2f;
            float crdrWidth = 1.2f;
            float.TryParse(towerApartmentDepthField.text, out aptDepth);
            float.TryParse(corridorWidthField.text, out crdrWidth);

            currentElement.Initialize(aptDepth, crdrWidth, BuildingType.Tower);
            currentElementIndex = buildingElementHolder.childCount - 2;

            buildingNameInputField.onValueChanged.RemoveAllListeners();
            buildingNameInputField.onValueChanged.AddListener(currentElement.OnBuildingNameChanged);

            podiumBriefPanel.ClearPrevious();
            podiumFloorsInputField.SetValue(string.Empty);
            podiumFloor2FloorInputField.SetValue(string.Empty);
            podiumOffsetInputField.SetValue(string.Empty);
            basementBriefPanel.ClearPrevious();
            basementFloorsInputField.SetValue(string.Empty);
            basementFloor2FloorInputField.SetValue(string.Empty);
            basementOffsetInputField.SetValue(string.Empty);
            currentElement.buildingPanel = this;


            currentElement.SetProceduralBuilding(designInputTabs.OnBuildingAdded(currentElement));
            currentElement.buildingData.coreWidth = Standards.TaggedObject.CoreSizesPerBuildingType[BuildingType.Tower][0];
            currentElement.buildingData.coreLength = Standards.TaggedObject.CoreSizesPerBuildingType[BuildingType.Tower][1];
            if (!loading)
            {
                //AddGroundFloor();
            }
            obj.GetComponent<Toggle>().SetValue(true);
        }

        /// <summary>
        /// Called from the Unity UI to cancel the addition of a building element
        /// </summary>
        public void CancelBuildingAddition()
        {

        }

        /// <summary>
        /// Called when the values of a floor element have changed
        /// </summary>
        /// <param name="floorIndex">The index of the floor element</param>
        /// <param name="levels">The new levels of the floor element</param>
        /// <param name="f2f">The new floor to floor height of the floor element</param>
        /// <param name="aptTypes">The new list of apartment types of the floor element</param>
        public void UpdateFloorValues(int floorIndex, List<int> levels, float f2f, float leftDepth, float rightDepth, string[] aptTypes = null)
        {
            currentElement.UpdateFloorValues(floorIndex, levels, f2f, leftDepth, rightDepth, aptTypes);
        }

        /// <summary>
        /// Adds a building element with set values to the list
        /// </summary>
        /// <param name="building">The building data</param>
        public void AddBuildingElement(BuildingData building)
        {
            if (currentElement != null)
            {
                currentElement.GetComponent<Toggle>().isOn = false;
            }
            GameObject obj = Instantiate(buildingElementPrefab, buildingElementHolder);
            obj.name = "BuildingElement_" + (buildingElementHolder.childCount - 1);
            obj.transform.SetSiblingIndex(buildingElementHolder.childCount - 2);
            currentElementIndex = buildingElementHolder.childCount - 2;
            currentElement = obj.GetComponent<BuildingElement>();
            currentElement.Initialize();
            currentElement.typologyDropdown = typologyDropdown;
            currentElement.coreAlignmentDropdown = coreAllignmentDropdown;
            currentElement.floorPanel = floorPanel;
            currentElement.coreLockDropdown = coreLockDropDown;
            currentElement.buildingNameInputField = buildingNameInputField;
            currentElement.corridorWidthField = corridorWidthField;
            currentElement.buildingPanel = this;

            coreAllignmentDropdown.onValueChanged.RemoveAllListeners();
            coreLockDropDown.onValueChanged.RemoveAllListeners();
            typologyDropdown.onValueChanged.RemoveAllListeners();
            buildingNameInputField.onValueChanged.RemoveAllListeners();
            coreAllignmentDropdown.onValueChanged.AddListener(currentElement.OnCoreAllignmentChanged);
            coreLockDropDown.onValueChanged.AddListener(currentElement.OnCoreLockChanged);
            typologyDropdown.onValueChanged.AddListener(currentElement.OnAspectChanged);
            buildingNameInputField.onValueChanged.AddListener(currentElement.OnBuildingNameChanged);

            currentElement.SetProceduralBuilding(designInputTabs.OnBuildingAdded(currentElement));
            currentElement.SetBuildingData(building);

            for (int i = 0; i < building.levels.Count; i++)
            {
                floorPanel.AddFloorElement(building.levels[i], building.floorHeights[i], currentElement.proceduralBuilding.AddFloor(i, "LoadedFloor"));
            }

            obj.GetComponent<Toggle>().group = GetComponent<ToggleGroup>();
            obj.GetComponent<Toggle>().isOn = true;
        }

        /// <summary>
        /// Loads a tower building element
        /// </summary>
        /// <param name="building">The data of the building to be loaded</param>
        public void LoadTowerBuildingElement(BuildingData building)
        {
            if (currentElement != null)
            {
                currentElement.Deselect();
            }
            towerBuildingUI.SetActive(true);
            linearBuildingUI.SetActive(false);
            mansionBuildingUI.SetActive(false);
            GameObject obj = Instantiate(buildingElementPrefab, buildingElementHolder);
            obj.name = "BuildingElement_" + (buildingElementHolder.childCount - 1);
            obj.transform.SetSiblingIndex(buildingElementHolder.childCount - 2);
            currentElementIndex = buildingElementHolder.childCount - 2;
            currentElement = obj.GetComponent<BuildingElement>();
            currentElement.Initialize();

            towerApartmentDepthField.SetValue(building.apartmentDepth.ToString("0.00"));
            currentElement.buildingNameInputField = buildingNameInputField;
            buildingNameInputField.onValueChanged.RemoveAllListeners();
            buildingNameInputField.onValueChanged.AddListener(currentElement.OnBuildingNameChanged);

            currentElement.SetProceduralBuilding(designInputTabs.OnBuildingAdded(currentElement));
            currentElement.SetBuildingData(building);
            currentElement.buildingPanel = this;

            //----------For Podium--------//
            if (building.podiumLevels != null && building.podiumLevels.Length > 0)
            {
                podiumFloorsInputField.SetValue(building.podiumLevels.Length);
                currentElement.proceduralBuilding.AddPodium(building.podiumLevels, building.podiumFloorHeight, building.podiumPercentages, building.podiumOffset);
            }
            podiumFloor2FloorInputField.SetValue(building.podiumFloorHeight);
            podiumOffsetInputField.SetValue(building.podiumOffset);
            if (building.podiumPercentages != null && building.podiumPercentages.Count > 0)
            {
                foreach (var item in building.podiumPercentages)
                {
                    podiumBriefPanel.LoadBriefElement(item.Key, item.Value);
                }
            }

            //----------For Basement--------//
            if (building.basementLevels != null && building.basementLevels.Length > 0)
            {
                basementFloorsInputField.SetValue(building.basementLevels.Length);
                currentElement.proceduralBuilding.AddBasement(building.basementLevels, building.basementFloorHeight, building.basementPercentages, building.basementOffset);
            }
            basementFloor2FloorInputField.SetValue(building.basementFloorHeight);
            basementOffsetInputField.SetValue(building.basementOffset);
            if (building.basementPercentages != null && building.basementPercentages.Count > 0)
            {
                foreach (var item in building.basementPercentages)
                {
                    basementBriefPanel.LoadBriefElement(item.Key, item.Value);
                }
            }

            towerFloorPanel.ClearPrevious();

            for (int i = 0; i < building.levels.Count; i++)
            {
                towerFloorPanel.LoadTowerFloorElement(building.levels[i], building.floorHeights[i], building.percentages[i], currentElement.proceduralBuilding.LoadTowerFloor(i, "LoadedFloor", building.percentages[i], building.floorHeights[i], building.levels[i].ToArray(), building.aptTypesTower[i]));
            }
        }

        public void LoadMansionBuildingElement(BuildingData building)
        {
            if (currentElement != null)
            {
                currentElement.Deselect();
            }
            towerBuildingUI.SetActive(false);
            linearBuildingUI.SetActive(false);
            mansionBuildingUI.SetActive(true);
            GameObject obj = Instantiate(buildingElementPrefab, buildingElementHolder);
            obj.name = "BuildingElement_" + (buildingElementHolder.childCount - 1);
            obj.transform.SetSiblingIndex(buildingElementHolder.childCount - 2);
            currentElementIndex = buildingElementHolder.childCount - 2;
            currentElement = obj.GetComponent<BuildingElement>();
            currentElement.Initialize();

            blocksPanel.buildingElement = currentElement;
            //blocksPanel.apartmentDepthField.SetValue(building.apartmentDepth.ToString("0.00"));
            blocksPanel.apartmentDepthField.SetValue(building.depth);
            blocksPanel.levelsField.SetValue(building.levels[0][0] + "-" + building.levels[0].Last());
            blocksPanel.floor2FloorField.SetValue(building.floorHeights[0]);
            blocksPanel.coreWidthField.SetValue(building.coreWidth);
            blocksPanel.coreLengthField.SetValue(building.coreLength);
            currentElement.buildingNameInputField = buildingNameInputField;
            buildingNameInputField.onValueChanged.RemoveAllListeners();
            buildingNameInputField.onValueChanged.AddListener(currentElement.OnBuildingNameChanged);

            currentElement.SetProceduralBuilding(designInputTabs.OnBuildingAdded(currentElement));
            currentElement.SetBuildingData(building);
            currentElement.buildingPanel = this;

            //----------For Podium--------//
            if (building.podiumLevels != null && building.podiumLevels.Length > 0)
            {
                podiumFloorsInputField.SetValue(building.podiumLevels.Length);
                currentElement.proceduralBuilding.AddPodium(building.podiumLevels, building.podiumFloorHeight, building.podiumPercentages, building.podiumOffset);
            }
            podiumFloor2FloorInputField.SetValue(building.podiumFloorHeight);
            podiumOffsetInputField.SetValue(building.podiumOffset);
            if (building.podiumPercentages != null && building.podiumPercentages.Count > 0)
            {
                foreach (var item in building.podiumPercentages)
                {
                    podiumBriefPanel.LoadBriefElement(item.Key, item.Value);
                }
            }

            //----------For Basement--------//
            if (building.basementLevels != null && building.basementLevels.Length > 0)
            {
                basementFloorsInputField.SetValue(building.basementLevels.Length);
                currentElement.proceduralBuilding.AddBasement(building.basementLevels, building.basementFloorHeight, building.basementPercentages, building.basementOffset);
            }
            basementFloor2FloorInputField.SetValue(building.basementFloorHeight);
            basementOffsetInputField.SetValue(building.basementOffset);
            if (building.basementPercentages != null && building.basementPercentages.Count > 0)
            {
                foreach (var item in building.basementPercentages)
                {
                    basementBriefPanel.LoadBriefElement(item.Key, item.Value);
                }
            }

            blocksPanel.ClearPrevious();
            for (int i = 0; i < building.mansionBlocks.Length; i++)
            {
                blocksPanel.AddBlock(building.mansionBlocks[i],true);
            }
            blocksPanel.proceduralMansion = currentElement.proceduralBuilding.AddMansionFloor(0, "Mansion", blocksPanel);
            blocksPanel.proceduralMansion.levels = building.levels[0].ToArray();
            blocksPanel.proceduralMansion.floor2floor = building.floorHeights[0];
            blocksPanel.proceduralMansion.aptDepth = building.apartmentDepth;
        }

        public void DeletePodium()
        {
            podiumBriefPanel.ClearPrevious();
            podiumFloor2FloorInputField.SetValue(string.Empty);
            podiumFloorsInputField.SetValue(string.Empty);
            podiumOffsetInputField.SetValue(string.Empty);
        }

        public void DeleteBasement()
        {
            basementBriefPanel.ClearPrevious();
            basementFloor2FloorInputField.SetValue(string.Empty);
            basementFloorsInputField.SetValue(string.Empty);
            basementOffsetInputField.SetValue(string.Empty);
        }

        /// <summary>
        /// Loads a linear building element
        /// </summary>
        /// <param name="building">The data of the building to be loaded</param>
        public void LoadLinearBuildingElement(BuildingData building)
        {
            if (currentElement != null)
            {
                currentElement.Deselect();
            }
            towerBuildingUI.SetActive(false);
            linearBuildingUI.SetActive(true);
            mansionBuildingUI.SetActive(false);
            GameObject obj = Instantiate(buildingElementPrefab, buildingElementHolder);
            obj.name = "BuildingElement_" + (buildingElementHolder.childCount - 1);
            obj.transform.SetSiblingIndex(buildingElementHolder.childCount - 2);
            currentElementIndex = buildingElementHolder.childCount - 2;
            currentElement = obj.GetComponent<BuildingElement>();
            currentElement.Initialize();

            //apartmentDepthField.SetValue(building.apartmentDepth.ToString("0.00"));
            corridorWidthField.SetValue(building.corridorWidth.ToString("0.00"));

            currentElement.typologyDropdown = typologyDropdown;
            currentElement.coreAlignmentDropdown = coreAllignmentDropdown;
            currentElement.floorPanel = floorPanel;
            currentElement.coreLockDropdown = coreLockDropDown;
            currentElement.buildingNameInputField = buildingNameInputField;
            currentElement.corridorWidthField = corridorWidthField;
            currentElement.buildingPanel = this;

            coreAllignmentDropdown.onValueChanged.RemoveAllListeners();
            coreLockDropDown.onValueChanged.RemoveAllListeners();
            typologyDropdown.onValueChanged.RemoveAllListeners();
            buildingNameInputField.onValueChanged.RemoveAllListeners();
            coreAllignmentDropdown.onValueChanged.AddListener(currentElement.OnCoreAllignmentChanged);
            coreLockDropDown.onValueChanged.AddListener(currentElement.OnCoreLockChanged);
            typologyDropdown.onValueChanged.AddListener(currentElement.OnAspectChanged);
            buildingNameInputField.onValueChanged.AddListener(currentElement.OnBuildingNameChanged);

            currentElement.SetProceduralBuilding(designInputTabs.OnBuildingAdded(currentElement));
            currentElement.SetBuildingData(building);


            //----------For Podium--------//
            if (building.podiumLevels != null && building.podiumLevels.Length > 0)
            {
                podiumFloorsInputField.SetValue(building.podiumLevels.Length);
                currentElement.proceduralBuilding.AddPodium(building.podiumLevels, building.podiumFloorHeight, building.podiumPercentages, building.podiumOffset);
            }
            podiumFloor2FloorInputField.SetValue(building.podiumFloorHeight);
            podiumOffsetInputField.SetValue(building.podiumOffset);
            if (building.podiumPercentages != null && building.podiumPercentages.Count > 0)
            {
                foreach (var item in building.podiumPercentages)
                {
                    podiumBriefPanel.LoadBriefElement(item.Key, item.Value);
                }
            }

            //----------For Basement--------//
            if (building.basementLevels != null && building.basementLevels.Length > 0)
            {
                basementFloorsInputField.SetValue(building.basementLevels.Length);
                currentElement.proceduralBuilding.AddBasement(building.basementLevels, building.basementFloorHeight, building.basementPercentages, building.basementOffset);
            }
            basementFloor2FloorInputField.SetValue(building.basementFloorHeight);
            basementOffsetInputField.SetValue(building.basementOffset);
            if (building.basementPercentages != null && building.basementPercentages.Count > 0)
            {
                foreach (var item in building.basementPercentages)
                {
                    basementBriefPanel.LoadBriefElement(item.Key, item.Value);
                }
            }

            floorPanel.ClearPrevious();

            for (int i = 0; i < building.levels.Count; i++)
            {
                if (building.rightDepth != null && building.rightDepth.Count > 0)
                {
                    floorPanel.LoadFloorElement(building.levels[i], building.floorHeights[i], building.percentages[i], currentElement.proceduralBuilding.LoadFloor(i, "LoadedFloor", building.percentages[i], building.floorHeights[i], building.levels[i].ToArray(), building.leftDepth[i], building.rightDepth[i]));
                }
                else
                {
                    floorPanel.LoadFloorElement(building.levels[i], building.floorHeights[i], building.percentages[i], currentElement.proceduralBuilding.LoadFloor(i, "LoadedFloor", building.percentages[i], building.floorHeights[i], building.levels[i].ToArray(), building.apartmentDepth, building.apartmentDepth));
                }
            }
        }

        /// <summary>
        /// Called when a building element is deleted
        /// </summary>
        /// <param name="sender">The current list of building elements</param>
        public IEnumerator OnBuildingElementDeleted(BuildingElement sender)
        {
            int deleteIndex = -1;
            for (int i = 0; i < buildingElementHolder.childCount - 1; i++)
            {
                if (buildingElementHolder.GetChild(i).GetComponent<BuildingElement>() == sender)
                {
                    Destroy(buildingElementHolder.GetChild(i).gameObject);
                    deleteIndex = i;
                }
            }
            yield return waitFrame;
            try
            {
                if (currentElementIndex > deleteIndex)
                {
                    buildingElementHolder.GetChild(currentElementIndex - 1).GetComponent<Toggle>().SetValue(true);
                    buildingElementHolder.GetChild(currentElementIndex - 1).GetComponent<BuildingElement>().Selected(true);
                }
                else if (currentElementIndex == deleteIndex)
                {
                    if (deleteIndex == buildingElementHolder.childCount - 1 && deleteIndex > 0)
                    {
                        buildingElementHolder.GetChild(currentElementIndex - 1).GetComponent<Toggle>().isOn = true;
                    }
                    else if (deleteIndex == 0)
                    {
                        if (buildingElementHolder.childCount > 1)
                        {
                            buildingElementHolder.GetChild(deleteIndex).GetComponent<Toggle>().isOn = true;
                        }
                    }
                    else
                    {
                        buildingElementHolder.GetChild(deleteIndex).GetComponent<Toggle>().isOn = true;
                    }
                }
            }
            catch (Exception e)
            {
                UnityEngine.Debug.Log(e);
            }
            designInputTabs.OnBuildingRemoved(deleteIndex);
        }

        /// <summary>
        /// Returns the data of all the buildings in the project
        /// </summary>
        /// <returns>List of Building Data</returns>
        public List<BuildingData> GetBuildingData()
        {
            List<BuildingData> data = new List<BuildingData>();
            foreach (Transform item in buildingElementHolder)
            {
                var build = item.GetComponent<BuildingElement>();
                if (build != null)
                {
                    data.Add(item.GetComponent<BuildingElement>().buildingData);
                }
            }
            return data;
        }

        /// <summary>
        /// Sets the selected building element based on index
        /// </summary>
        /// <param name="index">The index of the building element to be selected</param>
        public void SetSelectedBuildingElement(int index)
        {
            OnBuildingElementSelected(buildingElementHolder.GetChild(index).GetComponent<BuildingElement>());
        }

        /// <summary>
        /// Called when a floor element is added to the current building
        /// </summary>
        /// <param name="element">The floor element to be added</param>
        /// <param name="floorIndex">The index of the floor element</param>
        /// <returns>Procedural Floor</returns>
        public ProceduralFloor OnFloorAdded(FloorElement element, int floorIndex)
        {
            return currentElement.OnFloorAdded(element, floorIndex);
        }

        /// <summary>
        /// Called from Unity UI when the apartment depth changes
        /// </summary>
        /// <param name="value">The new apartment depth</param>
        public void OnApartmentDepthChanged(string value)
        {
            float depth = 7.2f;
            if (float.TryParse(value, out depth) && currentElement.buildingData.buildingType == 1)
            {
                currentElement.buildingData.apartmentDepth = depth;
            }
        }

        /// <summary>
        /// Called from Unity UI when the corridor width changes
        /// </summary>
        /// <param name="value">The new corridor width</param>
        public void OnCorridorWidthChanged(string value)
        {
            float width = 1.2f;
            {
                if (float.TryParse(value, out width))
                {
                    currentElement.buildingData.corridorWidth = width;
                }
            }
        }

        public void OnCoreWidthChanged(string value)
        {
            float width = 4.8f;
            {
                if (float.TryParse(value, out width))
                {
                    currentElement.buildingData.coreWidth = width;
                }
            }
        }

        public void OnCoreLengthChanged(string value)
        {
            float length = 7f;
            if (float.TryParse(value, out length))
            {
                currentElement.buildingData.coreLength = length;
            }
        }

        /// <summary>
        /// Called when a building element is selected
        /// </summary>
        /// <param name="sender">The current list of building elements</param>
        public void OnBuildingElementSelected(BuildingElement sender)
        {
            if (currentElement != null && currentElement != sender)
            {
                currentElement.Deselect();
            }
            currentElement = sender;

            if (sender.buildingType == BuildingType.Linear)
            {
                linearBuildingUI.gameObject.SetActive(true);
                towerBuildingUI.gameObject.SetActive(false);
                mansionBuildingUI.gameObject.SetActive(false);
                //if (sender.typology != 0)
                //{
                //    if (coreAllignmentDropdown.options.Count == 3)
                //    {
                //        coreAllignmentDropdown.options.RemoveAt(0);
                //    }
                //    coreAllignmentDropdown.SetValue(currentElement.coreAllignment - 1);
                //}
                //else if (sender.typology == 0)
                //{
                //    if (coreAllignmentDropdown.options.Count == 2)
                //    {
                //        coreAllignmentDropdown.options.Insert(0, new Dropdown.OptionData("Centre"));
                //    }
                //    coreAllignmentDropdown.SetValue(currentElement.coreAllignment);
                //}
                //coreAllignmentDropdown.RefreshShownValue();

                typologyDropdown.SetValue(currentElement.typology);
                typologyDropdown.RefreshShownValue();



                coreAllignmentDropdown.onValueChanged.RemoveAllListeners();
                coreLockDropDown.onValueChanged.RemoveAllListeners();
                typologyDropdown.onValueChanged.RemoveAllListeners();
                coreAllignmentDropdown.onValueChanged.AddListener(currentElement.OnCoreAllignmentChanged);
                coreLockDropDown.onValueChanged.AddListener(currentElement.OnCoreLockChanged);
                typologyDropdown.onValueChanged.AddListener(currentElement.OnAspectChanged);

                floorPanel.ClearPrevious();
                if (sender.proceduralBuilding.floors != null)
                {
                    for (int i = 0; i < sender.proceduralBuilding.floors.Count; i++)
                    {
                        floorPanel.AddFloorElement(sender.proceduralBuilding.floors[i].levels.ToList(), sender.proceduralBuilding.floors[i].floor2floor, sender.proceduralBuilding.floors[i], i == sender.proceduralBuilding.floors.Count - 1, false);
                    }
                }
            }
            else if (sender.buildingType == BuildingType.Tower)
            {
                linearBuildingUI.gameObject.SetActive(false);
                mansionBuildingUI.gameObject.SetActive(false);
                towerBuildingUI.gameObject.SetActive(true);
                placeButton.gameObject.SetActive(!sender.proceduralBuilding.Placed);
                towerFloorPanel.ClearPrevious();
                if (sender.proceduralBuilding.floors != null)
                {
                    for (int i = 0; i < sender.proceduralBuilding.floors.Count; i++)
                    {
                        var tower = sender.proceduralBuilding.floors[i] as ProceduralTower;
                        if (tower != null)
                        {
                            towerFloorPanel.AddFloorElement(sender.proceduralBuilding.floors[i].levels.ToList(), sender.proceduralBuilding.floors[i].floor2floor, tower, i == sender.proceduralBuilding.floors.Count - 1, false);
                        }
                    }
                }
                towerApartmentDepthField.SetValue(currentElement.buildingData.apartmentDepth.ToString("0.00"));
            }
            else if (sender.buildingType == BuildingType.Mansion)
            {
                linearBuildingUI.gameObject.SetActive(false);
                mansionBuildingUI.gameObject.SetActive(true);
                towerBuildingUI.gameObject.SetActive(false);
                blocksPanel.ClearPrevious();
                if (sender.proceduralBuilding.floors != null)
                {
                    blocksPanel.SetMansionBlocks(sender.buildingData.mansionBlocks.ToList());
                }
            }

            if (currentElement.proceduralBuilding.podium != null)
            {
                podiumBriefPanel.ClearPrevious();
                foreach (var item in currentElement.proceduralBuilding.podium.percentages)
                {
                    podiumBriefPanel.AddBriefElement(item.Key, item.Value);
                }
                podiumFloorsInputField.SetValue(currentElement.proceduralBuilding.podium.levels.Length.ToString());
                podiumFloor2FloorInputField.SetValue(currentElement.proceduralBuilding.podium.floor2floor.ToString());
                podiumOffsetInputField.SetValue(currentElement.proceduralBuilding.podium.footPrintOffset);
            }
            else
            {
                podiumBriefPanel.ClearPrevious();
                podiumFloorsInputField.SetValue(string.Empty);
                podiumFloor2FloorInputField.SetValue(string.Empty);
                podiumOffsetInputField.SetValue(string.Empty);
            }

            if (currentElement.proceduralBuilding.basement != null)
            {
                basementBriefPanel.ClearPrevious();
                foreach (var item in currentElement.proceduralBuilding.basement.percentages)
                {
                    basementBriefPanel.AddBriefElement(item.Key, item.Value);
                }
                basementFloorsInputField.SetValue(currentElement.proceduralBuilding.basement.levels.Length.ToString());
                basementFloor2FloorInputField.SetValue(currentElement.proceduralBuilding.basement.floor2floor.ToString());
                basementOffsetInputField.SetValue(currentElement.proceduralBuilding.basement.footPrintOffset);
            }
            else
            {
                basementBriefPanel.ClearPrevious();
                basementFloorsInputField.SetValue(string.Empty);
                basementFloor2FloorInputField.SetValue(string.Empty);
                basementOffsetInputField.SetValue(string.Empty);
            }

            buildingNameInputField.onValueChanged.RemoveAllListeners();
            buildingNameInputField.onValueChanged.AddListener(currentElement.OnBuildingNameChanged);
            buildingNameInputField.SetValue(currentElement.buildingData.name);
            for (int i = 0; i < buildingElementHolder.childCount - 1; i++)
            {
                if (buildingElementHolder.GetChild(i).GetComponent<BuildingElement>() == sender)
                {
                    currentElementIndex = i;
                }
            }
        }

        public void OnBriefChanged(BuildingBriefPanel briefPanel)
        {
            if (briefPanel.isPodium)
            {
                currentElement.proceduralBuilding.SetPodiumUse(briefPanel.GetPercentages(), this);
                currentElement.buildingData.podiumPercentages = briefPanel.GetPercentages();
            }
            else
            {
                currentElement.proceduralBuilding.SetBasementUse(briefPanel.GetPercentages());
                currentElement.buildingData.basementPercentages = briefPanel.GetPercentages();
            }
        }

        public void GetPodiumFeatures(out int[] levels, out float f2f, out float offset)
        {
            int numOfFloors = 2;
            if (!string.IsNullOrEmpty(podiumFloorsInputField.text))
            {
                if (!int.TryParse(podiumFloorsInputField.text, out numOfFloors))
                    numOfFloors = 2;
            }
            else
            {
                numOfFloors = 2;
            }
            podiumFloorsInputField.SetValue(numOfFloors.ToString());
            levels = new int[numOfFloors];
            for (int i = 0; i < numOfFloors; i++)
            {
                levels[i] = i;
            }
            f2f = 4.5f;
            if (!string.IsNullOrEmpty(podiumFloor2FloorInputField.text))
            {
                if (!float.TryParse(podiumFloor2FloorInputField.text, out f2f)) f2f = 4.5f;
            }
            else
            {
                f2f = 4.5f;
            }
            podiumFloor2FloorInputField.SetValue(f2f.ToString());
            offset = 0;
            if (!string.IsNullOrEmpty(podiumOffsetInputField.text))
            {
                if (!float.TryParse(podiumOffsetInputField.text, out offset)) offset = 0;
            }
            else
            {
                offset = 0;
            }
            podiumOffsetInputField.SetValue(offset.ToString());
        }

        public void GetBasementFeatures(out int[] levels, out float f2f, out float offset)
        {
            int numOfFloors = 2;
            if (!string.IsNullOrEmpty(basementFloorsInputField.text))
            {
                if (!int.TryParse(basementFloorsInputField.text, out numOfFloors)) numOfFloors = 2;
            }
            else
            {
                numOfFloors = 2;
            }
            basementFloorsInputField.SetValue(numOfFloors);
            levels = new int[numOfFloors];
            for (int i = 0; i < numOfFloors; i++)
            {
                levels[i] = (i + 1) * -1;
            }
            f2f = 4.5f;
            if (!string.IsNullOrEmpty(basementFloor2FloorInputField.text))
            {
                if (!float.TryParse(basementFloor2FloorInputField.text, out f2f)) f2f = 4.5f;
            }
            else
            {
                f2f = 4.5f;
            }
            basementFloor2FloorInputField.SetValue(f2f);
            offset = 0;
            if (!string.IsNullOrEmpty(basementOffsetInputField.text))
            {
                if (!float.TryParse(basementOffsetInputField.text, out offset)) offset = 0;
            }
            else
            {
                offset = 0;
            }
            basementOffsetInputField.SetValue(offset);
        }

        public void OnPodiumFeaturesChanged()
        {
            currentElement.proceduralBuilding.UpdatePodiumFeatures(this);
            currentElement.buildingData.UpdatePodiumFeatures(this);
        }

        public void OnBasementFeaturesChanged()
        {
            currentElement.proceduralBuilding.UpdateBasementFeatures(this);
            currentElement.buildingData.UpdateBasementFeatures(this);
        }
        #endregion

        #region Private Methods
        private void AddGroundFloor()
        {
            currentElement.buildingData.levels.Add(new List<int>() { 0 });
            currentElement.buildingData.percentages.Add(new Dictionary<string, float>() { { "Commercial", 100 } });
            currentElement.buildingData.floorHeights.Add(4.5f);
            currentElement.buildingData.aptTypesTower.Add(new string[] { });
            if (currentElement.buildingType == BuildingType.Linear)
                floorPanel.AddGroundFloor(currentElement.proceduralBuilding.AddFloor(0, "GroundFloor", new int[] { 0 }, 4.5f, new Dictionary<string, float>() { { "Commercial", 100 } }));
            else
                towerFloorPanel.AddGroundFloor(currentElement.proceduralBuilding.AddTowerFloor(0, "GroundFloor", new int[] { 0 }, 4.5f, null));
        }
        private void AddSecondFloor()
        {
            if (currentElement.buildingType == BuildingType.Linear)
                floorPanel.AddSecondFloor();
            else
                towerFloorPanel.AddSecondFloor();
        }
        private void AddEvents()
        {
            BuildingElement.selected += OnBuildingElementSelected;
            BuildingElement.deleted += OnBuildingDeleted;
        }
        private void RemoveEvents()
        {
            BuildingElement.selected -= OnBuildingElementSelected;
            BuildingElement.deleted -= OnBuildingDeleted;
        }
        #endregion
    }
}

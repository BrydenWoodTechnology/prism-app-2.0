﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraEnvironment : MonoBehaviour
{   
    Color defaultcolour;
    // Start is called before the first frame update
    void Start()
    {
        defaultcolour = GetComponent<Camera>().backgroundColor;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CameraColours(Color color)
    {       
        GetComponent<Camera>().backgroundColor = color;
    }

    public void CameraFlags(bool isOn)
    {
        if (!isOn)
        {
            GetComponent<Camera>().clearFlags = CameraClearFlags.SolidColor;
        }
        else 
        {
            GetComponent<Camera>().clearFlags = CameraClearFlags.Skybox;
        }        
    }

    public void ResetToDefaultColour()
    {
        GetComponent<Camera>().clearFlags = CameraClearFlags.SolidColor;
        GetComponent<Camera>().backgroundColor = defaultcolour;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for manipulating ui elements
    /// </summary>
    public class ManipulateUI : MonoBehaviour
    {

        private Button m_button { get; set; }

        /// <summary>
        /// Used when the ui element is selected
        /// </summary>
        public delegate void OnSelect();
        /// <summary>
        /// Triggered when the UI element is selected
        /// </summary>
        public event OnSelect selected;

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            m_button = GetComponent<Button>();
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Called when the pointer clicks the button
        /// </summary>
        public void OnClick()
        {
            m_button.interactable = false;
            if (selected != null)
            {
                selected();
            }
        }

        /// <summary>
        /// Called when the Element is deselected
        /// </summary>
        public void Deselect()
        {
            m_button.interactable = true;
        }
        #endregion
    }
}

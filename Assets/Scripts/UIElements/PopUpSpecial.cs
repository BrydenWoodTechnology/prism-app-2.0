﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for special pop-up UI elements
    /// </summary>
    public class PopUpSpecial : MonoBehaviour
    {
        #region Public Fields and Properties
        [Header("Referenced Elements")]
        [Space(5)]
        public RectTransform popper;
        public Transform popper_content;
        public RectTransform Mid_Screen;
        public GameObject loading_text;

        public Toggle popper_toggle;
        [Space(10)]
        [Header("UI Elements")]
        [Space(5)]
        [Tooltip("Define pop-up window color")]
        public Color32 color_start = new Color32();
        #endregion

        #region Private Fields and Properties
        private WaitForSeconds delay = new WaitForSeconds(0.35f);
        private WaitForSeconds delay_new = new WaitForSeconds(2f);
        private Image image { get; set; }
        private Color32 image_col { get; set; }
        private float start_a = 255;
        private float end_a = 0;
        private bool tog_state { get; set; }
        #endregion

        #region MonoBehaviour Methods
        void Awake()
        {

            if (Mid_Screen.gameObject != null)

            {
                popper.gameObject.SetActive(true);
                image = Mid_Screen.GetComponent<Image>();
                image_col = image.color;
                StartCoroutine(EaseOut(5f));
            }
            else
            {
                popper.gameObject.SetActive(false);
            }
        }
        // Use this for initialization
        void Start()
        {
            tog_state = false;
            color_start = popper.GetComponent<Image>().color;
            //Debug.Log(popper.gameObject.name+color_start.ToString());

            if (popper_content == null)
            {
                //Debug.Log("Content null");
                GameObject content = new GameObject("popper_content");
                content.transform.SetParent(popper);

                int pop_children = popper.childCount;
                for (int i = pop_children - 1; i > 0; i--)
                {
                    popper.GetChild(0).SetParent(content.transform);
                }


                popper_content = content.transform;
                popper_content.gameObject.SetActive(false);
            }

        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Starts the pop-up process
        /// </summary>
        /// <param name="mytog"></param>
        public void StartPopping(Toggle mytog)
        {
            tog_state = !tog_state;
            StartCoroutine(SmoothPop(popper, 1f, tog_state));
        }

        /// <summary>
        /// Starts the process of fading out
        /// </summary>
        /// <param name="seconds">The total time of the process</param>
        /// <returns>IEnumerator</returns>
        public IEnumerator EaseOut(float seconds)
        {
            yield return delay_new;
            loading_text.SetActive(false);
            float elapsedTime = 0;
            while (elapsedTime < seconds)
            {
                float a_now = (Mathf.Lerp(start_a, end_a, (elapsedTime / seconds)));
                image.color = new Color32(image_col.r, image_col.g, image_col.b, (byte)a_now);
                elapsedTime += 0.05f;
                if (elapsedTime >= seconds)
                {
                    image.color = new Color(image_col.r, image_col.g, image_col.b, (byte)end_a);
                    Mid_Screen.gameObject.SetActive(false);
                    popper.gameObject.SetActive(false);
                }
                yield return null;
            }
        }

        /// <summary>
        /// Starts a smooth movement
        /// </summary>
        /// <param name="pop">The Rect Transform of the element</param>
        /// <param name="seconds">The seconds required for the movement</param>
        /// <param name="activeState">On or Off</param>
        /// <returns>IEnumerator</returns>
        public IEnumerator SmoothPop(RectTransform pop, float seconds, bool activeState)
        {
            float elapsedTime = 0;

            Image image = pop.GetComponent<Image>();

            color_start = image.color;
            //Debug.Log(color_start.ToString());

            float start_a = color_start.a;
            float end_a;
            if (activeState == true)
            {
                pop.gameObject.SetActive(true);
                end_a = 220;

                while (elapsedTime < seconds)
                {
                    float a_now = (Mathf.Lerp(start_a, end_a, (elapsedTime / seconds)));
                    image.color = new Color32(color_start.r, color_start.g, color_start.b, (byte)a_now);
                    elapsedTime += 0.05f;

                    if (elapsedTime >= seconds)
                    {
                        image.color = new Color32(color_start.r, color_start.g, color_start.b, (byte)end_a);
                        popper_content.gameObject.SetActive(true);
                    }
                    yield return null;
                }
            }
            if (activeState == false)
            {
                end_a = 0;
                popper_content.gameObject.SetActive(false);

                yield return delay;
                while (elapsedTime < seconds)
                {
                    float a_now = (Mathf.Lerp(start_a, end_a, (elapsedTime / seconds)));
                    image.color = new Color32(color_start.r, color_start.g, color_start.b, (byte)a_now);
                    elapsedTime += 0.07f;

                    if (elapsedTime >= seconds)
                    {
                        image.color = new Color32(color_start.r, color_start.g, color_start.b, (byte)end_a);
                        pop.gameObject.SetActive(false);

                    }
                    yield return null;
                }
            }

        }
        #endregion
    }
}
﻿using BrydenWoodUnity.GeometryManipulation;
using BrydenWoodUnity.Lighting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// Used when the UI control element is moved
    /// </summary>
    public delegate void Moved(object sender);
    /// <summary>
    /// Used when a UI control element is selected
    /// </summary>
    /// <param name="sender"></param>
    public delegate void Selected(ControlPointUI sender);
    /// <summary>
    /// An enum with the constrain planes for the movement of scene objects by UI elements
    /// </summary>
    public enum MovePlanes
    {
        XY,
        XZ,
        YZ
    }

    /// <summary>
    /// A MonoBehaviour component Control Point UI. It can be used for moving scene objects (Inherits from UI Representation)
    /// </summary>
    public class ControlPointUI : UiRepresentation, IDragHandler, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler
    {
        #region Public Fields and Properties
        /// <summary>
        /// The type of plane to contrain the movement on (the actual plane will be parallel to this one but with its centre at the initial position of the scene object
        /// </summary>
        [Tooltip("The type of plane to contrain the movement on (the actual plane will be parallel to this one but with its centre at the initial position of the scene object")]
        public MovePlanes movePlane = MovePlanes.XZ;
        public Vector3 axis = Vector3.zero;
        public bool axisConstrain = false;

        /// <summary>
        /// The Image component of the GUI object
        /// </summary>
        [Tooltip("The Image component of the GUI object")]
        public Image image;
        /// <summary>
        /// The original color of the Image
        /// </summary>
        [Tooltip("The original color of the Image")]
        public Color original;
        /// <summary>
        /// The color to be used when the mouse is over the element
        /// </summary>
        [Tooltip("The color to be used when the mouse is over the element")]
        public Color mouseOver;
        /// <summary>
        /// The color to be used when the element is clicked
        /// </summary>
        [Tooltip("The color to be used when the element is clicked")]
        public Color selected;

        [HideInInspector]
        public List<Vector3> initialDiffs;
        public List<GeometryVertex> axisVertices { get; set; }

        public bool allignWhenShift { get; set; }

        public bool belongsToSite { get; set; }
        #endregion

        #region Private Fields
        private Plane plane;
        private Vector3 initPosition;
        private Vector3 initMousePosition;
        private List<Vector3> initObjectPos;
        #endregion

        #region Events
        /// <summary>
        /// Triggered when the element is moved
        /// </summary>
        public event Moved moved;
        /// <summary>
        /// Triggeres when the element stops moving
        /// </summary>
        public event Moved stoppedMoving;
        /// <summary>
        /// Triggered when the element is selected
        /// </summary>
        public event Selected onSelected;
        #endregion

        #region MonoBehaviour Methods
        private void Start()
        {
            /*
            if (sceneObject != null)
            {
                Initialize(sceneObject);
            }
            */
        }

        void Update()
        {
            if (prevCameraPosition != Camera.main.transform.position || prevOrthoSize != Camera.main.orthographicSize)
            {
                UpdateUIPosition();
            }
            prevCameraPosition = Camera.main.transform.position;
            prevOrthoSize = Camera.main.orthographicSize;
            if (allignWhenShift)
            {
                axisConstrain = Input.GetKey(KeyCode.LeftShift);
            }
        }

        private void OnDestroy()
        {
            if (sceneObjects != null && sceneObjects.Count > 0)
            {
                for (int i = 0; i < sceneObjects.Count; i++)
                {
                    if (sceneObjects[i] != null && sceneObjects[i].GetComponent<PolygonVertex>() != null)
                    {
                        moved -= sceneObjects[i].GetComponent<PolygonVertex>().OnMoved;
                    }
                }
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Initiates the instance
        /// </summary>
        /// <param name="sceneObjects">A list of gameObjects in scene to be moved by this control point</param>
        public void Initialize(List<Transform> sceneObjects)
        {
            this.sceneObjects = sceneObjects;
            initialDiffs = new List<Vector3>();
            for (int i = 0; i < sceneObjects.Count; i++)
            {
                initialDiffs.Add(sceneObjects[i].position - sceneObjects[0].position);
            }
            UpdateUIPosition();
            switch (movePlane)
            {
                case MovePlanes.XY:
                    plane = new Plane(new Vector3(0, 0, 1), sceneObjects[0].position);
                    break;
                case MovePlanes.XZ:
                    plane = new Plane(new Vector3(0, 1, 0), sceneObjects[0].position);
                    break;
                case MovePlanes.YZ:
                    plane = new Plane(new Vector3(1, 0, 0), sceneObjects[0].position);
                    break;
            }
            hasInitialized = true;
        }

        /// <summary>
        /// Initializes the instance
        /// </summary>
        /// <param name="singleObject">A single gameObject in scene to be moved by this control point</param>
        public override void Initialize(Transform singleObject)
        {
            if (sceneObjects == null || (sceneObjects.Count > 0 && sceneObjects[0] == null))
            {
                sceneObjects = new List<Transform>();
            }
            if (initialDiffs == null)
            {
                initialDiffs = new List<Vector3>();
            }
            sceneObjects.Add(singleObject);
            initialDiffs.Add(singleObject.position - sceneObjects[0].position);
            UpdateUIPosition();
            switch (movePlane)
            {
                case MovePlanes.XY:
                    plane = new Plane(new Vector3(0, 0, 1), sceneObjects[0].position);
                    break;
                case MovePlanes.XZ:
                    plane = new Plane(new Vector3(0, 1, 0), sceneObjects[0].position);
                    break;
                case MovePlanes.YZ:
                    plane = new Plane(new Vector3(1, 0, 0), sceneObjects[0].position);
                    break;
            }
            hasInitialized = true;
            GetComponent<Image>().enabled = true;
        }

        public override void UpdateUIPosition()
        {
            if (hasInitialized)
            {
                if (sceneObjects[0] == null)
                {
                    Destroy(gameObject);
                    return;
                }
                else
                {
                    GetComponent<RectTransform>().position = Camera.main.WorldToScreenPoint(sceneObjects[0].position);
                }
            }
        }

        /// <summary>
        /// Updates the screen position of the UI element
        /// </summary>
        /// <param name="sender">The transform which gives the new position</param>
        public void UpdatePositions(Transform sender)
        {
            for (int i = 0; i < sceneObjects.Count; i++)
            {
                if (sceneObjects[i] != sender && i != 0)
                {
                    sceneObjects[i].position = sender.position + initialDiffs[i];
                    if (sceneObjects[i].GetComponent<PolygonVertex>() != null)
                    {
                        sceneObjects[i].GetComponent<PolygonVertex>().OnMoved();
                    }
                    if (sceneObjects[i].GetComponent<MeshVertex>() != null)
                    {
                        sceneObjects[i].GetComponent<MeshVertex>().OnMoved();
                    }
                }
                else if (sceneObjects[i] != sender && i == 0)
                {
                    int senderIndex = -1;
                    for (int j = 0; j < sceneObjects.Count; j++)
                    {
                        if (sender == sceneObjects[j])
                        {
                            senderIndex = j;
                        }
                    }

                    if (senderIndex != -1)
                    {
                        sceneObjects[i].position = sender.position - initialDiffs[senderIndex];
                        if (sceneObjects[i].GetComponent<PolygonVertex>() != null)
                        {
                            sceneObjects[i].GetComponent<PolygonVertex>().OnMoved();
                        }
                        if (sceneObjects[i].GetComponent<MeshVertex>() != null)
                        {
                            sceneObjects[i].GetComponent<MeshVertex>().OnMoved();
                        }
                    }

                }
            }
        }

        /// <summary>
        /// Destroys the UI element
        /// </summary>
        /// <param name="sceneObject"></param>
        public override void DestroyUIRep(Transform sceneObject)
        {
            base.DestroyUIRep(sceneObject);
            StartCoroutine(UpdateDiffs());
        }

        /// <summary>
        /// Adds a scene object which will be moved by this element
        /// </summary>
        /// <param name="transform">The transform to be added</param>
        public override void AddSceneObject(Transform transform)
        {
            sceneObjects.Add(transform);
            initialDiffs.Add(transform.position - sceneObjects[0].position);
        }

        /// <summary>
        /// Updates the initial position differences between the controlled scene objects
        /// </summary>
        public void UpdateInitDiffs()
        {
            initialDiffs = new List<Vector3>();
            for (int i = 0; i < sceneObjects.Count; i++)
            {
                if (sceneObjects[i] != null)
                    initialDiffs.Add(sceneObjects[i].position - sceneObjects[0].position);
            }
            BrydenWoodUtils.ClearNull<Transform>(ref sceneObjects);
        }

        /// <summary>
        /// Called when the element is being dragged
        /// </summary>
        /// <param name="eventData"></param>
        public void OnDrag(PointerEventData eventData)
        {
            if (eventData.button == PointerEventData.InputButton.Left)
            {
                if (allignWhenShift && axisVertices != null && axisVertices.Count == 2 && axisConstrain)
                {
                    axis = axisVertices[1].currentPosition - axisVertices[0].currentPosition;
                }

                var dif = Input.mousePosition - initMousePosition;
                GetComponent<RectTransform>().position = initPosition + dif;
                Ray r = Camera.main.ScreenPointToRay(initPosition + dif);
                float dist = 0;
                if (plane.Raycast(r, out dist))
                {
                    if (axisConstrain && axis != Vector3.zero)
                    {
                        for (int i = 0; i < sceneObjects.Count; i++)
                        {
                            var moveVec = r.GetPoint(dist) - initObjectPos[i];
                            sceneObjects[i].position = initObjectPos[i] + Vector3.Project(moveVec, axis);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < sceneObjects.Count; i++)
                        {
                            sceneObjects[i].position = r.GetPoint(dist) + initialDiffs[i];
                        }
                    }
                }


                if (moved != null)
                {
                    moved(this);
                }

                image.color = selected;
            }
        }

        /// <summary>
        /// Toggles the axis constraint of the movement
        /// </summary>
        public void ToggleAxisConstraint()
        {
            axisConstrain = !axisConstrain;
        }

        /// <summary>
        /// Called when the pointer is down on top of the element
        /// </summary>
        /// <param name="eventData"></param>
        public void OnPointerDown(PointerEventData eventData)
        {
            if (eventData.button == PointerEventData.InputButton.Left)
            {
                if (axisVertices != null && axisVertices.Count == 2 && axisConstrain)
                {
                    axis = axisVertices[1].currentPosition - axisVertices[0].currentPosition;
                }
            }
            initObjectPos = new List<Vector3>();
            initMousePosition = Input.mousePosition;
            initPosition = GetComponent<RectTransform>().position;
            transform.SetAsLastSibling();
            plane = new Plane(Vector3.up, sceneObjects[0].transform.position);
            for (int i = 0; i < sceneObjects.Count; i++)
            {
                initObjectPos.Add(sceneObjects[i].transform.position);
            }
            if (onSelected != null)
            {
                onSelected(this);
            }
        }

        /// <summary>
        /// Called when the pointer is up on top of the element
        /// </summary>
        /// <param name="eventData"></param>
        public void OnPointerUp(PointerEventData eventData)
        {
            if (stoppedMoving != null)
            {
                stoppedMoving(this);
            }
            UpdateUIPosition();
        }

        /// <summary>
        /// Called when the pointer enters the element
        /// </summary>
        /// <param name="eventData"></param>
        public void OnPointerEnter(PointerEventData eventData)
        {
            image.color = mouseOver;
        }

        /// <summary>
        /// Called when the pointer exits the element
        /// </summary>
        /// <param name="eventData"></param>
        public void OnPointerExit(PointerEventData eventData)
        {
            image.color = original;
        }

        
        #endregion

        #region Private Methods
        private IEnumerator UpdateDiffs()
        {
            yield return new WaitForEndOfFrame();
            UpdateInitDiffs();
        }
        #endregion
    }
}

﻿using BrydenWoodUnity.DesignData;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour Singleton for prompting the user to refresh the selected building
    /// </summary>
    public class RefreshBuildingPopUp : Tagged<RefreshBuildingPopUp>
    {
        public ProceduralBuildingManager buildingManager;
        public List<KeyValuePair<ICancelable, string[]>> changedValues = new List<KeyValuePair<ICancelable, string[]>>();
     

        #region MonoBehaviour Methods
        void Start()
        {
            //changedValues = new Dictionary<ICancelable, string[]>();
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Called when the new values should be submitted and the building refreshed
        /// </summary>
        public void SubmitValues()
        {
            try
            {
                foreach (var item in changedValues)
                {
                    item.Key.SubmitValue(item.Value[1]);
                }
                changedValues.Clear();
            }
            catch(Exception e)
            {
                Debug.Log(e);
            }
            GetComponent<MovePanelOnCanvas>().OnToggle(false);
        }

        /// <summary>
        /// Called when the new values should be discarded
        /// </summary>
        public void CancelValues()
        {
            foreach (var item in changedValues)
            {
                item.Key.ResetValue(item.Value[0]);
            }
            changedValues.Clear();
            GetComponent<MovePanelOnCanvas>().OnToggle(false);
        }

        /// <summary>
        /// Adds a value to the list of updated values
        /// </summary>
        /// <param name="sender">The object with the updated value</param>
        /// <param name="value">The new and the old values</param>
        public void AddChangedValue(ICancelable sender, string[] value)
        {
            if (buildingManager.proceduralBuildings[buildingManager.currentBuildingIndex].buildingType == BuildingType.Tower)
            {
                if (buildingManager.proceduralBuildings[buildingManager.currentBuildingIndex].floors.Count > 0)
                {
                    if (buildingManager.proceduralBuildings[buildingManager.currentBuildingIndex].hasGeometry)
                    {
                        GetComponent<MovePanelOnCanvas>().OnToggle(true);
                        changedValues.Add(new KeyValuePair<ICancelable, string[]>(sender, value));
                    }
                    else
                    {
                        changedValues.Add(new KeyValuePair<ICancelable, string[]>(sender, value));
                        SubmitValues();
                    }
                }
                else
                {
                    changedValues.Add(new KeyValuePair<ICancelable, string[]>(sender, value));
                    SubmitValues();
                }
            }
            else
            {
                if (buildingManager.proceduralBuildings[buildingManager.currentBuildingIndex].floors.Count > 0)
                {
                    if (buildingManager.proceduralBuildings[buildingManager.currentBuildingIndex].currentFloor.hasGeometry)
                    {
                        GetComponent<MovePanelOnCanvas>().OnToggle(true);
                        changedValues.Add(new KeyValuePair<ICancelable, string[]>(sender, value));
                    }
                    else
                    {
                        changedValues.Add(new KeyValuePair<ICancelable, string[]>(sender, value));
                        SubmitValues();
                    }
                }
                else
                {
                    changedValues.Add(new KeyValuePair<ICancelable, string[]>(sender, value));
                    SubmitValues();
                }
            }
        }
        #endregion
    }
}

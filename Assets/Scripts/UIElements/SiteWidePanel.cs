﻿using BrydenWoodUnity.DesignData;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    public class SiteWidePanel : MonoBehaviour
    {
        public SiteWideBriefPanel briefPanel;
        public InputField floorInputField;
        public InputField floorHeightInputField;
        public InputField offsetInputField;
        public OnBriefChanged onBriedChanged;

        // Use this for initialization
        void Start()
        {
            if (onBriedChanged == null)
                onBriedChanged = new OnBriefChanged();
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void DeletePodium()
        {
            floorInputField.SetValue(string.Empty);
            floorHeightInputField.SetValue(string.Empty);
            offsetInputField.SetValue(string.Empty);
            briefPanel.ClearPrevious();
        }

        public void DeleteBasement()
        {
            floorInputField.SetValue(string.Empty);
            floorHeightInputField.SetValue(string.Empty);
            offsetInputField.SetValue(string.Empty);
            briefPanel.ClearPrevious();
        }

        public void OnBriefChanged()
        {
            if (briefPanel != null)
            {
                onBriedChanged.Invoke(new BriefChangedEventArgs
                {
                    index = -1,
                    use = briefPanel.GetPercentages()
                });
            }
        }

        public void SetValues(int floors, float floorHeight, float offset)
        {
            floorInputField.text = floors.ToString();
            floorHeightInputField.text = floorHeight.ToString();
            offsetInputField.text = offset.ToString();
        }

        public void SetBrief(Dictionary<string, float> percentages)
        {
            foreach (var item in percentages)
            {
                briefPanel.AddBriefElement(item.Key, item.Value);
            }
        }

        public Dictionary<string,float> GetPercentages()
        {
            return briefPanel.GetPercentages();
        }
    }

    [Serializable]
    public class OnBriefChanged : UnityEvent<BriefChangedEventArgs>
    {

    }

    public struct BriefChangedEventArgs
    {
        public int index;
        public Dictionary<string, float> use;
    }
}

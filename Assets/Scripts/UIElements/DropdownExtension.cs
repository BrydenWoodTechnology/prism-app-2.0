﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// Extends the Dropdown class from Unity
    /// </summary>
    public class DropdownExtension : MonoBehaviour, IPointerClickHandler
    {
        #region Events
        /// <summary>
        /// Used when the input field is selected
        /// </summary>
        /// <param name="inputfield">The selected input field</param>
        public delegate void OnSelected(Dropdown dropdown);
        /// <summary>
        /// Triggered when the input field is selected
        /// </summary>
        public event OnSelected selected;
        #endregion

        private void OnDestroy()
        {
            if (selected != null)
            {
                foreach (Delegate eh in selected.GetInvocationList())
                {
                    selected -= (OnSelected)eh;
                }
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (selected != null)
            {
                selected(GetComponent<Dropdown>());
            }
        }
    }
}

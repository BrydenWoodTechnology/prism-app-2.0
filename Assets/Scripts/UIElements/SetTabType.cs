﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component to set the type of different ribbon tabs
    /// </summary>
    public class SetTabType : MonoBehaviour
    {
        #region Public Fields and Properties
        public GameObject[] tab_types;
        public RectTransform tab_container;
        public TabTypes tabType = TabTypes.PopUp;

        [HideInInspector]
        public int tabTypeIndex = 0;
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            //TabSelecttor(1);
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Selects a tab to chenge its type
        /// </summary>
        /// <param name="tab_num">The index of the selected tab</param>
        public void TabSelecttor(int tab_num)
        {
            if (tab_container.childCount != 0)
            {
                DestroyImmediate(tab_container.GetChild(0).gameObject);
            }
            GameObject new_tab_type = Instantiate(tab_types[tab_num]);
            new_tab_type.name = tab_types[tab_num].name;
            new_tab_type.transform.SetParent(tab_container);
            new_tab_type.transform.localScale = new Vector3(1, 1, 1);
            new_tab_type.transform.localPosition = Vector3.zero;
        }
        #endregion
    }

    /// <summary>
    /// Different types of UI Ribbon tabs
    /// </summary>
    public enum TabTypes
    {
        Sliding = 1,
        PopUp = 0
    }
}

﻿using BrydenWoodUnity.DesignData;
using BrydenWoodUnity.GeometryManipulation;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BrydenWoodUnity
{
    /// <summary>
    /// A MonoBehaviour component for selecting objects
    /// </summary>
    public class Selector : Tagged<Selector>
    {
        #region Public Fields and Properties
        [Tooltip("The layer mask to be used for Raycasting")]
        [SerializeField]
        private LayerMask selectablesLayermask;
        [SerializeField]
        private ProceduralBuildingManager buildingManager;
        //[SerializeField]
        //private float distThreshold;
        //[SerializeField]
        //private RectTransform selectRectangle;
        //[SerializeField]
        //private Transform selectables;

        public List<GameObject> selectedObjects;
        public List<BaseDesignData> selected { get; set; }
        #endregion

        #region Private Fields and Properties
        private Camera cam;
        //private bool enableRectSelect;
        //private Vector3 originalPos;
        //private Vector3 deltaMouse;
        private bool deleteCores;
        #endregion

        #region Events
        /// <summary>
        /// Used when the selection is canceled
        /// </summary>
        public delegate void OnDeselect();
        /// <summary>
        /// Triggered when the selection is canceled
        /// </summary>
        public static event OnDeselect deselect;

        /// <summary>
        /// Used when multiple objects are selected
        /// </summary>
        /// <param name="designData">The list of selected objects</param>
        public delegate void OnSelected2(List<BaseDesignData> designData);
        /// <summary>
        /// Triggered when multiple objects are selected
        /// </summary>
        public static event OnSelected2 selected2;

        /// <summary>
        /// Used when a single object is selected
        /// </summary>
        /// <param name="designData">The selected object</param>
        public delegate void OnSelected(BaseDesignData designData);
        /// <summary>
        /// Triggered when a single object is selected
        /// </summary>
        public static event OnSelected selected1;
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            BaseDesignData.selected += SetSelected;
            BaseDesignData.deselected += RemoveSelected;
            if (buildingManager != null)
            {
                buildingManager.clearSelect += ClearSelect;
            }
        }

        private void OnDestroy()
        {
            foreach (Delegate eh in deselect.GetInvocationList())
            {
                deselect -= (OnDeselect)eh;
            }
            foreach (Delegate eh in selected2.GetInvocationList())
            {
                selected2 -= (OnSelected2)eh;
            }
            if (selected1 != null)
            {
                foreach (Delegate eh in selected1.GetInvocationList())
                {
                    selected1 -= (OnSelected)eh;
                }
            }
        }

        // Update is called once per frame
        void Update()
        {

            if (!EventSystem.current.IsPointerOverGameObject(-1))
            {
                if (Input.GetMouseButtonDown(0) && !(Input.GetKey(KeyCode.LeftShift) || (Input.GetKey(KeyCode.RightShift))) && !(Input.GetKey(KeyCode.LeftControl) || (Input.GetKey(KeyCode.RightControl))))
                {
                    ClearSelect();
                    cam = Camera.main;
                    Ray r = cam.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    if (Physics.Raycast(r, out hit, 5000, selectablesLayermask))
                    {
                        var obj = hit.collider.gameObject;
                        if (obj.layer == 20)
                        {
                            MoveObjects.TaggedObject.SetObjectToMove(obj.transform, r);
                        }
                        else
                        {
                            if (selected == null)
                            {
                                obj.SendMessage("Select");
                            }
                            else
                            {
                                obj.SendMessage("Select");
                            }
                        }
                    }
                    else
                    {
                        ClearSelect();
                        //originalPos = Input.mousePosition;
                        //enableRectSelect = true;
                        //selectedObjects = new List<GameObject>();
                    }
                }
                else if (Input.GetMouseButtonDown(0) && (Input.GetKey(KeyCode.LeftShift) || (Input.GetKey(KeyCode.RightShift))) && !(Input.GetKey(KeyCode.LeftControl) || (Input.GetKey(KeyCode.RightControl))))
                {
                    cam = Camera.main;
                    Ray r = cam.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    if (Physics.Raycast(r, out hit, 5000, selectablesLayermask))
                    {
                        var obj = hit.collider.gameObject;
                        if (selected == null)
                        {
                            obj.SendMessage("Select");
                        }
                        else
                        {
                            obj.SendMessage("Select");
                        }
                    }
                }
                else if (Input.GetMouseButtonDown(0) && (Input.GetKey(KeyCode.LeftControl) || (Input.GetKey(KeyCode.LeftControl))))
                {
                    cam = Camera.main;
                    Ray r = cam.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    if (Physics.Raycast(r, out hit, 5000, selectablesLayermask))
                    {
                        var obj = hit.collider.gameObject;
                        if (selected != null)
                        {
                            obj.SendMessage("DeSelect");
                        }
                    }
                }

                if (deleteCores)
                {
                    if (Input.GetKeyDown(KeyCode.Delete))
                    {
                        MoveObjects.TaggedObject.DeleteObject();
                    }
                }

                //if (Input.GetMouseButton(0) && enableRectSelect)
                //{
                //    deltaMouse = Input.mousePosition - originalPos;
                //    Debug.Log(deltaMouse.magnitude);
                //    if (deltaMouse.magnitude > distThreshold)
                //    {
                //        selectRectangle.gameObject.SetActive(true);
                //        selectRectangle.position = originalPos + deltaMouse / 2;
                //        selectRectangle.sizeDelta = new Vector2(Mathf.Abs(deltaMouse.x), Mathf.Abs(deltaMouse.y));
                //    }
                //}

                //if (Input.GetMouseButtonUp(0))
                //{
                //    if (selectRectangle.sizeDelta != Vector2.zero)
                //    {
                //        for (int i = 0; i < selectables.childCount; i++)
                //        {
                //            var obj = selectables.GetChild(i);

                //            var screenPoint = cam.WorldToScreenPoint(obj.position);
                //            var rect = new Rect(Vector3.zero,selectRectangle.sizeDelta);
                //            rect.center = originalPos + deltaMouse / 2;
                //            if (rect.Contains(screenPoint))
                //            {
                //                obj.gameObject.SendMessage("Select");
                //                selectedObjects.Add(obj.gameObject);
                //            }
                //        }
                //    }
                //    enableRectSelect = false;
                //    selectRectangle.gameObject.SetActive(false);
                //    selectRectangle.sizeDelta = Vector2.zero;
                //}
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Clears all selections
        /// </summary>
        public void ClearSelect()
        {
            if (selected != null)
            {
                foreach (var item in selected)
                {
                    item.DeSelect(false);
                }
                selected = null;
                if (deselect != null)
                {
                    deselect();
                }
            }
        }

        public void ToggleCoreSelection(bool allow)
        {
            if (allow)
            {
                selectablesLayermask = selectablesLayermask | (1 << 20);
            }
            else
            {
                selectablesLayermask = selectablesLayermask & ~(1 << 20);
            }
        }

        public void ToggleCoreDelete(bool allow)
        {
            if (allow)
            {
                selectablesLayermask = selectablesLayermask | (1 << 20);
            }
            else
            {
                selectablesLayermask = selectablesLayermask & ~(1 << 20);
            }
            MoveObjects.TaggedObject.deleteObject = allow;
            deleteCores = allow;
        }
        #endregion

        #region Private Methods
        private void SetSelected(BaseDesignData sender)
        {
            if (selected == null)
            {
                selected = new List<BaseDesignData>();
            }
            if (!selected.Contains(sender))
            {
                selected.Add(sender);
                if (selected.Count >= 2)
                {
                    if (selected2 != null)
                    {
                        selected2(selected);
                    }
                }
                else if (selected.Count == 1)
                {
                    var apt = selected[0] as ApartmentUnity;
                    if (apt != null)
                    {
                        if (selected1 != null)
                        {
                            selected1(selected[0]);
                        }
                    }
                }
            }
        }

        private void RemoveSelected(BaseDesignData sender)
        {
            if (selected.Contains(sender))
            {
                selected.Remove(sender);
                if (selected.Count >= 2)
                {
                    if (selected2 != null)
                    {
                        selected2(selected);
                    }
                }
                else if (selected.Count == 1)
                {
                    var apt = selected[0] as ApartmentUnity;
                    if (apt != null)
                    {
                        if (selected1 != null)
                        {
                            selected1(selected[0]);
                        }
                    }
                }
            }
        }
        #endregion
    }
}

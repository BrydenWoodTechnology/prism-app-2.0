﻿using BrydenWoodUnity.GeometryManipulation;
using BrydenWoodUnity.DesignData;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for managing error messages
    /// </summary>
    public class ErrorPopUp : MonoBehaviour
    {
        #region Public Fields and Properties
        [Header("Scene References:")]
        public Text body;
        public Transform errorParent;
        public Button autoResolveButton;
        public RectTransform errorHoverMessage;
        public ProceduralBuildingManager buildingManager;

        [Header("User Input:")]
        public Color errorColor = Color.red;
        public Color warningColor = Color.yellow;

        [Header("Events:")]
        public NotificationsEvent OnNotificationsUpdate;
        #endregion

        #region Private Fields and Properties
        private Dictionary<IConstrainable, Text> errors { get; set; }
        private Dictionary<IConstrainable, string> prevStates { get; set; }
        private bool updated = false;
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Awake()
        {
            body.color = errorColor;
            AddEvents();
            errors = new Dictionary<IConstrainable, Text>();
        }

        // Update is called once per frame
        void LateUpdate()
        {

            if (updated)
            {
                StartCoroutine(UpdateList());
            }
        }

        private void OnDisable()
        {
            RemoveEvents();
        }

        private void OnDestroy()
        {
            RemoveEvents();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Called when the constraints evaluation of a Design Object has changed
        /// </summary>
        /// <param name="sender">The Design Object</param>
        /// <param name="error">Whether there is an error</param>
        public void ConstraintsChanged(IConstrainable sender, bool error)
        {
            if (error)
            {
                if (errors.ContainsKey(sender))
                {
                    var notification = sender.GetNotification();
                    errors[sender].text = notification.Split(':')[0];
                }
                else
                {
                    GameObject gObj = Instantiate(body.transform.parent.gameObject, errorParent);
                    //RectTransform recTr = gObj.GetComponent<RectTransform>();
                    //Vector3 newPos = recTr.position + new Vector3(0, -recTr.rect.height * errors.Keys.Count, 0);
                    //recTr.position = newPos;
                    var notification = sender.GetNotification();
                    gObj.GetComponentInChildren<Text>().text = notification.Split(':')[0];

                    errors.Add(sender, gObj.GetComponentInChildren<Text>());

                    updated = true;
                }
            }
            else
            {
                if (errors.ContainsKey(sender))
                {
                    StartCoroutine(RemoveMesssage(sender));
                }
            }
        }

        /// <summary>
        /// Highlights the object which corresponds to the error message
        /// </summary>
        /// <param name="message">The error message</param>
        /// <param name="show">Toggles the highlight</param>
        public void HighlightMessageObject(Text message, bool show)
        {
            if (show)
            {

                foreach (var item in errors)
                {
                    if (item.Value == message)
                    {
                        errorHoverMessage.gameObject.SetActive(true);
                        //errorHoverMessage.position = new Vector3(errorHoverMessage.position.x, message.rectTransform.position.y, errorHoverMessage.position.z);
                        errorHoverMessage.GetChild(0).GetComponent<Text>().text = item.Key.GetNotification();
                        UpdateInfoHeight(errorHoverMessage, errorHoverMessage.GetChild(0).GetComponent<Text>(), 5);
                        item.Key.HighlightErrorObject(show);
                        break;
                    }
                }
            }
            else
            {
                errorHoverMessage.gameObject.SetActive(false);
                foreach (var item in errors)
                {
                    if (item.Value == message)
                    {
                        item.Key.HighlightErrorObject(show);
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Called when the auto-resolve button is used
        /// </summary>
        public void OnAutoResolve()
        {
            prevStates = new Dictionary<IConstrainable, string>();
            foreach (var item in errors)
            {
                ApartmentUnity apt = item.Key as ApartmentUnity;
                prevStates.Add(apt, apt.ApartmentType);
                double dist = double.MaxValue;
                string m_type = "";
                if (apt != null)
                {
                    foreach (var type in Standards.TaggedObject.ApartmentTypesMinimumSizes)
                    {
                        double di = apt.Area - type.Value;
                        if (di < dist && di >= 0)
                        {
                            dist = di;
                            m_type = type.Key;
                        }
                    }
                    if (m_type == "")
                    {
                        m_type = "1b1p";
                    }
                    apt.OnApartmentTypeChanged(m_type, apt.isM3);
                }
            }
        }

        /// <summary>
        /// Called when the undo button is used
        /// </summary>
        public void OnUndo()
        {
            if (prevStates != null)
            {
                foreach (var item in prevStates)
                {
                    (item.Key as ApartmentUnity).OnApartmentTypeChanged(item.Value,(item.Key as ApartmentUnity).isM3);
                }
            }
        }
        #endregion

        #region Private Methods

        private void AddEvents()
        {
            BaseDesignData.constraintsChanged += ConstraintsChanged;
            ErrorText.hoverOver += HighlightMessageObject;
            ErrorText.click += OnErrorClick;
        }

        private void RemoveEvents()
        {
            BaseDesignData.constraintsChanged -= ConstraintsChanged;
            ErrorText.hoverOver -= HighlightMessageObject;
            ErrorText.click -= OnErrorClick;
        }

        private IEnumerator UpdateList()
        {
            yield return new WaitForEndOfFrame();
            var rec0 = body.GetComponent<RectTransform>();
            Vector2 vec = errorParent.GetComponent<RectTransform>().sizeDelta;
            errorParent.GetComponent<RectTransform>().sizeDelta = new Vector2(vec.x, rec0.rect.height * errorParent.childCount);
            for (int i = 1; i < errorParent.childCount; i++)
            {
                var rec = errorParent.GetChild(i).GetComponent<RectTransform>();
                rec.localPosition = rec0.localPosition + new Vector3(0, -rec.rect.height * (i - 1), 0);
            }
            updated = false;
            if (OnNotificationsUpdate != null)
            {
                OnNotificationsUpdate.Invoke(errorParent.childCount != 1);
            }
        }

        private IEnumerator RemoveMesssage(IConstrainable sender)
        {
            Destroy(errors[sender].gameObject);
            yield return new WaitForEndOfFrame();
            errors.Remove(sender);
            updated = true;
        }

        private void OnModelToggled()
        {
            errors = new Dictionary<IConstrainable, Text>();
            StartCoroutine(ClearMessages());
        }

        private void ShowError(bool show)
        {
            //title.gameObject.SetActive(show);
            body.gameObject.SetActive(show);
            GetComponent<Image>().enabled = show;
        }

        private IEnumerator ClearMessages()
        {
            for (int i = 1; i < errorParent.childCount; i++)
            {
                Destroy(errorParent.GetChild(i).gameObject);
            }
            yield return new WaitForEndOfFrame();
        }

        private void ReorganizeList()
        {
            var counter = 0;
            foreach (var item in errors)
            {
                RectTransform recTr = item.Value.GetComponent<RectTransform>();
                recTr.localPosition += new Vector3(0, recTr.rect.height * counter, 0);
                counter++;
            }
        }

        private void OnErrorClick(Text sender, bool show)
        {
            if (buildingManager.previewMode == PreviewMode.Floors)
            {
                foreach (var item in errors)
                {
                    if (item.Value == sender)
                    {
                        item.Key.ErrorSelect();
                    }
                }
            }
        }

        private void UpdateInfoHeight(RectTransform container, Text info, float offset)
        {
            float lineHeight = BrydenWoodUtils.CalculateLineHeight(info) / (info.transform.root.localScale.y);
            var lines = info.text.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            int linesNum = info.text.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries).Length;
            var originalSize = container.sizeDelta;
            var newY = lineHeight * linesNum;
            container.sizeDelta = new Vector2(originalSize.x, lineHeight * linesNum + offset * 2);
        }
        #endregion
    }
}

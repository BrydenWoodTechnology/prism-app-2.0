﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeUIcolours : MonoBehaviour
{
    [Header("Colour Settings")]
    [Header("Toggles and buttons")]
    public ColorBlock colorBlock;    
    [Header("Scrollbars")]
    public ColorBlock scrollViewColorBlock;
    public Color textColour;
    public Font font;
    [Header("Dependant Objects")]
    public GameObject canvas;

    private Sprite sprite;
    public void Run(GameObject _canvas)
    {
        //Debug.Log(_canvas.transform.childCount);
        for(int i = 0; i<_canvas.transform.childCount; i++)
        {
            Transform child = _canvas.transform.GetChild(i);
            

            if (child.GetComponent<Button>() != null)
            {
                child.GetComponent<Button>().colors = colorBlock;
                if (child.GetComponentInChildren<Text>() != null)
                {
                    child.GetComponentInChildren<Text>().color = textColour;
                    child.GetComponentInChildren<Text>().font = font;
                }
                if (child.GetComponent<Image>() != null && child.name != "Arrow")
                {
                    child.GetComponent<Image>().sprite = sprite;
                    if (child.name == "Checkmark")
                    {
                        child.GetComponent<Image>().color = colorBlock.highlightedColor;
                    }
                    else
                    {
                        child.GetComponent<Image>().color = Color.white;
                    }

                }
            }
            else if (child.GetComponent<Toggle>() != null)
            {
                child.GetComponent<Toggle>().colors = colorBlock;
                if (child.GetComponentInChildren<Text>() != null)
                {
                    child.GetComponentInChildren<Text>().color = textColour;
                    child.GetComponentInChildren<Text>().font = font;
                }
                if (child.GetComponent<Image>() != null && child.name != "Arrow")
                {
                    child.GetComponent<Image>().sprite = sprite;
                    if (child.name == "Checkmark")
                    {
                        child.GetComponent<Image>().color = colorBlock.highlightedColor;
                    }
                    else
                    {
                        child.GetComponent<Image>().color = Color.white;
                    }

                }
            }
            else if (child.GetComponent<InputField>() != null)
            {
                child.GetComponent<InputField>().colors = colorBlock;
                child.GetComponent<InputField>().textComponent.color = textColour;
                child.GetComponent<InputField>().textComponent.font = font;
                child.GetComponent<InputField>().placeholder.color = textColour;
                

            }
            else if (child.GetComponent<Dropdown>() != null)
            {
                child.GetComponent<Dropdown>().colors = colorBlock;
                if (child.GetComponentInChildren<Text>() != null)
                {
                    child.GetComponentInChildren<Text>().color = textColour;
                }
                if (child.GetComponent<Image>() != null && child.name != "Arrow")
                {
                    child.GetComponent<Image>().sprite = sprite;
                    if (child.name == "Checkmark")
                    {
                        child.GetComponent<Image>().color = colorBlock.highlightedColor;
                    }
                    else
                    {
                        child.GetComponent<Image>().color = Color.white;
                    }

                }
            }
            else if (child.GetComponent<Text>() != null)
            {
                child.GetComponent<Text>().color = textColour;
                child.GetComponentInChildren<Text>().font = font;
            }
            else if (child.GetComponent<Scrollbar>() != null)
            {
                child.GetComponent<Scrollbar>().colors = scrollViewColorBlock;
                if (child.GetComponent<Image>() != null && child.name != "Arrow")
                {
                    child.GetComponent<Image>().sprite = sprite;
                    if (child.name == "Checkmark")
                    {
                        child.GetComponent<Image>().color = colorBlock.highlightedColor;
                    }
                    else
                    {
                        child.GetComponent<Image>().color = Color.white;
                    }

                }
            }

            if (child.GetComponent<Image>() != null && child.name != "Arrow" && (child.GetComponentInParent<Toggle>() != null || child.parent.name=="SlidingArea"))
            {
                child.GetComponent<Image>().sprite = sprite;
                if (child.name == "Checkmark")
                {
                    child.GetComponent<Image>().color = colorBlock.highlightedColor;
                }
                else
                {
                    child.GetComponent<Image>().color = Color.white;
                }

            }

            if (child.name.Contains("Separator"))
            {
                child.gameObject.SetActive(false);
            }
            if (child.GetComponent<Outline>() != null)
            {
                child.GetComponent<Outline>().enabled = false;
            }

            if (child.childCount > 0)
            {
                Run(child.gameObject);
            }
        }
        
    }

    public void ChangeFont(GameObject _canvas)
    {
        //Debug.Log(_canvas.transform.childCount);
        for (int i = 0; i < _canvas.transform.childCount; i++)
        {
            Transform child = _canvas.transform.GetChild(i);

            if (child.GetComponentInChildren<Text>() != null)
            {
                //child.GetComponentInChildren<Text>().color = textColour;
                child.GetComponentInChildren<Text>().font = font;
                child.GetComponentInChildren<Text>().fontSize = 14;
            }

           if (child.GetComponent<InputField>() != null)
            {
                //child.GetComponent<InputField>().colors = colorBlock;
                //child.GetComponent<InputField>().textComponent.color = textColour;
                child.GetComponent<InputField>().textComponent.font = font;
                child.GetComponent<InputField>().textComponent.fontSize = 14;
                // child.GetComponent<InputField>().placeholder.color = textColour;


            }
            else if (child.GetComponent<Dropdown>() != null)
            {
                
                if (child.GetComponentInChildren<Text>() != null)
                {
                    child.GetComponentInChildren<Text>().font = font;
                    child.GetComponentInChildren<Text>().fontSize = 14;
                }
               
            }     

            if (child.childCount > 0)
            {
                ChangeFont(child.gameObject);
            }
        }

    }

    public void ElementNavigation(GameObject _canvas)
    {
        Debug.Log("element Navigation");
        
        for (int i = 0; i < _canvas.transform.childCount; i++)
        {
            Transform child = _canvas.transform.GetChild(i);

            Navigation customNav = new Navigation();
            customNav.mode = Navigation.Mode.None;

            if (child.GetComponent<Toggle>() != null)
            {
               
                child.GetComponent<Toggle>().navigation = customNav;     
            }

            if (child.GetComponent<Button>() != null)
            {
                child.GetComponent<Button>().navigation = customNav;
            }
            else if (child.GetComponent<Dropdown>() != null)
            {
                child.GetComponent<Dropdown>().navigation = customNav;
            }
            else if (child.GetComponent<InputField>() != null)
            {
                child.GetComponent<InputField>().navigation = customNav;
            }

            if (child.childCount > 0)
            {
                ChangeFont(child.gameObject);
            }
        }

    }
}

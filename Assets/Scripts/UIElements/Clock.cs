using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using BrydenWoodUnity.Lighting;


namespace BrydenWoodUnity.UIElements
{
    public enum ClockType
    {
        analog,
        digital
    }
    /// <summary>
    /// A MonoBehaviour component from controlling the su position through a UI clock
    /// </summary>
    public class Clock : MonoBehaviour
    {
        #region Public Fields and Properties

        public ClockType type;

        //-- set start time 00:00
        public int minutes = 0;
        public int hour = 0;
        public int seconds = 0;
        public bool realTime = true;

        public GameObject pointerSeconds;
        public GameObject pointerMinutes;
        public GameObject pointerHours;
        public SunControlUI_ sunScript;

        public Text hoursText;
        public Text minText;

        [Header("Skybox materials")]
        public Material day;
        public Material evening;
        public Material night;


        //-- time speed factor
        public float clockSpeed = 1.0f;     // 1.0f = realtime, < 1.0f = slower, > 1.0f = faster
        #endregion

        #region Private Fields and Properties
        //-- internal vars
        private float msecs = 0;
        private float rotationMinutes;
        private float rotationHours;
        private float prevHour = 0;
        #endregion

        #region MonoBehaviour Methods
        void Start()
        {
            hour = sunScript.this_is_now.Hour;
            minutes = sunScript.this_is_now.Minute;
        }

        void Update()
        {
            hour = sunScript.this_is_now.Hour;
            minutes = sunScript.this_is_now.Minute;

            switch (type)
            {
                case ClockType.analog:
                    RotateAnalogClock();
                    break;

                case ClockType.digital:
                    DigitalClock();
                    break;
            }

            UpdateSkybox();
        }

        void RotateAnalogClock()
        {
            rotationMinutes = (int)((360.0f / 60.0f) * minutes);
            rotationHours = (int)(((360.0f / 12.0f) * hour) + ((360.0f / (60.0f * 12.0f)) * minutes));


            pointerMinutes.transform.localEulerAngles = new Vector3(0.0f, 0.0f, -rotationMinutes);
            pointerHours.transform.localEulerAngles = new Vector3(0.0f, 0.0f, -rotationHours);

        }

        void DigitalClock()
        {
            
            if (minutes.ToString().ToCharArray().Length == 1)
            {
                minText.text = "0"+minutes.ToString();
            }
            else
            {
                minText.text = minutes.ToString();
            }

            if (hour.ToString().ToCharArray().Length == 1)
            {
                hoursText.text = "0" + hour.ToString();
            }
            else
            {
                hoursText.text = hour.ToString();
            }

        }

        void UpdateSkybox()
        {
            //if (prevHour != hour)
            //{
            //    if (hour >= 7 && hour <= 19)
            //    {
            //        RenderSettings.skybox = day;
            //    }
            //    else if ((hour>19 && hour <21) || (hour>4 && hour<7))
            //    {
            //        RenderSettings.skybox = evening;
            //    }
            //    else
            //    {
            //        RenderSettings.skybox = night;
            //    }
            //    prevHour = hour;
            //}
            
        }
        #endregion
    }
}

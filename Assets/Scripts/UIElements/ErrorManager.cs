﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace BrydenWoodUnity.UIElements
{
    public class ErrorManager : MonoBehaviour
    {
        [Header("Scene References:")]
        public GameObject notificationsSign;
        //public Notifications notifications;
        //public ErrorPopUp errorPopUp;

        // Use this for initialization
        void Start()
        {
            notificationsSign.SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void OnNotificationsUpdated(bool hasNotifications)
        {
            notificationsSign.SetActive(hasNotifications);
        }
    }

    [System.Serializable]
    public class NotificationsEvent : UnityEvent<bool>
    {

    }
}

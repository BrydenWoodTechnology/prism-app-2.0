﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    public class SiteWideBriefPanel : BriefPanel
    {
        public SiteWidePanel siteWidePanel;
        public bool isBasement;
        public bool isPodium;

        public override void AddBriefElement()
        {
            bool containsOtherThanCommercial = false;
            foreach (Transform item in briefElementHolder)
            {
                var briefElement = item.GetComponent<BriefElement>();
                if (briefElement != null)
                {
                    if (briefElement.type == "Commercial")
                    {
                        containsOtherThanCommercial = true;
                    }
                }
            }
            if (briefElementHolder.childCount != 1)
            {
                containsOtherThanCommercial = true;
            }
            GameObject obj = Instantiate(briefElementPrefab, briefElementHolder);
            obj.name = "BriefElement_" + (briefElementHolder.childCount - 1);
            obj.GetComponent<BriefElement>().Initialize(isBasement, isPodium);
            obj.GetComponent<BriefElement>().onChanged.AddListener(OnBriefElementChanged);
            obj.GetComponent<BriefElement>().onDeleted.AddListener(OnBriefElementDeleted);
            obj.transform.SetSiblingIndex(briefElementHolder.childCount - 2);
            //if (containsOtherThanCommercial)
            //{
            //    obj.GetComponent<BriefElement>().RemoveCommercialFromList();
            //}

            siteWidePanel.OnBriefChanged();
        }

        public override void AddBriefElement(string type, float percentage)
        {
            GameObject obj = Instantiate(briefElementPrefab, briefElementHolder);
            obj.name = "BriefElement_" + (briefElementHolder.childCount - 1);
            obj.GetComponent<BriefElement>().Initialize(isBasement, isPodium, false);
            obj.GetComponent<BriefElement>().onChanged.AddListener(OnBriefElementChanged);
            obj.GetComponent<BriefElement>().onDeleted.AddListener(OnBriefElementDeleted);
            obj.transform.SetSiblingIndex(briefElementHolder.childCount - 2);
            //bool isGroundFloor = false;
            //if (siteBasementPanel.currentElement.levels != null && siteBasementPanel.currentElement.levels.Count > 0)
            //{
            //    isGroundFloor = floorPanel.currentElement.levels.Count == 1 && floorPanel.currentElement.levels[0] == 0;
            //}
            obj.GetComponent<BriefElement>().SetValues(type, percentage, false);
            briefElementHolder.GetComponent<RectTransform>().sizeDelta = new Vector3(briefElementHolder.GetComponent<RectTransform>().sizeDelta.x, briefElementHolder.childCount * 50);
            if (type == "Commercial" || type == "Other")
            {
                addBriefButton.interactable = false;
                addBriefButton.GetComponent<UITooltip>().tooltip.text = "You cannot have another type together with Comm/Other!";
            }
            else
            {
                addBriefButton.interactable = true;
                addBriefButton.GetComponent<UITooltip>().tooltip.text = "";
            }
        }

        public override void LoadBriefElement(string type, float percentage)
        {
            GameObject obj = Instantiate(briefElementPrefab, briefElementHolder);
            obj.name = "BriefElement_" + (briefElementHolder.childCount - 1);
            obj.transform.SetSiblingIndex(briefElementHolder.childCount - 2);
            obj.GetComponent<BriefElement>().onChanged.AddListener(OnBriefElementChanged);
            obj.GetComponent<BriefElement>().onDeleted.AddListener(OnBriefElementDeleted);
            //bool isGroundFloor = floorPanel.currentElement.levels.Count == 1 && floorPanel.currentElement.levels[0] == 0;
            obj.GetComponent<BriefElement>().LoadValues(type, percentage, false);
            briefElementHolder.GetComponent<RectTransform>().sizeDelta = new Vector3(briefElementHolder.GetComponent<RectTransform>().sizeDelta.x, briefElementHolder.childCount * 50);
            if (type == "Commercial" || type == "Other")
            {
                addBriefButton.interactable = false;
                addBriefButton.GetComponent<UITooltip>().tooltip.text = "You cannot have another type together with Comm/Other!";
            }
            else
            {
                addBriefButton.interactable = true;
                addBriefButton.GetComponent<UITooltip>().tooltip.text = "";
            }
        }

        public override void OnBriefElementChanged(BriefElement sender)
        {
            if (sender.type == "Commercial" || sender.type == "Other")
            {
                addBriefButton.interactable = false;
                addBriefButton.GetComponent<UITooltip>().tooltip.text = "You cannot have another type together with Comm/Other!";
            }
            else
            {
                addBriefButton.interactable = true;
                addBriefButton.GetComponent<UITooltip>().tooltip.text = "";
            }
            var perc = GetPercentages();
            float counter = 0;
            foreach (var item in perc)
            {
                counter += item.Value;
            }
            if (counter > 100)
            {
                percentageNotificationPanel.gameObject.SetActive(true);
                percentageNotificationPanel.GetChild(0).GetComponent<Text>().text = "The percentages "+(isPodium?"of your podium ": "of your basement ") +"add to more than 100%!";
            }
            else if (counter < 100)
            {
                percentageNotificationPanel.gameObject.SetActive(true);
                percentageNotificationPanel.GetChild(0).GetComponent<Text>().text = "The percentages " + (isPodium ? "of your podium " : "of your basement ") + "don't add up to 100%!";
            }
            else
            {
                percentageNotificationPanel.gameObject.SetActive(false);
                percentageNotificationPanel.GetChild(0).GetComponent<Text>().text = "";
            }
            siteWidePanel.OnBriefChanged();
        }

        protected override IEnumerator RemoveElement(BriefElement sender)
        {
            for (int i = 0; i < briefElementHolder.childCount - 1; i++)
            {
                if (briefElementHolder.GetChild(i).GetComponent<BriefElement>() == sender)
                {
                    Destroy(briefElementHolder.GetChild(i).gameObject);
                }
            }
            yield return new WaitForEndOfFrame();
            var perc = GetPercentages();
            float counter = 0;
            foreach (var item in perc)
            {
                counter += item.Value;
            }
            if (counter > 100)
            {
                percentageNotificationPanel.gameObject.SetActive(true);
                percentageNotificationPanel.GetChild(0).GetComponent<Text>().text = "The percentages " + (isPodium ? "of your podium " : "of your basement ") + "add to more than 100%!";
            }
            else if (counter < 100)
            {
                percentageNotificationPanel.gameObject.SetActive(true);
                percentageNotificationPanel.GetChild(0).GetComponent<Text>().text = "The percentages " + (isPodium ? "of your podium " : "of your basement ") + "don't add up to 100%!";
            }
            else
            {
                percentageNotificationPanel.gameObject.SetActive(false);
                percentageNotificationPanel.GetChild(0).GetComponent<Text>().text = "";
            }
            siteWidePanel.OnBriefChanged();
        }

        protected override void AddEvents()
        {
            //BriefElement.changed += OnBriefElementChanged;
            //BriefElement.deleted += OnBriefElementDeleted;
        }

        protected override void RemoveEvents()
        {
            //BriefElement.changed -= OnBriefElementChanged;
            //BriefElement.deleted -= OnBriefElementDeleted;
        }
    }
}

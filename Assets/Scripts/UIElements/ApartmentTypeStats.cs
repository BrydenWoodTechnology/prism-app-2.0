﻿using BrydenWoodUnity.DesignData;
using BrydenWoodUnity.GeometryManipulation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for managing the statistics of the apartment types
    /// </summary>
    public class ApartmentTypeStats : MonoBehaviour
    {
        #region Public Fields and Properties
        public ApartmentTypesChart apartmentTypesChart;
        public Text title;
        public Text info;
        public ProceduralBuildingManager buildingManager;
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            AddEvents();
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnDestroy()
        {
            RemoveEvents();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Called when Preview Mode changed
        /// </summary>
        /// <param name="mode">The current preview mode</param>
        public void OnPreviewChanged(PreviewMode mode)
        {
            switch (mode)
            {
                case PreviewMode.Buildings:
                    //OnClearInfo();
                    //var data = ProceduralBuildingManager.Instance.GetCurrentBuildingStats();
                    //title.text = data[0];
                    //info.text = data[1];
                    break;
                case PreviewMode.Floors:
                    break;
            }
        }

        /// <summary>
        /// Updates the building data displayed
        /// </summary>
        public void UpdateBuildingData()
        {
            OnClearInfo();
            var data = buildingManager.GetGeneralInfo();
            title.text = data[0];
            info.text = data[1];
            UpdateInfoHeight();
        }

        /// <summary>
        /// Called when the displayed information should be cleared
        /// </summary>
        public void OnClearInfo()
        {
            title.text = "";
            info.text = "";
            UpdateInfoHeight();
        }

        /// <summary>
        /// Called when an apartment type has been selected
        /// </summary>
        /// <param name="type">The selected apartment type</param>
        /// <param name="apts">The selected apartments</param>
        public void OnTypeSelected(string type, List<ApartmentUnity> apts)
        {
            title.text = string.Format("{0}\r\n", type);
            info.text = string.Format("Total {0}\r\n", apts.Count);
            //double area = 0;
            //for (int i = 0; i < apts.Count; i++)
            //{
            //    area += apts[i].area;
            //}
            //info.text += string.Format("Overall area: {0}\r\n", area.ToString("0.00"));
            List<float> areas = new List<float>();
            for (int i = 0; i < apts.Count; i++)
            {
                areas.Add(apts[i].Area);
            }

            var unique = areas.Distinct().ToArray();

            for (int i = 0; i < unique.Length; i++)
            {
                int counter = 0;
                for (int j = 0; j < areas.Count; j++)
                {
                    if (areas[j] == unique[i])
                    {
                        counter++;
                    }
                }

                info.text += (string.Format("{0} = {1}m\xB2\r\n", counter, unique[i]));
            }
            UpdateInfoHeight();
        }

        /// <summary>
        /// Called when the apartment types have been deselected
        /// </summary>
        /// <param name="type">The previously selected type</param>
        /// <param name="apts">The previously selected apartments</param>
        public void OnTypeDeselect(string type, List<ApartmentUnity> apts)
        {
            UpdateBuildingData();
        }
        #endregion

        #region Private Methods
        private void UpdateInfoHeight()
        {
            float lineHeight = BrydenWoodUtils.CalculateLineHeight(info) / info.transform.root.localScale.y;
            int linesNum = info.text.Split(new char[] { '\n' }).Length + 1;
            info.GetComponent<RectTransform>().sizeDelta = new Vector2(info.GetComponent<RectTransform>().sizeDelta.x, lineHeight * linesNum);
            info.GetComponent<RectTransform>().parent.GetComponent<RectTransform>().sizeDelta = info.GetComponent<RectTransform>().sizeDelta;
        }
        private void AddEvents()
        {
            DisplayControls.previewChanged += OnPreviewChanged;
            buildingManager.overallUpdated += UpdateBuildingData;
        }

        private void RemoveEvents()
        {
            DisplayControls.previewChanged -= OnPreviewChanged;
            buildingManager.overallUpdated -= UpdateBuildingData;
        }
        #endregion

    }
}

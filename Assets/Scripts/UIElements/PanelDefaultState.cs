﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PanelDefaultState : MonoBehaviour
{
    public UnityEvent DefaultState;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ReturnToDefault()
    {
        if (DefaultState != null)
        {
            DefaultState.Invoke();
        }
    }
}

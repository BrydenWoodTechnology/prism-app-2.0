﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BrydenWoodUnity.UIElements
{
    public class LandingPageManager : MonoBehaviour
    {
        [DllImport("__Internal")]
        private static extern void PopupOpenerCaptureClick(string link);
        //private static void PopupOpenerCaptureClick(string link) { }

        public GameObject splashScreenBackground;

        // Use this for initialization
        void Start()
        {
            if (GameObject.Find("ShouldLoadFiles") != null)
            {
                splashScreenBackground.SetActive(false);
                gameObject.SetActive(false);
            }
            else if (GameObject.Find("NotFirstStart") != null)
            {
                splashScreenBackground.SetActive(false);
                gameObject.SetActive(false);
                Destroy(GameObject.Find("NotFirstStart"));
            }
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void OnButtonPointerDown(string link)
        {
            PopupOpenerCaptureClick(link);
        }

        public void OnStartApplication()
        {
            SceneManager.LoadScene(1);
        }
    }
}

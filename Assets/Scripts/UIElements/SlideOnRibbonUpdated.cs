﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component to slide elements on the UI Ribbon
    /// </summary>
    public class SlideOnRibbonUpdated : MonoBehaviour
    {
        #region Public Fields and Properties
        [Header("Scene References:")]
        public RectTransform container;
        public Toggle main_toggle;
        public Transform MainContainer;
        public List<GameObject> elementsToSlide = new List<GameObject>();
        public bool last_tog_value;
        public bool toggleValue;
        public bool executing_routine;
        public bool coroutine_renewed;
        #endregion

        #region Private Fields and Properties
        private float distance { get; set; }
        private float full_distance { get; set; }
        private GameObject moverParent { get; set; }
        private bool error_ms { get; set; }
        private int tog_counter { get; set; }
        private int last_tog_counter { get; set; }
        private WaitForSeconds co_delay { get; set; }
        private WaitForSeconds tog_delay { get; set; }
        private IEnumerator slider { get; set; }
        private Vector3 startPos { get; set; }
        private Vector3 endPos { get; set; }
        private Vector3 currentPos { get; set; }
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            executing_routine = false;
            error_ms = false;
            tog_counter = 0;
            last_tog_counter = 0;
            co_delay = new WaitForSeconds(0.2f);
            tog_delay = new WaitForSeconds(0.1f);


            MainContainer = transform.parent.parent.parent;
            //--fill elements to slide list with the gameobjects on my right ride
            int myIndex = transform.parent.parent.GetSiblingIndex();
            int allIndices = MainContainer.childCount;
            if (container != null)
            {
                distance = container.rect.width;
            }
            else
            {
                distance = 0;
            }
            full_distance = distance;
            last_tog_value = false;
            toggleValue = false;
            coroutine_renewed = false;
            if (myIndex != allIndices - 1)
            {
                for (int i = myIndex + 1; i < allIndices; i++)
                {
                    elementsToSlide.Add(MainContainer.transform.GetChild(i).gameObject);
                }
            }
        }

        // Update is called once per frame
        void Update()
        {
            
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Groups sliding elements together
        /// </summary>
        public void GroupSlidingElements(/*Toggle pressedTog*/)
        {
            //error_ms = false;
            tog_counter++;
            toggleValue = !toggleValue;
            // -- IF COROUTINE NOT RUNNING: FIRE COROUTINE
            if (executing_routine == false && error_ms == false)
            {
                if (moverParent == null)
                {
                    moverParent = new GameObject("MoverParent");
                }
                moverParent.transform.SetParent(MainContainer);
                moverParent.transform.localPosition = transform.localPosition;

                startPos = moverParent.transform.localPosition;
                if (toggleValue)
                {
                    //--calculate end position to slide right
                    endPos = new Vector3(startPos.x + distance, startPos.y, 0);
                }
                else if (toggleValue == false)
                {
                    //--calculate end position to slide left
                    endPos = new Vector3(startPos.x - distance, startPos.y, 0);
                }

                GroupFunction(slider, toggleValue, endPos);

            }
            if (error_ms)
            {
                error_ms = false;

                //Debug.Log("error changed");
            }
        }

        /// <summary>
        /// Checks the toggle value
        /// </summary>
        public void IdleToggleValueChange()
        {
            if (toggleValue)
            {
                if (container.gameObject.activeSelf)
                {
                    main_toggle.isOn = true;
                    error_ms = false;
                }
                if (container.gameObject.activeSelf == false)
                {
                    main_toggle.isOn = false;
                    error_ms = false;
                }
            }
            if (toggleValue == false)
            {
                if (container.gameObject.activeSelf)
                {
                    main_toggle.isOn = true;
                    error_ms = false;
                }
                if (container.gameObject.activeSelf == false)
                {
                    main_toggle.isOn = false;
                    error_ms = false;
                }
            }
        }

        /// <summary>
        /// Groups elements together and starts the sliding process
        /// </summary>
        /// <param name="routine">The sliding proces</param>
        /// <param name="onOff">Toggle</param>
        /// <param name="end_here">The end vector of the sliding</param>
        public void GroupFunction(IEnumerator routine, bool onOff, Vector3 end_here)
        {           
            if (elementsToSlide.Count != 0)
            {
                for (int i = 0; i < elementsToSlide.Count; i++)
                {
                    elementsToSlide[i].transform.SetParent(moverParent.transform);
                }
            }

            distance = full_distance;


            routine = SlideElements(0.05f, moverParent.transform, container, end_here, onOff);

            StartCoroutine(routine);
        }

        /// <summary>
        /// Breaks the coroutine of sliding
        /// </summary>
        /// <param name="_popUp">The element to stop from sliding</param>
        //function that sends the message to the popUp tab to break its coroutine
        public void ListenSlider(RectTransform _popUp)
        {
            _popUp.GetComponent<PopUpElement>().delay_activated = !toggleValue;
            _popUp.GetComponent<PopUpElement>().counter++;
        }


        /// <summary>
        /// Slides the elements on the canvas
        /// </summary>
        /// <param name="seconds">The total time of the process</param>
        /// <param name="mover">The parent object for the movement</param>
        /// <param name="placeHolder">The placeholder element for the movement</param>
        /// <param name="go_here">The End point of the movement</param>
        /// <param name="leftRight">Left or Right slide</param>
        /// <returns></returns>
        public IEnumerator SlideElements(float seconds, Transform mover, RectTransform placeHolder, Vector3 go_here, bool leftRight)
        {
            coroutine_renewed = false;
            error_ms = false;
            last_tog_value = toggleValue;
            executing_routine = true;
            last_tog_counter = tog_counter;
            float elapsedTime = 0;

            while (elapsedTime < seconds)
            {
                if (leftRight == false)  //-- Turn off content to start sliding left
                {
                    placeHolder.gameObject.SetActive(false);
                }
                mover.localPosition = Vector3.Lerp(startPos, go_here, (elapsedTime / seconds));
                currentPos = mover.localPosition;
                elapsedTime += 0.06f * (335 / distance);
                //---transition is finished (last frame)
                if (elapsedTime >= seconds)
                {
                    mover.localPosition = go_here;
                    if (elementsToSlide.Count != 0)
                    {
                        for (int k = 0; k < elementsToSlide.Count; k++)
                        {
                            elementsToSlide[k].transform.SetParent(MainContainer.transform);
                        }
                    }
                    //if (mover.childCount == 0)
                    //{
                    //    Destroy(mover.gameObject);
                    //}
                    if (leftRight) //-- Turn on content when sliding right is complete
                    {
                        placeHolder.gameObject.SetActive(true);
                    }
                    if (coroutine_renewed)
                    {
                        if (leftRight)
                        {
                            placeHolder.gameObject.SetActive(false);
                        }
                        else if (leftRight == false)
                        {
                            placeHolder.gameObject.SetActive(true);
                        }
                    }
                    executing_routine = false;


                    if (toggleValue != last_tog_value)
                    {
                        error_ms = true;
                        //Debug.Log("error here 1" + ", executing routine: "+executing_routine + ", coroutine renewed: " +coroutine_renewed);
                        //Debug.Log("togglevalue: " + toggleValue);
                        IdleToggleValueChange(); ;
                        yield break;

                    }
                }

                //-- IF COROUTINE IS DISRUPTED BEFORE ITS COMPLETION
                if (toggleValue != last_tog_value && executing_routine && coroutine_renewed == false)
                {
                    go_here = startPos;
                    startPos = currentPos;
                    coroutine_renewed = true;
                    //distance = /*full_distance- */Vector3.Distance(moverStartPos, moverEndPos);
                    last_tog_value = toggleValue;

                }

                yield return null;
            }
        }
        #endregion
    }
}
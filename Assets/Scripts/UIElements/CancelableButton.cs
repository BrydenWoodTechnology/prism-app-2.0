﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for buttons that would require user confirmation for their action
    /// </summary>
    public class CancelableButton : MonoBehaviour, ICancelable
    {
        /// <summary>
        /// Triggered when the value of the button has been submitted
        /// </summary>
        public OnButtonSubmitted buttonSubmitted;
        [Tooltip("Whether this button affects the whole project or the selected building")]
        public bool allProject = true;
        
        /// <summary>
        /// Called from the UnityUI when the button is pressed
        /// </summary>
        public void OnClick()
        {
            if (allProject)
            {
                RefreshPopUp.TaggedObject.AddChangedValue(this, new string[] { String.Empty, String.Empty });
            }
            else
            {
                RefreshBuildingPopUp.TaggedObject.AddChangedValue(this, new string[] { String.Empty, String.Empty });
            }
        }

        /// <summary>
        /// Resets the value of the button
        /// </summary>
        /// <param name="prevValue">The value to reset to</param>
        public void ResetValue(string prevValue)
        {
            
        }

        /// <summary>
        /// Submits the action of the value
        /// </summary>
        /// <param name="currentValue">The new value to be submitted</param>
        public void SubmitValue(string currentValue)
        {
            buttonSubmitted.Invoke(this);
        }
    }

    /// <summary>
    /// A Unity Event class for confirming the actions of a Unity UI Button
    /// </summary>
    [Serializable]
    public class OnButtonSubmitted : UnityEvent<object>
    {

    }
}

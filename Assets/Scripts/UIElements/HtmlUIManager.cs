﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Runtime.InteropServices;

public class HtmlUIManager : MonoBehaviour
{
    [DllImport("__Internal")]
    private static extern string CallFunction(string methodName, string arg);

    public Text urlInputField;
    public GameObject gridplane;

    #region Events
    public delegate void OnlatlonReceived(string latlon);
    public static event OnlatlonReceived OnLatLonReceived;

    public delegate void OnBrowserOnline(bool online);
    public static event OnBrowserOnline OnBrowserOnLine;

    public delegate void OnLoadSampleProject(string text);
    public static event OnLoadSampleProject onloadSampleProject;

    public UnityEvent createNewProject;
    public UnityEvent loadProject;
   
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    #region Receivers
    public void OnUrlReceived(string url)
    {
        if (url == "")
            return;
        string[] filter = url.Split(new string[] { "#map=" }, System.StringSplitOptions.None);
        string[] coords = filter[1].Split('/');
        string latlon = coords[1] + "," + coords[2];
        Debug.Log(latlon);
        urlInputField.text = url;
        gridplane.SetActive(false);
        OnLatLonReceived(latlon);

    }

    public void OnCreateProject()
    {
        if (createNewProject != null)
        {
            createNewProject.Invoke();
        }
    }

    public void OnLoadProject()
    {
        if (loadProject != null)
        {
            loadProject.Invoke();
        }
    }

    public void OnLoadSamplProject(string data)
    {

        onloadSampleProject(data);
        
    }

    public void OnBrowserOffline(string offline)
    {
        bool online = bool.Parse(offline);
        Debug.Log("OnBrowserOffline" + offline);
        OnBrowserOnLine(online);
    }

    #endregion

    #region Senders
    public void OpenUrlOverlay()
    {
        
#if UNITY_EDITOR

#elif UNITY_WEBGL
        CallFunction("initialise", "");
#endif
    }

    public void GoToPlatformsGraph()
    {
#if UNITY_EDITOR

#elif UNITY_WEBGL
        CallFunction("openPlatformsGraph", "");
#endif
    }
    #endregion
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for altering the icons of the navigation mode
    /// </summary>
    public class Nav_Selector : MonoBehaviour
    {
        #region Public Fields and Properties
        public List<Sprite> cursors;
        public bool slideback;
        public Toggle ribbon_tab;
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Sets the navigation image based on a given index
        /// </summary>
        /// <param name="index">The index of the seleted image</param>
        public void SetNavigationImage(int index)
        {
            transform.GetComponent<Image>().sprite = cursors[index];
            if (slideback)
            {
                ribbon_tab.isOn = false;
            }
        }
        #endregion

    }
}

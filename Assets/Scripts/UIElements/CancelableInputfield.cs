﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for managing the changes in value of a Input Field
    /// </summary>
    public class CancelableInputfield : MonoBehaviour, ICancelable
    {
        public OnInputSubmitted valueSubmitted;
        [Tooltip("Whether it should call for refreshing the whole project or just the selected building")]
        public bool allProject = true;

        private InputField inputField;
        private string prevValue;

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            inputField = GetComponent<InputField>();
            prevValue = inputField.text;
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Called when the value of the Input Field changes
        /// </summary>
        /// <param name="val">The new value</param>
        public void OnValueChanged(string value)
        {
            if (allProject)
            {
                RefreshPopUp.TaggedObject.AddChangedValue(this, new string[] { prevValue, value.ToString() });
            }
            else
            {
                RefreshBuildingPopUp.TaggedObject.AddChangedValue(this, new string[] { prevValue, value.ToString() });
            }
        }

        /// <summary>
        /// Resets the value of the Dropdown
        /// </summary>
        /// <param name="prevValue">The previous value</param>
        public void ResetValue(string prevValue)
        {
            inputField.SetValue(prevValue);
            this.prevValue = prevValue;
        }

        /// <summary>
        /// Submits the new value of the dropdown
        /// </summary>
        /// <param name="currentValue">The new value</param>
        public void SubmitValue(string currentValue)
        {
            valueSubmitted.Invoke(currentValue);
            prevValue = currentValue;
        }
        #endregion
    }

    /// <summary>
    /// A UnityEvent class for when the new values are submitted
    /// </summary>
    [System.Serializable]
    public class OnInputSubmitted : UnityEvent<string>
    {

    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    public class CancelableToggle : MonoBehaviour, ICancelable
    {
        public OnToggleSubmitted toggleSubmitted;
        public GameObject notificationPanel;
        [Tooltip("Whether this button affects the whole project or the selected building")]
        public bool allProject = false;
        private Toggle toggle;
        private bool prevValue;
        public void ResetValue(string prevValue)
        {
            toggle.SetValue(bool.Parse(prevValue));
            this.prevValue = bool.Parse(prevValue);
        }

        public void SubmitValue(string currentValue)
        {
            toggleSubmitted.Invoke(bool.Parse(currentValue));
            prevValue = bool.Parse(currentValue);
        }

        // Start is called before the first frame update
        void Start()
        {
            toggle = GetComponent<Toggle>();
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void OnToggleChanged(bool value)
        {
            if (notificationPanel == null)
            {
                if (allProject)
                {
                    RefreshPopUp.TaggedObject.AddChangedValue(this, new string[] { prevValue.ToString(), value.ToString() });
                }
                else
                {
                    RefreshBuildingPopUp.TaggedObject.AddChangedValue(this, new string[] { prevValue.ToString(), value.ToString() });
                }
            }
            else
            {
                if (value)
                {
                    notificationPanel.GetComponent<CoresNotification>().AddChangedValue(this, new string[] { prevValue.ToString(), value.ToString() });
                }
                else
                {
                    SubmitValue(value.ToString());
                }
            }
        }
    }

    [System.Serializable]
    public class OnToggleSubmitted: UnityEvent<bool>
    {

    }
}

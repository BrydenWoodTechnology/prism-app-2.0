﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BrydenWoodUnity.UIElements
{
    public class CoresNotification : MonoBehaviour
    {
        public List<KeyValuePair<ICancelable, string[]>> changedValues = new List<KeyValuePair<ICancelable, string[]>>();
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void SubmitValues()
        {
            try
            {
                foreach (var item in changedValues)
                {
                    item.Key.SubmitValue(item.Value[1]);
                }
                changedValues.Clear();
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }
            GetComponent<MovePanelOnCanvas>().OnToggle(false);
        }

        public void CancelValues()
        {
            foreach (var item in changedValues)
            {
                item.Key.ResetValue(item.Value[0]);
            }
            changedValues.Clear();
            GetComponent<MovePanelOnCanvas>().OnToggle(false);
        }

        public void AddChangedValue(ICancelable sender, string[] value)
        {
            GetComponent<MovePanelOnCanvas>().OnToggle(true);
            changedValues.Add(new KeyValuePair<ICancelable, string[]>(sender, value));
        }
    }
}

﻿using BrydenWoodUnity.DesignData;
using BrydenWoodUnity.UIElements;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour for controling the wall widths
    /// </summary>
    public class WallWidth : MonoBehaviour, ICancelable
    {
        #region Public Fields and Properties
        [Header("UI References")]
        public Dropdown constructionTypesDropdown;
        public InputField partyWall;
        public InputField exteriorWall;
        public InputField corridorWall;
        public InputField panelLength;
        public InputField lineLoadSpan;

        [Header("Scene References")]
        public ProceduralApartmentLayout apartmentLayout;
        public ProceduralBuildingManager buildingManager;
        #endregion

        private string prevPreset;
        private string prevParty;
        private string prevExterior;
        private string prevCorridor;
        private string prevPanelLength;
        private string prevLineSpan;


        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            prevPreset = "preset;" + constructionTypesDropdown.options[0].text;
            prevParty = "party;" + Standards.TaggedObject.ConstructionFeatures["PartyWall"];
            prevExterior = "exterior;" + Standards.TaggedObject.ConstructionFeatures["ExteriorWall"];
            prevCorridor = "corridor;" + Standards.TaggedObject.ConstructionFeatures["CorridorWall"];
            prevPanelLength = "panel;" + Standards.TaggedObject.ConstructionFeatures["PanelLength"];
            prevLineSpan = "line;" + Standards.TaggedObject.ConstructionFeatures["LineSpan"];
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Called when the construction type changes
        /// </summary>
        /// <param name="val">The index of the construction type</param>
        public void OnConstructionTypeChanged(int val)
        {
            string type = constructionTypesDropdown.options[val].text.Replace(' ','_');
            var values = Standards.TaggedObject.ManufacturingPresets[type];
            exteriorWall.SetValue(Math.Round(values[0],3).ToString());
            partyWall.SetValue(Math.Round(values[1],3).ToString());
            corridorWall.SetValue(Math.Round(values[2],3).ToString());
            panelLength.SetValue(Math.Round(values[3],3).ToString());
            lineLoadSpan.SetValue(Math.Round(values[4],3).ToString());

            RefreshPopUp.TaggedObject.AddChangedValue(this, new string[] { prevPreset, "preset;" + type });
        }

        /// <summary>
        /// Called when the party wall width updates
        /// </summary>
        /// <param name="val">The new wall width as a string</param>
        public void OnPartyWallUpdated(string val)
        {
            RefreshPopUp.TaggedObject.AddChangedValue(this, new string[] { prevParty, "party;" + val });
        }

        /// <summary>
        /// Called when the exterior wall width updates
        /// </summary>
        /// <param name="val">The new wall width as a string</param>
        public void OnExteriorWallUpdated(string val)
        {
            RefreshPopUp.TaggedObject.AddChangedValue(this, new string[] { prevExterior, "exterior;" + val });
        }

        /// <summary>
        /// Called when the corridor wall width updates
        /// </summary>
        /// <param name="val">The new wall width as a string</param>
        public void OnCorridorWallUpdated(string val)
        {
            RefreshPopUp.TaggedObject.AddChangedValue(this, new string[] { prevCorridor, "corridor;" + val });
        }

        public void OnLineSpanChanged(string val)
        {
            RefreshPopUp.TaggedObject.AddChangedValue(this, new string[] { prevLineSpan, "line;" + val });
        }

        public void OnPanelLengthChanged(string val)
        {
            RefreshPopUp.TaggedObject.AddChangedValue(this, new string[] { prevPanelLength, "panel;" + val });
        }

        public void ResetValue(string prevValue)
        {
            var cells = prevValue.Split(';');
            switch (cells[0])
            {
                case "preset":
                    string type = cells[1];
                    var values = Standards.TaggedObject.ManufacturingPresets[type];
                    for (int i = 0; i < constructionTypesDropdown.options.Count; i++)
                    {
                        if (constructionTypesDropdown.options[i].text == type)
                        {
                            constructionTypesDropdown.SetValue(i);
                            break;
                        }
                    }
                    exteriorWall.SetValue(values[0].ToString());
                    partyWall.SetValue(values[1].ToString());
                    corridorWall.SetValue(values[2].ToString());
                    panelLength.SetValue(values[3].ToString());
                    lineLoadSpan.SetValue(values[4].ToString("0.00"));
                    prevPreset = "preset;" + type;
                    break;
                case "party":
                    partyWall.SetValue(cells[1]);
                    prevParty = "party;" + cells[1];
                    break;
                case "exterior":
                    exteriorWall.SetValue(cells[1]);
                    prevExterior = "exterior;" + cells[1];
                    break;
                case "corridor":
                    corridorWall.SetValue(cells[1]);
                    prevCorridor = "corridor;" + cells[1];
                    break;
                case "line":
                    lineLoadSpan.SetValue(cells[1]);
                    prevLineSpan = "line;" + cells[1];
                    break;
                case "panel":
                    panelLength.SetValue(cells[1]);
                    prevPanelLength = "panel;" + cells[1];
                    break;
            }
        }

        public void CheckIfCustomValues(string key, double value)
        {
            double[] values = new double[0];
            bool custom = false;
            if (Standards.TaggedObject.ManufacturingPresets.TryGetValue(constructionTypesDropdown.options[constructionTypesDropdown.value].text, out values))
            {
                switch (key)
                {
                    case "PartyWall":
                        if (value != values[1]) custom = true;
                        break;
                    case "ExteriorWall":
                        if (value != values[0]) custom = true;
                        break;
                    case "CorridorWall":
                        if (value != values[2]) custom = true;
                        break;
                    case "PanelLength":
                        if (value != values[3]) custom = true;
                        break;
                    case "LineSpan":
                        if (value != values[4]) custom = true;
                        break;
                }
            }
            else
            {
                custom = true;
            }

            if (custom && constructionTypesDropdown.options[constructionTypesDropdown.value].text != "Custom")
            {
                constructionTypesDropdown.options.Add(new Dropdown.OptionData("Custom"));
                constructionTypesDropdown.SetValue(constructionTypesDropdown.options.Count - 1);
                constructionTypesDropdown.RefreshShownValue();
            }
        }

        public void SubmitValue(string currentValue)
        {
            var cells = currentValue.Split(';');
            float m_val;
            switch (cells[0])
            {
                case "preset":
                    //if (constructionTypesDropdown.options.Count > 7)
                    //{
                    //    constructionTypesDropdown.options.RemoveAt(7);
                    //}
                    string type = cells[1];
                    var values = Standards.TaggedObject.ManufacturingPresets[type];
                    exteriorWall.SetValue(values[0].ToString());
                    partyWall.SetValue(values[1].ToString());
                    corridorWall.SetValue(values[2].ToString());
                    panelLength.SetValue(values[3].ToString());
                    lineLoadSpan.SetValue(values[4].ToString("0.00"));

                    Standards.TaggedObject.ConstructionFeatures["PartyWall"] = (float)values[1];
                    Standards.TaggedObject.ConstructionFeatures["ExteriorWall"] = (float)values[0];
                    Standards.TaggedObject.ConstructionFeatures["CorridorWall"] = (float)values[2];
                    Standards.TaggedObject.ConstructionFeatures["PanelLength"] = (float)values[3];
                    Standards.TaggedObject.ConstructionFeatures["LineSpan"] = (float)values[4];
                    StartCoroutine(UpdateApartmentLayout());
                    buildingManager.CheckSystemConstraints();
                    prevPreset = "preset;" + type;
                    Standards.TaggedObject.EvaluateStructuralWalls();

                    break;
                case "party":
                    if (!String.IsNullOrEmpty(cells[1]) && float.TryParse(cells[1], out m_val))
                    {
                        Standards.TaggedObject.ConstructionFeatures["PartyWall"] = m_val;
                        StartCoroutine(UpdateApartmentLayout());
                        buildingManager.CheckSystemConstraints();
                        CheckIfCustomValues("PartyWall", m_val);
                    }
                    prevParty = "party;" + cells[1];
                    Standards.TaggedObject.EvaluateStructuralWalls();
                    break;
                case "exterior":
                    if (!String.IsNullOrEmpty(cells[1]) && float.TryParse(cells[1], out m_val))
                    {
                        Standards.TaggedObject.ConstructionFeatures["ExteriorWall"] = m_val;
                        StartCoroutine(UpdateApartmentLayout());
                        buildingManager.CheckSystemConstraints();
                        CheckIfCustomValues("ExteriorWall", m_val);
                    }
                    prevExterior = "exterior;" + cells[1];
                    break;
                case "corridor":
                    if (!String.IsNullOrEmpty(cells[1]) && float.TryParse(cells[1], out m_val))
                    {
                        Standards.TaggedObject.ConstructionFeatures["CorridorWall"] = m_val;
                        StartCoroutine(UpdateApartmentLayout());
                        buildingManager.CheckSystemConstraints();
                        CheckIfCustomValues("CorridorWall", m_val);
                    }
                    prevCorridor = "corridor;" + cells[1];
                    break;
                case "line":
                    if (!String.IsNullOrEmpty(cells[1]) && float.TryParse(cells[1], out m_val))
                    {
                        Standards.TaggedObject.ConstructionFeatures["LineSpan"] = m_val;
                        StartCoroutine(UpdateApartmentLayout());
                        buildingManager.CheckSystemConstraints();
                        CheckIfCustomValues("LineSpan", m_val);
                    }
                    prevLineSpan = "line;" + cells[1];
                    break;
                case "panel":
                    if (!String.IsNullOrEmpty(cells[1]) && float.TryParse(cells[1], out m_val))
                    {
                        Standards.TaggedObject.ConstructionFeatures["PanelLength"] = m_val;
                        StartCoroutine(UpdateApartmentLayout());
                        buildingManager.CheckSystemConstraints();
                        CheckIfCustomValues("PanelLength", m_val);
                    }
                    prevPanelLength = "panel;" + cells[1];
                    break;
            }
            //Debug.Log(constructionTypesDropdown.options.Count);
            //Debug.Log(constructionTypesDropdown.value);
            ReportData.TaggedObject.ConstructionFeaturesName = constructionTypesDropdown.options[constructionTypesDropdown.value].text;
            ReportData.TaggedObject.ConstructionFeatures = Standards.TaggedObject.ConstructionFeaturesReport;
        }
        #endregion

        #region Private Methods
        private IEnumerator UpdateApartmentLayout()
        {
            if (buildingManager.previewMode == PreviewMode.Apartments)
            {
                yield return StartCoroutine(apartmentLayout.ResetLayout());
                apartmentLayout.Initialize();
                apartmentLayout.SetManufacturingSystem(apartmentLayout.manufacturingSystem);
                apartmentLayout.ShowOutline(true);
            }
            else
            {

            }
        }
        #endregion
    }
}

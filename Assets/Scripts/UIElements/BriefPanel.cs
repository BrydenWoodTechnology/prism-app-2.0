﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for managing the brief elements
    /// </summary>
    public class BriefPanel : MonoBehaviour
    {
        #region Public Fields and Properties
        [Header("Scene References:")]
        public Transform briefElementHolder;
        public GameObject briefElementPrefab;
        public Button addBriefButton;
        public FloorPanel floorPanel;
        public RectTransform percentageNotificationPanel;
        #endregion

        #region Events
        /// <summary>
        /// Used when any of the brief elements has changed
        /// </summary>
        /// <param name="sender"></param>
        public delegate void BriefChanged(BriefPanel sender);
        /// <summary>
        /// Triggered when any of the brief elements has changed
        /// </summary>
        public static event BriefChanged changed;
        #endregion

        #region MonoBehaviour Methods
        protected virtual void OnDestroy()
        {
            if (changed != null)
            {
                foreach (Delegate eh in changed.GetInvocationList())
                {
                    changed -= (BriefChanged)eh;
                }
            }
            RemoveEvents();
        }

        // Use this for initialization
        protected virtual void Start()
        {
            AddEvents();
        }

        // Update is called once per frame
        protected virtual void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Adds a brief element to the list
        /// </summary>
        public virtual void AddBriefElement()
        {
            //bool containsOtherThanCommercial = false;
            //foreach (Transform item in briefElementHolder)
            //{
            //    var briefElement = item.GetComponent<BriefElement>();
            //    if (briefElement != null)
            //    {
            //        if (briefElement.type == "Commercial")
            //        {
            //            containsOtherThanCommercial = true;
            //        }
            //    }
            //}
            //if (briefElementHolder.childCount != 1)
            //{
            //    containsOtherThanCommercial = true;
            //}
            GameObject obj = Instantiate(briefElementPrefab, briefElementHolder);
            obj.name = "BriefElement_" + (briefElementHolder.childCount - 1);
            obj.GetComponent<BriefElement>().Initialize();
            obj.GetComponent<BriefElement>().onChanged.AddListener(OnBriefElementChanged);
            obj.GetComponent<BriefElement>().onDeleted.AddListener(OnBriefElementDeleted);
            obj.GetComponent<BriefElement>().briefPanel = this;
            obj.transform.SetSiblingIndex(briefElementHolder.childCount - 2);

            var perc = GetPercentages();
            float counter = 0;
            foreach (var item in perc)
            {
                counter += item.Value;
            }
            if (percentageNotificationPanel != null)
            {
                if (counter > 100)
                {
                    percentageNotificationPanel.gameObject.SetActive(true);
                    percentageNotificationPanel.GetChild(0).GetComponent<Text>().text = string.Format("The percentages of building {0}, floor {1} add to more than 100%!", floorPanel.currentElement.proceduralFloor.building.Index, floorPanel.currentElementIndex);
                }
                else if (counter < 100)
                {
                    percentageNotificationPanel.gameObject.SetActive(true);
                    percentageNotificationPanel.GetChild(0).GetComponent<Text>().text = string.Format("The percentages of building {0}, floor {1} don't add up to 100%!", floorPanel.currentElement.proceduralFloor.building.Index, floorPanel.currentElementIndex);
                }
                else
                {
                    percentageNotificationPanel.gameObject.SetActive(false);
                    percentageNotificationPanel.GetChild(0).GetComponent<Text>().text = "";
                }
            }
            //if (containsOtherThanCommercial)
            //{
            //    obj.GetComponent<BriefElement>().RemoveCommercialFromList();
            //}

            floorPanel.OnBriefChanged();
        }

        /// <summary>
        /// Adds a brief element to the list, with set values
        /// </summary>
        /// <param name="type">The apartment type</param>
        /// <param name="percentage">The brief percentage</param>
        public virtual void AddBriefElement(string type, float percentage)
        {
            GameObject obj = Instantiate(briefElementPrefab, briefElementHolder);
            obj.name = "BriefElement_" + (briefElementHolder.childCount - 1);
            obj.GetComponent<BriefElement>().Initialize(false);
            obj.GetComponent<BriefElement>().onChanged.AddListener(OnBriefElementChanged);
            obj.GetComponent<BriefElement>().onDeleted.AddListener(OnBriefElementDeleted);
            obj.transform.SetSiblingIndex(briefElementHolder.childCount - 2);
            bool isGroundFloor = false;
            if (floorPanel.currentElement.levels != null && floorPanel.currentElement.levels.Count > 0)
            {
                isGroundFloor = floorPanel.currentElement.levels.Count == 1 && floorPanel.currentElement.levels[0] == 0;
            }
            obj.GetComponent<BriefElement>().SetValues(type, percentage, isGroundFloor);
            briefElementHolder.GetComponent<RectTransform>().sizeDelta = new Vector3(briefElementHolder.GetComponent<RectTransform>().sizeDelta.x, briefElementHolder.childCount * 50);
            var perc = GetPercentages();
            float counter = 0;
            foreach (var item in perc)
            {
                counter += item.Value;
            }
            if (percentageNotificationPanel != null)
            {
                if (counter > 100)
                {
                    percentageNotificationPanel.gameObject.SetActive(true);
                    percentageNotificationPanel.GetChild(0).GetComponent<Text>().text = string.Format("The percentages of building {0}, floor {1} add to more than 100%!", floorPanel.currentElement.proceduralFloor.building.Index, floorPanel.currentElementIndex);
                }
                else if (counter < 100)
                {
                    percentageNotificationPanel.gameObject.SetActive(true);
                    percentageNotificationPanel.GetChild(0).GetComponent<Text>().text = string.Format("The percentages of building {0}, floor {1} don't add up to 100%!", floorPanel.currentElement.proceduralFloor.building.Index, floorPanel.currentElementIndex);
                }
                else
                {
                    percentageNotificationPanel.gameObject.SetActive(false);
                    percentageNotificationPanel.GetChild(0).GetComponent<Text>().text = "";
                }
            }
            //if (type == "Commercial" || type == "Other")
            //{
            //    addBriefButton.interactable = false;
            //    addBriefButton.GetComponent<UITooltip>().tooltip.text = "You cannot have another type together with Comm/Other!";
            //}
            //else
            //{
            //    addBriefButton.interactable = true;
            //    addBriefButton.GetComponent<UITooltip>().tooltip.text = "";
            //}
            //floorPanel.OnBriefChanged();
        }

        /// <summary>
        /// Loads a new brief element with given type and percentage
        /// </summary>
        /// <param name="type">The apartment type of the element</param>
        /// <param name="percentage">The percentage of the element</param>
        public virtual void LoadBriefElement(string type, float percentage)
        {
            GameObject obj = Instantiate(briefElementPrefab, briefElementHolder);
            obj.name = "BriefElement_" + (briefElementHolder.childCount - 1);
            obj.transform.SetSiblingIndex(briefElementHolder.childCount - 2);
            obj.GetComponent<BriefElement>().onChanged.AddListener(OnBriefElementChanged);
            obj.GetComponent<BriefElement>().onDeleted.AddListener(OnBriefElementDeleted);
            bool isGroundFloor = floorPanel.currentElement.levels.Count == 1 && floorPanel.currentElement.levels[0] == 0;
            obj.GetComponent<BriefElement>().LoadValues(type, percentage, isGroundFloor);
            briefElementHolder.GetComponent<RectTransform>().sizeDelta = new Vector3(briefElementHolder.GetComponent<RectTransform>().sizeDelta.x, briefElementHolder.childCount * 50);
            var perc = GetPercentages();
            float counter = 0;
            foreach (var item in perc)
            {
                counter += item.Value;
            }
            if (percentageNotificationPanel != null)
            {
                if (counter > 100)
                {
                    percentageNotificationPanel.gameObject.SetActive(true);
                    percentageNotificationPanel.GetChild(0).GetComponent<Text>().text = string.Format("The percentages of building {0}, floor {1} add to more than 100%!", floorPanel.currentElement.proceduralFloor.building.Index, floorPanel.currentElementIndex);
                }
                else if (counter < 100)
                {
                    percentageNotificationPanel.gameObject.SetActive(true);
                    percentageNotificationPanel.GetChild(0).GetComponent<Text>().text = string.Format("The percentages of building {0}, floor {1} don't add up to 100%!", floorPanel.currentElement.proceduralFloor.building.Index, floorPanel.currentElementIndex);
                }
                else
                {
                    percentageNotificationPanel.gameObject.SetActive(false);
                    percentageNotificationPanel.GetChild(0).GetComponent<Text>().text = "";
                }
            }
            //if (type == "Commercial" || type == "Other")
            //{
            //    addBriefButton.interactable = false;
            //    addBriefButton.GetComponent<UITooltip>().tooltip.text = "You cannot have another type together with Comm/Other!";
            //}
            //else
            //{
            //    addBriefButton.interactable = true;
            //    addBriefButton.GetComponent<UITooltip>().tooltip.text = "";
            //}
        }

        /// <summary>
        /// Called when a brief element has changed
        /// </summary>
        /// <param name="sender">The changed brief element</param>
        public virtual void OnBriefElementChanged(BriefElement sender)
        {
            //if (sender.type == "Commercial" || sender.type == "Other")
            //{
            //    addBriefButton.interactable = false;
            //    addBriefButton.GetComponent<UITooltip>().tooltip.text = "You cannot have another type together with Comm/Other!";
            //}
            //else
            //{
            //    addBriefButton.interactable = true;
            //    addBriefButton.GetComponent<UITooltip>().tooltip.text = "";
            //}
            var perc = GetPercentages();
            float counter = 0;
            foreach (var item in perc)
            {
                counter += item.Value;
            }
            if (percentageNotificationPanel != null)
            {
                if (counter > 100)
                {
                    percentageNotificationPanel.gameObject.SetActive(true);
                    percentageNotificationPanel.GetChild(0).GetComponent<Text>().text = string.Format("The percentages of building {0}, floor {1} add to more than 100%!", floorPanel.currentElement.proceduralFloor.building.Index, floorPanel.currentElementIndex);
                }
                else if (counter < 100)
                {
                    percentageNotificationPanel.gameObject.SetActive(true);
                    percentageNotificationPanel.GetChild(0).GetComponent<Text>().text = string.Format("The percentages of building {0}, floor {1} don't add up to 100%!", floorPanel.currentElement.proceduralFloor.building.Index, floorPanel.currentElementIndex);
                }
                else
                {
                    percentageNotificationPanel.gameObject.SetActive(false);
                    percentageNotificationPanel.GetChild(0).GetComponent<Text>().text = "";
                }
            }
            floorPanel.OnBriefChanged();
        }

        /// <summary>
        /// Called when a brief element has been deleted
        /// </summary>
        /// <param name="sender">The deleted brief element</param>
        public virtual void OnBriefElementDeleted(BriefElement sender)
        {
            StartCoroutine(RemoveElement(sender));
        }

        /// <summary>
        /// Returns the percentages of all the types currently used
        /// </summary>
        /// <returns></returns>
        public virtual Dictionary<string, float> GetPercentages()
        {
            Dictionary<string, float> percentages = new Dictionary<string, float>();
            for (int i = 0; i < briefElementHolder.childCount; i++)
            {
                var element = briefElementHolder.GetChild(i).GetComponent<BriefElement>();
                if (element != null)
                {
                    if (!percentages.ContainsKey(element.type))
                        percentages.Add(element.type, element.percentage);
                    else
                        percentages[element.type] += element.percentage;
                }
            }
            return percentages;
        }

        /// <summary>
        /// Clears the list of brief elements
        /// </summary>
        public virtual void ClearPrevious()
        {
            List<GameObject> toDelete = new List<GameObject>();
            for (int i = 0; i < briefElementHolder.childCount - 1; i++)
            {
                toDelete.Add(briefElementHolder.GetChild(i).gameObject);
            }

            for (int i = 0; i < toDelete.Count; i++)
            {
                DestroyImmediate(toDelete[i]);
            }
            addBriefButton.interactable = true;
            addBriefButton.GetComponent<UITooltip>().tooltip.text = "";
        }
        #endregion

        #region Private Methods
        protected virtual IEnumerator RemoveElement(BriefElement sender)
        {
            for (int i = 0; i < briefElementHolder.childCount - 1; i++)
            {
                if (briefElementHolder.GetChild(i).GetComponent<BriefElement>() == sender)
                {
                    Destroy(briefElementHolder.GetChild(i).gameObject);
                }
            }
            yield return new WaitForEndOfFrame();
            var perc = GetPercentages();
            float counter = 0;
            foreach (var item in perc)
            {
                counter += item.Value;
            }
            if (percentageNotificationPanel != null)
            {
                if (counter > 100)
                {
                    percentageNotificationPanel.gameObject.SetActive(true);
                    percentageNotificationPanel.GetChild(0).GetComponent<Text>().text = string.Format("The percentages of building {0}, floor {1} add to more than 100%!", floorPanel.currentElement.proceduralFloor.building.Index, floorPanel.currentElementIndex);
                }
                else if (counter < 100)
                {
                    percentageNotificationPanel.gameObject.SetActive(true);
                    percentageNotificationPanel.GetChild(0).GetComponent<Text>().text = string.Format("The percentages of building {0}, floor {1} don't add up to 100%!", floorPanel.currentElement.proceduralFloor.building.Index, floorPanel.currentElementIndex);
                }
                else
                {
                    percentageNotificationPanel.gameObject.SetActive(false);
                    percentageNotificationPanel.GetChild(0).GetComponent<Text>().text = "";
                }
            }
            floorPanel.OnBriefChanged();
        }

        protected virtual void AddEvents()
        {
            //BriefElement.changed += OnBriefElementChanged;
            //BriefElement.deleted += OnBriefElementDeleted;
        }

        protected virtual void RemoveEvents()
        {
            //BriefElement.changed -= OnBriefElementChanged;
            //BriefElement.deleted -= OnBriefElementDeleted;
        }

        #endregion
    }
}

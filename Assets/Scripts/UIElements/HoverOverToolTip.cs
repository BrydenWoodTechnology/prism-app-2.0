﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace BrydenWoodUnity.UIElements
{
    public class HoverOverToolTip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public Text tooltipText;

        private Selectable uiComponent;

        // Use this for initialization
        void Start()
        {
            uiComponent = GetComponent<Selectable>();
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (!uiComponent.interactable)
            {
                tooltipText.gameObject.SetActive(true);
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            tooltipText.gameObject.SetActive(false);
        }
    }
}

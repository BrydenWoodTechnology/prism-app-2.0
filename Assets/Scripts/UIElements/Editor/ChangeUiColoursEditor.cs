﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

[CustomEditor(typeof(ChangeUIcolours))]
public class ChangeUiColoursEditor : Editor
{  
    
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        ChangeUIcolours changeUIcolours = (ChangeUIcolours)target;
        if(GUILayout.Button("Change Colours"))
        {
            changeUIcolours.Run(changeUIcolours.canvas);
        }

        if (GUILayout.Button("Change Font"))
        {
            changeUIcolours.ChangeFont(changeUIcolours.canvas);
        }

        if (GUILayout.Button("Change Element Navigation"))
        {
            changeUIcolours.ElementNavigation(changeUIcolours.canvas);
        }
    }


}

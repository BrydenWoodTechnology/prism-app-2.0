﻿using BrydenWoodUnity.DesignData;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    public class BlockElement : MonoBehaviour
    {
        public BlocksPanel blocksPanel;
        [HideInInspector]
        public MansionBlock MansionBlock 
        { 
            get
            {
                return mansionBlock;
            }

            set
            {
                mansionBlock = value;
                for (int i=0; i<apt1.options.Count; i++)
                {
                    if (mansionBlock.firstApartment == apt1.options[i].text)
                    {
                        apt1.SetValue(i);
                        break;
                    }
                }
                apt1.RefreshShownValue();
                for (int i = 0; i < apt2.options.Count; i++)
                {
                    if (mansionBlock.secondApartment == apt2.options[i].text)
                    {
                        apt2.SetValue(i);
                        break;
                    }
                }
                apt2.RefreshShownValue();
                aptImage1.color = Standards.TaggedObject.ApartmentTypesColours[mansionBlock.firstApartment];
                aptImage2.color = Standards.TaggedObject.ApartmentTypesColours[mansionBlock.secondApartment];
            } 
        }
        public Dropdown apt1;
        public Dropdown apt2;
        public Image aptImage1;
        public Image aptImage2;
        private MansionBlock mansionBlock;
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void SetFirstApartmentType(int val)
        {
            MansionBlock.firstApartment = MansionBlock.ApartmentTypes[val];
            aptImage1.color = Standards.TaggedObject.ApartmentTypesColours[MansionBlock.ApartmentTypes[val]];
            blocksPanel.ChangeBlock(this);
        }

        public void SetSecondApartmentType(int val)
        {
            MansionBlock.secondApartment = MansionBlock.ApartmentTypes[val];
            aptImage2.color = Standards.TaggedObject.ApartmentTypesColours[MansionBlock.ApartmentTypes[val]];
            blocksPanel.ChangeBlock(this);
        }

        public void OnDeleteElement()
        {
            blocksPanel.RemoveBlock(this);
        }

        public void OnElementSwitchedPosition()
        {
            blocksPanel.OnElementSwitchedPosition();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace BrydenWoodUnity.UIElements
{
    public class UICustomPolygonSettings : MonoBehaviour {

        public Text titleMix;
        public Text polygonName;
        public InputField floorsInput;
        public InputField heightInput;

        public CustomPolygonEventArgs eventArgs;

        public OnCustomPolygonChanged onCustomPolygonChanged;


        // Use this for initialization
        void Start() {
            eventArgs = new CustomPolygonEventArgs()
            {
                floors = 2,
                floor2Floor = 4.5f,
                index = transform.GetSiblingIndex()
            };
        }

        // Update is called once per frame
        void Update() {

        }

        public void SetFloors()
        {
            eventArgs.floors = int.Parse(floorsInput.text);
            onCustomPolygonChanged.Invoke(eventArgs);
        }
        public void SetHeight()
        {
            eventArgs.floor2Floor = float.Parse(heightInput.text);
            onCustomPolygonChanged.Invoke(eventArgs);
        }

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for toggling a button with text
    /// </summary>
    public class ToggleButtonText : MonoBehaviour
    {
        #region Public Fields and Properties
        [Header("Scene References:")]
        public Text label;

        [Header("User Input:")]
        public string onText;
        public string offText;
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            if (GetComponent<Toggle>().isOn)
            {
                label.text = onText;
            }
            else
            {
                label.text = offText;
            }
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Called when the button is pressed
        /// </summary>
        /// <param name="isOn">Toggles the text</param>
        public void OnPressed(bool isOn)
        {
            if (isOn)
            {
                label.text = onText;
            }
            else
            {
                label.text = offText;
            }
        }
        #endregion
    }
}

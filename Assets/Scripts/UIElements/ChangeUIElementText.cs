﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeUIElementText : MonoBehaviour
{
    public Text text;
    public string optionA;
    public string optionB;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeText(bool isOn)
    {
        if (isOn)
        {
            text.text = optionA;
        }
        else
        {
            text.text = optionB;
        }
    }
}

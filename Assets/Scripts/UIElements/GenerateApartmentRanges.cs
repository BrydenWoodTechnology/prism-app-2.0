﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BrydenWoodUnity.DesignData;
using BrydenWoodUnity.UIElements;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for generating the apartment area ranges
    /// </summary>
    public class GenerateApartmentRanges : MonoBehaviour
    {
        #region Public Fields and Properties
        [Header("Assets References:")]
        public GameObject apartmentTypePrefab;
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Generates an area range for a specific apartment type
        /// </summary>
        public void GenerateApartType()
        {
            foreach (var item in Standards.TaggedObject.ApartmentTypesMinimumSizes)
            {
                GameObject obj = Instantiate(apartmentTypePrefab, transform);
                string type = item.Key;
                double min = item.Value;
                double max = Standards.TaggedObject.ApartmentTypesMaximumSizes[type];

                var areaRange = obj.GetComponent<ApartmentAreaRange>();
                areaRange.type = type;
                areaRange.originalMaxArea = (float)max;
                areaRange.originalMinArea = (float)min;

                obj.transform.GetChild(1).GetComponent<InputField>().text = min + "-" + max;
                obj.transform.GetChild(0).GetComponent<Text>().text = "Apt: " + type;
            }
        }
        #endregion
    }
}

﻿using BrydenWoodUnity.DesignData;
using ChartAndGraph;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A base class for chart graphs regarding statistics of Design Objects
    /// </summary>
    public class DesignDataChart : MonoBehaviour
    {
        #region Public Fields and Properties
        [Header("Scene References:")]
        public Text title;
        public CanvasPieChart chart;

        [Header("Assets References:")]
        public Material chartMaterial;

        [HideInInspector]
        public BaseDesignData designData;
        [HideInInspector]
        public bool selected = false;
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        public virtual void Initialize()
        {
            chart.DataSource.Clear();
            chart.GetComponent<ItemLabels>().TextFormat.Suffix = " m\xB2";
        }

        public virtual void SetChartValues(BaseDesignData designData, bool original = false)
        {

        }

        public virtual void SetChartValues(Dictionary<string, float> areas)
        {

        }

        public virtual void UpdateChartValues(BaseDesignData designData)
        {

        }

        public virtual void OnHoverOver(PieChart.PieEventArgs eventArgs)
        {

        }

        public virtual void OnNonHovered()
        {

        }

        public virtual void OnClicked(PieChart.PieEventArgs eventArgs)
        {

        }
        #endregion
    }
}

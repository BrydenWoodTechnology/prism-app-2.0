﻿using BrydenWoodUnity.UIElements;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    public class DynamicDimensions : MonoBehaviour
    {
        #region Public Fields and Properties
        [Header("Referenced Elements")]
        public Image line;
        public Vector3 startAnchor;
        public Vector3 endAnchor;
        public InputField dimensionInput;
        public GameObject input_box;
        public Detect_me detector;
        public Text displayValue;

        [Space(10)]
        [Header("User-defined Values")]
        [Tooltip("Define how many pixels on the screen correspond to one meter")]
        public float scale;
        public float lineThickness;
        public bool readOnly;
        public bool horizontal;
        public bool enableDrag;
        public bool upSide_outSide;

        [Space(10)]
        [Header("Progress Tracking Values")]
        public float length;
        public Vector2 mouseStartPos;
        public Vector2 mouseCurrentPos;
        public Vector2 mouseProjectedPos;
        public float Value
        {
            get
            {
                float val = 0.0f;
                float.TryParse(dimensionInput.text, out val);
                return val;
            }
        }
        #endregion

        #region Private Fields and Properties
        private float lastLength { get; set; }
        private float magnitude { get; set; }
        private float diff { get; set; }
        private Vector3 end_line { get; set; }
        private bool stretchDim { get; set; }
        private bool userDim { get; set; }
        #endregion


        #region Events
        /// <summary>
        /// Used when the value of the dimension has changed
        /// </summary>
        /// <param name="sender">The dimension which changed</param>
        public delegate void DimensionEvent(DynamicDimensions sender);
        /// <summary>
        /// Triggered when the value of the dimension has changed
        /// </summary>
        public event DimensionEvent valueChanged;
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            stretchDim = false;
            userDim = false;
            //SetLine();
        }

        // Update is called once per frame
        void Update()
        {
            if (enableDrag)
            {
                DragDimensions();
            }
        }

        void OnDestroy()
        {
            if (valueChanged != null)
            {
                foreach (Delegate del in valueChanged.GetInvocationList())
                {
                    valueChanged -= (DimensionEvent)del;
                }
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Sets the Input Field of the dimension as cancelable
        /// </summary>
        public void SetAsCancelable()
        {
            dimensionInput.onEndEdit.RemoveAllListeners();
            dimensionInput.gameObject.AddComponent<CancelableInputfield>().allProject = true;
            dimensionInput.onEndEdit.AddListener(dimensionInput.gameObject.GetComponent<CancelableInputfield>().OnValueChanged);
        }

        /// <summary>
        /// Method to create the dimesions line from startAnchor to endAnchor
        /// </summary>
        public void SetLine()
        {
            float start_height = lineThickness;
            magnitude = Vector3.Distance(startAnchor, endAnchor);
            length = magnitude;
            if (!readOnly)
            {
                if (dimensionInput != null)
                {
                    dimensionInput.SetValue((magnitude / scale).ToString("0.00"));
                }
                displayValue.gameObject.SetActive(false);
            }
            else
            {
                foreach (Transform t in input_box.transform)
                {
                    if (t.gameObject != displayValue.gameObject)
                    {
                        t.gameObject.SetActive(false);
                    }
                }
                displayValue.text = (magnitude / scale).ToString("0.00");
            }

            line.rectTransform.sizeDelta = new Vector2(magnitude / transform.root.localScale.x, start_height);
            line.transform.position = startAnchor;
            Vector3 direction = endAnchor - startAnchor;
            Vector3 zeroDirection = new Vector3(startAnchor.x + 10, startAnchor.y, startAnchor.z) - startAnchor;
            float angle = Vector3.Angle(zeroDirection, direction);
            var cross = Vector3.Cross(zeroDirection, direction).normalized;
            line.transform.rotation = Quaternion.Euler(0, 0, cross.z != 0 ? cross.z * angle : angle);
            if (angle == 180)
            {
                input_box.transform.localEulerAngles = new Vector3(0, 0, -180);
            }
            Vector3 box_pos = input_box.transform.localPosition;
            if (upSide_outSide)
            {
                input_box.transform.localPosition = new Vector3(box_pos.x, 20 * (angle == 180 ? -1 : 1), box_pos.z);
            }
            else
            {
                input_box.transform.localPosition = new Vector3(box_pos.x, -20 * (angle == 180 ? -1 : 1), box_pos.z);
            }
        }



        /// <summary>
        /// Update the line once the length has been externally altered
        /// </summary>
        public void UpdateLine()
        {
            float width = length;
            float height = line.rectTransform.rect.height;
            line.transform.position = startAnchor;
            line.rectTransform.sizeDelta = new Vector2(width / transform.root.localScale.x, height);
        }


        /// <summary>
        /// Update the length of the line once new value <para>input</para> has been inserted by the user through the Input field
        /// </summary>
        /// <param name="input">THe input field from which the values should be taken</param>
        public void UpdateLength(InputField input)
        {
            if (input.text != "")
            {
                float result;
                if (float.TryParse(input.text, out result))
                {
                    length = result * scale;
                    UpdateLine();
                    if (valueChanged != null)
                    {
                        valueChanged(this);
                    }
                }
                else
                {
                    Debug.Log("Input is not a float value");
                }
            }
        }


        /// <summary>
        /// Change line length while dragging the mouse on either x or y axis
        /// </summary>
        public void DragDimensions()
        {
            if (detector.enter)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    end_line = endAnchor;
                    mouseStartPos = Input.mousePosition;
                    lastLength = length;
                    stretchDim = true;
                }

            }
            if (Input.GetMouseButtonUp(0))
            {
                if (stretchDim)
                {
                    if (valueChanged != null)
                    {
                        valueChanged(this);
                    }
                }
                stretchDim = false;
            }
            if (stretchDim)
            {
                mouseCurrentPos = Input.mousePosition;
                Vector2 mouseDir = mouseCurrentPos - mouseStartPos;
                Debug.DrawLine(mouseCurrentPos, mouseStartPos, Color.red);
                Vector3 dimDir = (endAnchor - startAnchor);

                mouseProjectedPos = Vector3.Project((Vector3)mouseDir, dimDir) + startAnchor;
                Debug.DrawLine(mouseStartPos, mouseProjectedPos - mouseStartPos, Color.blue);
                CalculateDimensions(mouseDir, dimDir);
            }
        }

        /// <summary>
        /// Calculate the new dimensions of the line based on the mouse dragging
        /// </summary>
        public void CalculateDimensions(Vector2 mouseOr, Vector2 dimOr)
        {
            Vector2 projectedV = mouseProjectedPos - (Vector2)startAnchor;
            var cross2 = Vector3.Cross(mouseOr, dimOr).normalized;
            float aangle = Vector2.Angle(mouseOr, dimOr);
            if (aangle < 90)
            {
                diff = projectedV.magnitude;
            }
            else
            {
                diff = -projectedV.magnitude;
            }
            length = lastLength + diff;
            if (length >= 0)
            {
                UpdateLine();
                dimensionInput.SetValue((length / scale).ToString("0.00"));
            }
        }
        #endregion
    }
}

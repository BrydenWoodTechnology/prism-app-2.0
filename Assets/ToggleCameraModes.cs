﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace BrydenWoodUnity.Navigation
{
    public class ToggleCameraModes : MonoBehaviour
    {

        public GameObject[] images;
        public int mode = 0;
        public OnToggleCameraMode toggleCameraMode;
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void ToggleModes()
        {
            mode = (mode + 1) % images.Length;
            for (int i = 0; i < images.Length; i++)
            {
                images[i].SetActive(false);
            }
            images[(mode + 1) % images.Length].SetActive(true);
            toggleCameraMode.Invoke(mode);
        }
    }

    [Serializable]
    public class OnToggleCameraMode : UnityEvent<int>
    {

    }
}

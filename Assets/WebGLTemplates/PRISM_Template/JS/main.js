const xbtn = document.querySelector(".x-btn");
const overlay = document.querySelector(".overlay");
const submit = document.querySelector(".submit-btn");
const input = document.querySelector(".input-url");
const landingPage = document.querySelector("#landing-page");
const creditsPage = document.querySelector("#credits-page");
const createbtn = document.querySelector("#create");
const loadbtn = document.querySelector("#load");
const loadsamplebtn = document.querySelector("#load-sample");
const prismlogo = document.querySelector("#prism-logo");
const connection = document.querySelector("#connection-icon");
const closeVideoBtn = document.querySelector("#close-video");

//------ Event Listeners -------//
xbtn.addEventListener("click", function () {
    overlay.classList.add("hidden");
});

submit.addEventListener("click", function () {
    overlay.classList.add("hidden");
    //let latlon = getLatLon();
    console.log(input.value);
    gameInstance.SendMessage("HtmlUIManager", "OnUrlReceived", input.value);
});

createbtn.addEventListener("click", function () {
    landingPage.classList.add("hidden");
    document.querySelector("#how-to").classList.remove("hidden");
    gameInstance.SendMessage("HtmlUIManager", "OnCreateProject", '');
});
loadbtn.addEventListener("click", function () {
    landingPage.classList.add("hidden");
    gameInstance.SendMessage("HtmlUIManager", "OnLoadProject", '');
});

loadsamplebtn.addEventListener("click", async function () {
    landingPage.classList.add("hidden");
    const text = await getLocalFile("PRISM_SAMPLE_PROJECT");
    gameInstance.SendMessage("HtmlUIManager", "OnLoadSamplProject", JSON.stringify(text));
});

prismlogo.addEventListener("click", function () {
    creditsPage.classList.toggle("hidden");
});

closeVideoBtn.addEventListener("click", function () {
    document.querySelector("#how-to").classList.add("hidden");
});

// Update the online status icon based on connectivity
window.addEventListener('online', updateOnlineIndicator);
window.addEventListener('offline', updateOfflineIndicator);
updateOnlineIndicator();

//------ Functions -------//
function initialise() {
    overlay.classList.remove("hidden");
    input.value = "";
    window.open('https://www.openstreetmap.org/#map=13/51.5031/-0.1153', '_blank');
}

function getLatLon() {
    if (input.innerText != "") {
        let path = input.innerText.split('#map=');
        let coords = path[1].split('/');
        let latlon = coords[1] + "," + coords[2];
        console.log(latlon);
        return latlon;
    }
}

function updateOnlineIndicator() {
    connection.classList.add("hidden");
    gameInstance.SendMessage("HtmlUIManager", "OnBrowserOffline", 'True');
}
function updateOfflineIndicator() {
    connection.classList.remove("hidden");
    gameInstance.SendMessage("HtmlUIManager", "OnBrowserOffline", 'False');
}

function openPlatformsGraph() {
    window.open('systemsEvaluation.html', '_blank');
}

async function getLocalFile(filename) {
    const filetext = await fetch(`./Data/${filename}.json`, { mode: 'no-cors' })
        .then(response => response.json())
        .then(data => { return data })
        .catch(error => console.error(error));

    return filetext;
}



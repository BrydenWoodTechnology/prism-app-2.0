mergeInto(LibraryManager.library, {

  Hello: function () {
    window.alert("Hello, world!");
  },

  HelloString: function (str) {
    window.alert(Pointer_stringify(str));
  },

  PrintFloatArray: function (array, size) {
    for (var i = 0; i < size; i++)
      console.log(HEAPF32[(array >> 2) + i]);
  },

  AddNumbers: function (x, y) {
    return x + y;
  },

  StringReturnValueFunction: function () {
    var returnStr = "bla";
    var bufferSize = lengthBytesUTF8(returnStr) + 1;
    var buffer = _malloc(bufferSize);
    stringToUTF8(returnStr, buffer, bufferSize);
    return buffer;
  },

  BindWebGLTexture: function (texture) {
    GLctx.bindTexture(GLctx.TEXTURE_2D, GL.textures[texture]);
  },

  // function for getting API response to unity  by page.
  ReturnFromAPI: function () {

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", 'http://api-node.bwtcreativetech.io/18050-REM/M1J35aJ39_Topography?pagination=true', false); // false for synchronous request
    xmlHttp.setRequestHeader("Authorization", "Basic " + btoa("18050-Unity-Web:haTB9s38fBNU"));
    xmlHttp.withCredentials = true;
    xmlHttp.send(null);

    var returnStr = xmlHttp.responseText;
    var bufferSize = lengthBytesUTF8(returnStr) + 1;
    var buffer = _malloc(bufferSize);
    stringToUTF8(returnStr, buffer, bufferSize);
    return buffer;
  },

  // function for getting API response to unity. This function takes URL argument as string.


  ReturnFromAPIurl: function (requestURL) {

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", Pointer_stringify(requestURL), false); // false for synchronous request
    xmlHttp.setRequestHeader("Authorization", "Basic " + btoa("19026-configure:9fKDtwIpLZWkt4PD"));
    xmlHttp.withCredentials = true;
    xmlHttp.send(null);

    var returnStr = xmlHttp.responseText;
    var bufferSize = lengthBytesUTF8(returnStr) + 1;
    var buffer = _malloc(bufferSize);
    stringToUTF8(returnStr, buffer, bufferSize);
    return buffer;
  },

  // function (asynchronous) for getting API response to unity. This function takes URL argument as string.

  ReturnFromAPIurlAsync: function (requestURL) {

    var xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function () {
      // below only runs if request is completed successfully. Handle other situations.
      if (this.readyState == 4 && this.status == 200) {
        // Typical action to be performed when the response is ready.

        var returnStr = xhr.responseText;
        //convert and send the response to unity
        var bufferSize = lengthBytesUTF8(returnStr) + 1;
        var buffer = _malloc(bufferSize);
        stringToUTF8(returnStr, buffer, bufferSize);
        return buffer;
      }
    };


    xhr.open("GET", Pointer_stringify(requestURL), true); // true for asynchronous request
    xhr.setRequestHeader("Authorization", "Basic " + btoa("19026-configure:9fKDtwIpLZWkt4PD"));
    xhr.withCredentials = true;
    xhr.send(null);
  },


  // function for call a js function
  CallFunction: function (methodName, arg) {
    var method = Pointer_stringify(methodName);
    var args = Pointer_stringify(arg);
    window[method](args);
    //window.dispatchEvent(args);
  },

  // function for call a js function
  CallEvent: function (arg) {    
    var args = Pointer_stringify(arg);    
    window.dispatchEvent(args);
  }




});
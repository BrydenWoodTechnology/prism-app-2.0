mergeInto(LibraryManager.library, {
  
ImportPDFLibrary: function()
	{
		var my_awesome_script = document.createElement('script');
		my_awesome_script.setAttribute('src','https://unpkg.com/jspdf@latest/dist/jspdf.min.js');
		document.head.appendChild(my_awesome_script);
	},

ExportPdfReport: function(data,fileName)
	{
		var fName = Pointer_stringify(fileName);
		var dtStr = Pointer_stringify(data);
		
		var aptColours = {'SpecialApt':[40,40,40],
			'Studio':[103,12,61],
                        '1b2p':[123,32,81],
                        '2b3p':[229,217,223],
                        '2b4p':[202,174,188],
                        '3b5p':[164,105,136]};
		
		var trafficColours = 
		{
			0:[255,0,0],
			1:[255,255,0],
			2:[0,255,0]
		};

		var imgData = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAckAAAA5CAIAAADSuEeLAAAACXBIWXMAAAsTAAALEwEAmpwYAAAFuGlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMxNDIgNzkuMTYwOTI0LCAyMDE3LzA3LzEzLTAxOjA2OjM5ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdEV2dD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlRXZlbnQjIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOnBob3Rvc2hvcD0iaHR0cDovL25zLmFkb2JlLmNvbS9waG90b3Nob3AvMS4wLyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxOCAoV2luZG93cykiIHhtcDpDcmVhdGVEYXRlPSIyMDE4LTEyLTA3VDEwOjM4OjM4WiIgeG1wOk1ldGFkYXRhRGF0ZT0iMjAxOC0xMi0wN1QxMDozODozOFoiIHhtcDpNb2RpZnlEYXRlPSIyMDE4LTEyLTA3VDEwOjM4OjM4WiIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo1ZWI1OGQ0Ni05MWFkLTcxNGQtODY5Ny1jMjlhMzRhMWVlMDgiIHhtcE1NOkRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDpjMDUyNzgzYi02NjQyLWQxNDUtYjA3Ni05ODYxNDYxZWJiMmUiIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDozMTU3ZGM0ZS05MzQ0LTBiNDAtYmJjYi04NWNlNTQwM2U4Y2UiIGRjOmZvcm1hdD0iaW1hZ2UvcG5nIiBwaG90b3Nob3A6Q29sb3JNb2RlPSIzIj4gPHhtcE1NOkhpc3Rvcnk+IDxyZGY6U2VxPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0iY3JlYXRlZCIgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDozMTU3ZGM0ZS05MzQ0LTBiNDAtYmJjYi04NWNlNTQwM2U4Y2UiIHN0RXZ0OndoZW49IjIwMTgtMTItMDdUMTA6Mzg6MzhaIiBzdEV2dDpzb2Z0d2FyZUFnZW50PSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxOCAoV2luZG93cykiLz4gPHJkZjpsaSBzdEV2dDphY3Rpb249InNhdmVkIiBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOjVlYjU4ZDQ2LTkxYWQtNzE0ZC04Njk3LWMyOWEzNGExZWUwOCIgc3RFdnQ6d2hlbj0iMjAxOC0xMi0wN1QxMDozODozOFoiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE4IChXaW5kb3dzKSIgc3RFdnQ6Y2hhbmdlZD0iLyIvPiA8L3JkZjpTZXE+IDwveG1wTU06SGlzdG9yeT4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7G+YqjAAABu0lEQVR4nO3UUW6jMBSG0Wt3EbPGrKirbZgHKLGBMJHmfzxHETUXJ20o+trj8fj+811VVdWGH21fHk6rqk2nrU3ry+F+qd2f7sN13V4b+rihVWvV63XcJsOevh/bdlxP++97+/Cu7aPaa//69q/TpLfp1dpxsn1U39bTtvqd/+uj1qtf7fqv2o59+oL7dz9+weEuTZPTvRrvbY339v2/qcb987YaT+v9cH7krp/A4ZGbLPNimS8tp53LfOl+uFxdWq4ura/nm23P2/VhchgeJofjefEzTJ7z+n8m99vOv/pycTN8dzdu7tXnr/rg9MMn5OaRu3oCewGQpq0AedoKkKetAHnaCpCnrQB52gqQp60AedoKkKetAHnaCpCnrQB52gqQp60AedoKkKetAHnaCpCnrQB52gqQp60AedoKkKetAHnaCpCnrQB52gqQp60AedoKkKetAHnaCpCnrQB52gqQp60AedoKkKetAHnaCpCnrQB52gqQp60AedoKkKetAHnaCpCnrQB52gqQp60AedoKkKetAHnaCpCnrQB52gqQp60AedoKkKetAHnaCpCnrQB52gqQp60AeX8BJJi8cTlF0QcAAAAASUVORK5CYII=';
      var format = 'png';

		var contact = {'BW':['tel. +44(0)20-7253-4772\r\nemail info@brydenwood.co.uk\r\nweb brydenwood.co.uk','Bryden Wood\r\n100 Gray\'s Inn Road,\r\nLondon, WC1X 8AL, UK']}
		
		var obj = JSON.parse(dtStr);
		console.log(obj);
		
		var dc = new jsPDF('landscape');
		
		//Setting the first page of the document
      dc.setFontSize(10);
      dc.setTextColor(40);
      dc.text(obj.date, 10,10);

      dc.setLineWidth(0.2);
      dc.setDrawColor(0);
      dc.line(10,15,287,15);

      dc.setFontSize(16);
      dc.setTextColor(0,0,255);
      dc.text('Precision Manufactured Housing For London', 10,25);

      dc.setFontSize(14);
      dc.setTextColor(0);
      dc.text('Configuration Report Tool', 10,35);

      dc.setFontSize(6);
      dc.text(contact['BW'][0], 220,200);
      dc.text(contact['BW'][1], 260,200);

      //console.log(dc.convertStringToImageData(obj.images[0]));
      //dc.addImage(dc.convertStringToImageData(obj.images[0]), 'JPEG', 10,40,277,155);

	//Setting general metrics page of the document
      dc.addPage();

      dc.setFontSize(12);
      dc.setTextColor(0);
      dc.text('1.0 Building Metrics', 10,14);

      dc.setLineWidth(0.2);
      dc.setDrawColor(0);
      dc.line(10,15,287,15);

      dc.setFontSize(14);
      dc.setFontStyle("bold");
      dc.text('General:', 15,25);

      dc.setFontSize(12);
      dc.setFontStyle("normal");
      dc.text('location ' + obj.location, 15,35);
      dc.text('Site Area: ' + obj.siteArea + ' m\xB2', 15,43);
      dc.text('PTAL Rating: ' + obj.ptalRating, 15,51);
      dc.text('Density (units / hectare): ' + obj.unitDensity, 15,59);
      dc.text('Density (habitable rooms / hectare): ' + obj.habitableDensity, 15,67);
      dc.text('GEA: ' + obj.gea + ' m\xB2', 15,75);
      dc.text('GIA: ' + obj.gia + ' m\xB2', 15,83);
      dc.text('NIA: ' + obj.nia + ' m\xB2', 15,91);
      dc.text('Net to Gross: ' + obj.netgross + ' %', 15,99);

	dc.setFontSize(10);
      dc.text('40%', 70,99);
      dc.addImage(imgData, format, 80, 95, 45.7, 5.7, undefined, 'medium');
      dc.text('80%', 80+47,99);
      dc.setFillColor(0,0,0);
      dc.rect(80 + ((obj.netgross-40)/40)*45.7,94,1,7.7,'F');

      dc.setFontSize(14);
      dc.setFillColor(118,183,178);
      dc.rect(20,190,5,-80,'F');
      dc.text('GEA', 20,200);

      dc.setFillColor(89,161,79);
      dc.rect(35,190,5,-80 * (obj.gia/obj.gea),'F');
      dc.text('GIA', 35,200);

      dc.setFillColor(237,201,72);
      dc.rect(50,190,5,-80 * (obj.nia/obj.gea),'F');
      dc.text('NIA', 50,200);

      dc.setFontSize(14);
      dc.setFontStyle("bold");
      dc.text('Apartment Types:', 150,25);

      dc.setFontStyle("normal");

      var startPointX = 150;
      var c1 = 0;
      var c2 = 0;
      var totalApts = 0;
      for (var key in obj.aptNumbers)
      {
        var cells = obj.aptNumbers[key].split(",");
        totalApts = totalApts + parseInt(cells[1],10);
      }

	dc.setFontSize(12);
      for (var key in obj.aptNumbers)
      {
        var cells = obj.aptNumbers[key].split(",");
        var value = parseInt(cells[1],10);
        var perc = ((value/totalApts)*100).toFixed(1);
        dc.text(key+': '+cells[0]+' with area of '+value+' m\xB2 ('+perc+'%)', 150, 35 + c1*8);
        console.log(key);
        dc.setFillColor(aptColours[key][0],aptColours[key][1],aptColours[key][2]);
        dc.rect(startPointX,90,100*(perc/100),10,'F');
        startPointX = startPointX + 100*(perc/100);
        c1 ++;
      }

	  dc.setFontSize(14);
      dc.setFontStyle("bold");
      dc.text(obj.ConstructionFeaturesName + ' Construction Features:', 150,120);
      dc.setFontSize(12);
      dc.setFontStyle("normal");
      c1 = 0;
      for (var key2 in obj.ConstructionFeatures)
      {
        dc.text(key2+': '+obj.ConstructionFeatures[key2]+' m',150,130+c1*8);
        c1++;
      }
      dc.setFontSize(14);

	  //Setting the pages for each individual building

      for (j=0; j<obj.buildings.length; j++)
      {
        dc.addPage();

        dc.setFontSize(12);
        dc.setTextColor(0);
        dc.text('1.'+(j+1)+'.0 Building Metrics', 10,14);

        dc.setLineWidth(0.2);
        dc.setDrawColor(0);
        dc.line(10,15,287,15);

        dc.setFontSize(14);
        dc.setFontStyle("bold");
        dc.text(obj.buildings[j].buildingName+' Data:', 15,25);

        dc.setFontSize(12);
        dc.setFontStyle("normal");
        dc.text('GEA: ' + obj.buildings[j].gea + ' m\xB2', 15,35);
        dc.text('GIA: ' + obj.buildings[j].gia + ' m\xB2', 15,43);
        dc.text('NIA: ' + obj.buildings[j].nia + ' m\xB2', 15,51);
        dc.text('Net to Gross: ' + obj.buildings[j].netgross + ' %', 15,59);

	dc.setFontSize(10);
        dc.text('40%', 70,59);
        dc.addImage(imgData, format, 80, 55, 45.7, 5.7, undefined, 'medium');
        dc.text('80%', 80+47,59);
        dc.setFillColor(0,0,0);
        dc.rect(80 + ((obj.buildings[j].netgross-40)/40)*45.7,54,1,7.7,'F');

        dc.setFontSize(14);
        dc.setFillColor(118,183,178);
        dc.rect(20,190,5,-80,'F');
        dc.text('GEA', 20,200);

        dc.setFillColor(89,161,79);
        dc.rect(35,190,5,-80 * (obj.buildings[j].gia/obj.buildings[j].gea),'F');
        dc.text('GIA', 35,200);

        dc.setFillColor(237,201,72);
        dc.rect(50,190,5,-80 * (obj.buildings[j].nia/obj.buildings[j].gea),'F');
        dc.text('NIA', 50,200);

        dc.setFontSize(14);
        dc.setFontStyle("bold");
        dc.text('Apartment Types:', 150,25);

        dc.setFontStyle("normal");

        var startPointX = 150;
        var c1 = 0;
        var c2 = 0;
        var totalApts = 0;
        for (var key in obj.buildings[j].aptNumbers)
        {
          var cells = obj.buildings[j].aptNumbers[key].split(",");
          totalApts = totalApts + parseInt(cells[1],10);
        }

        dc.setFontSize(12);
        for (var key in obj.buildings[j].aptNumbers)
        {
          var cells = obj.buildings[j].aptNumbers[key].split(",");
          var value = parseInt(cells[1],10);
          var perc = ((value/totalApts)*100).toFixed(1);
          dc.text(key+': '+cells[0]+' with area of '+value+' m\xB2 ('+perc+'%)', 150, 35 + c1*8);
          console.log(key);
          dc.setFillColor(aptColours[key][0],aptColours[key][1],aptColours[key][2]);
          dc.rect(startPointX,90,100*(perc/100),10,'F');
          startPointX = startPointX + 100*(perc/100);
          c1 ++;
        }
        dc.setFontSize(14);

        //SEtting the modules manufacturing methods page of the document
        dc.addPage();

        dc.setFontSize(12);
        dc.setTextColor(0);
        dc.text('1.'+(j+1)+'.1 Systems Analysis / Volumetric (Category 1)', 10,14);

        dc.setLineWidth(0.2);
        dc.setDrawColor(0);
        dc.line(10,15,287,15);

	dc.setFontStyle('bold');
        dc.text("System:",15,22);
        dc.setFontSize(10);
        dc.text("Total Height",117,22);
        dc.text("Floor 2 Floor", 148,22);
        dc.text("Party Wall", 178,22);
        dc.text("Exterior Wall", 203,22);
        dc.text("Element Size", 231,22);
        dc.text("Span", 266,22);
        dc.setFontSize(12);

        var yConstrVal = 30;
        for (var key in obj.buildings[j].constructionSystems)
        {
          if (key.includes("Modules"))
          {
          dc.setFontSize(10);
          dc.setFontStyle('bold');
          dc.text(key + ': ', 15,yConstrVal);

          for (c=0; c<obj.buildings[j].constructionSystems[key].feasibility.length; c++)
          {
            dc.setFillColor(trafficColours[obj.buildings[j].constructionSystems[key].feasibility[c]][0],trafficColours[obj.buildings[j].constructionSystems[key].feasibility[c]][1],trafficColours[obj.buildings[j].constructionSystems[key].feasibility[c]][2]);
            dc.ellipse(130+c*28,yConstrVal-1,2,2,'DF');
          }

          yConstrVal += 6;
          for (k=0; k<obj.buildings[j].constructionSystems[key].warnings.length; k++)
          {
            dc.setFontStyle('normal');
            if (obj.buildings[j].constructionSystems[key].warnings[k].length!==0)
            {
              dc.text(obj.buildings[j].constructionSystems[key].warnings[k],20,yConstrVal);
              yConstrVal += 5;
            }
          }
          yConstrVal += 1;
        }
        }

        //SEtting the panels manufacturing methods page of the document
        dc.addPage();

        dc.setFontSize(12);
        dc.setTextColor(0);
        dc.text('1.'+(j+1)+'.2 Systems Analysis / Panelised (Category 2)', 10,14);

        dc.setLineWidth(0.2);
        dc.setDrawColor(0);
        dc.line(10,15,287,15);

	dc.setFontStyle('bold');
        dc.text("System:",15,22);
        dc.setFontSize(10);
        dc.text("Total Height",117,22);
        dc.text("Floor 2 Floor", 148,22);
        dc.text("Party Wall", 178,22);
        dc.text("Exterior Wall", 203,22);
        dc.text("Element Size", 231,22);
        dc.text("Span", 266,22);
        dc.setFontSize(12);

        var yConstrVal = 30;
        for (var key in obj.buildings[j].constructionSystems)
        {
          if (key.includes("Panels"))
          {
          dc.setFontSize(10);
          dc.setFontStyle('bold');
          dc.text(key + ': ', 15,yConstrVal);

          for (c=0; c<obj.buildings[j].constructionSystems[key].feasibility.length; c++)
          {
            dc.setFillColor(trafficColours[obj.buildings[j].constructionSystems[key].feasibility[c]][0],trafficColours[obj.buildings[j].constructionSystems[key].feasibility[c]][1],trafficColours[obj.buildings[j].constructionSystems[key].feasibility[c]][2]);
            dc.ellipse(130+c*28,yConstrVal-1,2,2,'DF');
          }

          yConstrVal += 6;
          for (k=0; k<obj.buildings[j].constructionSystems[key].warnings.length; k++)
          {
            dc.setFontStyle('normal');
            if (obj.buildings[j].constructionSystems[key].warnings[k].length!==0)
            {
              dc.text(obj.buildings[j].constructionSystems[key].warnings[k],20,yConstrVal);
              yConstrVal += 5;
            }
          }
          yConstrVal += 1;
        }
        }

      }

//Setting the page for Volumetric Modules

      var c2 = 0;
      var modules = obj.modulesNumbers;
      var modColors = obj.modulesColors;
      for (var mod in modules)
      {
        if (c2%25==0)
        {
          c2 = 0;
          dc.addPage();

          dc.setFontSize(12);
          dc.setTextColor(0);
          dc.text('2.1 System: Volumetric (Category 1)', 10,14);

          dc.setLineWidth(0.2);
          dc.setDrawColor(0);
          dc.line(10,15,287,15);

          dc.setFontSize(14);
          dc.setFontStyle("bold");
          dc.text('Distinct Modules:', 15,25);

          dc.setFontSize(10);
          dc.setFontStyle("normal");
          dc.text('Module Key ([Full Inclusion] | [Partial Inclusion])',15,35);
          dc.text('Width:',100,35);
          dc.text('Length:',130,35);
          dc.text('Height:',160,35);
          dc.text('Total Number:',190,35);

        var modCells = mod.split(":");
        var size = modCells[2].split(",");
        dc.text(modCells[1],15,45+c2*5);
        dc.text(size[0],100,45+c2*5);
        dc.text(size[1],130,45+c2*5);
        dc.text(size[2],160,45+c2*5);
        dc.text(modules[mod].toString(),190,45+c2*5);

        }
        else
        {
      var modCells = mod.split(":");
      var size = modCells[2].split(",");
      dc.text(modCells[1],15,45+c2*5);
      dc.text(size[0],100,45+c2*5);
      dc.text(size[1],130,45+c2*5);
      dc.text(size[2],160,45+c2*5);
      dc.text(modules[mod].toString(),190,45+c2*5);
        }
        c2++;
      }

      //Setting the page for 2D Panels

      var c3 = 0;
      var panels = obj.panelsNumbers;
      for (var pan in panels)
      {
        if (c3%25==0)
        {
          c3 = 0;
          dc.addPage();

          dc.setFontSize(12);
          dc.setTextColor(0);
          dc.text('2.2 System: Panelised (Category 2)', 10,14);

          dc.setLineWidth(0.2);
          dc.setDrawColor(0);
          dc.line(10,15,287,15);

          dc.setFontSize(14);
          dc.setFontStyle("bold");
          dc.text('Distinct Panels:', 15,25);

          dc.setFontSize(10);
          dc.setFontStyle("normal");
          dc.text('Panel Key (Area m\xB2)',15,35);
          dc.text('Length:',100,35);
          dc.text('Width:',130,35);
          dc.text('Height:',160,35);
          dc.text('Total Number:',190,35);

        var modCells = pan.split(":");
        var size = modCells[2].split(",");
        dc.text(modCells[1],15,45+c3*5);
        dc.text(size[0],100,45+c3*5);
        dc.text(size[1],130,45+c3*5);
        dc.text(size[2],160,45+c3*5);
        dc.text(panels[pan].toString(),190,45+c3*5);

        }
        else
        {
      var modCells = pan.split(":");
      var size = modCells[2].split(",");
      dc.text(modCells[1],15,45+c3*5);
      dc.text(size[0],100,45+c3*5);
      dc.text(size[1],130,45+c3*5);
      dc.text(size[2],160,45+c3*5);
      dc.text(panels[pan].toString(),190,45+c3*5);
        }
        c3++;
      }

      //Setting the general warnings page of the document
      dc.addPage();

      dc.setFontSize(12);
      dc.setTextColor(0);
      dc.text('2.0 Comments / Warnings', 10,14);

      dc.setLineWidth(0.2);
      dc.setDrawColor(0);
      dc.line(10,15,287,15);

      var warnings = obj.generalWarnings;

      dc.setFontSize(13);
      dc.setTextColor(0);
      for (i=0; i<warnings.length; i++)
      {
        dc.text(warnings[i], 10,25 + i*10);
      }
	  
		dc.save(fName+'.pdf');
	},

});